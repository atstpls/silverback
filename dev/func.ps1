
function msgbox {
  param(
    [string]$msg
  )
  Add-Type -AssemblyName PresentationFramework | Out-Null
  [System.Windows.MessageBox]::Show($msg) | Out-Null
}
function re {
  cd $env:USERPROFILE\source\repos
}
function Get-DotNetVersions {
  Get-ChildItem 'HKLM:\SOFTWARE\Microsoft\NET Framework Setup\NDP' -recurse | gp -name Version -EA 0 | where { $_.PSChildName -match '^(?!S)\p{L}'} | select PSChildName, Version
}
function Get-GithubCommits {
  param(
      [string]$user_repo,
      [string]$token
      )
  $headers = @{Authorization="token $token";Accept="application/vnd.github.v3+json"}
  $commits = Invoke-RestMethod -Headers $headers  -Uri "https://api.github.com/repos/$user_repo/commits"

  $a=@()
  $commits.commit | Select-Object -First 6 | %{

      $com_url = Invoke-RestMethod -Headers $headers -Uri $_.commit.url 
      $dels = $com_url.stats.deletions
      $file = $com_url.files.filename
      $adds = $com_url.stats.additions
      $patc = $com_url.files.patch 

      # $x = $_.tree |group path -NoElement
      $a += New-Object -TypeName psobject -Property @{
          Author          = $_.author.name;
          Email           = $_.author.email;
          Date            = $_.author.date;
          # Message       = $_.message.SubString(0,20);
          # Files         = $($x.Values -join "`n" | Out-String);
          URL             = $_.url;
          Additions       = $adds;
          Deletions       = $dels;
          Filename        = $file;
          Patch           = $patc;

      } 
  }
  $a 
}

function ref { 
  param(
    [string]$mod
  )
  $ErrorActionPreference = 'SilentlyContinue'
  switch ($mod) { 
    'at'    {  Remove-Module AzureTools; Import-Module ~\source\repos\AzureTools\AzureTools.psm1 }
    'et'    {  Remove-Module EndpointTools; Import-Module ~\source\repos\AzureTools\EndpointTools.psm1 }
    'it'    {  Remove-Module IdentityTools; Import-Module ~\source\repos\AzureTools\IdentityTools.psm1 }
    default {  
      Remove-Module AzureTools; Import-Module ~\source\repos\AzureTools\AzureTools.psm1 
      Remove-Module EndpointTools; Import-Module ~\source\repos\AzureTools\EndpointTools.psm1 
      Remove-Module IdentityTools; Import-Module ~\source\repos\AzureTools\IdentityTools.psm1 
    }
  }
  $ErrorActionPreference = 'Continue'
  
}

function Load-ExternalFunctions {
  Invoke-RestMethod -Uri $extUrl/extFunc.ps1 | Invoke-Expression   
}
function Show-Functions {
  param(
    [string]$file,
    [switch]$buildArray
  )
  $funcs =  Select-String -Pattern '^function?\s([^\s\(\{]+)' -Path $file | %{$_.Matches} | %{$_.Captures.Groups[1].Value}
  
  if ($buildArray) {
    Write-Host `n'$funcs =  @('`n
    $funcs | sort | %{
      Write-Host `t"`'$_`'"
    }
    Write-Host "`n)"
    Write-Host 'Export-ModuleMember -Function $funcs'`n


  }
  else {
    return $funcs | Sort-Object
  }
}

function Show-AliasObjects {
  param(
    [string]$file,
    [array]$functions
  )
  if (!$functions) {
    $funcs = Show-Functions $file 
  }
  else {
    $funcs = $functions
  }
  $funcs | Foreach-Object{
    Clear-Vars first 
    $first = Select-String "$_" $file -Context 1,4 | Select-Object -first 1
    $aliasObject = ($first | out-string).split("`n") | Foreach-Object{$_.split(':')[2] }
    $aliasObject
  }
}

function Get-ExtTempAWSCreds {
  param
  (
      [Parameter(Mandatory = $true )]
      [ValidateLength(12,12)][string]$accountid,

      [Parameter(Mandatory = $true )]
      [string]$user,
      
      [Parameter(Mandatory = $true )]
      [string]$role,
      
      [Parameter(Mandatory = $true )]
      [string]$credfile,

      [Parameter(Mandatory = $true )]
      [ValidateLength(6,6)][string]$token
  )

  function main {

  [array]$t = Get-Content $env:USERPROFILE\.cwaite.txt

  $id = [System.Runtime.InteropServices.marshal]::PtrToStringAuto([System.Runtime.InteropServices.marshal]::SecureStringToBSTR($($t[0] | ConvertTo-SecureString)))
  $key = [System.Runtime.InteropServices.marshal]::PtrToStringAuto([System.Runtime.InteropServices.marshal]::SecureStringToBSTR($($t[1] | ConvertTo-SecureString)))
  $sn = "arn:aws:iam::" + $accountid + ":mfa/" + $user
  $ra = "arn:aws:iam::" + $accountid + ":role/" + $role

  $r = Use-STSRole -AccessKey $id -SecretKey $key -RoleArn $ra -Region "us-east-1" -RoleSessionName $user -SerialNumber $sn -TokenCode $token -DurationInSeconds 43200
  $expire = $r.Credentials.Expiration

  # Clear out old temp credentials
  [array]$creds = (Select-String -Path $env:USERPROFILE\.aws\credentials -Pattern '\[temp\]').LineNumber
  [array]$linenumbers = @()
  $creds|%{$linenumbers += ($_-1),$_,($_+1),($_+2),($_+3),($_+4)} | Out-File $env:USERPROFILE\.aws\credentials

  # Add credentials to $env:USERPROFILE\.aws\credentials file
  $content = @"

  [temp]
  aws_access_key_id=$($r.Credentials.AccessKeyId)
  aws_secret_access_key=$($r.Credentials.SecretAccessKey)
  aws_session_token=$($r.Credentials.SessionToken)

"@
  Add-Content -Value $content -Path $env:USERPROFILE\.aws\credentials
  if ($r){
      Write-Host -Fore Green "[+] " -NoNewLine; Write-Host $(Get-Date).ToShortTimeString()": Temp creds for"$role" role valid until " -NoNewLine;Write-Host -Fore Green $expire `n
  }
}
  Test-AwsPowerShell
  Test-CredentialFile
  main
}
function Test-AwsPowerShell {
  if (!($(Get-Module AWSPowerShell))) {
      Write-Fail "AWSPowerShell module not installed... Exiting!"
      Break
  }
}
function Test-CredentialFile {
  if (!(Test-Path $env:USERPROFILE\.aws\credentials)) {
      Write-Host -Foreground Gray "[-] " -NoNewLine; Write-Host "$env:USERPROFILE\.aws\credentials file not found... creating."
      New-Item -Type File -Value '' -Path "$env:USERPROFILE\.aws\credentials" -Force | Out-Null 
  }
}
function Get-Ext ($tok){
  Get-ExtTempAWSCreds  -accountid $ext_id -user $ext_user -role operations -credfile $ext_credfile -token $tok
}

Set-Alias -Name le -Value "Load-ExternalFunctions" -Scope Global -Force 
Set-Alias -Name cc -Value "Copy-Cred" -Scope Global -Force 








