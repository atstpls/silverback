function Test-AwsPowerShell {
  if (!($(Get-Module AWSPowerShell))) {
      Write-Host -Fore Red "[-] " -NoNewLine; Write-Host "AWSPowerShell module not installed... Exiting!"
      Break
  }
}
function Test-CredentialFile {
  if (!(Test-PATH $env:USERPROFILE\.aws\credentials)) {
      Write-Host -Foreground Gray "[-] " -NoNewLine; Write-Host "$env:USERPROFILE\.aws\credentials file not found... creating."
      New-Item -Type File -Value '' -PATH "$env:USERPROFILE\.aws\credentials" -Force | Out-Null 
  }
}

function Get-ExtTempAWSCreds {
  param
  (
      [Parameter(Mandatory = $true )]
      [ValidateLength(12,12)][string]$accountid,

      [Parameter(Mandatory = $true )]
      [string]$user,
      
      [Parameter(Mandatory = $true )]
      [string]$role,
      
      [Parameter(Mandatory = $true )]
      [string]$credfile,

      [Parameter(Mandatory = $true )]
      [ValidateLength(6,6)][string]$token
  )

  function main {

  [array]$t = Get-Content $env:USERPROFILE\.cwaite.txt

  $id = [System.Runtime.InteropServices.marshal]::PtrToStringAuto([System.Runtime.InteropServices.marshal]::SecureStringToBSTR($($t[0] | ConvertTo-SecureString)))
  $key = [System.Runtime.InteropServices.marshal]::PtrToStringAuto([System.Runtime.InteropServices.marshal]::SecureStringToBSTR($($t[1] | ConvertTo-SecureString)))
  $sn = "arn:aws:iam::" + $accountid + ":mfa/" + $user
  $ra = "arn:aws:iam::" + $accountid + ":role/" + $role

  $r = Use-STSRole -AccessKey $id -SecretKey $key -RoleArn $ra -Region "us-east-1" -RoleSessionName $user -SerialNumber $sn -TokenCode $token -DurationInSeconds 43200
  $expire = $r.Credentials.Expiration

  # Clear out old temp credentials
  [array]$creds = (Select-String -PATH $env:USERPROFILE\.aws\credentials -Pattern '\[temp\]').LineNumber
  [array]$linenumbers = @()
  $creds|%{$linenumbers += ($_-1),$_,($_+1),($_+2),($_+3),($_+4)} | Out-File $env:USERPROFILE\.aws\credentials

  $content = @"

  [temp]
  aws_access_key_id=$($r.Credentials.AccessKeyId)
  aws_secret_access_key=$($r.Credentials.SecretAccessKey)
  aws_session_token=$($r.Credentials.SessionToken)

"@
  Add-Content -Value $content -PATH $env:USERPROFILE\.aws\credentials
  if ($r){
      Write-Success $(Get-Date).ToShortTimeString(),": Temp creds for"$role" role valid until ",$expire
  }
}
  Test-AwsPowerShell
  Test-CredentialFile
  main
}


function Show-AliasObjects {
  param(
    [string]$file,
    [array]$functions
  )
  if (!$functions) {
    $funcs = Show-Functions $file 
  }
  else {
    $funcs = $functions
  }
  $funcs | Foreach-Object{
    Clear-Vars first 
    $first = Select-String "$_" $file -Context 1,4 | Select-Object -first 1
    $aliasObject = ($first | out-string).split("`n") | Foreach-Object{$_.split(':')[2] }
    $aliasObject
  }
}

function Show-Functions {
  param(
    [string]$file,
    [switch]$buildArray
  )
  $funcs =  Select-String -Pattern '^function?\s+([^\s\(\{]+)' -PATH $file | Foreach-Object{$_.Matches} | Foreach-Object{$_.Captures.Groups[1].Value}
  $filts =  Select-String -Pattern '^filter?\s+([^\s\(\{]+)' -PATH $file | Foreach-Object{$_.Matches} | Foreach-Object{$_.Captures.Groups[1].Value}
  
  if ($buildArray) {
    Write-Host `n'$funcs =  @('`n
    $funcs + $filts | sort | Foreach-Object{
      Write-Host `t"`'$_`'"
    }
    Write-Host "`n)"
    Write-Host 'Export-ModuleMember -Function $funcs'`n
  }
  else {
    return $funcs + $filts | Sort-Object
  }
}

function Open-NetworkConnections {
  try {
    [System.Diagnostics.Process]$proc = [System.Diagnostics.Process]::new();
    $proc.StartInfo.FileName = "rundll32.exe";
    $proc.StartInfo.Arguments = "shell32.dll,Control_RunDLL ncpa.cpl,,0";
    $proc.Start();
  }
  catch { }
}
function Get-DotNetVersions {
  Get-ChildItem 'HKLM:\SOFTWARE\Microsoft\NET Framework Setup\NDP' -recurse | gp -name Version -EA 0 | Where-Object { $_.PSChildName -match '^(?!S)\p{L}'} | Select-Object PSChildName, Version
}
function Get-AzSubscriptionAlerts {
  param([string]$sub)

  $alerts = Invoke-AzRestMethod -Method GET -PATH "subscriptions/$sub/providers/Microsoft.Security/alerts?api-version=2020-01-01"
  ($alerts.Content |ConvertFrom-Json).value.properties | Select-Object timeGeneratedUtc,alertDisplayName,severity,compromisedEntity
}
function Get-GithubCommits {
  param(
      [string]$user_repo,
      [string]$token
      )
  $headers = @{Authorization="token $token";Accept="application/vnd.github.v3+json"}
  $commits = Invoke-RestMethod -Headers $headers  -Uri "https://api.github.com/repos/$user_repo/commits"

  $a=@()
  $commits.commit | Select-Object-Object -First 6 | Foreach-Object{

      $com_url = Invoke-RestMethod -Headers $headers -Uri $_.commit.url 
      $dels = $com_url.stats.deletions
      $file = $com_url.files.filename
      $adds = $com_url.stats.additions
      $patc = $com_url.files.patch 

      # $x = $_.tree |group PATH -NoElement
      $a += New-Object -TypeName psobject -Property @{
          Author          = $_.author.name;
          Email           = $_.author.email;
          Date            = $_.author.date;
          # Message       = $_.message.SubString(0,20);
          # Files         = $($x.Values -join "`n" | Out-String);
          URL             = $_.url;
          Additions       = $adds;
          Deletions       = $dels;
          Filename        = $file;
          Patch           = $patc;

      } 
  }
  $a 
}
function Get-PromptForObtainingTokens {
  $default="N"
  Write-Host "Obtain " -NoNewLine;Write-Host -Fore Blue "msgraph" -NoNewLine;Write-Host " and " -NoNewLine;
  Write-Host -Fore Blue "azsvcmgmt" -NoNewLine;Write-Host " tokens? [y/$default]" -NoNewLine;
  $value = Read-Host

  if (!$value) {
    $value = $default 
  }
  if ($value -ne 'N'){
    Write-Host "`n"
    Get-ModuleRefresh 
    Get-NewTokens msgraph 
    Get-NewTokens azsvcmgmt
  }
}

function New-MessageBox {
  param(
    [string]$msg
  )
  Add-Type -AssemblyName PresentationFramework | Out-Null
  [System.Windows.MessageBox]::Show($msg) | Out-Null
}
function Get-ToReposDirectory {
  Set-Location $env:USERPROFILE\source\repos
}

function Test-ModuleFile {
  param(
    [string]$modulefile,

    [string]$func,

    [switch]$raw
  )
  Clear-Vars obj,objs
  $objs = [System.Collections.ArrayList]::new()
  $cmdletObjects = (Invoke-Expression $("$" + $modulefile + "Cmdlets")).Name
  $showFunctions  = Show-Functions ("$env:USERPROFILE\source\repos\AzureTools\" + $moduleFile + ".psm1")
  $exportFunctions = Invoke-Expression $("$" + $modulefile + "Functions")
  $allofthem  = $cmdletObjects.Name + $showFunctions + $exportFunctions  
  
  if ($func) {
    $allofthem = $func
  }
  $allofthem | ForEach-Object {
    if ($_ -in $cmdletObjects)   { $cmdletObject = "Y" } else { $cmdletObject = "-" }
    if ($_ -in $showFunctions)   { $showFunction = "Y"} else { $showFunction = "-"}
    if ($_ -in $exportFunctions) { $exportFunction = "Y" } else {$exportFunction = "-"}

    $obj = New-Object -TypeName psobject -Property @{
      function        = $_
      cmdletObject    = $cmdletObject
      showFunction    = $showFunction
      exportFunction  = $exportFunction
    }
    [void]$objs.Add($obj)
  }
  if ($raw){
    $objs | Select-Object function,cmdletObject,showFunction,exportFunction
  }
  else {
    $objs | Select-Object function,cmdletObject,showFunction,exportFunction | Where-Object { 
        $_.cmdletObject -eq "-" -or $_.showFunction -eq "-" -or $_.exportFunction -eq "-" 
    }
  }
}



Import-Module oh-my-posh
Import-Module posh-git
Set-PoshPrompt -Theme robbyrussel 
Set-PSReadLineOption -Colors @{"Parameter"="#cc5801"}

$env:PATH             += ";$env:PATH\AppData\Roaming\Python\Python39\Scripts"
$env:PATH = $env:PATH += ';C:\Program Files\Git\bin;C:\Program Files\Git;C:\Program Files\Git\usr\bin'
$env:PATH = $env:PATH += ";$env:PATH\go\bin"
$env:PATH = $env:PATH += ';C:\Windows\Microsoft.NET\Framework64\v4.0.30319\'

Set-Alias -Name le -Value "Load-ExternalFunctions" -Scope Global -Force 
Set-Alias -Name cc -Value "Copy-Cred" -Scope Global -Force 
Set-Alias -Name re -Value "Get-ToReposDirectory" -Scope Global -Force 
Set-Alias -Name mb -Value "New-MessageBox" -Scope Global -Force 
Set-Alias -Name ref -Value "Get-ModuleRefresh" -Scope Global -Force 



