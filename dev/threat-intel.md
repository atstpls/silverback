
# Leveraging Knowledge of the Adversary 

Leveraging knowledge of adversaries improves detections.


- [Threat Intelligence](#threat-intelligence)
    - [Definitions and Concepts](#definitions-and-concepts)
    - [Analytic Process](#analytic-process)
    - [Structured Analytic Techniques](#structured-analytic-techniques)
    - [Generation vs Consumption](#generation-vs-consumption)
    - [Threat Modeling](#threat-modeling)
- [Collection](#collection)
    - [Intrusion Analysis](#intrusion-analysis)
    - [Malware](#malware)
    - [Domains](#domains)
    - [External](#external)
    - [Certificates](#certificates)
- [Processing](#processing)
    - [Kill Chain](#kill-chain)
    - [Diamond Model](#diamond-model)
    - [CoA Matrix](#coa-matrix)
    - [MITRE Framework](#mitre-framework)
- [Production](#production)
    - [Logical Fallacies](#logical-fallacies)
    - [Cognitive Biases](#cognitive-biases)
    - [Analysis of Competing Hypotheses](#analysis-of-competing-hypotheses)
    - [Clustering Intrusions](#clustering-intrusions)
    - [Creating an Activity Group](#creating-an-activity-group)
- [Dissemination](#dissemination)
    - [Tactical Intel](#tactical-intel)
    - [Operational Intel](#operational-intel)
    - [Strategic Intel](#strategic-intel)
- [Attribution](#attribution)
    - [Threat Actors](#threat-actors)
    - [Activity Groups](#activity-groups)
    - [Campaigns](#campaigns)

<br>

## Threat Intelligence

Intelligence is both a product and a process. 

The **product** is analytical judgements resulting from a significant amount of collection across multiple sources and time periods. 

The **process** is extensive intrusion analysis and relentless pursuit of malware, domains, and external datasets.

The basis of all CTI is adversary actions (in other words intrusion data). Incident responders play a key role in the collection step.

IR is guided by CTI, CTI lives on data from IR.

Data > Information (facts) which answers yes/no questions > Make analytical judgements based on facts 


Use ACH mixed with Kill Chain, Diamond, and Rule of 2 to correlate clusters and validate findings 

Activity Groups are communicated to broad audiences
But there are technical indicators and more precise language around each when you zoom
Also a more formal definition of the group which is a cluster of unique tradecraft and victimology 


Analyze and cluster intrusions primarily to satisfy intel requirements, not track adversaries.

Groups are not names, they are definitions. And definitions must be understood.

Once we understand activity groups, we can design defensive strategies around them, playbooks for defenders 

For 3rd party attribution analysis, extract the evidence not the conclusions





<img class="img-fluid" width="1600px" src="img/intel-cycle.png" alt="img-verification">

<br>

### Definitions and Concepts 

|Term|Definition|
|-|-|
|[Data](#data-information-intelligence)|Sets of values and individual elements of what makes up information|
|[Information](#data-information-intelligence)|Collection of data that can answer yes/no questions |
|[Intelligence](#data-information-intelligence)|Doesn't answer yes/no questions |
|[Intelligence Requirements](#intelligence-requirements)|Objectives that satisfy knowledge gap about threat or operational environment |
|[Counterintelligence]()|Identification, assessment, and neutralization of adversary intelligence activities|
|[Threat Intel]()|Analyzed information about the hostile intent, opportunity, and capability of an adversary that satisfies a requirement|
|Vulnerabilities|Opportunities, but alone are not threats|
|Adversary/threat|Represents human behind keyboard|
|Intrusion|Any successful or failed attempt by adversary to compromise system|
|Intrusion Set|Clustering and linking of multiple intrusions together via key indicators | 
|[Activity Group](#activity-group)|Set of events/activities associated by similarities in features and weighted by confidence scoring. Unique clusters of intrusions mathematically defined by team's analytical weighting (confidence scoring)|
|[Threat Actor](#threat-actor)|Who is responsible, an attribution-based clustering|
|[Campaign/Operation](#campaign)|Adversary's mission focus, the objective of a series of intrusions or operations|
|[Traffic Light Protocol (TLP)](#traffic-light-protocol)|White, Green, Amber, Red are banners to identify who information can be shared to| 
|Persona|Fake name/identifier used by adversary, online presence|
|[TTPs](#tradecraft-vs-ttps)|Tactic is high-level tradecraft, technique is manner accomplished, procedure is granular steps/commands|
|[Tradecraft](#tradecraft-vs-ttps)|Combination of methods, capabilities, resources leveraged in course of actions|
|[Indicators](#indicators)|Data in addition to context|
|Key Indicator|Defines an activity group or cluster. Has optimal mitigation, detection, and prioritization |
|Indicator Acceleration|The more indicators an org has, the more it detects and the more it accrues, at a rate increasing over time|
|Target|Ultimate goal of adversary, can be individuals, groups, organizations, companies, governments, facilities |
|Victim|Anything compromised along the way|
|Generation|Generate intelligence through structured models |
|Collection|Analyze intrusions, seek intelligence from external sources|
|Analysis & Production|Exploit intelligence, construct descriptions of campaigns, actors, and organizations|
|Storage & Dissemination|Share and use intelligence, tactical, operational, and strategic|
|Analytical Judgement|Assess information significance and impact using theoretical foundations and structured methodologies, Heuer's Analysis of Competing Hypothesis framework, Lewin's Force Field Analysis|
|Data-Driven Analysis|Assess dataset accuracy and completeness using logically-driven and repeatable methods|
|Conceptually-Driven Analysis|numerous unknowns and undefined variables/relationships, accuracy driven by mental models and feedback over time, more common but doesn't often lead to analytic consensus, especially important to identify biases |

<br>

### Analytic Process

Analytic process produces analytical judgements

- Analyze observed activity and adversary intent 
- Synthesize (combine ideas to form a theory or system) migrated activity, profiles, and campaigns 
- Deconstruct intrusions to understand the impact and assess how best to respond
- Analyze threat surface to identify vulnerabilities and weaknesses
- Break down adversary campaigns to identify trends that give insight into intent and capabilities

<br>

**Kent's Analytic Doctrine**

Sherman Kent was a Yale University History professor, considered father of intelligence analysis, during WWII joined the OSS which became the CIA. Authored *Strategic Intelligence for American World Policy* to formalize understanding of analysis 

|Step|Description|
|-|-|
|1| Focus on Policymaker Concerns (Intelligence must be useful)|
|2| Avoidance of a Personal Policy Agenda (Break your biases)|
|3| Intellectual Rigor|
|4| Conscious Effort to Avoid Analytic Biases (Break your biases)|
|5| Willingness to Consider Other Judgements |
|6| Systematic Use of Outside Experts |
|7| Collective Responsibility for Judgement |
|8| Effective Communication of Policy-Support Information & Judgement |
|9| Candid Admission of Mistakes  |

<br>

|Method|Description|
|-|-|
|Working Memory|- processes inputs, decides their importance, what gets focus and maintained|
|Pattern Recognition|- matches info from external sources to information in long-term memory<br>- 6 patterns, template matching, prototype matching, top-down processing|
|Template Matching|- theory states every object/experience stored as a template<br>- way we interpret something is how it gets stored in template|
|Prototype Matching|- theory states every object/experience stored as prototype<br>- or average of similiar templates, more flexible and can pattern match with incomplete information|
|Top-Down|- use previous knowledge to fill in gaps<br>- not always accurate, accounts for optical illusions in experience-based cognitive biases|
|Dual Process Theory|-System 1 is unconscious decision-making, fast intuitive, mental models, often right<br>- System 2 is intentional, deliberate complex decision-making<br>- Mental Models are experience-based assumptions and expectations of the way the world operates<br>- Kill Chain (System 2) is the actions adversary must take and where defenses are needed<br>- Diamond Model (System 2) is data-driven analysis using clustering, classification, game theory, and graph analysis|
|Mental Models|Experience-bassed assumptions and expectations of the way the world operates (System 1)|

<br>

### Structured Analytic Techniques

Approaches to better evaluate information while reducing impact of bias for more transparent, testable, and defendable analytical judgements 

**2 key tasks**
    
1. Decomposition (into components) to be considered separately
2. Visualization (capturing) to organize and show interrelations 


**2 tools for SA**T 

1. Brainstorming tools 
        - help capture and sort information quickly 
        - group is ideal, tool with collaboration 

2. Sorting tools 
        - spreadsheets, pivot tables, manipulate data to identify trends 

MindMup - online tool for brainstorming 


**8 categories refined to 6 families** 

- getting organized 
- reframing 
- exploration 
- foresight 
- diagnostic 
- decision support 

**Richards J Heuer** 

- Focused on analysis types, critical thinking models and approaches, overcoming biases 
- authored books *Psychology of Intelligence Analysis* and *Structured Analytic Techniques for Intelligence Analysis*
- developed [Analysis of Competing Hypotheses](#analysis-of-competing-hypotheses)

<br>

### Generation vs Consumption 

#### Generation 

Intrusion analysis, strategic value, creating intel through models/processes, analyze intrusions and understand larger campaigns of adversary 

Threat intel team as independent team with direct ties to different orgs, in center of all security-related teams with direct ties to other teams 

**Priority Intel Requirements** (PIRs) fluctuate due to immediate future, threats, situations. Intended audience and their goals determine type of threat intelligence generated and how it is to be used 

|Type|Description|
|-|-|
|Strategic|businesss units at most risk, investments in security|
|Operational|adversary groups in industry, assets targeted by FUZZYSQUIRREL |
|Tactical  |behaviors SOC to focus on, indicators most relevant |

<br>

**Collection Management Framework (CMF)** plan for how you collect data, where you collect it from, and what type of data you collect 

| | Endpoint Protection | Windows Systems | Network | Firewall | 
|-|-|-|-|-|
|Data Type|System Alert|Host-Based Logs|Netflow|System Alert|
|Kill Chain Coverage|Exploitation and Installation|Exploitation, Installation, and AoO|Internal Recon, Delivery, C2 | Internal Recon, Delivery, C2 |
|Follow on Collection | Malware sample | Files and timelines | Packet capture | Netflow | 
|Typical Storage in Days | 30 days | 60 days | 23 days | 60 days | 

<br>

#### Consumption

Intel teams with **High-functioning Defenders** on top of **Well-tuned Passive Defenses** inside a **Secure-by-design Defensible Architecture** beats the adversary 

Defense-focused, organization-specific, validate what you are looking at is legitimate and try to apply it to your use cases.

The **Sliding Scale** is a way to frame discussion of actions without getting overly technical. Take things from the right (more expensive) and drive the lessons to the left (internalize knowledge of threats)

|Type|Description|
|-|-|
|Architechure|Supply chain, network architecture, maintenance, patching, better environments, vulns, development |
|Passive Defense|Protection without constant human interaction, firewalls, IPS, AV, IDS, SIEMS, tools, tuning defenses, placement of technologies, visibility and collection, making the static flexible|
|Active Defense|Consuming intelligence, Monitor/respond to, learn from adversaries internal to network, threat hunting, IR, network monitoring, malware analysis, human component of defense in pursuit and countering of adversary.  ACDC model,|
|Intelligence|Collect data, exploit it into information, produce intelligence, adversary emulation, hypothesis generation, team restructuring, RED TEAMS emulate adversary, pentesting validates architecture and vulnerabilities|
|Offense|Legal countermeasures, hack-back, costly, poor ROI, harder than defensive, not in spirit of the law, take-down operations are NOT offensive  |

<br>

<img class="img-fluid" width="1600px" src="img/new-pyramid.png" alt="img-verification">

<br><br>

Threat-based approaches require programming knowledge focused on the adversary 

Environmental-based generate alerts but lack context for activity detected.

|  | Environmental | Threat | 
|-|-|-|
| Unknowns |  Modeling               | Threat Behaviors | 
| Knowns   |  Configuration Analysis | Indicators |

<br>

### Threat Modeling

Build threat models using either **Systems Analysis** or **Target-centric Analysis**. 
Identify what information or resources could be targeted, add potential adversaries that may target you.

Systems analysis looks at 4 components 
- structure 
- information or technologies 
- individuals or roles 
- tasks that it completes 

Explores forces that cause the system to change

|Term|Definition|
|-|-|
|System|An orderly grouping of interdependent components linked together according to a plan to achieve a specific goal |
|Systems Analysis|Set of mental models, templates, and prototypes that make up a system we are concerned with, Use to build baseline understanding of organization, so we are postured to analyze threates in relation to that system, or analyze adversary and identify their requirements |
|Threat Modeling|Aligns team around common understanding of threats, highlights gaps in security postures|
|Target-centric intelligence analysis|Developed by Robert Clark, Creates flexible, updatable conceptual model designed to prevent stovepiping|
|VERIS|Vocabulary for Event Recording and Incident Sharing captures metrics on events and incidents, strategic level counterpart to indicator sharing|

<br>

Pivot off information and resources to create new targets, Pivot off adversaries to identify toolsTTPs they use.

#### VERIS 

Vocabulary for Event Recording and Incident Sharing is a strategic-level counterpart to indicator sharing.

**4 As of incident details**

|Type|Description|
|-|-|
|Action|files, software, social engineering, abuse, misuse, physical (USB), configurations that resulted in incident, environmental like storms, natural disasters|
|Asset|who managed asset, variety, owner, employee |
|Actor|adversary or employee responsible, internal/external/partner/unknown, motivation, variety |
|Attribute|how was asset affected, CIA, type of data, duration of compromise, identifies impact |

<br>

date, incident ID, confirmed or not, how discovered, YES/NO TRUE/FALSE answers simplifying use, can provide variety, vector, or vulnerability 

**VCAF VERIS Common Attack Framework** captures nuanced technical details of attacks 

- understand technical details associated with an incident 
- prioritize mitigations based on all incident types 
- understand junction of targeting and capabilities 
- capture incident context that goes beyond technical artifacts 
- ease communication of cybersecurity concepts with non-cybersecurity experts 

Track and conduct trend analysis on threats, which helps update threat profiles.  Track internal data against external data presented in large-scale reports like Verizon DBIR. 

<br>

## Collection

Most good threat data comes from incident response data. It is key to understanding what adversary did and how they accomplished it. 

Extract data to identify patterns and key indicators. 

### Intrusion Analysis 

|THREAT ACTOR|ACTIVITY GROUP|CAMPAIGN|
|-|-|-|
|- Clustered into hacktivists, military, foreign intel, cyber criminals<br>- Loose term that is largely abused and focuses on attribution<br>- Construct is based on "who" attribution<br>- Analytical baggage makes defense difficult|- Prime form of tracking threats by prioritizing Infrastructure, Victim, Tradecraft vertices<br>- Construct is based on "how"<br>- Bound by elements of behavior<br>- Defending against activity group is more precise. Clustering of intrusions grouped by similarity and weighted by confidence<br>- Clusters tracked are specific to an analytical/intel requirements |- The mission focus of adversary across multiple intrusions<br>- Construct is based on our understanding of adversary intent<br>- Large datasets help but collection/visibility gaps can misguide understanding |

<br>

#### Tradecraft vs TTPs 

|Tradecraft|TTPs|
|-|-|
|- Techniques, methods, and technologies leveraged by adversaries<br>- Could include family of malware or style of malicious software used<br>- Consistent use of macro-enabled Word documents to deliver memory-only rootkit<br>- Could include multiple sets of TTPs--Adversary uses rootkit with VPN access, now there are two distinct sets of TTPs, but adversary tradecraft is to leverage them together<br>- can describe M.O. of adversary while encompassing one set or more of tactics<br>- Tradecraft is extremely scalable since it's not bound to any activy group or cluster of intrusions<br>- Contains a high level of context (this tradecraft is indicative of this type of adversary behavior)|- Specific actions despite adversary involved or clustering of activity<br>- very scalable and have context but focus more on behavior being observed<br>- Could help identify rootkit without looking for indicators|
|- Could help identify that a rootkit is being used in a new way by a specific threat|- would describe tactic (persistence) and procedures (specifics of macros) but wouldn't account for family of rootkit leveraged|

<br>

#### Indicator Life Cycle

Describes how indicators beget indicators 

<img class="img-fluid" width="1200px" src="img/ind-life.png" alt="img-verification">

<br><br>

- Indicator is revealed
- When vetted and operationalized it becomes MATURE 
- Passive CoA it becomes UTILIZED 
- Intrusion analysis produces more REVEALED 

Bad indicators will beget bad indicators, good beget good, even when bad is discovered, extracting it and everything that led to it/resulted from it is extremely difficult 

Risk of pollution is high when automation of CTI ingestion occurs. Proliferation of sources of external intel, redundant, exacerbates pollution


**4 main indicator types**

|Type|Description|
|-|-|
|IP addresses|source, hop points, VPS|
|Domains|compromised, actor registered, DDNS| 
|Accounts|email, VPS, service, personal|
|Unique Strings|passwords, mutex, handles, encryption keys|

<br>

**KEY INDICATORS**

- remain consistent across multiple intrusions 
- uniquely distinguish campaigns
- distinguish campaigns from benign activity 
- align to a single category of adversary action (Exploitation)

Value is dictated by adversary. 

Indicator sweeps, SIEM data enrichment, best use is for knowledge gap filling 

#### Pivoting  

Argus does passive collection and analysis of Network Flow data (can also use firewall logs)  
- 'ra' tool       ra -nnnr argus.log.1.gz - 'host 89.34.237.11'
- nnn - don't look up any address names ( reverse resolution ), ports, or protocol names 
- r - read from a file 
- number in logfile means number of days ago it was generated 

Indicator Life Cycle in Action 

```
Got 89.34.237.11 from external report  ( REVEALED )
Vetted it in network flow logs and identified VICTIMS 
Passive CoA (search results) gave us context to reveal another indicator '/fjerk/' 
Vetted, operationalized again and got results (UTILIZED)  
C2 URLs give us 104.224.166.148 (REVEALED)
```

Always proceed to the RIGHT from phase where detection takes place 

### Malware 

Clustering and pivoting through malware samples, header metadata, code reuse, configuration data, PDB strings, ttimestamps, compile environments, PE resources, import tables, digital certificates, mutex 

RichPV-hash fingerprints specific compiling environments, finds relationship between binaries 

Code reuse reveals links in Adversary but can be misleading 

DC3 Malware Configuration Parser (DC3-MWCP) parses configuration information from malware like addresses, passwords, filenames, mutex names 

### Domains 

Adversary registered, dynamic DNS, and legitimate but compromised 

DDNS records have TTL to expedite propagation, providers include Oray, ChangeIP, DYNDNS, 

TTL sets how long DNS clients will cache the IP address for 

### External Sources 

Threat feeds have tactical information and indicators, not intelligence 

**Threat Intelligence Quotient** (TIQ) 

Test that performs:

|Name|Description|
|-|-|
|Novelty Test|Ratio of new indicators and retiring of old ones as feed progresses |
|Overlap Test|Evaluates overlap of indicators between various feeds |
|Population Test|Compare top quantities of reported IP addresses on specific datae by Country or ASN  

<br>

**Collective Intelligence Framework** (CIP) 

Set of scripts that pull data from internal and external data sources to manage data from threat feeds 

**Datasploit** identifies credentials, API keys, subdomains, domain history 

**Discover** performs recon, scanning 

**InfoGo** identifies registration info, subdomains, emails based off of a domain or email address 

**Shodan**, geographical information and maps, GCHQ's cyberchef

### Certificates 

TLS cert fingerprints, issuer, subject, dates, self-signed, let's encrypt

**CaseFile** is offline version of **Maltego** used for sensitive investigations, does not contain transforms 

<br>

## Processing 

**Intelligence Life Cycle**


1. Planning and Direction - Identify and prioritize
2. Collection - Data is collected to fill intelligence gap 
3. Processing and Exploitation - Preparation for raw data
4. Analysis and Production - SATs to form analytical judgement 
5. Dissemination - distributing to customers 

<img class="img-fluid" width="1600px" src="img/intel-cycle-2.png" alt="img-verification">

<br>

Adversaries often align to industry verticals, target specific information types, event or incident-based, 

Pivot off information and resources, ( new targets ) or pivot off adversaries ( new tools/resources/triggers )

Identify critical information/assets -> identify adversaries -> pivot on data points 

<br>

#### Kill Chain 

<img class="img-fluid" width="1600px" src="img/killchain-new.webp" alt="img-verification">

<br><br>

|Phase|Description|
|-|-|
|Recon Precursors|social networking, linked infrastructure/tools, identication of targets, organizational research|
|Weaponization|configuring implants, evasion techniques, containers for delivery, human choices like config data, creds, metadata fields|
|Delivery|Mechanism in which payload gets to target, infrastructure/tool/obfuscation techniques |
|Exploitation|Disposition of exploit: human or technical, affected application, method, exploit code, CVE, POC, follow-on actions|
|Installation|Persistence and invocation, filenames, directories, registry keys/values, communications, droppers and their infastructure|
|C2|Family, hash, protocol, infrastructure, operating mode characteristics (check-ins, beaconing, interactive (commands, uploads, downloads)|
|AoO|Commands executed, tools transferred ( privesc, loggers, stealers, second-stage), files exfiltrated/modified/staged/moved/created|

<br>


#### DIAMOND MODEL 



- Adds layer of depth to data already characterized by Kill Chain which improves intrusion analysis and cross-intrusion correlation 
- Core features are adversary, capability, infrastructure, and victim 
- Meta features are timestamp (start and end), phase, result, direction, methodology, and resources 
- Each core feature and its meta feature should have confidence value 

**Axioms** 

1. Adversary takes step toward intended goal by using capability over infrastructure against victim to produce a result 
2. Set of adversaries exist which seek to compromise computer systems or networks to further their intent/satisfy needs 
3. Every system and every victim asset has vulnerabilities and exposures 
4. Every malicious activity contains 2 or more phases which must be executed in succession to achieve desired result 
5. Every intrusion event requires one or more external resources to be satisfied prior to success 
6. Relationship always exists between adversary and their victim(s) even if distant, fleeting, or indirect 
7. Subset of adversaries that have motivation, resources, capabilities to sustain malicious effects over time against victims resisting mitigation efforts 

<br>

<img class="img-fluid" width="1600px" src="img/diamond-meta.png" alt="img-verification">

<br><br>


|Type|Description|
|-|-|
|Adversary|Individual, group, online presence, accounts, intent, human choices, preference for coding languages, keyboard settings, configuration data, header metadata, code reuse| 
|Capability/TTP|Tools & tradecraft,  exploits, backdoors, staging methods, situational awareness|
|Infrastructure|Any vehicle for delivering capabilities, service accounts, ip addresses, domains, hosts used for recon, email infrastructure, accounts created for supporting operations, social networking personas|
|Victim|Recipient of capabilities deployed across infrastructure by adversaries, person, organization, persona, victim assets (systems, networks )|

<br>

#### Courses of Action (CoA) Matrix 

- it's the Kill Chain for Defenders 
- assists in determining actions available, shows completeness of coverage of intrusions, focuses key indicators, identifies capability gaps 


<img class="img-fluid" width="1600px" src="img/coa-matrix.jpg" alt="img-verification">

<br><br>

|Phase|Description|
|-|-|
|Discover|Happened before? logs, signatures, heuristics, applies to indicators/TTPs |
|Detect|Identify known-bad activity, alert that activity is taking place|
|Deny|Prevent occurrence, blocking IPs, emails, connectivity|
|Disrupt|Stop after already occurring, IPS, DEP/ASLR, force closing long TCP sessions|
|Degrade|Interfering, slowing down, rate limits, attachment stripping, FTP PUT limitations, TCP window sizes |
|Deceive|Providing misinformation to adversary, rerouting traffic to black hole, intentionaly cache poisoning of known C2 domains, deliberate downloads of web bugs|
|Destroy|Hack back, reducing adversary capabilities, denial of service, law enforcement arrests, affects future intrusions|

<br>

Some courses of action are mutually exclusive, Discover/Detect are passive, 
Active or mitigating actions tend to be mutually exclusive to one another  
Deny will prevent disruption, degradation, deception and so forth 

Both passive courses of action should be executed for a given piece of intelligence, and one mitigating course of action selected 
Selection depends on capabilities and intel gain/loss calculus 

Articulate all theoretical courses of action 
Document limitations and inhibitors:  technical, procedural, cognitive 
Identify common limitations 

Consistent limitations drive future investment and focus 

#### MITRE Framework 

Tactics, techniques observed in real world supporting detections, analytics, threat intel, adversary emulation, assessment and engineering 

Complements Kill Chain by defining tactics as the adversary's technical goals.  Tactics are not phases

MITRE sees tactics as goals, not how they're achieving it 

## Production

MISP - Add Event, Add Attachments, Add Event Attributes, visual correlation, linking, 

Platforms should have  Common format, Scalable method, Secure method, Sharable method 

### Logical Fallacies 

Flaws in reason 

|Type|Description|
|-|-|
|Anecdotal|personal experience is used over compelling evidence|
|Appeal to probability|making determination based on what's most likely the case|
|Appeal to the stone|identifying claim as absurd without any proof to dismiss |
|Argument from silence|accepting a conclusion due to lack of evidence against |
|Argument from repitition|arguing so much that eventually people accept the conclusion |
|Burden of proof|requiring someone to disprove someone else's claim instead of requiring proof |
|Middle ground|making a compromise between two points an accepted truth |

<br>

### Cognitive Biases 

Unavoidable but manageable constraints on how we think that influence decisions and assessments 

allow creating own version of reality where inaccurate judgements and illogical interpretations occur 

|Type|Description|
|-|-|
|Anchoring/Focusing|Overvaluing one piece of information, prevents seeking new/competing information|
|Confirmation|Include or reject evidence based on its alignment to a preferred hypothesis|
|Congruence|Failure to find/test alternate hypothesis, but find ways to present data that tests existing hypothesis|
|Hindsight|Unlikely outcome seen as obvious, victim-blaming, simplifies problem|
|Illusory Correlation|stereotypes, observe correlation where none exists|
|Correlative and Causal Confusion| (Cum hoc ergo propter hoc) confusing correlation of events with a causal relationship|
|Mirror Image|book Prisoner, written by Jason Rezaian who couldn't explain Kickstarter to Kazem in Iran|
|Self-serving|Attributing our success to our personal characteristics and blame outside variables for our failures |
|Actor-Observer|Our actions from external causes, other's actions from internal causes, slower drivers are idiots, faster drivers are maniacs|
|Halo Effect|Single trait influence's someone's overall personality|

<br>

### Analysis of Competing Hypotheses

- developed by Richards Heuer Jr, method for evaluating based on observed data while mitigating biases 
- identify all potential hypothesis, collect all evidence, compare evidence with hypotheses, rank and choose best 


||Step|Description|
|-|-|-|
|1.| Hypothesis|Enumerate all hypotheses|
|2. |Evidence|Seek supporting and refuting evidence for each, include deductions and assumptions|
|3. |Diagnostics|Compare evidence for and against each hypothesis as more or less helpful in determining most valid. Matrix of hypotheses and evidence.|
|4. |Refinement|Remove nondiagnostic evidence, add overlooked evidence, include formation of new hypotheses, document evidence excluded |
|5. |Prioritization|Prioritize hypotheses by their likelihood by seeking additional evidence refuting them , reject-first approach  |
|6. |Sensitivity|| |
|7. |Conclusion and Evaluation|| 

<br>

||H1|H2|H3|H4|
|-|-|-|-|-|
|E1. first |-|+|-|-|
|E2. second |+|-|-|-|
|E3. third |-|+|+|+|
|E4. fourth |+|-|-|-|
|E5. fifth |0|0|0|-|
|E6. sixth |+|-|-|-|

<br>

||H4|H3|H2|H1|
|-|-|-|-|-|
|E1. first |-|-|+|-|
|E2. second |-|-|-|+|
|E3. third |+|+|+|-|
|E4. fourth |-|-|-|+|
|E5. fifth |-|0|0|0|
|E6. sixth |-|-|-|+|

<br>

Final report should include hypotheses considered and most important pieces of evidence in your conclusion 

<br>

### Clustering Intrusions 

Mandiant, Palo Alto's Unit 42, Symantec, Microsoft, Kaspersky, Dragos all have their unique schools of thought 


|Type|Description|
|-|-|
|Link Analysis|Analysis of relationships between data points, Entities need sources, links need context<br>Tools: IBM Analyst Notebook, Gotham/Metropolis, Centrifuge, Gephi/Graphviz, New4J, Titan, Linkurious, Keylines |
|Data Analysis|Cleaning, transforming, and modeling of data to reveal patterns and new insights into the data itself |
|Temporal Data Analysis|Reveals patterns of activity that reoccur, identifies trending adversary activity |
|Trend Analysis|Intelligence over time reveals patterns between intrusions, basis for definition of clusters of intrusions|

Style guide helps cluster and track intrusions over long term and guide analysis (such as setting formal definitions for groups).

Must have:
- team structure
- accepted lexicon 
- words, phrases, and actions not to do 
- sample structure analysis techniques 
- sample intelligence requirements with example outputs 
- guidance to analysts on how to create clusters and finalize intelligence products 
- key processes to follow 


|Do|Don't|
|-|-|
|Use names inspired by incidents | Name campaign after tool/TTP |
|Obscure name inspiration |Use enumerated names, attribution-based schemes |
|Use humor |   Name campaign after incident |


[APT Groups and Operations Matrix](https://docs.google.com/spreadsheets/d/1H9_xaxQHpWaa4O_Son4Gx0YOIzlcBWMsdvePFX68EKU/pubhtml#) maps together all different campaign names


1. Create an analytical model to analyze intrusions, select key findings, and cluster them 
2. The output, a cluster/group, is unique to each organization and not reversible 
3. The intrusions they see, the weights they apply, the findings, are all unique 
4. Clusters cannot have a 1-to-1 relationship, but they can have links (one or more Diamond Model vertices) to other clusters 



Visual analysis, link analysis, clustering, and other forms of analysis on top of structured datasets help reveal patterns 

But to tie intrusions (to intrusion sets, to campaigns, to groups ), we need to use structured analytical techniques (ACH, Rule of 2)

Use ACH mixed with Kill Chain, Diamond, and Rule of 2 to correlate clusters and validate findings 
- Follow ACH steps 
- Classify evidence based on Kill Chain/Diamond Model 
- Support in each clustering of evidence drives confidence in assessment 

<img class="img-fluid" width="1600px" src="img/activity-threads.webp" alt="img-verification">

<br><br>

Full ACH process often unnecessary, except when:
- lack of evidence makes correlation ambiguous 
- intrusion maps to multiple similarly defined clusters 
- disagreement between analysts exists 


**Categorize Evidence**

|KC|Diamond|Evidence<br>(Intrusion Data)|Intrusion<br>Set 1|Intrusion<br>Set 2|Intrusion<br>Set 3|
|-|-|-|-|-|-|
|Recon|Adversary<br>TTP<br>Infrastructure<br>Victim|-<br>Complex Search Queries<br>DuckDuckGo<br>Acme|-<br>+<br>-<br>+|-<br>-<br>+<br>+|-<br>-<br>+<br>+|
|AoO|Adversary<br>TTP<br>Infrastructure<br>Victim|LeetStar<br>LatMovement SMB<br>-<br>Research Networks|-<br>-<br>-<br>+|+<br>+<br>-<br>+|-<br>-<br>-<br>-|

<br>

**Enumerate Intrusion-Campaign Hypotheses**

|Evd|Campaign A|Campaign B|Other Campaign|
|-|-|-|-|
|E1||+||
|E2||+||
|E3|||+|

<br>


<img class="img-fluid" width="1600px" src="img/ach.jpg" alt="img-verification">

<br><br>

**External Intrusion Reports**

Complement Knowledge Gaps 

- address methods and behaviors you didn't previously know 
- think operationally (leverage Diamond Model)
- inspire threat-based hypotheses on how you hunt in your network 

Do Not Merge With Your Data

- Not all intelligence is created equal 
- Marketing sometimes wins out 
- Using the vendor info or name as your campaign name forces you to lose control of the narrative 

<br>

### Creating an Activity Group 

||Step|Description|
|-|-|-|
|1|Analytical Problem|Define what you're solving, your intel requirement| 
|2|Feature Selection|Event features and weighting of what's important to you |
|3|Creation|Analyze events/intrusions and compare against the model to cluster |
|4|Growth|Compare new events and classify them into the Activity Group |
|5|Analysis|Analyze Activity Group itself to address the Analytical Problem |
|6|Redefinition|Redefine model as your needs change and more to new cluster |

<br>

Analyze and cluster intrusions primarily to satisfy intel requirements, not track adversaries.

Sometimes it will relate to tracking a specific team or specific TTPs used by multiple teams or unique styles and approaches.  Diamond Model is component of meeting requirements.

Look for intrusions that meet both requirements
    - consistent TTPs/capabilities 
    - consistent victims/peer organizations 

Shortcut - apply Diamond Model and look for overlaps between two vertices in intrusions or campaigns 
         - identify unique characteristics(key indicators of behavioral TTPs)
         - map unique characteristics to Diamond Model 

Rule of 2
    - look for consistency in intrusions in some key way (key indicators or behavioral TTPs) to create an activity group 

Classify clusters as:  
    - Active - any linked intrusion within 6 months
    - Inactive - no observed linked intrusions for over 6 months 
    - Dormant - no observed linked intrusions for over 1 year 

Group Names aren't names, they are definitions 

If out of 100 intrusions, 5 use specific IP and same 5 also use Poison Ivy with specific mutex, cluster together and make up activity group name 


<img class="img-fluid" width="1600px" src="img/activity-attack-graph.png" alt="img-verification">

<br><br>


## Dissemination

- [Tactical Intel](#tactical-intel)
- [Operational Intel](#operational-intel)
- [Strategic Intel](#strategic-intel)
- [Attribution](#attribution)

<br>

Know your audience, different formats, intel needs 

### Tactical Intel 

Tactical threat intelligence presented to tactical level defenders 

- YARA rules (text, hex, regex)
- global rule, private rule (do not alert, but used for triggering other rules), 
- Tailor IOCs to eliminate as many false positives as possible prior to uncovering threat 
- Validate IOCs by testing them against digital images in the environment 

### Operational Intel 

- bridge between strategic and tactical 
- understand technical but look at bigger picture 
- should identify knowledge gaps and foster partner sharing to minimize gaps 
- should document and understand evolution of adversary campaigns and threat changes 
- help structure security teams to match size, training, and subject matter expertise to counter appropriate threats 

**National Level Government Information**

- Derived from criminal investigations, public/private partnerships, foreign intelligence
- Dissemination points:
    - US-CERT (DHS)
    - InfraGard (FBI)
    - NCSC (UK)
    - ACSC (Australia)
    - CSA (Singapore)

**Information Sharing and Analysis Centers** (ISACs) share threat information among civilian and government sector. Industries have them when deemed to be critical infrastructure.

**Information Sharing and Analysis Organizations** (ISAOs) are nongovernmental orgs to encourage provate community sharing and sharing between private and govt sectors. Created with EO 13691 in 2015.

- ISAOs expand the concept of ISACs and allow ISAOs to have their information treated as Protected Critical Infrastructure Information which protects the information from disclosure inclusing through FOIA or Sunshine laws, also it is exempt from regulatory and civil litigation 


**STIX: Structured Threat Information eXpression**

- describes threat information
- STIX1 is XML-based, STIX2 is JSON-based
- In response to critques of STIX 1.0, STIX 2.0 adn 2.1 was put under OASIS 
- OASIS - technical committee taking input and governing STIX 2.1, a more flexible and simplistic, leverages a graph-based model approach 
- Changes from STIX1.0: One standard (CybOX now in STIX), Indicator pattern language with KC phases 

**CybOX: Cyber Observable eXpression** 

- Describes observables (IOCs)
- Previously distinct, in STIX2, combined with STIX 

**TAXII: Trusted Automated eXchange of Indicator Information** 

- Transport mechanism for STIX 
- specification on how to send threat data, STIX is the standard for how to structure that threat data 
- TAXII Implementations
    - Source/Subscriber - Source determines what is available to Subscribers 
    - Peer to Peer - Multiple organizations can produce data and multiple can consume 
    - Hub and Spoke - Hub acts as clearinghouse for all information, subscribers can pull data and push data 

Related efforts include MAEC, CAPEC, and CybOX, each are MITRE-run languages for documenting observable indicators and information about malware, campaigns, and cyber activity 

**Sharing Best Practices** 

- Ensure authentication and logging 
- Include references and appendixes 
- Strip out all unneeded data 
- Use standards that make sense for your organization 
- Interfaces to share in common standards 

Organizational Metrics are subdivided into operational efficiency and workload metrics
Risk metrics are subdivided into threat-oriented metrics and others

**Traffic Light Protocol** 

|Condition|Use|Sharing|
|-|-|-|
|  RED  | Cannot be acted upon by additional parties, could lead to impacts on part's privacy, reputation, operations, if misused | no sharing with parties outside of specific exchange, meeting, or conversation in which it is originally disclosed |
| AMBER | requires support to be acted on, carries risks to privacy/reputation/operations if shared outside orgs involved | only with members of own org who need to know, and only as widely as necessary to act | 
| GREEN | useful for awareness of all participating orgs and peers within broader community/sector | only with peers and partner orgs within their sector/community, but not via publicly accessible channels | 
| WHITE | carries minimal or no forseeable risk of misuse IAW applicable rules and procedures for public release | without restriction, subject to copyright controls | 

<br>

**Problems with metrics**

- inconsistent terminology (attack)
- significance or weight of metrics unclear or ambiguous 
- measures themselves are incorrectly interpreted as quantification of some situation--such as network sweeps are not attacks 
- physics envy--tendency to try to quantify everything--assigning numerical values to non-numerical criteria, such as actor is capable of A, B, C so sophistication level is "1"
- measures themselves are subject to interpretation (nondeterministic), or assignment of criteria to numerical values is subjective 
- metrics do not map to nor suggest follow-up actions that will influence future measurements of the same metric 
- metrics are defined by management or non-SME analysis 

**Benefits of metrics**

- opportunity for clear, concise communication of message 
- can be interpreted by wide audience 
- represents large amount of data 
- visual data representation naturally more compelling 
- target metrics useful by management AND analysts 

#### Campaign Heatmap

- vertical columns are campaigns tracked, rows are intrusion attempts in a single month 
- far right column is total distinct campaigns in each month, bottom row is months which each campaign is active 
- provisional - intrusions that correlate to other intrusions, but no currently-named campaigns 
- pending attribution - intrusions that do not correlate to other intrusion attempts 
- X's mark potential areas for exploration 
    - unattributed intrusions (pending/provisional)
    - campaigns active for months but inactive in one month 
    - multiple campaigns operating in same months as one another with high level of consistency 

||Carbanak|FIN7|Squirrel|APT|OMG|XYZ|ET|||||
|-|-|-|-|-|-|-|-|-|-|-|-|
|JAN|||||||||X|||
|FEB||||||||||||
|MAR|||X|||||||||
|APR||||||||||||
|MAY||||||X||||||
|JUN||||||||||||
|JUL||||||||||||

<br>

#### Organizational Heat Map


MITRE ATT&CK navigator shows where you have mitigations and detections
Color represents how many, one or both, etc.

<img class="img-fluid" width="1600px" src="img/org-heatmap.png" alt="img-verification">

<br><br>

#### Incident One-Slider 
    
Kill chain showing progress and success of intrusion from adversary's perspective, with applicable mitigations and cumulative effect on kill chain 


<img class="img-fluid" width="1600px" src="img/org-heatmap.png" alt="img-verification">

<br><br>

#### Mitigation Scorecard 

Measures utility of passive and mitigating courses of action and maps specific incidents to the capabilities of network defenders, organized loosely by kill chain phase. Provides high-level visual of threat to courses of action mappings 

- Columns represent capabilities that map to passive courses of action 
- Dark shading indicates technology was applicable to the incident in a given row 
- First group of columns is "early warning" or "over the horizon"
- Second group of columns applies to all other phases of the kill chain after recon 
- Lightly shaded cells represent capabilites that would have mitigated the activity had it not been mitigated by something else 
- Dark cells with white dot represent capability that mitigated the intrusion 

Columns are organized "Inbound" for Delivery and Weaponization and "Outbound" for Exploit and C2 

Columns all the way on right are Proposed Changes and their applicability to each row 

#### Vector Mitigation Effectiveness Metric 
    
- Overall success of adversaries using a vector shown in bar graph
- Y Axis is Number of occurrences (bottom part Mitigated, Top part Delivered) 
- X Axis is Month of year 

#### Analytical Completeless Metric 
    

Completeless of intelligence collection at each phase of the Kill Chain and Diamond Model. Each phase has a diamond, each diamond is colored/numbered for those which intel is normally collected 


<img class="img-fluid" width="1600px" src="img/kcdm.jpg" alt="img-verification">

<br>

### Strategic Intel 

- Generally presented to executives with a focus on policy outcomes 
- Provides complete picture, direct attribution is more valued
- Used for global threat analysis and trends, national security and foreign policy, business operations, mergers, acquisitions
- Can drive indictments, name and shame, 
- Can make business case for security 
- Analyst must understand technical needs, requirements, considerations, and map them to organization mission 
- Reports should combine various sources of intel to present easily consumable narrative, longest lasting form of intel 
- Should be meticulously created, edited, proofed, cross-examined
- Clearly separate facts, observables, evidence from interpretations, conclusions 
- Consistently present findings then analysis, analysis then findings 
- Use measured language for findings and estimative language for analysis 
- Objective uncertainty is numerical (0-100%), subjective uncertainty is linguistic 

Remote > Unlikely > Even Chance > Probably, Likely > Almost Certainly 

Assessments are not facts 

Adversary = Assessment 
Infrastructure, Victim, Capability = Fact 

**Confidence Assessments**

Assessment = confidence + analysis + evidence + sources 

We assess <confidence> that <assessment> because of <evidence> from <sources>

||||
|-|-|-|
|High|supported by preponderance of evidence with no evidence against |- intrusions whose distinct indicators align to other intrusions in a campaign in 2 or more phases of Kill Chain as well as generalized TTP alignment|
|Moderate|significant evidence is missing, new evidence could invalidate|- correllation in more than one phase of Kill Chain<br>- correlation to a single phase with a generalized TTP alignment in other phases<br>- identification of 2 sides of Diamond Model in any single stage (3 vertices)  |
|Low|other likely hypotheses exist, little evidence available to support|corellation in only one phase of Kill Chain| 

<br>

## Attribution 

- Extremely costly intelligence requirement 
- Collection requires data and knowledge management across years and multiple analysts and data types
- Analysis and Production phase must include analyst with historical and cultural understanding of entity 
- False flag and counterintelligence operations always a possibility  

Be explicit about what type of attribution you're discussing 

|Type|Description|
|-|-|
|Activity Group|Common set of shared attributes across an intrusion, a behavioral profile|
|Actor|Individual who executed actions that led to observations made|
|Criminal|Individuals who may be prosecuted|
|State (Direct/Indirect)|Government or proxy in whose interests actions were executed|

- [Threat Actors](#threat-actors)
- [Activity Groups](#activity-groups)
- [Campaigns](#campaigns)

<br>

### Threat Actors

```mermaid
pie
"Dogs" : 386
"Cats" : 85.9
"Rats" : 15
```
 
<br>


```mermaid 
---
config:
  theme: dark
---

xychart-beta
    title "Normalized Turnout vs. Age in 2020 General Election"
    x-axis "Age" 0 --> 100
    y-axis "Normalized Turnout (votes)" 0.0 --> 1.2
    idaho ["27": 0.9092749214282918, "24": 0.8686197482325805, "22": 0.8555067943336734, "53": 1.008639020851231, "55": 1.0251912273436719, "62": 1.0381060754483744, "58": 1.0305752733979248, "46": 1.0011439819236523, "41": 0.9870509684556469, "20": 0.8133209076200066, "36": 0.9665545737222209, "68": 1.0508013870086383, "66": 1.0537199203985357, "90": 0.9763725026476002, "47": 1.0013220678208716, "78": 1.03286278774235, "50": 1.0138965351429212, "76": 1.0468857348113796, "44": 1.0012380795339397, "49": 1.0083164388858028, "79": 1.048689671097691, "51": 1.0191176139838118, "72": 1.0536802968479617, "54": 1.022701000150168, "74": 1.046876107844305, "25": 0.8782145542262446, "26": 0.8899636366413273, "70": 1.0583931990308655, "65": 1.04789490840275, "40": 0.9877993641530833, "64": 1.0479860085082275, "63": 1.041068294077176, "52": 1.0160958658850792, "60": 1.035478737129017, "31": 0.9461891333565774, "56": 1.032485364667809, "45": 1.006454404958462, "29": 0.9171805403079084, "57": 1.0301871073591575, "73": 1.0518852820922189, "19": 0.9054881432151789, "38": 0.9723742308537664, "28": 0.9031761080742241, "59": 1.0359263169129935, "30": 0.9237044230753676, "43": 0.9975971096511139, "21": 0.8008572102086952, "42": 0.9896532944255556, "33": 0.9498883959813378, "23": 0.8632941276936503, "88": 0.9769850191138735, "37": 0.9808189146336387, "85": 1.0175764811051908, "32": 0.9536075462355017, "80": 1.0355481007318825, "69": 1.059728818564594, "82": 1.0322215871643636, "67": 1.058535801922358, "34": 0.9551644401415161, "86": 1.0054610917337408, "39": 0.9761497725565552, "61": 1.0397263552324143, "48": 0.999762017196052, "83": 1.0231701212642603, "35": 0.965814255661391, "71": 1.0488573400195746, "75": 1.0479447338142016, "84": 1.015385687005983, "77": 1.0462387441212428, "81": 1.0245014788821216, "18": 0.9844635233731055, "87": 1.0003985950675365, "89": 0.9525690247872804, "96": 0.8379663189701876, "98": 0.8728815822606121, "99": 0.7262374764408293, "93": 0.90710651892645, "95": 0.8593896531249688, "91": 0.9545759267386614, "92": 0.9066345124430707, "94": 0.8705537486415036, "97": 0.8594526348412181]
    missouri ["23": 0.6765013048265256, "24": 0.6676231643981233, "26": 0.7011554262162525, "67": 1.1746350295783328, "79": 1.1301259419977714, "84": 1.0535668294217355, "37": 0.9432924692293694, "78": 1.1520002589606673, "74": 1.1828165680950542, "54": 1.0761737846549302, "50": 1.054663580299634, "64": 1.1430985099211208, "42": 0.9875115726342606, "34": 0.882337741714598, "56": 1.0851586333056797, "28": 0.7361992022306533, "72": 1.1858084948425036, "40": 0.9639144978491317, "63": 1.1391377557159976, "44": 1.0073725016037367, "46": 1.023362072926767, "81": 1.1122825614083163, "75": 1.173614341564935, "35": 0.9011660397218775, "36": 0.9111863449007048, "30": 0.7844891624507483, "29": 0.7659074994573438, "25": 0.6772560049527895, "82": 1.0890956470530297, "80": 1.121582110350515, "85": 1.029710342204029, "71": 1.1820833729005216, "53": 1.0717011960568488, "22": 0.6772149584434117, "19": 0.7786145369042214, "18": 0.9105734573487835, "20": 0.7352686524165445, "21": 0.707351885783516, "43": 0.9959738011505108, "39": 0.954700488504603, "45": 1.0162431162677776, "83": 1.0864649116633698, "55": 1.074165622860342, "49": 1.0472086723486878, "38": 0.9357682504079153, "62": 1.1318600140520234, "31": 0.8175182660181344, "59": 1.0953630630317361, "61": 1.1273529494207668, "48": 1.0317923045252286, "27": 0.714595665823209, "32": 0.8320421800433689, "58": 1.0973294727626257, "57": 1.091129793438202, "76": 1.166636796328174, "73": 1.187074557474995, "52": 1.0604096515096788, "65": 1.1585541022994397, "69": 1.178535960160136, "33": 0.8653822263151925, "68": 1.177820173954008, "60": 1.1060160721558658, "77": 1.1577429490861406, "66": 1.1671233970560944, "70": 1.186789416711719, "47": 1.0261575971540255, "41": 0.9735758635978784, "51": 1.0542801106283788, "86": 1.0209161146812702, "87": 0.9992923815156438, "89": 0.9519697511829167, "88": 0.9654737795375745, "92": 0.889185315387453, "91": 0.9332815087565561, "94": 0.8055549679598043, "93": 0.8553741031662269, "90": 0.9558728011855672, "95": 0.750490799475098, "96": 0.6612290787967618, "98": 0.5797433961960198, "97": 0.6203400969593874, "101": 0.44792855849686936, "99": 0.5307631743759437, "100": 0.39588012606648804, "102": 0.35200268153935965]
    north-carolina ["86": 0.7678816112734356, "43": 1.0391217376385418, "55": 1.1400452719695724, "45": 1.058838270280871, "76": 1.0828898589529934, "73": 1.1387232206515538, "49": 1.1079391626312591, "46": 1.0713369795005954, "77": 1.0641304981943749, "59": 1.1556471036607052, "34": 0.895848162090799, "35": 0.919132317926431, "61": 1.163857953700746, "70": 1.162029898233815, "62": 1.1657444455823898, "28": 0.761109986661207, "31": 0.8308277714630738, "32": 0.8573708633227459, "41": 1.0123558682189115, "69": 1.1701130049892428, "19": 0.8447856263001204, "54": 1.1519235380275843, "53": 1.1377571015945556, "21": 0.8376293550142963, "25": 0.7726397088392923, "38": 0.9747375515114578, "66": 1.1748241397371642, "91": 0.5145103505615777, "74": 1.1214689635531077, "30": 0.8098768321099215, "36": 0.9368390241938339, "56": 1.1477774557599716, "33": 0.8799170303115655, "20": 0.8927329042201534, "68": 1.1732805625682459, "52": 1.1391246827568113, "48": 1.09922285207814, "27": 0.7481689610233934, "42": 1.029094317655834, "23": 0.7976924081872743, "64": 1.1734648981115705, "72": 1.1461590697231026, "57": 1.1553179785646266, "24": 0.7720056054177331, "84": 0.8637744502484326, "82": 0.9191665431359884, "29": 0.7920388658703621, "100": 0.13633932263213866, "40": 0.9988630515851257, "102": 0.11256336829739015, "22": 0.8257686909423039, "58": 1.1584303961322753, "71": 1.1605247570919417, "47": 1.0923772680885482, "85": 0.8136149012649606, "51": 1.120645387568102, "50": 1.1194970874273151, "65": 1.1703635276363886, "78": 1.0468989789718612, "63": 1.1787140237168499, "79": 1.0173770798933852, "83": 0.888921764792627, "67": 1.1786135463006908, "60": 1.1623711288805554, "44": 1.0490248500875208, "37": 0.9603541188961136, "98": 0.18780784924577018, "81": 0.9533881451911659, "26": 0.7884682735401397, "87": 0.7233468993568651, "95": 0.31479269283375577, "94": 0.35950240894027424, "99": 0.15302522093341236, "75": 1.1007728799976664, "39": 0.981948285918485, "92": 0.4662285656616883, "89": 0.6126799434800668, "96": 0.2728610158041565, "90": 0.5790597903482616, "93": 0.4125777784653279, "88": 0.6675455591103618, "80": 0.9889700562525663, "97": 0.22902598224114035, "101": 0.12790481809656798, "103": 0.0828615348775542, "106": 0.0698356141220931, "107": 0.02242361297188155, "105": 0.05315514848302198, "104": 0.07414119088712051]
    ohio["18": 1.0259215651407412, "19": 0.9080529003756633, "20": 0.8300724954800021, "21": 0.7851351041312538, "22": 0.7087798838792608, "23": 0.6773473482480838, "24": 0.6597524160443842, "25": 0.6715744760746558, "26": 0.6703357549753352, "27": 0.6919933510929679, "28": 0.7293693466013045, "29": 0.7413639960745787, "30": 0.7778978777850075, "31": 0.7992607735063084, "32": 0.8256111812442976, "33": 0.8531466906836123, "34": 0.876959514523238, "35": 0.8894968097893639, "36": 0.9069237171570266, "37": 0.9275505673783865, "38": 0.9419564159837762, "39": 0.9435221728639936, "40": 0.9595035090558545, "41": 0.9671251272620875, "42": 0.9788234217402944, "43": 0.9790532777203825, "44": 0.9956535375074619, "45": 0.9995903048487884, "46": 1.0104103017908126, "47": 1.012233612010957, "48": 1.0302884460769473, "49": 1.0404666147007873, "50": 1.0538250314462483, "51": 1.0564590730620016, "52": 1.0662932742408846, "53": 1.0747427292192664, "54": 1.0747226848024418, "55": 1.0871600740607683, "56": 1.0909435716698461, "57": 1.097540005765534, "58": 1.108279167606085, "59": 1.119750900169591, "60": 1.1269389944501953, "61": 1.1303644415382161, "62": 1.1341486877807923, "63": 1.1442562859361773, "64": 1.151330688086238, "65": 1.1576603120373472, "66": 1.1673147788017821, "67": 1.1681164772501944, "68": 1.17016753878915, "69": 1.175491481183319, "70": 1.1789580534560273, "71": 1.1797557088054547, "72": 1.1820134082297264, "73": 1.1781049099364957, "74": 1.1718835764839812, "75": 1.1706749087107837, "76": 1.1655399381675295, "77": 1.1582560015885897, "78": 1.154458719471363, "79": 1.143996211801759, "80": 1.135384466946053, "81": 1.1312984724479171, "82": 1.1157153557701356, "83": 1.1035939526836016, "84": 1.093094390197864, "85": 1.0698110280407556, "86": 1.0531290264014357, "87": 1.0420180169371218, "88": 1.0248271426294169, "89": 0.9773298758502594, "90": 0.9617846423588001, "91": 0.9481505545434142, "92": 0.8988753856117789, "93": 0.8889344458248604, "94": 0.8448594207111164, "95": 0.8004690886517238, "96": 0.7843545875190799, "97": 0.7665311129395841, "98": 0.7576900468218822, "99": 0.6882913568004169, "101": 0.516692465449354, "102": 0.6218176695800908, "110": 0.12869071354099978, "113": 0.6668193197234045, "119": 0.3947895287412988, "100": 0.681995535953335, "106": 0.40425365770167754, "103": 0.5645947045078136, "104": 0.5346207813968011, "120": 0.551448589342101, "105": 0.4402944687456721, "107": 0.14817327372966668, "108": 0.31252501057218485, "111": 0.0, "112": 0.43529643079676544, "114": 0.4700243014178735, "115": 0.41088813121489726, "116": 0.8412606781952328, "117": 0.44122877211700895, "134": 0.0, "109": 0.6819330497673848, "118": 0.0]
    oklahoma["18": 0.8407872240832298, "19": 0.6877074650296461, "20": 0.5781970743287718, "21": 0.562542963191815, "22": 0.5134375661512636, "23": 0.5078271781176064, "24": 0.49229803820902357, "25": 0.5028814374624841, "26": 0.5050576718925652, "27": 0.5518961398584055, "28": 0.5630580950139852, "29": 0.5938329760622635, "30": 0.6213358894533431, "31": 0.6391468389546762, "32": 0.6553660677899882, "33": 0.6869048208023757, "34": 0.6996178995422216, "35": 0.7205644131113175, "36": 0.7181690971700352, "37": 0.7355062851623765, "38": 0.7561700272617597, "39": 0.7523014245973727, "40": 0.7560318724750357, "41": 0.7635916825891398, "42": 0.7786086737680645, "43": 0.7699318803150289, "44": 0.7780688409454788, "45": 0.7987643020697643, "46": 0.808086443790102, "47": 0.8096981078402088, "48": 0.8181613267941872, "49": 0.8275310390368363, "50": 0.8282409188651783, "51": 0.8450598356878812, "52": 0.8454532901507047, "53": 0.8364426012520251, "54": 0.8601413904307462, "55": 0.8607860002007832, "56": 0.8685495292138844, "57": 0.8794085202729466, "58": 0.8853609382083379, "59": 0.8855691855112404, "60": 0.8886845712761459, "61": 0.8993729772139917, "62": 0.9013832220311659, "63": 0.9052883744047893, "64": 0.9145501126196658, "65": 0.9213206292368299, "66": 0.9115572387823404, "67": 0.9223565529176245, "68": 0.9257567527093961, "69": 0.9163211768273605, "70": 0.9218641065947693, "71": 0.9141769466965982, "72": 0.9098793393381701, "73": 0.9039338323514656, "74": 0.9040168433646654, "75": 0.8996235841302388, "76": 0.8957387928773102, "77": 0.878856444931623, "78": 0.8605024302465435, "79": 0.8619121804072329, "80": 0.846572581409457, "81": 0.8253150223890696, "82": 0.8013693655990979, "83": 0.7969471945964985, "84": 0.7639535161296208, "85": 0.7208272671218977, "86": 0.7340667126152998, "87": 0.6872194277074005, "88": 0.660911249374857, "89": 0.6530987283559218, "90": 0.600352664590093, "91": 0.5739521362895633, "92": 0.5667043719570302, "93": 0.5178082164520109, "94": 0.4911447723506776, "95": 0.4762541993004224, "96": 0.4759591467091822, "97": 0.4410543506335487, "99": 0.4263753405674218, "119": 0.974538693703465, "130": 1.261817642428983, "131": 1.3670013782240598, "101": 0.5266866440558416, "98": 0.4443249911373503, "100": 0.47976748070180325, "103": 0.7157753883791339, "983": 1.0341188884980654, "104": 0.4543293297095024, "120": 0.9425870142856199, "117": 0.8303667936202248, "1073": 1.1072515169661425, "106": 0.7133255229107995, "107": 0.859829254448665, "102": 0.5519558333310147, "105": 0.30649902922059297, "336": 1.137990962808481, "112": 0.8466800758347854, "108": 0.8460913417214905, "116": 0.8998354621974984, "113": 0.9799415961807784, "135": 1.2547008547008547, "1824": 1.0943074298123157, "966": 1.086391311502386, "115": 1.124657887952916, "1081": 1.0674325674325675, "110": 1.1071147165974753, "140": 1.0368043314265327, "1085": 1.085428809325562, "821": 1.1566339066339066, "973": 1.0285994672648255, "1056": 1.0285994672648255, "139": 1.1046666980083324, "109": 0.9606150433476303, "118": 1.0897327047523393, "969": 1.06494140625, "1034": 1.06494140625, "1049": 1.134710377354203, "114": 1.0565167195367433, "134": 1.0625131995776134, "122": 1.2044793484584062, "1038": 1.2044793484584062, "1041": 1.2044793484584062, "1066": 1.2044793484584062, "899": 1.0493907392363933, "956": 1.016851980542043, "133": 1.0490181818181818, "160": 1.0490181818181818, "1042": 1.0490181818181818, "1047": 1.071706683971447]
    pennsylvania ["53": 1.054022955493885, "60": 1.0858996957879141, "47": 1.0140847681781755, "54": 1.0548740275429997, "52": 1.043972472612399, "62": 1.1014599226376478, "80": 1.067649906226079, "46": 1.0041689934576075, "66": 1.117648317371187, "70": 1.1249368890444442, "79": 1.0825504165830426, "73": 1.1220432059168286, "43": 0.9849027724236823, "64": 1.1113810824853156, "50": 1.036524658649039, "51": 1.0373052986910674, "75": 1.1061774872693184, "68": 1.1234280008457915, "69": 1.1239713927990098, "38": 0.9543047289605755, "58": 1.0752368484743973, "61": 1.0936165796781483, "65": 1.1127557177685599, "78": 1.0896891772917945, "67": 1.120025795730185, "49": 1.0319042975505828, "63": 1.1044158545918763, "42": 0.9799307497183937, "76": 1.0983128283682149, "87": 0.9608417328928585, "41": 0.9774093495839106, "39": 0.9549976270994901, "37": 0.9438339700328925, "48": 1.0243847002160242, "57": 1.0699844255300495, "44": 0.9930825834165715, "45": 1.003264613850014, "71": 1.123515013512937, "40": 0.9674885955960222, "36": 0.9324402909914934, "77": 1.0970862200510003, "56": 1.0670387179710414, "55": 1.0563511423497094, "72": 1.1184922310727712, "90": 0.8738127870257913, "83": 1.0332047767815993, "74": 1.1082986671286212, "85": 0.9969845124580886, "82": 1.0502036664198693, "59": 1.0813540289700228, "81": 1.0630681915389777, "35": 0.9172875464734449, "84": 1.0206933433272065, "86": 0.9806655158464798, "88": 0.9356993841250729, "34": 0.895097227862919, "33": 0.8914468977025641, "32": 0.8648844365428081, "31": 0.8477917964754784, "30": 0.8208090103196369, "29": 0.8182386029572115, "28": 0.8045106647689891, "27": 0.7812092867973992, "26": 0.7570554087173132, "25": 0.7618264791110967, "24": 0.7749892368175677, "23": 0.7789513287164848, "22": 0.8135158320460396, "21": 0.920769855317469, "20": 0.9363530780583845, "19": 0.9980834053574781, "18": 1.0800307569507057, "89": 0.9013718040684543, "91": 0.8444014626129455, "92": 0.8175153443913237, "93": 0.7780733701702451, "94": 0.7286610843466876, "96": 0.6641426294968082, "95": 0.7418852945521124, "97": 0.6463719559288131, "98": 0.606249205658335, "99": 0.5398550147273379, "100": 0.44487607646528066, "101": 0.3564786021592687, "102": 0.32866635392124044, "103": 0.3279395693688806]


```

<br>

```mermaid
 %%{ init: { 'flowchart': { 'curve': 'stepBefore' } } }%%
graph LR;
A<-->B;
A<-->C;
B<-->D;
C<-->D;

```

<br>

```mermaid
 %%{ init: { 'flowchart': { 'curve': 'stepBefore' } } }%%

---
title: Diamond Model
---
erDiagram

    ADVERSARY {
        string name
    }
    
    INFRASTRUCTURE {
        string registrationNumber PK
        string make
        string model
        string[] parts
    }
    
    CAPABILITIES {
        string driversLicense PK "The license #"
        string(99) firstName "Only 99 characters are allowed"
        string lastName
        string phone UK
        int age
    }
    
    VICTIM {
        string carRegistrationNumber PK, FK
        string driverLicence PK, FK
    }
    
    ADVERSARY ||--|{ INFRASTRUCTURE : driven 
    ADVERSARY ||--|{ CAPABILITIES : uses 
    CAPABILITIES ||--|{ VICTIM : is 
    INFRASTRUCTURE ||--|{ VICTIM : allows 
    CAPABILITIES ||--|{ INFRASTRUCTURE : as 


```




### Activity Groups

For every intrusion event, there's an adversary using capability over infrastructure against victim 

Activity groups track relationships between these 4 vertices

associated by similarities in features/processes and weighted by confidence.  Each metadata edge gives different weights

2 AG purposes: framework to answer analytic questions requiring a breadth of activity knowledge
               development of mitigation strategies with intended effect broader than activity threads 

AGs formed to identify common adversary behind events and threads usually using similarities in infrastructure and capabilities. AGs are not static, they adjust with the adversary group  


### Campaigns 





**4 Approaches to "True" Attribution** 

Two different categories of attribution can help reach moderate to high confidence assessments 

|Type|Description|
|-|-|
|Adversary Admission|Adversary admits/takes credit for actions|
|Leaks|Substantial information released|
|Direct Access|Interacting with and collecting evidence from adversary (SIGINT, HUMINT)|
|Intrusion Analysis|Using compromised environment and patterning out activity|

<br>

**Achieving Value of Attribution Without Attribution**

1. Define an analytical model of what that entity represents to you
- Motivations (intent)
- Opportunities
- Capabilities

2. Compare intrusions to the model
- your cluster operates in the interests of the entity 

Leveraging Attribution
- LEAs prosecute cyber criminals and rogue members of state 
- Nation states, signalling to aggressors 
- Organizations use for business decisions

**Spectrum of State Responsibility**

|Type|Description|
|-|-|
|State-prohibited|will help stop the 3rd party attack|
|State-prohibited-but-inadequate|cooperative but unable to stop 3rd party attack|
|State-ignored|aware but unwilling to take official action|
|State-encouraged|encourages 3rd party attack via policy|
|State-shaped|provides support to 3rd party attack|
|State-coordinated|suggests operational details for 3rd party attack|
|State-ordered|directs 3rd party to conduct attack on its behalf|
|State-rogue-conducted|out-of-control entities of national government conduct attack|
|State-executed|conducts attack under its direct control|
|State-integrated|integrates 3rd party proxies and goverment entities for attacks|

<br>

Complex web of human and geopolitical interactions 

State and criminal activity can merge 

Geopolitical landscape

**Deriving Intent**

We see perceived intent, not actual intent or motivation 

We attempt to infer intent from observed impact 

**State Attribution **

- Understand state deeply
- Define Models for each entity (large collections, long durations, supplement with leaks/admission/direct intel)
- Follow ACH steps 
- Classify evidence based on threat definition (for 3rd parties, extract evidence not conclusions)
    - intent
    - opportunity (technical, political, logistical)
    - capability
- confidence assessment informed by support in each group of evidence 

ACH emphasizes we try to REJECT hypoteses, not support them.

**ACH Matrix Template for State Attribution** 

|Category|Evidence|Country 1|Country 2|Other Entity|
|-|-|-|-|-|
|Intent|E1<br>E2<br>E3|+<br><br>-|<br>+<br>+|+<br><br>-|
|Opportunity|E4<br>E5<br>E6|+<br> <br>-|+<br> <br>-|+<br> <br>-|
|Capability|E7<br>E8<br>E9|+<br>++<br> |+<br> <br>+| <br> <br>+|

<br>

Opportunity weighs same so:
- seek additional evidence to fill intel gap 
- continue if remaining evidence is strong enough 
- stop and consider evidence is inconclusive 

False flags are operations designed to redirect blame of attack to a third party 
- GRU created Olympic Destroyer malware to look like Lazarus to increase tension between North/South Korea 

**15 Axioms for Intelligence Analysts**

1. Believe in your own professional judgements
2. Be aggressive and do not fear being wrong 
3. It is better to be mistaken than to be wrong 
4. Avoid mirror imaging at all costs
5. Intelligence is of no value if it is not disseminated
6. Coordination is necessary, bu do not settle for consensus
7. When everyone agrees on an issue, something is probably wrong 
8. Consumers don't care how much you know, just tell them what's important 
9. Form is never more important than substance 
10. Aggressively pursue collection of information you need 
11. Do not take the editing process too seriously
12. Know your community counterparts and talk frequently 
13. Never let your career take precedence over your job
14. Being an intelligence analyst is not a popularity contest
15. Do not take your job, or yourself, too seriously 

<br>
<br>

- [Case Studies](#case-studies)
    - [Moonlight Maze](#moonlight-maze)
    - [Operation Bodyguard](#operation-bodyguard)
    - [Operation Aurora](#operation-aurora)
    - [Promethium and Neodymium](#promethium-and-neodymium)
    - [Ukraine December 2016](ukraine-december-2016)
    - [Carbanak](#carbanak)
    - [Hexane](#hexane)
    - [Poison Hurricane](#poison-hurricane)
    - [Epic Turla](#epic-turla)
    - [GlassRAT](#glassrat)
    - [CVE-2014-1761](#cve-2014-1761)
    - [Human Operated Ransomware](#human-operated-ransomware)
    - [NYSE Computer Glitch](#nyse-computer-glitch)
    - [Turkey Pipeline Explosion](#turkey-pipeline-explosion)
    - [Panama Papers](#panama-papers)
    - [APT10 and APT31](#apt10-and-apt31)
    - [Axiom](#axiom)
    - [HackingTeam](#hacking-team)
    - [Metrics from CTI Summit](#metrics-from-cti-summit)
    - [APT10 and CloudHopper](#apt10-and-cloudhopper)
    - [Soviet Disinformation Operations](#soviet-disinformation-operations)
    - [Lazarus Group](#lazarus-group)
- [Tools]
    - [Maltego](#maltego)
    - [Wireshark](#wireshark)
    - [MISP](#misp)
    - [YARA](#yara)


<br>


## Case Studies 

### Moonlight Maze 

Initial intrusions were thought to be isolated events 
FTP logs from C2 server showed activity occurred for years 
- Victim-centric analysis tied targeting to the Military Critical Technologies List 
- Artifacts including code and activity patterns pointed to Russia 
- Russian word for "child process", quiet for Orthodox Christmas holiday 
- 2016 server named "HR Test" analyzed by Kaspersky Labs/King's College and found LOKI2 
- Kaspersky Labs identified Penquin Turla toolkit based off LOKI2 in 2014 
- C2 Protocol overlap between two, targets of Penquin Turla aligned with strategic goals of Moonlight Maze 
- "Publicity *degrades* visibility in ongoing investigations. Publicity *increases* visibility in historic investigations."

### Operation Bodyguard 

- Allies used passive and active measures for counterintelligence 
- Disinformation for D-Day invasions, fake armaments positioned along England 

### Operation Aurora

- Publicly disclosed by Google in 2010 
- Victims were Chinese human rights activists and dissidents 
- Revealed to be numerous campaigns against Northrop Grumman, Morgan Stanley, Dow Chemical, more 
- 2011 other groups discovered to be linked: Elderwood Project, Operation Shady RAT, Operation Night Dragon, Breach of RSA SecurID 
- Analysis across campaigns & groups attributed China's PLA Unit 61398 
- Zero days, numerous malware families (PoisonIvy, Hydraq), phishing emails, watering hole-style exploitation 
- `Aurora` was a common file folder referenced by malware, compromised VPS servers used for C2 
- The capability and infrastructure overlap helped unite singular view on overall campaign coordinated against 30 U.S. companies
- Forensics revealed consistent source IP of Chinese government-owned infrastructure 
- OPSEC failures by Chinese operators included call signs (social media & gaming), intercepted communications showing Chinese government officials directed the campaign 

Lessons Learned:  Pattern analysis across intrusions 


In many ways Operation Aurora (2010) kicked off an industry 
- victims were gmail accounts of Chinese human rights activists and dissidents 
- 2011 other groups linked to Operation Aurora, targeting oil, gas, petrochemical companies, banks, defense contractors 
- Infrastructure and capability overlap between intrusions observed, helping bridge them into campaigns 
- shared data sets helped to unite a singular view on overall campaigns coordinated 
- OPSEC failures identified, intercepted/leaked communications 
- attribution (always an assessment) came from combination of technical clues, opsec failures, leaked intercepted cables, consistent patterns in infrastructure choices
- many campaigns are cross-sector, analyzing one industry isn't enough to understand full scope of adversary efforts 

### Promethium and Neodymium

Two unique activity groups that have similar victim location, timing, and use the same zero-day exploit prior to public disclosure 

Their tradecraft is different but there are strong ties.  Could be same people behind them, or have same boss, but better to separate activity and link them than say it was one single team 

Promethium primarily used Truvasys malware as first stage implant and infected targets with links via messengers that would connect the victim to malicious documents. Zero-day was Adobe Flash.

Neodymium primarily used Wingbird malware delivered through spear phishing emails tailored to victims. Emails contained RTF documents that exploited same zero-day.

The two vertices (Victim & Capabilities) were enough to declare the cluster of intrusions an activity group for each one.  The fact that there is overlap between activity groups is fine.

### Ukraine December 2016 

ESET discovered malware samples, shared details to Dragos and the two arrived at different analysis.

### Carbanak 

- Carberp was a cyber crime toolkit (appeared Fall 2010) sold on Russian fraud forums, had VNC, RDP, steals certificates, sniffers, loggers, MBR rootkit 
- Sold for 50K
- Malware based on Carberp used by APT against over 100 banks in various countries
- Phishing emails with malicous attachment, creds stolen off network, led to theft, ATM compromises, fake accounts 
- Began also targeting hotels, restaurants, retail stores
- Used Google for C2 
- Criminals created bank accounts, transferred money, remote ATM theft, largest cyber heist in history 

APT is a style, not a category. Commodity malware was repurposed by a more capable actor. Malware is the tool, the threat is human.

### Hexane 

- Identified 2018, targeting oil and gas in Middle East
- Targeted telecommunications networks looking to target offshore oil rigs 
- Leveraged DanBot malware which is not unique to them
- compile times used as clustering option and pivot point, also unique project file path and fake names
- When adversary chooses to manipulate, their choices are interesting 
- code reuse, pivot on typos, passwords (keyboard walk), DNS requests, mutex to find overlap  

### Poison Hurricane 

- Malware forced DNS lookups through Hurricane Electric free DNS hosting servers 
- Allowed adversaries to create new host entries for existing domains 
- Allowed requests to microsoft.com and adobe.com but pointed to malicious IPs 
- PlugX or Sogu samples

### Epic Turla 

- Snake/Uroburos attributed to Russian government, in operation since Moonlight Maze 
- Sophisticated development, government/military focus, responsible for Agent.BTZ
- C2 involved intercepting unencrypted satellite-based internet connections broadcast to regions such as Middle East 

### GlassRAT

- Uncovered November 2015 by RSA, tied to PlugX campaign, undetected Trojan targeting Chinese nationals for 3 years 
- Uses compromised digital certificate from trusted CA, shared C2 with other campaigns in possible OPSEC failure 
- RSA identified overlap by tracking DNS and using visualization of C2/DNS malware samples, victims 

Historical analysis is key.  Distinct groups may be same/cooperating teams with different mission sets or assignments.

### CVE-2014-1761

- RCE in Word, MS published article with hashes, useragent, IP address, domain, TLS fingerprint 
- Used TLS cert to find 6 IP addresses this cert was seen on 

### Human Operated Ransomware

- Class of ransomware where adversary penetrates network and tailors operation to victim
- Did more damage than auto-spreading malware like ETERNALBLUE and WannaCry 
- more initial access vulnerabilities and tradecraft proliferation 
- OST like Powershell, Cobalt Strike
- willingness of victims to pay ransom 
- Wadhrama Attack Chain by PARINACOTA 
- Doppelpaymer Ransomware good example where nuance between TTPs and tradecraft matter 
- Might be RDP access like Wadhrama, but use of Dridex signifies different Activity Group 
- Trickbot (Emotet) as loader, Cobalt Strike or PowerShell to position the Ryuk ransomware 
- Bloodhound to compromise and leverage DCs 

### NYSE Computer Glitch 

7/8/15 - NYSE halt, glitch blamed publicly 
- All United Airlines flights were grounded, unknown computer problem was blamed 
- Wall Street Journal website went down 
- Illusory correlation: events viewed as cyber attack
- Hindsight bias: some claimed they knew all along it was WAN optimizer, not cyber attack 

NYSE and UA shared vendor caused their issues, WSJ was DOS'ed by those rushing to see about NYSE and UA 

### Turkey Pipeline Explosion 

- 2008 Baku-Tbilsi-Ceyhan (BTC) explosion, Turkish government claimed it was physical terrorist attack by extremists 
- 2014 Bloomberg reported it was Russian cyber attack based on interviews with incident responders
- They discovered malware in control center of gas pipeline, assumed correlation meant causation 
- Nothing was published to indicate malware had the capabilities to cause physical explosion

### Panama Papers 

- Whistleblower John Doe leaked files from law firm Mossack Fonseca, a Panama-based company used by wealthy people 
- German journalist received documents, submitted to International Consortium of Investigative Journalists (ICIJ) 
- 2.6 TB of leaked data took 1 year to analyze, used OST like Solr and Tika to index data, Nuix for OCR 
- Used Neo4J and Linkurious for link analysis 

Journalists used requirements to drive their analysis, intelligence-driven hypotheses 

### APT10 and APT31

- Norwegian managed service provider and U.S. law firm breached after indictment of APT10 actors  
- APT10 reported to target managed service providers and use Trochilus malware 
- Recorded Future and Rapid7 found overlaps and attributed breaches to APT10
- Identified new variants, TTPs, and Chinese shell company 
- PWC and MSFT noted methods, style of registered domains, and more were actually APT31
- Way malware was deployed was unique. These details would not have been known by RecFuture/Rapid7
- APT10 is not CloudHopper, APT31 is not ZIRCONIUM 

Group names are definitions not often publicly known 

### Axiom 

- PlugX originally Chinese-based malware, propagated to other groups
- multistage RAT with C2, upload/download, keylog. Linked to Poison Ivy with campaign overlap 
- Campaigns largely focused on government organizations and espionage
- Hikit (McRat) is malware with rootkit, RAT functions, kernel driver to monitor traffic, waits for connection 
- Linked to Bit9 compromise that stole private certs and signed malicious software 
- Started with public web server hit with SQL attack
- Axiom (Novetta, iSight, Bit9, Cisco) attributed to Chinese Intel community which leveraged variety of tools over course of campaign: PlugX, Hikit, GhostRat, Poison Ivy, Hydraq, DeputyDog, Derusbi 
- Similar naming conventions for C2 domains, used compromised infrastructure and victim-specific C2
- Used more complex malware for harder targets, victims had geopolitical context 
- Adversary seemed to have different teams that handed off access to each other (dev, access, operations)

Analysis took multiple companies and data sets to tie different campaigns to the same group 

### HackingTeam 

- Mercenary group that does surveillance and exploitation services for governments and law enforcement 
- 400 GB data stolen and posted online which contained malware, exploits and target lists 

Be aware of these mercenary groups and their tools/capabilities.  Anyone can use them after a dump.

### Metrics From CTI Summit 

- chart from ThreatConnect presented at SANS CTI Summit 
- Mapped to revenue saved, reduced mean time to discover adversaries, and reduced mean cost of a breach 
- Good metrics justify value of CTI team 

### APT10 and CloudHopper 

- 2009 FireEye starts tracking, first used Poison Ivy then PlugX, clear indications developers were not same as operators 
- APT10 has wide variety of capabilities, victims, and TTPs, almost too many to realistically track 
- Work contributed to FBI indictment of two individuals in 2018
- Indictments and government documents can be useful for uncovering TTPs and victimology and validating your own 
- 2017 tracked CloudHopper which highly correlated to APT10, but stated it's almost certainly APT10 since they couldn't know  what FireEye's definitions were 
- Human choices like work hours, work days, where domains registered, when malware compiled are good data points 

Finalized intelligence products require a significant amount of collection across time 
All CTI starts with an intrusion 
Intrusion analysis requires relentless pursuit of malware, domains, external datasets, and creativity

### Soviet Disinformation Operations 

- dezinformatsiya (active measures) in 1960s KGB and Statsi's HVA conducted over 10,000 operations 
- Faked source of information, then used state-run media to push out stories into multiple languages
- operatives all over world worked them into newspapers, media 

### Lazarus Group 

- 2013 McAfee reported on Dark Seoul malware leveraged against South Korea organizations as Operation Troy
- MBR-wiping, RAT deployed through spear phishing emails to banks, media, and governments 
- 2014 Guardians of Peace leaked Sony data
- Fake groups were made to look responsible (NewRomanic Cyber Army Team, Whois HackingTeam)
- WannaCry connections
    - Infrastructure overlap with Lazarus Group 
    - Code and encryption key overlap 
    - Wiper in WannCry linked to Operation Blockbuster
    - Financial-based and espionage motives 
- Distinctive patterns were observed: commonality in malware, style, naming conventions 
- Helped tie together earlier campaign Operation Flame 

Over time, tradecraft and victimology changed and overlapped so much it's useless to defenders 

<br>
<br>
<br>
<br>


Attribution is a very complex issue. Organizations and vendors use different names for the same threat actors derived by different datasets and different analysis techniques. Mappings often rely on findings from unconventional data sources or single incident analysis. 


- Threat intelligence focused on defender outcomes to produce realistic counters/mitigations 
- Behavioral indicators and choices, human fingerprints, geopolitical context, strategic interests, 
- Splunk, Rekall, FireEye, Yara 
- Identifying less defended subsidaries or supply chain weak links that result in softer targets to acquire intended data
- Content and construct validation ( different tools/techniques converging on the same analysis) are used to validate observations and conclusions
- Weighted scoring is assigned to observations and artifacts obtained from high-integrity sources such centralized logging and collection from system memory
- Experience using systems analysis and target-centric analysis to build and maintain threat models
- Understanding intent helps develop analytic and detective strategies which create more effective mitigation 
- Matching threat models to adversary tradecraft, observing all behaviors, even if they initially appear to conflict with one another
- How conditions and actions impact behaviors, context and meaning in which behaviors, results, abstract out key observations 
- Correlate intrusions, build personas, understand motivations
- Using abstraction to prevent manipulation of observations in order to shape our conclusions
- Zooming out with abstraction helps expose the weak links in our processes and reveals key patterns only visible from higher ground
- Ensure the authenticity of their observations and artifacts by using memory collection and centralized logging
- Validating their observations and conclusions
- Using multiple different tools and methods are used to build a more complete picture of actor operations
- Clustering with Kill Chain phase, Diamond Model vertices, and ATT&CK 
- Identifying "human fingerprint" in code and choice in operations, indicators, patterns, tradecraft 
- Structuring detailed playbooks, analytics, and pre-made queries 
- Documenting knowledge necessary for defending organizations 
- Recommending necessary technological, tradecraft, or procedural changes
- Analysis reports on incidents/campaigns, trending, and attack forecasting
- Technical reports and analysis walk-throughs on malware samples
- Vocabulary for Event Recording and Incident Sharing (VERIS) captures metrics on events and incidents 
- Analyzed information about the hostile intent, opportunity, and capability of an adversary that satisfies a requirement 
- Clustering and linking of intrusions on key indicators to form intrustion sets. 
- Clustering sets of events/activities associated by similarities in features and weighted by confidence scoring 
- Clustring of intrusions (by vertices on diamond model) with weighting on Adversary, Infrastructure, Tradecraft (Capability/TTP), and/or Victim of intrusions, cluster you're tracking that's specific to an analytical/intel requirement 
- Combining methods, capabilities, and resources leveraged into TTPs and Tradecraft  
- Prime form of tracking threats by prioritizing Infrastructure, Victim, Tradecraft vertices, to weight the "how" 
- Defending against activity groups with behavior-based analysis for more precise analytical judgements 
- Use Kill Chain model to categorize indicators, identify patterns and relationships.
- Use the Diamond Model for structured pivots to discover artifacts and events that are related to known, vetted indicators
- Using Kill Chain analysis for categorizing indicators for context and associated phase


Knowledge of adversary behaviors improves the speed of detection and response, the quality of tools and techniques delivered to the analyst, and the ability of management to make time-sensitive decisions.


We categorize threat intelligence sources into the following groups:


1. Malware - Profiling the static and behavorial characteristics of malware and malicious infrastructure to assess and predict its capabilities. Determine the intended use of the malware/infrastructure and fully map capabilities, Extract as much information as possible for the purpose of intelligence, Use sample to determine tools and techniques used by the adversary, 

    - Perform automated analysis to get a quick look at APIs called, dropped files, connections, SSL certs, mutexes
    - Perform static analysis using AV/Hash checks, strings, metadata, imports
    - Perform dynamic analysis using tools to inspect File/Registry/Process/Network activity with Process Hacker, Process Monitor, FakeNet, Fiddler, Network Monitor 
    - Perform static code analysis to discover how the program operates without running its code
    - Perform dynamic code analysis to interact with malware, modify its execution, fully map its capabilities, and extract indicators & TTPs
    - Perform memory analysis to examine how samples interact with system memory
    - Develop tools to interact with the malware, automate parts of analysis, simulate it
    - Encryption to hide the contents of traffic
    - Proxy Tunnels to hide the true destination of an application's traffic
    - VPN Tunnels to hide the true destination of all traffic
    - VPN With Cloaking Device to hide the true source of the traffic
    - Rogue system what it's doing on network 
    - Evasion techniques on disk, encrypted VMs, hidden VMs, hidden OS, Live OS 
    - Code signing certificates, software publishers, hashes, digital signatures 
    - bypassing path rules, publisher rules, leveraging trusted programs 
Documents with a VBA macro that rely on the user enabling macros
Documents with Embedded OLE objects that rely on the user executing them
Documents that exploit Office and deliver embedded shellcode

olevba, oledump 

Being able to observe how malware interacts with the operating system is essential for understanding its full capabilities and what actions it performed on a victim system.  When cryptors are used to avoid detection and reverse engineering, the information analysts are after is encrypted, decrypted just before use, and deleted immediately after use.  Observing the malware use this information in a controlled environment allows us to access this information.
Additionally, different behaviors are sometimes exhibited  depending on what the malware finds on a victim system.  In some cases, if the malware detects that it has been started in a virtual machine, it functions as usual but may use a different, fake address list for C2 communications.


2. Domains - adversary registered, dynamic DNS, and legitimate but compromised, identify patterns in registration information and infrastructure choices by the adversary , track infrastructure changes via domains, TLS certificates, passive DNS, Whois lookup data, RiskIQ/PassiveTotal, DomainTools, to understand infrastructure connections and historical context, campaign overlaps, historical analysis, high-trust domain fronting, third-party services 
Encoding and encryption to hide shellcode data and script content
 use of high-reputation domains, such as Content
Delivery Networks (CDN), to forward and hide the true destinations of
traffic.  Front end servers of CDNs such as Amazon CloudFront, Google,
Microsoft Azure, CloudFlare, Fastly, and Akamai can be used to "route"
traffic to another host on their infrastructure without revealing the
identity of the destination host.  This destination host could be a
proxy, mirror, Tor bridge, file server---anything, and could allow an
insider or malware to communicate to an unidentifiable host (via an
encrypted channel when HTTPS is used).

Obfuscation and anti-sandbox techniques to evade analysis and
reverse engineering


System profiling to identify and deny/redirect repeat visitors,
security analysts, and systems that cannot be exploited

honeyclients THug mimicks browser 

    - Malicious domains and IPs pointed to a sinkholes. 
    - IP addresses belonging to VPNs and VPSs, hosting services and reverse proxies are
    - Third party domain registrars like GoDaddy, Hover, and DreamHost manage reservations of domain names for many different users
    - Dynamic DNS is being used and a domain is pointing at multiple different IP addresses in a short period of time,
    - Compromised legitimate sites being used for Delivery, C2, and Exfil
    - Information discovered about IPs and domains that without historical context 
resources such as domain names, IP address blocks, and autonomous systems (AS).  Authoritative responses, non-authoritative answers, iterative queries, cached names and addresses, 


3. External Datasets - sharing honeypots, deception technologies from other orgs, Information Sharing and Analysis Centers (ISACs), VT Enterprise searching, downloading, domains, ips, hashes, urls, matching on community rules, bulk searching, 

Once an adversary obtains valid credentials, lateral movement can be accomplished by executing just one command on a remote system.  This one command is frequently a PowerShell one-liner that downloads and runs a stager on the victim via web delivery.  Since an authenticated account is used to execute code, this one remote command blends in with daily system administration tasks and can be very difficult to detect.
There are many different ways to perform lateral movement, but this will focus on methods that use authenticated sessions.  To track this activity, we need to understand what techniques can be used, ensure we have an accurate way to detect them when they occur, and collect the results in a way that will allow us to answer key questions during the response---such as "What accounts and systems have been compromised at this point in time?"
This document will use last week's exercise to demonstrate this process.

Scripts are plain text files containing instructions describing how to accomplish a set of tasks.  A script host is a program that reads these instructions and executes them on the system.  By integrating loops, conditional statements, and control statements, both system administrators and malware authors are able to use scripting to automate actions on a system.
Understanding how each scripting technology can be used is essential for analyzing and countering adversary operations.  Incident responders need to know which processes are used to host the different types of scripts and the different ways each scripting technology can be used to interact with the OS.

Become familiar with scripting technologies used on Windows systems and the processes that can host them:

Batch files use cmd.exe to run commands
VBScript and JScript files can create hidden COM objects and use WinAPI functions by loading .NET assemblies in memory
VBA code can be used to create hidden COM objects and call WinAPI functions on systems where Office is installed
PowerShell code can run in non-traditional hosts to avoid restrictions/monitoring of traditional hosts such as powershell.exe and powershell_ise.exe

The injection techniques described in this document require access to Windows API functions such as VirtualAllocEx and WriteProcessMemory.  API access is traditionally reserved for processes running on the system that were started from executables present on the filesystem.
Scripting languages used for system administration like PowerShell, VBA/VBScript, and JScript can access Windows APIs using COM objects.  The .NET Framework is used to run managed code and can also access the Windows API.  Because of these capabilities, scripts and .NET assemblies are frequently used by malware to perform memory injection.
The following payload delivery techniques can be used to access the Windows API functions required for memory injection:

memory injection techniques, common ways that malware accesses the Windows API, and several payload delivery techniques that can be used with phishing to deploy memory-only malware on a system

Script or Office Doc that runs VBA code
Script or Office Doc that calls PowerShell
Script or Office Doc that runs a .NET assembly

4. TLS Certificates - Encryption and tunneling are used in combination with approved protocols to bypass firewall restrictions and avoid detection by security tools and IR teams C2 servers can be protected by redirectors, domain fronting, or the use of third party services to hide their identity and location. The leveraging of legitimate certificates, legitimate processes, and legitimate domains allows an implant to disguise its presence on the system as well as how it looks on the network,  Chain of trust, fingerprinting, time-series analysis, link analysis, frequency analysis 

Censys.io 

    - Connection Types Synchronous/Direct, Asynchronous/Indirect
    - Connection Intervals Short/regular intervals, Longer/irregular intervals
    - Connection Encryption Unencrypted, Encrypted/Tunneled
    - Connection Protocols Many different (IRC, HTTP, SSH, ICMP), Few allowed (DNS, HTTP, HTTPS)
    - Connection Patterns Unusual behavior, odd/missing values in headers, payload, Closely mimics behavior and characteristics of legitimate traffic
    - C2 Server Count Single/multipurpose, Multiple with different functions
    - C2 Server Certificates None/Self-Signed, Valid Signed
    - C2 Server Domains Newly registered/Direct IP/Dynamic DNS, Reputable domains, 3rd party (webmail, cloud storage, social media), Tor
    - Process Talking Unapproved/Odd behavior, Legitimate/Approved/Normal behavior, injection, spoofing, api use, selection 
    - Process Executables File Mapped to Disk, In Memory Only, injection type, memory footprint, choice of operations


Tying intrusions, samples, and campaigns together to understand overall picture of threat group 

1. We all have a collection bias 

    Different data sets, different perspectives of groups, 

    Data sets often composed of one industry, then claim campaigns are targeting that industry 

    Defender's Bias 

        - don't know all targets
        - don't know all objectives 


2. Attribution is very difficult 

    perceived intent 

    Vendors use different data sets and name different actors 

    How do use the information differently when you have attribution 

    Difficult to have clear picture of actors, motive, payoff, effort, victims, 

    All we get is observations of behavior 

    Assessment can reduce number of possible realities.

    - intentional or negligent 
    - this negates intent, either way we have actionable information 
    - hone down to a perception of intent that satisfies intelligence requirements 

    Use activity groups so you can defend against tradecraft 

    This activity group is operating in our perception of North Korea.

    Here's my perception, here's the model I've chosen to put this in 
    I've made observations and assessments to facilitate defense 
    Attribution accuracy doesn't matter, 


Our perception of adversary intent is driven by what we can see and there are multiple intentions 
going into operations which limits our understanding of it 
Focus on what we need, make sure it's actionable 
Here's my perception of North Korea interests, this is what we're seeing and how it matches up with model 


Nothing about sources or methods makes something accurate 

"It's top secret, does not correlate to it's accurate, it just means it was sensitive. I've seen a lot of really stupid top secret documents."


The VERIS Community Database (VCDB) is an open and free repository of publicly-reported security incidents in VERIS format. 

VERIS 4 A's:  
- actor
- action
- asset
- attribute 

Mapping requirements to processes 
    - tactical (IR/Triage) - what signatures to catch malware, technical indicators and behaviors, reversing, analysis 
    - operational (Hunting/Detection) - intel on adversary behavior informing holistic remediation, hunting, behavioral detections, data collection, campaign history, behavioral descriptions, end-to-end adversary operations
    - strategic (security/organizational leadership) - business context of threat, impact informing risk management and organizational direction, adversary background, assessed intention/motivation, victimology, adversary evolution, interests, intent 


Threat Intelligence Products must be evaluated on these elements:

    - Data Sources and Visibility - must align with org environment 
                                  - providers cannot see/collect everything from every threat landscape, we must curate best we can 

    - Contextual Awareness        - need understanding of org business to be relevant  
                                  - can't protect against all threats, must contextualize threats according to requirements/threat model

    - Action Relevance            - need understanding of operations to recommend proper actions 
                                  - generic actions have no value, must have useful recommendations for custom environment/operations 

Framing effect - influenced by context and delivery 
In-group bias - favor those in your group 
Anchoring - first judgement influences following judgements 
confirmation bias - favor judgements that support your own 
belief bias - if conclusion supports your beliefs, you rationalize anything to support it 


reactance - rather do opposite of what someone is telling you to do 
backfire effect - challenged belief makes you believe more strongly 

barnum effect - see specifics in vague statements by filling in the gaps 
groupthink - social dynamics determining the outcome 


Threat intelligence improves decision making before, during, and after cybersecurity incidents reducing operational mean time to recovery, reducing adversary dwell time, and enabling root cause analysis

Must provide context and action: describe threat, illustrate impact, recommend action 

Good threat intel satisfies CART: 
    - Completeness - sufficient detail to enable proper response 
    - Accuracy - inaccuracy drives confusion and failure 
    - Relevance - must align with org threat model and operations 
    - Timeliness - must be used fast enough to make positive effect 

### ROI - Ways to Assess CTI value 

Mean Adversary Dwell Time -  time measured between adversary first gained unauthorized access and when access is removed 
Mean Time to Recovery - time measured from first operational disruption to return of normal operations 

Threat intel is knowledge, the outcome of analytic process using hypothesis-led and evidence-based analysis from a variety of data sources. 

It produces insights on adversaries and their malicious activity. Defenders use insight to improve decision making.

Reduces mean time to recovery and adversary dwell time

who - actors, sponsors, employers 
what - capabilities and infrastructure 
where - target, industries, verticals, geographic regions 
when - timelines, patterns 
why - motives and intent 
how - behaviors and patterns 

Threat intel answers 3 questions: Threat, Impact, Action 
- do this by defining context and defining action 

Context describes threat, proves/disproves relevance and impact to audience 
Action describes impact to organization if threat is realized, technical and policy recommendations customized for threat, its behavior, and impact 

Recommendations 
- detective guidance, technical indicators, signatures
- policy guidance
- detailed threat behavior to enable hunting 
- data collection suggestions to support detection 
- threat scope and impact details to support strategic decision-making 

Threat behavior analytics identify adversary tradecraft, leverage contextual knowledge of the environment
- not generic like anomaly-based, not static like signature-based, 

Vulnerability analysis - uncover/understand inherent opportunities that can be leveraged by adversaries for undesired results. 
                       - provides 4 elements: 
                            - description - short easily understood
                            - threat awareness - understand active exploitation, scope, scale of use 
                            - impact - potential damage 
                            - mitigation - actions available to defend or reduce risk 
                       - technical expertise ensures correct details, context provides correct risk level, 

### CURRENT EVENTS 

In 2024 attacks were primarily leveraged by Ransomware and other Extortion-related threat actors

As one might imagine, the main vector for those initial entry points was Web applications

one-third of all breaches involved Ransomware or some other Extortion technique

Ransomware was a top threat across 92% of industries

Over the past three years, the combination of Ransomware and other Extortion breaches accounted for almost two-thirds (fluctuating between 59% and 66%) of financially motivated attacks.


#### Progress MOVEIT VULN 

1. Forced Authentication - Use Responder to listen. Modify SSH client to Pass UNC path instead of public key
                      - Target server reaches out to authenticate, NTLMv2 hash for movitsvc account is cracked
                      - movitsvc is the account that runs the SFTP server  

                      Mitigation:    prevent svc account from remote logins
                                     prevent authentication by filepath 
                                     non-standard usernames that can't be guessed 

2. Assume Identity of Users   - Upload Public Key, Supply an empty private key file, get partial authentication, prompted for password 
                           - Interrupt password process, server sees partial authentication and grants access 
                           - NEXT, just write the Public Key to the logs, then point server to that path 

                      Mitigation:    prevent authentication by filepath (must have designated place for authorized public keys)
                                     non-standard usernames that can't be guessed 

BEHAVIORS:    TRYING TO FIND VALID USERNAME (dictionary)
              ABNORMAL KEY CHECK (parsing error)
              ABNORMAL KEY CHECK METHOD (filepath)      
              
### Recommendations 

Introduction
Setting up the requirements is the first task to be completed before investing time in researching and collecting any type of intelligence. However, in many conversations on the topic I have been into, requirements are too often confused with "which tool do we need?" and "how many people do we need?” While these parameters are part of the equation, the main goal of setting the requirements is to understand which type of information is of interest for a given organization. This because there are mainly two issues:
The overall amount of information received from different sources (e.g. sharing groups, feeds, etc.) is huge and a big part of it not relevant to your organization;
Even if you would focus only on the amount of information that interests your organization, most of the times such amount of data would still be well over what is the analyst(s) capacity.

Therefore a proper model has to define the requirements and also their priority, in order to be sure that the most relevant and most critical information is processed and not lost in the noise. 
I like to split the types of requirements in three different groups:

High Level Requirements
As the name suggests, these are general requirements like defining what type of threat actor is of interest, understanding which are the business industries of operation, etc.
Functional Requirements
These are more practical and technical requirements, based on what type of infrastructure your organization has.
Capability/Visibility Requirements
This is literally what information the analyst needs to have access to, in order to get the proper internal visibility needed to meet the requirements defined in the previous two categories.
Defining Threat Intelligence Requirements
Following are the three types of requirements explained in (slightly) more details, to give an example of what each one means. This list does not want to be exhaustive, but rather to set up an initial direction that will have to be tailored to your specific organization.

High Level Requirements

Countries of Operation
This is a very high level one. The granularity of this has to be defined. It could be referring just to the macro regions of operation (quite high level though for big organizations), to each country were major operational branches are, or to each county were the organization has a presence (even with small branches).
E.g. if your organization has no presence/business in Asia or country X, you may not be interested in activities targeting specifically that region/country.
E.g. actions led by this could be blocking traffic towards countries your organization has no business with (and/or generating an alert).
 
Business Industries of Operation
The core business of the company (e.g. insurance, bank/finance, manufacturing, energy, etc.) is obviously known and the first to start with.
This point also refers to understanding all other secondary (but relevant) industries your company is involved in and/or possesses sensitive information about;
E.g. your organization (e.g. core business finance) is also involved in oil plants, with access to blueprints for business reasons. Are there groups after these specific IP/info? Which ones?
 
Business Top Critical Assets
Assets refers to both type of critical data for the organization (Credit Card and Financial account data, Personal Identifiable Information, Intellectual Property, Confidential business information, Credentials and IT System Information), and Operational Systems for which their availability is business critical.
 
What type of Adversary may be targeting your business?
E.g. Hacktivist, Organized Crime, Corporate Espionage, Nation-State, etc.
 
Who will consume the Intelligence you collect/produce?
SOC analysts, CISO, etc., to understand whether you need to collect/produce technical, tactical and/or strategic intelligence.
 
Functional Requirements

Physical external/perimetral exposure
Servers facing external network: 
What services are publicly exposed? What OS version do they run? What DB + version? Etc. (selecting those of major importance first)
Which devices are reachable from the outside?
E.g. printers with remote maintenance access.
 
Physical internal exposure
What systems do you use internally (i.e. that have access to the internal network)?
Windows / OSX / *nix ? Which version?
Mobile?
What software/version do you use internally? (IE, Outlook, Flash, etc.). Are there unpatched vulnerabilities to be monitored? Are any of those being exploited in the wild?
What type of attachments do you allow? What types of file are allowed to be downloaded from the internal network?
Network infrastructure (yes, that famous diagram no one ever has)
 
What type of attacks/threats does your organization fear most?
DDoS attacks
Banking Trojan
Drive-by / EK
Credentials' Phishing
Intellectual Property (IP) exfiltration
Etc.
 
Capability/Visibility Requirements

Given that the best intelligence is the one you can gather from your own environment, and higher visibility into your environment will lead you to use information and tools in a more efficient way. Following there are the resources needed to have visibility on the data needed to fulfill those requirements. 

Email logs
As basic requirements, it is of paramount importance being able to access all email logs containing timestamp, sender, recipient, subject, attachment(s) name, attachment(s) hash value.
Being able to access the quarantined attachments, or having an address were to forward malicious emails for automatic processing in a safe environment;
Having access to the email header as well would be a great plus.
 
Network: Proxylogs, Firewall logs, IDS logs, DNS logs, etc.
 
Passive DNS
Another must have is a passive DNS: collect all DNS resolutions ever made by any machine within your network;
Third-party pDNS: always useful to get a broader view.
 
Endpoint visibility
Being able to search/collect information and artifacts from endpoints (i.e. memory, registry hives, running processes, etc.)
 
External feeds and sources
Free/Paid feeds of indicators
Hopefully each analyst belongs to one or more trusted sharing communities, which are usually not public. If not, create your network of trusted peers, this is a must have for an analyst.
 
Centralized storage and correlation
This may be full-fledged Threat Intelligence Platform (TIP) or an Excel spreadsheet
Useful as central collection point of the collected intel. 
Ideally can be integrated with other internal tools to allow automation
 
Action Plan
The following is a list of actions to take, which is mapped on the above requirements:

Enumerate your environment (functional requirements: internal and external exposure)
Evaluate your most critical assets the business wants you to protect (high level requirements: business top critical asset).
Identify your Adversaries (high level requirements: what type of adversary may target our business)
Prioritize the type of attacks/threats most dangerous for the business (functional requirements: what type of attacks/threats do you fear?)
Identify the main countries and especially business industries of operation (high level requirements: countries and business industries of operation)
Identify who will be the TI consumers (high level requirements: who will consume the TI?)
Once it is clear what you need to protect and what type of information needs to be collected, it is time to move to the "capability/visibility requirements”, keeping in mind what information you need in order to cover all the requirements defined above.

We have already mentioned that the first and best intelligence feeds you can get are from your own internal environment. Specifically, as also mentioned by Scott J. Roberts in his blog [1], starting from the analysis of your past incident can give you immediately a good indication about your requirements. Do those incidents fit into the requirements you have set? If not, refine them. From the past incidents, it will be possible also to check how mature are the capability/visibility requirements. If that incident will happen again, would you be able to either prevent or detect it? The requirements will tell you.
Last but not least, remember that this is an iterative process and all those requirements need to be reviewed and refined periodically, because the threat landscape will change, as well as the organization infrastructure and/or secondary business industries may change as well. How often? This is really tailored to the organization (e.g. 6 or 12 months). 

### CAMPAIGNS 

GlassRAT

Carbanak 

Axiom 

Equation Group 



