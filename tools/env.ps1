
### ### ### ### ###
#   VARIABLES     #
### ### ### ### ###


$vars               = (Get-Variable).Name
$global:defVars     = "'" + $($vars -join "','") + "'"


### ### ### ### ###
#     CMDLETS     #
### ### ### ### ###

function Get-DownloadModule {
    param(
        [string]$mod
    )
    $ErrorActionPreference = 'SilentlyContinue'
    switch($mod) {
        'dt'    { 
                    try {
                        . { Invoke-WebRequest -useb "https://gitlab.com/atstpls/silverback/-/raw/master/tools/DirTools.ps1"} | Invoke-Expression | Out-Null     
                    }
                    catch {
                        Write-Host "Switching to local file ~\source\repos\silverback\tools\DirTools.ps1"
                        . $env:USERPROFILE\source\repos\silverback\tools\DirTools.ps1 
                    }

                    $dttl = ("    " + "$([char]8228)" * 4 + "$([char]8282)" * 4)
                    $dttr = ("      " + "$([char]8282)" * 4 + "$([char]8228)" * 4)
                
                    $dtbl = ("    " + "$([char]8228)" * 4 + "$([char]8282)" * 12)
                    $dtbr = ("$([char]8282)" * 12 + "$([char]8228)" * 4)
                
                    Write-Host -Fore DarkCyan "`t`t$dttl`t" -NoNewLine;Write-Host "  DirTools (" -NoNewLine;
                    Write-Host -Fore DarkCyan "dt" -NoNewLine;Write-Host ")" -NoNewLine;Write-Host -Fore DarkCyan " $dttr"
                    Write-Host -Fore DarkCyan "`t$dtbl" -NoNewLine;Write-Host -Fore DarkGray "`t    Ver 1.5 " -NoNewLine;Write-Host -Fore DarkCyan "`t      $dtbr"
                    Write-Host ""
                }

        'it'   { 
                    try {
                        . { Invoke-WebRequest -useb "https://gitlab.com/atstpls/silverback/-/raw/master/tools/IdTools.ps1"} | Invoke-Expression | Out-Null  
                    }
                    catch {
                        Write-Host "Switching to local file ~\source\repos\silverback\tools\IdTools.ps1"
                        . $env:USERPROFILE\source\repos\silverback\tools\IdTools.ps1 
                    }

                    $ittl = ("    " + "$([char]0x02D9)" * 4 + "$([char]8282)" * 12)
                    $ittr = ("      " + "$([char]8282)" * 12 + "$([char]0x02D9)" * 4)

                    $itbl = ("    " + "$([char]0x02D9)" * 4 + "$([char]8282)" * 4)
                    $itbr = ("    " + "$([char]8282)" * 4 + "$([char]0x02D9)" * 4)


                    Write-Host -Fore DarkYellow "`t$ittl`t" -NoNewLine;Write-Host "   IdTools (" -NoNewLine;
                    Write-Host -Fore DarkYellow "it" -NoNewLine;Write-Host ")" -NoNewLine;Write-Host -Fore DarkYellow " $ittr"
                    Write-Host -Fore DarkYellow "`t`t$itbl" -NoNewLine;Write-Host -Fore DarkGray "`t     Ver 1.0" -NoNewLine;Write-Host -Fore DarkYellow "`t  $itbr"
                    Write-Host ""
                }



        'et'    { 
                    try {
                        . { Invoke-WebRequest -useb "https://gitlab.com/atstpls/silverback/-/raw/master/tools/EpTools.ps1"} | Invoke-Expression | Out-Null  
                    }
                    catch {
                        Write-Host "Switching to local file ~\source\repos\silverback\tools\EpTools.ps1"
                        . $env:USERPROFILE\source\repos\silverback\tools\EpTools.ps1 
                    }

                    $ettl = ("$([char]8228)" * 4 + "$([char]8282)" * 20 )
                    $ettr = ("    " + "$([char]8282)" * 20 + "$([char]8228)" * 4 )
                
                    $etbl = ("$([char]2417)" * 4 + "$([char]8282)" * 20 )
                    $etbr = ("$([char]8282)" * 20 + "$([char]2417)" * 4 )
                
                    Write-Host -Fore DarkGreen "    $ettl"`t -NoNewLine;Write-Host "EndpointTools (" -NoNewLine;
                    Write-Host -Fore DarkGreen "et" -NoNewLine;Write-Host ")" -NoNewLine;Write-Host -Fore DarkGreen "$ettr"
                    Write-Host -Fore DarkGreen "    $etbl"`t -NoNewLine;Write-Host -Fore DarkGray "     Ver 0.1" -NoNewLine;Write-Host -Fore DarkGreen "`t      $etbr"
                    Write-Host ""
                }

        'ht'    { 
                    try {
                        . { Invoke-WebRequest -useb "https://gitlab.com/atstpls/silverback/-/raw/master/tools/HeTools.ps1"} | Invoke-Expression | Out-Null  
                    }
                    catch {
                        Write-Host "Switching to local file ~\source\repos\silverback\tools\HeTools.ps1"
                        . $env:USERPROFILE\source\repos\silverback\tools\HeTools.ps1 
                    }

                    $httl = ("    " + "$([char]8228)" * 4 + "$([char]8282)" * 4)
                    $httr = ("     " + "$([char]8282)" * 4 + "$([char]8228)" * 4)
                
                    $htbl = ("    " + "$([char]0x02D9)" * 4 + "$([char]8282)" * 4)
                    $htbr = ("$([char]8282)" * 4 + "$([char]0x02D9)" * 4)
                
                
                
                    Write-Host -Fore DarkMagenta "`t`t$httl`t" -NoNewLine;Write-Host " HelperTools (" -NoNewLine;
                    Write-Host -Fore DarkMagenta "ht" -NoNewLine;Write-Host ")" -NoNewLine;Write-Host -Fore DarkMagenta "$httr"
                    Write-Host -Fore DarkMagenta "`t`t$htbl" -NoNewLine;Write-Host -Fore DarkGray "`t     Ver 1.5" -NoNewLine;Write-Host -Fore DarkMagenta "`t      $htbr"
                    Write-Host ""
                }



        'mt'    { 
                    try { 
                        . { Invoke-WebRequest -useb "https://gitlab.com/atstpls/silverback/-/raw/master/tools/MeTools.ps1"} | Invoke-Expression | Out-Null     
                    }
                    catch {
                        Write-Host "Switching to local file ~\source\repos\silverback\tools\MeTools.ps1"
                        . $env:USERPROFILE\source\repos\silverback\tools\MeTools.ps1 
                    }

                    $attl = ("    " + "$([char]8228)" * 4 + "$([char]8282)" * 4)
                    $attr = ("      " + "$([char]8282)" * 4 + "$([char]8228)" * 4)
                
                    $atbl = ("    " + "$([char]8228)" * 4 + "$([char]8282)" * 12)
                    $atbr = ("$([char]8282)" * 12 + "$([char]8228)" * 4)
                
                
                
                    Write-Host -Fore DarkCyan "`t`t$attl`t" -NoNewLine;Write-Host " MemTools (" -NoNewLine;
                    Write-Host -Fore DarkCyan "mt" -NoNewLine;Write-Host ")" -NoNewLine;Write-Host -Fore DarkCyan "$attr"
                    Write-Host -Fore DarkCyan "`t$atbl" -NoNewLine;Write-Host -Fore DarkGray "`t     Ver 1.5" -NoNewLine;Write-Host -Fore DarkCyan "`t      $atbr"
                    Write-Host ""
                }

        'jt'    { 
                    try {
                        . { Invoke-WebRequest -useb "https://gitlab.com/atstpls/silverback/-/raw/master/tools/jafar.ps1"} | Invoke-Expression | Out-Null   
                    }
                    catch {
                        Write-Host "Switching to local file ~\source\repos\silverback\tools\jafar.ps1"
                        . $env:USERPROFILE\source\repos\silverback\tools\jafar.ps1 
                    }
                }


        'at'    { 
                    try {
                        . { Invoke-WebRequest -useb "https://gitlab.com/atstpls/silverback/-/raw/master/tools/agents.ps1"} | Invoke-Expression | Out-Null   
                    }
                    catch {
                        Write-Host "Switching to local file ~\source\repos\silverback\tools\agents.ps1"
                        . $env:USERPROFILE\source\repos\silverback\tools\agents.ps1 
                    }

                    $gatl = ("    " + "$([char]8228)" * 4 + "$([char]8282)" * 4)
                    $gatr = ("      " + "$([char]8282)" * 4 + "$([char]8228)" * 4)
                
                    $gabl = ("    " + "$([char]8228)" * 4 + "$([char]8282)" * 12)
                    $gabr = ("$([char]8282)" * 12 + "$([char]8228)" * 4)
                
                
                
                    Write-Host -Fore DarkGray "`t`t$gatl`t" -NoNewLine;Write-Host " AgentTools (" -NoNewLine;
                    Write-Host -Fore DarkGray "at" -NoNewLine;Write-Host ")" -NoNewLine;Write-Host -Fore DarkGray "$gatr"
                    Write-Host -Fore DarkGray "`t$gabl" -NoNewLine;Write-Host -Fore Gray "`t     Ver 1.5" -NoNewLine;Write-Host -Fore DarkGray "`t      $gabr"
                    Write-Host ""
                }


        'kt'    { 
                    try {
                        . { Invoke-WebRequest -useb "https://gitlab.com/atstpls/silverback/-/raw/master/tools/kafar.ps1"} | Invoke-Expression | Out-Null   
                    }
                    catch {
                        Write-Host "Switching to local file ~\source\repos\silverback\tools\kafar.ps1"
                        . $env:USERPROFILE\source\repos\silverback\tools\kafar.ps1 
                    }
                }
    }
    $ErrorActionPreference = 'Continue'
}
function Get-Ext ($tok) {
    Get-ExtTempAWSCreds  -accountid $ext_id -user cwaite -role operations -credfile ~\.cwaite.txt -token $tok
}


### ### ### ### ###
#      CHECK      #
### ### ### ### ###


[array]$global:apis = @(
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "graph.microsoft.com"
            ApiName   = "MS Graph"
            VarName   = "msgraph"
            Endpoints = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "graph.windows.net"
            ApiName   = "AAD Graph"
            VarName   = "aadgraph"
            Endpoints = "me", "users", "groups", "contacts", "directoryRoles", "applications", "servicePrincipals", "domains", "policies"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "app.vssps.visualstudio.com"
            ApiName   = "Azure DevOps"
            VarName   = "devops"
            Endpoints = "https://docs.microsoft.com/rest/api/azure/devops?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "management.core.windows.net"
            ApiName   = "Azure Core Management"
            VarName   = "azcoremgmt"
            Endpoints = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "management.azure.com"
            ApiName   = "Azure Service Management"
            VarName   = "azsvcmgmt"
            Endpoints = "https://msdn.microsoft.com/library/azure/ee460799.aspx"
        }),
    $(New-Object -TypeName psobject -Property @{
            #      ApiHost="enrollment.manage.microsoft.com"
            ApiHost   = "api.manage.microsoft.com"
            ApiName   = "Intune"
            VarName   = "intune"
            Endpoints = "https://docs.microsoft.com/intune/intune-graph-apis?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps#intune-permission-scopes"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = ($cur_tenant_id + "-my.sharepoint.com")
            ApiName   = "OneDrive"
            VarName   = "onedrive"
            Endpoints = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            #  ApiHost=($cur_tenant_id + ".sharepoint.com")
            ApiHost   = "microsoft.sharepoint-df.com"
            ApiName   = "Sharepoint"
            VarName   = "sharepoint"
            Endpoints = "https://docs.microsoft.com/sharepoint/dev/sp-add-ins/get-to-know-the-sharepoint-rest-service?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "mysignins.microsoft.com"
            ApiName   = "MySignins"
            VarName   = "mysignins"
            Endpoints = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "officeapps.live.com"
            ApiName   = "Office Apps"
            VarName   = "officeapps"
            Endpoints = "https://ocws.officeapps.live.com/ocs/v2/"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "outlook.office365.com"
            ApiName   = "MS Exchange"
            VarName   = "exchange"
            Endpoints = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "api.spaces.skype.com"
            ApiName   = "MS Teams"
            VarName   = "teams"
            Endpoints = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "api.diagnostics.office.com"
            ApiName   = "SARA"
            VarName   = "sara"
            Endpoints = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "vault.azure.net"
            ApiName   = "Azure Key Vault"
            VarName   = "keyvault"
            Endpoints = "https://docs.microsoft.com/rest/api/keyvault/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "api.azrbac.mspim.azure.com"
            ApiName   = "Azure PIM"
            VarName   = "azure_pim"
            Endpoints = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "print.print.microsoft.com"
            ApiName   = "Universal Print"
            VarName   = "print"
            Endpoints = "https://docs.microsoft.com/en-us/universal-print/hardware/universal-print-oem-app-registration?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "api.yammer.com"
            ApiName   = "Yammer"
            VarName   = "yammer"
            Endpoints = "https://developer.yammer.com/docs/getting-started"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "officespeech.platform.bing.com"
            ApiName   = "Speech"
            VarName   = "speech"
            Endpoints = "https://docs.microsoft.com/azure/cognitive-services/speech/home?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "api.skypeforbusiness.com"
            ApiName   = "Skype"
            VarName   = "skype"
            Endpoints = "https://docs.microsoft.com/skype-sdk/ucwa/authenticationusingazuread?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "batch.core.windows.net"
            ApiName   = "Azure Batch"
            VarName   = "azure_batch"
            Endpoints = "https://docs.microsoft.com/azure/batch/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "api.azuredatacatalog.com"
            ApiName   = "Azure Catalog"
            VarName   = "azure_catalog"
            Endpoints = "https://docs.microsoft.com/azure/data-catalog/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "azure.microsoft.com/services/data-explorer"
            ApiName   = "Azure Data Explorer"
            VarName   = "azure_data_explorer"
            Endpoints = "https://docs.microsoft.com/azure/kusto/api/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "datalake.azure.net"
            ApiName   = "Azure Data Lake"
            VarName   = "azure_data_lake"
            Endpoints = "https://docs.microsoft.com/azure/data-lake-store/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "atlas.microsoft.com"
            ApiName   = "Azure Maps"
            VarName   = "azure_maps"
            Endpoints = "https://docs.microsoft.com/azure/azure-maps/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "purview.azure.net"
            ApiName   = "Azure Purview"
            VarName   = "azure_purview"
            Endpoints = "https://docs.microsoft.com/azure/purview?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "aadrm.com"
            ApiName   = "Azure Rights Management Services"
            VarName   = "azure_rms"
            Endpoints = "https://docs.microsoft.com/azure/information-protection/develop/developers-guide?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "storage.azure.com"
            ApiName   = "Azure Storage"
            VarName   = "azure_storage"
            Endpoints = "https://docs.microsoft.com/azure/storage/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "azurecustomerinsights.com"
            ApiName   = "Customer Insights"
            VarName   = "insights"
            Endpoints = "https://docs.microsoft.com/dynamics365/customer-engagement/customer-insights/ref/apiquickref?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }) ,       
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "dynamics.microsoft.com"
            ApiName   = "Dynamics 365 Business Central"
            VarName   = "dynamics_365"
            Endpoints = "https://docs.microsoft.com/dynamics-nav/fin-graph?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "admin.services.crm.dynamics.com"
            ApiName   = "Dynamics CRM"
            VarName   = "dynamics_crm"
            Endpoints = "https://go.microsoft.com/fwlink/?linkid=2143766"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "erp.dynamics.com"
            ApiName   = "Dynamics ERP"
            VarName   = "dynamics_erp"
            Endpoints = "https://docs.microsoft.com/dynamics365/unified-operations/dev-itpro/data-entities/services-home-page?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "service.flow.microsoft.com"
            ApiName   = "Flow Service"
            VarName   = "flow_service"
            Endpoints = "https://docs.microsoft.com/flow/dev-enterprise-intro?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "manage.office.com"
            ApiName   = "Office 365 Management APIs"
            VarName   = "o365_mgmt"
            Endpoints = "https://msdn.microsoft.com/office-365/office-365-management-activity-api-reference"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "onenote.com"
            ApiName   = "OneNote"
            VarName   = "onenote"
            Endpoints = "https://msdn.microsoft.com/office/office365/howto/onenote-landing"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "analysis.windows.net"
            ApiName   = "Power BI Service"
            VarName   = "power_bi"
            Endpoints = "https://docs.microsoft.com/power-bi/service-get-started?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        }),
    $(New-Object -TypeName psobject -Property @{
            ApiHost   = "rts.powerapps.com"
            ApiName   = "PowerApps Runtime Service"
            VarName   = "powerapps"
            Endpoints = "https://docs.microsoft.com/powerapps/developer/common-data-service/overview?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
        })
)

Foreach ($api in $apis) {
    New-TimeDatedVar -nam $api.varName -val $api.ApiHost 
}


### ### ### ### ###
#      SETUP      #
### ### ### ### ###

$moduleNames =     @(
    "ht"    # Helper Tools
    "dt"    # Directory Tools
    "et"    # Identity Tools
    "it"    # Endpoint Tools
    #"mt"    # Memory Tools
    #"pt"    # PitBoss Tools
    "at"    # Agent Tools
    "kt"    # Kafar Tools
    #"jt"    # Jafar Tools
)

function New-HexoWebsite {
    param(
        [string]$repoName
    )

    Set-Location "$env:USERPROFILE\source\repos\"
    git clone https://github.com/hexojs/site.git
    et-Location site
    npm install
    hexo new post "Resiliency in a Digital World"
    
    yarn add hexo-theme-aurora
    Copy-Item ".\node_modules\hexo-theme-aurora\_config.yml" "_config.aurora.yml"
    code ".\_config.aurora.yml"
    code ".\_config"
    hexo create page about
    hexo clean & hexo g & hexo server

    # _config.yml
    timezone: America/Chicago

}


New-Alias -Name "dm" -Value "Get-DownloadModule"

Foreach ($moduleName in $moduleNames){
    Get-DownloadModule $moduleName
}


# function Search-GoogleDork {
#     param(
#         [string]$term
#     )

#     # Broad domain search w/ negative search
#     'example.com -www -shop -share -ir -mfa'
    
#     # Code leaks
#     'pastebin.com'
#     'jsfiddle.net'
#     'codebeautify.org'
#     'codepen.io'

#     # PHP extension w/ parameters
#     'example.com ext:php inurl:?'

#     # Disclosed XSS and Open Redirect Bug Bounties
#     'openbugbounty.org inurl:reports intext:"example.com"'

#     # File upload endpoints
#     'example.com ”choose file”'

#     # Cloud Storage
#     's3.amazonaws.com'
#     'blob.core.windows.net'
#     'googleapis.com'
#     'drive.google.com'
#     'dev.azure.com'
#     'onedrive.live.com'
#     'digitaloceanspaces.com'
#     'sharepoint.com'
#     's3-external-1.amazonaws.com'
#     's3.dualstack.us-east-1.amazonaws.com'
#     'dropbox.com/s'
#     'box.com/s'
#     'docs.google.com inurl:"/d/"'


#     # Bug Bounty programs and Vulnerability Disclosure Programs
#     "submit vulnerability report" | "powered by bugcrowd" | "powered by hackerone"

#     */security.txt "bounty"

#     WordPress
#     inurl:/wp-admin/admin-ajax.php

#     Drupal
#     intext:"Powered by" & intext:Drupal & inurl:user

#     Joomla
#     */joomla/login

#     XSS prone parameters
#     inurl:q= | inurl:s= | inurl:search= | inurl:query= inurl:& example.com

#     Open Redirect prone parameters
#     inurl:url= | inurl:return= | inurl:next= | inurl:redir= inurl:http example.com

#     SQLi Prone Parameters
#     inurl:id= | inurl:pid= | inurl:category= | inurl:cat= | inurl:action= | inurl:sid= | inurl:dir= inurl:& example.com

#     SSRF Prone Parameters
#     inurl:http | inurl:url= | inurl:path= | inurl:dest= | inurl:html= | inurl:data= | inurl:domain= | inurl:page= inurl:& example.com

#     LFI Prone Parameters
#     inurl:include | inurl:dir | inurl:detail= | inurl:file= | inurl:folder= | inurl:inc= | inurl:locate= | inurl:doc= | inurl:conf= inurl:& example.com

#     RCE Prone Parameters
#     inurl:cmd | inurl:exec= | inurl:query= | inurl:code= | inurl:do= | inurl:run= | inurl:read= | inurl:ping= inurl:& example.com

#     Apache Server Status Exposed
#     */server-status apache

#     JFrog Artifactory
#     jfrog.io'

#     Firebase
#     firebaseio.com'

#     Extensions
#    ' ext:log | ext:txt | ext:conf | ext:cnf | ext:ini | ext:env | ext:sh | ext:bak | ext:backup | ext:swp | ext:old | ext:~ | ext:git | ext:svn | ext:htpasswd | ext:htaccess

#     API Docs
#     inurl:apidocs | inurl:api-docs | inurl:swagger | inurl:api-explorer'

#     High % inurl keywords
#     inurl:config | inurl:env | inurl:setting | inurl:backup | inurl:admin | inurl:php example[.]com

