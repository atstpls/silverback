New-Module -Name EndpointTools -Scriptblock {
[array]$global:EndpointToolsCmdlets = @(
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-Netstat"
            Alias       = "gns"
            Description = "Get network connections for endpoint"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Resolve-NameGoogle"
            Alias       = "rng"
            Description = "Resolve DNS name using Google"
            Category    = "dns"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Invoke-ReverseDnsLookup"
            Alias       = "irdl"
            Description = "Resolve IP range to DNS names"
            Category    = "dns"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Resolve-IpGoogle"
            Alias       = "rig"
            Description = "Resolve IP address using Google"
            Category    = "dns"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Resolve-NameCloudflare"
            Alias       = "rnc"
            Description = "Resolve DNS name using Cloudflare"
            Category    = "dns"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Resolve-IpCloudflare"
            Alias       = "ric"
            Description = "Resolve IP address using Cloudflare"
            Category    = "dns"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Resolve-NameWinAPI"
            Alias       = "rnw"
            Description = "Resolve DNS name using Windows API"
            Category    = "dns"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Resolve-IpWinAPI"
            Alias       = "riw"
            Description = "Resolve IP Address using Windows API"
            Category    = "dns"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Resolve-NameExt"
            Alias       = "rne"
            Description = "Resolve a name externally"
            Category    = "dns"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-Headers"
            Alias       = "gh"
            Description = "Get the headers being returned by an endpoint"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-EndpointCertificate"
            Alias       = "gec"
            Description = "Get certificate on an endpoint"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Search-GoogleResults"
            Alias       = "sgr"
            Description = "Search google indexed pages for website"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-PublicFacingEndpoints"
            Alias       = "gpep"
            Description = "Get public facing endpoints"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Invoke-EndpointCheck"
            Alias       = "iepc"
            Description = "Check all known endpoints ( `$eps )"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-EndpointsExternal"
            Alias       = "gepe"
            Description = "Get all external endpoints"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-Endpoint"
            Alias       = "gep"
            Description = "Get an endpoint"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-Parameters"
            Alias       = "gp"
            Description = "Check a web response for params (use -saml for SAML req/response)"
            Category    = "util"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-TableFromRequest"
            Alias       = "gtfr"
            Description = "Get object of table in web request"
            Category    = "util"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-OauthMetadata"
            Alias       = "gomd"
            Description = "Get metadata from oauth endpoint"
            Category    = "auth"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }), 
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-SamlMetadata"
            Alias       = "gsmd"
            Description = "Get metadata from saml endpoint"
            Category    = "saml"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }), 
    $(New-Object -TypeName psobject -Property @{
            Name        = "Test-IsHostResolvable"
            Alias       = "thr"
            Description = "Test if host resolves"
            Category    = "dns"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),    
    $(New-Object -TypeName psobject -Property @{
            Name        = "Test-IsHostResolvableExt"
            Alias       = "thre"
            Description = "Test if host resolves externally"
            Category    = "dns"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "New-UriObject"
            Alias       = "nuo"
            Description = "Create a new URI object"
            Category    = "util"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Test-IsServiceAlive"
            Alias       = "tisa"
            Description = "Test if web service responds"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-ContainerDetails"
            Alias       = "gcd"
            Description = "Get container details from various repos"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-Title"
            Alias       = "gttl"
            Description = "Get title of page in web response"
            Category    = "util"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-HostFromUri"
            Alias       = "ghfu"
            Description = "Get the host within a URI"
            Category    = "util"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
 

    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-NextRequest"
            Alias       = "gnr"
            Description = "Attempt to request next page using forms"
            Category    = "util"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-EndpointToolsCmdlets"
            Alias       = "getc"
            Description = "Get EndpointTool cmdlets"
            Category    = "util"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-EndpointTools"
            Alias       = "et"
            Description = "Get usage on cmdlets in this module "
            Category    = "util"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-IpPassiveDns"
            Alias       = "gipd"
            Description = "Get passive dns for IP"
            Category    = "dns"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-WhoisDazzle"
            Alias       = "gwdz"
            Description = "Get WHOIS data for an IP from Dazzlepod"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-WhoisData"
            Alias       = "gwd"
            Description = "Get WHOIS Data for an IP"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "good"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-DnsLookup"
            Alias       = "gdl"
            Description = "Get DNS lookup (and reverse lookup) for host"
            Category    = "dns"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = ""
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-AliveEndpoints"
            Alias       = "gaep"
            Description = "Check for endpoints with http/s services"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = @"

Go:         go get -u github.com/tomnomnom/httprobe

"@
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-EndpointsDead"
            Alias       = "gepd"
            Description = "List known endpoints not alive"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = "-"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-EndpointsNew"
            Alias       = "gepn"
            Description = "Get new endpoints"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = "-"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-Gf"
            Alias       = "ggf"
            Description = "Get gf"
            Category    = "util"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = @'


Go:         go get -u github.com/tomnomnom/gf

Config:     $gf = (Get-ChildItem "$env:USERPROFILE\go\pkg\mod\github.com\tomnomnom\gf*").FullName
            Copy-Item -Recurse "$gf\examples" "$env:USERPROFILE/.gf"
            Add-Content -Path $profile -Value '. $GOPATH/src/github.com/tomnomnom/gf/gf-completion.bash'
            Copy-Item -Recurse "$GOPATH/src/github.com/tomnomnom/gf/examples" "$env:USERPROFILE/.gf"
    
'@
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-AssetFinder"
            Alias       = "gaf"
            Description = "Get assetfinder"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "good"
            Help        = @'

Go:         go get -u github.com/tomnomnom/assetfinder

'@
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-SubFinder"
            Alias       = "gsf"
            Description = "Get subdomains for a domain"
            Category    = "recon"
            Mod         = "EpTools"
            Status      = "good"
            Help        = @'

Go:         $GO111MODULE="on"; go get -v github.com/projectdiscovery/subfinder/v2/cmd/subfinder

'@
        }), 
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-HttpProbe"
            Alias       = "ghp"
            Description = "Probe HTTP ports for an endpoint"
            Category    = "recon"
            Mod         = "EpTools"
            Status      = "good"
            Help        = @'

Go:         $GO111MODULE="on"; go get -u github.com/tomnomnom/httprobe

'@
        }), 
	$(New-Object -TypeName psobject -Property @{
            Name        = "Get-Meg"
            Alias       = "gmeg"
            Description = "Fetch many paths from many hosts"
            Category    = "util"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = @'

Go:         go get -u github.com/tomnomnom/meg

'@
        }), 
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-UnFurl"
            Alias       = "guf"
            Description = "Format URLs provided on STDIN"
            Category    = "util"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = @'


Go:         go get -u github.com/tomnomnom/unfurl

'@
        }),    
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-CertSpotter"
            Alias       = "gcs"
            Description = "Discover certificates for a domain"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = @'

Go:         go get software.sslmate.com/src/certspotter/cmd/certspotter
   
'@
        }), 
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-Amass"
            Alias       = "gama"
            Description = "Discover subdomains for a domain"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = @"

Go:         go get -v github.com/OWASP/Amass/v3/... 
            
Docker:     git clone https://github.com/OWASP/Amass.git
            cd Amass
            docker build -t amass .

"@
        }),        
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-AllUrls"
            Alias       = "gurl"
            Description = "Discover urls for domains"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = @"

Go:         $GO111MODULE="on"; go get -u -v github.com/lc/gau 
            
Docker:     git clone https://github.com/lc/gau.git
            cd gau
            docker build -t gau .

"@
        }),        
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-CurrentList"
            Alias       = "gcl"
            Description = "Get a current list of resolved, alive, notalive"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = "-"
        }),        
 
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-Subover"
            Alias       = "gso"
            Description = "Get subover"
            Category    = "info"
            Mod         = "EpTools"
            Status      = "bad"
            Help        = "-"
        })
)

###################################

#########  INFO  ###########

###################################

function Resolve-IpGoogle {
    param(
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        [ValidateScript({$_ -match [IPAddress]$_ })]  
        [string]$ip
    )
    try {
        $answer = Invoke-RestMethod "https://dns.google/resolve?name=$ip.in-addr.arpa`&type=PTR" -UserAgent $dua 
    }
    catch {}
    if ($answer.Answer){
        $answer.Answer.data.trim('.') 
    }
}
function Resolve-NameGoogle { 
    param(
        [string]$name
    )
    try {
        $answer = Invoke-RestMethod "https://dns.google/resolve?name=$name`&type=A" -UserAgent $dua
    }
    catch {}
    if ($answer.Answer){
        $answer.Answer.data.trim('.') 
    }
}
function Resolve-IpCloudflare {
    param(
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        [ValidateScript({$_ -match [IPAddress]$_ })]  
        [string]$ip
    )
    $split  = $ip.split('.')
    $address = "$split[3]\.$split[2]\.$split[1]\.$split[0]"     
    try {
        $answer = Invoke-RestMethod "https://dns.google/resolve?name=$address.in-addr.arpa`&type=PTR" -UserAgent $dua -Headers @{'Accept' = 'application/dns-json' } 
    }
    catch {}
    if ($answer.Answer){
        $answer.Answer.data.trim('.')
    }
}
function Resolve-NameCloudFlare {
    param(
        [string]$name
    ) 
    try {
        $answer = Invoke-RestMethod "https://cloudflare-dns.com/dns-query?name=$name" -UserAgent $dua -Headers @{'Accept' = 'application/dns-json' } 
    }
    catch {}
    if ($answer.Answer){
        $answer.Answer.data.trim('.') 
    }
}
function Resolve-IpWinAPI {
    param(
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        [ValidateScript({$_ -match [IPAddress]$_ })]  
        [string]$ip
    )
    try {
        $answer = [System.Net.Dns]::GetHostByAddress($ip)
    }
    catch {}
    if ($answer.HostName){
        $answer.HostName
    }
}
function Resolve-NameWinAPI {
    param(
    [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
    [string]$name
    )    
    try {
        $answer = [System.Net.Dns]::GetHostByName($name)
    }
    catch {}
    if ($answer.AddressList){
        $answer.AddressList.IPAddressToString
    }
}
function Resolve-NameExt {
    param (
        [string]$name
    )
    try {
        $a = Resolve-NameGoogle $name
    }
    catch {
        $a = Resolve-NameCloudflare $name
    }

    if ($a){
        New-Object -TypeName psobject -Property @{
            Name    = $name
            Address = $a
        }
    }
}
function Search-GoogleResults {
    param(
        [string]$targetsite = "imlive.s3.amazonaws.com",
        [string]$term,
        [array]$notinurl
    )

    $global:a = @()
    if ($notinurl) {
        [string]$nstring = ""
        $notinurl | ForEach-Object {
            $nstring += "+-inurl%3A$_"
        }
        $global:uri = "http://www.google.com/search?source=hp&q=site%3A$targetsite$nstring"
    }
    else {
        $global:uri = "http://www.google.com/search?source=hp&q=site%3A$targetsite+%2B+-+$term"
    }
    $global:googleresults = Invoke-RestMethod -proxy http://10.76.225.15 -uri $uri -useragent 'C15IE72011A'
    $global:hit = ($googleresults |  Select-String '[\;\#\,\-\.\<\>\/\w\=\?\"\s\d]+https[^\"]+' -AllMatches).Matches.Value
    if ($hit) {
        $hit | ForEach-Object {
            $desc = ($_ | Select-String '[^]<]+\<').Matches.Value.Trim('<')
            $val = ($_ | Select-String 'q=[^\s]+').Matches.Value.Trim('q=')
            $a += New-Object -TypeName psobject -Property @{
                Description = $desc
                Url         = $val
            }     
        }
        $a | Select-Object Url, Description
    }
}
function Get-OauthMetadata {
    param(
        $tenant_name = $tenant_name
    )
    Invoke-RestMethod "https://login.microsoftonline.com/$tenant_name/v2.0/.well-known/openid-configuration"
}
function Invoke-EndpointCheck {

    Clear-Vars totalEps, aliveEps
    $arr = New-Object System.Collections.ArrayList 
    $global:totalEps = New-Object System.Collections.ArrayList 
    $names = $eps | Foreach-Object { ( $_ -split '/')[2] }
    $names | % {  
        $ob = 'https://' + $_
        [void]$arr.Add($ob)
        $ob = 'http://' + $_
        [void]$arr.Add($ob)
    }

    $TotalCount = $arr.count 
    $Progress = 0

    Foreach ($ep in $arr) {
        Clear-Vars out
        $out = Get-Endpoint $ep 
        [void]$totalEps.Add($out)

        $Progress = $Progress + 1 
        Get-Progress -interval 10 -progress $Progress -item endpoint -TotalCount $TotalCount 
    }

    $global:aliveEps = $totalEps | Where-Object status -ne 0 | Where-Object status -notmatch '^$'

    Write-Success $totalEps.count, 'total endpoints saved as $totalEps'
    Write-Success $aliveEps.count, 'endpoints alive saved as $aliveEps'

}
function Get-Endpoint {
    param(
        [Parameter(Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
        [array]$endpoints,
        
        [array]$domains,
        
        [switch]$discover,
        [switch]$check,
        [switch]$headers,
        [switch]$o,

        [switch]$followRedirects,
        [switch]$useDefaultCreds,
        [switch]$defaultUserAgent
    )
    if ($PSBoundParameters.Values.Count -eq 0 -and $args.count -eq 0) { Get-CmdletHelp "Get-Hosts"; Break }
    if (!$domains -and !$endpoints) { Get-CmdletHelp "Get-Hosts"; Break }
    
    if ($domains) {
        if ($discover) {
            $subfinder = Get-Subfinder $domains
            $assetfinder = Get-AssetFinder $domains
            $certspotter = Get-CertSpotter $domains 
            $amass = Get-Amass $domains
            $endpoints = $subfinder + $assetfinder + $certspotter + $amass | Get-Unique
        }
        else {
            if (!(Test-Path $env:USERPROFILE\endpointTools)) {
                New-Item -Name endpointTools -Path $env:USERPROFILE
            }
            $endpoints = Import-CliXml $env:USERPROFILE\endpointTools\uniqueHosts.xml
            $epsAlive = Import-CliXml $env:USERPROFILE\endpointTools\uniqueAlive.xml
            $epsWithHeaders = Import-CliXml $env:USERPROFILE\endpointTools\uniqueHeaders.xml
        }
    }
     
    $global:epsWithHeaders = [System.Collections.ArrayList]::New()
    $global:dnsNames = Get-HostFromUri $endpoints
     
    if ($native) {
        $global:epsAlive = Test-IsServiceAlive $dnsNames -native
    }
    else {
        $global:epsAlive = Test-IsServiceAlive $dnsNames
    }

    Foreach ($ep in $epsAlive) {
        $certificate = Get-EndpointCertificate $dnsName -o 
        $dnsNameList = $certificate.Cert.DnsNameList.Unicode
        $header = Get-Headers -o $ep
        $header | Add-Member -NotePropertyName dnsNameList -NotePropertyValue $($dnsNameList -join "`n") 
        [void]$epsWithHeaders.Add($header)
    }

    if ($o) { $epsWithHeaders }
    else { $epsWithHeaders | Select-Object endpoint, server, statusCode, dnsNameList | Format-Table -AutoSize -Wrap }
}

function Get-EndpointCertificate {

    param (
        [Parameter(ValueFromPipeline, Mandatory = $true)]
        [array]$dnsNames,

        [int]$Port = 443,

        [switch]$o
    )
    
    Clear-Vars Certificate, cert, allCerts 
    
    $global:allCerts = [System.Collections.ArrayList]::New()
    
    Foreach ($dnsName in $dnsNames) {
        $TcpClient = New-Object -TypeName System.Net.Sockets.TcpClient
        try {
            $TcpClient.Connect($dnsName, $Port)
            $TcpStream = $TcpClient.GetStream()
            $Callback = { param($sender, $cert, $chain, $errors) return $true }
            $SslStream = New-Object -TypeName System.Net.Security.SslStream -ArgumentList @($TcpStream, $true, $Callback)
            try {
                $SslStream.AuthenticateAsClient('')
                $Certificate = $SslStream.RemoteCertificate
            }
            catch {}
            finally {
                $SslStream.Dispose()
            }
        }
        catch {}
        finally {
            $TcpClient.Dispose()
        }
        if ($Certificate) {
            if ($Certificate -isnot [System.Security.Cryptography.X509Certificates.X509Certificate2]) {
                $Certificate = New-Object -TypeName System.Security.Cryptography.X509Certificates.X509Certificate2 -ArgumentList $Certificate
            }
            $cn = $Certificate.Subject.split(',')[0].trim('CN=')
            $expd = $Certificate.GetExpirationDateString()
            $effd = $Certificate.GetEffectiveDateString()
            if ($dnsName -match [IPAddress]$_ ) {
                $ip = $dnsName
            }
            else {
                try {
                    $ip = (Resolve-DnsName $dnsName).IpAddress | Select-Object -Last 1
                }
                catch {
                    $ip = "-"
                }
            }
            $cert = New-Object -TypeName psobject -Property @{
                dnsName = $dnsName 
                address = $ip
                cn      = $cn
                cert    = $Certificate
                expDate = $expd
                effDate = $effd
            }
            [void]$allCerts.Add($cert)
        }

        if ($o) {
            return $allCerts
        }
        else {
            $allCerts | Select-Object dnsName, address, CN, ExpDate, EffDate | Format-List  

            Write-Success "Certificates saved as `$allCerts"
            Write-Host "`n"
        }
    }
}

function Get-EndpointTools {
    param(
        [Parameter(Mandatory = $false, Position = 0, ValueFromPipeline = $true)]
        [string]$type = 'default'
    )  

    Switch ($type) {

        'i' { Get-EndpointToolsCmdlets 'info' }
        'd' { Get-EndpointToolsCmdlets 'dns' }
        's' { Get-EndpointToolsCmdlets 'saml' }
        'a' { Get-EndpointToolsCmdlets 'auth' }
        'u' { Get-EndpointToolsCmdlets 'util' }
        default {
      
            Write-Host "`n"
            Write-Host -Fore DarkGreen " ...:::" -NoNewLine; Write-Host "`tUsage:   " -NoNewLine;
            Write-Host -Fore DarkGreen "et " -NoNewLine; Write-Host -Fore DarkGray "[category]"
            Write-Host "`n"

            [pscustomobject]$options = [ordered]@{
                's          saml'  = "SAML info"
                'a          auth'  = "Authentication info"
                'd          dns'   = "DNS functions"
                'i          info'  = "Gather information/routine checks"
                'u          util'  = "Utilities/helper functions"
            }

            $options.Keys | Foreach-Object {
                $firstLetter = $_[0]
                Write-Host -Fore DarkGreen "`tet " -NoNewLine;Write-Host "$firstLetter" -Fore White -NoNewLine
                Write-Host -Fore DarkGray "$($_.trim($firstLetter))" -NoNewLine ; Write-Host "`t$($options.Item("$_"))"
            }
            Write-Host "`n"
        }
    }
}

function Invoke-ReverseDnsLookup {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSShouldProcess', '')]
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseApprovedVerbs', '')]
    [CmdletBinding()]
    Param (
        [Parameter(Position = 0, Mandatory = $True,ValueFromPipeline=$True)]
        [String]
        $IpRange
    )

    BEGIN {

        function Parse-IPList ([String] $IpRange)
        {

            function IPtoInt
            {
                Param([String] $IpString)

                $Hexstr = ""
                $Octets = $IpString.Split(".")
                foreach ($Octet in $Octets) {
                        $Hexstr += "{0:X2}" -f [Int] $Octet
                }
                return [Convert]::ToInt64($Hexstr, 16)
            }

            function InttoIP
            {
                Param([Int64] $IpInt)
                $Hexstr = $IpInt.ToString("X8")
                $IpStr = ""
                for ($i=0; $i -lt 8; $i += 2) {
                        $IpStr += [Convert]::ToInt64($Hexstr.SubString($i,2), 16)
                        $IpStr += '.'
                }
                return $IpStr.TrimEnd('.')
            }

            $Ip = [System.Net.IPAddress]::Parse("127.0.0.1")

            foreach ($Str in $IpRange.Split(","))
            {
                $Item = $Str.Trim()
                $Result = ""
                $IpRegex = "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"

                # First, validate the input
                switch -regex ($Item)
                {
                    "^$IpRegex/\d{1,2}$"
                    {
                        $Result = "cidrRange"
                        break
                    }
                    "^$IpRegex-$IpRegex$"
                    {
                        $Result = "range"
                        break
                    }
                    "^$IpRegex$"
                    {
                        $Result = "single"
                        break
                    }
                    default
                    {
                        Write-Warning "Improper input"
                        return
                    }
                }

                #Now, start processing the IP addresses
                switch ($Result)
                {
                    "cidrRange"
                    {
                        $CidrRange = $Item.Split("/")
                        $Network = $CidrRange[0]
                        $Mask = $CidrRange[1]

                        if (!([System.Net.IPAddress]::TryParse($Network, [ref] $Ip))) { Write-Warning "Invalid IP address supplied!"; return}
                        if (($Mask -lt 0) -or ($Mask -gt 30)) { Write-Warning "Invalid network mask! Acceptable values are 0-30"; return}

                        $BinaryIP = [Convert]::ToString((IPtoInt $Network),2).PadLeft(32,'0')
                        #Generate lower limit (Excluding network address)
                        $Lower = $BinaryIP.Substring(0, $Mask) + "0" * ((32-$Mask)-1) + "1"
                        #Generate upper limit (Excluding broadcast address)
                        $Upper = $BinaryIP.Substring(0, $Mask) + "1" * ((32-$Mask)-1) + "0"
                        $LowerInt = [Convert]::ToInt64($Lower, 2)
                        $UpperInt = [Convert]::ToInt64($Upper, 2)
                        for ($i = $LowerInt; $i -le $UpperInt; $i++) { InttoIP $i }
                    }
                    "range"
                    {
                        $Range = $item.Split("-")

                        if ([System.Net.IPAddress]::TryParse($Range[0],[ref]$Ip)) { $Temp1 = $Ip }
                        else { Write-Warning "Invalid IP address supplied!"; return }

                        if ([System.Net.IPAddress]::TryParse($Range[1],[ref]$Ip)) { $Temp2 = $Ip }
                        else { Write-Warning "Invalid IP address supplied!"; return }

                        $Left = (IPtoInt $Temp1.ToString())
                        $Right = (IPtoInt $Temp2.ToString())

                        if ($Right -gt $Left) {
                            for ($i = $Left; $i -le $Right; $i++) { InttoIP $i }
                        }
                        else { Write-Warning "Invalid IP range. The right portion must be greater than the left portion."; return}

                        break
                    }
                    "single"
                    {
                        if ([System.Net.IPAddress]::TryParse($Item,[ref]$Ip)) { $Ip.IPAddressToString }
                        else { Write-Warning "Invalid IP address supplied!"; return }
                        break
                    }
                    default
                    {
                        Write-Warning "An error occurred."
                        return
                    }
                }
            }
        }
    }

    PROCESS {
        Parse-IPList $IpRange | ForEach-Object {
            try {
                Write-Verbose "Resolving $_"
                $Temp = [System.Net.Dns]::GetHostEntry($_)

                $Result = @{
                    IP = $_
                    HostName = $Temp.HostName
                }

                New-Object PSObject -Property $Result
            }
            catch [System.Net.Sockets.SocketException] {
                Write-Verbose "Error: $_"
            }
        }
    }
}
function Test-IsHostResolvable {
    param(
        [string]$target 
    )
    Clear-Vars address
    $address    = (Resolve-DnsName -Name $target -QuickTimeout -ErrorAction Ignore).IPAddress
    $address    = ($address -split ' ')[0]
    if ($address){
        return $address
    }
    else {
        return $null
    }
}
function Test-IsHostResolvableExt {
    param(
        [Parameter(Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
        [string]$target
    )  

    Clear-Vars address
    $ErrorActionPreference = 'SilentlyContinue'

    $address = (Invoke-RestMethod "https://dns.google/resolve?name=$target`&type=A" -UserAgent $(Get-UA)).Answer.data 
    $address = ($address -split ' ')[-1]
    $ErrorActionPreference = 'Continue'
    if ($address){
        return $address
    }
    else {
        return $null
    }
}

function Get-EndpointToolsCmdlets {
    param(
        [Parameter(Mandatory = $false, Position = 0, ValueFromPipeline = $true)]
        [string]$type
    )  

    if (!$type) {
        $cmdlets = $EndpointToolsCmdlets | Sort-Object Name
    }
    else {
        $cmdlets = $EndpointToolsCmdlets | Where-Object Category -eq $type | Sort-Object Name
    }
    Write-Host "`n"
    Write-Host -Fore DarkGray ("`tCmdlet`t`t`t`t") -NoNewLine; Write-Host -Fore DarkGray " Alias`t`t" -Nonewline;
    Write-Host -Fore DarkGray " Description" 
    Write-Host "`t$('-' * 24)`t $('-' * 5)`t`t $('-' * 30)"
    $cmdlets | ForEach-Object {
        if ($_.Status -eq 'good') {
            Write-Host -Fore Green 'o' -NoNewLine
        }
        else {
            Write-Host -Fore Red 'x' -NoNewLine
        }
        if ($_.Name.Length -gt 23) {
            Write-Host -Fore Gray ("`t" + $_.Name + "`t ") -NoNewLine; Write-Host -Fore DarkGreen $_.Alias -Nonewline
            Write-Host -Fore Gray (" `t`t " + $_.Description ) 
        }    
        elseif ($_.Name.Length -gt 15) {
            Write-Host -Fore Gray ("`t" + $_.Name + "`t`t ") -NoNewLine; Write-Host -Fore DarkGreen $_.Alias -Nonewline
            Write-Host -Fore Gray (" `t`t " + $_.Description ) 
        }
        elseif ($_.Name.Length -gt 7) {
            Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t ") -NoNewLine; Write-Host -Fore DarkGreen $_.Alias -Nonewline
            Write-Host -Fore Gray (" `t`t " + $_.Description ) 
        }
        else {
            Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t`t ") -NoNewLine; Write-Host -Fore DarkGreen $_.Alias -Nonewline
            Write-Host -Fore Gray (" `t`t " + $_.Description ) 
        }
    }
    Write-Host "`n"
}
function Get-EndpointsExternal {

    Clear-Vars xeps, address, headers, status, hn

    if (Test-IsConnected) {
        Write-Info 'VPN must be disconnected'
        Break
    }
    $global:xeps = [System.Collections.ArrayList]::new() 


    Foreach ($ep in $eps) {
        #   $hn = Get-HostFromUri $ep 
    
        #   if (Test-IsHostResolvableExt $hn){
        #     $address      = ((Resolve-DnsName -Name $hn).IPAddress -split ' ')[0] 
        #     if (Test-IsServiceAlive $ep) {
        #       $headers    = Get-Headers -o $ep 
        #       if ($headers.statusCode){
        #         $status = $headers.statusCode 
        #       }
        #       else {
        #         $status = "-"
        #       }
        #     }
        #   }
        #   else {
        #     $address      = "-"
        #   }

        #   [pscustomobject]$epinfo  = @{
        #     address     = $address 
        #     endpoint    = $ep 
        #     status      = $status 
        #   }
        #   [void]$xeps.Add($epinfo)
        # 
        $out = Get-Endpoint $ep
        [void]$xeps.Add($out)
    }

    $xeps = $xeps | Where-Object address -ne '-' | Where-Object status -ne '-' 
    Write-Success 'External endpoints saved as $xeps'
}
function Get-Headers {
    param(
        [Parameter(Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
        [string]$endpoint,

        [switch]$o,

        [switch]$followRedirects,

        [switch]$defaultCreds,

        [switch]$keepSession,

        [string]$hostheader,

        [string]$useragent,

        [switch]$defaultUserAgent
    )

    if ($endpoint -notmatch 'http') {
        $endpoint = 'http://' + $endpoint
    }
    $dnsName = $endpoint.split('/')[2]

    $request = [System.Net.WebRequest]::Create( $endpoint )
    $request.Timeout = 1000
    if (!$followRedirects) {
        $request.AllowAutoRedirect = $false
    }
    if ($hostheader) {
        $request.Host = $hostheader
    }    
    if ($useragent) {
        $request.UserAgent = $useragent
    }
    if ($defaultUserAgent) {
        $request.UserAgent = $defuseragent
    }
    if ($defaultCreds) {
        $request.UseDefaultCredentials = $true
    }
    if ($keepSession) {
        $request.CookieContainer = $session.Cookies 
    }

    $request.ServerCertificateValidationCallback += { return $true }

    try {
        $resp = $request.GetResponse()
        $headers = $resp.Headers 
        $status = $resp.StatusCode 
        $statusCode = [int]$status
        $statusDesc = $resp.StatusDescription 

        [psobject]$res = @{}
        $headers.AllKeys | Sort-Object | Foreach-Object {
            $res | Add-Member -NotePropertyName $_ -NotePropertyValue $headers.Item($_) -Force
        }
        $res | Add-Member -NotePropertyName "status" -NotePropertyValue $status -Force
        $res | Add-Member -NotePropertyName "statusCode" -NotePropertyValue $statusCode -Force
        $res | Add-Member -NotePropertyName "statusDescription" -NotePropertyValue $statusDesc -Force
        $res | Add-Member -NotePropertyName "endpoint" -NotePropertyValue $endpoint -Force
        $res | Add-Member -NotePropertyName "dnsName" -NotePropertyValue $dnsName -Force

        $out = $res | Select-Object -ExcludeProperty Item, Count, IsFixedSize, IsReadOnly, IsSynchronized, Keys, Values, SyncRoot 
        if ($o) { $out }
        else { $out | Write-HostColored -Pattern '^Status' -ForegroundColor DarkGreen -WholeLine | Format-List }
    } 
    catch [System.Net.WebException] { 
        $status = $_.Exception.Response.StatusCode 
        $statusDescription = $_.Exception.Response.StatusDescription
        $statusCode = [int]$status

        if (!$hostheader) { $hostheader = $dnsName }
        if (!$status) { $status = "-" }    
        if (!$statusDescription) { $statusDescription = "-" }

        $headers = $_.Exception.Response.Headers  
        [psobject]$res = @{}
        $headers.AllKeys | Sort-Object | Foreach-Object {
            $res | Add-Member -NotePropertyName $_ -NotePropertyValue $headers.Item($_) -Force
        }
        $res | Add-Member -NotePropertyName "status" -NotePropertyValue $status -Force
        $res | Add-Member -NotePropertyName "statusCode" -NotePropertyValue $statusCode -Force
        $res | Add-Member -NotePropertyName "statusDescription" -NotePropertyValue $statusDescription -Force
        $res | Add-Member -NotePropertyName "endpoint" -NotePropertyValue $endpoint -Force
        $res | Add-Member -NotePropertyName "hostheader" -NotePropertyValue $hostheader -Force
        $res | Add-Member -NotePropertyName "dnsName" -NotePropertyValue $dnsName -Force

        $out = $res | Select-Object -ExcludeProperty Item, Count, IsFixedSize, IsReadOnly, IsSynchronized, Keys, Values, SyncRoot 
        if ($o) {
            $out
        }
        else {
            $out | Format-List | Write-HostColored -Pattern '^Status' -ForegroundColor Red -WholeLine 
        }
    }     
}
function Test-IsServiceAlive {
    param(
        [Parameter(ValueFromPipeline)]
        [array]$dnsNames,
        
        [array]$protocolPorts,
        
        [switch]$native
    )
    $dnsNames = Get-HostFromUri $dnsNames 
    [array]$targetPorts = @( 'http:80', 'https:443', 'http:8080', 'https:8443' )
    if ($protocolPorts) {
        Foreach ($protPort in $protocolPorts) {
            if ($protPort -notmatch '^http') {
                $protPort = ConvertTo-ProtocolPort $protPort
            }
            $targetPorts += $protocolPorts
        }
    }
    if ($native) {
        Foreach ($dnsName in $dnsNames) {
            $uri = ConvertTo-Uri $dnsName 
            Foreach ($port in $targetPorts) {
                $uriPort = $uri + ':' + $port
                try {
                    Invoke-RestMethod -Uri $uriPort -SkipCertificateCheck -TimeoutSec 2 | Out-Null
                    $uriPort
                }
                catch [System.TimeoutException] {  }
                catch { $uriPort }                        
            }
        }
    }
    else {
        $targetPorts = $targetPorts | Where-Object { $_ -ne 'http:80' } | Where-Object { $_ -ne 'https:443' }
        Get-HttpProbe -dnsNames $dnsNames -protocolPorts $targetPorts -timeout 2000
    }
}
function Get-Title {
    param(
        [Parameter(Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
        $req
    )  
    if ($req){
        $title = ""
        $match = ""
        # $match = $req | Select-String '<title>(.*)[^<]'
        $match = -join $req | Select-String '<title>.*[^<]+' 
        
        if ($match) {
            $title = ($match | Get-MatchesValue).Split(">")[1].Split("<")[0].Trim("")
            # $title = [System.Web.HttpUtility]::UrlDecode($match.Matches.Groups[1].Value) 
            # return $title.Trim("")
        }
        else {
            $title  = ''
        }
        $title
    }
}

function Get-NextRequest {
	[cmdletbinding()]param(
	    $req = $lastReq,

	    [switch]$first 
	)
	if ( $PSBoundParameters.containskey("debug") )
	{
	    if ( $debug = [bool]$PSBoundParameters.item("debug") )
	    { 
		$DebugPreference = "Continue"
	    }
	}
	if (!$lastReq -and !$first)
	{
	    Write-Fail 'There is no next request'
	    Break
	}
	Clear-Vars lastReq, params, done, redir, dest  

	Write-Debug 'Building request...'

	$global:params = @{
	    MaximumRedirection = 0
	}
	if ($first)
	{
	    Clear-Vars p, f
	    $params.Add('SessionVariable', 'session')
	} else
	{
	    $params.Add('WebSession', $session)
	}
	if ($req.BaseResponse.RequestMessage.RequestUri.AbsoluteUri)
	{
	    $referer = $req.BaseResponse.RequestMessage.RequestUri.AbsoluteUri
	    $headers = @{Referer = $referer }
	    $params.Add('Headers', $headers)
	}
	if ($req.StatusCode)
	{
	    $global:p = Get-Parameters $req -o -parm 
	    if ($p) {
		Write-Info 'Parameters: ' 
		$p
	    }

	    $global:f = Get-Parameters $req -o -form 
	    if ($f)
	    {
		Write-Info 'Form action: ', $f.Action
		Write-Info 'Form method: ', $f.Method
	    }
	}

	$done = $false
	$redir = $true 
	if ($first)
	{
	    $dest = $req
	} else
	{
	    $dest = $f.Action
	}
	$base = (($dest -split '//')[1] -split '/')[0]
	Write-Debug ('Base:' + $base ) 

	while ($redir -eq $true)
	{ 

	    Clear-Vars lastReq
	    try
	    {
		if ($first -or ($statusCode -eq 302) -or ($statusCode -eq 301))
		{
		    Write-Host "`n"
		    Write-Attempt 'Sending GET:', $dest
		    $global:lastReq = Invoke-WebRequest -AllowUnencryptedAuthentication -Method 'GET' -Uri $dest -UseDefaultCredentials @params  
		} elseif (($statusCode -eq 200) -and !$f)
		{
		    Break
		} else
		{
		    Write-Host "`n"
		    Write-Attempt 'Sending POST:', $dest
		    $global:lastReq = Invoke-WebRequest -Method 'POST' -Uri $dest -UseDefaultCredentials @params -Body $p
		}

		$statusCode = $lastReq.StatusCode
	    } catch
	    {
		Write-Debug $_.Exception.Message
		$statusCode = [int]$_.Exception.Response.StatusCode
		$location = $_.Exception.Response.Headers.Location
	    }
	    if ($location.AbsoluteURI)
	    {
		$location = $location.AbsoluteURI
	    }
	    $location = [System.Web.HttpUtility]::UrlDecode($location)
	    Write-Debug ('Status code: ' + $statusCode )
	    Write-Debug ('Location: ' + $location )

	    if ($params.headers)
	    {
		$headers = @{Referer = $dest }
		$params.headers = $headers 
	    }
	    if ($statusCode -match '^3')
	    {
		$dest = $location 
		if ($dest -match '../')
		{
		    $dest = $dest -replace "\.\.", ''
		}
		if ($dest -notmatch 'https')
		{
		    $dest = "https://" + $base + $dest
		}
		Write-Success '302 >', $dest

		if ($params.SessionVariable)
		{
		    $params.Remove('SessionVariable')
		    $params.Add('WebSession', $session)
		}
	    } else
	    {
		$redir = $false
		$done = $true
	    }

	}

	if ($done -eq $false)
	{
	    $dest = $f.action 
	    $meth = $f.method
	    Write-Attempt 'Req :', $dest
	    $global:lastReq = Invoke-WebRequest -Uri $dest -Method $meth -Body $p -UseDefaultCredentials @params 
	    $statusCode = $lastReq.StatusCode 
	}

	Clear-Vars p, f, title 

	$global:p = Get-Parameters $lastReq -o -parm 
	$global:f = Get-Parameters $lastReq -o -form
	$title = Get-Title $lastReq 

	if ($title)
	{
	    Write-Host -Fore DarkGreen "> $statusCode - $title" 
	} else
	{
	    Write-Host -Fore DarkGreen "> $statusCode - $dest" 
	}
	Write-Host "`n"

	if ($f)
	{
	    Write-Info "Action:", $f.Action
	    Write-Info "Method:", $f.Method
	}
	if ($p)
	{
	    Write-Host "`n"
	    $p 
	#	| Foreach-Object {
	#	if ($_ -match 'response')
	#	{
	#	    Write-Host -Fore Magenta " > $_ $($p.Item($_).SubString(0,40))..."
	#	} elseif ($_ -match 'SAML')
	#	{
	#	    Write-Info $_, ($($p.Item($_).SubString(0, 40)) + "...") 
	#	} else
	#	{
	#	    Write-Info $_, ':', $($p.Item($_))
	#	}
	   # }
	}
}
function Get-PublicFacingEndpoints {
    param(
        [Parameter(Mandatory = $true, Position = 0)]
        [switch]$external
    )

    $Progress = 0 
    $TotalCount = $eps.count 
    if (!$external) {
        $global:CurrentEndpoints = [System.Collections.ArrayList]::new() 
        Foreach ($ep in $eps) {
            $Progress += 1
            Get-Progress -interval 10 -TotalCount $TotalCount -progress $progress -item 'endpoint'
            $output = Get-Endpoint $ep
            [void]$CurrentEndpoints.Add($output)
        }
        Write-Success $CurrentEndpoints.count, 'objects saved as $CurrentEndpoints'
    }
    else {
        $global:xeps = [System.Collections.ArrayList]::new() 
        Foreach ($ep in $eps) {
            $Progress += 1
            Get-Progress -interval 10 -TotalCount $TotalCount -progress $progress -item 'endpoint'
            $output = Get-Endpoint $ep
            [void]$xeps.Add($output)
        }
        Write-Success $xeps.count, 'objects saved as $xeps'
    }
}
function New-UriObject {
    param(
        [Parameter(Mandatory = $true, Position = 0)]
        [string]$url,

        [Parameter(Mandatory = $false, Position = 1)]
        [string]$pDict
    )  
  
    $HttpValueCollection = [System.Web.HttpUtility]::ParseQueryString([String]::Empty)
    foreach ($Item in $pDict.GetEnumerator()) {
        if ($Item.Value.Count -gt 1) {
            foreach ($Value in $Item.Value) {
                $ParameterName = $Item.Key
                if ($AddSquareBracketsToArrayParameters) { $ParameterName += '[]' }                 
                $HttpValueCollection.Add($ParameterName, $Value)
            }
        }
        else {
            $HttpValueCollection.Add($Item.Key, $Item.Value)
        }
    }
    $Request = [System.UriBuilder]($url)
    $Request.Query = $HttpValueCollection.ToString()
    return $Request.Uri
}

function Get-Netstat {
    param(
        [Parameter(Mandatory = $false, Position = 0)]
        [switch]$o
    )  
	$netstat = @()
	
	# Run netstat for tcp and udp
	$netstat_tcp = &{netstat -ano -p tcp}  | Select-String -skip 4
	$netstat_udp = &{netstat -ano -p udp} | Select-String -skip 4
	
	# Process output into objects
	foreach ($line in $netstat_tcp) { 	
		$val = -Split $line
		$l = $val[1] -Split ":" 
		$r = $val[2] -Split ":" 		
		$netstat += new-Object PSObject -Property @{
			Protocol		= $val[0] 
			Src_Address		= $l[0]
			Src_Port 		= [int]$l[1]
			Dst_Address 	= $r[0] 
			Dst_Port 		= [int]$r[1] 
			State 			= $val[3] 
			ProcessId 		= [int]$val[4]
			ProcessName 	= [String](Get-Process 2>$null -Id ([int]$val[4])).Name
		}			
	}
	foreach ($line in $netstat_udp) { 	
		$val = -Split $line
		$l = $val[1] -Split ":" 
		$netstat += new-Object PSObject -Property @{
			Protocol		= $val[0] 
			Src_Address		= $l[0]
			Src_Port 		= [int]$l[1]
			Dst_Address 	= $null
			Dst_Port 		= [int]$null 
			State 			= $null
			ProcessId 		= [int]$val[3]
			ProcessName 	= [String](Get-Process -Id ([int]$val[3])).Name
		}
	}
	if ($o){
        return $netstat 
    }
    else {
        return $netstat | Select-String ProcessName,ProcessId,Src_Address,Src_Port,Dst_Address,Dst_Port,Protocol,State | Format-Table -Auto
    }
}

function Get-Parameters {

    param(
        [Parameter(ValueFromPipeline)]
        $res,
    
        [switch]$saml,
        [switch]$form,
        [switch]$parm,
        [switch]$json,
        [switch]$o
    )

    Clear-Vars base, caps, parms, agencysel, obj, samlToken 
  
    $base = $res.BaseResponse.RequestMessage.RequestUri.Authority
    $parms = [System.Collections.ArrayList]::new()

    $caps = ($res.Content | Select-String -AllMatches "name=[\'\`"]+([^\`"\']+)[\'\`"]+[^\>]+value=[\'\`"]+([^\`"\']+).").Matches.Captures 
    foreach ($cap in $caps) {
        $obj = New-Object -TypeName psobject -Property @{
            name  = $cap.Groups[1].Value
            value = $cap.Groups[2].Value
        }
        [void]$parms.Add($obj) 
    }
  
    # $agencysel = $res | Select-String "name=._eventId_proceed"

    # if ($agencysel){
    #   $parms.Add('_eventId_proceed',"")
    # }

    if ($parm) {
        return $parms
    }

    if ($json) {
        $hits = ($s.Content | Select-String -AllMatches "(`"[a-zA-Z0-9]+`":`"[^\`"]+`")").Matches.Captures.Groups.Value
        $hits = $hits | Sort-Object -Unique 
        $j = "{" + ($hits -join ',') + '}' 
        $out = $j | ConvertFrom-Json 
        return $out
    }

    if ($form) {
        $forms = @()
        # $hits  = ($res.Content | Select-String -AllMatches '<form[^\>]+').Matches.Value
        $hits = ( $res.Content | Select-String -AllMatches '(?sm)(<form.*</form>)').Matches.Value

        if ($hits) {
            $hits | Foreach-Object {
                $action = $_ | Select-String "action=.([^\`"\']+)"
                if ($action) {
                    $action = $action.Matches.Captures.Groups[1].Value
                }
                $method = $_ | Select-String "method=.([^\`"\']+)" 
                if ($method) {
                    $method = $method.Matches.Captures.Groups[1].Value
                }
                if (!$action -and !$method) {
                    $action = $_ | Select-String "href=.([^\`"\']+)"
                    if ($action) {
                        $action = $action.Matches.Captures.Groups[1].Value
                        $method = 'get'
                    }
                }
                if ($action -match '^/') {
                    $action = "https://" + $base + $action 
                }
                $forms += New-Object -TypeName psobject -Property @{
                    Action = [System.Web.HttpUtility]::UrlDecode($action)
                    Method = $method 
                }
            }
        }
        $out = $forms 
        return $out
    }

    if ($saml) {
        $samlToken = ($parms | Where-Object Name -match 'SAML').Value 
        if ($samlToken) {
            [xml]$xmlObj = $samlToken | ConvertFrom-Base64
            if ($xmlObj.Response) {
                $out = New-Object -TypeName psobject -Property @{
                    Destination = $xmlObj.Response.Destination
                    Type        = "response"
                    Object      = $xmlObj.Response
                    Token       = $samlToken
                }
                if (!$o) {
                    Write-Info 'SAML Type:', $($out.Type)
                    Write-Info 'SAML Destination:', $($out.Destination)
                }
                return $out
            }
            elseif ($xmlObj.AuthnRequest) {
                $out = New-Object -TypeName psobject -Property @{
                    Destination = $xmlObj.AuthnRequest.Destination
                    Type        = "request"
                    Object      = $xmlObj.AuthnRequest 
                    Token       = $samlToken
                }
                if (!$o) {
                    Write-Info 'SAML Type:', $($out.Type)
                    Write-Info 'SAML Destination:', $($out.Destination)
                }
                return $out
            }
            else {
                Write-Fail 'No SAML found:', $($res.BaseResponse.ResponseUri.AbsoluteUri)
            }      
        }
    }
}
function Get-TableFromRequest {
    param(
        [Parameter(Mandatory = $true, Position = 0)]
        $req,

        [Parameter(Mandatory = $false, Position = 1)]
        $num
    )  
  
    $tables = @($req.ParsedHtml.getElementsByTagName("TABLE"))
  
    if (!$num) {
        Write-Fail 'Need to specify which table'
        Write-Fail 'there are', $tables.count  
        Break
    }
    $table = $tables[$num]
    $titles = @()
    $rows = @($table.Rows)
    $output = @()
    foreach ($row in $rows) {
        $cells = @($row.Cells)
        if ($cells[0].tagName -eq "TH") {
            $titles = @($cells | % { ("" + $_.InnerText).Trim() })
            continue
        }
        if (-not $titles) {
            $titles = @(1..($cells.Count + 2) | % { "P$_" })
        }
        $resultObject = [Ordered] @{}
        for ($counter = 0; $counter -lt $cells.Count; $counter++) {
            $title = $titles[$counter]
            if (-not $title) { continue }
            $resultObject[$title] = ("" + $cells[$counter].InnerText).Trim()
        }
        $output += [PSCustomObject] $resultObject
    }
    $output 
}

function Get-HostFromUri {
    param(
        [Parameter(ValueFromPipeline)]
        [array]$uris,
        [switch]$native
    )
    Clear-Vars first, second
  
    $dnsNames = [System.Collections.ArrayList]::New()
  
    if ($native) {
        Foreach ($uri in $uris) {
            if ($uri -match '/') {
                $uri = ($uri -split '/')[2]
            }
            if ($uri -match ':') {
                $uri = ($uri -split ':')[0]
            }
            [void]$dnsNames.Add($uri)
        }
    }
    else {
        $names = $uris | Get-Unfurl -mode domains
        [void]$dnsNames.Add($names)
    }
    $dnsNames
}

function Get-ContainerDetails {
    param(
        [string]$target
    )


    Clear-Vars repos, assets, out, images
    $global:out = [System.Collections.ArrayList]::new() 
    $global:repos = [System.Collections.ArrayList]::new() 
    $global:assets = [System.Collections.ArrayList]::new() 

    foreach ($url in $repo_urls) {
    
        Write-Success 'Searching', $url
        $repolist = Invoke-RestMethod ($url + "service/rest/v1/repositories")
        foreach ($l in $repolist) {
            $global:images = [System.Collections.ArrayList]::new()
            Clear-Vars imagelist 
            $imagelist = Invoke-RestMethod ($url + "service/rest/v1/search?repository=$l")
            if ($imagelist) {
                while ($imagelist.continuationToken) {
                    foreach ($i in $imagelist.Items) {
                        [void]$images.Add($i)
                    }
                    imagelist     = Invoke-RestMethod ($url + "service/rest/v1/search?repository=$l`&continuationToken=$($imagelist.continuationToken)")
                }
                [void]$images.Add($imagelist.Items)
            }
            $l | Add-Member -NotePropertyName images -NotePropertyValue $images 
            [void]$repos.Add($l)
        }

        $assetlist = (Invoke-RestMethod ($url + "service/rest/v1/search/assets")).Items
        foreach ($a in $assetlist) {
            [void]$assets.Add($a) 
        }

        $repoinfo = New-Object -TypeName psobject -Property @{
            source     = $url
            repoCount  = $repolist.count 
            assetCount = $assetlist.count 
        }
        [void]$out.Add($repoinfo)
    }
    $out | Format-Table -auto
}

## API ### 



function Get-SamlMetadata {
    [CmdletBinding()]
    [OutputType([xml], [System.Xml.XmlElement[]])]
    param (
        # Identity Provider Authority URI
        [string] $Issuer = "https://sts.windows.net/5e41ee74-0d2d-4a72-8975-998ce83205eb/federationmetadata/2007-06/federationmetadata.xml",

        # Azure AD Application Id
        [Parameter(Mandatory = $false, Position = 2)]
        [guid] $AppId
    )

    ## Build common federation metadata URI
    $uriFederationMetadata = New-Object System.UriBuilder $Issuer
    if (!$uriFederationMetadata.Path.EndsWith('/FederationMetadata/2007-06/FederationMetadata.xml', $true, $null)) { $uriFederationMetadata.Path += '/FederationMetadata/2007-06/FederationMetadata.xml' }
    if ($AppId) {
        $uriFederationMetadata.Query = ConvertTo-QueryString @{
            AppId = $AppId
        }
    }

    ## Download and parse federation metadata
    $FederationMetadata = Invoke-RestMethod -UseBasicParsing -Uri $uriFederationMetadata.Uri.AbsoluteUri -ErrorAction Stop  # Should return ContentType 'application/samlmetadata+xml'
    if ($FederationMetadata -is [string]) {
        try {
            [xml] $xmlFederationMetadata = $FederationMetadata -replace '^[^<]*', ''
        }
        catch { throw }
    }
    else {
        [xml] $xmlFederationMetadata = $FederationMetadata
    }

    return $xmlFederationMetadata.GetElementsByTagName('EntityDescriptor')

}
function Get-IpPassiveDns {
    
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        [ValidateScript({$_ -match [IPAddress]$_ })]  
        [string]$ip
    )
    $uri = "https://freeapi.robtex.com/ipquery/$ip"
    try {
        $pdns = (Invoke-RestMethod -Uri $uri -UserAgent $defaultUseragent).pas
        foreach ($p in $pdns) {
            $time = New-Object -Type DateTime -ArgumentList 1970, 1, 1, 0, 0, 0, 0
            $time = $time.AddSeconds($p.t)
            $p | Add-Member -NotePropertyName Time -NotePropertyValue $time
            $p | Add-Member -NotePropertyName Name -NotePropertyValue $p.o
        }
        $pdns | Select-Object Name, Time | Sort-Object -Desc Time
    }
    catch [System.Net.Http.HttpRequestException] {
        $status = $error[0].exception.Response.StatusCode
        $title = $error[0].ErrorDetails.Message
        [psobject]@{status = $status; title = $title }
    }
}

function Get-WhoisDazzle {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        [ValidateScript({$_ -match [IPAddress]$_ })]  
        [string]$ip
    )
    $h = @{'accept' = 'application/json' }
    $u = "http://dazzlepod.com/ip/$ip.json"
    Invoke-RestMethod -Headers $h -Uri $u -UserAgent $defaultUseragent
}

function Get-WhoisData {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        [ValidateScript({$_ -match [IPAddress]$_ })]  
        [string]$ip
    )
    $h = @{'accept' = 'application/json' }
    $u = "https://whois.arin.net/rest/ip/$ip"
    (Invoke-RestMethod -Headers $h -Uri $u -UserAgent $defaultUseragent).net
}
function Get-DnsLookup {
    param(
        [Parameter(Position = 0)]
        [ValidateNotNullOrEmpty()]
        [string]$dnsName
    )
    $array = [System.Collections.ArrayList]::New()
    [array]$sources = @(
        [psobject]@{
            name = "google"
            fwd  = "https://dns.google/resolve?name=$dnsName`&type=A"
            rev  = "https://dns.google/resolve?name=$dnsName.in-addr.arpa`&type=PTR"
            head = @{}
        },
        [psobject]@{
            name = "cloudflare"
            fwd  = "https://cloudflare-dns.com/dns-query?name=$dnsName"
            rev  = "https://dns.google/resolve?name=$dnsName.in-addr.arpa`&type=PTR" 
            head = @{'Accept' = 'application/dns-json' } 
        }
    )
    $params = @{
        UserAgent = @{'UserAgent' = $defaultUseragent }
    }
    
    if ($name -match [IPAddress]$_ ) {
        Foreach ($source in $sources) {
            try {
                $resp = Invoke-RestMethod -Uri $source.rev @params
                $data = $resp.Answer.data
            }
            catch {
                $data = [System.Net.Dns]::GetHostByAddress($dnsName)
            }
            $obj = New-Object -TypeName psobject -Property @{
                source = $source.name
                name   = $resp.answer.name.trim('.')
                ttl    = $resp.answer.ttl
                data   = $data
                type   = "reverse"
            }
            [void]$array.Add($obj)
        }
    }
    else {  
        Foreach ($source in $sources) {
            try {
                $response = Invoke-RestMethod $source.fwd -UserAgent $defaultUseragent -Headers $source.head 
                if ($response.Answer) {
                    ForEach ($answer in $response.Answer) {
                        if ($answer.name) { $aname = $answer.name.trim('.') } else { $aname = "-" } 
                        if ($answer.ttl) { $attl = $answer.ttl } else { $attl = "-" } 
                        if ($answer.data) { $adata = $answer.data } else { $adata = "-" } 
                        $obj = New-Object -TypeName psobject -Property @{
                            source = $source.name
                            name   = $aname
                            ttl    = $attl 
                            data   = $adata
                            type   = "forward"
                        }
                        [void]$array.Add($obj)
                    }
                }
            }
            catch [System.Net.Http.HttpRequestException] {
                $response = $error[0].exception.Response
                $title = $error[0].ErrorDetails.Message
                Write-Host $source, $title 
            }
        }
    }
    $array | Select-Object source, name, ttl, data, type | Format-Table -AutoSize 
}




function Get-SubFinder {
    param(
        [array]$domains,
        [switch]$o
    )
    if ($PSBoundParameters.Values.Count -eq 0 -and $args.count -eq 0) { Get-CmdletHelp "Get-SubFinder"; Break }
        
    $global:subdomains = [System.Collections.ArrayList]::New()
    
    Foreach ($domain in $domains) {
        try {
            $results = subfinder -all -silent -d $domain
        }
        catch {
        }
        Foreach ($results in $results) {
            [void]$subdomains.Add($result)
        }
    }
    if ($o) {
        $subdomains
    }
    else {
        Write-Info $array.count, 'subdomains saved as $subdomains'
    }
}

function Get-AssetFinder {
    param(
        [array]$domains
    )
    if ($PSBoundParameters.Values.Count -eq 0 -and $args.count -eq 0) { Get-CmdletHelp "Get-AssetFinder"; Break }
        
    Foreach ($domain in $domains) {
        try {
            & "$env:GOHOME\bin\assetfinder" -subs-only $domain 
        }
        catch {
            docker run --rm -i heywoodlh/tomnomnom-tools:latest assetfinder -subs-only $domain 
        }
    }
}

function Get-HttpProbe {
    param(
        [array]$dnsNames,
        
        [array]$protocolPorts
    )
    [string]$portArgs = ''

    $protocolPorts = $protocolPorts | Where-Object { $_ -ne 'http:80' } | Where-Object { $_ -ne 'https:443' }
    Foreach ($protocolPort in $protocolPorts) {
        $portArgs += "-p $protocolPort "
    }
    Invoke-Expression "`"$dnsNames`" | & "$env:GOHOME\bin\httprobe" $portArgs -t 2000"
}


function Get-AliveEndpoints {
    param(
        [array]$dnsNames,
        [switch]$native
    )
    if ($PSBoundParameters.Values.Count -eq 0 -and $args.count -eq 0) { Get-CmdletHelp "Get-AliveEndpoints"; Break }

    $dnsNames = $dnsNames | Foreach-Object { Get-HostFromUri $dnsName }

    if ($native) { 
        Foreach ($dnsName in $dnsNames) {
            $status = Test-IsServiceAlive $dnsName
        }
    }

    else {
        try { $hostnames | httprobe }
        catch { docker run --rm -i heywoodlh/tomnomnom-tools:latest ash -c "cat | httprobe $dnsNames" }
    }


    $array = [System.Collections.ArrayList]::New()
    $out = $hosts | httprobe
    $out = $out | Sort-Object -Unique 
    Foreach ($o in $out) {
        $resp = Get-Headers $o -o 
        [void]$array.Add($resp)
    }
    $array
}
function Get-GF {
    param(
        [Parameter(ValueFromPipeline)]
        [string]$pattern
    )
    if ($PSBoundParameters.Values.Count -eq 0 -and $args.count -eq 0) { Get-CmdletHelp "Get-GF"; Break }

    if (!$pattern) {
        Write-Fail 'Must use one of the following patterns:'
        docker run --rm -i heywoodlh/tomnomnom-tools:latest ash -c "gf -list"
    }
    else {
        try { gf $pattern }
        catch { docker run --rm -i heywoodlh/tomnomnom-tools:latest ash -c "cat | gf $pattern" }
    }
}
function Get-Meg {
    param(
        [string]$pathsFile,

        [string]$hostsFile,

        [string]$outDir
    )
    if ($args.count -lt 3) { Get-CmdletHelp "Get-Meg"; Break }

    if ($header) {
        meg --verbose --header "Host: $header" $paths $hosts
    }
    else {
        meg --verbose --header $paths $hosts
    } 
    try {
        meg --verbose $pathsFile $hostsFile $outDir
    }
    catch {
        docker run -v $($pathsFile):/tmp/paths -v $($hostsFile):/tmp/hosts -v $($outDir):/tmp/outDir --rm -i heywoodlh/tomnomnom-tools:latest meg /tmp/paths /tmp/hosts /tmp/outDir $argv[4..20]
        docker run -v $($hostsFile):/tmp/hosts -v $($outDir):/tmp/outDir --rm -i heywoodlh/tomnomnom-tools:latest meg $pathsFile /tmp/hosts /tmp/outDir $args4 
    }
}
function Get-Unfurl {
    param(
        [Parameter(ValueFromPipeline)]
        [array]$urls,
        
        [ValidateSet("keys", "values", "keypairs", "domains", "paths", "format")]
        [string]$mode
    )
    if ($PSBoundParameters.Values.Count -eq 0 -and $args.count -eq 0) { Get-CmdletHelp "Get-Unfurl"; Break }
   
    try {
        $urls | unfurl -u $mode
    }
    catch {
        docker run --rm -i heywoodlh/tomnomnom-tools:latest bash -c "cat | unfurl -u $mode "
    }
}
function ConvertTo-Deflate {
    param(
        $base64data
    )

    $data = [System.Convert]::FromBase64String($base64data)
    $ms = New-Object System.IO.MemoryStream
    $ms.Write($data, 0, $data.Length)
    $ms.Seek(0, 0) | Out-Null

    $sr = New-Object System.IO.StreamReader(New-Object System.IO.Compression.DeflateStream($ms, [System.IO.Compression.CompressionMode]::Decompress))

    while ($line = $sr.ReadLine()) {  
        $line
    }
}
function Get-AllUrls {
    param(
        [array]$domains
    )
    if ($PSBoundParameters.Values.Count -eq 0 -and $args.count -eq 0) { Get-CmdletHelp "Get-AllUrls"; Break }

    Foreach ($domain in $domains) {
        try { gau $domain }
        catch { docker run --rm -i gau $domain }
    }
}
function Get-CertSpotter {
    param(
        [array]$domains
    )
    if ($PSBoundParameters.Values.Count -eq 0 -and $args.count -eq 0) { Get-CmdletHelp "Get-CertSpotter"; Break }

    Foreach ($domain in $domains) {
        try {
            $domain | certspotter -watchlist - 
        }
        catch {
        }
    }
}
function Get-Amass {

    param(
        [array]$domains
    )
    if ($PSBoundParameters.Values.Count -eq 0 -and $args.count -eq 0) { Get-CmdletHelp "Get-Amass"; Break }
    
    Foreach ($domain in $domains) {
        try {
            amass enum -active -d $domain -dir . 
        }
        catch {
            docker run -it --rm -v "C:\Users\atstaple\:/root" amass enum -active -d $domain -dir /root/.
        }
    }
}

function Get-Subover {
    SubOver.exe -l $env:USERPROFILE\hosts
}



############################

########## MAIN ############

############################


$global:EndpointToolsFunctions = $EndpointToolsCmdlets.Name 

$EndpointToolsCmdlets | Foreach-Object { Set-Alias -Name $_.Alias -Value $_.Name -Scope Global -Force }


Export-ModuleMember -function $EndpointToolsFunctions 

}

