New-Module -Name Kafar -Scriptblock {

    [array]$global:KafarCmdlets = @(
    
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-KuPrice"
            Alias         = "gkp"
            Description   = "Get Kucoin price"
            Category      = "u"
            Mod           = "kafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-KafarCmdlets"
            Alias         = "gjc"
            Description   = "Get cmdlets"
            Category      = "h"
            Mod           = "kafar"
            Status        = "good"
        }),               
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-KafarTools"
            Alias         = "kt"
            Description   = "Get tools"
            Category      = "h"
            Mod           = "kafar"
            Status        = "good"
        }),               
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-KuServerTime"
            Alias         = "kust"
            Description   = "Get server time"
            Category      = "u"
            Mod           = "kafar"
            Status        = "good"
        }),               
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-KuSignature"
            Alias         = "ksig"
            Description   = "Get signature for API use"
            Category      = "u"
            Mod           = "kafar"
            Status        = "good"
        }),               
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-KuApi"
            Alias         = "kapi"
            Description   = "Get Kucoin API endpoint"
            Category      = "i"
            Mod           = "kafar"
            Status        = "good"
        }),               
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-KuAccountOverview"
            Alias         = "kao"
            Description   = "Get account overview"
            Category      = "i"
            Mod           = "kafar"
            Status        = "good"
        }),  
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-KuPositions"
            Alias         = "kp"
            Description   = "Get KuCoin positions"
            Category      = "i"
            Mod           = "kafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-KuTransactions"
            Alias         = "kp"
            Description   = "Get KuCoin transactions"
            Category      = "i"
            Mod           = "kafar"
            Status        = "good"
        }),  
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-KuOrders"
            Alias         = "kp"
            Description   = "Get KuCoin orders"
            Category      = "i"
            Mod           = "kafar"
            Status        = "good"
        }),  
        $(New-Object -TypeName psobject -Property @{
            Name          = "ConvertTo-USDollar"
            Alias         = "cud"
            Description   = "Convert to dollars"
            Category      = "u"
            Mod           = "kafar"
            Status        = "good"
        }),  
        $(New-Object -TypeName psobject -Property @{
            Name          = "New-Badger"
            Alias         = "nb"
            Description   = "Create a new badger"
            Category      = "o"
            Mod           = "kafar"
            Status        = "good"
        }),  
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-Projections"
            Alias         = "gpp"
            Description   = "Get price projections"
            Category      = "o"
            Mod           = "kafar"
            Status        = "good"
        })    
    )

    function Get-KafarTools {
        param(
        [string]$type = 'default'
        )
    
        Switch ($type) {
    
            'a'          {  Get-KafarCmdlets 'a'       }
            'c'          {  Get-KafarCmdlets 'c'      }
            'i'          {  Get-KafarCmdlets 'i'       }
            'o'          {  Get-KafarCmdlets 'o'      }
            'p'          {  Get-KafarCmdlets 'p'      }
            's'          {  Get-KafarCmdlets 's'      }
            'u'          {  Get-KafarCmdlets 'u'      }
        default        {
            
            Write-Host "`n"
            Write-Host -Fore DarkCyan " ...:::" -NoNewLine;Write-Host "`tUsage:   " -NoNewLine;
                Write-Host -Fore DarkCyan "kt " -NoNewLine;Write-Host -Fore DarkGray "[category]"
            Write-Host "`n"
    
            [pscustomobject]$options = [ordered]@{
                'a'       = "Assets`t >   Info on assets"
                'c'       = "Calc  `t >   Order calculations"
                'i'       = "Info `t >   Query for information"
                'o'       = "Orders`t >   Trading tools"
                'p'       = "Prices`t >   Get prices"
                's'       = "Set `t >   Set conditions"
                'u'       = "Utils  `t >   Support functions"
            }
    
            $options.Keys | Foreach-Object {
            Write-Host -Fore DarkCyan "`tkt " -NoNewLine;
            Write-Host -Fore DarkGray "$_" -NoNewLine ; Write-Host "`t`t$($options.Item("$_"))"
            }
            Write-Host "`n"
        }
        }
    }
    function Get-KafarCmdlets {
        param(
            [string]$type
        )

        if (!$type) {
            $cmdlets = $KafarCmdlets | Sort-Object Name
        }
        else {
            $cmdlets = $KafarCmdlets | Where-Object Category -eq $type | Sort-Object Name
        }
        Write-Host "`n"
        $cmdlets | Select-Object Name,Alias,Description | Format-Table -Auto  |  Write-HostColored -PatternColorMap @{
            'Name'          ='DarkGray'
            'Alias'         ='DarkGray'
            'Description'   ='DarkGray'
            '-'             ='DarkCyan'} 
        Write-Host "`n"
    }  
    function Get-KuServerTime {
        param(
            [switch]$raw
        )
        $endpoint = "https://api.kucoin.com/api/v1/timestamp"
        (Invoke-RestMethod -Uri $endpoint).data
    }
    function Get-KuAccountOverview {
        param(
            [switch]$raw
        )
        if ($raw){
            Get-KuApi -path "account-overview?currency=XBT" -raw
        }
        else {
            Get-KuApi -path "account-overview?currency=XBT"
        }
    }
    function ConvertTo-USDollar {
        param(
            [Parameter(Mandatory=$true)]
            [float]$btc,

            [float]$price, 

            [switch]$whole
        )
        if ($price){
            $out                = $price * $btc
        }
        else {
            $symbolPrice        = Get-KuPrice 
            $out                = $btc * $symbolPrice
        }
        if ($whole){
            return [Math]::Round($out, 0)
        }
        else {
            return [Math]::Round($out, 2)
        }
    }
    function Get-KuSignature {
        param(
            [string]$secret,
            [string]$message
        )
        $global:secret_b                       = [Text.Encoding]::UTF8.GetBytes($secret)
        $global:message_b                      = [Text.Encoding]::UTF8.GetBytes($message)
        $global:hmacsha                        = New-Object System.Security.Cryptography.HMACSHA256
        $global:hmacsha.key                    = $secret_b
        $global:signatureBytes                 = $hmacsha.ComputeHash($message_b)
        $global:signature                      = [Convert]::ToBase64String($signatureBytes)
        # $signature                             = Write-Output $signatureBytes -NoEnumerate | ConvertTo-HexString -Delimiter ''
        return $signature
    }
    function Get-KuTransactions {
        param(
            [switch]$raw
        )
        if ($raw){
            Get-KuApi -path "transaction-history" -raw
        }
        else {
            (Get-KuApi -path "transaction-history").dataList
        }
    }
    function Get-KuOrders {
        param(
            [string]$type       = "active",
            [switch]$raw,
            [switch]$o 
        )
        if ($raw){
            Get-KuApi -path "orders" -raw
        }
        else {
            if ($type -eq "done"){
                if ($o){
                    (Get-KuApi -path "orders?status=done").items
                }
                else {
                    (Get-KuApi -path "orders?status=done").items | Format-Table -AutoSize
                }
            }
            else {
                if ($o){
                    (Get-KuApi -path "orders?status=active").dataList
                }
                else {
                    (Get-KuApi -path "orders?status=active").dataList | Format-Table -AutoSize
                }
            }
        }
    }
    function Get-KuPositions {
        param(
            [string]$symbol,
            [switch]$raw
        )
        $out = Get-KuApi -path "position?symbol=$symbol" -raw
        if ($raw){
            $out.data
        }
        else {
            $obj = New-Object -TypeName psobject -Property @{
                __upnl__                = ConvertTo-USDollar -btc $out.unrealisedPnl
                __symbol__              = $out.settleCurrency
                __upnlp__               = $out.unrealisedPnlPcnt
                __avgPrice__            = $out.avgEntryPrice
                __liqPrice__            = $out.liquidationPrice
                __margin__              = ConvertTo-USDollar -btc $out.posMargin
                __lev__                 = $out.realLeverage
                __qty__                 = $out.currentQty
                __mkPrice__             = $out.markPrice 
            }
            $obj | Select-Object __symbol__,__qty__,__lev__,__upnl__,__upnlp__,__mkPrice__,__avgPrice__,__liqPrice__,__margin__ | Format-Table -Auto
        }
    }

    function Get-Projections {
        param(
            $buyPri,
            $qty,
            $animal,
            $symbol         = "BTCUSDT"
        )

        class Animal
        {
            [ValidateNotNullOrEmpty()][string]$symbol
            [ValidateNotNullOrEmpty()][float]$tp
            [ValidateNotNullOrEmpty()][float]$bo
            [ValidateNotNullOrEmpty()][float]$so
            [ValidateNotNullOrEmpty()][float]$sos
            [ValidateNotNullOrEmpty()][float]$os
            [ValidateNotNullOrEmpty()][float]$ss
            [ValidateNotNullOrEmpty()][float]$sl
            [float]$so1_s
            [float]$so2_s
            [float]$so3_s
            [float]$so4_s
            [float]$so5_s
            [float]$so1
            [float]$so2
            [float]$so3
            [float]$so4
            [float]$so5
        }
        class Order
        {
            [float]$__price__
            [float]$__volume__
            [string]$__name__
            [float]$__total__
        }
        switch ($animal) {
            "whale"    {
                $bot = [Animal]@{ 
                    symbol  = $symbol
                    tp      = 0.22
                    bo      = 10
                    so      = 10
                    sos     = 0.21
                    os      = 1.09
                    ss      = 1.15
                    sl      = 0

                }
            }
            "lion" { 
                $bot = [Animal]@{
                    symbol  = $symbol
                    tp      = 5
                    bo      = 10
                    so      = 20
                    sos     = 0.21
                    os      = 1.35
                    ss      = 2.58
                    sl      = 0
                }
            }
            "badger" {
                $bot = [Animal]@{
                    symbol  = $symbol
                    tp      = 11
                    bo      = 3
                    so      = 9
                    sos     = 0.55
                    os      = 2.1
                    ss      = 1.39
                    sl      = 0
                }
            }
            default {}
        }
        $priDev             = $bot.sos
        $global:pkg         = [System.Collections.ArrayList]::New();    [void]$pkg.Add([Order]@{__price__=$buyPri; __volume__=$qty; __name__='entry'; __total__=$qty})

        $profPer            = $bot.tp / 100
        $sellPriLong        = $buyPri * ( 1 + $profPer);                [void]$pkg.Add([Order]@{__price__=$sellPriLong; __volume__=0; __name__='sellLong'; __total__=$qty})
        $sellPriShort       = $buyPri * ( 1 - $profPer);                [void]$pkg.Add([Order]@{__price__=$sellPriShort; __volume__=0; __name__='sellShort'; __total__=$qty})

        $so1Vol             = $bot.bo * $bot.os;      
        $totalVol           = $qty + $so1Vol
        $bot.so1            = $buyPri - ( $priDev * $buyPri ) / 100;    [void]$pkg.Add([Order]@{__price__=$bot.so1; __volume__=$so1Vol; __name__='so1_L'; __total__=$totalVol})
        $bot.so1_s          = $buyPri + ( $priDev * $buyPri ) / 100;    [void]$pkg.Add([Order]@{__price__=$bot.so1_s; __volume__=$so1Vol; __name__='so1_S'; __total__=$totalVol})  

        $priDev             = $priDev * $bot.ss / 100
        $so2Vol             = $so1Vol * $bot.os
        $totalVol           = $totalVol + $so2Vol
        $bot.so2            = $bot.so1 - ( $priDev * $bot.so1 );                [void]$pkg.Add([Order]@{__price__=$bot.so2; __volume__=$so2Vol; __name__='so2_L'; __total__=$totalVol})
        $bot.so2_s          = $bot.so1_s + ( $priDev * $bot.so1_s );              [void]$pkg.Add([Order]@{__price__=$bot.so2_s; __volume__=$so2Vol; __name__='so2_S'; __total__=$totalVol})
        
        $priDev             = $priDev * $bot.ss / 100
        $so3Vol             = $so2Vol * $bot.os
        $totalVol           = $totalVol + $so3Vol
        $bot.so3            = $bot.so2 - ( $priDev * $bot.so2 );          [void]$pkg.Add([Order]@{__price__=$bot.so3; __volume__=$so3Vol; __name__='so3_L'; __total__=$totalVol})
        $bot.so3_s          = $bot.so2_s + ( $priDev * $bot.so2_s );        [void]$pkg.Add([Order]@{__price__=$bot.so3_s; __volume__=$so3Vol; __name__='so3_S'; __total__=$totalVol})
        
        $priDev             = $priDev * $bot.ss  / 100
        $so4Vol             = $so3Vol * $bot.os
        $totalVol           = $totalVol + $so4Vol
        $bot.so4            = $bot.so3 - ( $priDev * $bot.so3 );          [void]$pkg.Add([Order]@{__price__=$bot.so4; __volume__=$so4Vol; __name__='so4_L'; __total__=$totalVol})
        $bot.so4_s          = $bot.so3_s + ( $priDev * $bot.so3_s );        [void]$pkg.Add([Order]@{__price__=$bot.so4_s; __volume__=$so4Vol; __name__='so4_S'; __total__=$totalVol})
                
        $priDev             = $priDev * $bot.ss  / 100
        $so5Vol             = $so4Vol * $bot.os
        $totalVol           = $totalVol + $so5Vol
        $bot.so5            = $bot.so4 - ( $priDev * $bot.so4 );          [void]$pkg.Add([Order]@{__price__=$bot.so5; __volume__=$so5Vol; __name__='so5_L'; __total__=$totalVol})
        $bot.so5_s          = $bot.so4_s + ( $priDev * $bot.so4_s );        [void]$pkg.Add([Order]@{__price__=$bot.so5_s; __volume__=$so5Vol; __name__='so5_S'; __total__=$totalVol})

        $pkg | Select-Object __name__, __price__, __volume__, __total__ | Sort-Object __price__ -Descending | Format-Table -AutoSize 

    }
    function New-Badger {
        param(
            [switch]$raw,
            [ValidateRange("sell", "buy")]
            [string]$side,
            [string]$symbol         = "XBTUSDM",
            [string]$size,
            [string]$price,
            [string]$leverage
        )

        Get-KuApi -path orders -post 
        
        $clientOid      = Get-Random -Maximum 999999999999999

        $body   = "{`"clientOid`":`"$clientOid`",`"side`":`"$side`",`"symbol`":`"$symbol`",`"leverage`":`"$leverage`",`"size`":`"$size`",`"price`":`"$price`"}"

        $out = Get-KuApi -path "orders" -body $body

        if ($raw){
            $out.data
        }
        else {
            $obj = New-Object -TypeName psobject -Property @{
                __upnl__                = ConvertTo-USDollar -btc $out.unrealisedPnl
                __symbol__              = $out.settleCurrency
                __upnlp__               = $out.unrealisedPnlPcnt
                __avgPrice__            = $out.avgEntryPrice
                __liqPrice__            = $out.liquidationPrice
                __margin__              = ConvertTo-USDollar -btc $out.posMargin
                __lev__                 = $out.realLeverage
                __qty__                 = $out.currentQty
                __mkPrice__             = $out.markPrice 
            }
            $obj | Select-Object __symbol__,__qty__,__lev__,__upnl__,__upnlp__,__mkPrice__,__avgPrice__,__liqPrice__,__margin__ | Format-Table -Auto
        }
    }
    function Get-KuPrice {
        param(
            [string]$symbol         = 'XBTUSDM'
        )
        if ($raw){
            Get-KuApi -path "ticker?symbol=$symbol"
        }
        else {
            Get-KuApi -path "ticker?symbol=$symbol" | Select-Object -Expand bestAskPrice
        }
    }
    function Get-KuApi {
        param(
            [string]$path,
            [string]$post
        )

        $endpoint               = "https://api-futures.kucoin.com/api/v1/$path"
        $kucoin_api             = Get-Cred kucoin_api
        $kucoin_sec             = Get-Cred kucoin_sec
        $kucoin_pass            = Get-Cred kucoin_pass
        $timestamp              = Get-Date | ConvertTo-UnixTimestamp
        
        if ($post){
            $global:toSign          = "${timestamp}POST/api/v1/${path}${post}"
        }
        else {
            $global:toSign          = "${timestamp}GET/api/v1/$path"
        }
        $global:messSignature   = Get-KuSignature -message $toSign -secret $kucoin_sec 
        $global:passSignature   = Get-KuSignature -message $kucoin_pass -secret $kucoin_sec 

        $global:sortedArgs      = [System.Collections.SortedList]::New()
        $sortedArgs.Add(  'KC-API-SIGN', $messSignature )
        $sortedArgs.Add(  'KC-API-TIMESTAMP', $timestamp )
        $sortedArgs.Add(  'KC-API-KEY', $kucoin_api )
        $sortedArgs.Add(  'KC-API-PASSPHRASE', $passSignature )
        $sortedArgs.Add(  'KC-API-KEY-VERSION', "2" )
        $sortedArgs.Add(  'Content-Type',  "application/json" )

        if ($post){
            $global:res = Invoke-RestMethod -Uri $endpoint -Method POST -Headers $sortedArgs -Body $post
        }
        else {
            $global:res = Invoke-RestMethod -Uri $endpoint -Method GET -Headers $sortedArgs
        }

        if ($raw){
            $res
        }
        else{
            $res.data 
        }    
    }

    
    $global:KafarFunctions =  $KafarCmdlets.Name 

    $KafarCmdlets | Foreach-Object { Set-Alias -Name $_.Alias -Value $_.Name -Scope Global -Force }

    Export-ModuleMember -Function $KafarFunctions 
} 
