
New-Module -Name PBTools -Scriptblock {

[array]$global:PBToolsCmdlets = @(

    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoHistory"
        Alias         = "goh"
        Description   = "Get history of symbol on Osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "bad"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoGainers"
        Alias         = "gog"
        Description   = "Get symbols that gained on Osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "bad"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoLosers"
        Alias         = "golo"
        Description   = "Get symbols that lost on Osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "bad"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoVolume"
        Alias         = "gov"
        Description   = "Get volume for symbol on Osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "bad"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-CosmosApi"
        Alias         = "gca"
        Description   = "Get API endpoint on Cosmos"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "bad"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoApi"
        Alias         = "goa"
        Description   = "Get API endpoint on Osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "bad"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoLiquidity"
        Alias         = "gol"
        Description   = "Get liquidity for symbol on Osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "bad"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoPair"
        Alias         = "gopair"
        Description   = "Get pair on Osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "bad"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoPool"
        Alias         = "gop"
        Description   = "Get pool on Osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "bad"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoSymbol"
        Alias         = "gos"
        Description   = "Get symbol on Osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "good"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoPoolFees"
        Alias         = "gopf"
        Description   = "Get fees for a pool on Osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "good"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoSymbol"
        Alias         = "gos"
        Description   = "Get Osmosis symbol using denom"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "good"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoDenom"
        Alias         = "god"
        Description   = "Get Osmosis denom using symbol"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "good"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-KeplrBalance"
        Alias         = "gkb"
        Description   = "Get balance with Keplr"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "good"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-KeplrStaking"
        Alias         = "gks"
        Description   = "Get staked coins with Keplr"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "bad"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoPoolApr"
        Alias         = "gopa"
        Description   = "Get APR for Osmosis pool"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "bad"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoDominance"
        Alias         = "god"
        Description   = "Get token dominance on Osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "good"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoPrice"
        Alias         = "gopr"
        Description   = "Get token price on Osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Status        = "bad"
        Help            = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
      Name          = "Get-KuCoinAPI"
      Alias         = "ka"
      Description   = "Use KuCoin API"
      Category      = "a"
      Mod           = "PBTools"
      Help        = @'
      sub/user                   user info of all sub-users
      sub-accounts               list of sub-accounts
      sub-accounts/{subUserId}   sub-abrount details
      accounts                   list of accounts
      accounts/{id}              abrount details
      accounts/transferable      abrount transferable balance
      accounts/sub-transfer      master and sub-abrount transfers
      accounts/inner-transfer    inner transfer
      accounts/ledgers           history of withdrawal/deposits
      deposit-address
      deposit-addresses
      deposits
      hist-deposits
      withdrawals
      withdrawals/quotas
      hist-withdrawals
      base-fee                   basic fee rate of user 
      trade-fees                 actdual fee rate of trading pair 
      orders
      margin/order
      orders/multi
      orders/{orderId}
      order/client-order/{clientOid}
      hist-orders
      limit/orders
      fills
      limit/fills
      stop-order
      stop-order/{orderId}
      stop-order/cancel
      stop-order/queryOrderByClientOid
      stop-order/cancelOrderByClientOid
      symbols
      market/orderbook/level1?symbol=BTC-USDT
      market/allTickers
      market/stats
      market/orderbook/level2_20?symbol=BTC-USDT
      markets
      market/histories
      market/candles
      currencies
      currencies/BTC
      prices
      mark-price/USDT-BTC/current
      margin/config
      margin/abrount
      margin/borrow
      margin/borrow/outstanding
      margin/borrow/repaid
      margin/repay/all
      margin/repay/single 
      margin/lend
      margin/lend/{orderId}
      margin/toggle-auto-lend
      margin/lend/active
      margin/lend/done
      margin/lend/trade/unsettled
      margin/lend/traade/settled
      margin/lend/assets
      margin/market
      margin/trade/last
      timestamp
      status
      bullet-public
      bullet-private

'@
    }),
    $(New-Object -TypeName psobject -Property @{
      Name          = "Get-KuCoinTicker"
      Alias         = "kt"
      Description   = "Get KuCoin ticker"
      Category      = "c"
      Mod           = "PBTools"
      Help        = @'
'@
    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-KuCoinCurrency"
        Alias         = "kc"
        Description   = "Get KuCoin currency"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-KuCoinCoin"
        Alias         = "kk"
        Description   = "Get KuCoin coin"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-KuCoinMarketList"
        Alias         = "km"
        Description   = "List KuCoin markets"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-KuCoinSymbolStats"
        Alias         = "ks"
        Description   = "Get KuCoin symbol stats"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-KuCoinTradeHistory"
        Alias         = "kh"
        Description   = "List KuCoin trade history"
        Category      = "d"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-KuCoinPrice"
        Alias         = "kp"
        Description   = "List KuCoin price"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-KuCoinOrder"
        Alias         = "ko"
        Description   = "List KuCoin order"
        Category      = "d"
        Mod           = "PBTools"
        Help        = @'
'@
    }), 

    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-PBToolsCmdlets"
        Alias         = "gpt"
        Description   = "Get PBTools cmdlets"
        Category      = "u"
        Mod           = "PBTools"
        Help        = @'
'@
  }),
  $(New-Object -TypeName psobject -Property @{
    Name          = "Get-DealsFromCsv"
    Alias         = "gd"
    Description   = "Get deals from CSV"
    Category      = "d"
    Mod           = "PBTools"
    Help        = @'
'@
}),
    $(New-Object -TypeName psobject -Property @{
      Name          = "Get-PBTools"
      Alias         = "pt"
      Description   = "Get PBTools"
      Category      = "u"
      Mod           = "PBTools"
      Help        = @'
'@
    }),
    
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-CoinMarketCapAPI"
        Alias         = "ca"
        Description   = "Get CMC API"
        Category      = "a"
        Mod           = "PBTools"
        Help        = @'
/v1/cryptocurrency/map                  - CoinMarketCap ID map
/v1/cryptocurrency/info                 - Metadata
/v1/cryptocurrency/listings/latest      - Latest listings
/v1/cryptocurrency/listings/historical  - Historical listings
/v1/cryptocurrency/quotes/latest        - Latest quotes
/v1/cryptocurrency/quotes/historical    - Historical quotes
/v1/cryptocurrency/market-pairs/latest  - Latest market pairs
/v1/cryptocurrency/ohlcv/latest         - Latest OHLCV
/v1/cryptocurrency/ohlcv/historical     - Historical OHLCV
/v1/cryptocurrency/price-performance-stats/latest - Price performance Stats
/v1/cryptocurrency/categories           - Categories
/v1/cryptocurrency/category             - Category
/v1/cryptocurrency/airdrops             - Airdrops
/v1/cryptocurrency/airdrop              - Airdrop
/v1/cryptocurrency/trending/latest      - Trending Latest
/v1/cryptocurrency/trending/most-visited - Trending Most Visited
/v1/cryptocurrency/trending/gainers-losers - Trending Gainers & Losers
'@
    }), 
        
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-CoinMarketCapSymbol"
        Alias         = "cs"
        Description   = "Get CMC symbol"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-CoinMarketCapListings"
        Alias         = "cl"
        Description   = "Get new coin listings"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-CoinMarketCapQuote"
        Alias         = "cq"
        Description   = "Get CMC quote"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-CoinQuoteSymbol"
        Alias         = "cqs"
        Description   = "Get coin, quote, and symbol"
        Category      = "u"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-Formatting"
        Alias         = "gf"
        Description   = "Get percents,prices,numbers formatted correctly"
        Category      = "u"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-Coin"
        Alias         = "coin"
        Description   = "Get coin details"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }), 
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-CoinPriceHistory"
        Alias         = "history"
        Description   = "Get coin price history"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-CoinVolume"
        Alias         = "volume"
        Description   = "Get volumes of coins"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-SignatureWithKey"
        Alias         = "gsk"
        Description   = "Get HMAC256 signature with key"
        Category      = "u"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-CoinStats"
        Alias         = "stats"
        Description   = "Get stats for a coin"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OsmoRewards"
        Alias         = "gor"
        Description   = "Get rewards for coin on osmosis"
        Category      = "o"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-ApeBoard"
        Alias         = "ape"
        Description   = "Get coin balance using ApeBoard"
        Category      = "c"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-BoostedPools"
        Alias         = "gbp"
        Description   = "Get SOLID boosted pools"
        Category      = "s"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-PriceCheck"
        Alias         = "gpc"
        Description   = "Get SOLID price check for FTM, OXD, RDL, etc"
        Category      = "s"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-Yield"
        Alias         = "gy"
        Description   = "Get yield"
        Category      = "s"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-Yield2"
        Alias         = "gy2"
        Description   = "Get yield 2"
        Category      = "s"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-Oxd"
        Alias         = "goxd"
        Description   = "Get OXD"
        Category      = "s"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-OxdPools"
        Alias         = "gxp"
        Description   = "Get OXD pools"
        Category      = "s"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-SafetyOrders"
        Alias         = "gso"
        Description   = "Get safety orders"
        Category      = "x"
        Mod           = "PBTools"
        Status        = "bad"
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-Icon"
        Alias         = "gic"
        Description   = "Get icon image path for a token"
        Category      = "s"
        Mod           = "PBTools"
        Help        = @'
'@
    }),
    $(New-Object -TypeName psobject -Property @{
        Name          = "Get-EthWalletHash"
        Alias         = "gewh"
        Description   = "Get hash for ETH wallet"
        Category      = "x"
        Mod           = "PBTools"
        Status        = "bad"
    })
)


[array]$global:strategies = @(
   $(New-Object -TypeName psobject -Property @{
      Name           = "BB & Stochastics"
      Type           = "Long Term Swing Trading"
      SubType        = "Finds bottom reversals"
      Id             = 9072            
      Description    = @"
Uses BB to find volatility, and the Stochastics with Region Crossovers to determine when the price is starting to recover.
"@ 
      Settings       = @"
Take Profit 17%
Stop Loss 5.5%
Trailing Stop loss percentage 3% arming 7%
"@ 
   }),
   $(New-Object -TypeName psobject -Property @{
      Name           = "EMA Crossovers"
      Type           = "Long Term Swing Trading"
      SubType        = "-"
      Id             = 9167              
      Description    = @"
Uses EMA crossover on the 4h chart. Good for coins that keep trend for longer periods of time, not coins moving quickly up or down such as FTT, and BNB. 
"@
      Settings       = "Use `"Sell Based on Strategy`" option"
   }),
   $(New-Object -TypeName psobject -Property @{
      Name           = "BB & Stochastics"
      Type           = "Short Term Swing Trading"
      SubType        = "Finds bottom of reversals"
      Id             = 9073              
      Description    = @"
Uses the Bollinger Bands to identify volatility, and the Stochastics with Region Crossovers to determine when the price is starting to recover
"@
      Settings       = @"
Take Profit 9%
Stop Loss 3.5%
Trailing Stop loss percentage 1.5% arming 3%
"@
   }),
   $(New-Object -TypeName psobject -Property @{
      Name           = "ADX & Stochastics"
      Type           = "Momentum + Volatility Scalping"
      SubType        = "Finds bottom of uptrending markets"
      Id             = 9057             
      Description    = @"
Uses the MESA on the Daily chart to identify a positive trend. Then uses the ADX to filter out signals in a ranging market, and the Stochastics with region crossovers to find the bottoms of the trend.

If you intend to use this strategy with the New AI, then it is recommended to remove the MESA filter. If you would like to use it as it is, then it is better to keep the filter (or add a new one)
"@
      Settings       = @"
Take Profit 5%
Stop Loss 1%
Trailing Stop loss percentage 1% arming 1.5%
"@
   }),
   $(New-Object -TypeName psobject -Property @{
      Name           = "MESA x Williams %R"
      Type           = "Short Term Swing Trading"
      SubType        = "-"
      Id             = 9062             
      Description    = @"
Uses the MESA to identify the trend, and then Williams %R on the same timeframe to buy the dips. 
"@
      Settings       = @"
Take Profit 9%
Stop Loss 3.5%
Trailing Stop loss percentage 1.5% arming 3%
"@
   }),
   $(New-Object -TypeName psobject -Property @{
      Name           = "Parabolic SAR x ADX"
      Type           = "Trend + Volatility Scalping"
      Id             = 9067             
      Description    = @"
Uses the Parabolic SAR to open up positions, and the ADX to act as a filter for the Parabolic SAR
"@
      Settings       = @"
Set up only 1 position per coin in the baseconfig
Set up a cooldown period of around 25-50 minutes
Take Profit 5%
Stop Loss 1%
Trailing Stop loss percentage 1% arming 1.5%
"@
   })
)

# Utils
function Get-Icon {
    param(
        [string]$symbol
    )
    $icons.Item($symbol)
}
function Get-PBTools {
  param(
    [string]$type = 'default'
  )

  Switch ($type) {

    'b'          {  Get-PBToolsCmdlets 'b'       }
    'c'          {  Get-PBToolsCmdlets 'c'      }
    'd'          {  Get-PBToolsCmdlets 'd'      }    
    'a'          {  Get-PBToolsCmdlets 'a'      }
    'u'          {  Get-PBToolsCmdlets 'u'      }
    'o'          {  Get-PBToolsCmdlets 'o'      }
    's'          {  Get-PBToolsCmdlets 's'      }
    'x'          {  Get-PBToolsCmdlets 'x'      }
    default        {
      
      Write-Host "`n"
        Write-Host -Fore DarkCyan " ...:::" -NoNewLine;Write-Host "`tUsage:   " -NoNewLine;
            Write-Host -Fore DarkCyan "pt " -NoNewLine;Write-Host -Fore DarkGray "[category]"
      Write-Host "`n"

      [pscustomobject]$options = [ordered]@{
        'b'       = "Bots `t- 3Commas API to interact with trading bots"
        'c'       = "Coins  `t- KuCoin and CoinMarketCap API to get info on coins"
        'd'       = "Deals `t- KuCoin deals past and present"
        'a'       = "API `t- Interact with API endpoints"
        'u'       = "Utils `t- Module utilities"
        'o'       = "Osmosis `t- Osmosis chain utilities"
        's'       = "Solidly `t- Solidly project utilities"
        'x'       = "Bybit and 3Commas functions"
      }

      $options.Keys | Foreach-Object {
        Write-Host -Fore DarkCyan "`tpt " -NoNewLine;
        Write-Host -Fore DarkGray "$_" -NoNewLine ; Write-Host "`t`t$($options.Item("$_"))"
      }
      Write-Host "`n"
    }
  }
}
function Get-PBToolsCmdlets {
  param(
    [string]$type
  )

  if (!$type) {
    $cmdlets = $PBToolsCmdlets | Sort-Object Name
  }
  else {
    $cmdlets = $PBToolsCmdlets | Where-Object Category -eq $type | Sort-Object Name
  }
  Write-Host "`n"
  Write-Host -Fore DarkGray ("`tCmdlet`t`t`t`t") -NoNewLine;Write-Host -Fore DarkGray " Alias`t`t" -Nonewline;
      Write-Host -Fore DarkGray " Description" 
  Write-Host "`t$('-' * 24)`t $('-' * 5)`t`t $('-' * 30)"
  $cmdlets | ForEach-Object {
    if ($_.Name.Length -gt 23){
      Write-Host -Fore Gray ("`t" + $_.Name + "`t ") -NoNewLine;Write-Host -Fore DarkCyan $_.Alias -Nonewline
      Write-Host -Fore Gray (" `t`t " + $_.Description ) 
    }    
    elseif ($_.Name.Length -gt 15){
      Write-Host -Fore Gray ("`t" + $_.Name + "`t`t ") -NoNewLine;Write-Host -Fore DarkCyan $_.Alias -Nonewline
      Write-Host -Fore Gray (" `t`t " + $_.Description ) 
    }
    elseif ($_.Name.Length -gt 7){
      Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t ") -NoNewLine;Write-Host -Fore DarkCyan $_.Alias -Nonewline
      Write-Host -Fore Gray (" `t`t " + $_.Description ) 
    }
    else {
      Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t`t ") -NoNewLine;Write-Host -Fore DarkCyan $_.Alias -Nonewline
      Write-Host -Fore Gray (" `t`t " + $_.Description ) 
    }
  }
  Write-Host "`n"
}  
function Get-CoinQuoteSymbol {
    param(
        [string]$coin,
        [string]$quote,
        [string]$symbol
    )
    if ($coin -and !$quote){
        return $coin.ToUpper(),'USDT',($coin + '-USDT').ToUpper()
    }
    if ($coin -and $quote){
        return $coin.ToUpper(),$quote.ToUpper(),($coin + '-' + $quote).ToUpper()
    }
    if ($symbol){
        $coin       = ($symbol.split('-')[0]).ToUpper()
        $quote      = ($symbol.split('-')[1]).ToUpper() 
        return $coin,$quote,$symbol.ToUpper()
    }
}
function ConvertFrom-UnixTime {
    param(
        $unixTime
    )
    $origin = New-Object -Type DateTime -ArgumentList 1970, 1, 1, 0, 0, 0, 0
    $obj = $origin.AddSeconds([int]$unixTime)
    $obj -f "{0:hh\:mm\:ss}"
}
function Get-Formatting {
    param(
        $price,
        $percent,
        $number,
        $time
    )

    if ($price){
        if ($price.getType().Name -ne 'Double'){
            $price = [Convert]::ToDouble($price)
        }
        if ($price -lt 1){
            return "{0:C4}" -f $price
        }
        else {
            return "{0:C}" -f $price
        }
    }
    elseif ($percent){
        if ($percent.getType().Name -ne 'Double'){
            $percent = [Convert]::ToDouble($percent)
        }
        if ($percent -lt 1){
            return "{0:P2}" -f $percent
        }
        else {
            return "{0:P}" -f $percent
        } 
    }    
    elseif ($number){
        if ($number.getType().Name -ne 'Double'){
            $number = [Convert]::ToDouble($number)
        }
        if ($number -lt 1){
            return "{0:N2}" -f $number
        }
        else {
            return "{0:N}" -f $number
        }
    }
    elseif ($time){
        (Get-Date) -f "{0:hh\:mm\:ss}"
    }
    else{}    
        
}
function Get-SignatureWithKey {
    param(
        [string]$secret,
        [string]$message
    )

    $hmacsha = New-Object System.Security.Cryptography.HMACSHA256
    $hmacsha.key = [Text.Encoding]::ASCII.GetBytes($secret)
    $signature = $hmacsha.ComputeHash([Text.Encoding]::ASCII.GetBytes($message))
    $signature = [Convert]::ToBase64String($signature)
    return $signature



    # $hmac                   = New-Object System.Security.Cryptography.HMACSHA256
    # $hmac.key               = [Text.Encoding]::UTF8.GetBytes($key)
    # $signed_data            = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($toSign)) 
    # $encoded_data           = [Convert]::ToBase64String($signed_data)
    # return $encoded_data
}

# KuCoin 
function Get-KuCoinApi {

    param(
        $endpoint,
        $method  = "GET",
        $postdata
    )

    $global:kucoin_pb       = Get-Cred kucoin_pb
    $global:kucoin_pb_sec   = Get-Cred kucoin_pb_sec
    $global:passphrase      = Get-Cred passphrase


    $base_url               = 'https://api.kucoin.com/api/v1/'
    $global:now             = [DateTimeOffset]::UtcNow.ToUnixTimeMilliseconds()
    $global:url             = $base_url + $endpoint 

    if ($postdata){
        $global:str_to_sign     = $now.ToString() + $method + '/api/v1/' + $endpoint  + $postdata
    }
    else {
        $global:str_to_sign     = $now.ToString() + $method + '/api/v1/' + $endpoint
    }

    $hmac                   = New-Object System.Security.Cryptography.HMACSHA256
    $hmac.key               = [Text.Encoding]::UTF8.GetBytes($kucoin_pb_sec)
    $signed_data            = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($str_to_sign)) 
    $encoded_data           = [Convert]::ToBase64String($signed_data)
    
    $hmac                   = New-Object System.Security.Cryptography.HMACSHA256
    $hmac.key               = [Text.Encoding]::UTF8.GetBytes($kucoin_pb_sec)
    $signed_pass            = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($passphrase)) 
    $encoded_pass           = [Convert]::ToBase64String($signed_pass)
    


    $global:headers = @{
        'KC-API-KEY'            =  $kucoin_pb
        'KC-API-SIGN'           =  $encoded_data
        'KC-API-TIMESTAMP'      =  $now.ToString()
        'KC-API-PASSPHRASE'     =  $encoded_pass
        'KC-API-KEY-VERSION'    =  2
        'Content-Type'          = "application/json"
    }

    if ($postdata){
        $response = Invoke-RestMethod -Uri $url -Headers $headers -Method $method -Body $postdata
    }
    else {
        $response = Invoke-RestMethod -Uri $url -Headers $headers -Method $method
    }
    $response.data

}
function Get-KuCoinCoin {
    param(
        [string]$coin,
        [string]$quote,
        [string]$symbol
    )
    $coin,$quote,$symbol = Get-CoinQuoteSymbol -coin $coin -quote $quote -symbol $symbol
    Get-KuCoinAPI -endpoint "currencies/$coin"
}
function Get-KuCoinMarketList {
    $global:markets = Get-KuCoinAPI -endpoint "markets"
    Write-Success '            ','(',$tickers.count,')','Saved as $markets'
}
function Get-KuCoinTicker {
    param(
        [string]$coin,
        [string]$quote,
        [string]$symbol,
        [switch]$raw
    )
    if (!$tickers){
        $global:tickers = (Get-KuCoinAPI -endpoint "market/allTickers").ticker
        if (!$raw){
            Foreach ($obj in $tickers){
                New-Object -TypeName psobject -Property @{
                    coin            = ($obj.symbol -split '-')[0]
                    pair            = $obj.symbol 
                    buy             = Get-Formatting -price $obj.buy
                    sell            = Get-Formatting -price $obj.sell
                    changeRate      = Get-Formatting -percent $obj.changeRate
                    changePrice     = Get-Formatting -price $obj.changePrice 
                    high            = Get-Formatting -price $obj.high
                    low             = Get-Formatting -price $obj.low
                    volume          = Get-Formatting -number $obj.vol 
                    volumeValue     = Get-Formatting -number $obj.volValue 
                    lastPrice       = Get-Formatting -price $obj.last 
                    avgPrice        = Get-Formatting -price $obj.averagePrice
                    taker           = Get-Formatting -percent $obj.takerFeeRate
                    maker           = Get-Formatting -percent $obj.makerFeeRate
                }
            }
        }
    }

    $coin,$quote,$symbol = Get-CoinQuoteSymbol -coin $coin -quote $quote -symbol $symbol 

    if ($symbol){
        $tickers | Where-Object symbol -eq $symbol
    }
    else {
        Write-Success '            ','(',$tickers.count,')','Saved as $tickers'
    }

    $global:newTickers = [System.Collections.ArrayList]::New()
    $oldTickers = Import-CliXml -Path $env:USERPROFILE\tickers.xml 
    Foreach ($tick in $tickers){
        if ($_.symbol -notin $oldTickers.symbol){
            [void]$newTickers.Add($_)
        }
    }
    if ($newTickers.count -gt 0){
        Write-Success '            ','(',$newTickers.count,')','Saved as $newTickers'
        Write-Host "`n"
        $newTickers | Select-Object symbol,low,high,averagePrice | Format-Table -AutoSize 
    }
}
function Get-KuCoinSymbolStats {
    param(
        [string]$coin,
        [string]$quote,
        [string]$symbol,
        [switch]$raw
    )

    $coin, $quote, $symbol = Get-CoinQuoteSymbol -coin $coin -quote $quote -symbol $symbol 
    $global:resp = Get-KuCoinAPI -endpoint "market/stats?symbol=$symbol"

    if ($raw){
        $resp
    }
    else {
        Foreach ($obj in $resp){
            New-Object -TypeName psobject -Property @{
                time            = Get-Formatting -time $obj.time 
                coin            = ($obj.symbol -split '-')[0]
                pair            = $obj.symbol 
                buy             = Get-Formatting -price $obj.buy
                sell            = Get-Formatting -price $obj.sell
                changeRate      = Get-Formatting -percent $obj.changeRate
                changePrice     = Get-Formatting -price $obj.changePrice 
                high            = Get-Formatting -price $obj.high
                low             = Get-Formatting -price $obj.low
                volume          = Get-Formatting -number $obj.vol 
                volumeValue     = Get-Formatting -number $obj.volValue 
                lastPrice       = Get-Formatting -price $obj.last 
                avgPrice        = Get-Formatting -price $obj.averagePrice
                taker           = Get-Formatting -percent $obj.takerFeeRate
                maker           = Get-Formatting -percent $obj.makerFeeRate
            }
        }
    }
}
function Get-KuCoinTradeHistory {
    param(
        [Parameter(Mandatory=$true)]
        [string]$coin,
        [string]$quote,
        [string]$symbol,
        [switch]$buy,
        [switch]$sell
    )
    
    $coin,$quote,$symbol = Get-CoinQuoteSymbol -coin $coin -quote $quote -symbol $symbol
    $data = Get-KuCoinAPI -endpoint "market/histories?symbol=$symbol"

    ForEach ($d in $data){
        $d.time = (ConvertFrom-UnixTime $($d.time.ToString()[0..9] -join '')) -f "{0:hh\:mm\:ss}"
    }
    if ($buy){
        $data = $data | Where-Object side -eq 'buy'
    }
    if ($sell){
        $data = $data | Where-Object side -eq 'sell'
    }
    $data | Select-Object sequence,time,side,price,size | Sort-Object -Desc sequence | Format-Table -AutoSize 
}
function Get-KuCoinPrice {
    param(
        [array]$coins,
        [string]$base,
        [switch]$raw
    )

    if ($base){
        $base = $base.ToUpper()
        Get-KuCoinAPI -endpoint "prices?base=$base"
    }
    elseif ($coins){
        $currencies = [System.Collections.ArrayList]::New()
        Foreach ($coin in $coins){
            $coin,$quote,$symbol = Get-CoinQuoteSymbol -coin $coin -quote $quote -symbol $symbol
            [void]$currencies.Add($coin)
        }
        $currencies = $currencies -join ','
        $resp = Get-KuCoinAPI -endpoint "prices?currencies=$currencies"
        if ($raw) {
            $resp
        }
        else {
            $names = ($resp | Get-Member -MemberType NoteProperty).Name
            Foreach ($name in $names){
                New-Object -TypeName psobject -Property @{
                    time        =  Get-Formatting -time $(Get-Date)
                    coin        =  $name 
                    price       =  Get-Formatting -price $resp.$name 
                }
            }
        }
    }
    else {
        Get-KuCoinAPI -endpoint "prices"
    }
}
function Get-KuCoinOrder {
    param(
        # [string]$clientOid,
        [string]$coin,
        [string]$quote,
        [string]$symbol,
        [switch]$buy,
        [switch]$sell,
        [switch]$raw

    )
    $coin,$quote,$symbol = Get-CoinQuoteSymbol -coin $coin -quote $quote -symbol $symbol
    $global:orders = (Get-KuCoinAPI -endpoint "orders").items

    if ($raw){
        $orders
    }
    else {
        ForEach ($order in $orders){
            $order.createdAt = (ConvertFrom-UnixTime $($order.createdAt.ToString()[0..9] -join '')) -f "{0:hh\:mm\:ss}"
        }

        if ($buy){
            $out = $orders | Where-Object side -eq 'buy'
        }
        elseif ($sell) {
            $out = $orders | Where-Object side -eq 'sell' 
        }
        else {
            $out = $orders
        }

        if ($coin){
            $symbol = $coin + '-' + $quote
            $symbol = $symbol.ToUpper()
            $out = $out | Where-Object symbol -match $symbol
        }
        
        if ($raw) {
            $out 
        }
        else {
            $out | Select-Object createdAt,symbol,side,size,price,fee,clientOid | Sort-Object symbol,side |Format-Table -Auto
        }
    }
}
# Cap 
function Get-CoinMarketCapAPI {
    param(
        [string]$endpoint
    )
    $cmc_api_key = Get-Cred coinmarketcap
    $headers = @{
        'Content-Type'      = "application/json"
        'X-CMC_PRO_API_KEY' = $cmc_api_key
    } 
    $uri = "https://pro-api.coinmarketcap.com/v1/$endpoint"
    $resp = Invoke-RestMethod -Uri $uri -Headers $headers 
    $resp.data
}
function Get-CoinMarketCapSymbol {
    param(
        [string]$coin,
        [string]$quote,
        [string]$symbol,
        [switch]$raw
    )

    $coin, $quote, $symbol = Get-CoinQuoteSymbol -coin $coin -quote $quote -symbol $symbol 
    $global:resp = Get-CoinMarketCapAPI -endpoint "cryptocurrency/info?symbol=$coin"
    $global:out = $resp | Select-Object -exp $coin
    $out
}
function Get-CoinMarketCapListings {
    param(
        [string]$platform = "Ethereum"
    )
    
    $out = [System.Collections.ArrayList]::New()
    $global:map = (Get-CoinMarketCapAPI -endpoint "cryptocurrency/map").data

    if ($platform){
      $map = $map | Where-Object { $_.platform.name  -eq $platform }
    }
    if ($raw){
        $map
    }
    else {
        Foreach ($c in $map){
            $p = $c.platform

            $obj = New-Object -TypeName psobject -Property @{
                platform    = $p.name 
                id          = $c.id
                name        = $c.name
                rank        = $c.rank
                symbol      = $c.symbol 
                firstData   = $c.first_historical_data
                lastData    = $c.last_historical_data
            }
            [void]$out.Add($obj)
        }
        $out | Sort-Object -Desc firstData | Select-Object symbol,name,platform,firstData | Select-Object -First 10
    }
}
function Get-CoinMarketCapQuote {
    param(
        [string]$coin,
        [string]$quote,
        [string]$symbol,
        [switch]$raw
    )
    $coin,$quote,$symbol = Get-CoinQuoteSymbol -coin $coin -quote $quote -symbol $symbol
    if ($symbol){
        $resp = Get-CoinMarketCapAPI -endpoint "cryptocurrency/quotes/latest?symbol=$symbol"
    }
    else {
        $resp = Get-CoinMarketCapAPI -endpoint "cryptocurrency/quotes/latest"
    }

    if ($raw){
        $resp
    }
    else {
        $global:obj    = $resp.data | Select-Object -exp $symbol 
        $USD             = $obj.quote.USD
    
        New-Object -TypeName psobject -Property @{
            id              = $obj.id 
            date_added      = $obj.date_added 
            pairs           = $obj.num_market_pairs
            tags            = $obj.tags 
            max_supply      = $obj.max_supply
            cir_supply      = $obj.circulating_supply
            tot_supply      = $obj.total_supply
            platform        = $obj.platform 
            rank            = $obj.cmc_rank
            quote           = $obj.quote 
            last_updated    = $obj.last_updated 
            name            = $obj.name
            coin            = $obj.symbol 
            price           = "{0:C}" -f $USD.price
            vol_24h         = "{0:P2}" -f $USD.volume_24h       
            vol_chg_24h     = "{0:P2}" -f $USD.volume_change_24h
            per_chg_1h      = "{0:P2}" -f $USD.percent_change_1h 
            per_chg_24h     = "{0:P2}" -f $USD.percent_change_24h 
            per_chg_7d      = "{0:P2}" -f $USD.percent_change_7d 
            per_chg_30d     = "{0:P2}" -f $USD.percent_change_30d 
            per_chg_60d     = "{0:P2}" -f $USD.percent_change_60d 
            per_chg_90d     = "{0:P2}" -f $USD.percent_change_90d 
            mktcap          = '{0:C}' -f $USD.market_cap  
            mktcap_dominance= $USD.market_cap_dominance 
            fd_mktcap       = $USD.fully_diluted_market_cap 
        } 
    }
}

function Get-3commasApi {

    param(
        [string]$endpoint,
        [string]$method         = "GET",
        [string]$postdata
    )
    $global:response         = ''
    $global:3commas_pb       = Get-Cred 3commas_pb
    $global:3commas_pb_sec   = Get-Cred 3commas_pb_sec
    $global:baseuri          = "https://api.3commas.io"
    $global:toSign           = "/public/api/ver1/$endpoint"

    $global:signature = Get-SignatureWithKey -message $toSign -secret $3commas_pb_sec
    
    $global:headers = @{
        'APIKEY'         = $3commas_pb
        'Signature'      = $signature
        'Content-Type'   = 'application/json'
    }
    $global:newUri = "$baseuri$toSign"
    $global:response = Invoke-RestMethod -Uri $newUri -Headers $headers -Method $method
    if ($response) {
        $response | Select-Object -First 10
    }
    else {
        Write-Fail 'No response'
    }
}

function Write-CoinProfile {
    param(
        [string]$symbol
    )

    $kk  = kk $symbol
    $out = @"
    {
      "contractAddress": "$($kk.contractAddress)",
      "avatar": "/static/mock-images/avatars/$($kk.name.ToLower()).png",
      "symbol": "$($kk.name)",
      "platform": "none",
      "name": "$($kk.fullname)",
      "cover": "xxx"
    },
"@
    $out | Set-Clipboard 
    Write-Host $out 
    Write-Host "`n"
    Write-Success 'Copied to clipboard'
}

# Custom
function Get-CoinVols {
    param(
        [string]$coin,
        [string]$quote,
        [string]$symbol
    )

    $coin,$quote,$symbol = Get-CoinQuoteSymbol -coin $coin -quote $quote -symbol $symbol 
    if (!$tickers) {
        Get-KuCoinTicker 
    }
    if (!$vols){
        $global:vols = $tickers.symbol | Foreach-Object { 
            Get-KuCoinSymbolStats -symbol $_ 
        }
    }
    if (!$ranges){
        $global:ranges = $vols | ForEach-Object {
            $_.low  = Get-Formatting -price  $_.low 
            $_.high = Get-Formatting -price $_.high
            $perc  = Get-Formatting -percent ($range / $_.averagePrice)
            $_.averagePrice  = Get-Formatting -price $_.averagePrice 
            $range = Get-Formatting -price ($_.high - $_.low)
 
            $cmc    = Get-CoinMarketCapSymbol -symbol $symbol
            $added  = $cmc.date_added
            $name   = $cmc.name 

            New-Object -TypeName psobject -Property @{ 
                symbol      = $_.symbol
                avg_price   = $_.averagePrice
                range       = $range
                percentage  = $perc
                low         = $_.low
                high        = $_.high
                volume      = $_.vol 
                lastPrice   = $_.last
                changeRate  = $_.changeRate 
                dateAdded   = $added 
                name        = $name
            } 
        }
    }

    $ranges | Where-Object percentage -lt 999999999 
}
function Get-DealsFromCsv {
    param(
        [switch]$volatility,
        [switch]$days,
        [switch]$today,
        [switch]$roi,
        [switch]$raw
    )

    $global:d = [System.Collections.ArrayList]::New()
    $global:content = Get-Content -Path $env:USERPROFILE/csvs/*.csv -Raw | ConvertFrom-Csv -Delimiter ';'
    if ($raw){
        $global:d     = $content  
        Write-Success '      $d','created'
        Write-Info '      Total number of deals:', $d.count
    }
    else {

        $content = $content | Sort-Object -Unique deal_id | Where-Object {$_.duration -ne 'duration'}
        Foreach ($deal in $content){

            $ts =  [timespan]::fromseconds($deal.duration)
            $duration = ("{0:hh\:mm\:ss}" -f $ts)

            $pair = $deal.pair -replace '-','/'

            $obj = New-Object -TypeName psobject -Property @{
            bo          = [math]::Round($deal.base_order_volume,2)
            # bo_type     = $deal.base_order_volume_type -replace '_currency','' 
            bot         = $deal.bot
            ss          = "{0:P2}" -f $deal.martingale_step_coefficient
            os          = "{0:P2}" -f $deal.martingale_volume_coefficient
            buy_amt     = "{0:N}" -f $deal.bought_amount
            buy_vol     = "{0:N}" -f $deal.bought_volume
            closed_at   = $deal.closed_at
            close_date  = $deal.close_date
            # created     = $deal.created_at
            duration    = $duration
            profit      = "{0:C}" -f $deal.final_profit
            max_so      = "{0:N}" -f $deal.max_safety_orders_count 
            pair        = $pair
            profit_base = "{0:P2}" -f $deal.profit_percentage_from_base_volume
            profit_total= "{0:N}" -f $deal.profit_percentage_from_total_volume
            sos         = "{0:P2}" -f $deal.safety_order_step_percentage
            so          = "{0:N}" -f $deal.safety_order_volume
            'so_vol / bo_vol' = $deal.'safety_order_volume / base_order_volume'
            # so_vol_type = $deal.safety_order_volume_type -replace '_currency','' 
            sold_amt    = "{0:N}" -f $deal.sold_amount
            sold_vol    = "{0:N}" -f $deal.sold_volume
            status      = $deal.status 
            tp          = "{0:P2}" -f $deal.take_profit_condition
            # tp_type     = $deal.take_profit_type -replace '_volume','' 
            used_so     = $deal.used_safety_orders 

            }
            [void]$d.Add($obj)

        }

        if ($volatility){
            
        }
        elseif ($days){
            $counts = $content | Foreach-Object { 
                New-Object -TypeName psobject -Property @{
                    pair = $_.pair
                    days = (($d | Where-Object pair -eq $_.pair ).close_date | Sort-Object -Unique ).count
                }
            }
            $counts | Sort-Object -Unique pair | Sort-Object -desc days | Select-Object -first 25
        }
        elseif ($today){
            $d | Where-Object 
        }
        elseif ($roi){
            $objs = [System.Collections.ArrayList]::New()
            Foreach ($deal in $content) { 
                try {
                    $soVol = [math]::round($deal.safety_order_volume,0)
                    $totSo = $soVol * $deal.used_safety_orders
                    $toInv = $totSo + [math]::round($deal.base_order_volume,0)
                    $finPr = [math]::round($deal.final_profit,2)
                    $return = [math]::round(( $finPr / $toInv * 100 ),2)
                }
                catch {
                    $return = '-'
                }

                $obj = New-Object -TypeName psobject -Property @{
                    deal_id     = $deal.deal_id
                    roi         = $return
                    totInv      = $toInv     
                    bo          = [math]::round($deal.base_order_volume,0)
                    so          = [math]::round($deal.safety_order_volume,0)
                    tp          = [math]::round($deal.take_profit_condition,1)
                    uso         = $deal.used_safety_orders
                    fp          = [math]::round($deal.final_profit,2)
                    coin        = $deal.pair
                    sos         = $deal.safety_order_step_percentage
                    created_at  = $deal.created_at
                    closed_at   = $deal.closed_at
                } 
                [void]$objs.Add($obj)
            }
            $objs | Select-Object coin,created_at,closed_at,bo,so,sos,uso,tp,totInv,fp,roi | Sort -Desc roi 
        } 
        else {
            Write-Success '      $d','created'
            Write-Info '      Total number of deals:', $d.count

            $totalProfit = [Double]::New()
            $d | ForEach-Object { $totalProfit += $_.profit}


            $zeroProfit = $d | Where-Object profit -lt 1 
            $lowProfit = $d  | Where-Object profit -gt 0  | Where-Object profit -lt 10 
            $medProfit = $d  | Where-Object profit -ge 10 | Where-Object profit -le 20 
            $highProfit = $d | Where-Object profit -gt 20 

            New-Object -TypeName psobject -Property @{
                'Zero Profit Deals'             = $zeroProfit.Count
                '$ 0-10  Profit Deals'          = $lowProfit.count
                '$ 10-20 Profit Deals'          = $medProfit.count
                '$ 20 + Profit'                 = $highProfit.count
                'Total Profit'                  = $totalProfit 
            }           
        }
        
    }
}
function Get-CoinPriceHistory {
    param(
        [string]$coin,
        [string]$quote,
        [string]$symbol,
        [switch]$q
    )
    $xmlPath = "$env:USERPROFILE\$coin.xml"
    $coin,$quote,$symbol = Get-CoinQuoteSymbol -coin $coin -quote $quote -symbol $symbol 
    $started = Test-Path $xmlPath
    if ($started){
        [array]$tracker = Import-CliXml -Path $xmlPath  
        [array]$current = Get-KuCoinPrice -coin $coin 
        $tracker += $current 
        if (!$q){
            $tracker | Select-Object time,coin,price | Format-Table -Auto 
        }
        $tracker | Export-CliXml -Path $xmlPath 
    }
    else {
        [array]$current = Get-KuCoinPrice -coin $coin 
        $current | Export-CliXml -Path $xmlPath 
        if (!$q) {
            $current | Select-Object time,coin,price | Format-Table -Auto 
        }
    }
}
function Get-Coin {
    param(
        [string]$coin,
        [string]$quote,
        [string]$symbol,
        [string]$raw
    )
    
    $kucoin = Get-KuCoinCoin -coin $coin
    $cmc    = Get-CoinMarketCapSymbol -coin $coin 

    $contract_addresses = [System.Collections.ArrayList]::New()
    Foreach ($c in $cmc.contract_address){
        $contract_address = "$($c.contract_address)    $($c.platform.Name) "
        [void]$contract_addresses.Add($contract_address)
    }
    $out = New-Object -TypeName psobject -Property @{
        contract_addresses          = $contract_addresses -join "`n"
        fullname                    = $kucoin.fullname 
        name                        = $kucoin.name
    }
    if ($raw){
        $out
    }
    else {
        $out | Format-Table -Auto -Wrap 
    }
}
function stats {
    param(
        [string]$coin,
        [string]$quote,
        [string]$symbol,
        [string]$raw
    )
    if ($raw){
        $kucoin = Get-KuCoinSymbolStats -coin $coin -raw 
    }
    else {
        $kucoin = Get-KuCoinSymbolStats -coin $coin 
    }
    $kucoin 
}
function hist {
    param(
        [string]$coin,
        [string]$quote,
        [string]$symbol,
        [string]$raw
    )
    if ($raw){
        $kucoin = Get-KuCoinSymbolStats -coin $coin -raw 
    }
    else {
        $kucoin = Get-KuCoinSymbolStats -coin $coin 
    }
    $kucoin 
}
function Get-CoinJsonList {

    $coins = kk   
    $global:json = $coins[0..9] | %{ Write-Host "      {
            contractAddress:   $($_.contractAddress),
            avatar:            `"$('/static/mock-images/avatars/' + $($_.fullName.ToLower()) + '.jpg')`",
            symbol:            `"$($_.name)`",
            name:              `"$($_.fullname)`",
        },"
    }
    Write-Success $json.count,'objects saved as $json'
}
function Get-ApeBoard {
    param(
        [string]$address, 
        [string]$solana, 
        # [string]$endpoint, = "wallet/terra"
        [switch]$kuji
    )
    $headers = @{
        passcode        = '5a102a34f60fa7ec9d643a8a0e72cab9'
        'ape-secret'    = 'U2FsdGVkX1/mMwcXP1tVTeVxm+AkSfrzpWbovzKmyHcc4c8/N7iy6/JmRqtuMj3BPp0/A3ihRa8dddyZWTBG/9j6unKTmv1Wcqnr9v8IrPPe2TaqXbuh+tRH80UwfFzqmzcXKEqy1k59Fg0ooy6wvg=='
    }
    if ($address){
        
        if ($kuji) {
            $uri = "https://api.apeboard.finance/kujiraTerra/$address"
            $res = Invoke-RestMethod -Uri $uri -Headers $headers
            if (!$raw) {
                New-Object -TypeName psobject -Property @{
                    farms           = $res.farms 
                    balance         = $res.bids.balance 
                    rewardsBalannce = $res.bids.rewards.balance 
                    tokensBalance   = $res.bids.tokens.balance 
                    bidPremium      = $res.bids.premium 
                }
            }
        }
    }
}

######  COSMOS 

function Get-KeplrBalance {
    
    param(
        [string]$symbol
    )

    if ($symbol) {
        [array]$symbols   = $symbol
    }
    else {
        $symbols   =  ($wallets.keplr | Get-Member | Where-Object  MemberType -eq 'NoteProperty').Name
    }

    $global:balances = [System.Collections.ArrayList]::New()
    foreach ($symbol in $symbols){
        $address = $wallets.keplr.$symbol.address
        $fullname = $wallets.keplr.$symbol.fullname 

        try {
            $amount = (Invoke-RestMethod "https://lcd-$fullname.keplr.app/bank/balances/$address" -TimeoutSec 5 ).result.amount
        
            switch ($symbol) {
                'cheq' { $amount = [System.Math]::Round( ( $amount / 1000000000000000000000 ), 2 ) }
                'rowan' { $amount = [System.Math]::Round( ( $amount / 1000000000000000000 ), 2 ) }
                default { $amount = [System.Math]::Round( ( $amount / 1000000 ), 2 ) }
            }
            $price = Get-OsmoPrice $symbol 
            [double]$total  =  [Math]::Round(( $price * $amount ), 2 )

            $obj = New-Object -TypeName psobject -Property @{
                symbol          = $symbol
                amount          = $amount
                price           = $price
                total           = $total
            }
            [void]$balances.Add($obj)
        }
        catch {
            $obj = New-Object -TypeName psobject -Property @{
                symbol          = $symbol
                balance         = "-" 
                amount          = "-"
                price           = "-"
                total           = "-"
            }
            [void]$balances.Add($obj)
        }
    }
    if ($o){
        $balances
    }
    else {
        $balances | Select-Object symbol,amount,price,total    
    }
}
function Get-KeplrStaking {
    param(
        [string]$symbol
    )

    $symbols = [System.Collections.ArrayList]::New()
    if (!$symbol){

        $props   =  $wallets.keplr | Get-Member | Where-Object  MemberType -eq 'NoteProperty'
        foreach ($prop in $props){
            $symbol = $prop.name
            [void]$symbols.Add($symbol)  
        }
    }
    else {
        $symbols += $symbol
    }
    
    foreach ($symbol in $symbols){
        $address = $wallets.keplr.$symbol.address
        $fullname = $wallets.keplr.$symbol.fullname 
        $delegations = (Invoke-RestMethod "https://lcd-$fullname.keplr.app/staking/delegators/$address/delegations").result.delegation
        
        foreach ($d in $delegations){

            $price = Get-OsmoPrice $symbol 
            [double]$shares =  [Math]::Round(($d.shares / 1000000 ),2 )
            [double]$total  =  [Math]::Round(( $price * $shares ), 2 )
            
            $d | Add-Member -NotePropertyName total -NotePropertyValue $total
            $d | Add-Member -NotePropertyName price -NotePropertyValue $price
            $d | Add-Member -NotePropertyName symbol -NotePropertyValue $symbol
        }
        if ($o){
            $delegations
        }
        else {
            $delegations | Select-Object symbol,shares,price,total    
        }
    }
}
function Get-CosmosApi {
    param(
        [string]$endpoint 
    )
    Invoke-RestMethod -Uri "https://api.cosmos.network/cosmos/$endpoint"
}
$global:ibcs = New-Object -TypeName psobject -Property @{
    'ibc/1480B8FD20AD5FCAE81EA87584D269547DD4D436843C1D20F15E00EB64743EF4' = "akt"
    'ibc/27394FB092D2ECCD56123C74F36E4C1F926001CEADA9CA97EA622B25F41E5EB2' = "atom"
    'ibc/F867AE2112EFE646EC71A25CD2DFABB8927126AC1E19F1BBF0FF693A4ECA05DE' = "band"
    'ibc/D805F1DA50D31B96E4282C1D4181EDDFB1A44A598BFF5666F4B43E4B8BEA95A5' = "bcna"
    'ibc/FE2CD1E6828EC0FAB8AF39BAC45BC25B965BA67CCBC50C13A14BD610B0D1E2C4' = "boot"
    'ibc/4E5444C35610CC76FC94E7F7886B93121175C28262DDFDDE6F84E82BF2425452' = "btsg"
    'ibc/7A08C6F11EF0F59EB841B9F788A87EC9F2361C7D9703157EC13D940DC53031FA' = "cheq"
    'ibc/EA3E1640F9B1532AB129A571203A0B9F789A7F14BB66E350DCBFA18E1A1931F0' = "cmdx"
    'ibc/E6931F78057F7CC5DA0FD6CEF82FF39373A6E0452BF1FD76910B93292CF356C1' = "cro"
    'ibc/346786EA82F41FE55FAD14BF69AD8BA9B36985406E43F3CB23E6C45A285A9593' = "darc"
    'ibc/307E5C96C8F60D1CBEE269A9A86C0834E1DB06F2B3788AE4F716EDB97A48B97D' = "dig"
    'ibc/EA4C0A9F72E2CEDF10D0E7A9A6A22954DB3444910DB5BE980DF59B05A46DAD1C' = "dsm"
    'ibc/9712DBB13B9631EDFA9BF61B55F1B2D290B2ADB67E3A4EB3A875F3B6081B3B84' = "dvpn"
    'ibc/5973C068568365FFF40DEDCF1A1CB7582B6116B731CD31A12231AE25E20B871F' = "eeur"
    'ibc/770D676475E63F10FF9AB47AFA994C6299B71EBE2CF69B7993414BDAFDA2732A' = "fet"
    'ibc/E97634A40119F1898989C2A23224ED83FDD0A57EA46B3A094E287288D1672B44' = "grav"
    'ibc/B9E0A1A524E98BB407D3CED8720EFEFD186002F90C1B1B7964811DD0CCC12228' = "huahua"
    'ibc/52B1AA623B34EB78FD767CEA69E8D7FA6C9CFE1FBF49C5406268FD325E2CC2AC' = "iov"
    'ibc/7C4D60AA95E5A7558B0A364860979CA34B7FF8AAF255B87AF9E879374470CEC0' = "iris"
    'ibc/F3FF7A84A73B62921538642F9797C423D2B4C4ACB3C7FCFFCE7F12AA69909C4B' = "ixo"
    'ibc/46B44899322F3CD854D2D46DEEF881958467CDD4B3B10086DA49296BBED94BED' = "juno"
    'ibc/204A582244FC241613DBB50B04D1D454116C58C4AF7866C186AA0D6EEAD42780' = "krt"
    'ibc/9989AD6CCA39D1131523DB0617B50F6442081162294B4795E26746292467B525' = "like"
    'ibc/8A34AF0C1943FD0DFCDE9ADBF0B2C9959C45E87E6088EA2FC6ADACD59261B8A2' = "lum"
    'ibc/0EF15DF2F02480ADE0BB6E85D9EBB5DAEA2836D3860E9F97F9AADE4F57A31AA0' = "luna"
    'ibc/3BCCC93AD5DF58D11A6F8A05FA8BC801CBA0BA61A981F57E91B8B598BF8061CB' = "med"
    'ibc/297C64CC42B5A8D8F82FE2EBE208A6FE8F94B86037FA28C4529A23701C228F7A' = "neta"
    'ibc/1DC495FCEFDA068A3820F903EDBD78B942FBD204D7E93D3BA2B432E9669D1A59' = "ngm"
    'uosmo' = "osmo"
    'ibc/1DCC8A6CB5689018431323953344A9F6CC4D0BFB261E88C9F7777372C10CD076' = "regen"
    'ibc/8318FD63C42203D16DDCAF49FE10E8590669B3219A3E87676AC9DA50722687FB' = "rowan"
    'ibc/0954E1C28EB7AF5B72D24F3BC2B47BBB2FDF91BDDFD57B74B99E133AED40972A' = "scrt"
    'ibc/9BBA9A1C257E971E38C1422780CE6F0B0686F0A3085E2D61118D904BFE0F5F5E' = "somm"
    'ibc/987C17B11ABC2B20019178ACE62929FE9840202CE79498E29FE8E5CB02B7C0A4' = "stars"
    'ustars' = "stars"
    'ibc/655BCEF3CDEBE32863FF281DBBE3B06160339E9897DC9C9C9821932A5F8BA6F8' = "tick"
    'ibc/67795E528DF67C5606FC20F824EA39A6EF55BA133F4DC79C90A8C47A0901E17C' = "umee"
    'ibc/BE1BB42D4BE3C30D50B68D7C41DB4DFCE9678E8EF8C539F6E6A9345048894FCC' = "ust"
    'ibc/E7B35499CFBEB0FF5778127ABA4FB2C4B79A6B8D3D831D4379C4048C238796BD' = "vdl"
    'ibc/B547DC9B897E7C3AA5B824696110B8E3D2C31E3ED3F02FF363DCBAD82457E07E' = "xki"
    'ibc/A0CC0CF735BFB30E730C70019D4218A1244FF383503FF7579C9201AB93CA9293' = "xprt"
}
$global:assets = New-Object -TypeName psobject -Property @{
    osmo = New-Object -TypeName psobject -Property @{
        symbol = "OSMO"
        ibc    = "uosmo"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/osmo.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/osmo.svg"
       address  = ""
        desc   = "The native token of Osmosis"
        name   = "Osmosis"
   }
   ion = New-Object -TypeName psobject -Property @{
        symbol = "ION"
        ibc    = "uion"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/ion.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/ion.svg"
       address  = ""
        desc   = ""
        name   = "Ion"
   }
   atom = New-Object -TypeName psobject -Property @{
        symbol = "ATOM"
        ibc    = "ibc/27394FB092D2ECCD56123C74F36E4C1F926001CEADA9CA97EA622B25F41E5EB2"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/atom.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/atom.svg"
       address  = ""
        desc   = "The native staking and governance token of the Cosmos Hub."
        name   = "Cosmos"
   }
   akt = New-Object -TypeName psobject -Property @{
        symbol = "AKT"
        ibc    = "ibc/1480B8FD20AD5FCAE81EA87584D269547DD4D436843C1D20F15E00EB64743EF4"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/akt.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/akt.svg"
       address  = ""
        desc   = "Akash Token (AKT) is the Akash Network's native utility token, used as the primary means to govern, secure the blockchain, incentivize participants, and provide a default mechanism to store and exchange value."
        name   = "Akash Network"
   }
   xprt = New-Object -TypeName psobject -Property @{
        symbol = "XPRT"
        ibc    = "ibc/A0CC0CF735BFB30E730C70019D4218A1244FF383503FF7579C9201AB93CA9293"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/xprt.png"
        svg   = ""
       address  = ""
        desc   = "The XPRT token is primarily a governance token for the Persistence chain."
        name   = "Persistence"
   }
   iris = New-Object -TypeName psobject -Property @{
        symbol = "IRIS"
        ibc    = "ibc/7C4D60AA95E5A7558B0A364860979CA34B7FF8AAF255B87AF9E879374470CEC0"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/iris.png"
        svg   = ""
       address  = ""
        desc   = "The IRIS token is the native governance token for the IrisNet chain."
        name   = "IRISnet"
   }
   dvpn = New-Object -TypeName psobject -Property @{
        symbol = "DVPN"
        ibc    = "ibc/9712DBB13B9631EDFA9BF61B55F1B2D290B2ADB67E3A4EB3A875F3B6081B3B84"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/dvpn.png"
        svg   = ""
       address  = ""
        desc   = "DVPN is the native token of the Sentinel Hub."
        name   = "Sentinel"
   }
   cro = New-Object -TypeName psobject -Property @{
        symbol = "CRO"
        ibc    = "ibc/E6931F78057F7CC5DA0FD6CEF82FF39373A6E0452BF1FD76910B93292CF356C1"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/cro.png"
        svg   = ""
       address  = ""
        desc   = "CRO coin is the token for the Crypto.com platform."
        name   = "Cronos"
   }
   regen = New-Object -TypeName psobject -Property @{
        symbol = "REGEN"
        ibc    = "ibc/1DCC8A6CB5689018431323953344A9F6CC4D0BFB261E88C9F7777372C10CD076"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/regen.png"
        svg   = ""
       address  = ""
        desc   = "REGEN coin is the token for the Regen Network Platform"
        name   = "Regen Network"
   }
   iov = New-Object -TypeName psobject -Property @{
        symbol = "IOV"
        ibc    = "ibc/52B1AA623B34EB78FD767CEA69E8D7FA6C9CFE1FBF49C5406268FD325E2CC2AC"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/iov.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/iov.svg"
       address  = ""
        desc   = "IOV coin is the token for the Starname (IOV) Asset Name Service"
        name   = "Starname"
   }
   tick = New-Object -TypeName psobject -Property @{
        symbol = "TICK"
        ibc    = "ibc/655BCEF3CDEBE32863FF281DBBE3B06160339E9897DC9C9C9821932A5F8BA6F8"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/tick.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/tick.svg"
       address  = ""
        desc   = "TICK coin is the token for the Microtick Price Discovery & Oracle App"
        name   = "Microtick"
   }
   ngm = New-Object -TypeName psobject -Property @{
        symbol = "NGM"
        ibc    = "ibc/1DC495FCEFDA068A3820F903EDBD78B942FBD204D7E93D3BA2B432E9669D1A59"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/ngm.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/ngm.svg"
       address  = ""
        desc   = "e-Money NGM staking token. In addition to earning staking rewards the token is bought back and burned based on e-Money stablecoin inflation."
        name   = "e-Money"
   }
   eeur = New-Object -TypeName psobject -Property @{
        symbol = "EEUR"
        ibc    = "ibc/5973C068568365FFF40DEDCF1A1CB7582B6116B731CD31A12231AE25E20B871F"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/eeur.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/eeur.svg"
       address  = ""
        desc   = "e-Money EUR stablecoin. Audited and backed by fiat EUR deposits and government bonds."
        name   = "e-Money EUR"
   }
   bcna = New-Object -TypeName psobject -Property @{
        symbol = "BCNA"
        ibc    = "ibc/D805F1DA50D31B96E4282C1D4181EDDFB1A44A598BFF5666F4B43E4B8BEA95A5"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/bcna.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/bcna.svg"
       address  = ""
        desc   = "The BCNA coin is the transactional token within the BitCanna network, serving the legal cannabis industry through its payment network, supply chain and trust network."
        name   = "BitCanna"
   }
   juno = New-Object -TypeName psobject -Property @{
        symbol = "JUNO"
        ibc    = "ibc/46B44899322F3CD854D2D46DEEF881958467CDD4B3B10086DA49296BBED94BED"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/juno.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/juno.svg"
       address  = ""
        desc   = "The native token of JUNO Chain"
        name   = "Juno"
   }
   ixo = New-Object -TypeName psobject -Property @{
        symbol = "IXO"
        ibc    = "ibc/F3FF7A84A73B62921538642F9797C423D2B4C4ACB3C7FCFFCE7F12AA69909C4B"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/ixo.png"
        svg   = ""
       address  = ""
        desc   = "The native token of IXO Chain"
        name   = "IXO"
   }
   like = New-Object -TypeName psobject -Property @{
        symbol = "LIKE"
        ibc    = "ibc/9989AD6CCA39D1131523DB0617B50F6442081162294B4795E26746292467B525"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/like.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/like.svg"
       address  = ""
        desc   = "LIKE is the native staking and governance token of LikeCoin chain, a Decentralized Publishing Infrastructure to empower content ownership, authenticity, and provenance."
        name   = "LikeCoin"
   }
   luna = New-Object -TypeName psobject -Property @{
        symbol = "LUNA"
        ibc    = "ibc/0EF15DF2F02480ADE0BB6E85D9EBB5DAEA2836D3860E9F97F9AADE4F57A31AA0"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/luna.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/luna.svg"
       address  = ""
        desc   = "The native staking token of Terra."
        name   = "Luna"
   }
   ust = New-Object -TypeName psobject -Property @{
        symbol = "UST"
        ibc    = "ibc/BE1BB42D4BE3C30D50B68D7C41DB4DFCE9678E8EF8C539F6E6A9345048894FCC"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/ust.png"
        svg   = ""
       address  = ""
        desc   = "The USD stablecoin of Terra."
        name   = "TerraUSD"
   }
   krt = New-Object -TypeName psobject -Property @{
        symbol = "KRT"
        ibc    = "ibc/204A582244FC241613DBB50B04D1D454116C58C4AF7866C186AA0D6EEAD42780"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/krt.png"
        svg   = ""
       address  = ""
        desc   = "The KRW stablecoin of Terra."
        name   = "TerraKRW"
   }
   btsg = New-Object -TypeName psobject -Property @{
        symbol = "BTSG"
        ibc    = "ibc/4E5444C35610CC76FC94E7F7886B93121175C28262DDFDDE6F84E82BF2425452"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/btsg.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/btsg.svg"
       address  = ""
        desc   = "BitSong Native Token"
        name   = "BitSong"
   }
   xki = New-Object -TypeName psobject -Property @{
        symbol = "XKI"
        ibc    = "ibc/B547DC9B897E7C3AA5B824696110B8E3D2C31E3ED3F02FF363DCBAD82457E07E"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/xki.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/xki.svg"
       address  = ""
        desc   = "The native token of Ki Chain"
        name   = "Ki"
   }
   scrt = New-Object -TypeName psobject -Property @{
        symbol = "SCRT"
        ibc    = "ibc/0954E1C28EB7AF5B72D24F3BC2B47BBB2FDF91BDDFD57B74B99E133AED40972A"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/scrt.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/scrt.svg"
       address  = ""
        desc   = "The native token of Secret Network"
        name   = "Secret Network"
   }
   med = New-Object -TypeName psobject -Property @{
        symbol = "MED"
        ibc    = "ibc/3BCCC93AD5DF58D11A6F8A05FA8BC801CBA0BA61A981F57E91B8B598BF8061CB"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/med.png"
        svg   = ""
       address  = ""
        desc   = "Panacea is a public blockchain launched by MediBloc, which is the key infrastructure for reinventing the patient-centered healthcare data ecosystem"
        name   = "MediBloc"
   }
   boot = New-Object -TypeName psobject -Property @{
        symbol = "BOOT"
        ibc    = "ibc/FE2CD1E6828EC0FAB8AF39BAC45BC25B965BA67CCBC50C13A14BD610B0D1E2C4"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/boot.png"
        svg   = ""
       address  = ""
        desc   = "The staking token of Bostrom"
        name   = "Bostrom"
   }
   cmdx = New-Object -TypeName psobject -Property @{
        symbol = "CMDX"
        ibc    = "ibc/EA3E1640F9B1532AB129A571203A0B9F789A7F14BB66E350DCBFA18E1A1931F0"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/cmdx.png"
        svg   = ""
       address  = ""
        desc   = "Native Token of Comdex Protocol"
        name   = "Comdex"
   }
   cheq = New-Object -TypeName psobject -Property @{
        symbol = "CHEQ"
        ibc    = "ibc/7A08C6F11EF0F59EB841B9F788A87EC9F2361C7D9703157EC13D940DC53031FA"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/cheq.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/cheq.svg"
       address  = ""
        desc   = "Native token for the cheqd network"
        name   = "Cheqd"
   }
   stars = New-Object -TypeName psobject -Property @{
        symbol = "STARS"
        ibc    = "ibc/987C17B11ABC2B20019178ACE62929FE9840202CE79498E29FE8E5CB02B7C0A4"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/stars.png"
        svg   = ""
       address  = ""
        desc   = "The native token of Stargaze"
        name   = "Stargaze"
   }
   lum = New-Object -TypeName psobject -Property @{
        symbol = "LUM"
        ibc    = "ibc/8A34AF0C1943FD0DFCDE9ADBF0B2C9959C45E87E6088EA2FC6ADACD59261B8A2"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/lum.png"
        svg   = ""
       address  = ""
        desc   = "Native token of the Lum Network"
        name   = "Lum"
   }
   huahua = New-Object -TypeName psobject -Property @{
        symbol = "HUAHUA"
        ibc    = "ibc/B9E0A1A524E98BB407D3CED8720EFEFD186002F90C1B1B7964811DD0CCC12228"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/huahua.png"
        svg   = ""
       address  = ""
        desc   = "The native token of Chihuahua Chain"
        name   = "Chihuahua"
   }
   vdl = New-Object -TypeName psobject -Property @{
        symbol = "VDL"
        ibc    = "ibc/E7B35499CFBEB0FF5778127ABA4FB2C4B79A6B8D3D831D4379C4048C238796BD"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/vdl.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/vdl.svg"
       address  = ""
        desc   = "The native token of Vidulum"
        name   = "Vidulum"
   }
   dsm = New-Object -TypeName psobject -Property @{
        symbol = "DSM"
        ibc    = "ibc/EA4C0A9F72E2CEDF10D0E7A9A6A22954DB3444910DB5BE980DF59B05A46DAD1C"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/dsm.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/dsm.svg"
       address  = ""
        desc   = "The native token of Desmos"
        name   = "Desmos"
   }
   dig = New-Object -TypeName psobject -Property @{
        symbol = "DIG"
        ibc    = "ibc/307E5C96C8F60D1CBEE269A9A86C0834E1DB06F2B3788AE4F716EDB97A48B97D"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/dig.png"
        svg   = ""
       address  = ""
        desc   = "The native token of Dig Chain."
        name   = "Dig Chain"
   }
   rowan = New-Object -TypeName psobject -Property @{
        symbol = "ROWAN"
        ibc    = "ibc/8318FD63C42203D16DDCAF49FE10E8590669B3219A3E87676AC9DA50722687FB"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/rowan.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/rowan.svg"
       address  = ""
        desc   = "Rowan Token (ROWAN) is the Sifchain Network's native utility token, used as the primary means to govern, provide liquidity, secure the blockchain, incentivize participants, and provide a default mechanism to store and exchange value."
        name   = "Sifchain Rowan"
   }
   somm = New-Object -TypeName psobject -Property @{
        symbol = "SOMM"
        ibc    = "ibc/9BBA9A1C257E971E38C1422780CE6F0B0686F0A3085E2D61118D904BFE0F5F5E"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/somm.png"
        svg   = ""
       address  = ""
        desc   = "The native token of Sommelier Chain."
        name   = "Sommelier Chain"
   }
   band = New-Object -TypeName psobject -Property @{
        symbol = "BAND"
        ibc    = "ibc/F867AE2112EFE646EC71A25CD2DFABB8927126AC1E19F1BBF0FF693A4ECA05DE"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/band.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/band.svg"
       address  = ""
        desc   = "The native token of BandChain"
        name   = "Band Protocol"
   }
   darc = New-Object -TypeName psobject -Property @{
        symbol = "DARC"
        ibc    = "ibc/346786EA82F41FE55FAD14BF69AD8BA9B36985406E43F3CB23E6C45A285A9593"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/darc.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/darc.svg"
       address  = ""
        desc   = "The native staking and governance token of the Konstellation Network."
        name   = "Konstellation"
   }
   umee = New-Object -TypeName psobject -Property @{
        symbol = "UMEE"
        ibc    = "ibc/67795E528DF67C5606FC20F824EA39A6EF55BA133F4DC79C90A8C47A0901E17C"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/umee.png"
        svg   = ""
       address  = ""
        desc   = "The native staking and governance token of the Umee Network."
        name   = "Umee"
   }
   grav = New-Object -TypeName psobject -Property @{
        symbol = "GRAV"
        ibc    = "ibc/E97634A40119F1898989C2A23224ED83FDD0A57EA46B3A094E287288D1672B44"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/grav.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/grav.svg"
       address  = ""
        desc   = "The native token of Gravity Bridge"
        name   = "Graviton"
   }
   fet = New-Object -TypeName psobject -Property @{
        symbol = "FET"
        ibc    = "ibc/770D676475E63F10FF9AB47AFA994C6299B71EBE2CF69B7993414BDAFDA2732A"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/fet.png"
        svg   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/fet.svg"
       address  = ""
        desc   = "The native token of Fetch.ai Chain"
        name   = "Fetch.ai"
   }
   neta = New-Object -TypeName psobject -Property @{
        symbol = "NETA"
        ibc    = "ibc/297C64CC42B5A8D8F82FE2EBE208A6FE8F94B86037FA28C4529A23701C228F7A"
        png   = "https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/neta.png"
        svg   = ""
       address  = "juno168ctmpyppk90d34p3jjy658zf5a5l3w8wk35wht6ccqj4mr0yv8s4j5awr"
        desc   = "The native token cw20 for Neta on Juno Chain"
        name   = "Neta"
   }
}
$global:osmosisAssets = @(
    $(New-Object -TypeName psobject -Property @{
        symbol = "OSMO"
        ibc    = "uosmo"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/osmo.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/osmo.svg}"
      address  = ""
        desc   = "The native token of Osmosis"
        name   = "Osmosis"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "ION"
        ibc    = "uion"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/ion.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/ion.svg}"
      address  = ""
        desc   = ""
        name   = "Ion"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "ATOM"
        ibc    = "ibc/27394FB092D2ECCD56123C74F36E4C1F926001CEADA9CA97EA622B25F41E5EB2"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/atom.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/atom.svg}"
      address  = ""
        desc   = "The native staking and governance token of the Cosmos Hub."
        name   = "Cosmos"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "AKT"
        ibc    = "ibc/1480B8FD20AD5FCAE81EA87584D269547DD4D436843C1D20F15E00EB64743EF4"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/akt.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/akt.svg}"
      address  = ""
        desc   = "Akash Token (AKT) is the Akash Network's native utility token, used as the primary means to govern, secure the blockchain, incentivize participants, and provide a default mechanism to store and exchange value."
        name   = "Akash Network"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "XPRT"
        ibc    = "ibc/A0CC0CF735BFB30E730C70019D4218A1244FF383503FF7579C9201AB93CA9293"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/xprt.png}"
      address  = ""
        desc   = "The XPRT token is primarily a governance token for the Persistence chain."
        name   = "Persistence"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "IRIS"
        ibc    = "ibc/7C4D60AA95E5A7558B0A364860979CA34B7FF8AAF255B87AF9E879374470CEC0"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/iris.png}"
      address  = ""
        desc   = "The IRIS token is the native governance token for the IrisNet chain."
        name   = "IRISnet"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "DVPN"
        ibc    = "ibc/9712DBB13B9631EDFA9BF61B55F1B2D290B2ADB67E3A4EB3A875F3B6081B3B84"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/dvpn.png}"
      address  = ""
        desc   = "DVPN is the native token of the Sentinel Hub."
        name   = "Sentinel"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "CRO"
        ibc    = "ibc/E6931F78057F7CC5DA0FD6CEF82FF39373A6E0452BF1FD76910B93292CF356C1"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/cro.png}"
      address  = ""
        desc   = "CRO coin is the token for the Crypto.com platform."
        name   = "Cronos"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "REGEN"
        ibc    = "ibc/1DCC8A6CB5689018431323953344A9F6CC4D0BFB261E88C9F7777372C10CD076"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/regen.png}"
      address  = ""
        desc   = "REGEN coin is the token for the Regen Network Platform"
        name   = "Regen Network"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "IOV"
        ibc    = "ibc/52B1AA623B34EB78FD767CEA69E8D7FA6C9CFE1FBF49C5406268FD325E2CC2AC"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/iov.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/iov.svg}"
      address  = ""
        desc   = "IOV coin is the token for the Starname (IOV) Asset Name Service"
        name   = "Starname"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "TICK"
        ibc    = "ibc/655BCEF3CDEBE32863FF281DBBE3B06160339E9897DC9C9C9821932A5F8BA6F8"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/tick.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/tick.svg}"
      address  = ""
        desc   = "TICK coin is the token for the Microtick Price Discovery & Oracle App"
        name   = "Microtick"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "NGM"
        ibc    = "ibc/1DC495FCEFDA068A3820F903EDBD78B942FBD204D7E93D3BA2B432E9669D1A59"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/ngm.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/ngm.svg}"
      address  = ""
        desc   = "e-Money NGM staking token. In addition to earning staking rewards the token is bought back and burned based on e-Money stablecoin inflation."
        name   = "e-Money"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "EEUR"
        ibc    = "ibc/5973C068568365FFF40DEDCF1A1CB7582B6116B731CD31A12231AE25E20B871F"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/eeur.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/eeur.svg}"
      address  = ""
        desc   = "e-Money EUR stablecoin. Audited and backed by fiat EUR deposits and government bonds."
        name   = "e-Money EUR"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "BCNA"
        ibc    = "ibc/D805F1DA50D31B96E4282C1D4181EDDFB1A44A598BFF5666F4B43E4B8BEA95A5"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/bcna.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/bcna.svg}"
      address  = ""
        desc   = "The BCNA coin is the transactional token within the BitCanna network, serving the legal cannabis industry through its payment network, supply chain and trust network."
        name   = "BitCanna"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "JUNO"
        ibc    = "ibc/46B44899322F3CD854D2D46DEEF881958467CDD4B3B10086DA49296BBED94BED"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/juno.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/juno.svg}"
      address  = ""
        desc   = "The native token of JUNO Chain"
        name   = "Juno"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "IXO"
        ibc    = "ibc/F3FF7A84A73B62921538642F9797C423D2B4C4ACB3C7FCFFCE7F12AA69909C4B"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/ixo.png}"
      address  = ""
        desc   = "The native token of IXO Chain"
        name   = "IXO"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "LIKE"
        ibc    = "ibc/9989AD6CCA39D1131523DB0617B50F6442081162294B4795E26746292467B525"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/like.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/like.svg}"
      address  = ""
        desc   = "LIKE is the native staking and governance token of LikeCoin chain, a Decentralized Publishing Infrastructure to empower content ownership, authenticity, and provenance."
        name   = "LikeCoin"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "LUNA"
        ibc    = "ibc/0EF15DF2F02480ADE0BB6E85D9EBB5DAEA2836D3860E9F97F9AADE4F57A31AA0"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/luna.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/luna.svg}"
      address  = ""
        desc   = "The native staking token of Terra."
        name   = "Luna"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "UST"
        ibc    = "ibc/BE1BB42D4BE3C30D50B68D7C41DB4DFCE9678E8EF8C539F6E6A9345048894FCC"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/ust.png}"
      address  = ""
        desc   = "The USD stablecoin of Terra."
        name   = "TerraUSD"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "KRT"
        ibc    = "ibc/204A582244FC241613DBB50B04D1D454116C58C4AF7866C186AA0D6EEAD42780"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/krt.png}"
      address  = ""
        desc   = "The KRW stablecoin of Terra."
        name   = "TerraKRW"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "BTSG"
        ibc    = "ibc/4E5444C35610CC76FC94E7F7886B93121175C28262DDFDDE6F84E82BF2425452"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/btsg.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/btsg.svg}"
      address  = ""
        desc   = "BitSong Native Token"
        name   = "BitSong"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "XKI"
        ibc    = "ibc/B547DC9B897E7C3AA5B824696110B8E3D2C31E3ED3F02FF363DCBAD82457E07E"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/xki.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/xki.svg}"
      address  = ""
        desc   = "The native token of Ki Chain"
        name   = "Ki"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "SCRT"
        ibc    = "ibc/0954E1C28EB7AF5B72D24F3BC2B47BBB2FDF91BDDFD57B74B99E133AED40972A"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/scrt.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/scrt.svg}"
      address  = ""
        desc   = "The native token of Secret Network"
        name   = "Secret Network"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "MED"
        ibc    = "ibc/3BCCC93AD5DF58D11A6F8A05FA8BC801CBA0BA61A981F57E91B8B598BF8061CB"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/med.png}"
      address  = ""
        desc   = "Panacea is a public blockchain launched by MediBloc, which is the key infrastructure for reinventing the patient-centered healthcare data ecosystem"
        name   = "MediBloc"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "BOOT"
        ibc    = "ibc/FE2CD1E6828EC0FAB8AF39BAC45BC25B965BA67CCBC50C13A14BD610B0D1E2C4"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/boot.png}"
      address  = ""
        desc   = "The staking token of Bostrom"
        name   = "Bostrom"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "CMDX"
        ibc    = "ibc/EA3E1640F9B1532AB129A571203A0B9F789A7F14BB66E350DCBFA18E1A1931F0"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/cmdx.png}"
      address  = ""
        desc   = "Native Token of Comdex Protocol"
        name   = "Comdex"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "CHEQ"
        ibc    = "ibc/7A08C6F11EF0F59EB841B9F788A87EC9F2361C7D9703157EC13D940DC53031FA"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/cheq.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/cheq.svg}"
      address  = ""
        desc   = "Native token for the cheqd network"
        name   = "Cheqd"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "STARS"
        ibc    = "ibc/987C17B11ABC2B20019178ACE62929FE9840202CE79498E29FE8E5CB02B7C0A4"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/stars.png}"
      address  = ""
        desc   = "The native token of Stargaze"
        name   = "Stargaze"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "LUM"
        ibc    = "ibc/8A34AF0C1943FD0DFCDE9ADBF0B2C9959C45E87E6088EA2FC6ADACD59261B8A2"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/lum.png}"
      address  = ""
        desc   = "Native token of the Lum Network"
        name   = "Lum"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "HUAHUA"
        ibc    = "ibc/B9E0A1A524E98BB407D3CED8720EFEFD186002F90C1B1B7964811DD0CCC12228"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/huahua.png}"
      address  = ""
        desc   = "The native token of Chihuahua Chain"
        name   = "Chihuahua"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "VDL"
        ibc    = "ibc/E7B35499CFBEB0FF5778127ABA4FB2C4B79A6B8D3D831D4379C4048C238796BD"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/vdl.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/vdl.svg}"
      address  = ""
        desc   = "The native token of Vidulum"
        name   = "Vidulum"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "DSM"
        ibc    = "ibc/EA4C0A9F72E2CEDF10D0E7A9A6A22954DB3444910DB5BE980DF59B05A46DAD1C"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/dsm.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/dsm.svg}"
      address  = ""
        desc   = "The native token of Desmos"
        name   = "Desmos"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "DIG"
        ibc    = "ibc/307E5C96C8F60D1CBEE269A9A86C0834E1DB06F2B3788AE4F716EDB97A48B97D"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/dig.png}"
      address  = ""
        desc   = "The native token of Dig Chain."
        name   = "Dig Chain"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "ROWAN"
        ibc    = "ibc/8318FD63C42203D16DDCAF49FE10E8590669B3219A3E87676AC9DA50722687FB"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/rowan.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/rowan.svg}"
      address  = ""
        desc   = "Rowan Token (ROWAN) is the Sifchain Network's native utility token, used as the primary means to govern, provide liquidity, secure the blockchain, incentivize participants, and provide a default mechanism to store and exchange value."
        name   = "Sifchain Rowan"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "SOMM"
        ibc    = "ibc/9BBA9A1C257E971E38C1422780CE6F0B0686F0A3085E2D61118D904BFE0F5F5E"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/somm.png}"
      address  = ""
        desc   = "The native token of Sommelier Chain."
        name   = "Sommelier Chain"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "BAND"
        ibc    = "ibc/F867AE2112EFE646EC71A25CD2DFABB8927126AC1E19F1BBF0FF693A4ECA05DE"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/band.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/band.svg}"
      address  = ""
        desc   = "The native token of BandChain"
        name   = "Band Protocol"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "DARC"
        ibc    = "ibc/346786EA82F41FE55FAD14BF69AD8BA9B36985406E43F3CB23E6C45A285A9593"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/darc.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/darc.svg}"
      address  = ""
        desc   = "The native staking and governance token of the Konstellation Network."
        name   = "Konstellation"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "UMEE"
        ibc    = "ibc/67795E528DF67C5606FC20F824EA39A6EF55BA133F4DC79C90A8C47A0901E17C"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/umee.png}"
      address  = ""
        desc   = "The native staking and governance token of the Umee Network."
        name   = "Umee"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "GRAV"
        ibc    = "ibc/E97634A40119F1898989C2A23224ED83FDD0A57EA46B3A094E287288D1672B44"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/grav.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/grav.svg}"
      address  = ""
        desc   = "The native token of Gravity Bridge"
        name   = "Graviton"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "FET"
        ibc    = "ibc/770D676475E63F10FF9AB47AFA994C6299B71EBE2CF69B7993414BDAFDA2732A"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/fet.png; svg=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/fet.svg}"
      address  = ""
        desc   = "The native token of Fetch.ai Chain"
        name   = "Fetch.ai"
}),
   $(New-Object -TypeName psobject -Property @{
        symbol = "NETA"
        ibc    = "ibc/297C64CC42B5A8D8F82FE2EBE208A6FE8F94B86037FA28C4529A23701C228F7A"
        logo   = "@{png=https://raw.githubusercontent.com/osmosis-labs/assetlists/main/images/neta.png}"
      address  = "juno168ctmpyppk90d34p3jjy658zf5a5l3w8wk35wht6ccqj4mr0yv8s4j5awr"
        desc   = "The native token cw20 for Neta on Juno Chain"
        name   = "Neta"
})
)
function Test-IsWalletSet {
    param(
        [string]$symbol
    )
    if (Get-Variable $symbol | Out-Null) {return $true}
}

# Osmosis 

function Get-OsmoRewards {
    param(
        [string]$symbol 
    )
    Clear-Vars rewards
    $totalRewards = 0
    if ($symbol){
        switch ($symbol){
            'atom'        { $url = "https://lcd-cosmos.keplr.app/distribution/delegators/$atom/rewards" }
            'scrt'        { $url = "https://lcd-secret.keplr.app/distribution/delegators/$scrt/rewards" }
            'juno'        { $url = "https://lcd-juno.keplr.app/distribution/delegators/$juno/rewards" }
            'stars'        { $url = "https://lcd-stargaze.keplr.app/distribution/delegators/$stars/rewards" }
            'dvpn'        { $url = "https://lcd-dvpn.keplr.app/distribution/delegators/$dvpn/rewards" }
            'osmo'        { $url = "https://lcd-osmosis.keplr.app/distribution/delegators/$osmo/rewards" }
            'dig'        { $url = "https://lcd-dig.keplr.app/distribution/delegators/$dig/rewards" }
            'dsm'        { $url = "https://lcd-desmos.keplr.app/distribution/delegators/$dsm/rewards" }
            default       {  }
        }

        $global:rewards = (Invoke-RestMethod -Uri $url).result.rewards.reward 
        Foreach ($reward in $rewards) {
            $totalRewards += $reward.amount / 1000000
        }
        [Math]::Round($totalRewards, 3)
    }
    else {

    }
}
function Get-OsmoApi {
    param(
        [string]$endpoint 
    )
    Invoke-RestMethod -Uri "https://api-osmosis.imperator.co/$endpoint"
}
function Get-OsmoPair {
    param(
        [string]$base_symbol,
        [string]$quote_symbol
    )

    $global:pairs = (Invoke-RestMethod "https://api-osmosis.imperator.co/pairs/v1/summary").data 
    if ($base_symbol){
        $pairs | Where-Object base_symbol -eq $base_symbol 
    }
    if ($quote_symbol){
        $pairs | Where-Object quote_symbol -eq $quote_symbol 
    }
    if (!$base_symbol -and !$quote_symbol){
        Write-Success $pairs.count,'pairs saved as $pairs'
    }
}
function Get-OsmoVolume {
    param(
        [string]$symbol
    )

    Invoke-RestMethod "https://api-osmosis.imperator.co/tokens/v2/volume/$symbol/chart"
}
function Get-OsmoLiquidity {
    param(
        [string]$symbol
    )

    Invoke-RestMethod "https://api-osmosis.imperator.co/tokens/v2/liquidity/$symbol/chart"
}
function Get-OsmoHistory {
    param(
        [Parameter(Mandatory=$true)]
        [string]$symbol,
        
        [Parameter(Mandatory=$true)]
        [ValidateSet('5','15','30','60','120','240','720','1440','10080','43800')]
        [string]$timeframe,

        [switch]$o
    )

    $hist = Invoke-RestMethod "https://api-osmosis.imperator.co/tokens/v2/historical/$symbol/chart?tf=$timeframe"

    ForEach ($h in $hist){
        $h.time = (ConvertFrom-UnixTime $($h.time.ToString()[0..9] -join '')) -f "{0:hh\:mm\:ss}"
        $h.high = [Math]::Round($h.high, 2)
        $h.low = [Math]::Round($h.low, 2)
        $h.open = [Math]::Round($h.open, 2)
        $h.close = [Math]::Round($h.close, 2)
        $h.volume = [Math]::Round($h.volume, 2)
    }
    
    if ($o) {
        $hist
    }
    else {
        $hist | Select-Object time,open,high,low,close,volume | Format-Table -AutoSize
    }
}
function Get-OsmoGainers {

    $global:gainers = (Invoke-RestMethod "https://api-osmosis.imperator.co/tokens/v2/top/gainers") 

    ForEach ($g in $gainers){
        $g.price = "`$ $([Math]::Round($g.price, 2))"
        $g.price_24h_change = "$([Math]::Round($g.price_24h_change, 2)) %"
    }

    $gainers | Select-Object name,symbol,price,price_24h_change
}
function Get-OsmoLosers {

    $global:losers = Invoke-RestMethod "https://api-osmosis.imperator.co/tokens/v2/top/losers" 
    
    ForEach ($l in $losers){
        $l.price = "`$ $([Math]::Round($l.price, 2))"
        $l.price_24h_change = "$([Math]::Round($l.price_24h_change, 2)) %"
    }
    
    $losers | Select-Object name,symbol,price,price_24h_change
}
function Get-OsmoPool {
    param(
        [string]$id,

        [switch]$o
    )
    if ($id){
        $pool = Invoke-RestMethod "https://api-osmosis.imperator.co/pools/v2/$id"
    }
    else {
        $response = Invoke-RestMethod "https://api-osmosis.imperator.co/pools/v2/all"
        $poolNums = ($response | Get-Member | Where-Object MemberType -eq 'NoteProperty').Name
        $global:pools = $poolNums | Foreach-Object { $response.$_ }
        if ($pools) {
            Write-Success $pools.count,'pools saved as $pools'
        }
    }

    if ($o){
        $pool
    }
    else {
        "$($pool[0].symbol)/$($pool[1].symbol)"
    }
}
function Get-OsmoSymbol {
    param(
        [string]$symbol
    )
    if ($symbol){
        Invoke-RestMethod "https://api-osmosis.imperator.co/tokens/v2/$symbol"
    }
    else {
        $global:symbols = Invoke-RestMethod "https://api-osmosis.imperator.co/tokens/v2/all"
        Write-Success $symbols.count,'symbols saved as $symbols'
    }
}
function Get-OsmoPrice {
    param(
        [string]$symbol
    )
    $price = Invoke-RestMethod "https://api-osmosis.imperator.co/tokens/v2/price/$symbol"
    
    $p = [Math]::Round($price.price, 2)
    $p
}
function Get-OsmoDominance {
    $global:dominance = Invoke-RestMethod "https://api-osmosis.imperator.co/tokens/v2/dominance/all" 
    $dominance | Select-Object name,symbol,dominance
}
function Get-OsmoPoolFees {
    param(
        [string]$id
    )
    if ($id){
        (Invoke-RestMethod "https://api-osmosis.imperator.co/fees/v1/$id").data 
    }
    else {
        $global:fees = Invoke-RestMethod "https://api-osmosis.imperator.co/fees/v1/all" 
        Write-Success $fees.count,'fees saved as $fees'
    }
}
function Get-OsmoDenom {
    param(
        [string]$symbol
    )
    (Invoke-RestMethod "https://api-osmosis.imperator.co/search/v1/denom?symbol=$symbol").denom
}

function Get-OsmoActivePools {

    param(
        [string]$address = "osmo1pnkcx7cdea2csn06wlgvalxhc9jq6u3f6nqnr7"
    )

    $global:active_pools = [System.Collections.ArrayList]::New()

    $coins = (Invoke-RestMethod -Uri "https://lcd-osmosis.keplr.app/osmosis/lockup/v1beta1/account_locked_coins/$address").coins

    foreach ($c in $coins){
        $id = $c.denom.split('/')[2]
        $pool_name = Get-OsmoPool -id $id
        
        $obj = New-Object -TypeName psobject -Property @{
            pool     = $pool_name 
            amount   = $c.amount / 100000
        }
        [void]$active_pools.Add($obj)
    }
    $active_pools | Select-Object pool,amount | Format-Table -AutoSize

}

function Get-OsmoPoolApr {
    param(
        [string]$id
    )

    if ($id){

        $apr = Invoke-RestMethod "https://api-osmosis.imperator.co/apr/v2/$id"

        $pool_name      = Get-OsmoPool -id $id 

        $obj = New-Object -TypeName psobject -Property @{
            id      =   $id
            name    =   $pool_name 
            '1d'    =   [Math]::Round($apr[0].apr_list.apr_1d, 2)
            '7d'    =   [Math]::Round($apr[0].apr_list.apr_7d, 2)
            '14d'   =   [Math]::Round($apr[0].apr_list.apr_14d, 2)
        }

        $obj | Select-Object id,name,'1d','7d','14d'
    }
}

# Terra
function Get-TerraWalletBalances {
    
    param(
        [string]$walletAddress  = $wallets.keplr.terra.address 
    )

    $contractAddresses = @(
        'terra14z56l0fp2lsf86zy3hty2z47ezkhnthtr9yq76'
    )

    Foreach ($addr in $contractAddresses){
        $introspectionQuery = '
        query GetWasmContractsContractAddressStorePayload {
            WasmContractsContractAddressStore(
                ContractAddress: "' + $addr + '"
                QueryMsg: "{ \"balance\": { \"address\": \"' + $walletAddress + '\"
                    }
                }"
            ){
                Height
                Result
                __typename
            }
        }
'
        $data = (Invoke-GraphQLQuery -Query $introspectionQuery -Uri $uri ).data
        $balance = ($data.WasmContractsContractAddressStore.Result | ConvertFrom-Json).balance
        $balance
    }
} 

function Get-BoostedPools {
    param(
      [string]$symbol,
      [switch]$all
    )
  
    $global:out       = [System.Collections.ArrayList]::New()
  
    $non_empty_pools = $pools | Where-Object {$_.rewardTokens} | Where-Object { $_.totalTvlUsd -gt 0 }| Where-Object { $_.poolPrice -gt 0 }
  
    if ($all){
      $boosted_pools = $non_empty_pools | Where-Object { $_.poolData.price0Usd -gt 0 } | Where-Object { $_.poolData.price1Usd -gt 0 }
    }
    else {
      $boosted_pools = $non_empty_pools | Where-Object {$_.boostOxDao -gt 2.4 } | Where-Object { $_.boostSolidex -gt 1.6 } | Where-Object { $_.poolData.price0Usd -gt 0 } | Where-Object { $_.poolData.price1Usd -gt 0 }
    }
    
    Foreach ($boosted_pool in $boosted_pools) {
  
      ForEach ($rewardToken in $boosted_pool.rewardTokens){
            
        switch($rewardToken.id) {
          '0x888EF71766ca594DED1F0FA3AE64eD2941740A20'  { $reward = 'SOLID'}
          '0xc5A9848b9d145965d821AaeC8fA32aaEE026492d'  { $reward = 'OXD (V2)' }
          default {'-'}
        }
        $obj = New-Object -TypeName psobject -Property @{
          symbol                  = $boosted_pool.poolData.symbol
          rewardToken             = $reward
          rewardRate              = [float]$rewardToken.rewardRate
          periodEnds              = $(ConvertFrom-UnixTime $rewardToken.periodFinish)
          boostOxdao              = $boosted_pool.boostOxDao
          boostSolidex            = $boosted_pool.boostSolidex 
        }
        [void]$out.add($obj)
      }
    }
    if ($symbol){
      $out | Where-Object symbol -match $symbol | Select-Object symbol,rewardToken,rewardRate,periodEnds,boostOxdao,boostSolidex | Sort-Object -desc rewardRate | Format-Table -AutoSize 
    }
    else {
      $out | Select-Object symbol,rewardToken,rewardRate,periodEnds,boostOxdao,boostSolidex | Sort-Object -desc rewardRate | Select-Object -First 40 | Format-Table -AutoSize 
    }
  }
  
  function Get-PriceCheck {
    param(
      [int]$interval = 180,
      [int]$minutes  = 1000
    )
      $timeout = new-timespan -Minutes $minutes
      $sw = [diagnostics.stopwatch]::StartNew()
      while ($sw.elapsed -lt $timeout)
      {
          $prices = Invoke-RestMethod 'https://oxdapi.s3.amazonaws.com/protocol.json'
          $solidPrice   = [Math]::Round($prices.solidPrice, 2) 
          $oxSolidPrice = [Math]::Round($prices.oxSolidPrice, 2) 
  
          $ftm = Invoke-RestMethod -Uri 'https://api.coingecko.com/api/v3/coins/fantom'
          $rdl = Invoke-RestMethod -Uri 'https://api.coingecko.com/api/v3/coins/radial-finance'
          $oxdv1 = Invoke-RestMethod -Uri 'https://api.coingecko.com/api/v3/coins/0xdao'
          $oxdv2 = Invoke-RestMethod -Uri 'https://api.coingecko.com/api/v3/coins/0xdao-v2'
  
          $coin_gecko   = (Invoke-RestMethod "https://api.coingecko.com/api/v3/simple/token_price/fantom?contract_addresses=$($rdl.contract_address),$($oxdv1.contract_address),$($ftm.contract_address),$($oxdv2.contract_address)&vs_currencies=usd")
          
          [float]$rdlPrice        = $coin_gecko.$($rdl.contract_address).usd
          [float]$oxdv1Price      = [Math]::Round($coin_gecko.$($oxdv1.contract_address).usd,2)
          [float]$ftmPrice        = $coin_gecko.$($ftm.contract_address).usd
          [float]$oxdv2Price      = $coin_gecko.$($oxdv2.contract_address).usd
  
          $time = get-date -UFormat "%H:%M"
          Write-Host ">  $time   FTM: " -NoNewLine 
          Write-Host -ForeGround Blue "$ftmPrice " -NoNewLine     
          Write-Host -ForeGround White "  OXDv1: " -NoNewLine
          Write-Host -ForeGround Cyan "$oxdv1Price "  -NoNewLine 
          Write-Host -ForeGround White "  OXDv2: " -NoNewLine 
          Write-Host -ForeGround Green "$oxdv2Price" -NoNewLine        
          Write-Host -ForeGround White "   SOLID: " -NoNewLine 
          Write-Host -ForeGround Yellow "$solidPrice" -NoNewLine 
          Write-Host -ForeGround White "  oxSOLID: " -NoNewLine
          Write-Host -ForeGround Cyan "$oxSolidPrice "  -NoNewLine 
          Write-Host -ForeGround White "  RDL: " -NoNewLine
          Write-Host -ForeGround Gray "$rdlPrice "    
          start-sleep -seconds $Interval
      }
      Write-Host -ForegroundColor Red [+] (Get-Date -UFormat "%H:%M") "Done"
  }
  
  function Get-Yield2 {
    param(
      $solidStart,
      $oxdStart,
      [int]$days,
      [int]$daysUntil
    )
  
      $global:out       = [System.Collections.ArrayList]::New()
      $solidDailyReward  = 92.055225
      $oxdDailyReward    = 88.091879
      $day              = 0
  
      $startBalance      = 1776 

      $fission_daily_rate = 0.001269 

      $solid  =  19.850  -   6.425
      $oxd    =  19    -   6.153 
      
      $oxdBribes = Invoke-RestMethod https://api.oxdao.fi/bribes
      $oxdPools  = Invoke-RestMethod https://api.oxdao.fi/pools
  
      
  
      Write-Host "`n"
      Write-Host "StartBalance         : $startBalance"
      Write-Host "Solid Daily Reward   : $solidDailyReward"
      Write-Host "OXD Daily Reward     : $oxdDailyReward"
      Write-Host "`n"
      $solidRate = 1.05
      $oxdRate   = 1.05 
  
      while ($solidStart -lt $daysUntil) {
  
        
        $solidEnd            = $solidStart * $solidRate 
  
        $oxdEnd              = $oxdStart + $oxdRate
        
        $day                 = $day + 1
  
        $solidPrice          = 1.82 
        $oxdPrice            = 8.56
  
        $obj = New-Object -TypeName psobject -Property @{
          Day                  = $day
          
          solidStart                = [math]::round($solidStart, 0)
          oxdStart                  = [math]::round($oxdStart, 0)
          
          solidTotal            = "$  " +  [math]::round($solidEnd * $solidPrice, 2)
          oxdTotal              = "$  " +  [math]::round($oxdEnd * $oxdPrice, 2)
  
          # solidDailyReward     = [math]::round($solidDailyReward, 2)
          # oxdDailyReward       = [math]::round($oxdDailyReward, 2)
  
  
          solidEnd              = [math]::round($solidEnd, 2)
          oxdEnd                = [math]::Round($oxdEnd, 2)
        }
  
        $solidStart     = $solidEnd
        $oxdStart       = $oxdEnd
  
        [void]$out.Add($obj)
  
      }
      $out | Select-Object Day,solidStart,solidEnd,solidTotal,oxdStart,oxdEnd,oxdTotal | Format-Table -AutoSize
  
  }
   
  function Get-OxdPools {
    param(
      [string]$pool
    )
    $global:oxdPools         = (Invoke-RestMethod -Uri 'https://api.oxdao.fi/pools').poolData
    $oxdPools | Select-Object symbol,price0Usd,price1Usd   
  }
  function Get-OxdWftmRatio {
  
    param(
      [float]$oxd,
      [float]$wftm
    )
    [float]$ftmPrice = (kp ftm).price.trim('$')
    $protocol = Invoke-RestMethod -Uri 'https://api2.oxdao.fi/protocol' 
    [float]$oxdPrice = $protocol.oxdPrice
  
    if ($oxd){
      $wftmNeeded = ( $oxd * $oxdPrice ) / $ftmPrice
      $wftmNeeded 
    }
    else{
      $oxNeeded = ( $wftm * $ftmPrice ) / $oxdPrice
      $oxdNeeded 
    }
  
  }

  function Get-Oxd {
    param(
      $balance 
    )
    $ftmPrice = (kp ftm).price.trim('$')
    $protocol = Invoke-RestMethod -Uri 'https://api2.oxdao.fi/protocol' 
    $oxdPrice = $protocol.oxdPrice
  
    if ($balance){
      $oxd = $balance / 2
      $wftm = $oxd * 5 
      $oxdTotal = $oxd * $oxdPrice 
      $wftmPrice =   [float]$ftmPrice 
      $wftmTotal =   $wftm * $wftmPrice
  
      $global:out       = [System.Collections.ArrayList]::New()
  
      $oxdObj = New-Object -TypeName psobject -Property @{
        Name              = 'OXD'
        Amount            = [math]::round($oxd, 3)
        Price             = [math]::round($oxdPrice, 2)
        Total             = [math]::round($oxdTotal, 2)
      }
      $wftmObj += New-Object -TypeName psobject -Property @{
        Name              = 'WFTM'
        Amount            = [math]::round($wftm, 0)
        Price             = [math]::round($wftmPrice, 2)
        Total             = [math]::round($wftmTotal, 2)
      }
      [void]$out.Add($oxdObj)
      [void]$out.Add($wftmObj)
      
      $out | Select-Object Name,Amount,Price,Total | Format-Table -AutoSize
  
      Write-Success ' '
      Write-Success 'Total:    ',($oxdObj.total + $wftmObj.total)
      Write-Success ' '
    }
    else {
      $global:out       = [System.Collections.ArrayList]::New()
  
      $daysUntil = 30000
      $startBalance = 1981 
      $dailyRate = 1.05
      
      while ($startBalance -lt $daysUntil) {
  
        $solidReward     = $startBalance * $dailyRate 
        $oxdReward       = $startBalance * $dailyRate 
        $oxdTotal        = $oxdReward * $oxdPrice  
        $solidTotal      = $solidReward * $solidPrice 
        $totalRewardPrice = $oxdTotal + $solidTotal 
  
        $tokenReward      = $totalRewardPrice / 6.75 
        $endBalance       = $startBalance + $tokenReward 
  
        $day            = $day + 1
  
        $obj = New-Object -TypeName psobject -Property @{
          Day           = $day
          StartBalance  = [math]::round($startBalance, 0)
          DailyReward   = [math]::round($totalRewardPrice, 2)
          EndBalance    = [math]::Round($endBalance, 2)
        }
  
        $startBalance   = $endBalance
        [void]$out.Add($obj)
  
      }
      $out | Select-Object Day,StartBalance,DailyReward,EndBalance | Format-Table -AutoSize
    }
  }
  
  function Get-Yield {
    param(
      $startBalance,
      $hourlyReward,
      [int]$days,
      [int]$daysUntil
    )
  
      $global:out       = [System.Collections.ArrayList]::New()
      $dailyReward      = $hourlyReward * 24
      # $dailyRate        = [math]::round($dailyReward/$startBalance, 4 ) 
      $dailyRate        = 0.1269
      $day              = 0
  
      Write-Host "`n"
      Write-Host "StartBalance  : $startBalance"
      Write-Host "Hourly        : $hourlyReward"
      Write-Host "DailyReward   : $dailyReward"
      Write-Host "`n"
  
      while ($startBalance -lt $daysUntil) {
  
        $endBalance     = $startBalance + $dailyReward 
        $day            = $day + 1
  
        $obj = New-Object -TypeName psobject -Property @{
          Day           = $day
          StartBalance  = [math]::round($startBalance, 0)
          DailyRate     = $dailyRate
          DailyReward   = [math]::round($dailyReward, 2)
          EndBalance    = [math]::Round($endBalance, 2)
        }
  
        $startBalance   = $endBalance
        # $dailyReward    = $startBalance * $dailyRate
  
        [void]$out.Add($obj)
  
      }
      $out | Select-Object Day,StartBalance,DailyRate,DailyReward,EndBalance | Format-Table -AutoSize
  }

#########################################
#########################################
#########################################
#########################################
#########################################

function Get-SafetyOrders {

    param(
      $targetProfit                   = 0.07,
      $trailingDeviation              = 0,
      $safetyOrderPriceDeviation      = 0.21,
      $baseOrderPriceUSD              = 10000,
      $baseOrderVolumeUSD             = 2,
      $safetyOrderCount               = 3,
      $safetyOrderVolumeUSD           = 135,
      $safetyOrderVolumeScale         = 5.33,
      $safetyOrderStepScale           = 1.14,
      $biggestPercentageGain          = 5,
      
      [Parameter(Mandatory=$true)]
      $leverage,

      [ValidateSet(0.5, 1)]
      $maintenanceMarginRate          = 0.5,

      [ValidateSet('long','short')]
      [string]$mode                   = 'long'
    )
    
    $obj = [System.Collections.ArrayList]::New()
  
    $orderPrice         = $baseOrderPriceUSD
    $orderVolume        = $baseOrderVolumeUSD
    $dealVolume         = $orderVolume
    $liquidPrice        = Get-LiquidationPrice -avgEntryPrice $baseOrderPriceUSD -leverage $leverage -maintenanceMarginRate $maintenanceMarginRate -mode $mode            
    $contractValue      = $orderVolume / $orderPrice
    $avgEntryPrice      = $dealVolume / $contractValue
    $liquidDelta        = $liquidPrice - $orderPrice
    $targetPrice        = $orderPrice * 0.988873977228452
    $targetDelta        = $orderPrice - $targetPrice
    # $targetVolume       = $dealVolume + ($targetProfit/100 * $dealVolume)
    # $tgtContractValue   = $tgtVolume / profitDelta        = $targetProfit / 
    
    $b = New-Object -TypeName psobject -Property @{
      order           = 0 
      orderVolume     = [math]::Round($orderVolume, 2)
      orderPrice      = [math]::Round($orderPrice, 2)
      dealVolume      = [math]::Round($dealVolume, 2)
      avgEntryPrice   = [math]::Round($avgEntryPrice,2)
      contractValue   = [math]::Round($contractValue, 4) 
      liquidPrice     = [math]::Round($liquidPrice, 2)
      liquidDelta     = [math]::Round($liquidDelta, 2)
      targetPrice     = [math]::Round($targetPrice, 2)
      targetDelta     = [math]::Round($targetDelta, 2)
      unrealizedPNL   = 0
      soPriceDev      = 0
      soVolScale      = 0
      soStepScale     = 0
    }
    [void]$obj.Add($b)
  
    $soPriceDev      = $orderPrice * ($safetyOrderPriceDeviation / 100)
    $soVolume        = $safetyOrderVolumeUSD 

    1..$safetyOrderCount | ForEach-Object {
  
      $orderPrice       = $orderPrice + $soPriceDev
      $orderVolume      = $soVolume 
      $contractValue    = ($orderVolume / $orderPrice) + ($dealVolume / $avgEntryPrice )
      $dealVolume       = $dealVolume + $orderVolume 
      $avgEntryPrice    = $dealVolume / $contractValue
      $liquidPrice      = Get-LiquidationPrice -avgEntryPrice $avgEntryPrice -leverage $leverage -maintenanceMarginRate $maintenanceMarginRate -mode $mode             
      $liquidDelta      = $liquidPrice - $orderPrice
      $targetPrice      = $orderPrice * 0.988873977228452
      $targetDelta      = $orderPrice - $targetPrice

      $o = New-Object -TypeName psobject -Property @{
        order           = $_ 
        orderVolume     = [math]::Round($orderVolume,2) 
        orderPrice      = [math]::round($orderPrice, 2)
        dealVolume      = [math]::Round($dealVolume, 2) 
        contractValue   = [math]::Round($contractValue, 2)
        avgEntryPrice   = [math]::Round($avgEntryPrice, 2)
        liquidPrice     = [math]::Round($liquidPrice, 2)
        liquidDelta     = [math]::Round($liquidDelta, 2)
        targetPrice     = [math]::Round($targetPrice, 2)
        targetDelta     = [math]::Round($targetDelta, 2)
        unrealizedPNL   = 0
        soPriceDev      = [math]::Round($soPriceDev,2)      
        soVolScale      = [math]::Round($safetyOrderVolumeScale,2)
        soStepScale     = [math]::Round($safetyOrderStepScale,2)
      }
      [void]$obj.Add($o)
  
      $soVolume       = $orderVolume * $safetyOrderVolumeScale      
      $soPriceDev     = $soPriceDev * $safetyOrderStepScale           
    }
    $obj | Select-Object order,orderVolume,orderPrice,dealVolume,contractValue,unrealizedPNL,avgEntryPrice,liquidPrice,liquidDelta,targetPrice,targetDelta | Format-Table -auto
  
}


    function Get-EthWalletHash {
    param(
        $n,
        $r,
        $p,
        $salt,
        $ciphertext,
        $mac,
        $keystore,
        [switch]$info
    )
    if ($keystore){
        $data = Get-Content $keystore | ConvertFrom-Json 
        $params = $data.Crypto.kdfparams
        if (!($n)){
        $n = $params.n
        }
        if (!($r)){
        $r = $params.r
        }
        if (!($p)){
        $p = $params.p
        }
        if (!($salt)){
        $salt = $params.salt
        }
        if (!($mac)){
        $mac = $data.Crypto.mac
        }
        if (!($ciphertext)){
        $ciphertext = $data.Crypto.ciphertext
        }
        $out = "`$ethereum`$s*$n*$r*$p*$salt*$ciphertext*$mac"
    }
    else{
        $out = "`$ethereum`$s*$n*$r*$p*$salt*$ciphertext*$mac"
    }
    if ($info){
        $obj = New-Object -TypeName psobject -Property @{
        n       = $n
        r       = $r
        p       = $p
        salt    = $salt 
        mac     = $mac 
        ciphertext = $ciphertext
        }
        $obj | Select n,r,p,salt,mac,ciphertext 
    }
    else {
        return $out
    }
    }









$global:PBToolsFunctions =  $PBToolsCmdlets.Name 


$PBToolsCmdlets | Foreach-Object { Set-Alias -Name $_.Alias -Value $_.Name -Scope Global -Force }


Export-ModuleMember -Function $PBToolsFunctions 
}
