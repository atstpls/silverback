New-Module -Name Jafar -Scriptblock {

    [array]$global:JafarCmdlets = @(
    
        $(New-Object -TypeName psobject -Property @{
            Name          = "ConvertTo-USDollar"
            Alias         = "cud"
            Description   = "Convert to US Dollar"
            Category      = "u"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "ConvertTo-BTC"
            Alias         = "ctb"
            Description   = "Convert to Bitcoin"
            Category      = "u"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-Signature"
            Alias         = "gsig"
            Description   = "Get signature"
            Category      = "u"
            Mod           = "jafar"
            Status        = "good"
        }),        
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-JafarCmdlets"
            Alias         = "gjc"
            Description   = "Get cmdlets"
            Category      = "h"
            Mod           = "jafar"
            Status        = "good"
        }),               
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-JafarTools"
            Alias         = "ja"
            Description   = "Get tools"
            Category      = "h"
            Mod           = "jafar"
            Status        = "good"
        }),    
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-BybitBalance"
            Alias         = "gbb"
            Description   = "Get balance"
            Category      = "a"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-Price"
            Alias         = "gp"
            Description   = "Get price"
            Category      = "p"
            Mod           = "jafar"
            Status        = "good"
        }),               
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-BybitApi"
            Alias         = "gba"
            Description   = "Get Bybit API"
            Category      = "i"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-ServerTime"
            Alias         = "gst"
            Description   = "Get server time"
            Category      = "i"
            Mod           = "jafar"
            Status        = "good"
        }),               
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-Announcement"
            Alias         = "gan"
            Description   = "Get announcement"
            Category      = "i"
            Mod           = "jafar"
            Status        = "good"
        }),               
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-InitialMargin"
            Alias         = "gim"
            Description   = "Get initial margin"
            Category      = "c"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-MaintenanceMargin"
            Alias         = "gmm"
            Description   = "Get maintenance margin"
            Category      = "c"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-UnrealizedLoss"
            Alias         = "gul"
            Description   = "Get unrealized loss"
            Category      = "c"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-LiquidationPrice"
            Alias         = "glp"
            Description   = "Get liquidation price"
            Category      = "c"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-FeeToOpen"
            Alias         = "gfo"
            Description   = "Get fee to open"
            Category      = "c"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-PositionMargin"
            Alias         = "gpm"
            Description   = "Get position margin"
            Category      = "c"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-FeeToClose"
            Alias         = "gfc"
            Description   = "Get fee to close"
            Category      = "c"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-UnrealizedPNL"
            Alias         = "gup"
            Description   = "Get unrealized profit and loss"
            Category      = "c"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-OrderCost"
            Alias         = "goc"
            Description   = "Get order cost"
            Category      = "c"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-AverageEntryPrice"
            Alias         = "gae"
            Description   = "Get average entry price"
            Category      = "c"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-UnrealizedPNLPercentage"
            Alias         = "gup"
            Description   = "Get unrealized profit/loss percentage"
            Category      = "c"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-CurrencyObject"
            Alias         = "gco"
            Description   = "Get currency object USD/BTC"
            Category      = "u"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-FundingRate"
            Alias         = "gfr"
            Description   = "Get current funding rate"
            Category      = "i"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-FundingFee"
            Alias         = "gff"
            Description   = "Get funding fee for a deal"
            Category      = "i"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-PositionValue"
            Alias         = "gpv"
            Description   = "Get value of a position"
            Category      = "i"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-ProjectedFees"
            Alias         = "gpf"
            Description   = "Get projected fees of a deal"
            Category      = "i"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-ProjectedPNL"
            Alias         = "gpp"
            Description   = "Get projected PNL for a deal"
            Category      = "o"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-BankruptcyPrice"
            Alias         = "gbp"
            Description   = "Get bankruptcy price"
            Category      = "c"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-BtcPosition"
            Alias         = "gbp"
            Description   = "Get position"
            Category      = "o"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Set-BybitLeverage"
            Alias         = "sbl"
            Description   = "Set leverage for orders"
            Category      = "s"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-BtcOrder"
            Alias         = "gbo"
            Description   = "Get all active orders"
            Category      = "o"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "New-BybitOrder"
            Alias         = "nbo"
            Description   = "Create new buy/sell order"
            Category      = "o"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-WalletRecords"
            Alias         = "gwr"
            Description   = "Get wallet records"
            Category      = "a"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-WithdrawHistory"
            Alias         = "gwh"
            Description   = "Get withdraw history"
            Category      = "a"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Remove-BybitOrder"
            Alias         = "rbo"
            Description   = "Cancel all orders"
            Category      = "o"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-Formatting"
            Alias         = "gf"
            Description   = "Format text to price/percent"
            Category      = "u"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Invoke-Jafar"
            Alias         = "ij"
            Description   = "Start jafar app"
            Category      = "u"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Close-BtcPosition"
            Alias         = "cbp"
            Description   = "Close a position"
            Category      = "o"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "New-Deal"
            Alias         = "nd"
            Description   = "Create order on Bybit"
            Category      = "o"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-BybitClosedPNL"
            Alias         = "pnl"
            Description   = "Get closed PNL"
            Category      = "a"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Set-DealStops"
            Alias         = "sds"
            Description   = "Set TP and SL stops"
            Category      = "s"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Set-StopHuntTrap"
            Alias         = "sst"
            Description   = "Set trap for Stop Hunt"
            Category      = "s"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-BtcSide"
            Alias         = "gbs"
            Description   = "Get current Buy or Sell direction"
            Category      = "i"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-BybitLeverage"
            Alias         = "gbl"
            Description   = "Get current leverage"
            Category      = "i"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-DealStatus"
            Alias         = "gds"
            Description   = "Get current deal stats"
            Category      = "i"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-RiskRewardObject"
            Alias         = "grr"
            Description   = "Get prices for a risk/reward ratio"
            Category      = "c"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-DealObject"
            Alias         = "gdo"
            Description   = "Get deal object"
            Category      = "u"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Set-BtcSide"
            Alias         = "sbs"
            Description   = "Set side to buy or sell"
            Category      = "s"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-Deal"
            Alias         = "gd"
            Description   = "Get current deal"
            Category      = "o"
            Mod           = "jafar"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "ConvertTo-SignedString"
            Alias         = "css"
            Description   = "Convert number to string with sign (+/-)"
            Category      = "u"
            Mod           = "jafar"
            Status        = "good"
        }),                                         
        $(New-Object -TypeName psobject -Property @{
            Name          = "New-BadgerObject"
            Alias         = "no"
            Description   = "Create new object of orders/safety orders"
            Category      = "o"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-BadgerObject"
            Alias         = "bo"
            Description   = "Get a badger object"
            Category      = "o"
            Mod           = "jafar"
            Status        = "bad"
        }),     
        $(New-Object -TypeName psobject -Property @{
            Name          = "New-Deal"
            Alias         = "nd"
            Description   = "New deal"
            Category      = "o"
            Mod           = "jafar"
            Status        = "bad"
        }),
        $(New-Object -TypeName psobject -Property @{
            Name          = "Get-BadgerDeal"
            Alias         = "bd"
            Description   = "Get badger deals"
            Category      = "o"
            Mod           = "jafar"
            Status        = "bad"
        })   
    )        
    function New-Deal {
        param(
            [Parameter(Mandatory=$true)]
            [psobject]$dealObj,
            [switch]$noStopLoss,
            [switch]$noTakeProfit,
            [switch]$raw
        )

        Clear-AllVars -vnames initArgs,finalArgs,sortedArgs,toSign,jsonArgs

        $endpoint                       = "https://api.bybit.com/v2/private/order/create"
        $global:bybit_api               = Get-Cred bybit_api
        $global:bybit_sec               = Get-Cred bybit_sec

        [hashtable]$global:initArgs = @{
            'api_key'           =   $bybit_api
            'side'              =   $dealObj.side 
            'symbol'            =   "BTCUSD"
            'time_in_force'     =   "GoodTillCancel"
            'qty'               =   $dealObj.orderVol
            'order_type'        =   $dealObj.orderType
            'take_profit'       =   [string][math]::Round($dealObj.tp, 0)
            'stop_loss'         =   [string][math]::Round($dealObj.sl, 0)
        }          

        if ($dealObj.orderType -eq 'Limit') {
            $initArgs.Add('price', [string]$dealObj.orderPrice)
        }

        if ($noStopLoss) {
            $initArgs.Remove('stop_loss')
        }
        if ($noTakeProfit) {
            $initArgs.Remove('takeProfit')
        }

        $timestamp                       = Get-Date | ConvertTo-UnixTimestamp
        $initArgs.Add('timestamp', $timestamp)

        $global:sortedArgs               = [System.Collections.SortedList]$initArgs
        $global:finalArgs                = $sortedArgs.Keys | Foreach-Object {
            "$_=$($sortedArgs.item($_))"
        }
        $global:toSign                   = $finalArgs -join '&'
        $global:signature                = Get-Signature -message $toSign -secret $bybit_sec 
        $initArgs.Add('sign',$signature)

        $global:jsonArgs                 = $initArgs | ConvertTo-Json
        $global:res = Invoke-RestMethod -Uri $endpoint -Method POST -Body $jsonArgs -ContentType 'application/json'
        $res.result | Select-Object side,qty,symbol,price,take_profit,stop_loss,reject_reason | Format-Table -AutoSize  | Write-HostColored -PatternColorMap @{'side'='DarkCyan';'symbol'='DarkCyan';'qty'='DarkCyan';'price'='DarkCyan';'take_profit'='DarkGreen';'stop_loss'='DarkRed';'reject_reason'='DarkYellow'} 
    }
    function New-BadgerObject {
        param(
            [float]$orderPrice,

            [string]$side                   = 'Sell',
            [int]$orderVol                  = 1000,
            [string]$orderType              = 'Limit',
            [int]$riskReward                = 3,
            [int]$safetyBuf                 = 20,
            [int]$safetyDev                 = 20,
            [int]$safetyVolScale            = 4,
            [float]$safetyStepScale         = 1,
            [int]$leverage                  = 50,

            [switch]$last,
            [switch]$badger

        )
        if ($last){
            $orderPrice             = $bo.safeDevPrice
            $orderVol               = $bo.safeVol
            $orderType              = "Limit"
            $side                   = $bo.side
            $riskReward             = $bo.riskRew
            $safetyBuf              = $bo.safeBuf
            $safetyDev              = $bo.safeDev
            $lastTotalVol           = $bo.totalVol
            $lastAvgPrice           = $bo.avgPrice
            $safetyStepScale        = $bo.safeStepScale 
            $safetyVolScale         = $bo.safeVolScale 
            $leverage               = $bo.ordLev 
        }
        else {            
            $lastTotalVol           = 0
            $lastAvgPrice           = $bo.orderPrice
            if (!($orderPrice)){
                $orderPrice         = Get-Price
            }
        }

        $liqPrice                   = Get-LiquidationPrice -leverage $leverage -side $side -avgEntryPrice $orderPrice 
        $safetyVol                  = $orderVol * $safetyVolScale
        if ($side -eq 'Buy'){
            $sl                     = $orderPrice - ( $riskRew * 5  )
            $tp                     = $orderPrice + ( $riskRew * 53 )
            $safetyBufPrice         = $liqPrice + $safetyBuf
            $safetyDevPrice         = $orderPrice - ($safetyDev * $safetyStepScale)
        }
        else {
            $sl                     = $orderPrice + ( $riskRew * 5 )
            $tp                     = $orderPrice - ( $riskRew * 53 )
            $safetyBufPrice         = $liqPrice - $safetyBuf
            $safetyDevPrice         = $orderPrice + ($safetyDev * $safetyStepScale)
        }

        $avgEntryPrice              = Get-AverageEntryPrice -first $safetyVol,$safetyDevPrice -last $lastTotalVol,$lastAvgPrice  
        
        $avgPrice                   = [math]::Round($aep.avgPrice, 0)
        $totalVol                   = $avgEntryPrice.totalorderVol

        $global:bo = New-Object -TypeName psobject -Property @{
                orderPrice              = $orderPrice
                orderVol                = $orderVol
                orderType               = $orderType
                orderLev                = $leverage
                tp                      = $tp
                sl                      = $sl
                side                    = $side
                riskRew                 = $riskReward 
                initMargin              = 0
                pProfit                 = 0
                pLoss                   = 0
                pFees                   = 0
                liqPrice                = $liqPrice
                safeVol                 = $safetyVol
                safeBuf                 = $safetyBuf
                safeBufPrice            = $safetyBufPrice
                safeDev                 = $safetyDev 
                safeDevPrice            = $safetyDevPrice
                safeVolScale            = $safetyVolScale
                safeStepScale           = $safetyStepScale 
                safeStep                = $safetyStep
                totalVol                = $totalVol
                avgPrice                = $avgPrice
        }

        $po                         = Get-ProjectedPNL -dealObj $bo -o
        $pProfit                    = $po.pProfit
        $pLoss                      = $po.pLoss 
        $pFees                      = $po.pFees 
        $initMargin                 = $po.initMargin

        $bo.pProfit                 = [Math]::Round($pProfit, 0)
        $bo.pLoss                   = [Math]::Round($pLoss, 0)
        $bo.pFees                   = [Math]::Round($pFees, 0)
        $bo.initMargin              = [Math]::Round($initMargin, 0)


        $bo | Select-Object orderVol,orderPrice,orderType,orderLev,side,pProfit,pLoss,pFees,riskRew,initMargin,tp,sl,safeVol,safeDev,safeDevPrice,safeBuf,safeBufPrice,liqPrice,avgPrice,totalVol 
    }
    function New-BadgerDeal {
        param(
            [float]$orderPrice,
            [int]$orderVol              = 1000,
            [int]$num                   = 5,
            [string]$side               = 'Sell',
            [int]$safetyBuffer          = 20,
            [int]$safetyPriceDev        = 20,
            [int]$safetyVolScale        = 4,
            [float]$safetyStepScale     = 1,
            [switch]$order
        )


        $global:bd = [System.Collections.ArrayList]::New()
   
        if ($orderPrice){
            $first = New-Object -orderPrice $orderPrice -orderVol $orderVol -safetyBuffer $safetyBuffer -safetyPriceDev $safetyPriceDev -safetyVolScale $safetyVolScale -safetyStepScale $safetyStepScale -badger
        }
        else {
            $first = New-Object -orderVol $orderVol -safetyBuffer $safetyBuffer -safetyPriceDev $safetyPriceDev -safetyVolScale $safetyVolScale -safetyStepScale $safetyStepScale -badger
        }
        [void]$bd.Add($first)
        
        $addOrders                    = $num - 2
        0..$addOrders | Foreach-Object {
            $obj = New-Object -last -badger
            [void]$bd.Add($obj)
        }
        if ($order){
            Foreach ($o in $obj){
                New-Deal -dealObj $o -noStopLoss
            }
        }
        $bd | Select-Object orderVol,orderPrice,totalVol,avgPrice,pProfit,pLoss,safeVol,safeDevPrice,safeBufPrice,liqPrice | Format-Table -AutoSize  | Write-HostColored -PatternColorMap @{'totalVol'='Cyan';'orderVol'='DarkCyan';'avgPrice'='Cyan';'liqPrice'='DarkRed';'orderPrice'='DarkCyan';'pLoss'='DarkGreen';'pProfit'='DarkGreen';'safeBufPrice'='DarkYellow';'safeDevPrice'='DarkYellow';'safeVol'='DarkYellow'} 
    }
    function Get-BadgerDeal {
        param(
            [switch]$o
        )
        if ($o){
            $bd
        }
        else {
            $bd | Select-Object orderVol,orderPrice,totalVol,avgPrice,pProfit,pLoss,safeVol,safeDevPrice,safeBufPrice,liqPrice | Format-Table -AutoSize  | Write-HostColored -PatternColorMap @{'totalVol'='Cyan';'orderVol'='DarkCyan';'avgPrice'='Cyan';'liqPrice'='DarkRed';'orderPrice'='DarkCyan';'pLoss'='DarkGreen';'pProfit'='DarkGreen';'safeBufPrice'='DarkYellow';'safeDevPrice'='DarkYellow';'safeVol'='DarkYellow'} 
        }
    }
    function Get-BadgerObject {
        param(
            [switch]$o
        )
        if ($o){
            $bo
        }
        else {
            $bo | Select-Object orderVol,orderPrice,totalVol,avgPrice,pProfit,pLoss,safeVol,safeDevPrice,safeBufPrice,liqPrice | Format-Table -AutoSize  | Write-HostColored -PatternColorMap @{'totalVol'='Cyan';'orderVol'='DarkCyan';'avgPrice'='Cyan';'liqPrice'='DarkRed';'orderPrice'='DarkCyan';'pLoss'='DarkGreen';'pProfit'='DarkGreen';'safeBufPrice'='DarkYellow';'safeDevPrice'='DarkYellow';'safeVol'='DarkYellow'} 
        }
    }


    ##########    CALCULATION    ############
    #########################################

    function Get-AverageEntryPrice {
        param(
            [array]$first,
            [array]$last

        )
        $firstOut                   =  $first[0] / $first[1]
        $lastOut                    =  $last[0] / $last[1]  
        $totalContractValue         =  $firstOut + $lastOut
        $totalOrderVol              =  $first[0] + $last[0]
        $avgPrice                   =  $totalOrderVol / $totalContractValue 

        New-Object -TypeName psobject -Property @{
            avgPrice             = [math]::Round($avgPrice,2) 
            totalContractValue   = $totalContractValue
            totalOrderVol        = $totalOrderVol
        }    
    }
    function Get-BankruptcyPrice {
        param(
            [Parameter(Mandatory=$true)]
            $avgEntryPrice,

            $leverage = 65,

            [Parameter(Mandatory=$true)]
            [ValidateSet('Buy','Sell','None')]
            [string]$side
        )
        if ($side -eq 'None'){
            $bankruptcyPrice = 0
        }
        if ($side -eq 'Sell'){
            $bankruptcyPrice = $avgEntryPrice * ($leverage / ($leverage + 1))
        }
        if ($side -eq 'Buy') {
            $bankruptcyPrice = $avgEntryPrice * ($leverage / ($leverage - 1))
        }
        return [math]::Round($bankruptcyPrice, 2)

    }
    function Get-UnrealizedPNL {
        param(
            [Parameter(Mandatory=$true)]
            $avgEntryPrice,
            
            [Parameter(Mandatory=$true)]
            $orderVol,

            [float]$closingPrice,

            [Parameter(Mandatory=$true)]
            [ValidateSet('Buy','Sell','None')]
            [string]$side             
        )
        if ($closingPrice -lt 1){
            $closingPrice       = Get-Price 
        }
        if ($side -eq 'Buy'){
            $out                = $orderVol * ( (1/$avgEntryPrice)  -  (1/$closingPrice) ) 
        }
        else {
            $out                = $orderVol * ( (1/$closingPrice)  -  (1/$avgEntryPrice) )
        }
        $unrealizedPNL          = Get-CurrencyObject -btc $out
        return $unrealizedPNL
    }
    function Get-UnrealizedPNLPercentage {
        param(
            [Parameter(Mandatory=$true)]
            $unrealizedPNL,
            
            [Parameter(Mandatory=$true)]
            $posMargin
        )
        $out            = ( $unrealizedPNL / $posMargin ) * 100
        [math]::Round($out, 2)
    }
    function Get-UnrealizedLoss {
        param(
            [Parameter(Mandatory=$true)]
            $maintenanceMargin,

            [Parameter(Mandatory=$true)]
            $initialMargin
        )
        $out            = $initialMargin.btc - $maintenanceMargin.btc
        $outUSD         = ConvertTo-USDollar $out 
        $outBTC         = [math]::Round($out, 5)
        New-Object -TypeName psobject -Property @{
            usd         = $outUSD 
            btc         = $outBTC
        }
    }
    function Get-ProjectedFees {
        param(
            [int]$orderVol,
            [float]$orderPrice,
            [float]$avgEntryPrice,
            [float]$closingPrice,
            [string]$side,
            [switch]$o
        )
        if ($orderPrice -lt 1){
            $orderPrice             = Get-Price 
        }
        if ($avgEntryPrice -lt 1){
            $avgEntryPrice          = $orderPrice
        }
        $leverage                   = Get-BybitLeverage
        $feeToOpen                  = Get-FeeToOpen -orderVol $orderVol -orderPrice $orderPrice 
        $bankruptcyPrice            = Get-BankruptcyPrice -avgEntryPrice $avgEntryPrice -leverage $leverage -side $side 
        $feeToClose                 = Get-FeeToClose -orderVol $orderVol -bankruptcyPrice $bankruptcyPrice 
        $positionValue              = Get-PositionValue -orderVol $orderVol -orderPrice $orderPrice
        $fundingRate                = Get-FundingRate 
        $fundingFee                 = Get-FundingFee -positionValue $positionValue.btc -fundingRate $fundingRate
        $totalFees                  = $feeToOpen.usd + $feeToClose.usd + $fundingFee.usd
        $projectedFees              = Get-CurrencyObject -usd $totalFees
        
        $projObj = New-Object -TypeName psobject -Property @{
            orderVol                = $orderVol
            avgPrice                = $avgEntryPrice
            orderLev                = $leverage
            feeToOpen               = $feeToOpen.usd
            feeToClose              = $feeToClose.usd 
            bankruptcyPrice         = $bankruptcyPrice
            positionValue           = $positionValue.usd
            fundingRate             = $fundingRate
            fundingFee              = $fundingFee.usd
            totalFees               = $totalFees
            pFees                   = $projectedFees
            
        }

        if ($o){
            return $projObj
        }
        else {
            return $projectedFees
        }
    }
    function Get-ProjectedPNL {
        param(
            [psobject]$dealObj,
            [switch]$menu
        )

        $global:initMargin         = Get-InitialMargin -orderVol $dealObj.orderVol -leverage $dealObj.orderLev -orderPrice $dealObj.orderPrice
        $global:projPositionProfit = Get-UnrealizedPNL -avgEntryPrice $dealObj.orderPrice -orderVol $dealObj.orderVol -closingPrice $dealobj.tp -side $dealObj.side
        $global:projPositionLoss   = Get-UnrealizedPNL -avgEntryPrice $dealObj.orderPrice -orderVol $dealObj.orderVol -closingPrice $dealobj.sl -side $dealObj.side
        $global:projFees           = Get-ProjectedFees -orderVol $dealObj.orderVol -orderPrice $dealObj.orderPrice -closingPrice $dealobj.tp -side $dealObj.side 
        $global:projProfit         = $projPositionProfit.usd - $projFees.usd 
        $global:projLoss           = $projPositionLoss.usd - $projFees.usd 

        $global:po = New-Object -TypeName psobject -Property @{
            initMargin              = $initMargin.usd
            projPositionProfit      = $projPositionProfit.usd
            projPositionLoss        = $projPositionLoss.usd
            pFees                   = $projFees.usd
            pLoss                   = [Math]::Round($projLoss, 0)
            pProfit                 = [Math]::Round($projProfit, 0)
            orderVol             = $dealObj.orderVol
            orderPrice              = $dealObj.orderPrice 
            tp                      = $dealobj.tp 
            sl                      = $dealObj.sl
            side                    = $dealObj.side 
        }


        if ($menu) {
            New-Object -TypeName psobject -Property @{
                side                       = $dealObj.side 
                orderVol                   = $dealObj.orderVol
                pProfit                    = ConvertTo-SignedString -number $projProfit 
                pLoss                      = ConvertTo-SignedString -number $projLoss
                initMargin                 = ConvertTo-SignedString -number $initMargin.usd
            } | Select-Object side,orderVol,initMargin,pProfit,pLoss | Format-Table side,orderVol,initMargin, @{
                n="pLoss";e={"{0:N2}" -f $_.pLoss};a="right"}, @{
                    n="pProfit";e={"{0:N2}" -f $_.pProfit};a="right"} -Auto | Write-HostColored -PatternColorMap @{'Sell'='Red';'\('='Red';'Buy'='Green';'\+[\d\.]+'='Green';'\-[\d\.]+'='Red';'[-]{1,}'='DarkCyan'} 
        }
        else {
            return $po
        }
    }
    function ConvertTo-SignedString {
        param(
            $number,
            [switch]$whole
        )
        
        if ($number -gt 0){
            if ($whole){
                $out = "+$([math]::round($number, 0))"
            }
            else {
                $out = "+${number}"
            }
        }
        else {
            [string]$out = $number
        }
        return $out
    }
    function Get-PositionValue {
        param(
            [int]$orderVol,

            [float]$orderPrice

        )
        $out            = $orderVol / $orderPrice 
        $posValue       = Get-CurrencyObject -btc $out
        return $posValue
    }
    function Get-FundingFee {
        param(
            [float]$positionValue,
            [float]$fundingRate
        )
        $out            = $positionValue * $fundingRate 
        $fundingFee     = Get-CurrencyObject -btc $out
        return $fundingFee
    }
    function Get-FeeToClose {

        param(
            [Parameter(Mandatory=$true)]
            [int]$orderVol,

            [Parameter(Mandatory=$true)]
            [float]$bankruptcyPrice
        )
        $out = ($orderVol / $bankruptcyPrice ) * 0.0006
        $feeToClose         = Get-CurrencyObject -btc $out
        return $feeToClose
    }    
    function Get-FeeToOpen {

        param(
            [Parameter(Mandatory=$true)]
            [int]$orderVol,

            [Parameter(Mandatory=$true)]
            [float]$orderPrice
        )
        $out = ($orderVol / $orderPrice ) * 0.0006
        $feeToOpen          = Get-CurrencyObject -btc $out
        return $feeToOpen
    }    
    function Get-DealObject {
        param(
            [switch]$live,

            [int]$orderVol,

            [float]$orderPrice,

            [int]$riskReward = 5,

            [string]$side,

            [switch]$menu,

            [int]$leverage = 65,

            [int]$safetyBuffer = 15,

            [int]$safetyPriceDev

        )

        if ($live){
            $position = Get-BtcPosition -o 
            $global:do = New-Object -TypeName psobject -Property @{
                orderVol        = $position.size
                orderPrice      = [math]::Round($position.entry_price, 2)
                avgPrice        = [math]::Round($position.entry_price, 2)
                orderLev        = $position.orderLev 
                tp              = $position.take_profit
                sl              = $position.stop_loss
                side            = $position.side
                riskRew         = $riskReward
                orderType       = "Market"
                initMargin      = 0
                pProfit         = 0
                pLoss           = 0
                pFees           = 0
                liqPrice        = 0  
                safeBufPrice    = 0
                safeDevPrice    = 0
                safeBuf         = $safetyBuffer
                safeDev         = $safetyPriceDev
            }
        }
        else {
            if ($orderPrice){
                $orderType          = "Limit"
            }
            else {
                $orderType          = "Market"
                $orderPrice         = Get-Price
            }
            if ($orderVol -lt 1){
                $orderVol        = Get-BtcQuantity
            }
            if ($side.length -eq 0){
                $side               = Get-BtcSide 
            }
            $leverage               = Get-BybitLeverage
            $liqPrice               = Get-LiquidationPrice -leverage $leverage -side $side -avgEntryPrice $orderPrice 

            $global:do = New-Object -TypeName psobject -Property @{
                orderVol     = $orderVol
                orderPrice      = $orderPrice 
                orderType       = $orderType
                orderLev        = $leverage
                tp              = 0
                sl              = 0
                side            = $side
                riskRew         = $riskReward
                initMargin      = 0
                pProfit         = 0
                pLoss           = 0
                pFees           = 0
                liqPrice        = $liqPrice
                safeBufPrice    = 0
                safeDevPrice    = 0
                safeBuf         = $safetyBuffer
                safeDev         = $safetyPriceDev
            }
        }

        if ($do.side -eq 'Buy'){
            $do.sl              = $do.orderPrice - ( $do.riskRew * 5  )
            $do.tp              = $do.orderPrice + ( $do.riskRew * 53 )
            $do.safeBufPrice    = $do.liqPrice + $safetyBuffer
            $do.safeDevPrice    = $do.orderPrice - $safetyPriceDev
        }
        elseif ($do.side -eq 'Sell'){
            $do.sl              = $do.orderPrice + ( $do.riskRew * 5 )
            $do.tp              = $do.orderPrice - ( $do.riskRew * 53 )
            $do.safeBufPrice    = $do.liqPrice - $safetyBuffer
            $do.safeDevPrice    = $do.orderPrice + $safetyPriceDev
        }
        else {
            Write-Fail 'No side'
            Break
        }
        $po                     = Get-ProjectedPNL -dealObj $do -o
        $projProfit             = $po.pProfit
        $projLoss               = $po.pLoss 
        $projFees               = $po.pFees 
        $initMargin             = $po.initMargin

        $do.pProfit             = $projProfit
        $do.pLoss               = $projLoss
        $do.pFees               = $projFees
        $do.initMargin          = $initMargin

        if ($menu) {
            New-Object -TypeName psobject -Property @{
                side                       = $dealObj.side 
                orderVol                = $dealObj.orderVol
                projectedProfit            = ConvertTo-SignedString -number $projProfit 
                projectedLoss              = ConvertTo-SignedString -number $projLoss
                initalMargin               = ConvertTo-SignedString -number $initMargin.usd
            } | Select-Object side,orderVol,initialMargin,projectedProfit,projectedLoss | Format-Table side,orderVol,initialMargin, @{
                n="projectedLoss";e={"{0:N2}" -f $_.projectedLoss};a="right"}, @{
                    n="projectedProfit";e={"{0:N2}" -f $_.projectedProfit};a="right"} -Auto | Write-HostColored -PatternColorMap @{'Sell'='Red';'\('='Red';'Buy'='Green';'\+[\d\.]+'='Green';'\-[\d\.]+'='Red';'[-]{1,}'='DarkCyan'} 
        }
        else {
            return $do
        }
    }
    function Get-PositionMargin {
        param(
            $initialMargin,
            $feeToClose
        )
        $out        = $initialMargin.btc + $feeToClose.btc
        $positionMargin         = Get-CurrencyObject -btc $out
        return $positionMargin
    }
    function Get-InitialMargin {
        param(
            [Parameter(Mandatory=$true)]
            $orderVol,

            [Parameter(Mandatory=$true)]
            $orderPrice,

            [Parameter(Mandatory=$true)]
            $leverage
        )
        $out = $orderVol / ($orderPrice * $leverage)
        $initialMargin      = Get-CurrencyObject -btc $out
        return $initialMargin
    }
    function Get-MaintenanceMargin {
        param(
            [Parameter(Mandatory=$true)]
            $orderVol,

            [Parameter(Mandatory=$true)]
            $orderPrice,

            [ValidateSet(0.5, 1)]
            $maintenanceMarginRate     = 0.5
        )
        $out            = ($orderVol/$orderPrice) * ($maintenanceMarginRate/100)
        $maintenanceMargin      = Get-CurrencyObject -btc $out
        return $maintenanceMargin
    }  
    function Get-Deal {
        param(
            [switch]$o
        )
        $currentPrice = Get-Price
        $info = Get-BtcPosition -o
        
        if ($info){
            if ($info.side -eq 'Buy'){
                $dollarsFromStopLoss = $currentPrice - $info.stop_loss 
                $dollarsFromTakeProfit = $info.take_profit - $currentPrice 
            }
            else {
                $dollarsFromTakeProfit = $currentPrice - $info.take_profit
                $dollarsFromStopLoss = $info.stop_loss - $currentPrice 
            }
            $signeduPNL             =  ConvertTo-SignedString -number $(ConvertTo-USDollar -btc $info.unrealised_pnl) -whole
            
            if ($info.unrealised_pnl -gt 0){
                $uPNLpProfit            = "${signeduPNL} / $([math]::round($do.pProfit, 0))"
                $uPNLpLoss              = [math]::round($do.pLoss, 0)
            }
            else {
                $uPNLpProfit            = [math]::round($do.pProfit, 0)
                $uPNLpLoss              = "${signeduPNL} / $([math]::round($do.pLoss, 0))"
            }

            $posMargin           = ConvertTo-USDollar -btc $info.position_margin -whole
            $feeToClose          = ConvertTo-USDollar -btc $info.occ_closing_fee -whole   
            $balance             = ConvertTo-USDollar -btc $info.wallet_balance -whole

            $obj = New-Object -TypeName psobject -Property @{
                posMargin           = $posMargin
                feeToClose          = $feeToClose
                balance             = $balance  
                fromStopLoss        = [math]::Round($dollarsFromStopLoss, 0)
                fromTakeProfit      = [math]::Round($dollarsFromTakeProfit, 0)
                uPNLpProfit         = $uPNLpProfit
                uPNLpLoss           = $uPNLpLoss
                side                = $do.side
            }
        }
        if ($o) {
            $obj 
        }
        else {
            $obj | Format-Table side, balance, @{
            n="posMargin";e={"{0:N0}" -f $_.posMargin};a="right"}, fromStopLoss, fromTakeProfit, @{
                    n="uPNL / pLoss";e={"{0:N0}" -f $_.uPNLpLoss};a="right"}, @{
                    n="uPNL / pProfit";e={"{0:N0}" -f $_.uPNLpProfit};a="right"} -Auto | Write-HostColored -PatternColorMap @{'Sell'='Red';'\('='Red';'Buy'='Green';'\+[\d\.]+'='Green';'\-[\d\.]+'='Red';'[-]{1,}'='DarkCyan'}
        }
    }
    function Get-OrderCost {
        param(
            [Parameter(Mandatory=$true)]
            $initialMargin,

            [Parameter(Mandatory=$true)]
            $feeToOpen,

            [Parameter(Mandatory=$true)]
            $feeToClose
        )

        $out            = $initialMargin.btc + $feeToOpen.btc + $feeToClose.btc
        $orderCost      = Get-CurrencyObject -btc $out
        return $orderCost       
    }  
    
    ##########      UTILITIES    ############
    #########################################

    function Get-ByBitApi {

        param(
            [string]$endpoint            ,
            [string]$arguments           ,
            [string]$method              = "GET",
            [switch]$raw
        )

        $global:res = ""
        $global:bybit_api               = Get-Cred bybit_api
        $global:bybit_sec               = Get-Cred bybit_sec
        $global:newUri                 = "https://api.bybit.com/${endpoint}"
        
        [hashtable]$global:initArgs = @{
            'api_key'           =   $bybit_api
            'recv_window'       =   "8000"
        }  

        if ($arguments) {
            $global:parsedArgs = $arguments.split('&')
            foreach ($a in $parsedArgs){
                $argKey = $a.split('=')[0]
                $argVal = $a.split('=')[1]
                $initArgs.Add($argKey, $argVal)  
            }
        }
        $timestamp                = Get-Date | ConvertTo-UnixTimestamp
        $initArgs.Add('timestamp', $timestamp)

        $global:sortedArgs               = [System.Collections.SortedList]$initArgs
        $global:finalArgs                = $sortedArgs.Keys | Foreach-Object {
            "$_=$($sortedArgs.Item($_))"
        }
        $global:toSign                   = $finalArgs -join '&'
        $global:signature                = Get-Signature -message $toSign -secret $bybit_sec 
        $initArgs.Add('sign',$signature)

        $global:finalArgs                = $initArgs.Keys | Foreach-Object { "$_=$($initArgs.Item($_))"}
        $global:uriArgs                  = $finalArgs -join '&'

        $newUri                          = "${newUri}?${uriArgs}"
        $global:res = Invoke-RestMethod -Uri $newUri

        if ($raw){
            $res
        }
        else {
            $res.result
        }
    }
            
    class MyMenuOption {
        [String]$DisplayName
        [ScriptBlock]$Script

        [String]ToString() {
            Return $This.DisplayName
        }
    }
    function New-MenuItem([String]$DisplayName, [ScriptBlock]$Script, [string]$varname) {
        $MenuItem = [MyMenuOption]::new()
        $MenuItem.DisplayName = $DisplayName
        $MenuItem.Script = $Script
        $MenuItem.Output = $varname
        Return $MenuItem
    }


    function ConvertTo-BTC {
        param(
            [Parameter(Mandatory=$true)]
            [float]$quantity,

            [float]$symbolPrice 
        )
        if ($symbolPrice){
            $out                = $quantity / $symbolPrice
            "{0:N5}" -f $out

        }
        else {
            $symbolPrice        = Get-Price 
            $out                = $quantity / $symbolPrice
            "{0:N5}" -f $out
        }
    }
    function ConvertTo-USDollar {
        param(
            [Parameter(Mandatory=$true)]
            [float]$btc,

            [float]$price, 

            [switch]$whole
        )
        if ($price){
            $out                = $price * $btc
        }
        else {
            $symbolPrice        = Get-Price 
            $out                = $btc * $symbolPrice
        }
        if ($whole){
            return [Math]::Round($out, 0)
        }
        else {
            return [Math]::Round($out, 2)
        }
    }
    function Get-Signature {
        param(
            [string]$secret,
            [string]$message
        )
        $global:secret_b                       = [Text.Encoding]::UTF8.GetBytes($secret)
        $global:message_b                      = [Text.Encoding]::UTF8.GetBytes($message)
        $global:hmacsha                        = New-Object System.Security.Cryptography.HMACSHA256
        $global:hmacsha.key                    = $secret_b
        $global:signatureBytes                 = $hmacsha.ComputeHash($message_b)
        # $global:signature                      = [Convert]::ToBase64String($signatureBytes)
        $signature                             = Write-Output $signatureBytes -NoEnumerate | ConvertTo-HexString -Delimiter ''
        return $signature
    }
    function Get-JafarTools {
        param(
        [string]$type = 'default'
        )
    
        Switch ($type) {
    
            'a'          {  Get-JafarCmdlets 'a'       }
            'c'          {  Get-JafarCmdlets 'c'      }
            'i'          {  Get-JafarCmdlets 'i'       }
            'o'          {  Get-JafarCmdlets 'o'      }
            'p'          {  Get-JafarCmdlets 'p'      }
            's'          {  Get-JafarCmdlets 's'      }
            'u'          {  Get-JafarCmdlets 'u'      }
        default        {
            
            Write-Host "`n"
            Write-Host -Fore DarkCyan " ...:::" -NoNewLine;Write-Host "`tUsage:   " -NoNewLine;
                Write-Host -Fore DarkCyan "ja " -NoNewLine;Write-Host -Fore DarkGray "[category]"
            Write-Host "`n"
    
            [pscustomobject]$options = [ordered]@{
                'a'       = "Assets`t >   Info on assets"
                'c'       = "Calc  `t >   Order calculations"
                'i'       = "Info `t >   Query for information"
                'o'       = "Orders`t >   Trading tools"
                'p'       = "Prices`t >   Get prices"
                's'       = "Set `t >   Set conditions"
                'u'       = "Utils  `t >   Support functions"
            }
    
            $options.Keys | Foreach-Object {
            Write-Host -Fore DarkCyan "`tja " -NoNewLine;
            Write-Host -Fore DarkGray "$_" -NoNewLine ; Write-Host "`t`t$($options.Item("$_"))"
            }
            Write-Host "`n"
        }
        }
    }
    function Get-JafarCmdlets {
    param(
        [string]$type
    )

    if (!$type) {
        $cmdlets = $JafarCmdlets | Sort-Object Name
    }
    else {
        $cmdlets = $JafarCmdlets | Where-Object Category -eq $type | Sort-Object Name
    }
    Write-Host "`n"
    $cmdlets | Select-Object Name,Alias,Description | Format-Table -Auto  |  Write-HostColored -PatternColorMap @{
        'Name'          ='DarkGray'
        'Alias'         ='DarkGray'
        'Description'   ='DarkGray'
        '-'             ='DarkCyan'} 
    # Write-Host -Fore DarkGray ("`tCmdlet`t`t`t`t") -NoNewLine;Write-Host -Fore DarkGray " Alias`t`t" -Nonewline;
    #     Write-Host -Fore DarkGray " Description" 
    # Write-Host "`t$('-' * 24)`t $('-' * 5)`t`t $('-' * 30)"
    # $cmdlets | ForEach-Object {
    #     if ($_.Name.Length -gt 23){
    #     Write-Host -Fore Gray ("`t" + $_.Name + "`t ") -NoNewLine;Write-Host -Fore DarkCyan $_.Alias -Nonewline
    #     Write-Host -Fore Gray (" `t`t " + $_.Description ) 
    #     }    
    #     elseif ($_.Name.Length -gt 15){
    #     Write-Host -Fore Gray ("`t" + $_.Name + "`t`t ") -NoNewLine;Write-Host -Fore DarkCyan $_.Alias -Nonewline
    #     Write-Host -Fore Gray (" `t`t " + $_.Description ) 
    #     }
    #     elseif ($_.Name.Length -gt 7){
    #     Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t ") -NoNewLine;Write-Host -Fore DarkCyan $_.Alias -Nonewline
    #     Write-Host -Fore Gray (" `t`t " + $_.Description ) 
    #     }
    #     else {
    #     Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t`t ") -NoNewLine;Write-Host -Fore DarkCyan $_.Alias -Nonewline
    #     Write-Host -Fore Gray (" `t`t " + $_.Description ) 
    #     }
    # }
    Write-Host "`n"
    }  
    function Get-Formatting {
        param(
            $price,
            $percent,
            $number,
            $time
        )

        if ($price){
            if ($price.getType().Name -ne 'Double'){
                $price = [Convert]::ToDouble($price)
            }
            if ($price -lt 1){
                return "{0:C4}" -f $price
            }
            else {
                return "{0:C}" -f $price
            }
        }
        elseif ($percent){
            if ($percent.getType().Name -ne 'Double'){
                $percent = [Convert]::ToDouble($percent)
            }
            if ($percent -lt 1){
                return "{0:P2}" -f $percent
            }
            else {
                return "{0:P}" -f $percent
            } 
        }    
        elseif ($number){
            if ($number.getType().Name -ne 'Double'){
                $number = [Convert]::ToDouble($number)
            }
            if ($number -lt 1){
                return "{0:N2}" -f $number
            }
            else {
                return "{0:N}" -f $number
            }
        }
        elseif ($time){
            (Get-Date) -f "{0:hh\:mm\:ss}"
        }
        else{}    
            
    }
    function Get-CurrencyObject {
        param(
            [float]$btc,
            [float]$usd
        )
        if ($btc){
            $outUSD         = ConvertTo-USDollar $btc 
            $outBTC         = [math]::Round($btc, 5)
            $currencyObj    = New-Object -TypeName psobject -Property @{
                usd         = $outUSD 
                btc         = $outBTC
            }    
            return $currencyObj
        }
        if ($usd){
            $outBTC         = ConvertTo-BTC $usd 
            $outUSD         = [math]::Round($usd, 2)
            $currencyObj    = New-Object -TypeName psobject -Property @{
                usd         = $outUSD 
                btc         = $outBTC
            }    
            return $currencyObj
        }
    }
        

    ##########        INFO       ############
    #########################################
      
    function Get-DealStatus {
        param(
            $avgEntryPrice,

            [switch]$live 
        )

        if ($live){
            $info           = Get-BtcPosition -o 

            if ($pos.Margin){

                $side               = $info.side 
                $leverage           = $info.orderLev 
                $avgEntryPrice      = $info.entry_price 
                $orderVol        = $info.size
                $posMargin          = $info.position_margin 
                $liqPrice           = $info.liq_price 
                $bankruptcyPrice    = $info.bust_price 
                $feeToClose         = $info.occ_closing_fee
                $takeProfit         = $info.take_profit 
                $stopLoss           = $info.stop_loss
                $walletBalance      = $info.wallet_balance 
                $unrealizedPNL      = $info.unrealised_pnl 

                Write-Info 'live fork'
                $feeToOpen          = Get-FeeToOpen -orderVol $orderVol -orderPrice $orderPrice 
                $initialMargin      = Get-InitialMargin -orderVol $orderVol -orderPrice $orderPrice -leverage $leverage
                $orderCost          = Get-OrderCost -initialMargin $initialMargin -feeToOpen $feeToOpen -feeToClose $feeToClose 
                $uplPercentage      = Get-UnrealizedPNLPercentage -unrealizedPNL $unrealizedPNL -posMargin $posMargin 
                $maintenanceMargin  = Get-MaintenanceMargin -orderVol $orderVol -orderPrice $orderPrice 
                $unrealizedLoss     = Get-UnrealizedLoss -initialMargin $initialMargin -maintenanceMargin $maintenanceMargin
            }
            else {
                Write-Info 'live fork - else'
            }
        }
        elseif ($avgEntryPrice) {
            $side               = Get-BtcSide 
            $leverage           = Get-BybitLeverage
            $orderPrice         = Get-Price 
            $orderVol        = 5000
        
            Write-Info 'else fork'

            $liqPrice           = Get-LiquidationPrice -avgEntryPrice $avgEntryPrice -leverage $leverage -side $side 
            $feeToOpen          = Get-FeeToOpen -orderVol $orderVol -orderPrice $orderPrice 
            $bankruptcyPrice    = 0 #Get-BankruptcyPrice -avgEntryPrice $avgEntryPrice -leverage $leverage -side $side 
            $feeToClose         = Get-FeeToClose -orderVol $orderVol -bankruptcyPrice $bankruptcyPrice 
            $initialMargin      = Get-InitialMargin -orderVol $orderVol -orderPrice $orderPrice -leverage $leverage
            $posMargin          = Get-PositionMargin -initialMargin $initialMargin -feeToClose $feeToClose 
            $orderCost          = Get-OrderCost -initialMargin $initialMargin -feeToOpen $feeToOpen -feeToClose $feeToClose 
            $unrealizedPNL      = Get-UnrealizedPNL -avgEntryPrice $avgEntryPrice -orderVol $orderVol -side $side
            $uplPercentage      = Get-UnrealizedPNLPercentage -unrealizedPNL $unrealizedPNL -posMargin $posMargin
            $maintenanceMargin  = Get-MaintenanceMargin -orderVol $orderVol -orderPrice $orderPrice 
            $unrealizedLoss     = Get-UnrealizedLoss -initialMargin $initialMargin -maintenanceMargin $maintenanceMargin
        }
        else {

        }

        $global:statusObj          = New-Object -TypeName psobject -Property @{
            avgPrice                = $avgEntryPrice 
            orderPrice              = $orderPrice
            side                    = $side 
            orderLev                = $leverage 
            orderVol             = $orderVol 
            liqPrice                = $liqPrice 
            feeToOpen               = $feeToOpen.usd
            bankruptcyPrice         = $bankruptcyPrice
            feeToClose              = $feeToClose.usd 
            initMargin              = $initialMargin.usd
            orderCost               = $orderCost.usd
            posMargin               = $posMargin.usd 
            unrealizedPNL           = $unrealizedPNL.usd
            uplPercentage           = $uplPercentage 
            maintMargin             = $maintenanceMargin.usd
            unrealizedLoss          = $unrealizedLoss.usd
            tp                      = $takeProfit
            sl                      = $stopLoss
            walletBalance           = $walletBalance 
        }
        $statusObj 
    }
    function Get-LiquidationPrice {
        param(
            [Parameter(Mandatory=$true)]
            $avgEntryPrice,

            $leverage = 65,

            $maintenanceMarginRate     = 0.5,

            [Parameter(Mandatory=$true)]
            [ValidateSet('Buy','Sell','None')]
            [string]$side            
        )

        $top =  $avgEntryPrice * $leverage
        
        if ($side -eq 'Sell') {
            $one =  $leverage - 1
            $two =  ($maintenanceMarginRate/100) * $leverage
            $lp  =  $top / ($one + $two)
        }
        else {
            $one =  $leverage + 1
            $two =  ($maintenanceMarginRate/100) * $leverage
            $lp  =  $top / ($one - $two)
        }        
        [math]::Round($lp,0)
    }
    
    ##########        SET        ############
    #########################################

    function Set-BybitLeverage {
        param(
            $leverage = 65   
        )

        $endpoint               = "v2/private/position/leverage/save"
        $arguments              = "symbol=BTCUSD&leverage=$leverage"
    
        $res                    = Get-BybitApi -endpoint $endpoint -arguments $arguments -method POST -raw
        $res
    }
    function Set-BtcSide {
        param(
            [string]$side
        )
        New-TimeDatedVar -nam "btcSide" -val $side
    }
    function Set-BybitQuantity {
        param(
            [string]$orderVol
        )
        New-TimeDatedVar -nam "btcQuantity" -val $orderVol
    }
    function Set-DealStops {
        param(
            [Parameter(Mandatory=$true)]
            [psobject]$dealObj,

            [switch]$raw
        )

        $endpoint               = "https://api.bybit.com/v2/private/position/trading-stop"
        $bybit_api              = Get-Cred bybit_api
        $bybit_sec              = Get-Cred bybit_sec
        $timestamp              = Get-Date | ConvertTo-UnixTimestamp


        [hashtable]$global:initArgs = @{
            'api_key'           =   $bybit_api
            'timestamp'         =   $timestamp
            'symbol'            =   "BTCUSD"
            'time_in_force'     =   "GoodTillCancel"
            'order_type'        =   "Limit"
            'qty'               =   $dealObj.orderVol
            'take_profit'       =   [string]$dealobj.tp
            'stop_loss'         =   [string]$dealObj.sl
            'side'              =   $dealObj.side 
        }  

        $global:sortedArgs               = [System.Collections.SortedList]$initArgs
        $global:finalArgs                = $sortedArgs.Keys | Foreach-Object {
            "$_=$($sortedArgs.item($_))"
        }
        $global:toSign                   = $finalArgs -join '&'
        $global:signature                = Get-Signature -message $toSign -secret $bybit_sec 
        $sortedArgs.Add('sign',$signature)
        $global:jsonArgs                 = $sortedArgs | ConvertTo-Json
        $global:res = Invoke-RestMethod -Uri $endpoint -Method POST -Body $jsonArgs -ContentType 'application/json'

        if ($raw){
            $res.result
        }
        else{
            $dealObj | Select-Object side,orderVol,sls,tp | Format-Table -Auto 
        }        
    }
    function Set-StopHuntTrap {
    }

    ##########        INFO       ############
    #########################################
      
    function Get-Price {
        param(
            [switch]$raw
        )
        $res = Get-ByBitApi -endpoint 'v2/public/tickers' -arguments 'symbol=BTCUSD' -raw
        
        if ($raw) {
            $res
        }
        else {
            $out = ($res.result | Where-Object symbol -eq 'BTCUSD').last_price
            [math]::round($out,0)
        }
    }
    function Get-FundingRate {
        $out           = Get-BybitApi -endpoint 'v2/public/funding/prev-funding-rate' -arguments 'symbol=BTCUSD'
        $fundingRate   = [float]$out.funding_rate 
        return $fundingRate
    }
    function Get-ServerTime {
        param(
            [switch]$raw
        )
        $global:response = Get-BybitApi 'v2/public/time' -raw 
        if ($raw){
            $response
        }
        else {
            $response.time_now
        }
    }
    function Get-Announcement {
        param(
            [switch]$raw
        )
        $global:response = Get-BybitApi 'v2/public/announcement' -raw 
        if ($raw){
            $response
        }
        else {
            $response.ret_msg
        }
    }   

    ##########       ASSETS      ############
    #########################################

    function Get-WalletRecords {
        param(
            [switch]$raw,
            [switch]$o
        )
        $global:response       = Get-ByBitApi -endpoint 'v2/private/wallet/fund/records' -raw

        if ($raw){
            $response
        }
        elseif ($o) {
            $response.result.data
        }
        else {
            $response.result.data | Select-Object  amount,coin,type,wallet_balance | Format-Table -Auto 
        }
    }
    function Get-WithdrawHistory {
        param(
            [switch]$raw,
            [switch]$o
        )
        $global:response       = Get-ByBitApi -endpoint 'v2/private/wallet/withdraw/list' -raw
        if ($raw){
            $response
        }
        elseif ($o) {
            $response.result.data
        }
        else {
            $response.result.data | Select-Object  amount,coin,status | Format-Table -Auto 
        }
    }
    function Get-BybitBalance {
        param(
            [switch]$raw,
            [switch]$o
        )
        $endpoint                       = "https://api.bybit.com/v2/private/wallet/balance"
        $global:bybit_api               = Get-Cred bybit_api
        $global:bybit_sec               = Get-Cred bybit_sec
        [int64]$global:timestamp        = Get-Date | ConvertTo-UnixTimestamp

        [hashtable]$global:initArgs = @{
            'api_key'           =   $bybit_api
            'timestamp'         =   $timestamp
            'recv_window'       =   "8000"
        }  
        $global:sortedArgs               = [System.Collections.SortedList]$initArgs
        $global:finalArgs                = $sortedArgs.Keys | Foreach-Object {
            "$_=$($sortedArgs.item($_))"
        }
        $global:toSign                  = $finalArgs -join '&'
        $global:signature               = Get-Signature -message $toSign -secret $bybit_sec 
        $global:uriArgs                 = "${toSign}&sign=${signature}"
        $global:newUri                  = "${endpoint}?${uriArgs}"
        $global:res                     = Invoke-RestMethod -Uri $newUri -Method GET 
        
        if ($raw){
            $res
        }
        else {
            $r                          = $res.result.BTC
            $price                      = Get-Price
            $avail                      = ConvertTo-USDollar -btc $r.available_balance -price $price
            $wallet                     = ConvertTo-USDollar -btc $r.wallet_balance -price $price
            $pos_margin                 = ConvertTo-USDollar -btc $r.position_margin -price $price
            $rpl                        = ConvertTo-USDollar -btc $r.realised_pnl -price $price
            $upl                        = ConvertTo-USDollar -btc $r.unrealised_pnl -price $price
            $cum_rpl                    = ConvertTo-USDollar -btc $r.cum_realised_pnl -price $price
            $equity                     = ConvertTo-USDollar -btc $r.equity -price $price

            $global:out = New-Object -TypeName psobject -Property @{
                available           = $avail
                wallet              = $wallet
                pos_margin          = $pos_margin 
                ___rpl___           = $rpl
                ___upl___           = $upl
                __cum_rpl__         = $cum_rpl
                equity              = $equity 
            }
            $out | Format-Table -Auto |  Write-HostColored -PatternColorMap @{'Buy'='DarkGreen';'Sell'='DarkRed';'-'='DarkCyan'} 
        }
    }
    function Get-BybitClosedPNL {

        param(
            [string]$limit      = "10",
            [string]$page       = "1",
            [switch]$raw,
            [switch]$o
        )
        $endpoint                       = "https://api.bybit.com/v2/private/trade/closed-pnl/list"
        $global:bybit_api               = Get-Cred bybit_api
        $global:bybit_sec               = Get-Cred bybit_sec
        [int64]$global:timestamp        = Get-Date | ConvertTo-UnixTimestamp

        [hashtable]$global:initArgs = @{
            'api_key'           =   $bybit_api
            'timestamp'         =   $timestamp
            'symbol'            =   "BTCUSD"
            'limit'             =   $limit
            'page'              =   $page
        }  

        $global:sortedArgs               = [System.Collections.SortedList]$initArgs
        $global:finalArgs                = $sortedArgs.Keys | Foreach-Object {
            "$_=$($sortedArgs.item($_))"
        }
        $global:toSign                  = $finalArgs -join '&'
        $global:signature               = Get-Signature -message $toSign -secret $bybit_sec 
        $global:uriArgs                 = "${toSign}&sign=${signature}"
        $global:newUri                  = "${endpoint}?${uriArgs}"
        $global:res                     = Invoke-RestMethod -Uri $newUri -Method GET 
    
        if ($raw){
            $res
        }
        elseif ($o){
            $res.result.data
        }
        else {
            $out = New-Object System.Collections.ArrayList 
            Foreach ($r in $res.result.data){
                $dateObj = ConvertFrom-UnixTimestamp -unixTime $r.created_at 
                $obj = New-Object -TypeName psobject -Property @{
                    __day__             = $dateObj.toShortDateString()
                    __time__            = $dateObj.toShortTimeString()
                    __side__            = $r.side 
                    __type__            = $r.order_type
                    __size__            = $r.qty
                    __exec__            = $r.exec_type
                    __entry__           = [Math]::Round($r.avg_entry_price, 0)
                    __exit__            = [Math]::Round($r.avg_exit_price, 0)
                    __lev__             = $r.orderLev 
                    ___pnl___             = ConvertTo-USDollar -btc $r.closed_pnl -price $r.avg_exit_price
                }
                [void]$out.Add($obj)
            }
            return $out | Format-Table __day__,__time__,__side__,__type__,__size__,__exec__,__entry__,__exit__,__lev__, @{
                n="___pnl___";e={$_.___pnl___};a="right"}  -AutoSize | Write-HostColored -PatternColorMap @{
                    'Sell'      = 'DarkGray'
                    '\('        = 'Red'
                    'Buy'       = 'DarkCyan'
                    '-'         = 'DarkCyan'
                    'Market'    = 'DarkMagenta'
                    'Limit'     = 'Blue'
                } -SimpleMatch
        }
    }
    function Get-BtcSide {
        if ($btcSide.length -gt 0) {
            $now = Get-Date
            $then = (Get-Variable -Name "btcSide").CreatedAt 2>$null
            $exp = $then.AddMinutes(15) 
            if ($exp -gt $now) {
                return $btcSide
            }
        }
        $info               = Get-BtcPosition -o 
        if ($info){
            if ($info.side -eq 'None'){
                $answer = Read-Host 'Enter (b) for Buy or (s) for Sell'
                if ($answer -eq 'b'){
                    Set-BtcSide -side 'Buy'
                }
                else {
                    Set-BtcSide -side 'Sell'
                }
            }
            else {
                Set-BtcSide -side $info.side
            }
            return $btcSide 
        }
        else{
            $answer = Read-Host 'Enter (b) for Buy or (s) for Sell'
            if ($answer -eq 'b'){
                Set-BtcSide -side 'Buy'
            }
            else {
                Set-BtcSide -side 'Sell'
            }
            return $btcSide 
        }
    }

    ##########      ORDERS       ############
    #########################################

    function Get-BtcPosition {
        param(
            [switch]$raw,
            [switch]$o
        )
        $endpoint                       = "https://api.bybit.com/v2/private/position/list"
        $global:bybit_api               = Get-Cred bybit_api
        $global:bybit_sec               = Get-Cred bybit_sec
        [int64]$timestamp               = Get-Date | ConvertTo-UnixTimestamp

        [hashtable]$global:initArgs = @{
            'api_key'           =   $bybit_api
            'timestamp'         =   $timestamp
            'recv_window'       =   "8000"
        }  
        $global:sortedArgs               = [System.Collections.SortedList]$initArgs
        $global:finalArgs                = $sortedArgs.Keys | Foreach-Object {
            "$_=$($sortedArgs.item($_))"
        }
        $global:toSign                  = $finalArgs -join '&'
        $global:signature               = Get-Signature -message $toSign -secret $bybit_sec 
        $global:uriArgs                 = "${toSign}&sign=${signature}"
        $global:newUri                  = "${endpoint}?${uriArgs}"
        $global:res                     = Invoke-RestMethod -Uri $newUri -Method GET 
        $global:btc                     = $res.result.data | Where-Object symbol -eq 'BTCUSD'

        if ($raw){
            $res
        }
        elseif ($o){
            $btc
        }
        else {
            $price = Get-Price
            $unrealizedPNL            = ConvertTo-USDollar -btc $btc.unrealised_pnl -price $price 
            $positionMargin           = ConvertTo-USDollar -btc $btc.position_margin -price $price 
            $positionValue            = ConvertTo-USDollar -btc $btc.position_value  -price $price 
            $unrealizedPNL            = ConvertTo-SignedString -number $unrealizedPNL

            $out = New-Object -TypeName psobject -Property @{
                ___upl___             = $unrealizedPNL  
                __margin__            = $positionMargin 
                ___value___           = $positionValue 
                side                    = $btc.side 
                __size__                = $btc.size
                ___sl___              = [string]$btc.stop_loss
                ___tp___              = [string]$btc.take_profit 
                __liq__             = [string]$btc.liq_price
                __lev__             = [string]$btc.orderLev
                ___bal___             = ConvertTo-USDollar -btc $btc.wallet_balance -price $price 
            }
            $out | Format-Table side, __size__, @{
                n="__margin__";e={"{0:N2}" -f $_.__margin__};a="right"}, @{
                n="___value___";e={"{0:N2}" -f $_.___value___};a="right"}, @{
                    n="___upl___";e={"{0:N2}" -f $_.___upl___};a="right"}, @{
                        n="___bal___";e={"{0:N2}" -f $_.___bal___};a="right"}, @{
            n="__liq__";e={"{0:N0}" -f $_.__liq__};a="right"}, @{
                n="___sl___";e={"{0:N0}" -f $_.___sl___};a="right"}, @{
                    n="___tp___";e={"{0:N0}" -f $_.___tp___};a="right"} -Auto | Write-HostColored -PatternColorMap @{'Sell'='Red';'\('='Red';'Buy'='Green';'\+[\d\.]+'='Green';'\-[\d\.]+'='Red';'[-]{1,}'='DarkCyan'}
        }
    }
    function Close-BtcPosition {
        param(
            [string]$price,
            [switch]$raw
        )
        
        $endpoint                       = "https://api.bybit.com/v2/private/order/create"
        $global:bybit_api               = Get-Cred bybit_api
        $global:bybit_sec               = Get-Cred bybit_sec
        $timestamp                      = Get-Date | ConvertTo-UnixTimestamp

        [hashtable]$global:initArgs = @{
            'api_key'           =   $bybit_api
            'timestamp'         =   $timestamp
            'side'              =   "Buy" 
            'symbol'            =   "BTCUSD"
            'time_in_force'     =   "GoodTillCancel"
            'reduceOnly'        =   "true"
            'qty'               =   $do.orderVol
        }  
        if ($price) {
            $initArgs.Add('order_type', "Limit")
            $initArgs.Add('price', $price)
        }
        else {
            $initArgs.Add('order_type', "Market")
        }
        $global:sortedArgs               = [System.Collections.SortedList]$initArgs
        $global:finalArgs                = $sortedArgs.Keys | Foreach-Object {
            "$_=$($sortedArgs.item($_))"
        }
        $global:toSign                   = $finalArgs -join '&'
        $global:signature                = Get-Signature -message $toSign -secret $bybit_sec 
        $initArgs.Add('sign',$signature)

        $global:jsonArgs                = $initArgs | ConvertTo-Json
        $global:res = Invoke-RestMethod -Uri $endpoint -Method POST -Body $jsonArgs -ContentType 'application/json'
        if ($raw){
            $res.result | Select-Object symbol, side, order_type, qty, price | Format-Table -Auto 
        }
        else {
            $res.result
        }
    }
    function Get-BtcOrder {
        param(
            [switch]$raw,
            [switch]$o
        )

        $global:response    =  Get-ByBitApi -endpoint 'v2/private/order/list' -arguments 'symbol=BTCUSD' -raw

        if ($raw){
            $response
        }
        elseif ($o){
            $response.result.data
        }
        else {
            $out = New-Object System.Collections.ArrayList 
            Foreach ($r in $response.result.data){
                $obj = New-Object -TypeName psobject -Property @{
                    __day__             = $r.created_at.toShortDateString()
                    __time__            = $r.created_at.toShortTimeString()
                    __side__            = $r.side 
                    __type__            = $r.order_type
                    __size__            = $r.qty
                    __sl__              = [Math]::Round($r.stop_loss, 0)
                    __tp__              = [Math]::Round($r.take_profit, 0)
                    _status_            = $r.order_status
                }
                [void]$out.Add($obj)
            }
            return $out | Format-Table __day__,__time__,__side__,__type__,__size__, @{
                n="__tp__";e={$_.__tp__};a="right"}, @{n="__sl__";e={$_.__sl__};a="right"}, _status_  -AutoSize | Write-HostColored -PatternColorMap @{
                    'Sell'      = 'DarkRed'
                    '\('        = 'Red'
                    'Buy'       = 'DarkGreen'
                    '-'         = 'DarkCyan'
                    'Market'    = 'DarkMagenta'
                    'Limit'     = 'Blue'
                } -SimpleMatch

            return $out | Select-Object Format-Table -Auto  | Write-HostColored -PatternColorMap @{'Buy'='DarkGreen';'Sell'='DarkRed';'-'='DarkCyan'} 
        }
    }
    function Remove-BybitOrder {
        
        $endpoint                       = "https://api.bybit.com/v2/private/order/cancelAll"
        $global:bybit_api               = Get-Cred bybit_api
        $global:bybit_sec               = Get-Cred bybit_sec
        $timestamp                      = Get-Date | ConvertTo-UnixTimestamp

        [hashtable]$global:initArgs = @{
            'api_key'           =   $bybit_api
            'timestamp'         =   $timestamp
            'symbol'            =   "BTCUSD"
        }  
    
        $global:sortedArgs               = [System.Collections.SortedList]$initArgs
        $global:finalArgs                = $sortedArgs.Keys | Foreach-Object {
            "$_=$($sortedArgs.item($_))"
        }
        $global:toSign                   = $finalArgs -join '&'
        $global:signature                = Get-Signature -message $toSign -secret $bybit_sec 
        $initArgs.Add('sign',$signature)

        $global:jsonArgs                = $initArgs | ConvertTo-Json
        $global:res = Invoke-RestMethod -Uri $endpoint -Method POST -Body $jsonArgs -ContentType 'application/json'
    }
    function Invoke-Jafar {
        # $barrier = ("$([char]8282)" * 64)
        $barrier = ("." * 64)
        $attl = ("                   " + "$([char]8228)" * 2 + "$([char]8282)" * 2)
        $atml = ("               " + "$([char]8228)" * 2 + "$([char]8282)" * 6)
        $atbl = ("           " + "$([char]8228)" * 2 + "$([char]8282)" * 10)

        $attr = ("             " + "$([char]8282)" * 2 + "$([char]8228)" * 2)
        $atmr = ("  " + "$([char]8282)" * 6 + "$([char]8228)" * 2)
        $atbr = ("$([char]8282)" * 10 + "$([char]8228)" * 2)

        $global:lastBlock = ''
        Get-Deal | Export-CliXml -Path ~\jafar_banner.txt  
        Get-BtcOrder -Menu | Select-Object -First 15 | Export-CliXml -Path ~\jafar_temp.txt  


        $Opts = @(
            $(New-MenuItem -DisplayName "buy (4/1)" -Script { 
                Write-Host "`n"
                $answer = Read-Host '50 ? [Y/n]'
                switch($answer){
                    'n' { 
                        $amount = Read-Host 'Enter quantity'
                        New-FourBag -buy -orderVol $amount
                        $lastBlock = Get-BtcOrder 
                    }
                    default {
                        New-FourBag -buy -orderVol 50
                        $lastBlock = Get-BtcOrder 
                    }
                }
                $lastBlock | Export-CliXml -Path ~\jafar_temp.txt  
                Get-Deal | Export-CliXml -Path ~\jafar_banner.txt  
            }),
            $(New-MenuItem -DisplayName "sell (4/1)" -Script { 
                Write-Host "`n"
                $answer = Read-Host '50 ? [Y/n]'
                switch($answer){
                    'n' { 
                        $amount = Read-Host 'Enter quantity'
                        New-FourBag -sell -orderVol $amount
                        $lastBlock = Get-BtcOrder 
                    }
                    default {
                        New-FourBag -sell -orderVol 50
                        $lastBlock = Get-BtcOrder 
                    }
                }
                $lastBlock | Export-CliXml -Path ~\jafar_temp.txt  
                Get-Deal | Export-CliXml -Path ~\jafar_banner.txt  
            }),
            $(New-MenuItem -DisplayName "refresh" -Script { 
                Write-Host "`n"
                Get-Deal | Export-CliXml -Path ~\jafar_banner.txt  
            }),
            $(New-MenuItem -DisplayName "set leverage" -Script { 
                Write-Host "`n"
                $amount = Read-Host 'Leverage'
                $lev = Set-BybitLeverage -leverage $amount 
                $lastBlock = New-Object -TypeName psobject -Property @{Executing="executing";Leverage="Set leverage to $lev"}
                $lastBlock | Export-CliXml -Path ~\jafar_temp.txt  
                Get-Deal | Export-CliXml -Path ~\jafar_banner.txt  

            }),
            $(New-MenuItem -DisplayName "orders" -Script { 
                Write-Host "`n"
                Get-BtcOrder -Menu | Export-CliXml -Path ~\jafar_temp.txt  
                Get-Deal | Export-CliXml -Path ~\jafar_banner.txt  

            }),
            $(New-MenuItem -DisplayName "exit" -Script { 
                Write-Host "`n"
                Write-Host "Exiting..."
                Write-Host "`n`n`n"
                Clear-Host
                Break
            })
        )

        Do {
            Clear-Host
            Write-Host "`n`n`n`n`n`n`n"
            $lastBlock = Import-CliXml -Path ~\jafar_temp.txt
            $bannerBlock = Import-CliXml -Path ~\jafar_banner.txt
            $Chosen = $false
            $timeout = New-TimeSpan -Seconds 1
            $stopwatch = [System.Diagnostics.Stopwatch]::StartNew()
            Do {
                $Chosen = Show-Menu $Opts -Callback {
                    $lastTop = [Console]::CursorTop
                    [System.Console]::SetCursorPosition(0, 0)
                    Write-Host -Fore DarkCyan "$attl" -NoNewLine
                    Write-Host -Fore DarkCyan "$attr" 
                    Write-Host -Fore DarkCyan "$atml" -NoNewLine
                    Write-Host "   [" -NoNewLine;
                    Write-Host -Fore DarkCyan "J" -NoNewLine
                    Write-Host "]" -NoNewLine
                    Write-Host "afar " -NoNewLine;
                    Write-Host -Fore DarkCyan "$atmr          " -NoNewLine 
                    Write-Host -Fore Green "$(Get-Date -Format MM/dd)"
                    Write-Host -Fore DarkCyan "$atbl" -NoNewLine
                    Write-Host -Fore DarkCyan "   Ver 1.0   " -NoNewLine
                    Write-Host -Fore DarkCyan "$atbr   $(Get-Date -Format HH:mm:ss)"
                    Write-Host "`n"
                    Write-Host -Fore DarkCyan $barrier
                    # $(Get-BtcPosition -Menu | Format-Table -Auto) | Write-HostColored -Pattern "-" -ForegroundColor DarkCyan
                    $($bannerBlock | Select-Object __side__, __size__, _value_, _margin_, __upl__, __tp__, __sl__, __lev__  )| Format-Table __side__, __size__, _value_, _margin_, __upl__, __tp__, __sl__, @{n="__lev__";e={"{0:N0}" -f $_.__lev__};a="right"} | Write-HostColored -PatternColorMap @{"-"='DarkCyan';'Buy'='Green';'Sell'='Red';}
                    Write-Host "`n`n`n`n`n`n`n`n"
                    Write-Host -Fore DarkCyan $barrier

                    Write-Host "`n"
                    $($lastBlock) | Format-Table __day__, __side__, __size__, __type__, _status_, __tp__, __sl__ | Write-HostColored -PatternColorMap @{'Buy'='DarkGreen';'Sell'='DarkRed';'-'='DarkCyan';'Filled'='DarkGray';'Deactivated'='DarkGray';'Market'='DarkCyan';'New'='DarkYellow'} 
                    [System.Console]::SetCursorPosition(0, $lastTop)
                } 
            } Until (($stopwatch.elapsed -lt $timeout) -or ($Chosen -ne $false))
            & $Chosen.Script 
        } While ($true)
    }    

    $global:JafarFunctions =  $JafarCmdlets.Name 

    $JafarCmdlets | Foreach-Object { Set-Alias -Name $_.Alias -Value $_.Name -Scope Global -Force }

    Export-ModuleMember -Function $JafarFunctions 
} 









