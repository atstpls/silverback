New-Module -Name IdentityTools -Scriptblock {

    [array]$global:IdentityToolsCmdlets = @(
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Me"
                Alias       = "me"
                Description = "Get own account and system details"
                Category    = "info"
                Mod         = "IdTools"
                Status      = "good"
            }),  
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Us"
                Alias       = "us"
                Description = "Get team account and system details"
                Category    = "info"
                Mod         = "IdTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Identity"
                Alias       = "gid"
                Description = "Get an identity"
                Category    = "info"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-IdentityTools"
                Alias       = "it"
                Description = "Get usage on cmdlets in this module "
                Category    = "util"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-NewAccessToken"
                Alias       = "gnat"
                Description = "Get new tokens for MSGraph, AADGraph, and Azure Mgmt"
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-IdentityToolsCmdlets"
                Alias       = "gitc"
                Description = "Get IdentityTool cmdlets"
                Category    = "util"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-User"
                Alias       = "guser"
                Description = "Returns user accounts"
                Category    = "info"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-UserCertificates"
                Alias       = "guc"
                Description = "Returns user certificates"
                Category    = "info"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Invoke-BeAdmin"
                Alias       = "ba"
                Description = "Start session with -admin account"
                Category    = "auth"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "ConvertFrom-Saml"
                Alias       = "cfs"
                Description = "Convert saml to object"
                Category    = "saml"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Convert-SSidTToNName"
                Alias       = "cstn"
                Description = "Convert SSID to name"
                Category    = "util"
                Mod         = "IdTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Edit-SAMLResponse"
                Alias       = "esr"
                Description = "Make a change to a SAML response"
                Category    = "saml"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Format-Jwt"
                Alias       = "fj"
                Description = "Parse a JSON Web Token"
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AllDsregcmd"
                Alias       = "gadrc"
                Description = "Get info from dsregcmd output"
                Category    = "util"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AllKlist"
                Alias       = "gakl"
                Description = "Get klist output"
                Category    = "util"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AllCreds"
                Alias       = "gac"
                Description = "Show all creds for current user"
                Category    = "info"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AuthCode"
                Alias       = "gacd"
                Description = "Use nonce and PRT Cookie to get an authorization code"
                Category    = "util"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-CredObj"
                Alias       = "gco"
                Description = "Create a PS credential object with password"
                Category    = "auth"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-CyberarkCred"
                Alias       = "gcc"
                Description = "Retrieve admin password from CyberArk"
                Category    = "auth"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AccessTokensFromHarFile"
                Alias       = "gath"
                Description = "Extract tokens from a HAR file"
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "bad"
            }),

        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AccessTokenObjects"
                Alias       = "gato"
                Description = "Get all tokens as objects "
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Nonce"
                Alias       = "gn"
                Description = "Retrieve a nonce for a client app and resource"
                Category    = "auth"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-PrtCookie"
                Alias       = "gpc"
                Description = "Use a nonce to get a PRT Cookie"
                Category    = "auth"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-SAMLInfo"
                Alias       = "gsi"
                Description = "Submit a SAML response to an endpoint"
                Category    = "saml"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-SAMLRequest"
                Alias       = "gsr"
                Description = "Obtain a SAML request from an endpoint"
                Category    = "saml"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-SAMLResponse"
                Alias       = "gsr"
                Description = "Retrieve a SAML response for an endpoint"
                Category    = "saml"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-SmartCardCred"
                Alias       = "gscc"
                Description = "Create a PS credential object with PIV"
                Category    = "auth"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AccessToken"
                Alias       = "gat"
                Description = "Show all JWTs obtained from Azure AD"
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-JwtWithAuthCode"
                Alias       = "gjac"
                Description = "Use authorization code to get an access token"
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Invoke-AuthCodeFlow"
                Alias       = "iaf"
                Description = "Get token using authorization code flow"
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Invoke-ClientCredFlow"
                Alias       = "icf"
                Description = "Get token using client credentials flow"
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Invoke-DeviceCodeFlow"
                Alias       = "idf"
                Description = "Get token using device code flow"
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Invoke-ImplicitFlow"
                Alias       = "iif"
                Description = "Get token using implicit flow"
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Read-LastJwt"
                Alias       = "rlj"
                Description = "Read the last JWT that was read (`$lastJwt_azure)"
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "good"
            }),
	    $(New-Object -TypeName psobject -Property @{
                Name        = "Get-JwtWithCredObject"
                Alias       = "gjco"
                Description = "Get a JWT using PS credential object"
                Category    = "auth"
                Mod         = "IdTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-JwtWithPRT"
                Alias       = "gjprt"
                Description = "Get a JWT using PRT"
                Category    = "auth"
                Mod         = "IdTools"
                Status      = "bad"
            }),  
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-JwtWithPassword"
                Alias       = "gjp"
                Description = "Get a JWT using a password"
                Category    = "auth"
                Mod         = "IdTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-JwtWithRefreshToken"
                Alias       = "gjr"
                Description = "Get a JWT using a refresh token"
                Category    = "auth"
                Mod         = "IdTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-JwtWithSAML"
                Alias       = "gjs"
                Description = "Get a JWT using SAML assertion"
                Category    = "auth"
                Mod         = "IdTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Read-SAML"
                Alias       = "rs"
                Description = "Parse SAML request or SAML response"
                Category    = "saml"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Read-Jwt"
                Alias       = "rj"
                Description = "Read interesting fields in a JSON Web Token"
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Submit-SAMLResponse"
                Alias       = "ssr"
                Description = "Submit a SAML response to an endpoint"
                Category    = "saml"
                Mod         = "IdTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Test-IsJWTValid"
                Alias       = "tjv"
                Description = "Returns `$true if JWT is valid"
                Category    = "jwt"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Cred"
                Alias       = "gcred"
                Description = "Get stored credential"
                Category    = "util"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Add-Cred"
                Alias       = "acred"
                Description = "Add stored credential"
                Category    = "util"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Copy-Cred"
                Alias       = "cc"
                Description = "Copy stored credential to clipboard"
                Category    = "util"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "List-Creds"
                Alias       = "lc"
                Description = "List stored credentials"
                Category    = "util"
                Mod         = "IdTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-UserAccounts"
                Alias       = "gua"
                Description = "Get accounts for user"
                Category    = "info"
                Mod         = "IdTools"
                Status      = "good"
            })

    )

    [array]$global:apis = @(
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "graph.microsoft.com"
                ApiName   = "MS Graph"
                VarName   = "msgraph"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "graph.windows.net"
                ApiName   = "AAD Graph"
                VarName   = "aadgraph"
                Endpoints = "me", "users", "groups", "contacts", "directoryRoles", "applications", "servicePrincipals", "domains", "policies"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "app.vssps.visualstudio.com"
                ApiName   = "Azure DevOps"
                VarName   = "devops"
                Endpoints = "https://docs.microsoft.com/rest/api/azure/devops?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
                
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "management.core.windows.net"
                ApiName   = "Azure Core Management"
                VarName   = "azcoremgmt"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "management.azure.com"
                ApiName   = "Azure Service Management"
                VarName   = "azsvcmgmt"
                Endpoints = "https://msdn.microsoft.com/library/azure/ee460799.aspx"
            }),
        $(New-Object -TypeName psobject -Property @{
                #      ApiHost="enrollment.manage.microsoft.com"
                ApiHost   = "api.manage.microsoft.com"
                ApiName   = "Intune"
                VarName   = "intune"
                Endpoints = "https://docs.microsoft.com/intune/intune-graph-apis?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps#intune-permission-scopes"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = ($cur_tenant_id + "-my.sharepoint.com")
                ApiName   = "OneDrive"
                VarName   = "onedrive"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                #  ApiHost=($cur_tenant_id + ".sharepoint.com")
                ApiHost   = "microsoft.sharepoint-df.com"
                ApiName   = "Sharepoint"
                VarName   = "sharepoint"
                Endpoints = "https://docs.microsoft.com/sharepoint/dev/sp-add-ins/get-to-know-the-sharepoint-rest-service?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "mysignins.microsoft.com"
                ApiName   = "MySignins"
                VarName   = "mysignins"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "officeapps.live.com"
                ApiName   = "Office Apps"
                VarName   = "officeapps"
                Endpoints = "https://ocws.officeapps.live.com/ocs/v2/"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "outlook.office365.com"
                ApiName   = "MS Exchange"
                VarName   = "exchange"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "api.spaces.skype.com"
                ApiName   = "MS Teams"
                VarName   = "teams"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "api.diagnostics.office.com"
                ApiName   = "SARA"
                VarName   = "sara"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "vault.azure.net"
                ApiName   = "Azure Key Vault"
                VarName   = "keyvault"
                Endpoints = "https://docs.microsoft.com/rest/api/keyvault/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "api.azrbac.mspim.azure.com"
                ApiName   = "Azure PIM"
                VarName   = "azure_pim"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "print.print.microsoft.com"
                ApiName   = "Universal Print"
                VarName   = "print"
                Endpoints = "https://docs.microsoft.com/en-us/universal-print/hardware/universal-print-oem-app-registration?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "api.yammer.com"
                ApiName   = "Yammer"
                VarName   = "yammer"
                Endpoints = "https://developer.yammer.com/docs/getting-started"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "officespeech.platform.bing.com"
                ApiName   = "Speech"
                VarName   = "speech"
                Endpoints = "https://docs.microsoft.com/azure/cognitive-services/speech/home?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "api.skypeforbusiness.com"
                ApiName   = "Skype"
                VarName   = "skype"
                Endpoints = "https://docs.microsoft.com/skype-sdk/ucwa/authenticationusingazuread?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "batch.core.windows.net"
                ApiName   = "Azure Batch"
                VarName   = "azure_batch"
                Endpoints = "https://docs.microsoft.com/azure/batch/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "api.azuredatacatalog.com"
                ApiName   = "Azure Catalog"
                VarName   = "azure_catalog"
                Endpoints = "https://docs.microsoft.com/azure/data-catalog/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "azure.microsoft.com/services/data-explorer"
                ApiName   = "Azure Data Explorer"
                VarName   = "azure_data_explorer"
                Endpoints = "https://docs.microsoft.com/azure/kusto/api/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "datalake.azure.net"
                ApiName   = "Azure Data Lake"
                VarName   = "azure_data_lake"
                Endpoints = "https://docs.microsoft.com/azure/data-lake-store/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "atlas.microsoft.com"
                ApiName   = "Azure Maps"
                VarName   = "azure_maps"
                Endpoints = "https://docs.microsoft.com/azure/azure-maps/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "purview.azure.net"
                ApiName   = "Azure Purview"
                VarName   = "azure_purview"
                Endpoints = "https://docs.microsoft.com/azure/purview?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "aadrm.com"
                ApiName   = "Azure Rights Management Services"
                VarName   = "azure_rms"
                Endpoints = "https://docs.microsoft.com/azure/information-protection/develop/developers-guide?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "storage.azure.com"
                ApiName   = "Azure Storage"
                VarName   = "azure_storage"
                Endpoints = "https://docs.microsoft.com/azure/storage/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "azurecustomerinsights.com"
                ApiName   = "Customer Insights"
                VarName   = "insights"
                Endpoints = "https://docs.microsoft.com/dynamics365/customer-engagement/customer-insights/ref/apiquickref?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }) ,       
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "dynamics.microsoft.com"
                ApiName   = "Dynamics 365 Business Central"
                VarName   = "dynamics_365"
                Endpoints = "https://docs.microsoft.com/dynamics-nav/fin-graph?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "admin.services.crm.dynamics.com"
                ApiName   = "Dynamics CRM"
                VarName   = "dynamics_crm"
                Endpoints = "https://go.microsoft.com/fwlink/?linkid=2143766"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "erp.dynamics.com"
                ApiName   = "Dynamics ERP"
                VarName   = "dynamics_erp"
                Endpoints = "https://docs.microsoft.com/dynamics365/unified-operations/dev-itpro/data-entities/services-home-page?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "service.flow.microsoft.com"
                ApiName   = "Flow Service"
                VarName   = "flow_service"
                Endpoints = "https://docs.microsoft.com/flow/dev-enterprise-intro?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "manage.office.com"
                ApiName   = "Office 365 Management APIs"
                VarName   = "o365_mgmt"
                Endpoints = "https://msdn.microsoft.com/office-365/office-365-management-activity-api-reference"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "onenote.com"
                ApiName   = "OneNote"
                VarName   = "onenote"
                Endpoints = "https://msdn.microsoft.com/office/office365/howto/onenote-landing"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "analysis.windows.net"
                ApiName   = "Power BI Service"
                VarName   = "power_bi"
                Endpoints = "https://docs.microsoft.com/power-bi/service-get-started?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "rts.powerapps.com"
                ApiName   = "PowerApps Runtime Service"
                VarName   = "powerapps"
                Endpoints = "https://docs.microsoft.com/powerapps/developer/common-data-service/overview?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            })
    )

    Foreach ($api in $apis) {
        New-TimeDatedVar -nam $api.varName -val $api.ApiHost 
    }


    function Get-Cred ($account) {  
        $file = "." + $account + ".txt"
        $g = Get-Content $env:USERPROFILE\.creds\$file | ConvertTo-SecureString
        [System.Runtime.InteropServices.marshal]::PtrToStringAuto([System.Runtime.InteropServices.marshal]::SecureStringToBSTR($g)) 
    }
  
    function Copy-Cred ($account) {  
        $file = "." + $account + ".txt"
        $g = Get-Content $env:USERPROFILE\.creds\$file | ConvertTo-SecureString
        [System.Runtime.InteropServices.marshal]::PtrToStringAuto([System.Runtime.InteropServices.marshal]::SecureStringToBSTR($g)) | Set-Clipboard
        Write-Host -Fore Green "[+] " -NoNewLine; Write-Host "Password copied to clipboard"
    }
  
    function Add-Cred ($account) {
        $file = $env:USERPROFILE + "\.creds\." + $account + ".txt"
        Read-Host "Enter cred" | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString | Out-File $file
    }
  
    function List-Creds {
        (Get-ChildItem $env:USERPROFILE\.creds\*.txt).BaseName.trim('.')
    }


    ############################

    ########## INFO ############

    ############################

    function Get-Me {
        if ($me) {
            $me | Write-HostColored -Pattern 'sam' -ForegroundColor DarkCyan -WholeLine
        }
        else {
            $onprem = Get-OnPremUser $env:USERNAME
            $mail_admin = $onprem.mail -replace '@', '-admin@'
            $onprem_admin = Get-OnPremUser -Filter "(mail=$mail_admin)"
            $onprem_azure = Get-OnPremUser ($onprem.samaccountname + "-azure")

            [psobject]$global:me = [ordered]@{
                dnsdomain = $env:USERDNSDOMAIN
                domain    = $env:USERDOMAIN
                workplace = ${env:UE-V_Path} + $env:USERNAME
                computer  = $env:COMPUTERNAME
                localIp   = Get-NetIpAddress | Where-Object InterfaceAlias -eq "Ethernet 2" | Where-Object AddressFamily -eq 'IPv4'
                vpnIp     = Get-NetIpAddress | Where-Object PrefixOrigin -eq "dhcp" | Where-Object AddressFamily -eq 'IPv4'
                publicIp  = (Invoke-RestMethod http://ip-api.io/json -UserAgent $(Get-UA)).ip
                sam       = $onprem.samaccountname
                upn       = $onprem.userprincipalname
                sid       = $onprem.objectSid 
                admin_sam = $onprem_admin.samaccountname
                admin_upn = $onprem_admin.userprincipalname
                admin_sid = $onprem_admin.objectSid
                azure_sam = $onprem_azure.samaccountname
                azure_upn = $onprem.userprincipalname
                azure_sid = $onprem_azure.objectSid
            }
            $me | Write-HostColored -Pattern 'sam' -ForegroundColor DarkCyan -WholeLine
        }
    }
    function Get-Us {
        if ($us) {
                $us | Select-Object sam, sid, name, onPremUpn, azureUpn
            }
        else {

            $global:us_accounts = Get-NNetUUser -Filter "(title=$us_groups)"


            [psobject]$global:us = [System.Collections.ArrayList]::new()

            $us_accounts | ForEach-Object {

                $azureupn = ( Get-AzureUser $_.Mail ).userPrincipalName

                $obj = New-Object -TypeName psobject -Property @{
                    sam       = $_.samaccountname
                    sid       = $_.objectSid
                    name      = $_.name
                    onPremUpn = $_.userPrincipalName
                    azureUpn  = $azureupn
                }
                [void]$us.Add($obj)
            }
            $us | Select-Object sam, sid, name, onPremUpn, azureUpn
        }
    }
    function Get-User {
        [cmdletbinding()]param(
            [string]$prop,

            [switch]$hound,

            [switch]$o,

            [switch]$azure,

            [switch]$onprem
        )

        if ($hound) {
            Get-NewAccessToken "graph.microsoft.com"
            $global:AADUsers = Get-MSGraphApi -y -api "users?`$select=userPrincipalName, displayName, onPremisesSecurityIdentifier, id"
            $AADUsers | ForEach-Object {
                if ($_.userPrincipalName -NotMatch "#EXT#") {
                    $_ | Add-Member -NotePropertyName tenantId -NotePropertyValue $tenant_id -Force 
                }
                else {
                    $_ | Add-Member -NotePropertyName tenantId -NotePropertyValue $null -Force 
                }
            }
            Write-Success $AADUsers.Count, 'objects saved as $AADUsers'
        }
        else {
            $obj = Get-NNetUUser $prop
    
            if ($obj) {
                if (!$o) {
                    $groups = ($obj.memberof | Select-String 'CN=([^,]+)').Matches.Captures | Foreach-Object { $_.Groups[1].Value }
                    $mgr = ($obj.manager -split '=')[1].trim(',OU') -replace '\\', ''
                    $main = $obj | Select-Object userprincipalname, displayName, description, objectsid, pwdlastset, whencreated, whenchanged, lastlogontimestamp
                    $main | Add-Member -NotePropertyName manager -NotePropertyValue $mgr -Force 
                    $main | Add-Member -NotePropertyName memberOf -NotePropertyValue $($groups -join "`n") -Force 
                    $main
                }
                else {
                    $obj
                }
            }
            else {
                $obj = Get-AzureUser $prop
                if (!$o) {
                    $groups = Get-MSGraphApi -api "users/$($obj.userPrincipalName)/memberof"
                    $main = $obj.PSObject.Properties | Where-Object { $_.Value } | ForEach-Object {
                        New-Object -Typename psobject -Property @{
                            $($_.Name) = $($_.Value)
                        }
                    }
                    $main | Add-Member -NotePropertyName memberOf -NotePropertyValue $($groups.displayName -join "`n") -Force 
                    $main
                }
                else {
                    $obj
                }
            }
        }
    }
    function Get-UserAccounts {
        param(
            [Parameter(Mandatory = $True)]
            [ValidateNotNullOrEmpty()]
            [string]$firstName,
    
            [Parameter(Mandatory = $True)]
            [ValidateNotNullOrEmpty()][string]
            $lastName
        )

        Clear-Vars pairs
        $global:pairs = [System.Collections.ArrayList]::New()

        $global:azure_accounts = Get-MSGraphApi -api "users?filter=startswith(surname, '$lastName') and givenname eq '$firstName'" 
        $global:onprem_accounts = Get-NNetUUser -Filter "(sn=$lastName*)(givenName=$firstName)" 

        $onprem_accounts | ForEach-Object {

            # $pair = [System.Collections.ArrayList]::New()
            $opObj = New-Object -TypeName psobject -Property @{
                type        = "onPrem"
                upn         = $_.userPrincipalName 
                sam         = $_.samAccountName
                objId       = ("objectGuid=" + $_.objectguid)
                displayName = $_.displayName
                created     = $_.whencreated
                lastLogon   = $_.lastLogonTimestamp
            }
            [void]$pairs.Add($opObj)
    
            # Clear-Vars synced
            # $synced = $azure_accounts | Where-Object onPremisesSamAccountName -eq $_.samAccountName 
            # if ($synced) {
        }
  
        $azure_accounts | ForEach-Object {
            # $pair = [System.Collections.ArrayList]::New()
            $azObj = New-Object -TypeName psobject -Property @{
                type        = "AAD"
                upn         = $_.userPrincipalName 
                sam         = $_.onPremisesSamAccountName 
                objId       = ("id=" + $_.id) 
                displayName = $_.displayName
                created     = $_.createdDateTime
                lastLogon   = $_.lastLogonTimestamp
            }
            [void]$pairs.Add($azObj)
        }

        $pairs  | Select-Object type, sam, upn, objId, created, lastLogon | Sort-Object sam | Format-Table -auto | Write-HostColored -PatternColorMap @{
            '-azure'     = "Cyan"
            '-admin'     = "Green"
            '-paw'       = "DarkYellow" 
            '-svradmin@' = "Magenta"
            '-T'         = "Gray"
            '-da@'       = "Red" 
        } -WholeLine 
    }

    function Get-Identity {
    
        param(
            [string]$Identity,
  
            [switch]$ext,
  
            [switch]$followRedirects,
  
            [switch]$useDefaultCreds
        )
  
        Clear-Vars address, addressx, headers, headersx, headersr, headersxr, headersc, status, statusx, statusr, statusxr, ename, resp, mesg
  
        $ename = Get-HostFromUri $Identity
      
        $address = Test-IsHostResolvable $ename
  
        if (!$address) {
            $address = "-"
        }
  
        $addressx = Test-IsHostResolvableExt $ename
        if (!$addressx) {
            $addressx = "-"
        }
  
        if (Test-IsWebServiceAlive $Identity) {
      
            if ($followRedirects) {
                $headers = Get-Headers $Identity -o -followRedirects 
            }
            else {
                $headers = Get-Headers $Identity -o  
            }
            if (!$headers) {
                $headers = "-"
            }
  
            $headersc = Get-Headers $Identity -o -defaultCreds -followRedirects 
            if (!$headersc) {
                $headersc = "-"
            }
      
            $headersr = Get-Headers $Identity -o -followRedirects 
            if (!$headersr) {
                $headersr = "-"
            }
  
            try {
                $resp = Invoke-WebRequest $Identity -SkipCertificateCheck -Timeout 1 
                $title = Get-Title $resp
            }
            catch {
      
                $mesg = $_.ErrorDetails.Message 
                if ($mesg -match 'You don') {
                    $title = [System.Web.HttpUtility]::HtmlDecode( ( $mesg | Select-String 'You don[^\.]+').Matches.Value )
                }      
                elseif ($mesg -match 'Unable to id') {
                    $title = [System.Web.HttpUtility]::HtmlDecode( ( $mesg | Select-String 'Unable to id[^\.]+').Matches.Value )
                }
                elseif ($headers.Server) {
                    $title = $headers.Server 
                }
                else {
                    $title = "-"
                }
            }
        }
  
        $status = $headers.statusCode 
        $statusx = $headersx.statusCode  
        $statusr = $headersr.statusCode 
        $statusxr = $headersxr.statusCode 
  
        if ($ext) {
            $addressx = $address 
            $headersx = $headers
            $headersxr = $headersr  
            $statusx = $status
            $statusxr = $statusr  
        }
  
        $statusObject = [PSCustomObject]@{
            Identity  = $Identity   
            hostname  = $ename       
            address   = $address
            addressx  = $addressx
            headers   = $headers
            headersc  = $headersc 
            headersx  = $headersx
            headersr  = $headersr
            headersxr = $headersxr
            status    = $status  
            statusx   = $statusx   
            statusr   = $statusr  
            statusxr  = $statusxr 
            title     = $title
            response  = $resp 
        }
  
        if (!$all) {
            $statusObject | Select-Object Identity, address, status, title
        }
        else {
            $statusObject
        }
    }
    function Get-IdentityTools {
        param(
            [string]$type = 'default'
        )

        Switch ($type) {

            'j' { Get-IdentityToolsCmdlets 'jwt' }
            's' { Get-IdentityToolsCmdlets 'saml' }
            'a' { Get-IdentityToolsCmdlets 'auth' }
            'i' { Get-IdentityToolsCmdlets 'info' }
            'u' { Get-IdentityToolsCmdlets 'util' }
            default {
      
                Write-Host "`n"
                Write-Host -Fore DarkYellow " ...:::" -NoNewLine; Write-Host "`tUsage:   " -NoNewLine;
                Write-Host -Fore DarkYellow "it " -NoNewLine; Write-Host -Fore DarkGray "[category]"
                Write-Host "`n"

                [pscustomobject]$options = [ordered]@{
                    'j          jwt '  = "JWT functions"
                    's          saml'  = "SAML functions"
                    'a          auth'  = "Authentication functions"
                    'i          info'  = "Gather information about identities"
                    'u          util'  = "Utilities/helper functions"
                }

                $options.Keys | Foreach-Object {
                    $firstLetter = $_[0]
                    Write-Host -Fore DarkYellow "`tit " -NoNewLine;Write-Host "$firstLetter" -Fore White -NoNewLine
                    Write-Host -Fore DarkGray "$($_.trim($firstLetter))" -NoNewLine ; Write-Host "`t$($options.Item("$_"))"
                }
                Write-Host "`n"
            }
        }
    }
    function Get-IdentityToolsCmdlets {
        param(
            [string]$type
        )

        if (!$type) {
            $cmdlets = $IdentityToolsCmdlets | Sort-Object Name
        }
        else {
            $cmdlets = $IdentityToolsCmdlets | Where-Object Category -eq $type | Sort-Object Name
        }
        Write-Host "`n"
        Write-Host -Fore DarkGray ("`tCmdlet`t`t`t`t") -NoNewLine; Write-Host -Fore DarkGray " Alias`t`t" -Nonewline;
        Write-Host -Fore DarkGray " Description" 
        Write-Host "`t$('-' * 24)`t $('-' * 5)`t`t $('-' * 30)"
        $cmdlets | ForEach-Object {
            if ($_.Status -eq 'good') {
                Write-Host -Fore Green 'o' -NoNewLine
            }
            else {
                Write-Host -Fore Red 'x' -NoNewLine
            }
            if ($_.Name.Length -gt 23) {
                Write-Host -Fore Gray ("`t" + $_.Name + "`t ") -NoNewLine; Write-Host -Fore DarkYellow $_.Alias -Nonewline
                Write-Host -Fore Gray (" `t`t " + $_.Description ) 
            }    
            elseif ($_.Name.Length -gt 15) {
                Write-Host -Fore Gray ("`t" + $_.Name + "`t`t ") -NoNewLine; Write-Host -Fore DarkYellow $_.Alias -Nonewline
                Write-Host -Fore Gray (" `t`t " + $_.Description ) 
            }
            elseif ($_.Name.Length -gt 7) {
                Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t ") -NoNewLine; Write-Host -Fore DarkYellow $_.Alias -Nonewline
                Write-Host -Fore Gray (" `t`t " + $_.Description ) 
            }
            else {
                Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t`t ") -NoNewLine; Write-Host -Fore DarkYellow $_.Alias -Nonewline
                Write-Host -Fore Gray (" `t`t " + $_.Description ) 
            }
        }
        Write-Host "`n"
    }

    function Invoke-BeAdmin {
        [cmdletbinding()]param(
            $computer = "$env:computername",
            $account  = "$sam_admin"
        )

        New-TimeDatedVar -nam credVarName -val "${account}_cred"
        New-TimeDatedVar -nam credObjVarName -val "${account}_credObject"

        if (Test-IsVarExpired credObjVarName ) {
            Write-Fail $credObjVarName,'expired. Renewing...'
            Get-CyberarkCred -account $account 2>$null
        }

        $value = (Get-Variable $credVarName 2>$null).value
        if ($value.length -eq 0){
            Write-Fail $credObjVarName,'not found. Acquiring...'
            if (Test-IsConnected){
                Get-CyberarkCred -account $account 2>$null
            }
            else {
                Write-Fail 'Must connect to VPN to request credentials'
                Break
            }
        }
        $global:c = Get-CredObj -username $account -pwdVariable $credVarName
        try {
                Enter-PsSession -ComputerName $computer -Credential $c
        }
        catch {
                Write-Fail 'Maybe try JIT?'
        }

    }



    function Get-CyberarkCred {

        param(
            [Parameter(Mandatory = $True, ValueFromPipelineByPropertyName = $True)]
            [string]$account     = $sam_admin
        )

        if (!$credServer) {
            Read-Host 'You have not set the $credserver variable'
            Return $null
        }
        $ValidCerts = [System.Security.Cryptography.X509Certificates.X509Certificate2[]](Get-ChildItem Cert:\CurrentUser\My)
        $cert = $ValidCerts | Where-Object NotAfter -gt (Get-Date).ToShortDateString() | Where-Object EnhancedKeyUsageList -match "Smart"
        if ($cert.count -ne 1) {
            $cert = $cert | Sort-Object -desc NotAfter | Select-Object -first 1
        }
        $pattern = [Regex]::new('\d{10}')
        $piv = $pattern.matches($cert.Subject).Value

        $safe = ($env:UserName + "-" + $piv) 
        $auth = "https://$credServer/PasswordVault/api/auth/pkipn/Logon"
        $info = "https://$credServer/PasswordVault/api/Accounts?AppID=`&Safe=$safe`&Folder=Root`&Object=$credAccountObject"

        $tok = Invoke-RestMethod -Uri $auth -Method POST -Certificate $cert
        $aids = (Invoke-RestMethod -Uri $info -Headers @{'Authorization' = $tok }).Value
        $aid = ($aids | Where-Object Username -eq $account).Id 
        $pass = "https://$credServer/PasswordVault/api/Accounts/$aid/Password/Retrieve"
    
        $out = Invoke-RestMethod -Uri $pass -Headers @{'Authorization' = $tok } -Method POST  
        
        New-TimeDatedVar -nam "${account}_cred" -val $out
        $out
        
        $credObj = Get-CredObj -username $account -pwdVariable "${account}_cred"
        New-TimeDatedVar -nam "${account}_credObject" -val $credObj

    }

    function Get-UserCertificates {

        param(
            [switch]$new,
            [switch]$valid,
            [switch]$subject
        )
    
        $certDirs = (Get-ChildItem Cert:\LocalMachine\).Name | Foreach-Object { ("Cert:\LocalMachine\$_") }
        $certDirs += (Get-ChildItem Cert:\CurrentUser\).Name | Foreach-Object { ("Cert:\CurrentUser\$_") }

        $global:certs = $certDirs | Foreach-Object { Get-ChildItem "$_" }
        $certs | Foreach-Object {
            $_ | Add-Member -Name nbr -Force -Value $_.notBefore.ToShortDateString() -MemberType NoteProperty 
            $_ | Add-Member -Name naf -Force -Value $_.notAfter.ToShortDateString() -MemberType NoteProperty
        }

        $display = $certs

        if ($new) {
            $today = Get-Date 
            $yesterday = $today.AddDays("-100")
            $display = $display | Where-Object notbefore -gt $yesterday 
        }
        if ($valid) {
            $display = $display | Where-Object notafter -gt $(Get-Date) 
        } 
        if ($subject) {
            $display | Select-Object nbr, naf, subject, issuer | Sort-Object nbr
        }
        else {
            $display | Select-Object nbr, naf, issuer | Sort-Object nbr
        }
    }

    ############################

    ##########  SAML  ##########

    ############################

    function ConvertFrom-Saml {
        param(
            [string]$urlencoded_saml
        )
        $b64 = ConvertTo-UrlDecode $urlencoded_saml
        $mmemSStream = <# [IO.MemoryStream] #>   [Convert]::FromBase64String($b64)
        $defStream = New-Object IO.Compression.DeflateStream($mmemSStream, [IO.Compression.CompressionMode]::Decompress)
        $sr = New-Object IO.StreamReader($defStream, [Text.Encoding]::ASCII)
        [xml]$out = $sr.ReadToEnd() 
        if ($out.AuthnRequest) {
            return [xml]$out
        }
        else {
            return [xml]$out
        }
    }
    function Read-SAML {

        param(
            [Parameter(Mandatory = $True, ValueFromPipeline = $True)]
            [string]$samlToken,
            [switch]$o,
            [switch]$all
        )    

        if ($samlToken.getType().BaseType.Name -ne 'XmlNode') {
            $b64dec = $samlToken | ConvertFrom-Base64
            [xml]$xml = $b64dec 
        }
        else {
            $xml = $samlToken
        }

        if ($xml.Response) {
            $out = New-Object -TypeName psobject -Property @{
                destination  = $xml.Response.Destination 
                issuer       = $xml.Response.Issuer.'#text'
                issueInstant = $xml.Response.IssueInstant
                status       = ($xml.Response.Status.StatusCode.Value -split ":")[-1]
                xml          = $xml 
            }
            if ($xml.Response.Assertion) {
                # $spname = $xml.Response.Assertion.Subject.NameID.SPNameQualifier
                # $out |  Add-Member -MemberType NoteProperty -Name $spname -Value $spname -Force

                $address = $xml.Response.Assertion.AuthnStatement.SubjectLocality.Address
                $out |  Add-Member -MemberType NoteProperty -Name  address -Value $address -Force

                $attr = $xml.Response.Assertion.AttributeStatement.Attribute
                [pscustomobject]$obj = @{}
                $counter = ($attr.Count - 1)
                while ($counter -ne 0) {
                    $obj |  Add-Member -MemberType NoteProperty -Name  $attr[$counter].Name -Value $attr[$counter].AttributeValue.'#text' -Force
                    $counter --
                }
                $members = ($obj | Get-Member | Where-Object MemberType -eq 'NoteProperty').Name
                foreach ($m in $members) {
                    $val = $obj | Select-Object -exp $m
                    $out |  Add-Member -MemberType NoteProperty -Name $m -Value $val
                }
            }
        }
        elseif ($xml.AuthnRequest) {
            $out = New-Object -TypeName psobject -Property @{
                assertionConsumerServiceURL = $xml.AuthnRequest.AssertionConsumerServiceURL
                destination                 = $xml.AuthnRequest.Destination 
                issuer                      = $xml.AuthnRequest.Issuer
                issueInstant                = $xml.AuthnRequest.IssueInstant
                saml                        = $xml.AuthnRequest.saml
                xml                         = $xml 
            }
        }
        else {
            Write-Fail "Don't recognize saml format"
        }
        return $out
    }
    function Get-SAMLInfo {

        param(
            [string]$endpoint
        )

        $tries = @(
            '/saml/metadata'
            '/authenticate/saml/metadata'

        )

        foreach ($try in $tries) {
            $met = ""
            [xml]$met = (Invoke-WebRequest  -Timeoutsec 3 -SkipCertificateCheck -AllowUnencryptedAuthentication -UseDefaultCredentials -Uri ( $endpoint + $try )).Content 2>$null
            if ($met) {
                $entityID = $met.EntityDescriptor.entityID
                $binding = $met.EntityDescriptor.SPSSODescriptor.AssertionConsumerService.Binding
                $location = $met.EntityDescriptor.SPSSODescriptor.AssertionConsumerService.Location 

                New-Object -TypeName psobject -Property @{
                    entityID = $entityID 
                    binding  = $binding 
                    location = $location 
                }
            }
        }
    }
    function Get-SAMLRequest {
        param(
            [string]$url = "https://www.example.com",

            [switch]$o
        )
        $r = Invoke-WebRequest $url 
        $st = Get-Parameters $r -saml -o 
        if ($o) {
            return $st.Token 
        }
        else {
            $out = rs $st.Token -o 
            $type = $st.Type 
            if ($type -eq 'request') {
                $global:samlReq = $st.Token 
                $out 
                Write-Success 'object saved as $samlReq'
            }
            else {
                $global:samlRes = $st.Token 
                $out 
                Write-Success 'object saved as $samlRes'
            }
        }
    }
    function Get-SAMLResponse {
        [cmdletbinding()]param(
            [string]$targetsite = "https://www.example.com"
        )
        if ( $PSBoundParameters.containskey("debug") ) {
            if ( $debug = [bool]$PSBoundParameters.item("debug") ) { 
                $DebugPreference = "Continue"
            }
        }
    
        [xml]$xmlReq = @'
            <samlp:AuthnRequest xmlns="urn:oasis:names:tc:SAML:2.0:metadata" ID="" Version="2.0" IssueInstant="" IsPassive="false" AssertionConsumerServiceURL=""
            xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" ForceAuthn="false">
            <Issuer xmlns="urn:oasis:names:tc:SAML:2.0:assertion">Issuer</Issuer>
            </samlp:AuthnRequest>
'@

        # $xmlReq.AuthnRequest.IssueInstant = (Get-Date).ToUniversalTime().ToString('o')
        # $global:resp = Invoke-WebRequest -Uri $url -SessionVariable g -UseDefaultCredentials 
        # $global:resp = Invoke-WebRequest -UseDefaultCredentials @params -WebSession $g 
        # $global:parms += Get-Parameters $resp -parm 
        # $global:forms = Get-Parameters $resp -form 
        # $base = ($resp.BaseResponse.RequestMessage.RequestUri.ToString() | Select-String 'https://[^\/]+').Matches.Value
        # $method = $forms.Method 
        # $action = [System.Web.HttpUtility]::HtmlDecode($forms.Action)
        # if ($forms.Action -notmatch '^https'){
        #   $action = $base + $forms.Action   
        # }
        Clear-Vars p, f 
        Get-NextRequest -first -req $targetsite 
        if ($f) {
            while (!$p.SAMLResponse) {
                Get-NextRequest 
                if (!$f) {
                    break
                }
            }
        }

        if ($p.SAMLResponse ) {
            Read-Saml $p.SAMLResponse 
            $global:samlResponse = $p.SAMLResponse
            Write-Success 'SAMLResponse save as $samlResponse'
            Get-NextRequest
        }
    }
    function Edit-SAMLResponse ($sr) {

        $text = $sr.Token | ConvertFrom-Base64
        $newtext = $text -replace '0252808669', '1234567890'
        $newB64 = $newtext | ConvertTo-Base64
        $sr.Token = $newB64
        return $sr 
    }
    function Submit-SAMLResponse ($sr) {
        $url = $sr.Destination
        $global:res = Invoke-WebRequest $url -Method POST -Body @{SAMLResponse = $saml.Token } -UseDefaultCredentials -WebSession $g
        if ($res) {
            $title = $res.content | Select-String '<title>.*</title>' | Foreach-Object { $_.Matches } | Foreach-Object { $_.Value }
            if ($title) {
                Write-Host -Fore DarkYellow `n`t"$title"`n
                $global:saml = Get-Parameters $res -saml 
                if ($saml) {
                    Write-Debug ('SAML' + $saml.Type + '      ' + $saml.Destination)
                }
            }
            else {
                Write-Host $res.Headers 
            }
        }
    }


    ###########################

    #########  JWT  ###########

    ###########################

    function Format-Jwt {

        param(
           [Parameter(Mandatory = $True, ValueFromPipeline = $True)]
           [string]$JWT
        )
        $part = ($JWT -split '\.')[1]
        if ($part) {
            while ($part.Length % 4) { $part += "=" }
        }
        $decoded = ConvertFrom-Base64 $part
        $obj = $decoded | ConvertFrom-Json
        return $obj
    }
    function Get-AccessToken {
        Get-Variable | Where-Object Name -match '_token' | Where-Object Name -notmatch 'THREAD_SET' | Select-Object Name, CreatedAt, Value | Sort-Object -Desc CreatedAt
    }
    function Get-AccessTokensFromHarFile {
        param(
            [string]$filepath
        )
        $global:name = ($filepath -split '\\')[-1] -replace '\.', '_' -replace '_har', ''
        $har = Get-Content $filepath
        $global:har_tokens = [regex]::Matches($har, 'eyJ[^\.]+\.eyJ[^\.]+\.[^\"^\;]+').Value
  
        $global:objs = @()
        $har_tokens | Foreach-Object {
            $out = rj $_ -o 
            $out    | Add-Member -NotePropertyName "token" -NotePropertyValue $($_.Trim('\\'))
            $objs += $out  
        }

        $varName = $name + "_objs"
        New-Variable -Name $varName -Value $objs -Force -Scope Global 
        Write-Success 'Created', $varName 

        $global:accessTokens = $objs | Where-Object { $_.aud }
        $varName = $name + "_tokens"
        New-Variable -Name $varName -Value $accessTokens -Force -Scope Global 
        Write-Success 'Created', $varName 

        $global:oddScopes = $objs | Where-Object { $_.aud } | Where-Object scp -notmatch 'user_impersonation'
        $varName = $name + "_oddScopes"
        New-Variable -Name $varName -Value $oddScopes -Force -Scope Global 
        Write-Success 'Created', $varName 
 
    }
    function Get-AccessTokenObjects {
        param(
            [switch]$o
        )
        $output = @()
        $vars = Get-Variable | Where-Object Name -match '_token' | Where-Object Name -notmatch 'THREAD_SET' | Where-Object { $_.Value }
        foreach ($tok in $vars) {
            $obj = Format-Jwt $tok.Value 
            $issTime = (New-Object -Type DateTime -ArgumentList 1970, 1, 1, 0, 0, 0, 0).AddSeconds($obj.iat)
            $obj.iat = [System.TimeZoneInfo]::ConvertTimeFromUtc($issTime, [System.TimeZoneInfo]::FindSystemTimeZoneById('Central Standard Time')).ToString('yyyy-MM-dd HH:mm:ss')
            $expTime = (New-Object -Type DateTime -ArgumentList 1970, 1, 1, 0, 0, 0, 0).AddSeconds($obj.exp)
            $obj.exp = [System.TimeZoneInfo]::ConvertTimeFromUtc($expTime, [System.TimeZoneInfo]::FindSystemTimeZoneById('Central Standard Time')).ToString('yyyy-MM-dd HH:mm:ss')

            $output += $obj
 
            $expired = $false
            if ($expTime -lt $(Get-Date)) {
                $expired = $true
            }

            Add-Member -InputObject $obj -MemberType NoteProperty -Name "var" -Value $tok.Name

            if ($expired) {
                $obj.exp = $obj.exp -replace '$', ' - '
            }
            $obj.iat = $obj.iat -replace '$', '  '
            $obj.aud = $obj.aud -replace 'https://', '' -replace '/', ''
            $tokName = ($tok.Name -split '_')[0]

            $output += New-Object -TypeName psobject -Property @{
                Principal  = $obj.upn
                Resource   = $obj.aud 
                IssueTime  = $obj.iat 
                ExpireTime = $obj.exp
                Type       = $tokName
                o          = $obj
            }
        }
        if ($o){
            return $output
        }
        else {
            $output | Select-Object Principal,Resource,Type,IssueTime,ExpireTime | Sort-Object -desc IssueTime | Format-Table -AutoSize
        }
    }
    function Get-AllCreds {

        param(
            [switch]$svc,
            [switch]$o
        )

        $jwts = Get-AccessTokenObjects -o
        if ($o) {
            $global:tokenObjs = $jwts
            Write-Success $tokenObjs.count, 'objects saved as $tokenObjs'
            Break
        }
        else {
            $klist = Get-AllKlist
            $dsreg = Get-AllDsregcmd
        }
        $out = @()
        if ($jwts) { $out += $jwts }
        if ($klist) { $out += $klist }
        if ($dsreg) { $out += $dsreg } 

        # $array | Format-Table -auto | Write-HostColored -Pattern  -ForegroundColor Red -WholeLine
        # $arr | Select-Object var,upn,aud,amr,iat,exp,scp | Format-Table -auto
        if (!$svc) {
            $out = $out | Where-Object Type -ne 'svc'
        }
        $out | Select-Object Type, Principal, Resource, IssueTime, ExpireTime | Sort-Object -Desc IssueTime | Format-Table -auto | Write-HostColored -PatternColorMap @{krbtgt = "DarkYellow"; adfs = "Cyan"; ' - ' = "DarkYellow" } -WholeLine 
    }  
    function Get-NewAccessToken {
        [cmdletbinding()]param(
            [Parameter(Position = 0)]
            [array]$destinations    = $apis.VarName,

            [string]$tenant_id      = $tenant_id,

            [string]$type           = "azure",

            [switch]$q
        )

        if ( $PSBoundParameters.containskey("debug") ) {
            if ( $debug = [bool]$PSBoundParameters.item("debug") ) { 
                $DebugPreference = "Continue"
            }
        }
        if ($type -eq "azure") {
            foreach ($dest in $destinations) {
                try {
                    $desRes = ($apis | Where-Object { ($_.VarName -eq $dest) -or ($_.ApiHost -eq $dest)}).ApiHost
                    if ($q){
                        Invoke-AuthCodeFlow -resource $desRes -tenant_id $tenant_id -raw -q
                    }
                    else {
                        Invoke-AuthCodeFlow -resource $desRes -tenant_id $tenant_id -raw
                    } 
                }
                catch {
                    Write-Fail 'Failed to obtain', ("`$" + $dest)
                }
            }
        }
    }
    function Invoke-DeviceCodeFlow {
        param (
            [String]$client_id  = "1950a258-227b-4e31-a9cf-717495945fc2",
    
            [String]$tenant_id  = 'common',
    
            [String]$resource   = "graph.microsoft.com"
        )

        if ($resource[-1] -ne '/') { $resource += '/' }
        if ($resource -notmatch '^https') { $resource = 'https://' + $resource }
        $scope = $resource + ".default" 
  
        $body = @{
            "client_id" =     $client_id
            "resource" =      $resource
        }
        $UserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36"
        $Headers=@{}
        $Headers["User-Agent"] = $UserAgent
        $uri = "https://login.microsoftonline.com/common/oauth2/devicecode?api-version=1.0"
        $authResponse = Invoke-RestMethod -UseBasicParsing -Method Post -Uri $uri -Headers $Headers -Body $body
        $authResponse.user_code | Set-Clipboard

        Start-Process chrome.exe '--new-window https://microsoft.com/devicelogin' -Wait

        $answer = Read-Host "Press Enter to continue..."

        $body=@{
            "client_id" =  "1950a258-227b-4e31-a9cf-717495945fc2"
            "grant_type" = "urn:ietf:params:oauth:grant-type:device_code"
            "code" =       $authResponse.device_code
        }

        $uri2 = "https://login.microsoftonline.com/Common/oauth2/token?api-version=1.0"
        $global:Tokens = Invoke-RestMethod -UseBasicParsing -Method Post -Uri $uri2 -Headers $Headers -Body $body
        Write-Success 'Tokens acquired. Saved as `$Tokens'


        if ($resource -match 'graph.microsoft.com'){
            $global:msgraph_token  = $Tokens.access_token
            Write-Success 'Access token acquired. Saved as `$msgraph_token'
        }
        elseif ($resource -match 'graph.windows.net'){
            $global:aad_graph_token  = $Tokens.access_token
            Write-Success 'Access token acquired. Saved as `$aad_graph_token'
        }
        else {
            Write-Success 'Tokens acquired. Saved as `$Tokens'
            Write-Success 'Access token acquired. Saved as `$at'
        }
    }
    function Invoke-ClientCredFlow {
        param ( 
    
            [string]$client_id      = "fa90f83a-4720-4e43-b232-a497f23218f3",
    
            [string]$clientSecret   = $(Get-Cred rocket),

            [string]$scope          = "https://graph.microsoft.com/.default",
    
            [string]$tenant_id      = $(Get-Cred tenant)
        )


        $grantType = "client_credentials"
        $url = "https://login.microsoftonline.com/$tenant_id/oauth2/v2.0/token"
        $contentType = "application/x-www-form-urlencoded"
  

        $body = @{
            client_id     = $client_id
            scope         = $scope
            client_secret = $clientSecret
            grant_type    = $grantType
        }
  
  
  
  
        Invoke-RestMethod -Uri $url -Method POST -Body $body 
    }
    function Invoke-AuthCodeFlow {

        [cmdletbinding()]param (
            [string]$client_id          = "1950a258-227b-4e31-a9cf-717495945fc2",

            [string]$resource           = "graph.microsoft.com",

            [string]$tenant_id          = $tenant_id,

            [string]$redirect_uri       = "http://localhost",

            [string]$scope              = ".default",

            [switch]$raw,

            [switch]$challenge,

            [switch]$q
        )
        if ( $PSBoundParameters.containskey("debug") ) {
            if ( $debug = [bool]$PSBoundParameters.item("debug") ) { 
                $DebugPreference = "Continue"
            }
        }

        Get-Nonce -resource $resource
        Write-Debug $resource

        Get-PrtCookie -nonce $nonce_azure
        Write-Debug $resource

        Get-AuthCode -resource $resource
        Write-Debug $resource

        Get-JwtWithAuthCode -resource $resource -tenant_id $tenant_id
        Write-Debug $resource

        $api            = $apis | Where-Object ApiHost -eq $resource 
        $tokenName      = $api.VarName + "_token"
        $apiName        = $api.ApiName
        $lastAud        = ($lastJwt_azure | fj).aud

        if (!$tokenName) {
            $tokenName = "newToken"
        }
        if (!$apiName) {
            $apiName = "newApi"
        }  
        New-TimeDatedVar -nam $tokenName -val $lastJwt_azure 
        if ($q) {
        }
        else {
            Write-Success 'Obtained', $apiName, 'token as', ("`$" + $tokenName), ' aud:',$lastAud 
        }
    }
    function Invoke-ImplicitFlow {
        param ( 
            [string]$client_id = $m_client_id,
            [string]$tenant_id = "common",
            [string]$redirectUri = $m_redirect_uri,        
            [string]$scope = "https://graph.microsoft.com/.default",
            [string]$loginHint = $upn,
            [switch]$print
        )
 
        $url = "https://login.microsoftonline.com/$tenant_id/oauth2/v2.0/authorize"
        # $url                      = "https://login.live.com/oauth20_authorize.srf"
        $Parameters = [ordered]@{
            client_id     = $m_client_id
            response_type = "token"
            redirect_uri  = $m_redirect_uri
            response_mode = "fragment"
            scope         = $scope
            state         = "12345"
            nonce         = "678910"
            prompt        = "none"
            login_hint    = $loginHint
        }
        $UriObj = New-UriObject $url $Parameters
        if ($print) {
            Write-Host $UriObj 
        }
        else {
            $global:r = Invoke-WebRequest -Uri $UriObj -Method GET -UseDefaultCredentials -SessionVariable g
        }

        $global:s = Invoke-WebRequest -Uri ($base + $r.Forms[0].Action) -Body $r.Forms[0].Fields -Method $r.Forms[0].Method -UseDefaultCredentials -WebSession $g

    }
    function Read-LastJwt {
        Read-Jwt $lastJwt_azure
    }
    function Read-Jwt {
        [cmdletbinding()]param (
           [Parameter(Mandatory = $True, ValueFromPipeline = $True)]
           $JWT,
            [switch]$o,
            [switch]$e,
            [switch]$notoken
        )
        if ( $PSBoundParameters.containskey("debug") ) {
            if ( $debug = [bool]$PSBoundParameters.item("debug") ) { 
                $DebugPreference = "Continue"
            }
        }

        if ($JWT.length -eq 0) { 
            Write-Fail "Why you give me no", "`$token" 
            Break
        }
        elseif ($JWT.getType().Name -eq 'String') {
            $global:obj = Format-Jwt $JWT
        }
        elseif ($JWT.count -lt 1) {
            $list = 'token', 'access_token', 'id_token'
            $props = ($JWT | gm | Where-Object MemberType -eq NoteProperty).Name
            if ($props) {
                foreach ($p in $list) {
                    if ($p -in $props) { 
                        $string = ($JWT | Select-Object -exp $p).ToString() 
                        $obj = Format-Jwt $string
                        Write-Debug 'Token was an object'
                        break
                    }
                }
            }
        }
        else {
            Write-Fail "Token is incorrect format"
            Break
        }

        if (!(Test-IsJWTValid $msgraph_token)) {
            try {
                Get-NewAccessToken "graph.microsoft.com" -q
            }
            catch {
                $notoken = 1
            }
        }

        if ($e) {
            return $(New-Object -TypeName psobject -Property @{
                    aud   = $obj.aud
                    exp   = $obj.exp
                    appid = $obj.appid
                } )
        }

        if ($obj.iat) {
            $issTime = (New-Object -Type DateTime -ArgumentList 1970, 1, 1, 0, 0, 0, 0).AddSeconds($obj.iat) 
            $obj.iat = [System.TimeZoneInfo]::ConvertTimeFromUtc($issTime, [System.TimeZoneInfo]::FindSystemTimeZoneById('Central Standard Time'))
        }
        if ($obj.exp) {
            $expTime = (New-Object -Type DateTime -ArgumentList 1970, 1, 1, 0, 0, 0, 0).AddSeconds($obj.exp)
            $obj.exp = [System.TimeZoneInfo]::ConvertTimeFromUtc($expTime, [System.TimeZoneInfo]::FindSystemTimeZoneById('Central Standard Time'))
        }
        if ($obj.nbf) {
            $notBefore = (New-Object -Type DateTime -ArgumentList 1970, 1, 1, 0, 0, 0, 0).AddSeconds($obj.nbf)
            $obj.nbf = [System.TimeZoneInfo]::ConvertTimeFromUtc($notBefore, [System.TimeZoneInfo]::FindSystemTimeZoneById('Central Standard Time'))
        }  
        if (($obj.exp) -and ($obj.iat)) {
            $timeValid = [datetime]$obj.exp - [datetime]$obj.iat
            $obj | Add-Member -MemberType NoteProperty -Name validFor -Value $timeValid
        }
        if ($obj.auth_time) {
            $authTime = (New-Object -Type DateTime -ArgumentList 1970, 1, 1, 0, 0, 0, 0).AddSeconds($obj.auth_time)
            $obj.auth_time = [System.TimeZoneInfo]::ConvertTimeFromUtc($authTime, [System.TimeZoneInfo]::FindSystemTimeZoneById('Central Standard Time'))
        }
        # if ($obj.upn){
        #   try {
        #     $obj = $obj | Select-Object -Property * -ExcludeProperty family_name,given_name,unique_name,name 
        #   }
        #   catch {}
        # }

  
        if (!$notoken) {
            if ($obj.aud) {
                if (Test-IsGuid $obj.aud) {
                    try {
                        $audname = ($app_ids | Where-Object AppId -eq $obj.aud).AppName
                        # (Get-App $obj.aud).displayName
                        $obj.aud = "$($obj.aud) ( $audname )"
                    } 
                    catch {}
                }
            }    
            if ($obj.appid) {
                if (Test-IsGuid $obj.appid) {
                    try {
                        $appname = ($app_ids | Where-Object AppId -eq $obj.appid).AppName
                        # (Get-App $obj.appid).displayName
                        $obj.appid = "$($obj.appid) ( $appname )"
                    }
                    catch {}
                }
            }
            if ($obj.deviceid) {
                try {
                    $devname = (Get-Dev $obj.deviceid).displayName 
                    $obj.deviceid = "$($obj.deviceid) ( $devname )"
                }
                catch {}
            }
            if ($obj.scp -match '^\d') {
                try {
                    $rolename = (Get-Role $obj.scp).displayName
                    if (!$rolename) {
                        $query = 'directoryRoles?$filter=roleTemplateId eq ' + "'" + $obj.scp + "'"
                        $rolename = (Get-MSGraphApi -tok $msgraph_token -api $query).displayName
                    }
                    $obj.scp = "$($obj.scp) ( $rolename )" 
                }
                catch {}
            }    
        }

        if ($o) {
            return $obj
        }
        else {
            $obj | Write-HostColored -Pattern 'aud\s+:.*', 'upn\s+:.*', 'scp\s+:[^\n]+', 'amr\s+:.*', 'appid\s+:.*', 'deviceid\s+:.*' -ForegroundColor DarkGreen
        }
    }
    function Test-IsJWTValid ($JWT) {
        if ($JWT.length -eq 0) {
            return 0 
        }
        $expTime = (Format-Jwt $JWT).exp
        $nowTime = (ConvertTo-UnixTime $(Get-Date))

        if ($nowTime -le $expTime) {
            return 1
        }
        else {
            return 0
        }
    }
    function Get-JwtWithCredObject {

        param (
            [string]$principal = $cur_admin_upn  
        )

        if (!$dashAdminPass -or (Test-IsVarExpired dashAdminPass)) {
            Write-Attempt 'Renewing $dashAdminPass ...'
            $pass = Get-CyberarkCred
            New-TimeDatedVar -nam "dashAdminPass" -val $pass
        }
        $credObj = Get-CredObj $principal 
        $global:tok = Get-AADIntAccessTokenForAADGraph -Credentials $credObj
        return $tok
    }
    function Get-JwtWithPRT {

        [cmdletbinding()]param (
            [string]$client_id          = "1950a258-227b-4e31-a9cf-717495945fc2",
            [string]$resource           = "graph.microsoft.com",
            [string]$tenant_id          = $tenant_id,
            [string]$scope,
            [string]$redirect_uri       = "https://login.microsoftonline.com/common/oauth2/nativeclient"    
        )

        if ( $PSBoundParameters.containskey("debug") ) {
            if ( $debug = [bool]$PSBoundParameters.item("debug") ) { 
                $DebugPreference = "Continue"
            }
        }

        $resource_name = ($apisList | Where-Object ApiHost -eq $resource ).ApiName
        $client_name = ($ids | Where-Object AppId -eq $client_id ).AppName 
        Write-Debug ( "Resource name:" + $resource_name)
        if ($client_name.length -lt 1) { $client_name = $client_id }
        Write-Debug ( "Client name:`t`t" + $client_name )

        try {
            Get-Nonce -client_id $client_id -resource $resource -redirect_uri $redirect_uri
            if ($nonce_azure) { Write-Debug  ("Nonce received:`t`t" + $client_id + " > " + $resource + " > " + $redirect_uri ) }
        }
        catch {
            Write-Host -Fore Gray "[-] No nonce obtained using $client_name"
            Write-Debug  ("Nonce not received:`t" + $client_id + " > " + $resource + " > " + $redirect_uri )
            return
        }

        try {
            Get-PrtCookie -nonce $nonce_azure
            if ($prtCook_azure) { Write-Debug  ("PRT Cookie received:`t`t " + $client_id + " > " + $resource + " > " + $redirect_uri ) }
        }
        catch {
            Write-Host -Fore Gray "[-] No PRT cookie obtained using $client_name"
            Write-Debug  ("PRT Cookie not received:`t" + $client_id + " > " + $resource + " > " + $redirect_uri )
            return
        }

        try {
            Get-AuthCode -client_id $client_id -resource $resource -redirect_uri $redirect_uri -prtCook $prtCook_azure -nonce $nonce_azure 
            if ($authCode_azure) { Write-Debug  ("Code received:`t`t" + $client_id + " > " + $resource + " > " + $redirect_uri ) }
        }
        catch {
            Write-Host -Fore Gray "[-] No code obtained using $client_name"
            Write-Debug  ("Code not received:`t" + $client_id + " > " + $resource + " > " + $redirect_uri )
            return
        }

        try {
            Get-TokenWithAuthCode -client_id $client_id -tenant_id $tenant_id -redirect_uri $redirect_uri -scope $scope -authCode $authCode_azure -resource $resource
            if ($lastTkn_azure) {
                $global:token = $respWithToken
                Write-Success  "Saved token as `$token using ", $client_id, "and", $resource
                Write-Debug  ("Token received:`t`t" + $client_id + " > " + $resource + " > " + $redirect_uri )
      
                $global:obj = Read-Token $newToken -o 
                Add-Member -InputObject $obj -MemberType NoteProperty -Name token -Value $newToken
                return $obj
            }
        }
        catch {
            Write-Host -Fore Gray "[-] No token obtained using $client_name"
            Write-Debug  ("Token not received:`t`t" + $client_id + " > " + $resource + " > " + $redirect_uri )
            return
        }
    }
    function Get-JwtWithRefreshToken {
        param ( 
            [Parameter(Mandatory = $true)]
            [string]$refresh_tok,
    
            [string]$client_id      = "1950a258-227b-4e31-a9cf-717495945fc2",
    
            [string]$tenant_id      = $tenant_id,

            [string]$resource       = "graph.microsoft.com"
        )

        if ($client_id -eq "ab9b8c07-8f02-4f72-87fa-80105867a763" ) {
            $url = "https://login.windows.net/common/oauth2/token"
        }
        else {
            $url = "https://login.microsoftonline.com/$tenant_id/oauth2/token"
        }

        $resource = ConvertTo-Uri $resource
        $Body = [ordered]@{
            client_id     = $client_id
            resource      = $resource
            scope         = "openid"
            refresh_token = $refresh_tok
            grant_type    = "refresh_token"
        }  
        $global:r = Invoke-RestMethod -Uri $url -Method POST -Body $Body -ContentType "application/x-www-form-urlencoded" 

        if ($o){
            return $r
        }
        else {
            Write-Success "Tokens saved as", "`$r"
        } 
    }
    function Get-JwtWithPassword {
        param ( 
            [Parameter(Mandatory = $true)]
            [string]$uname,

            [Parameter(Mandatory = $true)]
            [string]$passwd,

            [string]$client_id      = "1950a258-227b-4e31-a9cf-717495945fc2",
    
            [string]$tenant_id      = $cur_tenant_id,

            [string]$resource       = "graph.microsoft.com"
        )

        $basicAuth = "${uname}:${passwd}"

        Get-AuthCode -client_id $client_id -resource $resource -basicAuth $basicAuth 
    }
    function Get-JwtWithSAML {

        param (
            [string]$sam,
    
            [String]$client_id          = "1950a258-227b-4e31-a9cf-717495945fc2", 
    
            [String]$resource           = "graph.microsoft.com",

            [String]$scope              = "openid offline_access",

            [switch]$o,

            [switch]$t
        )

        Clear-Vars upn, tokenName, up

        $up = Test-IsConnected
        if (!$up) {
            Write-Fail 'Not connected to domain'
            Break
        }

        $tokenName = ($apisList | Where-Object apiHost -eq $resource).TokenName

        if ($sam -match '-admin') {
            if (!$dashAdminPass -or (Test-IsVarExpired dashAdminPass)) {
                Write-Fail "Renewing $dashAdminPass ..."
                Get-CyberarkCred $sam_admin 
            }
            $upn = $upn_admin
            $pas = $dashAdminPass
            $tokenName = $tokenName + "_admin"
        }
        elseif ($sam -match 'azure') {
            if (!$dashAzurePass -or (Test-IsVarExpired dashAzurePass)) {
                Write-Fail "Renewing $dashAzurePass ..."
                Get-CyberarkCred $sam_azure 
            }
            $upn = $upn_azure 
            $pas = $dashAzurePass
            $tokenName = $tokenName + "_azure"
        }
        else {
            Write-Fail 'Not in operation yet'
            Break
        }

        $global:url = "https://login.microsoftonline.com/common/userrealm/" + $upn + "?api-version=1.0"
        $global:res = Invoke-RestMethod -Uri $url 
        $global:usernamemixed = $res.federation_active_auth_url 
        $global:client_req_id = $(New-Guid)
        $global:url2 = $res.federation_active_auth_url
        $global:headers = @{
            'SOAPAction'        = 'http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue'
            'Host'              = $sts_host
            'client-request-id' = $client_req_id
        }
        $date = Get-Date 
        $start = $date.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ")
        $end = $date.ToUniversalTime().AddMinutes(10).ToString("yyyy-MM-ddTHH:mm:ssZ")


        [xml]$global:template = @"
  <s:Envelope xmlns:s='http://www.w3.org/2003/05/soap-envelope'
  xmlns:a='http://www.w3.org/2005/08/addressing'
  xmlns:u='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'>
  <s:Header>
      <a:Action s:mustUnderstand='1'>http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue</a:Action>
      <a:MessageID>urn:uuid:99fa61a5-a407-4305-883f-94ef3f0218c2</a:MessageID>
      <a:ReplyTo>
          <a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>
      </a:ReplyTo>
      <a:To s:mustUnderstand='1'>$usernamemixed</a:To>
      <o:Security s:mustUnderstand='1'
          xmlns:o='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>
          <u:Timestamp u:Id='_0'>
              <u:Created></u:Created>
              <u:Expires></u:Expires>
          </u:Timestamp>
          <o:UsernameToken u:Id='uuid-2a38708c-193d-4b88-b3da-f422095bc10b'>
              <o:Username></o:Username>
              <o:Password></o:Password>
          </o:UsernameToken>
      </o:Security>
  </s:Header>
  <s:Body>
      <trust:RequestSecurityToken xmlns:trust='http://schemas.xmlsoap.org/ws/2005/02/trust'>
          <wsp:AppliesTo xmlns:wsp='http://schemas.xmlsoap.org/ws/2004/09/policy'>
              <a:EndpointReference>
                  <a:Address>urn:federation:MicrosoftOnline</a:Address>
              </a:EndpointReference>
          </wsp:AppliesTo>
          <trust:KeyType>http://schemas.xmlsoap.org/ws/2005/05/identity/NoProofKey</trust:KeyType>
          <trust:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Issue</trust:RequestType>
      </trust:RequestSecurityToken>
  </s:Body>
</s:Envelope>
"@

        [xml]$global:xmlReq = @'
<samlp:AuthnRequest xmlns="urn:oasis:names:tc:SAML:2.0:metadata" ID="" Version="2.0" IssueInstant="" IsPassive="" AssertionConsumerServiceURL=""
    xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" ForceAuthn="false">
    <Issuer xmlns="urn:oasis:names:tc:SAML:2.0:assertion">Issuer</Issuer>
</samlp:AuthnRequest>
'@

        if ($t) {
            $template.Envelope.Header.Security.Timestamp.Created = $start  
            $template.Envelope.Header.Security.Timestamp.Expires = $end  
            $template.Envelope.Header.Security.UsernameToken.Username = $upn.toString() 
            $template.Envelope.Header.Security.UsernameToken.Password = $pas.toString() 
            $template.Envelope.Header.MessageID = ("urn:uuid:" + $(New-Guid)) 
            $global:envelope = Invoke-RestMethod -Method POST -headers $headers -Uri $url2 -Body $template -ContentType "application/soap+xml"
        }
        else {
            $xmlReq.AuthnRequest.IssueInstant = (Get-Date).ToUniversalTime().ToString('o')
            $xmlReq.AuthnRequest.Issuer.'#text' = "$issuer"
            $xmlReq.AuthnRequest.AssertionConsumerServiceURL = "$usernamemixed"
            $xmlReq.AuthnRequest.ID = "12345"
            $xmlReq.AuthnRequest.ForceAuthn = $False.ToString().ToLowerInvariant()
            $xmlReq.AuthnRequest.IsPassive = $True.ToString().ToLowerInvariant()
            $global:envelope = Invoke-RestMethod -Method POST -headers $headers -Uri $url2 -Body $xmlReq -ContentType "application/soap+xml"
        }
        $global:assertion = $envelope.Envelope.Body.RequestSecurityTokenResponse.RequestedSecurityToken


        $global:enc_assertion = [System.Convert]::ToBase64String($assertion.InnerXml.ToCharArray());
  
        $resource = ConvertTo-Uri $resource
        $body = [ordered]@{
            scope      = $scope
            assertion  = $enc_assertion
            # grant_type  = "urn:ietf:params:oauth:grant-type:saml2-bearer"
            grant_type = "urn:ietf:params:oauth:grant-type:saml1_1-bearer"
            resource   = $resource 
            client_id  = $client_id 
        }
        $url_oauth = "https://login.microsoftonline.com/common/oauth2/token"

        $global:tok = Invoke-RestMethod -Method POST -Uri $url_oauth  -Body $body -ContentType "application/x-www-form-urlencoded"
        if ($o) {
            return $tok 
        }
        if ($tok) {
            New-TimeDatedVar -nam $tokenName -val $tok.access_token
            Write-Success ' Obtained', ($tokenName.split('_')[0]), 'token as', ('$' + $tokenName), 'at', (Get-Date).ToShortTimeString()
        }
        else {
            Write-Fail 'No token received'
        }
    }
    ############################

    ########## UTIL ############

    ############################

    function Get-Nonce {
        param (
            [Parameter(Mandatory = $true)]
            [string]$resource       = "graph.microsoft.com",

            [string]$client_id      = "1950a258-227b-4e31-a9cf-717495945fc2",

            [string]$redirect_uri   = "http://localhost",

            [array]$scope,

            [switch]$o
        )
    
        if ( $PSBoundParameters.containskey("debug") ) {
            if ( $debug = [bool]$PSBoundParameters.item("debug") ) { 
                $DebugPreference = "Continue"
            }
        }
        if (!($scope)){
            $scope = ".default"
        }     
        $headers = @{
            'User-Agent' = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 10.0; Win64; x64; Trident/7.0; .NET4.0C; .NET4.0E)'
            'UA-CPU'     = 'AMD64'
        }
        $url = "https://login.microsoftonline.com/$tenant_id/oauth2/v2.0/authorize?client_id=$client_id&response_type=code&redirect_uri=$redirect_uri&scope=$scope"
        Write-Debug ('URL:' + $url)

        $repns = Invoke-WebRequest -Uri $url -Method GET -Headers $headers -MaximumRedirection 0 -UseDefaultCredentials -SessionVariable g
        $nonc = ''
        $nonc = ($repns.content | Select-String '\"nonce\"\:\"([^\"]+)\"' | Foreach-Object { $_.Matches } | Foreach-Object { $_.Captures }).Groups[1].Value
        if ($nonc) {
            New-TimeDatedVar -nam nonce_azure -val $nonc
            Write-Debug ('Nonce obtained as ' + '$nonce_azure')
            if ($o) {
                return $nonc
            }
        }
        else {
            Write-Fail 'Could not get nonce'
            Break
        }
    }

    function Get-PrtCookie {

        param (
            [string]$nonce      = $nonce_azure,

            [switch]$o
        )


        if ( $PSBoundParameters.containskey("debug") ) {
            if ( $debug = [bool]$PSBoundParameters.item("debug") ) { 
                $DebugPreference = "Continue"
            }
        }
        Clear-Vars prtc, out, psi, p, fields

        if (test-path "C:\Program Files\Windows Security") {
            $targetFile = "C:\Program Files\Windows Security\BrowserCore\browsercore.exe"
        }
        else {
            $targetFile = 'C:\Windows\WinSxS\amd64_microsoft-windows-security-browsercore_31bf3856ad364e35_10.0.22621.1_none_bc639a90f23d859f\BrowserCore.exe'
        }
  

        $psi                        = New-Object System.Diagnostics.ProcessStartInfo
        $psi.FileName               = $targetFile
        $psi.UseShellExecute        = $false
        $psi.RedirectStandardInput  = $true
        $psi.RedirectStandardOutput = $true

        # $fields = "{`"method`":`"GetCookies`",`"uri`":`"https://login.microsoftonline.com/common/oauth2/authorize?sso_nonce=$nonce_azure`",`"sender`":`"https://login.microsoftonline.com`"}"
        $fields = "{`"method`":`"GetCookies`",`"uri`":`"https://login.microsoftonline.com/$tenant_id/oauth2/v2.0/authorize?sso_nonce=$nonce_azure`",`"sender`":`"https://login.microsoftonline.com`"}"
  
        $p                          = [System.Diagnostics.Process]::Start($psi)
        $p.StandardInput.BaseStream.Write([bitconverter]::GetBytes($fields.Length), 0, 4)
        $p.StandardInput.Write($fields)
        $out                        = $p.StandardOutput.ReadLine()

        if ($out.length -le 1) {
            $out = $p.StandardOutput.ReadLine()
        }

        

        $prtc = $out | Where-Object { $_.length -gt 1000 } | Select-String -AllMatches 'data\"[^\"]+\"([^\"]+)\"' | Foreach-Object { $_.Matches } | Foreach-Object { $_.Groups[1].Value } | Select-Object -First 1
        if ($prtc) {
            New-TimeDatedVar -nam prtCook_azure -val $prtc
            Write-Debug ('PRT cookie obtained as ' + '$prtCook_azure' )
            if ($o) {
                return $prtCook_azure
            }
        }
        else {
            Write-Fail 'Could not obtain new PRT'
        }
    }
        
    function Get-AuthCode {

        [cmdletbinding()]param (
            [Parameter(Mandatory = $true)]
            [string]$resource       = "graph.microsoft.com",

            [string]$prtCook        = $prtCook_azure,

            [string]$nonce          = $nonce_azure,

            [string]$client_id      = "1950a258-227b-4e31-a9cf-717495945fc2",

            [string]$redirect_uri   = "http://localhost",

            [string]$endpoint       = "https://login.microsoftonline.com",

            [array]$scope           = ".default",

            [switch]$challenge,

            [string]$basicAuth,

            [string]$ver            = '2' 
        )

        if ( $PSBoundParameters.containskey("debug") ) {
            if ( $debug = [bool]$PSBoundParameters.item("debug") ) { 
                $DebugPreference = "Continue"
            }
        }

        $redirect_uri   = ConvertTo-UrlEncode $redirect_uri
        $resource_uri   = ConvertTo-UrlEncode (ConvertTo-Uri $resource)
        $fullscope      = ConvertTo-UrlEncode ($resource_uri + $scope)

        Clear-Vars rr, s, t, exp, aCode, authCode_azure

        $url        = $endpoint + "/$tenant_id/oauth2/v2.0/authorize"
        $url        = $url + "?client_id=$client_id&response_type=code&response_mode=query&redirect_uri=$redirect_uri&scope=$fullscope"

        Write-Debug "scp:  $fullscope"
        Write-Debug "url:  $url"

        $headers = @{
            'User-Agent'                  = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 10.0; Win64; x64; Trident/7.0; .NET4.0C; .NET4.0E)'
            'x-ms-RefreshTokenCredential' = $prtCook_azure
        }


        # if ($ver -eq '2') {
        #     $fullscope  = [System.Web.HttpUtility]::UrlDecode($fullscope)
        #     $url        = $endpoint + "/Common/oauth2/authorize"
        #     $url        = $url + "?client_id=$client_id&response_type=code&response_mode=query&redirect_uri=$redirect_uri&scope=$fullscope"
        # }
        # else {
        #     $url        = $endpoint + "/Common/oauth2/authorize"
        #     $url        = $url + "?client_id=$client_id&response_type=code&response_mode=query&redirect_uri=$redirect_uri"
        #     $mscrid     = [guid]::NewGuid()
        #     $url        += "&client-request-id=$mscrid"
        # }

        # if ($basicAuth) {

        #     $bytes          = [System.Text.Encoding]::ASCII.GetBytes($basicAuth)
        #     $base64         = [System.Convert]::ToBase64String($bytes)
        #     $basicAuthValue = "Basic $base64"

        #     $mscrid         = [guid]::NewGuid()
        #     $url            += "&client-request-id=$mscrid"
        #     $headers        = @{
        #         'User-Agent'    = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 10.0; Win64; x64; Trident/7.0; .NET4.0C; .NET4.0E)'
        #         'Authorization' = $basicAuthValue
        #     }
        # }

        # else {

        #     $exp = Test-IsVarExpired nonce_azure

        #     if ($exp -eq 1) {
        #         Write-Debug 'Old nonce, renewing...'
        #         Get-Nonce -resource $resource
        #     }

        #     Clear-Vars exp 
        #     $exp = Test-IsVarExpired prtCook_azure
        #     if ($exp -eq 1) {
        #         Write-Debug 'Old prtCook, renewing...'
        #         Get-PrtCookie $nonce_azure
        #     } 

        #     if ($prtCook_azure) {
        #         $mscrid = [guid]::NewGuid()
        #         $url += "&client-request-id=$mscrid&mscrid=$mscrid&sso_nonce=$nonce_azure"
        #         $headers = @{
        #             'User-Agent'                  = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 10.0; Win64; x64; Trident/7.0; .NET4.0C; .NET4.0E)'
        #             'x-ms-RefreshTokenCredential' = $prtCook_azure
        #         }
        #         Write-Debug 'Set up for sso nonce and PRT cookie'
        #     }
        # }

        $global:rr = Invoke-WebRequest -Uri $url -Method GET -Headers $headers -MaximumRedirection 30 -UseDefaultCredentials -SessionVariable Global:g 
        if ($rr) {
            $aCode = $rr.content.split('"')[1].split("\")[0].split("=")[1]
            if ($aCode){
                $global:authCode_azure = $aCode 
                Add-Member -InputObject $(Get-Variable authCode_azure) -Name CreatedAt -MemberType NoteProperty -Value $(Get-Date) -Force
                Write-Debug 'Obtained auth code from first request'
            }
        }
        else {
            Write-Debug 'First request complete'
            $global:s = Invoke-WebRequest -Uri $url -Method GET -Headers $headers -MaximumRedirection 0 -UseDefaultCredentials -WebSession $g 
            if ($s.Content) {
                Write-Debug 'Second request complete'
            }
            else {
                Write-Debug 'Second request failed'
            }
            if ($s.Headers.Location) {
                $aCode = $s.Headers.Location | Select-String 'code=([^\&]+)' | Foreach-Object { $_.Matches } | Foreach-Object { $_.Groups[1].Value }
                New-TimeDatedVar -nam authCode_azure -val $aCode
                Write-Debug 'Obtained auth code from header'
            }
            elseif ($s.Content) {
                Write-Debug "Auth code not in headers.location"
                $aCode = $s.Content | Select-String 'code=([^\\]+)' | Foreach-Object { $_.Matches } | Foreach-Object { $_.Groups[1].Value } 
                if ($aCode -match 'cancel","') {
                    Clear-Vars aCode 
                    $resumeUrl = ($s.content | Select-String 'urlResume":"([^\"]+)').Matches.Captures.Groups[1].value
                    $global:u = Invoke-WebRequest -Uri $resumeUrl -Method GET -Headers $headers -MaximumRedirection 0 -UseDefaultCredentials -WebSession $g 
                    Write-Debug "response saved as `$u"
                }
                else {
                    $global:authCode_azure = $aCode 
                    Add-Member -InputObject $(Get-Variable authCode_azure) -Name CreatedAt -MemberType NoteProperty -Value $(Get-Date) -Force
                    Write-Debug 'Obtained auth code from body'
                }
            }
            else {
                Write-Fail 'No response received'
            }
        }
        $j = Get-Parameters $s -json 
        if ($j.urlLogin) {
            $global:t = Invoke-WebRequest -Uri $j.urlLogin -Method GET -Headers $headers -MaximumRedirection 0 -UseDefaultCredentials -WebSession $g


            $k = Get-Parameters $t -json 

            $global:v = Invoke-WebRequest -Uri $k.urlLogin -Method GET -Headers $headers -MaximumRedirection 0 -UseDefaultCredentials -WebSession $g


        }
        if ( (Get-Variable authCode_azure).Value.Length -gt 0 ) {
            Write-Debug 'Auth code obtained'
        }
        else {
            Write-Debug 'No authcode obtained'
        }
    }
    function Get-JwtWithAuthCode {
        [cmdletbinding()]param ( 
            
            [string]$resource       = "graph.microsoft.com",

            [string]$authCode       = $authCode_azure,

    
            [string]$client_id      = "1950a258-227b-4e31-a9cf-717495945fc2",
    
            [string]$tenant_id      = $tenant_id,

            [array]$scope           = ".default",

            [string]$redirect_uri   = "http://localhost/"
   
        )
        if ( $PSBoundParameters.containskey("debug") ) {
            if ( $debug = [bool]$PSBoundParameters.item("debug") ) {
                $DebugPreference = "Continue"
            }
        }

        $mscrid = [guid]::NewGuid()


        $UserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36"
        $Headers=@{}
        $Headers["User-Agent"]  = $UserAgent
        $uri                    = "https://login.microsoftonline.com/Common/oauth2/token?api-version=2.0"
        $body=@{
            "client_id"         = $client_id
            # "grant_type"        = "urn:ietf:params:oauth:grant-type:authorization_code"
            "grant_type"        = "authorization_code"
            "code"              = $authCode
            "redirect_uri"      = $redirect_uri
            "client-request-id" = $mscrid
            # "resource"          = "graph.microsoft.com"

        }
        $global:lastRes = Invoke-RestMethod -UseBasicParsing -Method Post -Uri $uri -Headers $Headers -Body $body


        # $resource = ConvertTo-Uri $resource
        Write-Debug "res:`t $resource"
        # if ($scope){
        #     $finalScope = $scope
        # }
        # else {
        #     $finalScope = ".default"
        # }
        # $scopes = [System.Collections.ArrayList]::New()
        # Foreach ($scp in $scope) {
        #     [void]$scopes.Add($resource + $scp)
        # }
        # $finalScope = $scopes -join " "
        Write-Debug "scp:`t $scope"

        # $url = "https://login.microsoftonline.com/$tenant_id/oauth2/v2.0/token" 
        Write-Debug "url:`t $uri"


        # $global:reqBody = [ordered]@{
        #     resource                = $resource
        #     scope                   = $finalScope
        #     code                    = $authCode
        #     grant_type              = "authorization_code"
        #     "client-request-id"     = $mscrid
        #     redirect_uri            = $redirect_uri

        # }

        # $lastRes = Invoke-RestMethod -Uri $url -Method POST -Body $reqBody 
        
        if ($lastRes.access_token.length -gt 0){
            $aud                    = (Format-Jwt $lastRes.access_token).aud
            Write-Debug "Aud: $aud"
        }
        else {
            Write-Fail "Did not receive response from $url"
        }
        New-TimeDatedVar -nam lastRes_azure -val $lastRes
        New-TimeDatedVar -nam lastJwt_azure -val $lastRes.access_token
        Write-Debug 'Saved as $lastJwt_azure'
    }

    function Get-CredObj {

        param(
            [string]$username,

            [string]$pwdVariable
        )
        $password = (Get-Variable $pwdVariable).Value 
        $secPass = ConvertTo-SecureString $password -AsPlainText -Force
        $o = New-Object System.Management.Automation.PSCredential ($username, $secPass)
        return $o
    }
    function GetTokenInformation {

        param
        (
            [Parameter(Mandatory = $true)]
            [IntPtr]
            $TokenHandle,
        
            [Parameter(Mandatory = $true)]
            $TokenInformationClass 
        )

        # initial query to determine the necessary buffer size
        $TokenPtrSize = 0
        $Success = $Advapi32::GetTokenInformation($TokenHandle, $TokenInformationClass, 0, $TokenPtrSize, [ref]$TokenPtrSize)
        [IntPtr]$TokenPtr = [System.Runtime.InteropServices.Marshal]::AllocHGlobal($TokenPtrSize)
    
        # retrieve the proper buffer value
        $Success = $Advapi32::GetTokenInformation($TokenHandle, $TokenInformationClass, $TokenPtr, $TokenPtrSize, [ref]$TokenPtrSize); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()
    
        if ($Success) {
            switch ($TokenInformationClass) {
                1 {
                    # TokenUser    
                    $TokenUser = $TokenPtr -as $TOKEN_USER
                    ConvertSidToStringSid -SidPointer $TokenUser.User.Sid
                }
                3 {
                    # TokenPrivilege
                    # query the process token with the TOKEN_INFORMATION_CLASS = 3 enum to retrieve a TOKEN_PRIVILEGES structure
                    $TokenPrivileges = $TokenPtr -as $TOKEN_PRIVILEGES
                
                    $sb = New-Object System.Text.StringBuilder
                
                    for ($i = 0; $i -lt $TokenPrivileges.PrivilegeCount; $i++) {
                        if ((($TokenPrivileges.Privileges[$i].Attributes -as $LuidAttributes) -band $LuidAttributes::SE_PRIVILEGE_ENABLED) -eq $LuidAttributes::SE_PRIVILEGE_ENABLED) {
                            $sb.Append(", $($TokenPrivileges.Privileges[$i].Luid.LowPart.ToString())") | Out-Null  
                        }
                    }
                    Write-Output $sb.ToString().TrimStart(', ')
                }
                17 {
                    # TokenOrigin
                    $TokenOrigin = $TokenPtr -as $LUID
                    Write-Output (Get-LogonSession -LogonId $TokenOrigin.LowPart)
                }
                22 {
                    # TokenAccessInformation
            
                }
                25 {
                    # TokenIntegrityLevel
                    $TokenIntegrity = $TokenPtr -as $TOKEN_MANDATORY_LABEL
                    switch (ConvertSidToStringSid -SidPointer $TokenIntegrity.Label.Sid) {
                        $UNTRUSTED_MANDATORY_LEVEL {
                            Write-Output "UNTRUSTED_MANDATORY_LEVEL"
                        }
                        $LOW_MANDATORY_LEVEL {
                            Write-Output "LOW_MANDATORY_LEVEL"
                        }
                        $MEDIUM_MANDATORY_LEVEL {
                            Write-Output "MEDIUM_MANDATORY_LEVEL"
                        }
                        $MEDIUM_PLUS_MANDATORY_LEVEL {
                            Write-Output "MEDIUM_PLUS_MANDATORY_LEVEL"
                        }
                        $HIGH_MANDATORY_LEVEL {
                            Write-Output "HIGH_MANDATORY_LEVEL"
                        }
                        $SYSTEM_MANDATORY_LEVEL {
                            Write-Output "SYSTEM_MANDATORY_LEVEL"
                        }
                        $PROTECTED_PROCESS_MANDATORY_LEVEL {
                            Write-Output "PROTECTED_PROCESS_MANDATORY_LEVEL"
                        }
                        $SECURE_PROCESS_MANDATORY_LEVEL {
                            Write-Output "SECURE_PROCESS_MANDATORY_LEVEL"
                        }
                    }
                }
            }
        }
        else {
            Write-Debug "GetTokenInformation Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
        }        
        try {
            [System.Runtime.InteropServices.Marshal]::FreeHGlobal($TokenPtr)
        }
        catch {
    
        }
    }
    function Get-AllKlist {

        $output = [System.Collections.ArrayList]::new()
        $klist = klist 
        $tickets = ($klist | Where-Object { $_ }) | Select-String Client -Context 0, 13

        foreach ($tick in $tickets) {
            $client = ($tick.toString() | Select-String 'Client: ([^\s]+)').Matches.Groups[1].Value
            $server = ($tick.toString() | Select-String 'Server: ([^\s]+)').Matches.Groups[1].Value
            $server = ($server.split('/') | Select-Object -First 2 ) -join '/'
    
            $start = (($tick.toString() | Select-String 'Start Time:\s+([^\(]+)').Matches.Groups[1].Value).ToString().trim()
            $start = ([datetime]::parse($start, $null)).ToString('yyyy-MM-dd HH:mm:ss')
    
            $end = (($tick.toString() | Select-String 'End Time:\s+([^\(]+)').Matches.Groups[1].Value).ToString().trim()
            $end = ([datetime]::parse($end, $null)).ToString('yyyy-MM-dd HH:mm:ss')
    
            if ($server -match 'krbtgt') {
                $type = 'tgt'
            }
            else {
                $type = 'svc'
            }

            $out = New-Object -TypeName psobject -Property @{
                Principal  = $client.ToLower() 
                Resource   = $server.ToLower() 
                IssueTime  = $start 
                ExpireTime = $end
                Type       = $type 
            }
            [void]$output.Add($out)
        }
        return $output
    }
    function Get-AllDsregcmd {
        $output = @()
        $status = dsregcmd /status 
        if ($status) {
            $start = ($status | Select-String 'UpdateTime[\s\:]+([^\.]+)').Matches.Groups[1].Value
            $start = [System.TimeZoneInfo]::ConvertTimeFromUtc($start, [System.TimeZoneInfo]::FindSystemTimeZoneById('Central Standard Time'))
            $start = ([datetime]::parse($start, $null)).ToString('yyyy-MM-dd HH:mm:ss')

            $end = ($status | Select-String 'ExpiryTime[\s\:]+([^\.]+)').Matches.Groups[1].Value
            $end = [System.TimeZoneInfo]::ConvertTimeFromUtc($end, [System.TimeZoneInfo]::FindSystemTimeZoneById('Central Standard Time'))
            $end = ([datetime]::parse($end, $null)).ToString('yyyy-MM-dd HH:mm:ss')
    
            try {
                $client = [System.DirectoryServices.AccountManagement.UserPrincipal]::Current.SamAccountName
            }
            catch {
                $client = "-"
            }
            $server = ($status | Select-String 'EnterprisePrtAuthority[\s\:]+(.*)').Matches.Groups[1].Value

            $output += New-Object -TypeName psobject -Property @{
                Principal  = $client.ToLower() 
                Resource   = $server.ToLower() 
                IssueTime  = $start 
                ExpireTime = $end
                Type       = "PRT"
            }
            return $output
        }
    }

    function Convert-SSidTToNName {
        [CmdletBinding()]
        param(
            [Parameter(Mandatory = $True, ValueFromPipeline = $True)]
            [String]
            [ValidatePattern('^S-1-.*')]
            $SID
        )

        try {
            $SID2 = $SID.trim('*')

            # try to resolve any built-in SIDs first
            #   from https://support.microsoft.com/en-us/kb/243330
            Switch ($SID2) {
                'S-1-0' { 'Null Authority' }
                'S-1-0-0' { 'Nobody' }
                'S-1-1' { 'World Authority' }
                'S-1-1-0' { 'Everyone' }
                'S-1-2' { 'Local Authority' }
                'S-1-2-0' { 'Local' }
                'S-1-2-1' { 'Console Logon ' }
                'S-1-3' { 'Creator Authority' }
                'S-1-3-0' { 'Creator Owner' }
                'S-1-3-1' { 'Creator Group' }
                'S-1-3-2' { 'Creator Owner Server' }
                'S-1-3-3' { 'Creator Group Server' }
                'S-1-3-4' { 'Owner Rights' }
                'S-1-4' { 'Non-unique Authority' }
                'S-1-5' { 'NT Authority' }
                'S-1-5-1' { 'Dialup' }
                'S-1-5-2' { 'Network' }
                'S-1-5-3' { 'Batch' }
                'S-1-5-4' { 'Interactive' }
                'S-1-5-6' { 'Service' }
                'S-1-5-7' { 'Anonymous' }
                'S-1-5-8' { 'Proxy' }
                'S-1-5-9' { 'Enterprise Domain Controllers' }
                'S-1-5-10' { 'Principal Self' }
                'S-1-5-11' { 'Authenticated Users' }
                'S-1-5-12' { 'Restricted Code' }
                'S-1-5-13' { 'Terminal Server Users' }
                'S-1-5-14' { 'Remote Interactive Logon' }
                'S-1-5-15' { 'This Organization ' }
                'S-1-5-17' { 'This Organization ' }
                'S-1-5-18' { 'Local System' }
                'S-1-5-19' { 'NT Authority' }
                'S-1-5-20' { 'NT Authority' }
                'S-1-5-80-0' { 'All Services ' }
                'S-1-5-32-544' { 'BUILTIN\Administrators' }
                'S-1-5-32-545' { 'BUILTIN\Users' }
                'S-1-5-32-546' { 'BUILTIN\Guests' }
                'S-1-5-32-547' { 'BUILTIN\Power Users' }
                'S-1-5-32-548' { 'BUILTIN\Account Operators' }
                'S-1-5-32-549' { 'BUILTIN\Server Operators' }
                'S-1-5-32-550' { 'BUILTIN\Print Operators' }
                'S-1-5-32-551' { 'BUILTIN\Backup Operators' }
                'S-1-5-32-552' { 'BUILTIN\Replicators' }
                'S-1-5-32-554' { 'BUILTIN\Pre-Windows 2000 Compatible Access' }
                'S-1-5-32-555' { 'BUILTIN\Remote Desktop Users' }
                'S-1-5-32-556' { 'BUILTIN\Network Configuration Operators' }
                'S-1-5-32-557' { 'BUILTIN\Incoming Forest Trust Builders' }
                'S-1-5-32-558' { 'BUILTIN\Performance Monitor Users' }
                'S-1-5-32-559' { 'BUILTIN\Performance Log Users' }
                'S-1-5-32-560' { 'BUILTIN\Windows Authorization Access Group' }
                'S-1-5-32-561' { 'BUILTIN\Terminal Server License Servers' }
                'S-1-5-32-562' { 'BUILTIN\Distributed COM Users' }
                'S-1-5-32-569' { 'BUILTIN\Cryptographic Operators' }
                'S-1-5-32-573' { 'BUILTIN\Event Log Readers' }
                'S-1-5-32-574' { 'BUILTIN\Certificate Service DCOM Access' }
                'S-1-5-32-575' { 'BUILTIN\RDS Remote Access Servers' }
                'S-1-5-32-576' { 'BUILTIN\RDS Endpoint Servers' }
                'S-1-5-32-577' { 'BUILTIN\RDS Management Servers' }
                'S-1-5-32-578' { 'BUILTIN\Hyper-V Administrators' }
                'S-1-5-32-579' { 'BUILTIN\Access Control Assistance Operators' }
                'S-1-5-32-580' { 'BUILTIN\Access Control Assistance Operators' }
                Default { 
                    $Obj = (New-Object System.Security.Principal.SecurityIdentifier($SID2))
                    $Obj.Translate( [System.Security.Principal.NTAccount]).Value
                }
            }
        }
        catch {
            Write-Verbose "Invalid SID: $SID"
            $SID
        }
    }

    ############################

    ########## MAIN ############

    ############################


    $global:IdentityToolsFunctions = $IdentityToolsCmdlets.Name

    $IdentityToolsCmdlets | Foreach-Object { Set-Alias -Name $_.Alias -Value $_.Name -Scope Global -Force }

    Export-ModuleMember -Function $IdentityToolsFunctions 
}
