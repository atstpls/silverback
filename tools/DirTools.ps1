New-Module -Name DirectoryTools -Scriptblock {

    ############################

    #########  VARS  ###########

    ############################
    $global:app_ids = @(
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure ESTS Service"
                AppId   = "00000001-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Windows Azure Active Directory"
                AppId   = "00000002-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Exchange Online"
                AppId   = "00000002-0000-0ff1-ce00-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Graph"
                AppId   = "00000003-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SharePoint Online"
                AppId   = "00000003-0000-0ff1-ce00-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Skype for Business Online"
                AppId   = "00000004-0000-0ff1-ce00-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Azure Workflow"
                AppId   = "00000005-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Yammer"
                AppId   = "00000005-0000-0ff1-ce00-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office portal"
                AppId   = "00000006-0000-0ff1-ce00-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Dynamics CRM"
                AppId   = "00000007-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Exchange Online Protection"
                AppId   = "00000007-0000-0ff1-ce00-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft.Azure.DataMarket"
                AppId   = "00000008-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Power BI Service"
                AppId   = "00000009-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Intune"
                AppId   = "0000000a-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Seller Dashboard"
                AppId   = "0000000b-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft App Access Panel"
                AppId   = "0000000c-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Rights Management Services"
                AppId   = "00000012-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Classic Portal"
                AppId   = "00000013-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft.Azure.SyncFabric"
                AppId   = "00000014-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MicrosoftAzureActiveAuthn"
                AppId   = "0000001a-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Power BI Information Service"
                AppId   = "0000001b-0000-0000-c000-000000000000"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Visio Data Visualizer"
                AppId   = "00695ed2-3202-4156-8da1-69f60065e255"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office 365 Management (mobile app)"
                AppId   = "00b41c95-dab0-4487-9791-b9d2c32c80f2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Teams Calling Meeting Devices Services"
                AppId   = "00edd498-7c0c-4e68-859c-5a55d518c9c0"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Skype for Business Application Configuration Service"
                AppId   = "00f82732-f451-4a01-918c-0e9896e784f9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Data Warehouse Polybase"
                AppId   = "0130cc9f-7ac5-4026-bd5f-80a08a54e6d9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Excel Online"
                AppId   = "0177ed5f-9edc-440f-89c2-e5aaeebdc1cc"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Device Registration Service"
                AppId   = "01cb2876-7ebd-4aa4-9cc9-d28bd4d359a9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MS-PIM"
                AppId   = "01fc33a7-78ba-4d2f-a4b7-768e336e890e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure SQL Database"
                AppId   = "022907d3-0f1b-48f7-badc-1ba6abab6d66"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "IDML Graph Resolver Service Fairfax"
                AppId   = "02379041-e4fa-4d5e-85bc-9534fd38b347"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "cisepvmstors01002"
                AppId   = "02d2267e-2c59-4982-a936-65bba0a30b90"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Application Registration Portal"
                AppId   = "02e3ae74-c151-4bda-b8f0-55fbf341de08"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Monitor Restricted"
                AppId   = "035f9e1d-4f00-4419-bf50-bf2d87eb4878"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Forms"
                AppId   = "038ed4f5-da78-47bc-8176-9153c79e0edb"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Spring Cloud Domain-Management"
                AppId   = "03b39d0f-4213-4864-a245-b1476ec03169"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Windows Notification Service"
                AppId   = "04436913-cf0d-4d2a-9cc6-2ffe7f1d3d1c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MDATPNetworkScanAgent"
                AppId   = "04687a56-4fc2-4e36-b274-b862fb649733"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Policy Administration Service"
                AppId   = "0469d4cd-df37-4d93-8a61-f8c75b809164"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Azure Cross-platform Command Line Interface"
                AppId   = "04b07795-8ddb-461a-bbee-02f9e1bf7b46"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Cloud App Security"
                AppId   = "05a65629-4c1b-48c1-a78b-804c4abdd4af"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "CIS-P-SPLUNK-APPREG-01"
                AppId   = "05d3f49e-5d67-494a-b63a-f1e49e6fe73c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "CIS-P-AZDEVOPS-APPREG-01"
                AppId   = "065a3c62-f994-4ca6-a552-22be776e8f29"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Power Platform Admin Center"
                AppId   = "065d9450-1e87-434e-ac2f-69af271549ed"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AAD Pin redemption client"
                AppId   = "06c6433f-4fb8-4670-b2cd-408938296b8e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SubstrateActionsService"
                AppId   = "06dd8193-75af-46d0-84bb-9b9bcaa89e8b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Machine Learning"
                AppId   = "0736f41a-0425-4b46-bdb5-1563eff02385"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "0762b11b-10bb-4f20-b3d3-75a06bce5ef9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SurveyMonkey"
                AppId   = "07978fee-621a-42df-82bb-3eabc6511c26"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SharePoint Online Web Client Extensibility"
                AppId   = "08e18876-6177-487e-b8b5-cf950c1e598c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Tech Community"
                AppId   = "09213cdc-9f30-4e82-aa6f-9b6e8d82dab3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "BenefitsFD"
                AppId   = "09a984f4-014c-43de-ae0c-7ec73dc053d3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ProjectWorkManagement"
                AppId   = "09abbdfd-ed23-44ee-a2d9-a627aa1c90f3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Cortana Experience with O365"
                AppId   = "0a0a29f9-0a25-49c7-94bf-c53c3f8fa69d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Mobile Application Management"
                AppId   = "0a5f63c0-b750-4f38-a71c-4fc0d58b89e2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "0b1c34b2-1d7f-48a2-805b-8fdf3bcad42e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "0b1ca7e3-3951-41d8-acdc-61eeb87bc87a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Graph Change Tracking"
                AppId   = "0bf30f3b-4a52-48df-9a82-234910c4a086"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Android App"
                AppId   = "0c1307d4-29d6-4389-a11c-5cbe7f65d7fa"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams Partner Tenant Administration"
                AppId   = "0c708d37-30b2-4f22-8168-5d0cba6f37be"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "0cb171dd-b35d-410b-af9f-9b841f303dbb"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "PowerApps"
                AppId   = "0cb2a3b9-c0b0-4f92-95e2-8955085f78c2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Media Analysis and Transformation Service"
                AppId   = "0cd196ee-71bf-4fd6-a57c-b491ffd4fb1e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Access IoT Hub Device Provisioning Service"
                AppId   = "0cd79364-7a90-4354-9984-6e36c841418d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office365 Zoom"
                AppId   = "0d38933a-0bbd-41ca-9ebd-28c4b5ba7cb7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Notebooks"
                AppId   = "0d973830-135d-4ffa-92cd-4c57650dc220"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Azure EA"
                AppId   = "0dbcecc1-8b29-410a-b222-b4f5241c6d0f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Tableau Desktop "
                AppId   = "0e6ea828-e179-4fe8-97b8-bafd3113c8ec"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams Intelligent Workspaces Interactions Service"
                AppId   = "0eb4bf93-cb63-4fe1-9d7d-70632ccf3082"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Photos"
                AppId   = "0ec2d138-5a70-4a33-b2c7-7d296c996ace"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office UWP PWA"
                AppId   = "0ec893e0-5785-4de6-99da-4ed124e5296c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Flow CDS Integration Service"
                AppId   = "0eda3b13-ddc9-4c25-b7dd-2f6ea073d6b7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Teams User Engagement Profile Service"
                AppId   = "0f54b75d-4d29-4a92-80ae-106a60cd8f5d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OfficeClientService"
                AppId   = "0f698dd4-f011-4d23-a33e-b36416dcb1e6"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Intune oAuth Graph"
                AppId   = "0f6e3eff-886c-4f7a-a1d7-6f1f0177273b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AIGraphClient"
                AppId   = "0f6edad5-48f2-4585-a609-d252b1c52770"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams ATP Service"
                AppId   = "0fa37baf-7afc-4baf-ab2d-d5bb891d53ef"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "App Service Fairfax"
                AppId   = "100d712e-3808-4aca-8025-3d92b2807f4e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "CLAIMS3 Remote App"
                AppId   = "10383c9c-326f-4ae3-bd38-0c13f689ec89"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Windows Azure Application Insights"
                AppId   = "11c174dc-1945-4a9a-a36b-c79a0f246b9b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Automated Call Distribution"
                AppId   = "11cd3e2e-fccb-42ad-ad00-878b93575e07"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Time Series Insights"
                AppId   = "120d688d-1518-4cf7-bd38-182f158850b6"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams PowerShell"
                AppId   = "12128f48-ec9e-42f0-b203-ea49fb6af367"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure OSSRDBMS Database"
                AppId   = "123cd850-d9df-40bd-94d5-c9f07b7fa203"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Metrics Monitor API"
                AppId   = "12743ff8-d3de-49d0-a4ce-6c91a4245ea0"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "127b34ba-c28c-40ff-af90-1ba3d147e8c5"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Learn On Demand LMS"
                AppId   = "143f708f-6a25-4180-bbe3-3167e75f8b47"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Intune CMDeviceService"
                AppId   = "14452459-6fa6-4ec0-bc50-1528a1a06bf0"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Azure"
                AppId   = "15689b28-1333-4213-bb64-38407dde8a5e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Sync client"
                AppId   = "1651564e-7ce4-4d99-88be-0a65050d8dc3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office Hive"
                AppId   = "166f1b03-5b19-416f-a94b-1d7aa2d247dc"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OCaaS Worker Services"
                AppId   = "167e2ded-f32d-49f5-8a10-308b921bc7ee"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Twitter"
                AppId   = "1683fa2e-8c1a-41e7-92f9-940cc8852759"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Power platform Community"
                AppId   = "17bf704f-25db-4ea9-af47-50b552cbb0f0"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "17cb5e59-2bbb-465c-8e4b-9dc5f06b6052"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Marketplace Caps API"
                AppId   = "184909ca-69f1-4368-a6a7-c558ee6eb0bd"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Intune DeviceActionService"
                AppId   = "18a4ad1e-427c-4cad-8416-ef674e801d32"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Machine Learning Services"
                AppId   = "18a66f5f-dbdf-4c17-9dd7-1634712a9cbe"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Reply-At-Mention"
                AppId   = "18f36947-75b0-49fb-8d1c-29584a55cac5"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Docs"
                AppId   = "18fbca16-2224-45f6-85b0-f7bf2b39b3f3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AZ PowerShell Module"
                AppId   = "1950a258-227b-4e31-a9cf-717495945fc2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Command Service"
                AppId   = "19686ca6-5324-4571-a231-77e026b0e06f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure DNS"
                AppId   = "19947cfd-0303-466c-ac3c-fcc19a7a1570"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams UIS"
                AppId   = "1996141e-2b07-4491-927a-5a024b335c78"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MySignins"
                AppId   = "19db86c3-b2b9-44cc-b339-36da233a3be2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Lab Services"
                AppId   = "1a14be2a-e903-4cec-99cf-b2e209259a0f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AzureMonitor"
                AppId   = "1a1facb7-70da-4695-a48c-600b997a20cf"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Active Directory PowerShell"
                AppId   = "1b730954-1685-4b74-9bfd-dac224a7b894"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "kanban-app.virtosoftware.com"
                AppId   = "1bcf8bbe-a3f7-4aba-b044-b95fc275a204"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "cisepvmstors02001"
                AppId   = "1be2247c-3e52-41f3-98b9-265d5ce3fc79"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Skype Team Substrate connector"
                AppId   = "1c0ae35a-e2ec-4592-8e08-c40884656fa5"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Common Data Service License Management"
                AppId   = "1c2909a7-6432-4263-a70d-929a3c1f9ee5"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "O365Account"
                AppId   = "1cda9b54-9852-4a5a-96d4-c2ab174f9edf"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Azure Policy Insights"
                AppId   = "1d78a85d-813d-46f0-b496-dd72f50a3ec0"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Sway"
                AppId   = "1dea5cdc-0d8b-4b12-901c-8d8afc3c6cf2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft B2B Admin Worker"
                AppId   = "1e2ca66a-c176-45ea-a877-e87f7231e0ee"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "NetworkTrafficAnalyticsService"
                AppId   = "1e3e4475-288f-4018-a376-df66fd7fac5f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Webex Teams Calendar Service "
                AppId   = "1e3faf23-d2d2-456a-9e3e-55db63b869b0"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Skype Presence Service"
                AppId   = "1e70cd27-4707-4589-8ec5-9bd20c472a46"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Multi-Factor Auth Connector"
                AppId   = "1f5530b3-261a-47a9-b357-ded261e17918"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Get Help"
                AppId   = "1f7f6f43-2f81-429c-8499-293566d0ab0c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "CIS-P-O365HEALTH-APPREG-01"
                AppId   = "1fad16e9-fc0b-47cc-bb21-071cd5154bfc"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Teams client"
                AppId   = "1fec8e78-bce4-4aaf-ab1b-5451cc387264"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Conference Auto Attendant"
                AppId   = "207a6836-d031-4764-a9d8-c1193f455f21"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MarketplaceAPI ISV"
                AppId   = "20e940b3-4c77-4b0b-9a53-9e16a1b010a7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "IC3 Long Running Operations Service"
                AppId   = "21a8a852-89f4-4947-a374-b26b2db3d365"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "21b33a72-9307-409a-ad41-ec798cfadf17"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft To-Do client "
                AppId   = "22098786-6e16-43cc-a27d-191a01a1e3b5"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Modern Contact Master"
                AppId   = "224a7b82-46c9-4d6b-8db0-7360fb444681"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Cortana at Work Bing Services"
                AppId   = "22d7579f-06c2-4baa-89d2-e844486adb9d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Flow CDS Integration Service GCC"
                AppId   = "2344da01-132e-4b8c-baba-9e5bf15c344e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Workday"
                AppId   = "234a1a24-5021-42e5-a087-335f9006d64b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ACOM Azure Website"
                AppId   = "23523755-3a2b-41ca-9315-f81f3f566a95"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ciswpvmstors04001"
                AppId   = "23740746-18f0-4d24-92cd-98ef1918a311"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OCPS Checkin Service"
                AppId   = "23c898c1-f7e8-41da-9501-f16571f8d097"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "app.flowforma.com"
                AppId   = "2409ef34-9af0-41ea-9565-8b3af816a01b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Domain Controller Services"
                AppId   = "2565bd9d-da50-47d4-8b85-4c97f669dc36"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Centralized Deployment"
                AppId   = "257601fd-462f-4a21-b623-7f719f0f90f4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Cloud App Security"
                AppId   = "25a6a87d-1e19-4c71-9cb0-16e88ff608f1"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Workflow"
                AppId   = "25c9380f-7b3d-493f-b769-db8dfe3f1194"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "CIS-P-SOLARWINDS-APPREG-01"
                AppId   = "25cecc5d-6196-45c5-ad38-e14bfe0800f2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Backup Management Service"
                AppId   = "262044b1-e2ce-469f-a196-69ab7ada62d3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Stream Service"
                AppId   = "2634dd23-5e5a-431c-81ca-11710d9079f4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Universal Store Native Client"
                AppId   = "268761a2-03f3-40df-8a8b-c3db24145b6b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Skype Teams Calling API Service"
                AppId   = "26a18ebc-cdf7-4a6a-91cb-beb352805e81"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Intune Checkin"
                AppId   = "26a4ae64-5862-427f-a9b0-044e62572a4f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Power BI/Cortana Integration"
                AppId   = "26a7ee05-5602-4d76-a7ba-eae8b7b67941"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Exact Data Match Service"
                AppId   = "273404b8-7ebc-4360-9f90-b40417f77b53"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "KustoService"
                AppId   = "2746ea77-4702-4b45-80ca-3c97e680e8b7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MS MAM Service API"
                AppId   = "27922004-5251-4030-b22d-91ecd9a37ea4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "My Apps"
                AppId   = "2793995e-0a7d-40d7-bd35-6968ba142197"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Bot Service CMEK Prod"
                AppId   = "27a762be-14e7-4f92-899c-151877d6d497"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "27ab2942-bce0-4536-a384-00e0ec84bf90"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Skype Business Voice Directory"
                AppId   = "27b24f1f-688b-4661-9594-0fdfde972edc"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Storefronts"
                AppId   = "28b567f6-162c-4f54-99a0-6887f387bbcc"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Salesforce"
                AppId   = "29104cd9-d43e-40db-ad10-2fd98c6803ba"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Authentication Broker (Azure MDM client)"
                AppId   = "29d9ed98-a469-4536-ade2-f981bc1d605e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Iot Hub Publisher App"
                AppId   = "29f411f1-b2cf-4043-8ac8-2185d7316811"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "PowerBI content pack"
                AppId   = "2a0c3efa-ba54-4e55-bdc0-770f9e39e9ee"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Cortana at Work Service"
                AppId   = "2a486b53-dbd2-49c0-a2bc-278bdfc30833"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office Online Client AAD- Augmentation Loop"
                AppId   = "2abdc806-e091-4495-9b10-b04d93c3f040"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "2b5ea956-bad8-4023-ad97-ea5c5ee14789"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MicrosoftTeamsCortanaSkills"
                AppId   = "2bb78a2a-f8f1-4bc3-8ecf-c1e15a0726e6"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Euclid"
                AppId   = "2ca80e7c-4ad1-444f-88f5-58f92b71b7b0"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Traffic Manager and DNS"
                AppId   = "2cf9eb86-36b5-49dc-86ae-9a63135dfa8c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Application Change Service"
                AppId   = "2cfc91a4-7baa-4a8f-a6c9-5f3d279060b8"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ciswpvmstors01002"
                AppId   = "2d15e3d9-ecf2-422f-a233-a2d6e9887a00"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OneNote"
                AppId   = "2d4d3d8e-2be3-4bef-9f87-7875a61c29de"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "2d77c928-2153-40d4-a866-240b109219a4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Dynamics CRM Learning Path"
                AppId   = "2db8cb1d-fb6c-450b-ab09-49b6ae35186b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams Admin Portal Service"
                AppId   = "2ddfbe71-ed12-4123-b99b-d5fc8a062a79"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Dynamics Data Integration"
                AppId   = "2e49aa60-1bd3-43b6-8ab6-03ada3d9f08b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft password reset service-Fairfax"
                AppId   = "2e5ecfc8-ea79-48bd-8140-c19324acb278"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "StarsLetsUATAddIn"
                AppId   = "2efc5eff-c8f2-4563-ba41-9691113eb249"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office 365 Information Protection"
                AppId   = "2f3f02c9-5679-4a5c-a605-0de55b07d135"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MS-CE-CXG-MAC-AadShadowRoleWriter"
                AppId   = "2f5afa01-cdcb-4707-a62a-0803cc994c60"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AzureDatabricks"
                AppId   = "2ff814a6-3304-4ab8-85cb-cd0e6f879c1d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "30dd57ec-e8ad-4989-946c-0562ab095ee5"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams ADL"
                AppId   = "30e31aeb-977f-4f4f-a483-b61e8377b302"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SharePoint Notification Service"
                AppId   = "3138fe80-4087-4b04-80a6-8866c738028a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Cost Management by Cloudyn"
                AppId   = "314a1cc2-6261-4276-87d3-93f824163983"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Cost Management XCloud"
                AppId   = "3184af01-7a88-49e0-8b55-8ecdce0aa950"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "NFV Resource Provider"
                AppId   = "328fd23b-de6e-462c-9433-e207470a5727"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "PowerAppsService"
                AppId   = "331cc017-5973-4173-b270-f0042fddfd75"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "BrowserStack"
                AppId   = "33261ead-27d2-41e8-97e5-24319826c2af"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SPAuthEvent"
                AppId   = "3340b944-b12e-47d0-b46b-35f08ec1d8ee"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OneDrive Web"
                AppId   = "33be1cef-03fb-444b-8fd3-08ca1b4d803f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Support - Network Watcher"
                AppId   = "341b7f3d-69b3-47f9-9ce7-5b7f4945fdbd"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Power Platform Governance Services"
                AppId   = "342f61e2-a864-4c50-87de-86abc6790d49"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Power BI Data Refresh"
                AppId   = "34cc6120-8c17-428c-b5aa-bede141fb74a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Mobile Application Management Backend"
                AppId   = "354b5b6d-abd6-4736-9f51-1be80049b91f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "CosmosDB Dedicated Instance"
                AppId   = "36e2398c-9dd3-4f29-9a72-d9f2cfc47ad9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AzureSupportCenter"
                AppId   = "37182072-3c9c-4f6a-a4b3-b3f91cacffce"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Windows VM Sign-In"
                AppId   = "372140e0-b3b7-4226-8ef9-d57986796201"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Media Services"
                AppId   = "374b2a64-3b6b-436b-934c-b820eacca870"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Unified Compliance Console"
                AppId   = "37659223-7626-47f7-8482-dedb321c2dc9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MARS"
                AppId   = "37ccb541-667a-4428-947a-f5dda698fa3c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "383248fc-70c5-4949-8189-963383a52ca3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office Management API Editor https://manage.office.com"
                AppId   = "389b1b32-b5d5-43b2-bddc-84ce938d6737"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Flow Portal GCC"
                AppId   = "38a893b6-d74c-4786-8fe7-bc3b4318e881"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft AAD Cloud AP"
                AppId   = "38aa3b87-a06d-4817-b275–7a316988d93b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft People Cards Service"
                AppId   = "394866fc-eedb-4f01-8536-3ff84b16be2a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Skype For Business Powershell Server Application"
                AppId   = "39624784-6cbe-4a60-afbe-9f46d10fdb27"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure AD Identity Governance - SPO Management"
                AppId   = "396e7f4b-41ea-4851-b04d-65de6cf1b4a3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "IC3 Gateway"
                AppId   = "39aaf054-81a5-48c7-a4f8-0293012095b9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Dynamics Provision"
                AppId   = "39e6ea5b-4aa4-4df2-808b-b6b5fb8ada6f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "3a55360a-baaa-4e3c-afcf-b22f99a74187"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Protected Message Viewer"
                AppId   = "3a9ddf38-83f3-4ea1-a33a-ecf934644e2d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "O365 Customer Monitoring"
                AppId   = "3aa5c166-136f-40eb-9066-33ac63099211"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Windows Admin Center"
                AppId   = "3aa85724-c5ce-42f5-b7f9-36b5a387b7b4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Smart Alerts"
                AppId   = "3af5a1e8-2459-45cb-8683-bcd6cccbcc13"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AzureBackupReporting"
                AppId   = "3b2fa68d-a091-48c9-95be-88d572e08fb7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "3bab4436-6f99-4c7a-8141-f2c83bfb76b1"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Demeter.WorkerRole"
                AppId   = "3c31d730-a768-4286-a972-43e9b83601cd"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office 365 Client Admin"
                AppId   = "3cf6df92-2745-4f6f-bbcf-19b59bcdb62a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Workflow"
                AppId   = "3d869111-12c7-4671-a291-c433e04cb9dd"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Teams Approvals"
                AppId   = "3e050dd7-7815-46a0-8263-b73168a42c10"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "PowerApps - apps.powerapps.com"
                AppId   = "3e62f81e-590b-425b-9531-cad6683656cf"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office 365 Import Service"
                AppId   = "3eb95cef-b10f-46fe-94e0-969a3d4c9292"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Application Change Service"
                AppId   = "3edcf11f-df80-41b2-a5e4-7e213cca30d1"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Internal_AccessScope"
                AppId   = "3f9bd1ee-5a72-4ad3-b67d-cb016f935bcf"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "3f9ee9d2-f922-4e5c-be3c-0347811a6743"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Todo web app"
                AppId   = "3ff8e6ba-7dc3-4e9e-ba40-ee12b60d6d48"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Information Protection API"
                AppId   = "40775b29-2688-46b6-a3b5-b256bd04df9f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Search Management"
                AppId   = "408992c7-2af6-4ff1-92e3-65b73d2b5092"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Web Tips"
                AppId   = "4342d7f7-63bc-45f7-bb14-877c5760282c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "O365 Suite UX"
                AppId   = "4345a7b9-9a63-4910-a426-35363201d503"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Power BI Premium"
                AppId   = "435b84e1-75f7-495c-9e18-b6ddbb633ab8"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Trivia"
                AppId   = "43bc466a-7678-476f-b904-2d933c5bbfc3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "DWEngineV2"
                AppId   = "441509e5-a165-4363-8ee7-bcf0b7d26739"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Dynamics 365 Apps Integration"
                AppId   = "44a02aaa-7145-4925-9dcd-79e6e1b94eff"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ConfigMgrSvc_25eff651-214b-4ad0-a4f6-f33a030a30e2"
                AppId   = "453a007a-107d-445c-83e1-4310ba69cae4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Call Recorder"
                AppId   = "4580fd1d-e5a3-4f56-9ad1-aab0e3bf8f76"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Windows Store for Business"
                AppId   = "45a330b1-b1ec-4cc1-9161-9f03992aa49f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azns AAD Webhook"
                AppId   = "461e8683-5575-4561-ac7f-899cc907d62a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "462fe621-8acb-4f1d-99a0-ec287c3c0deb"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Invitation Acceptance Portal"
                AppId   = "4660504c-45b3-4674-a709-71951a6b0763"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "46cdaa3f-9339-4b60-bf04-4dd98cd903d7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "PushChannel"
                AppId   = "4747d38e-36c5-4bc3-979b-b0ef74df54d1"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "PowerApps Service"
                AppId   = "475226c6-020e-4fb2-8a90-7a972cbfc1d4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure AD Application Proxy"
                AppId   = "47ee738b-3f1a-4fc7-ab11-37e4822b007e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Authenticator App resource:ff9ebd75-fe62-434a-a6ce-b3f0a8592eaf"
                AppId   = "4813382a-8fa7-425e-ab75-3b753aab3abb"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Salesforce"
                AppId   = "481732f5-fe5b-48c0-8445-d238ab230658"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "GatewayRP"
                AppId   = "486c78bf-a0f7-45f1-92fd-37215929e116"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Skype and Teams Tenant Admin API"
                AppId   = "48ac35b8-9aa8-4d74-927d-1f4a14a0b239"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Connectors"
                AppId   = "48af08dc-f6d2-435f-b2a7-069abd99c086"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "4906f920-9f94-4f14-98aa-8456dd5f78a8"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "PowerApps CDS Service - GCC"
                AppId   = "49186f10-5547-424c-88d0-a24d09fb4f79"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "4937fd3f-7a43-4926-a63b-dbaae46eacd3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "49471a10-fdbc-4ffb-b0b8-944f3df985d9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft.EventGrid"
                AppId   = "4962773b-9cdb-44cf-a8bf-237846a00ab7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Exchange Admin Center"
                AppId   = "497effe9-df71-4043-a8bb-14cf78c4b63b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Partner dashboard (missing on letter?)"
                AppId   = "4990cffe-04e8-4e8b-808a-1175604b879"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Partner"
                AppId   = "4990cffe-04e8-4e8b-808a-1175604b879f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Visual Studio Online"
                AppId   = "499b84ac-1321-427f-aa17-267ca6975798"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "cisepvmstors01001"
                AppId   = "49f09dea-b47d-4823-a4e7-ae95cdf67360"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Analysis Services"
                AppId   = "4ac7d521-0382-477b-b0f8-7e1d95f85ca2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Office Dotcom"
                AppId   = "4b233688-031c-404b-9a80-a4f3f2351f90"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Audit GraphAPI Application"
                AppId   = "4bfd5d66-9285-44a1-bb14-14953e8cdf5e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Targeted Messaging Service"
                AppId   = "4c4f550b-42b2-4a16-93f9-fdb9e01bb6ed"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "IPSubstrate"
                AppId   = "4c8f074c-e32b-4ba7-b072-0f39d71daf51"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "powerbi.microsoft.com"
                AppId   = "4cfdc3df-6fbd-44aa-bc6c-bc861a88e008"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Reserved Instance Application"
                AppId   = "4d0ad6c7-f6c3-46d8-ab0d-1406d5e6c86b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OfficeShredderWacClient"
                AppId   = "4d5c2d63-cf83-4365-853c-925fd1a64357"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "4e291c71-d680-4d0e-9640-0a3358e31177"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "4e31c259-4969-4c6a-9e94-64c5c9536c29"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SharePoint Home Notifier"
                AppId   = "4e445925-163e-42ca-b801-9073bfa46d17"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SalesForce Files Connect"
                AppId   = "4f8f6951-9505-4674-a4f0-a0e9366e354e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Flow Service GCC"
                AppId   = "50351660-e7b1-4621-8bc8-8503296a5535"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office 365 Reports"
                AppId   = "507bc9da-c4e2-40cb-96a7-ac90df92685c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Resource Graph"
                AppId   = "509e4652-da8d-478d-a730-e9d4a1996ca4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ConfigMgrSvc_1e5b1f04-4408-4453-b317-c2c58068fe82"
                AppId   = "50a7343e-a510-4f2d-bee1-0bf51ef9f3b7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft 365 Ticketing"
                AppId   = "510a5356-1745-4855-93a5-113ea589fb26"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams Mailhook"
                AppId   = "51133ff5-8e0d-4078-bcca-84fb7f905b64"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Region Move Orchestrator Application"
                AppId   = "51df634f-ddb4-4901-8a2d-52f6393a796b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "FindTime"
                AppId   = "5288fe6c-90b6-422a-a540-5696f6eb8393"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "52d3717e-a642-4d47-b703-2ffeccd156f2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "538c319b-f605-4135-8fda-bfe8c8e3640a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Common Data Service - Azure Data Lake Storage"
                AppId   = "546068c3-99b1-4890-8e93-c8aeadcfe56a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "5484fb6b-185a-45b9-9921-54ed1f4a0ec2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office Enterprise Protection Service"
                AppId   = "55441455-2f54-42b5-bc99-93e21cd4ae28"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "5572c4c0-d078-44ce-b81c-6cbf8d3ed39e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "55747057-9b5d-4bd4-b387-abf52a8bd489"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Configuration Manager Microservice"
                AppId   = "557c67cf-c916-4293-8373-d584996f60ae"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "55ec4b79-18bf-4457-a8e1-66cb9a3ffb5a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "STARS sFiles Integration"
                AppId   = "56208484-5bc9-4e77-8f1e-b9e3a289bbe1"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Diagnostic Services Trusted Storage Access"
                AppId   = "562db366-1b96-45d2-aa4a-f2148cef2240"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Graph Connector Service"
                AppId   = "56c1da01-2129-48f7-9355-af6d59d42766"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Branch Connect Web Service"
                AppId   = "57084ef3-d413-4087-a28f-f6f3b1ad7786"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "5754bb84-f388-44f0-b3f7-9233a05bbb34"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "579129a0-5696-4984-959f-068f95d8f8fd"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Compute"
                AppId   = "579d9c9d-4c83-4efc-8124-7eba65ed3356"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ciswpnsgflowstors01001"
                AppId   = "57a444cd-0411-45de-a07e-16a9b0dd3b71"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Cosmos DB Virtual Network To Network Resource Provider"
                AppId   = "57c0fc58-a83a-41d0-8ae9-08952659bdfd"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SharePoint Online Client"
                AppId   = "57fb890c-0dab-4253-a5e0-7188c88b2bb4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "57fcbcfa-7cee-4eb1-8b25-12d2030b4ee0"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "5801fbf1-8df2-4c3c-a65c-e230edf60ca2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Key Vault Managed HSM"
                AppId   = "589d5083-6f11-4d30-a62a-a4b316a14abf"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure AD Identity Governance Insights"
                AppId   = "58c746b0-a0b0-4647-a8f6-12dde5981638"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "M365 Pillar Diagnostics Service"
                AppId   = "58ea322b-940c-4d98-affb-345ec4cccb92"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft.SupportTicketSubmission"
                AppId   = "595d87a1-277b-4c0a-aa7f-44f8a068eafc"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Exchange Filtering Service"
                AppId   = "597cf567-d52d-4c00-aca6-b2126beb3fa1"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Windows Virtual Desktop AME"
                AppId   = "5a0aa725-4958-4b0c-80a9-34562e23f3b7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Information Protection"
                AppId   = "5b20c633-9a48-4a5f-95f6-dae91879051f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "cisedsqlsrv03001"
                AppId   = "5d1b1a99-982a-4db4-8f46-6c2f13790179"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "CABProvisioning"
                AppId   = "5da7367f-09c8-493e-8fd4-638089cddec3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams Web Client"
                AppId   = "5e3ce6c0-2b1f-4285-8d4b-75ee78787346"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Regional Service Manager"
                AppId   = "5e5e43d4-54da-4211-86a4-c6e7f3715801"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Cisco Webex Meetings"
                AppId   = "5ea3594e-dea9-4443-bc0b-afe0c1af187f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "not found "
                AppId   = "5f42f75b-0208-44bc-992b-0ca5e235dee6"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office Change Management"
                AppId   = "601d4e27-7bb3-4dee-8199-90d47d527e1c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Verifiable Credentials Issuer Service"
                AppId   = "603b8c59-ba28-40ff-83d1-408eee9a93e5"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "StreamToSubstrateRepl"
                AppId   = "607e1f95-b519-4bac-8a15-6196f40e8977"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Machine Learning Singularity"
                AppId   = "607ece82-f922-494f-88b8-30effaf12214"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Maps Resource Provider"
                AppId   = "608f6f31-fed0-4f7b-809f-90f6c9b3de78"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Managed Disks Resource Provider"
                AppId   = "60e6cd67-9c8c-4951-9b3c-23c25a2169af"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Event Hub MSI App"
                AppId   = "6201d19e-14fb-4472-a2d6-5634a5c97568"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Flow Portal"
                AppId   = "6204c1d1-4712-4c46-a7d9-3ed63d992682"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Teams ACL management service"
                AppId   = "6208afad-753e-4995-bbe1-1dfd204b3030"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "6253bca8-faf2-4587-8f2f-b056d80998a7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams AadSync"
                AppId   = "62b732f7-fc71-40bc-b27d-35efcb0509de"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ACR-Tasks-Network"
                AppId   = "62c559cd-db0c-4da0-bab2-972528c65d42"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office Scripts Service"
                AppId   = "62fd1447-0ef3-4ab7-a956-7dd05232ecc1"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ciswpnsgflowstors04001"
                AppId   = "6300c7e6-c595-43f8-ad09-67c95bd8e565"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "sparkfbo.sparknit.com"
                AppId   = "63014814-ea56-43e2-8495-3198a1424baa"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Dynamics CRM Online Administration"
                AppId   = "637fcc9f-4a9b-4aaa-8713-a2a3cfda1505"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Intune SCCM Connector"
                AppId   = "63e61dc2-f593-4a6f-92b9-92e4d2c03d4f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "CMAT"
                AppId   = "64a7b174-5779-4506-b54c-fbb0d80f1c9b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams Bots"
                AppId   = "64f79cb9-9c82-4199-b85b-77e35b7dcbcb"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "luis.ai.live"
                AppId   = "65920ba3-ab61-4a9b-9b10-505e5ce61b58"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Approval Management"
                AppId   = "65d91a3d-ab74-42e6-8a2f-0add61688c74"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Fluid Framework Preview"
                AppId   = "660d4be7-2665-497f-9611-a42c2668dbce"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "IAMTenantCrawler"
                AppId   = "66244124-575c-4284-92bc-fdd00e669cea"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "GRAPH-O365-Admin-Delegated"
                AppId   = "6639d5da-46c0-4457-91ed-3c86dfa8f6c3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MTS"
                AppId   = "6682cfa5-2710-44c9-adb8-5ac9d76e394a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office 365 Search Service"
                AppId   = "66a88757-258c-4c72-893c-3e8bed4d6899"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Office Web Apps Service"
                AppId   = "67e3df25-268a-4324-a550-0de1c7f97287"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "6807062e-abc9-480a-ae93-9f7deee6b470"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "cisepvmstors01005"
                AppId   = "684c6a67-b9c5-4df6-bd94-7e9ec6ee588b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Application Insights Configuration Service"
                AppId   = "6a0a243c-0886-468a-a4c2-eff52c7445da"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Container Registry"
                AppId   = "6a0ec4d3-30cb-4a83-91c0-ae56bc0e3d26"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OneDrive"
                AppId   = "6a9b9266-8161-4a7b-913a-a9eda19da220"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Workflow"
                AppId   = "6b2225e3-07de-4671-a2df-5e50cc296206"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "M365 Admin Services"
                AppId   = "6b91db1b-f05b-405a-a0b2-e3f60b28d645"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Container Instance Service"
                AppId   = "6bb8e274-af5d-4df2-98a3-4fd78b4cafd9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Geneva Alert RP"
                AppId   = "6bccf540-eb86-4037-af03-7fa058c2db75"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Intune MAM client resource"
                AppId   = "6c7e8096-f593-4d72-807f-a5f86dcc9c77"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Networking-MNC"
                AppId   = "6d057c82-a784-47ae-8d12-ca7b38cf06b4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Permission Service O365"
                AppId   = "6d32b7f8-782e-43e0-ac47-aaad9f4eb839"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Exchange Office Graph Client for AAD - Interactive"
                AppId   = "6da466b6-1d13-4a2c-97bd-51a99e8d4d74"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Kubernetes Service AAD Server"
                AppId   = "6dae42f8-4368-4678-94ff-3960e28e3630"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OCaaS Experience Management Service"
                AppId   = "6e99704e-62d5-40f6-b2fe-90aafbe3a710"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AD Hybrid Health"
                AppId   = "6ea8091b-151d-447a-9013-6845b83ba57b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AAD Connect v2"
                AppId   = "6eb59a73-39b2-4c23-a70f-e2e3ce8965b1"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "WindowsUpdate-Service"
                AppId   = "6f0478d5-61a3-4897-a2f2-de09a5a90c7f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Dual-write"
                AppId   = "6f7d0213-62b1-43a8-b7f4-ff2bb8b7b452"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft.AAD.BrokerPlugin resource:https://cs.dds.microsoft.com"
                AppId   = "6f7e0f60-9401-4f5b-98e2-cf15bd5fd5e3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Discovery Service"
                AppId   = "6f82282e-0070-4e78-bc23-e6320c5fa7de"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Dynamic Alerts"
                AppId   = "707be275-6b9d-4ee7-88f9-c0c2bd646e0f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Device Management Enrollment"
                AppId   = "709110f7-976e-4284-8851-b537e9bcb187"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Customer Engagement Portal"
                AppId   = "71234da4-b92f-429d-b8ec-6e62652e50d7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "7144e8cb-9837-45e0-83e4-bb3f86570f5f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Marketplace"
                AppId   = "716fcfea-a9ae-4782-8ee8-87b9392ffa18"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Weve"
                AppId   = "71a7c376-13e6-4100-968e-92ce98c5d3d2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "72782ba9-4490-4f03-8d82-562370ea3566"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AzureContainerService"
                AppId   = "7319c514-987d-4e9b-ac3d-d38c4f427f4c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Marketplace Container Management API"
                AppId   = "737d58c1-397a-46e7-9d12-7d8c830883c2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "74374a04-182f-444f-9dad-3978d27aad44"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure portal UI ADIbizaUX"
                AppId   = "74658136-14ec-4630-ad9b-26e160ff0fc6"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Azure Graph Client Library 2.1.9 Internal"
                AppId   = "7492bca1-9461-4d94-8eb8-c17896c61205"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Intune Web Company Portal"
                AppId   = "74bcdadc-2fdc-4bb3-8459-76d06952a0e9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Service Fabric Resource Provider"
                AppId   = "74cb6831-0dbb-4be1-8206-fd4df301cdc2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "751ff9b5-edde-4dc1-8093-adf647495745"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Skype for Business"
                AppId   = "7557eb47-c689-4224-abcf-aef9bd7573df"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "75d85e4b-6740-4b6b-8a54-359414c9719b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams Retail Service"
                AppId   = "75efb5bc-18a1-4e7b-8a66-2ad2503d79c6"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Exchange Office Graph Client for AAD (Noninteractive)"
                AppId   = "765fe668-04e7-42ba-aec0-2c96f1d8b652"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure SQL Managed Instance to Microsoft.Network"
                AppId   = "76c7f279-7959-468f-8943-3954880e0d8c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure SQL Virtual Network to Network Resource Provider"
                AppId   = "76cd24bf-a9fc-4344-b1dc-908275de6d6d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Skype For Business Powershell Client Application"
                AppId   = "7716031e-6f8b-45a4-b82b-922b1af0fbb4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure HDInsight Cluster API"
                AppId   = "7865c1d2-f040-46cc-875f-831a1ef6a28a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Yggdrasil"
                AppId   = "78e7bc61-0fab-4d35-8387-09a8d2f5a59d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Intune Advanced Threat Protection Integration"
                AppId   = "794ded15-70c6-4bcd-a0bb-9b7ad530a01a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Windows Azure Service Management API"
                AppId   = "797f4846-ba00-4fd7-ba43-dac1f8f63013"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Workflow"
                AppId   = "7a4894f4-998c-402a-b4a6-c242e43ffcab"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Cisco Webex Meetings"
                AppId   = "7a91e319-a65d-4ceb-909b-12203561dbf5"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "App Service"
                AppId   = "7ab7862c-4c57-491e-8a45-d52a7e023983"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Directory and Policy Cache"
                AppId   = "7b58f833-4438-494c-a724-234928795a67"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Advanced Threat Protection"
                AppId   = "7b7531ad-5926-4f2d-8a1d-38495ad33e17"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Network Watcher"
                AppId   = "7c33bfcb-8d33-48d6-8e60-dc6404003489"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Data Classification Service"
                AppId   = "7c99d979-3b9c-4342-97dd-3239678fb300"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Logic Apps"
                AppId   = "7cd684f4-8a78-49b0-91ec-6a35d38739ba"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Cognitive Services"
                AppId   = "7d312290-28c8-473c-a0ed-8e53749b6d6d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Power Platform Data Analytics"
                AppId   = "7dcff627-a295-4553-9229-b1f3513f82a8"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Flow Service"
                AppId   = "7df0a125-d3be-4c96-aa54-591f83ff541c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Federated Profile Service"
                AppId   = "7e468355-e4db-46a9-8289-8d414c89c43c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Intune DiagnosticService"
                AppId   = "7f0d9978-eb2a-4974-88bd-f22a3006fe17"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Export to data lake"
                AppId   = "7f15f9d9-cad0-44f1-bbba-d36650e07765"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "https://docs.microsoft.com/en-us/rest/api/authorization/globaladministrator/elevateaccess#code-try-0"
                AppId   = "7f59a773-2eaf-429c-a059-50fc5bb28b44"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "7fba38f4-ec1f-458d-906c-f4e3c4f41335"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft.EventHubs"
                AppId   = "80369ed6-5f11-4dd9-bef3-692475845e77"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "803793a3-997d-49e2-94cf-cceadf4272d9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Media Service"
                AppId   = "803ee9ca-3f7f-4824-bd6e-0b99d720c35c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft.ServiceBus"
                AppId   = "80a10ef9-8168-493d-abf9-3297c4ef6e3c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft 365 Security and Compliance Center"
                AppId   = "80ccca67-54bd-44ab-8625-4b79c4dc7775"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Billing RP"
                AppId   = "80dbdb39-4f33-4799-8b6f-711b5e3e61b6"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure AD Identity Governance - Entitlement Management"
                AppId   = "810dcf14-1858-4bf2-8134-4c369fa3235b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Cortana Runtime Service"
                AppId   = "81473081-50b9-469a-b9d8-303109583ecb"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Polly"
                AppId   = "817c2506-de4a-4795-971e-371ea75a03ed"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams Wiki Images Migration"
                AppId   = "823dfde0-1b9a-415a-a35a-1ad34e16dd44"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ViewPoint"
                AppId   = "8338dec2-e1b3-48f7-8438-20c30a534458"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Windows Azure Service Management API"
                AppId   = "84070985-06ea-473d-82fe-eb82b4011c9d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Stream Mobile Native"
                AppId   = "844cca35-0656-46ce-b636-13f48b0eecbd"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Sessionize.com"
                AppId   = "84bd1e48-f14c-488a-aff1-a1dd54bc7b94"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Fairfax - Power BI Gateway"
                AppId   = "85497d63-b21f-44e6-b91a-5e337b063302"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure API Management"
                AppId   = "8602e328-9b72-4f2d-a4ae-1387d013a2b3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "86089b29-f61e-4590-ac28-3c54be965241"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "86579c01-dcbb-4450-9f18-b30109ca10ae"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "8703e6b0-f9bd-457b-8506-cba161278bc4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Information Protection Sync Service"
                AppId   = "870c4f2e-85b6-4d43-bdda-6ed9a579b725"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "871c010f-5e61-4fb1-83ac-98610a7e9110"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Visual Studio"
                AppId   = "872cd9fa-d31f-45e0-9eab-6e460a02d1f1"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams - Device Admin Agent"
                AppId   = "87749df4-7ccf-48f8-aa87-704bad0e0e16"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SharePoint Notification Service"
                AppId   = "88884730-8181-4d82-9ce2-7d5a7cc7b81e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Parature Dynamics CRM"
                AppId   = "8909aac3-be91-470c-8a0b-ff09d669af91"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Box"
                AppId   = "89220c47-d3bd-4942-a242-bda247a5bc5b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office365 Shell WCSS-Client"
                AppId   = "89bee1f7-5e6e-4d8a-9f3d-ecd601259da7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Iot Hub"
                AppId   = "89d10474-74af-4874-99a7-c23c2f643083"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Teams Application Gateway"
                AppId   = "8a753eec-59bc-4c6a-be91-6bf7bfe0bcdf"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Device Management EMM API"
                AppId   = "8ae6a0b1-a07f-4ec9-927a-afb8d39da81c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "O365 Secure Score"
                AppId   = "8b3391f4-af01-4ee8-b4ea-9871b2499735"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "PowerAI"
                AppId   = "8b62382d-110e-4db8-83a6-c7e8ee84296a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "cisepvmstors01004"
                AppId   = "8bb76182-3661-4de6-85ab-b67ed45acee8"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "DeploymentScheduler"
                AppId   = "8bbf8725-b3ca-4468-a217-7c8da873186e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ResourceHealthRP"
                AppId   = "8bdebf23-c0fe-4187-a378-717ad86f6a53"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "M365 Pillar Diagnostics Service API"
                AppId   = "8bea2130-23a1-4c09-acfb-637a9fb7c157"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "My Profile"
                AppId   = "8c59ead7-d703-4a27-9e55-c96a0054c8d2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Power Query Online GCC-L5"
                AppId   = "8c8fbf21-0ef3-4f60-81cf-0df811ff5d16"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Policy Insights Provider Data Plane"
                AppId   = "8cae6e77-e04e-42ce-b5cb-50d82bce26b1"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Office Licensing Service"
                AppId   = "8d3a7d3c-c034-4f19-a2ef-8412952a9671"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "8dc8a478-cba8-4f07-b3c2-0f4545efff62"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Azure Signup Portal"
                AppId   = "8e0e8db5-b713-4e91-98e6-470fed0aa4c2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams Targeting Application"
                AppId   = "8e14e873-35ba-4720-b787-0bed94370b17"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams-T4L"
                AppId   = "8ec6bc83-69c8-4392-8f08-b3c986009232"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Windows Azure Security Resource Provider"
                AppId   = "8edd93e1-2103-40b4-bd70-6e34e586362d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Threat Protection"
                AppId   = "8ee8fdad-f234-4243-8f3b-15c294843740"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "EDU Assignments"
                AppId   = "8f348934-64be-4bb2-bc16-c54c96789f43"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Device Directory Service"
                AppId   = "8f41dc7c-542c-4bdd-8eb3-e60543f607ca"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "8f5f02ce-ab04-4a37-9f16-8ee9daadb9d5"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft.SMIT"
                AppId   = "8fca0a66-c008-4564-a876-ab3ae0fd5cff"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Sway"
                AppId   = "905fcf26-4eb7-48a0-9ff0-8dcc7194b5ba"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AADRM"
                AppId   = "90f610bf-206d-4950-b61d-37fa6fd1b224"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Dynamics Lifecycle services"
                AppId   = "913c6de4-2a4a-4a61-a9ce-945d2b2ce2e0"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office Delve"
                AppId   = "915d9449-5b40-4b44-97ee-5080a1f3e759"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "asmcontainerimagescanner"
                AppId   = "918d0db8-4a38-4938-93c1-9313bdfe0272"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure HDInsight Service"
                AppId   = "9191c4da-09fe-49d9-a5f1-d41cbe92ad95"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft.Relay"
                AppId   = "91bb937c-29c2-4275-982f-9465f0caf03d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Groupies Web Service"
                AppId   = "925eb0d0-da50-4604-a19f-bd8de9147958"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ComplianceWorkbenchApp"
                AppId   = "92876b03-76a3-4da8-ad6a-0511ffdf8647"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "9322f837-c8f8-4796-9aef-1677748fe553"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft password reset service"
                AppId   = "93625bc8-bfe2-437a-97e0-3d0060024faa"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Power Query Online GCC-L2"
                AppId   = "939fe80f-2eef-464f-b0cf-705d254a2cf2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Power Platform Global Discovery Service"
                AppId   = "93bd1aa4-c66b-4587-838c-ffc3174b5f13"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Meru19 First Party App"
                AppId   = "93efed00-6552-4119-833a-422b297199f9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "cisepvmstors04002"
                AppId   = "9415355d-50f6-4cef-a57e-ae13918a6a57"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Media Analysis and Transformation Service"
                AppId   = "944f0bd1-117b-4b1c-af26-804ed95e767e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office 365"
                AppId   = "94c63fef-13a3-47bc-8074-75af8c65887a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "pdf.apps-encodian.com"
                AppId   = "951d1513-0c5d-45c6-921b-73b56f46b96a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "952ef45a-ba2c-4a28-a6f9-b4cbe40e0cec"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MS Dart Team - Greg Pallotta - Delete after Feb 11 2021"
                AppId   = "9562394a-5b86-45af-9b0a-bfa944380b2b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft_Azure_Support"
                AppId   = "959678cf-d004-4c22-82a6-d2ce549a58b8"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Whiteboard Services"
                AppId   = "95de633a-083e-42f5-b444-a4295d8e9314"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MicrosoftAzureRedisCache"
                AppId   = "96231a05-34ce-4eb4-aa6a-70759cbb5e83"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ciswpvmstors01003"
                AppId   = "96470954-b30d-431d-a83e-d111ca663d72"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "965e190f-6cfb-47c9-8d52-b3c8aeb54f3c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams AuditService"
                AppId   = "978877ea-b2d6-458b-80c7-05df932f3723"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Multi-Factor Auth Client"
                AppId   = "981f26a1-7f43-403b-a875-f8b09b8cd720"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "O365 Demeter"
                AppId   = "982bda36-4632-4165-a46a-9863b1bbcf7d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Security Insights"
                AppId   = "98785600-1bb7-4fb9-b9fa-19afe2c8a360"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OfficeFeedProcessors"
                AppId   = "98c8388a-4e86-424f-a176-d1288462816f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Substrate Management"
                AppId   = "98db8bd6-0cc0-4e67-9de5-f187f1cd1b41"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "cisetvmstors03002"
                AppId   = "991ec66f-059d-406b-aa90-c67039095323"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Power Platform Dataflows Common Data Service Client"
                AppId   = "99335b6b-7d9d-4216-8dee-883b26e0ccf7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Dynamics 365 Business Central"
                AppId   = "996def3d-b36c-4153-8607-a6fd3c01b89f"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "99adaf47-f643-4c0e-8117-15335c87ebda"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "9afacd42-6903-4741-9534-34ff39b7df05"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft.DynamicsMarketing"
                AppId   = "9b06ebd4-9068-486b-bdd2-dac26b8a5a7a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Intune Company Portal"
                AppId   = "9ba1a5c7-f17a-4de9-a1f1-6178c8d51223"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "KaizalaActionsPlatform"
                AppId   = "9bb724a5-4639-438c-969b-e184b2b1e264"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SPO Management Shell"
                AppId   = "9bc3ab49-b65d-410a-85ad-de819febfddc"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Backup NRP Application"
                AppId   = "9bdab391-7bbe-42e8-8132-e4491dc29cc0"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Intune Service Discovery"
                AppId   = "9cb77803-d937-493e-9a3b-4b49de3f5a74"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "DirectoryLookupService"
                AppId   = "9cd0f7df-8b1a-4e54-8c0c-0ef3a51116f6"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "O365SBRM Service"
                AppId   = "9d06afd9-66c9-49a6-b385-ea7509332b0b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SCCM-UR-Web-App"
                AppId   = "9dc49822-abb8-41fc-8e13-3a619cb82cc6"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Conferencing Virtual Assistant"
                AppId   = "9e133cac-5238-4d1e-aaa0-d8ff4ca23f4e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OfficeServicesManager"
                AppId   = "9e4a5442-a5c9-4f6f-b03f-5b9fcaaf24b1"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Bing"
                AppId   = "9ea1ad79-fdb6-4f9a-8bc3-2b70f96e34c7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Services"
                AppId   = "9ed4cd8c-9a98-405f-966b-38ab1b0c24a3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AzureLockbox"
                AppId   = "a0551534-cfc9-4e1f-9a7a-65093b32bb38"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Enterprise File Sync Service"
                AppId   = "a07d3a2b-436e-40ce-9eba-4e5a5664f14c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AzureDnsFrontendApp"
                AppId   = "a0be0c72-870e-46f0-9c49-c98333a996f7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "EXO Remote PowerShell"
                AppId   = "a0c73c16-a7e3-4564-9a95-2bdf47383716"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Ideas Portal (PROD)"
                AppId   = "a0d29d68-211a-4f6b-a925-81d3f8d77896"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft AppSource"
                AppId   = "a0e1e353-1a3e-42cf-a8ea-3a9746eec58c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "ASA Curation Web Tool"
                AppId   = "a15bc1de-f777-408f-9d2b-a27ed19c72ba"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams AuthSvc"
                AppId   = "a164aee5-7d0a-46bb-9404-37421d58bdf7"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "PowerApps Service - GCC"
                AppId   = "a16afde6-431b-4662-b2b2-e5ee8c05b1f2"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "a1ef3a23-74fc-4c91-909f-691cc47a1c8e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Cosmos DB"
                AppId   = "a232010e-820c-4083-83bb-3ace5fc29d0b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft.MileIQ"
                AppId   = "a25dbca8-4e60-48e5-80a2-0664fdb5c9b6"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "a3037261-2c94-4a2e-b53f-090f6cdd712a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Compute Usage Provider"
                AppId   = "a303894e-f1d8-4a37-bf10-67aa654a0596"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "WindowsDefenderATP Portal"
                AppId   = "a3b79187-70b2-4139-83f9-6016c58cd27b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Azure AD Identity Protection"
                AppId   = "a3dfc3c6-2c7d-4f42-aeec-b2877f9bce97"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "a40d7d7d-59aa-447e-a655-679a4107e548"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams User Profile Search Service"
                AppId   = "a47591ab-e23e-4ffa-9e1b-809b9067e726"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "PowerApps Fairfax"
                AppId   = "a4b559be-784e-49ec-9b63-7208442255e1"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Data Migration Service"
                AppId   = "a4bad4aa-bf02-4631-9f78-a64ffdba8150"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Container Registry"
                AppId   = "a4c95b9e-3994-40cc-8953-5dc66d48348d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "IAM Supportability"
                AppId   = "a57aca87-cbc0-4f3c-8b9e-dc095fdc8978"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "mars-dci"
                AppId   = "a5c9c029-4ec5-46ab-8e2b-9b2e5a36c60b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "a672d62c-fc7b-4e81-a576-e60dc46e951d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Storage Resource Provider"
                AppId   = "a6aa9161-5291-40bb-8c5c-923b567bee3b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "services.spform.com"
                AppId   = "a6c0f54b-685f-4f71-ae97-af638a20ccb3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "a7245b5e-a1ef-4587-8a58-d48fd485d3d4"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "PowerApps - apps.gov.powerapps.us"
                AppId   = "a81833f1-fd18-490b-8598-60cd7b6b0382"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams VSTS"
                AppId   = "a855a166-fd92-4c76-b60d-a791e0762432"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Compute Artifacts Publishing Service"
                AppId   = "a8b6bf88-1d1a-4626-b040-9a729ea93c65"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "make.powerapps.com"
                AppId   = "a8f7a65c-f5ba-4859-b2d6-df772c264e9d"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure SQL Database and Data Warehouse"
                AppId   = "a94f9c62-97fe-4d19-b06d-472bed8d2bcf"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "a9a6ad6e-48fd-46e1-a89c-aec4029d7bdd"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams Shifts"
                AppId   = "aa580612-c342-4ace-9055-8edee43ccb89"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Office 365 Configure"
                AppId   = "aa9ecb1e-fd53-4aaa-a8fe-7a54de2c1334"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "aad70c20-ca13-4c6c-9e38-19f8b19217d0"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft.OfficeModernCalendar"
                AppId   = "ab27a73e-a3ba-4e43-8360-8bcc717114d8"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams Graph Service"
                AppId   = "ab3be6b7-f5df-413d-ac2d-abf1e3fd9c0b"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OneDrive Sync Engine"
                AppId   = "ab9b8c07-8f02-4f72-87fa-80105867a763"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "aba285d5-d9f3-427b-a994-e9deb4567639"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Azure Websites"
                AppId   = "abfa0a7c-a6b6-4736-8310-5855508787cd"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "ad0dc9d7-dc7d-4f20-9b2e-2e418e8272d9"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "AI Builder Authorization Service"
                AppId   = "ad40333e-9910-4b61-b281-e3aeeb8c3ef3"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Cisco Webex Meetings"
                AppId   = "ae264eaf-ee16-4935-96b8-13b522ee2e36"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "M365 License Manager"
                AppId   = "aeb86249-8ea3-49e2-900b-54cc8e308f85"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "aebc6443-996d-45c2-90f0-388ff96faa56"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OneDrive iOS App"
                AppId   = "af124e86-4e96-495a-b70a-90f90ab96707"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MicrosoftAzureActiveDirectoryIntegratedApp"
                AppId   = "af47b99c-8954-4b45-ab68-8121157418ef"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "TIE-B2B-Sync"
                AppId   = "af54b22e-5d98-4c6c-9d7f-e00ab6446519"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "aff03472-97bb-4cbd-8b7b-9a5f7615c13e"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "MOD Demo Platform UnifiedApiConsumer"
                AppId   = "aff75787-e598-43f9-a0ea-7a0ca00ababc"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "affadfb6-f17b-428f-97f9-9aae3b6175bc"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Teams Chat Aggregator"
                AppId   = "b1379a75-ce5e-4fa3-80c6-89bb39bf646c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Tableau Server"
                AppId   = "b138251a-837d-4d8f-b75d-e05cdc6e1b09"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "b162b15f-1f92-4791-8a12-9b1aab31987c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "b1b46579-7bb4-43bf-916f-de3024e56cfe"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "SalesInsightsWebApp"
                AppId   = "b20d0d3a-dc90-485b-ad11-6031e769e221"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "-"
                AppId   = "b23dd4db-9142-4734-867f-3577f640ad0c"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OneProfile Service"
                AppId   = "b2cc270f-563e-4d8a-af47-f00963a71dcd"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "OfficeDelve"
                AppId   = "b4114287-89e4-4209-bd99-b7d4919bcf64"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "CISAutomation_oXiORGPwMDWxQsDqh9J1WEV1o1xVXnQ62Ryh3hVDrr0="
                AppId   = "b43f2f83-4b8b-48cd-95f3-3e29a8fb1950"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Signup"
                AppId   = "b4bddae8-ab25-483e-8670-df09b9f1d0ea"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure DNS Managed Resolver"
                AppId   = "b4ca0290-4e73-4e31-ade0-c82ecfaabf6a"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "VolumeLicensingServiceCenter"
                AppId   = "b4ec8d8b-0275-4833-8568-690db4af4b45"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Azure Notification Service"
                AppId   = "b503eb83-1222-4dcc-b116-b98ed5216e05"
            }),
        $(New-Object -TypeName psobject -Property @{
                AppName = "Microsoft Exact Data Match Upload Agent"
                AppId   = "b51a99a9-ccaa-4687-aa2c-44d1558295f4"
            }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Teams & Teams And Channels Service"
            AppId          = "b55b276d-2b09-4ad2-8de5-f09cf24ffba9"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "b5ff67d6-3155-4fca-965a-59a3655c4476"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Spring Cloud DiagSettings App"
            AppId          = "b61cc489-e138-4a69-8bf3-c2c5855c8784"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "cisepautostors01001"
            AppId          = "b672922a-4271-43e6-8577-a54a3842e531"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "AzurePortal Console App"
            AppId          = "b677c290-cf4b-4a8e-a60e-91ba650a4abe"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft.MileIQ.RESTService"
            AppId          = "b692184e-b47f-4706-b352-84b288d2d9ee"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Invoicing"
            AppId          = "b6b84568-6c01-4981-a80f-09da9a20bbed"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Skype Business Voice Fraud Detection and Prevention"
            AppId          = "b73f62d0-210b-4396-a4c5-ea50c4fab79b"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "ConnectionsService"
            AppId          = "b7912db9-aa33-4820-9d4f-709830fdd78f"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Intune IW Service"
            AppId          = "b8066b99-6e67-41be-abfa-75db1a2c8809"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Hyper-V Recovery Manager"
            AppId          = "b8340c3b-9267-498f-b21a-15d5547fd85e"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Data Export Service for Microsoft Dynamics 365"
            AppId          = "b861dbcc-a7ef-4219-a005-0e4de4ea7dcf"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "M365CommunicationCompliance"
            AppId          = "b8d56525-1fd0-4121-a640-e0ede64f74b5"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Office Shredding Service"
            AppId          = "b97b6bd4-a49f-4a0c-af18-af507d1da76c"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Compute Recommendation Service"
            AppId          = "b9a92e36-2cf8-4f4e-bcb3-9d99e00e14ab"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Maps"
            AppId          = "ba1ea022-5807-41d5-bbeb-292c7e1cf5f6"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "OfficeGraph"
            AppId          = "ba23cd2a-306c-48f2-9d62-d3ecd372dfe4"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "cisnonprodaccess01"
            AppId          = "ba696491-7772-450c-9184-828ec7a40469"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "My Staff"
            AppId          = "ba9ff945-a723-4ab5-a977-bd8c9044fe61"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Dynamics 365 Portal"
            AppId          = "bab47555-038a-4434-a931-96cc6091cdd7"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "PnP.PowerShell"
            AppId          = "bb0c5778-9d5c-41ea-a4a8-8cd417b3ab71"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "bb0fc165-b959-4e50-a8fc-309c1193e396"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "CPIM Service"
            AppId          = "bb2a2e3a-c5e7-4f0a-88e0-8e01fd3fc1f4"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "bb3b57f4-9af8-43b8-ba5a-677214d679df"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "cisepvmstors04001"
            AppId          = "bb8eded3-c417-4673-991a-e68203b942cd"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft.Azure.ActiveDirectoryIUX"
            AppId          = "bb8f18b0-9c38-48c9-a847-e1ef3af0602d"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Outlook Online Add-in App"
            AppId          = "bc59ab01-8403-45c6-8796-ac3ef710b3e3"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "SQLVMResourceProviderAuth"
            AppId          = "bd93b475-f9e2-476e-963d-b2daf143ffb9"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "CIS-E-P-UAID-S01-001"
            AppId          = "be788382-740d-44c0-a721-ab8c84df8aa6"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "be79608a-6f59-4821-9911-610507dbe31b"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure AD Identity Governance"
            AppId          = "bf26f092-3426-4a99-abfb-97dad74e661a"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "AADPremiumService"
            AppId          = "bf4fa6bf-d24c-4d1c-8cfd-12063dd646b2"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Universal Store Entitlements Service"
            AppId          = "bf7b96b3-68e4-4fd9-b697-637f0f1e778c"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft.CustomProviders RP"
            AppId          = "bf8eb16c-7ba7-4b47-86be-ac5e4b2007a5"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Jarvis Transaction Service"
            AppId          = "bf9fc203-c1ff-4fd4-878b-323642e462ec"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "bfc44fc5-2fe3-4d02-98ec-1e5967475f68"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "c00e9d32-3c8d-4a7d-832b-029040e7db99"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Enterprise File Sync Admin Service"
            AppId          = "c03594ff-1168-40fb-aef7-eb9f1edf7278"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "M365DataAtRestEncryption"
            AppId          = "c066d759-24ae-40e7-a56f-027002b5d3e4"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Intune API"
            AppId          = "c161e42e-d4df-4a3d-9b42-e7a3c31f59d4"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Office Online Third Party Storage"
            AppId          = "c1f33bc0-bdb4-4248-ba9b-096807ddb43e"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "OCaaS Client Interaction Service"
            AppId          = "c2ada927-a9e2-4564-aae2-70775a2fa0af"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "MALogAnalyticsReader"
            AppId          = "c2b3bb2d-34bb-46d7-bc1f-94480c1345a0"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Advisor"
            AppId          = "c39c9bac-9d1f-4dfb-aa29-27f6365e5cb7"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Portal"
            AppId          = "c44b4083-3bb0-49c1-b47d-974e53cbdf3c"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure AD Identity Governance - Dynamics 365 Management"
            AppId          = "c495cfdc-814f-46a1-89f0-657921c9fbe0"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "c538f3e2-0bd2-467b-a9b4-9894989d4db0"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Office 365 Management APIs"
            AppId          = "c5393580-f805-4401-95e8-94b7a6ef2fc2"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "SharePoint Online Client Extensibility"
            AppId          = "c58637bb-e2e1-4312-8a00-04b5ffcd3403"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "aciapi"
            AppId          = "c5b17a4f-cc6f-4649-9480-684280a2af3a"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Office Store"
            AppId          = "c606301c-f764-4e6b-aa45-7caaaea93c9a"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Call Quality Dashboard"
            AppId          = "c61d67cf-295a-462c-972f-33af37008751"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "ISV Portal"
            AppId          = "c6871074-3ded-4935-a5dc-b8f8d91d7d06"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "c69735d3-13ee-48d9-902d-89528746185d"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "PowerAppsGov"
            AppId          = "c6d1e3ad-0185-40e0-a11b-0542b185d12c"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "AAD Request Verification Service - PROD"
            AppId          = "c728155f-7b2a-4502-a08b-b8af9b269319"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "AIRS"
            AppId          = "c7d28c4f-0d2c-49d6-a88d-a275cc5473c7"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Mixed Reality"
            AppId          = "c7ddd9b4-5172-4e28-bd29-1e0792947d18"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft To-Do"
            AppId          = "c830ddb0-63e6-4f22-bd71-2ad47198a23e"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Jira Server"
            AppId          = "c8e573e8-b3ff-4c89-afbd-d75731ba97fc"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "CIS-T-JENKINS-APPREG-01"
            AppId          = "c8f13b99-4d18-4e80-ac98-3e4e914958c7"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Common Data Service User Management"
            AppId          = "c92229fa-e4e7-47fc-81a8-01386459c021"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "MS Teams Griffin Assistant"
            AppId          = "c9224372-5534-42cb-a48b-8db4f4a3892e"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "PowerApps-Advisor"
            AppId          = "c9299480-c13a-49db-a7ae-cdfe54fe0313"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "MicrosoftEndpointDLP"
            AppId          = "c98e5057-edde-4666-b301-186a01b4dc58"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Forms"
            AppId          = "c9a559d2-7aab-4f13-a6ed-e7e9c52aec87"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Device Management Checkin"
            AppId          = "ca0a114d-6fbc-46b3-90fa-2ec954794ddb"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "FastTrack"
            AppId          = "ca4c34d6-ce70-4353-8568-ca59b64f2a63"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Log Analytics API"
            AppId          = "ca7f3f0b-7d91-482c-8e09-c5d840d0eac5"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "scundpaggregator.azurewebsites.net"
            AppId          = "ca85caad-68c1-4300-97df-1a9ce33d2d15"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Pass-through authentication"
            AppId          = "cb1056e2-e479-49de-ae31-7812af012ed8"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Power BI Premium"
            AppId          = "cb4dc29f-0bf4-402a-8b30-7511498ed654"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Windows AutoPilot Service API"
            AppId          = "cbfda01c-c883-45aa-aedc-e7a484615620"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Teams Services"
            AppId          = "cc15fd57-2c6c-4117-a88c-83b1d56b4bbe"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "cc66f1c6-492b-44d5-95f5-3ccf6fb81527"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Intune AAD BitLocker Recovery Key Integration"
            AppId          = "ccf4d8df-75ce-4107-8ea5-7afd618d4d8a"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Skype Teams Firehose"
            AppId          = "cdccd920-384b-4a25-897d-75161a4b74c1"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Azure Linux Virtual Machine Sign-In"
            AppId          = "ce6ff14a-7fdc-4685-bbe0-f6afdfcfa8e0"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "ce76e270-35f5-4bea-94ff-eab975103dc6"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Stream Portal"
            AppId          = "cf53fce8-def6-4aeb-8d30-b158e7b1cf83"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Teams Settings Store"
            AppId          = "cf6c77f8-914f-4078-baef-e39a5181158b"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "cf6d7e68-f018-4e0a-a7b3-126e053fb88d"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "cf90ab8f-8091-4c2d-b6a9-0b89a3312382"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Key Vault"
            AppId          = "cfa8b339-82a2-471a-a3c9-0fc0be7a4093"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Logic Apps - Azure AD (Fairfax)"
            AppId          = "cff1f8e9-bc9f-42b2-9b58-b4e67ea809f7"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Teams Task Service"
            AppId          = "d0597157-f0ae-4e23-b06c-9e65de434c4f"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "PowerApps"
            AppId          = "d1208c34-6081-4127-96fa-8c97ecb420c1"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "d176f6e7-38e5-40c9-8a78-3998aab820e7"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "cisepnsgflowstors04001"
            AppId          = "d1f47a01-07e5-43aa-8646-5cbb540c3b97"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Discovery Service"
            AppId          = "d29a4c00-4966-492a-84dd-47e779578fb7"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Operations Management Suite"
            AppId          = "d2a0a418-0aac-4541-82b2-b3142c89da77"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "ACR-Tasks-Prod"
            AppId          = "d2fa1650-4805-4a83-bcb9-cf41fe63539c"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Activity Feed Service"
            AppId          = "d32c68ad-72d2-4acb-a0c7-46bb2cf93873"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Support and Recovery Assistant (SARA)"
            AppId          = "d3590ed6-52b3-4102-aeff-aad2292ab01c"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "CIS-P-AZPIM-APPREG-01"
            AppId          = "d3ffafd1-42d8-437e-81c7-d4f68dc179c2"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "sharepointonline.echosign.com"
            AppId          = "d442aabe-e09f-428c-a57d-8e972996541b"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Intune Enrollment"
            AppId          = "d4ebce55-015a-49b5-a083-c84d1797ae8c"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "addins.sharepointsapiens.com"
            AppId          = "d4f43231-f66d-4f82-93b1-bd5a9d9945c1"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "AAD Terms Of Use"
            AppId          = "d52792f4-ba38-424d-8140-ada5b883f293"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "d5f90dc4-3855-4b1e-83cf-e04e2db710e9"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "d6c1243a-d9fa-4b5e-b14e-9d2bf0a3ae11"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Service Trust"
            AppId          = "d6fdaa33-e821-4211-83d0-cf74736489e1"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Office Licensing Service Agents"
            AppId          = "d7097cd1-c779-44d0-8c71-ab1f8386a97e"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "MCAPI Authorization Prod"
            AppId          = "d73f4b35-55c9-48c7-8b10-651f6f2acb2e"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "d7b530a4-7680-4c23-a8bf-c52c121d2e87"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Kaizala Sync Service"
            AppId          = "d82073ec-4d7c-4851-9c5d-5d97a911d71d"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Intune AndroidSync"
            AppId          = "d8877f27-09c0-43aa-8113-40151dae8b14"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "IDML Graph Resolver Service and CAD"
            AppId          = "d88a361a-d488-4271-a13f-a83df7dd99c2"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Request Approvals Read Platform"
            AppId          = "d8c767ef-3e9a-48c4-aef9-562696539b39"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Skype"
            AppId          = "d924a533-3729-4708-b3e8-1d2445af35e3"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Databricks Resource Provider"
            AppId          = "d9327919-6775-4843-9037-3fb0fb0473cb"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Office Licensing Service vNext"
            AppId          = "db55028d-e5ba-420f-816a-d18c861aefdf"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Skype for Business Voicemail"
            AppId          = "db7de2b5-2149-435e-8043-e080dd50afae"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Service Encryption"
            AppId          = "dbc36ae1-c097-4df9-8d94-343c3d091a76"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Graph"
            AppId          = "dbcbd02a-d7c4-42fb-8c27-b07e5118b848"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "dc17f63f-f1bf-47ba-ba55-5d8cf6bcf27a"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Kaizala"
            AppId          = "dc3294af-4679-418f-a30c-76948e23fe1c"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "forms-app.virtosoftware.com"
            AppId          = "dc798527-aa53-4b09-9127-1098a125bc77"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft SharePoint Online - SharePoint Home"
            AppId          = "dcad865d-9257-4521-ad4d-bae3e137b345"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Device Registration Client"
            AppId          = "dd762716-544d-4aeb-a526-687b73838a22"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Windows Configuration Designer (WCD)"
            AppId          = "de0853a1-ab20-47bd-990b-71ad5077ac7b"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "MileIQ Admin Center"
            AppId          = "de096ee1-dae7-4ee1-8dd5-d88ccc473815"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Skype for Business Management Reporting and Analytics - Legacy"
            AppId          = "de17788e-c765-4d31-aba4-fb837cfff174"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "de3c96ed-0fa3-4310-8472-290bb8767230"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Device Management Client"
            AppId          = "de50c81f-5f80-4771-b66b-cebd28ccdfc1"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Graph explorer (official site)"
            AppId          = "de8bc8b5-d9f9-48b1-a8ad-b748da725064"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "CloudCheckr"
            AppId          = "de98c77e-cb77-4109-8482-966c8beb3af9"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft AppPlat EMA"
            AppId          = "dee7ba80-6a55-4f3b-a86c-746a9231ae49"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "O365 UAP Processor"
            AppId          = "df09ff61-2178-45d8-888c-4210c1c7b0b2"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Miro (formerly RealtimeBoard)"
            AppId          = "df49091e-274d-4e90-83b8-f4a003911de3"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "teams contacts griffin processor"
            AppId          = "e08ab642-962a-4175-913c-165f557d799a"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "e0ee12cb-2032-40fc-a44f-d6d9f3fad1eb"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "e10afc0f-e781-4bae-b441-ab393dbad4bc"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "CIWebService"
            AppId          = "e1335bb1-2aec-4f92-8140-0e6e61ae77e5"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Teams Web Client"
            AppId          = "e1829006-9cf1-4d05-8b48-2e665cb48e6a"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "App Studio for Microsoft Teams"
            AppId          = "e1979c22-8b73-4aed-a4da-572cc4d0b832"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "e1b016e9-7e8c-4f72-b3ef-344c18e4ca62"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "cis-e-p-sqlmi-s02-001"
            AppId          = "e22135b7-1598-470f-b28a-df321f7e197a"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "SubscriptionRP"
            AppId          = "e3335adb-5ca0-40dc-b8d3-bedc094e523b"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft.ExtensibleRealUserMonitoring"
            AppId          = "e3583ad2-c781-4224-9b91-ad15a8179ba0"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "MicrosoftMigrateProject"
            AppId          = "e3bfd6ac-eace-4438-9dc1-eed439e738de"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Storage"
            AppId          = "e406a681-f3d4-42a8-90b6-c2b029497af1"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "e440c585-22cd-4553-9f72-f376a360513c"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft events"
            AppId          = "e462442e-6682-465b-a31f-652a88bfbe51"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Skype Web Experience On Office 365"
            AppId          = "e48d4214-364e-4731-b2b6-47dabf529218"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure SQL Database Backup To Azure Backup Vault"
            AppId          = "e4ab13ed-33cb-41b4-9140-6e264582cf85"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "CIS-E-P-FUNCTION-S01-01"
            AppId          = "e5399191-4e03-43c6-aa09-8e8677d4de9e"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "TestConnect"
            AppId          = "e57dcd55-ec62-4bdc-91b3-c94434edff79"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "cisepvmstors01003"
            AppId          = "e596178c-9dd4-481b-9221-e089b552fd7a"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "e64aa8bc-8eb4-40e2-898b-cf261a25954f"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "ciswpvmstors01001"
            AppId          = "e6d05648-9205-4489-a765-ee1e7cf2ee80"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "e6ee43eb-0fbc-4546-bc52-4c161fcdf4c4"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Meru19 MySQL First Party App"
            AppId          = "e6f9f783-1fdb-4755-acaf-abed6c642885"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "cisedsqlsrv03002"
            AppId          = "e82de916-91ce-49b0-9641-4d3b207b8c60"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "e845ffd0-c8d7-407a-9491-c6b2ee20cb85"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Social Engagement"
            AppId          = "e8ab36af-d4be-4833-a38b-4d6cf1cfd525"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Spring Cloud Resource Provider"
            AppId          = "e8de9221-a19c-4c81-b814-fd37c6caf9d2"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Monitor Control Service"
            AppId          = "e933bd07-d2ee-4f1d-933c-3752b819567b"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Skype for Business Name Dictionary Service"
            AppId          = "e95d8bee-4725-4f59-910d-94d415da51b9"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "e964cda3-b4a7-48a5-b87f-155faa9a50ff"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "P2P Server"
            AppId          = "e9ed9afa-c933-4d8c-a618-7ab47d0937a7"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Data Lake"
            AppId          = "e9f49c6b-5ce5-44c8-925d-015017e9f7ad"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Credential Configuration Endpoint Service"
            AppId          = "ea890292-c8c8-4433-b5ea-b09d0668e1a6"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Flow CDS Integration Service TIP1"
            AppId          = "eacba838-453c-4d3e-8c6a-eb815d3469a3"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Substrate Instant Revocation Pipeline"
            AppId          = "eace8149-b661-472f-b40d-939f89085bd4"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "DevilFish"
            AppId          = "eaf8a961-f56e-47eb-9ffd-936e22a554ef"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Service Bus MSI App"
            AppId          = "eb070ea5-bd17-41f1-ad68-5851f6e71774"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "ebf6b2b7-c635-4217-b6b7-21de4ac65764"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Power BI Government Community Cloud"
            AppId          = "ec04d7d8-0476-4acd-bce4-81f438363d37"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure AD Identity Governance - User Management"
            AppId          = "ec245c98-4a90-40c2-955a-88b727d97151"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "ecd6b820-32c2-49b6-98a6-444530e5a77a"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "edef81f4-9321-4969-bf1d-4815245b6dc1"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Skype For Business Entitlement"
            AppId          = "ef4c7f67-65bd-4506-8179-5ddcc5509aeb"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Managed Service Identity"
            AppId          = "ef5d5c69-a5df-46bb-acaf-426f161a21a2"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Power Query Online GCC-L4"
            AppId          = "ef947699-9b52-4b31-9a37-ef325c6ffc47"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "efd8e900-682f-43bd-b3a6-c29d5efaf7ad"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "LinkedIn"
            AppId          = "f03e9017-17a2-4eea-b8d1-c27da31393d2"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "MicrosoftAzureADFulfillment"
            AppId          = "f09d1391-098c-47d7-ac7e-6ed2afc5016b"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "AAD App Management"
            AppId          = "f0ae4899-d877-4d3c-ae25-679e38eea492"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "f0b72488-7082-488a-a7e8-eada97bd842d"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "VMware Boxer"
            AppId          = "f0bdb2fb-7148-4a4f-ac82-c2c357243d58"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "f1084725-5c2d-4f61-ac84-b0e7e19dd747"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "MaintenanceResourceProvider"
            AppId          = "f18474f2-a66a-4bb0-a3c9-9b8d892092fa"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "ReportReplica"
            AppId          = "f25a7567-8ec5-4582-8a65-bfd66b0530cc"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "NDES"
            AppId          = "f26cd57e-2e16-4752-a664-5da06853ba69"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Management Groups"
            AppId          = "f2c304cf-8e7e-4c3f-8164-16299ad9d272"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Bot Framework Dev Portal"
            AppId          = "f3723d34-6ff5-4ceb-a148-d99dcd2511fc"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Power Query Online"
            AppId          = "f3b07414-6bf4-46e6-b63f-56941f3f4128"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "OCPS Admin Service"
            AppId          = "f416c5fc-9ac4-4f66-a8e5-cb203139cbe4"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Edge"
            AppId          = "f44b1140-bc5e-48c6-8dc0-5cf5a53c0e34"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "80101be6e3e9441eb5602427"
            AppId          = "f452e56a-82f2-4809-83c7-330a8b46dc00"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Portfolios"
            AppId          = "f53895d3-095d-408f-8e93-8f94b391404e"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "O365 LinkedIn Connection"
            AppId          = "f569b9c7-be15-4e87-86f7-87d30d02090b"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Teams RetentionHook Service"
            AppId          = "f5aeb603-2a64-4f37-b9a8-b544f3542865"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Application Insights API"
            AppId          = "f5c26e74-f226-4ae8-85f0-b4af0080ac9e"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "listdesigner.azurewebsites.net"
            AppId          = "f5e14a0a-d636-431e-8e25-964d4d88384e"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "f5eaa862-7f08-448c-9c4e-f4047d4d4521"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Azure Log Search Alerts"
            AppId          = "f6b60513-f290-450e-a2f3-9930de61c5e7"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft.MileIQ.Dashboard"
            AppId          = "f7069a8d-9edc-4300-b365-ae53c9627fc4"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure Blueprints"
            AppId          = "f71766dc-90d9-4b7d-bd9d-4499c4331c3f"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Marketplace Api"
            AppId          = "f738ef14-47dc-4564-b53b-45069484ccc7"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Apple Internet Accounts"
            AppId          = "f8d98a96-0999-43f5-8af3-69971c7bb423"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "C025EF13-008C-4A5F-8819-3A3C3DA03697"
            AppId          = "f9729a43-2aa0-4740-81a5-b07e002ddfb7"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "f97b3686-bc70-4fee-b56d-f45878420d15"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Office 365 Enterprise Insights"
            AppId          = "f9d02341-e7aa-456d-926d-4a0ca599fbee"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Workflow"
            AppId          = "f9e1a512-fa24-4671-a2ae-0e3c7c7f8b06"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Mini Calendar and Date Picker"
            AppId          = "fa94021b-6cad-41d2-bf91-161e46628b46"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Exchange REST API Based Powershell"
            AppId          = "fb78d390-0c51-40cd-8e17-fdbfab77341b"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Substrate-FileWatcher"
            AppId          = "fbb0ac1a-82dd-478b-a0e5-0b2b98ef38fe"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Capacity "
            AppId          = "fbc197b7-9e9c-4f98-823f-93cb1cb554e6"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure AD Notification"
            AppId          = "fc03f97a-9db0-4627-a216-ec98ce54e018"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "fc0f3af4-6835-4174-b806-f7db311fd2f3"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Power BI Government Community Cloud"
            AppId          = "fc4979e5-0aa5-429f-b13a-5d1365be5566"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Azure AD Identity Protection"
            AppId          = "fc68d9e5-1f76-45ef-99aa-214805418498"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "AzureAutomation"
            AppId          = "fc75330b-179d-49af-87dd-3b1acf6827fa"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "WindowsDefenderATP"
            AppId          = "fc780465-2017-40d4-a0c5-307022471b92"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "fced9511-5de1-4ce1-9064-743aa72d37e3"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "SharePoint Migration Tool"
            AppId          = "fdd7719f-d61e-4592-b501-793734eb8a0e"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "Microsoft Support"
            AppId          = "fdf9885b-dd37-42bf-82e5-c3129ef5a302"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "-"
            AppId          = "fe966321-fbe5-4f67-9463-94fafa316665"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "make.gov.powerapps.us"
            AppId          = "feb2c8aa-4f70-4881-abec-521141627b04"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "cisepnsgflowstors01001"
            AppId          = "ff861d19-1253-4ec8-92fd-add0a49a4374"
        }),
        $(New-Object -TypeName psobject -Property @{
            AppName        = "SalesForce"
            AppId          = "1bccd5e6-c64e-4074-8581-66d54ba0f43d"
        })
    )

    try {
        $cur_user = [System.Security.Principal.WindowsIdentity]::GetCurrent()
        $cur_dom = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()
        $cur_for = [System.DirectoryServices.ActiveDirectory.Forest]::GetCurrentForest()
    }
    catch {}
    # AZURE

    ############################

    #########  LISTS  ##########

    ############################

    $global:AADAuthAdminsList = @(
        'c4e39bd9-1100-46d3-8c65-fb160da0071f',
        '88d8e3e3-8f55-4a1e-953a-9b9898b8876b',
        '95e79109-95c0-4d8e-aee3-d01accf2d47b',
        '729827e3-9c14-49f7-bb1b-9608f156bbb8',
        '790c1fb9-7f7d-4f88-86a1-ef1f95c05c1b',
        '4a5d8f65-41da-4de4-8968-e035b65339cf'
    )
    $global:AADAdminList = @(
        '88d8e3e3-8f55-4a1e-953a-9b9898b8876b',
        '95e79109-95c0-4d8e-aee3-d01accf2d47b',
        '729827e3-9c14-49f7-bb1b-9608f156bbb8',
        '790c1fb9-7f7d-4f88-86a1-ef1f95c05c1b',
        '4a5d8f65-41da-4de4-8968-e035b65339cf',
        'fe930be7-5e62-47db-91af-98c3a49a38b1'
    )
    $global:AADHelpdeskAdminsList = @(
        'c4e39bd9-1100-46d3-8c65-fb160da0071f',
        '88d8e3e3-8f55-4a1e-953a-9b9898b8876b',
        '95e79109-95c0-4d8e-aee3-d01accf2d47b',
        '729827e3-9c14-49f7-bb1b-9608f156bbb8',
        '790c1fb9-7f7d-4f88-86a1-ef1f95c05c1b',
        '4a5d8f65-41da-4de4-8968-e035b65339cf'
    ) 
    $global:AADPasswordAdminList = @(
        '88d8e3e3-8f55-4a1e-953a-9b9898b8876b',
        '95e79109-95c0-4d8e-aee3-d01accf2d47b',
        '966707d0-3269-4727-9be2-8c3a10f19b9d'
    )
    [hashtable]$global:ClaimsList = [ordered]@{
        aud                = "Recipient of the token - its audience. In v2.0 tokens, this is always the client ID of the API. In vv1.0 tokens it can be the client ID or the resource URI used in the request, depending on how the client requested the token"
        iss                = "Security token service (STS) that constructs and returns the token and the Azure AD tenant in which the user was authenticated If the token issued is a v2.0 token, the URI will end in /v2.0. The GUID that indicates that the user is a consumer user from a Microsoft account is 9188040d-6c67-4c5b-b112-36a304b66dad. Your app can use the GUID portion of the claim to restrict the set of tenants that can sign in to the app, if applicable"
        idp                = "Identity provider that authenticated the subject of the token. This value is identical to the value of the Issuer claim unless the user account not in the same tenant as the issuer - guests, for instance. If the claim isn't present, it means that the value of iss can be used instead. For personal accounts being used in an organizational context (for instance, a personal account invited to an Azure AD tenant), the idp claim may be 'live.com' or an STS URI containing the Microsoft account tenant 9188040d-6c67-4c5b-b112-36a304b66dad"
        iat                = "Issued At - When the authentication for this token occurred"
        nbf                = "Not Before - The time before which the JWT must not be accepted for processing"
        exp                = "Expiration Time - JWT must not be accepted after this time. A resource may reject the token before this time such as when a change in authentication is required or a token revocation has been detected"
        aio                = "Internal claim AD uses to record data for token reuse"
        acr                = "(Authentication Context) - Only present in v1.0 tokens. 0 = end-user authentication did not meet the requirements of ISO/IEC 29115"
        amr                = "how the subject of the token was authenticated. Only present in v1.0 tokens"
        appid              = "Application ID of the client using the token. Only present in v1.0 tokens. The application can act as itself or on behalf of a user. The application ID typically represents an application object, but it can also represent a service principal object in Azure AD"
        azp                = "Only present in v2.0 tokens, a replacement for appid. The application ID of the client using the token. The application can act as itself or on behalf of a user. The application ID typically represents an application object, but it can also represent a service principal object in Azure AD"
        appidacr           = "How the client was authenticated. Only present in v1.0 tokens. 0 = public client.1 = client ID and client secret. 2 = client certificate"
        azpacr             = "How the client was authenticated. Only present in v2.0 tokens, a replacement for appidacr. 0 = public client. 1 = client ID and client secret. 2 = client certificate"
        preferred_username = "The primary username that represents the user. It could be an email address, phone number, or a generic username without a specified format. Its value is mutable and might change over time. Since it is mutable, this value must not be used to make authorization decisions. It can be used for username hints, however, and in human-readable UI as a username. The profile scope is required in order to receive this claim. Present only in v2.0 tokens"
        name               = "Provides a human-readable value that identifies the subject of the token. The value is not guaranteed to be unique, it is mutable, and it's designed to be used only for display purposes. The profile scope is required in order to receive this claim"
        scp                = "The set of scopes exposed by the application which the client has requested and received consent. Apps should verify that these scopes are valid ones exposed by the app, and make authorization decisions based on the value of these scopes. Only included for user tokens"
        roles              = "The set of permissions exposed by your application that the requesting application or user has been given permission to call. For application tokens, this is used during the client credential flow (v1.0, v2.0) in place of user scopes. For user tokens this is populated with the roles the user was assigned to on the target application"
        wids               = "The tenant-wide roles assigned to this user, from the section of roles present in Azure AD built-in roles. This claim is configured on a per-application basis, through the groupMembershipClaims property of the application manifest. Setting it to All or DirectoryRole is required. May not be present in tokens obtained through the implicit flow due to token length concerns"
        groups             = "Group memberships of the subject. These values are unique (see Object ID) and can be safely used for managing access, such as enforcing authorization to access a resource. The groups included in the groups claim are configured on a per-application basis, through the groupMembershipClaims property of the application manifest. A value of null will exclude all groups, a value of SecurityGroup will include only Active Directory Security Group memberships, and a value of All will include both Security Groups and Microsoft 365 Distribution Lists"
        sub                = "The principal about which the token asserts information, such as the user of an app. This value is immutable and cannot be reassigned or reused. It can be used to perform authorization checks safely, such as when the token is used to access a resource, and can be used as a key in database tables. Because the subject is always present in the tokens that Azure AD issues, we recommend using this value in a general-purpose authorization system. The subject is, however, a pairwise identifier - it is unique to a particular application ID. Therefore, if a single user signs into two different apps using two different client IDs, those apps will receive two different values for the subject claim"
        oid                = "The immutable identifier for principal of the request. In ID tokens and app+user tokens, this is the object ID of the user. In app-only tokens, this is the object id of the calling service principal. It can also be used to perform authorization checks safely and as a key in database tables. This ID uniquely identifies the principal across applications - two different applications signing in the same user will receive the same value in the oid claim. Thus, oid can be used when making queries to Microsoft online services, such as the Microsoft Graph. The Microsoft Graph will return this ID as the id property for a given user account. Because the oid allows multiple apps to correlate principals, the profile scope is required in order to receive this claim for users. Note that if a single user exists in multiple tenants, the user will contain a different object ID in each tenant - they are considered different accounts, even though the user logs into each account with the same credentials"
        tid                = "Represents the Azure AD tenant that the user is from. For work and school accounts, the GUID is the immutable tenant ID of the organization that the user belongs to. For personal accounts, the value is 9188040d-6c67-4c5b-b112-36a304b66dad. The profile scope is required in order to receive this claim"
        unique_name        = "Only present in v1.0 tokens. Provides a human readable value that identifies the subject of the token. This value is not guaranteed to be unique within a tenant and should be used only for display purposes"
        uti                = "An internal claim used by Azure to revalidate tokens. Resources should not use this claim"
        rh                 = "An internal claim used by Azure to revalidate tokens. Resources should not use this claim"
        ver                = "Indicates the version of the access token"
        typ                = "Indicates the token is a JWT token"
        alg                = "Indicates the algorithm that was used to sign the token"
        kid                = "Specifies the thumbprint for the public key that can be used to validate the token signature"
        x5t                = "Functions the same (in use and value) as kid. x5t is a legacy claim emitted only in v1.0 ID tokens for compatibility purposes"

    }
    [array]$global:apisList = @(
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "graph.microsoft.com"
                ApiName   = "MS Graph"
                VarName   = "msgraph"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "graph.windows.net"
                ApiName   = "AAD Graph"
                VarName   = "aadgraph"
                Endpoints = "me", "users", "groups", "contacts", "directoryRoles", "applications", "servicePrincipals", "domains", "policies"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "app.vssps.visualstudio.com"
                ApiName   = "Azure DevOps"
                VarName   = "devops"
                Endpoints = "https://docs.microsoft.com/rest/api/azure/devops?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "management.core.windows.net"
                ApiName   = "Azure Core Management"
                VarName   = "azcoremgmt"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "management.azure.com"
                ApiName   = "Azure Service Management"
                VarName   = "azsvcmgmt"
                Endpoints = "https://msdn.microsoft.com/library/azure/ee460799.aspx"
            }),
        $(New-Object -TypeName psobject -Property @{
                #      ApiHost="enrollment.manage.microsoft.com"
                ApiHost   = "api.manage.microsoft.com"
                ApiName   = "Intune"
                VarName   = "intune"
                Endpoints = "https://docs.microsoft.com/intune/intune-graph-apis?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps#intune-permission-scopes"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = ($cur_tenant_id + "-my.sharepoint.com")
                ApiName   = "OneDrive"
                VarName   = "onedrive"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                #  ApiHost=($cur_tenant_id + ".sharepoint.com")
                ApiHost   = "microsoft.sharepoint-df.com"
                ApiName   = "Sharepoint"
                VarName   = "sharepoint"
                Endpoints = "https://docs.microsoft.com/sharepoint/dev/sp-add-ins/get-to-know-the-sharepoint-rest-service?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "mysignins.microsoft.com"
                ApiName   = "MySignins"
                VarName   = "mysignins"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "officeapps.live.com"
                ApiName   = "Office Apps"
                VarName   = "officeapps"
                Endpoints = "https://ocws.officeapps.live.com/ocs/v2/"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "outlook.office365.com"
                ApiName   = "MS Exchange"
                VarName   = "exchange"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "api.spaces.skype.com"
                ApiName   = "MS Teams"
                VarName   = "teams"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "api.diagnostics.office.com"
                ApiName   = "SARA"
                VarName   = "sara"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "vault.azure.net"
                ApiName   = "Azure Key Vault"
                VarName   = "keyvault"
                Endpoints = "https://docs.microsoft.com/rest/api/keyvault/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "api.azrbac.mspim.azure.com"
                ApiName   = "Azure PIM"
                VarName   = "azure_pim"
                Endpoints = ""
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "print.print.microsoft.com"
                ApiName   = "Universal Print"
                VarName   = "print"
                Endpoints = "https://docs.microsoft.com/en-us/universal-print/hardware/universal-print-oem-app-registration?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "api.yammer.com"
                ApiName   = "Yammer"
                VarName   = "yammer"
                Endpoints = "https://developer.yammer.com/docs/getting-started"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "officespeech.platform.bing.com"
                ApiName   = "Speech"
                VarName   = "speech"
                Endpoints = "https://docs.microsoft.com/azure/cognitive-services/speech/home?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "api.skypeforbusiness.com"
                ApiName   = "Skype"
                VarName   = "skype"
                Endpoints = "https://docs.microsoft.com/skype-sdk/ucwa/authenticationusingazuread?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "batch.core.windows.net"
                ApiName   = "Azure Batch"
                VarName   = "azure_batch"
                Endpoints = "https://docs.microsoft.com/azure/batch/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "api.azuredatacatalog.com"
                ApiName   = "Azure Catalog"
                VarName   = "azure_catalog"
                Endpoints = "https://docs.microsoft.com/azure/data-catalog/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "azure.microsoft.com/services/data-explorer"
                ApiName   = "Azure Data Explorer"
                VarName   = "azure_data_explorer"
                Endpoints = "https://docs.microsoft.com/azure/kusto/api/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "datalake.azure.net"
                ApiName   = "Azure Data Lake"
                VarName   = "azure_data_lake"
                Endpoints = "https://docs.microsoft.com/azure/data-lake-store/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "atlas.microsoft.com"
                ApiName   = "Azure Maps"
                VarName   = "azure_maps"
                Endpoints = "https://docs.microsoft.com/azure/azure-maps/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "purview.azure.net"
                ApiName   = "Azure Purview"
                VarName   = "azure_purview"
                Endpoints = "https://docs.microsoft.com/azure/purview?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "aadrm.com"
                ApiName   = "Azure Rights Management Services"
                VarName   = "azure_rms"
                Endpoints = "https://docs.microsoft.com/azure/information-protection/develop/developers-guide?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "storage.azure.com"
                ApiName   = "Azure Storage"
                VarName   = "azure_storage"
                Endpoints = "https://docs.microsoft.com/azure/storage/?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "azurecustomerinsights.com"
                ApiName   = "Customer Insights"
                VarName   = "insights"
                Endpoints = "https://docs.microsoft.com/dynamics365/customer-engagement/customer-insights/ref/apiquickref?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }) ,       
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "dynamics.microsoft.com"
                ApiName   = "Dynamics 365 Business Central"
                VarName   = "dynamics_365"
                Endpoints = "https://docs.microsoft.com/dynamics-nav/fin-graph?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "admin.services.crm.dynamics.com"
                ApiName   = "Dynamics CRM"
                VarName   = "dynamics_crm"
                Endpoints = "https://go.microsoft.com/fwlink/?linkid=2143766"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "erp.dynamics.com"
                ApiName   = "Dynamics ERP"
                VarName   = "dynamics_erp"
                Endpoints = "https://docs.microsoft.com/dynamics365/unified-operations/dev-itpro/data-entities/services-home-page?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "service.flow.microsoft.com"
                ApiName   = "Flow Service"
                VarName   = "flow_service"
                Endpoints = "https://docs.microsoft.com/flow/dev-enterprise-intro?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "manage.office.com"
                ApiName   = "Office 365 Management APIs"
                VarName   = "o365_mgmt"
                Endpoints = "https://msdn.microsoft.com/office-365/office-365-management-activity-api-reference"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "onenote.com"
                ApiName   = "OneNote"
                VarName   = "onenote"
                Endpoints = "https://msdn.microsoft.com/office/office365/howto/onenote-landing"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "analysis.windows.net"
                ApiName   = "Power BI Service"
                VarName   = "power_bi"
                Endpoints = "https://docs.microsoft.com/power-bi/service-get-started?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            }),
        $(New-Object -TypeName psobject -Property @{
                ApiHost   = "rts.powerapps.com"
                ApiName   = "PowerApps Runtime Service"
                VarName   = "powerapps"
                Endpoints = "https://docs.microsoft.com/powerapps/developer/common-data-service/overview?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps"
            })
    )
	
    $apisList | Foreach-Object { 
             $_ | Add-Member -NotePropertyName 'TokenName' -NotePropertyValue ($_.VarName + '_token') 
	     New-TimeDatedVar -nam $_.VarName -val $_.ApiHost
    }
    
    
    [array]$global:RoleList = @(

        $(New-Object -TypeName psobject -Property @{
                roleName = 'PrivilegedRoleAdminRights' 
                roleDesc = "Can add role assignments to all users"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'AppAdminsRights'                     
                roleDesc = "Can create new secrets for SPs, Write pwresetrights, groupsrights, globaladminrights, privroleadminrights"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'PrivAuthAdminRights' 
                roleDesc = "Reset ALL user passwords except external (`"#EXT#`")" 
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'AuthAdminsRights'                   
                roleDesc = "Reset ALL user passwords except external (`"#EXT#`")"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Intune administrator'
                roleDesc = "Add principals to cloud-resident security groups"
                roleId   = "3a2c62db-5318-420d-8d74-23affee5d9d5"
                roleVar  = "`$AADIntuneAdmins"
                roleAbuse = "Add user to group to gain privs"
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Groups Administrator' 
                roleDesc = "Add principals to cloud-resident security groups"
                roleId   = ""
                roleVar  = "`$AADGroupsAdmins"
                roleAbuse = "Add user to group to gain privs"
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Global Admin'
                roleDesc = "Full control of everything in the tenant"
                roleId   = ""
                roleVar  = "`$AADGlobalAdmins"
                roleAbuse = "Reset user password/Change user privs"
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'PrivilegedRoleAdministrator'
                roleDesc = "Add role assignments to all users"
                roleId   = 'e8611ab8-c189-46e8-94e1-60213ab1f814'
                roleVar  = ""
                roleAbuse = "`$AADPrivRoleAdmins"
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Application Admins'
                roleDesc = "Create and manage app registrations and enterprise apps."
                roleId   = "9b895d92-2cd3-44c7-9d02-a6ac2d5ea5c3"
                roleVar  = "`$AADAppAdmins"
                roleAbuse = "Create new secrets for SPs"
            }),
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Attack payload author'
                roleDesc = "Create attack payloads that an administrator can initiate later."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Attack simulation administrator'
                roleDesc = "Create and manage all aspects of attack simulation campaigns."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Authentication administrator'
                roleDesc = "Reset authentication method information for all non-admin users"
                roleId   = ""
                roleVar  = "`$AADAuthAdmins"
                roleAbuse = "Reset user password/Change user privs"
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Authentication policy administrator'
                roleDesc = "Create/manage all authentication methods and password protection policies"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Azure AD joined device local administrator'
                roleDesc = "Members of local administrators group on Azure AD-joined devices."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Azure DevOps administrator'
                roleDesc = "Manage Azure DevOps organization policy and settings."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Azure Information Protection administrator'
                roleDesc = "Manage all aspects of the Azure Information Protection product."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'B2C IEF Keyset administrator'
                roleDesc = "Manage secrets for federation and encryption in the Identity Experience Framework."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'B2C IEF Policy administrator'
                roleDesc = "Create/manage trust framework policies in the Identity Experience Framework."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Billing administrator'
                roleDesc = "Can perform common billing related tasks like updating payment information."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Cloud App Security Administrator '
                roleDesc = "Manage all aspects of the Cloud App Security product"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Cloud Application Administrator'
                roleDesc = "Manage all aspects of the Cloud App Security product"
                roleId   = '158c047a-c907-4556-b7ef-446551a6b5f7'
                roleVar  = "`$AADCloudAppAdmins"
                roleAbuse = "Create new secrets for SPs"
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Cloud device administrator'
                roleDesc = "Full access to manage devices in Azure AD."
                roleId   = ""
                roleVar  = "`$AADCloudDevAdmins"
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Compliance administrator'
                roleDesc = "Manage compliance configuration and reports in Azure AD and Office 365"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Compliance data administrator'
                roleDesc = "Create and manage compliance content."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Conditional Access administrator'
                roleDesc = "Manage Conditional Access capabilities."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Desktop Analytics administrator'
                roleDesc = "Manage Desktop management tools and services."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Domain name administrator'
                roleDesc = "Can manage domain names in cloud and on-premises."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Dynamics 365 administrator'
                roleDesc = "Can manage all aspects of the Dynamics 365 product."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Exchange administrator'
                roleDesc = "Can manage all aspects of the Exchange product."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Exchange recipient administrator'
                roleDesc = "Create or update Exchange Online recipients within the Exchange Online organization."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'External ID user flow administrator'
                roleDesc = "Create and manage all aspects of user flows."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
           })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'External ID user flow attribute administrator'
                roleDesc = "Create and manage the attribute schema available to all user flows."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'External Identity Provider administrator'
                roleDesc = "Configure identity providers for use in direct federation."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Global reader'
                roleDesc = "Can read everything that a global administrator can, but not update anything."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Helpdesk administrator'
                roleDesc = "Reset passwords for non-admins and Helpdesk admins."
                roleId   = ""
                roleVar  = "`$AADHelpdeskAdmins"
                roleAbuse = "Reset user password/Change user privs"
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Hybrid Identity Administrator'
                roleDesc = "Manage AD to Azure AD cloud sync and federation settings"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Identity Governance Administrator'
                roleDesc = "Manage access using Azure AD for identity governance scenarios."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Insights administrator'
                roleDesc = "Has administrative access in the Insights app"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Kaizala administrator'
                roleDesc = "Manage settings for Microsoft Kaizala."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Knowledge administrator'
                roleDesc = "Configure knowledge network and content understanding."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'License administrator'
                roleDesc = "Ability to assign, remove and update license assignments."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Network administrator'
                roleDesc = "Can manage network locations and review enterprise network design insights for Microsoft 365 Software as a Service applications."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Office apps administrator'
                roleDesc = "Manage Office apps cloud services, including policy and settings management, and manage the ability to select, unselect and publish `"what's new`" feature content to end-user's devices."
                roleId   = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Password Administrator'
                roleDesc = "Reset passwords for non-admins and Password admins"
                roleId   = ""
                roleVar  = "`$AADPasswordAdmins"
                roleAbuse = "Reset user password/Change user privs"
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Power BI administrator'
                roleDesc = "Can manage all aspects of the Power BI product"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Power platform administrator'
                roleDesc = "Can create and manage all aspects of Microsoft Dynamics 365, PowerApps, and Microsoft Flows."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Printer administrator'
                roleDesc = "Can manage all printers and printer connectors"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Privileged Authentication Administrator'
                roleDesc = "Allowed to view, set and reset authentication method information for any user (admin or non-admin)."
                roleId   = ""
                roleVar  = "`$AADPrivAuthAdmins"
                roleAbuse = "Reset user password/Change user privs"
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Privileged Role Administrator'
                roleDesc = "Manage role assignments in Azure AD, and all aspects of Privileged Identity Management."
                roleId   = ""
                roleVar  = "`$AADPrivRoleAdmins"
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Search Administrator'
                roleDesc = "Can create and manage all aspects of Microsoft Search settings."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Security Administrator'
                roleDesc = "Read security information and reports, and manage configuration in Azure AD and Office 365."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Service Support Administrator'
                roleDesc = "Read service health information and manage support tickets"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'SharePoint Administrator'
                roleDesc = "Manage all aspects of the SharePoint service"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Skype for Business administrator'
                roleDesc = "Manage all aspects of the Skype for Business product"
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Teams Administrator'
                roleDesc = "Manage the Microsoft Teams service."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Teams Communications Administrator'
                roleDesc = "Manage calling and meetings features within the Microsoft Teams service."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
           })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'Teams Devices Administrator'
                roleDesc = "Perform management related tasks on Teams certified devices."
                roleId   = ""
                roleVar  = ""
                roleAbuse = ""
            })
        $(New-Object -TypeName psobject -Property @{
                roleName = 'User Administrator'
                roleDesc = "Manage all aspects of users and groups, including resetting passwords for limited admins."
                roleId   = ""
                roleVar  = "`$AADUserAccountAdmins"
                roleAbuse = "Reset user password/Change user privs"
            })
    )
    [array]$global:samlEndpoints = @(
        $(New-Object -TypeName psobject -Property @{
                SamlName = "Tenable"
                SamlHome = "https://cloud.tenable.com"
                Endpoint = "https://cloud.tenable.com/saml/login/9b8c5eeb-f3bf-48f1-b931-b49f2642bb6b"
            })    
    )
    [array]$global:amrs = New-Object psobject -Property @{
        pwd      = "Password authentication"
        rsa      = "RSA key, Authenticator app, self-signed JWT with a service owned X509 certificate"
        otp      = "One-time passcode using email or text"
        fed      = "Federated assertion used (JWT, SAML)"
        wia      = "Windows Integrated Authentication"
        mfa      = "Multi-factor authentication"
        ngcmfa   = "Equivalent to mfa, used for provisioning of certain advanced credential types"
        wiaormfa = "Windows or MFA was used"
        none     = "No authentication"
    }

    $global:azure_pwsh_id = "1950a258-227b-4e31-a9cf-717495945fc2"

    $global:trustattributes = @{
        '0'  = "-"
        '1'  = "Non-Transitive"
        '2'  = "Uplevel clients (Win2000 or newer)"
        '4'  = "Quarantined Domain (Ext)"
        '8'  = "Forest Trust"
        '10' = "Cross-Organizational"
        '16' = "Cross-Organizational" 
        '20' = "Intra-Forest"
        '32' = "Intra-Forest"
        '64' = "Inter-Forest"
        '72' = "Cross-Forest (Ext)"
    }
    $global:trustdirections = @{
        '1' = "Inbound (TrustING)"
        '2' = "Outbound (TrustED)"
        '3' = "Bidirectional"
    }
    $global:trusttypes = @{
        '1' = "Downlevel (WinNT domain external)"
        '2' = "Uplevel (AD domain)"
        '3' = "MIT (non-Windows) Kerberos"
        '4' = "DCE (Theoretical trust type)"
    }
    $global:riskDetections = @(
        $(New-Object -TypeName psobject -Property @{
                Name = "Require MFA for administrators"
                Type = "-"
                Desc = "Require MFA for priviliged groups"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Secure security info registration"
                Type = "-"
                Desc = "Securing when and how users register for Azure AD Multi-Factor Authentication and self-service password reset"
            })
        $(New-Object -TypeName psobject -Property @{
                Name = "Block legacy authentication"
                Type = "-"
                Desc = "Block legacy authentication protocols like POP, SMTP, IMAP, and MAPI that can't enforce MFA"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Require MFA for all users"
                Type = "-"
                Desc = "Requires all users must complete MFA"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Require MFA for Azure management"
                Type = "-"
                Desc = "Require MFA for management tools like Azure Portal, Azure CLI, Azure PowerShell"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Require MFA for risky sign-in"
                Type = "-"
                Desc = "MFA for users who may have presented an authentication request that isn't authorized by the identity owner"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Require password change for risky users"
                Type = "-"
                Desc = "Force password change for users who may have presented an authentication request that isn't authorized by the identity owner"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Require compliant or hybrid joined devices"
                Type = "-"
                Desc = "Require specific device compliance settings like PIN unlock, device encryption, OS version"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Require approved app or app protection policy"
                Type = "-"
                Desc = "Restrict access to approved (modern authentication capable) client apps with Intune app protection policies."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Block access by location"
                Type = "-"
                Desc = "Restrict access to your cloud apps based on the IP address of a user"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Block access"
                Type = "-"
                Desc = "Block access to one or more apps"
            })
    )
    $global:conditionalAccessPolicies = @(
        $(New-Object -TypeName psobject -Property @{
                Name = "Anonymous IP address"
                Type = "Real-time"
                Desc = "Indicates sign-ins from an anonymous IP address (for example, Tor browser or anonymous VPN)"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Atypical travel"
                Type = "Offline"
                Desc = "Identifies two sign-ins originating from geographically distant locations, where at least one of the locations may also be atypical for the user, given past behavior. Among several other factors, this machine learning algorithm takes into account the time between the two sign-ins and the time it would have taken for the user to travel from the first location to the second, indicating that a different user is using the same credentials."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Anomalous Token"
                Type = "Offline"
                Desc = "Indicates unusual token lifetime or a token that is played from an unfamiliar locations. Covers Session Tokens and Refresh Tokens."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Token Issuer Anomaly"
                Type = "Offline"
                Desc = "Indicates the SAML token issuer for the associated SAML token is potentially compromised. The claims included in the token are unusual or match known attacker patterns."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Malware linked IP address"
                Type = "Offline"
                Desc = "Indicates sign-ins from IP addresses infected with malware that is known to actively communicate with a bot server. This detection has been deprecated."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Suspicious browser"
                Type = "Offline"
                Desc = "Indicates anomalous behavior based on suspicious sign-in activity across multiple tenants from different countries in the same browser."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Unfamiliar sign-in properties"
                Type = "Real-time"
                Desc = "Considers past sign-in history (IP, Latitude / Longitude and ASN) to look for anomalous sign-ins. The system stores information about previous locations used by a user, and considers these familiar locations. The risk detection is triggered when the sign-in occurs from a location that is not already in the list of familiar locations. Newly created users will be in learning mode for a while where unfamiliar sign-in properties risk detections will be turned off while our algorithms learn the user's behavior."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Admin confirmed user compromised"
                Type = "Offline"
                Desc = "Indicates an admin has selected 'Confirm user compromised' in the Risky users UI or using riskyUsers API."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Malicious IP address"
                Type = "Offline"
                Desc = "Indicates sign-in from a malicious IP address. An IP address is considered malicious based on high failure rates because of invalid credentials received from the IP address or other IP reputation sources."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Suspicious inbox manipulation rules"
                Type = "Offline"
                Desc = "Microsoft Defender for Cloud Apps profiles your environment and triggers alerts when suspicious rules that delete or move messages or folders are set on a user's inbox. This detection may indicate that the user's account is compromised, that messages are being intentionally hidden, and that the mailbox is being used to distribute spam or malware in your organization."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Password spray"
                Type = "Offline"
                Desc = "Indicates multiple usernames are attacked using common passwords in a unified brute force manner to gain unauthorized access."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Impossible travel"
                Type = "Offline"
                Desc = "Identifies two user activities (is a single or multiple sessions) originating from geographically distant locations within a time period shorter than the time it would have taken the user to travel from the first location to the second, indicating that a different user is using the same credentials."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "New country"
                Type = "Offline"
                Desc = "Microsoft Defender for Cloud Apps considers past activity locations to determine new and infrequent locations."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Activity from anonymous IP address"
                Type = "Offline"
                Desc = "Identifies that users were active from an IP address that has been identified as an anonymous proxy IP address."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Suspicious inbox forwarding"
                Type = "Offline"
                Desc = "Identifies suspicious email forwarding rules, for example, if a user created an inbox rule that forwards a copy of all emails to an external address."
            }),
        $(New-Object -TypeName psobject -Property @{
                Name = "Azure AD threat intelligence"
                Type = "Offline"
                Desc = "Indicates sign-in activity that is unusual for the given user or is consistent with known attack patterns based on Microsoft's internal and external threat intelligence sources."
            })
    )
    [array]$global:DirectoryToolsCmdlets = @(

        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AzureUser"
                Alias       = "gazu"
                Description = "Get one or all azure user objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Org"
                Alias       = "gorg"
                Description = "Get one or all azure organizations"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Set-Org"
                Alias       = "sorg"
                Description = "Set vars for an organization"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AzureApi"
                Alias       = "gaa"
                Description = "Get list of Azure API endpoints"
                Category    = "info"
                Mod         = "DirTools"
                Status      = "good"
                Help        = "-"
            }), 

        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Tenant"
                Alias       = "gten"
                Description = "Get one or all tenant objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-LinkedGpos"
                Alias       = "glg"
                Description = "Get linked GPOs"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-NNetFForest"
                Alias       = "gnf"
                Description = "Get one or all forests"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-DDomainSSearcher"
                Alias       = "gds"
                Description = "Get domain searcher object"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-GGptTTmpl"
                Alias       = "ggpt"
                Description = "Get GPT TMPL"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-GGroupsXXML"
                Alias       = "ggx"
                Description = "Get groups XML"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-HoundSummary"
                Alias       = "ghs"
                Description = "Get summary of hound vars"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-GGUIDMMap"
                Alias       = "ggm"
                Description = "Get map of all GUIDs"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Sub"
                Alias       = "gsub"
                Description = "Get one or all subscription objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-ResourceGroup"
                Alias       = "grg"
                Description = "Get one or all resource group objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Group"
                Alias       = "gg"
                Description = "Get one or all group objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-GroupOwner"
                Alias       = "ggo"
                Description = "Get one or all group owner objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-GroupMembers"
                Alias       = "ggm"
                Description = "Get one or all group member objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Role"
                Alias       = "gr"
                Description = "Get one or all role objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-RoleMembers"
                Alias       = "grm"
                Description = "Get one or all role member objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Dev"
                Alias       = "gd"
                Description = "Get one or all device objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-DevOwner"
                Alias       = "gdo"
                Description = "Get one or all device owner objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-App"
                Alias       = "ga"
                Description = "Get one or all app objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AppOwner"
                Alias       = "gao"
                Description = "Get one or all app owner objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "bad"
            }),    
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-ServicePrincipal"
                Alias       = "gsp"
                Description = "Get one or all service principal objects"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Scope"
                Alias       = "gs"
                Description = "Uses argument to find and return scope object via `$msgraph"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-UserRole"
                Alias       = "gur"
                Description = "Get roles for user"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-RoleTemplate"
                Alias       = "grt"
                Description = "Get role template"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AuthenticationAdminRole"
                Alias       = "gaar"
                Description = "Get authentication admin roles"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-PrivAuthAdminRights"
                Alias       = "gpaar"
                Description = "Get all priv auth admins"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-InbPermsVMs"
                Alias       = "gipav"
                Description = "Get inbound perms against vms"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-InbPermsResourceGroups"
                Alias       = "giprg"
                Description = "Get inbound perms against resource groups"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-InbPermsKeyVaults"
                Alias       = "gipkv"
                Description = "Get inbound perms against key vaults"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-SPsWithoutRoles"
                Alias       = "gspwr"
                Description = "Get service principals without roles"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Use-AzureRole"
                Alias       = "uar"
                Description = "Activate azure role"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-NNetUUser"
                Alias       = "gnu"
                Description = "Get ad user object"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-NNetCComputer"
                Alias       = "gnc"
                Description = "Get ad computer object"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-NNetGGroup"
                Alias       = "gng"
                Description = "Get ad group object"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-NNetGGroupMMember"
                Alias       = "gngm"
                Description = "Get ad group member object"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-NNetDDomain"
                Alias       = "gnd"
                Description = "Get ad domain object"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-NNetGGpo"
                Alias       = "gngpo"
                Description = "Get ad gpo object"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Convert-AADNName"
                Alias       = "can"
                Description = "Convert AD name"
                Category    = "util"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-OObjectAAcl"
                Alias       = "goacl"
                Description = "Get ad object acl"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Convert-LLDAPPProperty"
                Alias       = "cldap"
                Description = "convert ldap property"
                Category    = "util"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-NNetGGpoGGroup"
                Alias       = "gngg"
                Description = "Get ad gpo group object"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-DirectoryTools"
                Alias       = "dt"
                Description = "Get usage on cmdlets in this module "
                Category    = "util"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Api"
                Alias       = "gai"
                Description = "Call api with Oauth2 token or PAT"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-ApiEndpoints"
                Alias       = "gae"
                Description = "Lists known endpoints for an API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "good"
            }),  
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AzureCoreManagementApi"
                Alias       = "gcma"
                Description = "Call the AzureCoreManagement API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AADGraphApi"
                Alias       = "gaga"
                Description = "Call the AADGraph API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AzureDevOpsApi"
                Alias       = "gdoa"
                Description = "Call the AzureDevOps API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AzureServiceManagerApi"
                Alias       = "gasma"
                Description = "Call the AzureServiceManager API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-IntuneApi"
                Alias       = "gia"
                Description = "Call the Intune API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-MSExchangeApi"
                Alias       = "gea"
                Description = "Call the MSExchange API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-MSGraphApi"
                Alias       = "gga"
                Description = "Call the MSGraph API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-MSTeamsApi"
                Alias       = "gta"
                Description = "Call the MSTeams API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-MySignInsApi"
                Alias       = "gmsia"
                Description = "Call the MySignins API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-OfficeAppsApi"
                Alias       = "goaa"
                Description = "Call the OfficeApps API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-OneDriveApi"
                Alias       = "goda"
                Description = "Call the OneDriveApi API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-SARAApi"
                Alias       = "gsa"
                Description = "Call the SARA API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-SharepointApi"
                Alias       = "gspa"
                Description = "Call the Sharepoint API"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "New-CodeChallenge"
                Alias       = "ncc"
                Description = "Generates random code challenge"
                Category    = "util"
                Mod         = "DirTools"
                Status      = "good"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Convert-NNameTToSSid"
                Alias       = "cnts"
                Description = "Name to SID"
                Category    = "util"
                Mod         = "DirTools"
                Status      = "bad"
            }), 
        $(New-Object -TypeName psobject -Property @{
                Name        = "Test-IsGuid"
                Alias       = "tig"
                Description = "Returns a value if argument fits GUID format"
                Category    = "util"
                Mod         = "DirTools"
                Status      = "good"
            }),  
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-HelpDeskAdminRole"
                Alias       = "gta"
                Description = "Get Get-HelpDeskAdminRole role"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-PasswordAdminRole"
                Alias       = "gta"
                Description = "Get Get-PasswordAdminRole role"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-UserAccountAdminRole"
                Alias       = "gta"
                Description = "Get UserAccountAdminRole role"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-IntuneAdminRole"
                Alias       = "gta"
                Description = "Get Get-IntuneAdminRole role"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-GroupsAdminRole"
                Alias       = "gta"
                Description = "Get Get-GroupsAdminRole role"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-GlobalAdminRole"
                Alias       = "gta"
                Description = "Get Get-GlobalAdminRole role"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-PrivilegedAdminRole"
                Alias       = "gta"
                Description = "Get Get-PrivilegedAdminRole role"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-UserRightsAssignments"
                Alias       = "gta"
                Description = "Get UserRightsAssignments role"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
  
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AADPrincipalMap"
                Alias       = "gapm"
                Description = "Build AAD principal map "
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-ProcessedData"
                Alias       = "gpd"
                Description = "Get processed data"
                Category    = "util"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AADOObject"
                Alias       = "gado"
                Description = "Get an AD Object "
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-DDomainSSID"
                Alias       = "gds"
                Description = "Get domain SID"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Invoke-AACLSScanner"
                Alias       = "ias"
                Description = "Scan AD"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AADOObjectIInfo"
                Alias       = "gaoi"
                Description = "Get info about AD object"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-DDomainOObject"
                Alias       = "gdomo"
                Description = "Get AAD domain object"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-PrivAuthAdministrator"
                Alias       = "gpaa"
                Description = "Get priv auth admin"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-ApplicationAdmins"
                Alias       = "gaa"
                Description = "Get application admins"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-CloudApplicationAdmins"
                Alias       = "gcaa"
                Description = "Get cloud app admins"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-VMs"
                Alias       = "gvm"
                Description = "Get VMs"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-DirectoryToolsCmdlets"
                Alias       = "gdtc"
                Description = "Get AdTool cmdlets"
                Category    = "util"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-GuidContext"
                Alias       = "ggc"
                Description = "Get guid context"
                Category    = "util"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-OnPremSearcher"
                Alias       = "gops"
                Description = "Get On-prem AD searcher"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-OnPremUser"
                Alias       = "gopu"
                Description = "Get On-prem AD user"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-Adsi"
                Alias       = "gadsi"
                Description = "Get an AD object using ADSI"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-ADTrusts"
                Alias       = "gadt"
                Description = "Get AD trusts for a domain or forest"
                Category    = "ad"
                Mod         = "DirTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "New-Output"
                Alias       = "nout"
                Description = "New output "
                Category    = "util"
                Mod         = "DirTools"
                Status      = "bad"
            }),

        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AzureKeyVaultApi"
                Alias       = "gakva"
                Description = "Make api call to Azure Key Vault api"
                Category    = "api"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AppToSpRelations"
                Alias       = "gaspr"
                Description = "Get app service principal relationships"
                Category    = "azhd"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-ScopeRequirements"
                Alias       = "gsr"
                Description = "Get scope requirements for resource"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "bad"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-RiskDetections"
                Alias       = "grd"
                Description = "Get Identity Protection risk detections"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-ConditionalAccessPolicies"
                Alias       = "gcap"
                Description = "Get ConditionalAccessPolicies"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            }),
        $(New-Object -TypeName psobject -Property @{
                Name        = "Get-KeyVaults"
                Alias       = "gkv"
                Description = "Get key vaults"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "bad"
            }),
	    $(New-Object -TypeName psobject -Property @{
                Name        = "Get-AccountStatus"
                Alias       = "gas"
                Description = "Get status of ad accounts"
                Category    = "aad"
                Mod         = "DirTools"
                Status      = "good"
            })
    )
    function Get-DirectoryTools {
        param(
            [string]$type = 'default'
        )

        Switch ($type) {

            'd' { Get-DirectoryToolsCmdlets 'ad' }
            'z' { Get-DirectoryToolsCmdlets 'aad' }
            'i' { Get-DirectoryToolsCmdlets 'api' }
            'a' { Get-DirectoryToolsCmdlets 'auth' }
            'h' { Get-DirectoryToolsCmdlets 'azhd' }
            'u' { Get-DirectoryToolsCmdlets 'util' }
            default {
      
                Write-Host "`n"
                Write-Host -Fore DarkCyan " ...:::" -NoNewLine; Write-Host "`tUsage:   " -NoNewLine;
                Write-Host -Fore DarkCyan "dt " -NoNewLine; Write-Host -Fore DarkGray "[category]"
                Write-Host "`n"

                [pscustomobject]$options = [ordered]@{
                    'd          ad '    = "On-prem AD"
                    'z          aad'    = "Azure AD"
                    'i          api'    = "Interact with Azure APIs"
                    'a          auth'   = "Authenticate"
                    'h          azhd'   = "For use with BloodHound"
                    'u          util'   = "Utilities/Helper functions"
                }

                $options.Keys | Foreach-Object {
                    $firstLetter = $_[0]
                    Write-Host -Fore DarkCyan "`tdt " -NoNewLine;Write-Host "$firstLetter" -Fore White -NoNewLine
                    Write-Host -Fore DarkGray "$($_.trim($firstLetter))" -NoNewLine ; Write-Host "`t$($options.Item("$_"))"
                }
                Write-Host "`n"
            }
        }
    }
    ############################

    #######  UTILITIES  ########

    ############################

    function Get-DirectoryToolsCmdlets {
        param(
            [string]$type
        )

        if (!$type) {
            $cmdlets = $DirectoryToolsCmdlets | Sort-Object Name
        }
        else {
            $cmdlets = $DirectoryToolsCmdlets | Where-Object Category -eq $type | Sort-Object Name
        }
        Write-Host "`n"
        Write-Host -Fore DarkGray ("`tCmdlet`t`t`t`t") -NoNewLine; Write-Host -Fore DarkGray " Alias`t`t" -Nonewline;
        Write-Host -Fore DarkGray " Description" 
        Write-Host "`t$('-' * 24)`t $('-' * 5)`t`t $('-' * 30)"
        $cmdlets | ForEach-Object {
            if ($_.Status -eq 'good') {
                Write-Host -Fore Green 'o' -NoNewLine
            }
            else {
                Write-Host -Fore Red 'x' -NoNewLine
            }
            if ($_.Name.Length -gt 23) {
                Write-Host -Fore Gray ("`t" + $_.Name + "`t ") -NoNewLine; Write-Host -Fore DarkCyan $_.Alias -Nonewline
                Write-Host -Fore Gray (" `t`t " + $_.Description ) 
            }    
            elseif ($_.Name.Length -gt 15) {
                Write-Host -Fore Gray ("`t" + $_.Name + "`t`t ") -NoNewLine; Write-Host -Fore DarkCyan $_.Alias -Nonewline
                Write-Host -Fore Gray (" `t`t " + $_.Description ) 
            }
            elseif ($_.Name.Length -gt 7) {
                Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t ") -NoNewLine; Write-Host -Fore DarkCyan $_.Alias -Nonewline
                Write-Host -Fore Gray (" `t`t " + $_.Description ) 
            }
            else {
                Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t`t ") -NoNewLine; Write-Host -Fore DarkCyan $_.Alias -Nonewline
                Write-Host -Fore Gray (" `t`t " + $_.Description ) 
            }
        }
        Write-Host "`n"
    }
    function Get-GuidContext {
        param(
            [string]$guid = "3f23a8ed-c477-4420-82dc-f110ef5bdd79"
        )

	    if ($app_ids | Where-Object AppId -eq $guid) {
            Write-Success 'Client app id matched'
            return ( $app_ids | Where-Object AppId -eq $guid )
        }
        else {
            Write-Information 'No client app ids matched'
            if (Get-App -prop $guid -o) {
                Write-Success 'Azure app id matched'
                return ( Get-App -prop $guid -0 )
            }
            else {
                Write-Information 'No azure app ids matched'
                if (Get-ServicePrincipal -prop $guid -o) {
                    Write-Success 'Service Principal app id matched'
                    return ( Get-ServicePrincipal -prop $guid -o )
                }
                else {
                    Write-Information 'No service principal ids matched' 
                    if (Get-MSGraphApi -api directoryObjects/$guid ) {
                        Write-Success 'Directory Object id matched'
                        return ( Get-MSGraphApi -api directoryObjects/$guid )
                    }
                    else {
                        Write-Information 'No directory object ids matched'
                    }
                }
            }
        }
    }
    function New-CodeChallenge {
        # Add-Type -AssemblyName System.Web
        # $RandomNumberGenerator = New-Object System.Security.Cryptography.RNGCryptoServiceProvider
        # $Bytes = New-Object Byte[] 32
        # $RandomNumberGenerator.GetBytes($Bytes)
        # ([System.Web.HttpServerUtility]::UrlTokenEncode($Bytes)).Substring(0, 43)
        return '4LWMlmN6LbduFBE3Q3MN4lLaZYzSkvm082PQR7EwwVU'
    }
    function Get-ProcessedData {
        Write-Host "Compressing files"
        $location = Get-Location
        $name = $date + "-azurecollection"
        If ($OutputDirectory.path -eq $location.path) {
            $jsonpath = $OutputDirectory.Path + [IO.Path]::DirectorySeparatorChar + "$date-*.json"
            $destinationpath = $OutputDirectory.Path + [IO.Path]::DirectorySeparatorChar + "$name.zip"
        }
        else {
            $jsonpath = $OutputDirectory + [IO.Path]::DirectorySeparatorChar + "$date-*.json"
            $destinationpath = $OutputDirectory + [IO.Path]::DirectorySeparatorChar + "$name.zip"
        }

        $error.Clear()
        try {
            Compress-Archive $jsonpath -DestinationPath $destinationpath
        }
        catch {
            Write-Host "Zip file creation failed, JSON files may still be importable."
        }
        if (!$error) {
            Write-Host "Zip file created: $destinationpath"
            rm $jsonpath
            Write-Host "Done! Drag and drop the zip into the BloodHound GUI to import data."
        }
    }

    ############################

    #########  JWT   ###########

    ############################

    function Get-ScopeRequirements {
        # RoleManagement.Read.Directory, Directory.Read.All, RoleManagement.ReadWrite.Directory, Directory.ReadWrite.All, Directory.AccessAsUser.All
        # RoleManagement.Read.Directory, Directory.Read.All, RoleManagement.ReadWrite.Directory, Directory.ReadWrite.All, Directory.AccessAsUser.All
    }

    ###############################################

    ###### Azure Principals and Resources #########

    ###############################################

    function Get-Org {
        param(
            [string]$orgName
        )
        if ($orgName) {
            $orgObjs | Where-Object OrgName -eq $orgname | Select-Object OrgName, TenantName, TenantId, StsHost 
        }
        else {
            $orgObjs | Select-Object OrgName, TenantName, TenantId, StsHost 
        }
    }
    function Set-Org ($component) {
        $selection = $orgObjs | Where-Object OrgName -eq $component

        Set-Variable -Name tenant_id -Value $selection.TenantId -Scope Global -Force 
        Set-Variable -Name account_id -Value $selection.AccountId -Scope Global -Force
        Set-Variable -Name OutputDirectory -Value "$env:USERPROFILE\$component" -Scope Global -Force
    }
    function Get-Tenant {
        [cmdletbinding()]param(
            [string]$prop,

            [switch]$hound,

            [switch]$o
        )

        if ($hound) {
            Get-MsGraphApi -api organization 
        }
        elseif (!$prop) {
            $global:tenants = Get-AzureCoreManagementApi -api "tenants" -var "tenants"
        }
        elseif (Test-IsGuid $prop) {
            Get-AzureCoreManagementApi -api "tenants/$prop"
        }
        else {
            try {
                $query = 'tenants?$filter=displayName eq ' + "'" + $prop + "'"
                Write-Debug $query
                $global:tenants = Get-AzureCoreManagementApi -api $query
            }
            catch {
                Write-Fail 'Couldnt make it out'
            }  
        }
        if ($tenants) {
            if ($o) {
                $tenants
            }
            else {
                $tenants | Select-String displayName, defaultDomain, domains
            }
        }
    }
    function Get-Sub {
        (Invoke-RestMethod -Uri "https://graph.microsoft.com/v1.0/subscriptions" -Headers @{'Authorization'="Bearer $msgraph_token"} ).value
    }
    function Test-IsGuid {
        [OutputType([bool])]
        param
        (
            [Parameter(Mandatory = $true)]
            [string]$ObjectGuid
        )
        [regex]$guidRegex = '(?im)^[{(]?[0-9A-F]{8}[-]?(?:[0-9A-F]{4}[-]?){3}[0-9A-F]{12}[)}]?$'
        return $ObjectGuid -match $guidRegex
    }
    function Select-Subscription {
        param(
            [string]$OutputDirectory
        )
        $global:AADSubscriptions = Get-Sub -TenantId $tenant_id
        $TotalCount = $AADSubscriptions.Count

        $Progress = 0
        $global:AADCurrentSubs = [System.Collections.ArrayList]::new()

        $AADSubscriptions | ForEach-Object {
            $Subscription = $_
            $DisplayName = $Subscription.Name

            $Current = [PSCustomObject]@{
                Name           = $Subscription.Name
                SubscriptionId = $Subscription.SubscriptionId
                TenantId       = $Subscription.TenantId
            }
            [void]$AADCurrentSubs.Add($Current)
        }
        if ($OutputDirectory) {
            New-Output -Coll $CurrentSubs -Type "subscriptions" -Directory $OutputDirectory
            Write-Host -Fore DarkCyan "[+] " -NoNewLine; Write-Host "Built `$CurrentSubs"
            Write-Host -Fore DarkCyan "[+] " -NoNewLine; Write-Host "Wrote azsubscriptions.json"
        }
        else {
            return $CurrentSubs
        }
    } 
    function Get-ResourceGroup {
        $global:AADCurrentResourceGroups = [System.Collections.ArrayList]::new()

        $AADSubscriptions | ForEach-Object {
            $SubDisplayName = $_.Name
            Select-Subscription -SubscriptionID $_.Id | Out-Null
            $AADResourceGroups = Get-ResourceGroup
            $TotalCount = $AADResourceGroups.Count
            $Progress = 0
            $AADResourceGroups | ForEach-Object {
                $RG = $_
                $DisplayName = $RG.ResourceGroupName
                $id = $RG.resourceid
                $resourceSub = "$id".split("/", 4)[2]
                $ResourceGroup = [PSCustomObject]@{
                    ResourceGroupName = $RG.ResourceGroupName
                    SubscriptionID    = $resourceSub
                    ResourceGroupID   = $RG.ResourceId
                }
                [void]$AADCurrentResourceGroups.Add($ResourceGroup)
            }
        }
        # New-Output -Coll $CurrentResourceGroups -Type "resourcegroups" -Directory $OutputDirectory
        Write-Success $CurrentResourceGroups.Count, 'objects saved as $CurrentResourceGroups'
    }
    function Get-AzureUser {

        param(
            [string]$prop,

            [switch]$raw,

            [switch]$all
        )

        Clear-Vars out 
        if (Test-IsGuid $prop) {
            $out = Get-MSGraphApi -api "users/$prop"
        }
        elseif ($prop -match '@') {
            try {
                $query = 'users?$filter=userPrincipalName eq ' + "'" + $prop + "'"
                $out = Get-MSGraphApi -api $query
            }
            catch {}  
        }
        else {
            try {
                $query = 'users?$filter=mailNickname eq ' + "'" + $prop + "'"
                $out = Get-MSGraphApi -api $query
            }
            catch {} 
        }
        if ($raw) {
            $out
        }
        else {
            Get-NonNullProperties $out
        }
    }

    function Get-Role {
        [cmdletbinding()]param(
            [string]$prop,

            [switch]$hound,

            [switch]$o
        )

        if ($hound) {
            $global:AADRoles = Get-MSGraphApi -api "directoryRoles"
            $result = $AADRoles
        }        
        elseif ($prop) {
            if (Test-IsGuid $prop) {
                $query = 'directoryRoles?$filter=roleTemplateId eq ' + "'" + $prop + "'"
                $result = Get-MSGraphApi -api $query
            }
            else {
                $query = 'directoryRoles?$filter=displayName eq ' + "'" + $prop + "'"
                $result = Get-MSGraphApi -api $query
            }
        }
        else {
            if ($AADRoles) {
                $result = $AADRoles | Where-Object roleTemplateId -eq $prop
            }
            else {
                $result = $AADRoles | Where-Object displayName -eq $prop
            }
        }
        if ($result) {
            if ($o) {
                return $result 
            }
            else {
                return $result | Select-Object displayName, description
            }
        }
    }
    function Get-RoleTemplate {
        [cmdletbinding()]param(
            [string]$prop,

            [switch]$o
        )
        if ($AADRoleTemplates) {
            $result = $AADRoleTemplates
            if (Test-IsGuid $prop) {
                $result = $result | Where-Object roleTemplateId -eq $prop
            }
            else {
                $result = $result | Where-Object displayName -eq $prop
            }
        }
        else {
            if (!$prop) {
                $global:AADRoleTemplates = Get-MSGraphApi -api "directoryRoleTemplates"
                $result = $AADRoleTemplates
            }
            elseif (Test-IsGuid $prop) {
                $query = 'directoryRoleTemplates?$filter=roleTemplateId eq ' + "'" + $prop + "'"
                $result = Get-MSGraphApi -api $query
            }
            else {
                $query = 'directoryRoleTemplates?$filter=displayName eq ' + "'" + $prop + "'"
                $result = Get-MSGraphApi -api $query
            }
        }  
        if ($result) {
            if ($o) {
                return $result 
            }
            else {
                return $result | Select-Object displayName, description
            }
        }
    }
    function Get-RoleMembers {
        [cmdletbinding()]param(
            [string]$prop
        )

        if (!$prop) {
            if (!$AADRoles) {
                $global:AADRoles = Get-Role -o 
            }
            Clear-Vars AADRoleMembers
            $global:AADRoleMembers = [System.Collections.ArrayList]::new()
    
            $progress = 0
            $TotalCount = $AADRoles.count 
    
            Foreach ($role in $AADRoles) {
      
                $progress += 1 
                Write-Progress -interval 1 -TotalCount $TotalCount -progress $progress -item 'roles'

                $rid = $role.id 
                $RoleMembers = Get-MSGraphApi -api "directoryRoles/$rid/members?`$select=userPrincipalName,displayName,id,onPremisesSecurityIdentifier"
                ForEach ($Member in $RoleMembers) {
                    $RoleMembership = [PSCustomObject]@{
                        memberName     = $Member.displayName
                        memberID       = $Member.id
                        memberOnPremID = $Member.onPremisesSecurityIdentifier
                        memberUPN      = $Member.userPrincipalName
                        memberType     = $Member.'@odata.type'
                        roleID         = $role.roleTemplateId
                        roleName       = $role.displayName
                    }
                    [void]$AADRoleMembers.Add($RoleMembership)
                }      
            }
            $AADRoleMembers | Select-Object roleName, memberName | Sort-Object roleName
            Write-Success $AADRoleMembers.Count, 'objects saved as $AADRoleMembers'
        }
        elseif (Test-IsGuid $prop) {
            $query = "directoryRoles/$prop/members"
            Get-MSGraphApi -api $query
        }
        else {
            try {
                $results = $AADRoles | Where-Object displayName -match $prop 
                if ($results.count -gt 1) {
                    Write-Host 'More than one match:'
                    $results
                    break
                }
                else {
                    $rid = $results.id 
                    $query = "directoryRoles/$rid/members"
                    Get-MSGraphApi -api $query
                }
            }
            catch {}  
        }
    }
    function Get-UserRole {
        param(
            [switch]$hound
        )
        if ($hound) {
            if (!$AADRoles) {
                Get-Role
            }  
            if (!$AADRoleMembers) {
                Get-RoleMembers
            }
            if (!$AADUsers) {
                Get-AzureUser -hound 
            }
            $global:AADUsersAndRoles = [System.Collections.ArrayList]::new()
    
            ForEach ($User in $AADRoleMembers) {
                $CurrentUser = $User.memberID
                $CurrentObjectType = $User.memberType
                $CurrentUserName = $User.memberName
                $CurrentAADUserRoles = ($AADRoleMembers | Where-Object { $_.memberID -eq $CurrentUser }).RoleID
                $CurrentUserUPN = $User.memberUPN
                $CurrentUserOnPremID = $User.memberOnPremID

                $UserAndRoles = [PSCustomObject]@{
                    UserName     = $CurrentUserName
                    ObjectType   = $CurrentObjectType
                    UserID       = $CurrentUser
                    UserOnPremID = $CurrentUserOnPremID
                    UserUPN      = $CurrentUserUPN
                    RoleID       = $CurrentAADUserRoles
                }
                [void]$AADUsersAndRoles.Add($UserAndRoles)
            }
            Write-Success $AADUsersAndRoles.Count, 'objects saved as $AADUsersAndRoles'

            $global:AADUserRoles = $AADUsersAndRoles | Sort-Object -Unique -Property UserName 
            $global:AADUsersWithRoles = $AADUserRoles.UserID
            Write-Success $AADUsersWithRoles.Count, 'objects saved as $AADUsersWithRoles'

            $global:AADUsersWithoutRoles = $AADUsers | Where-Object { $_.ID -NotIn $AADUsersWithRoles }
            Write-Success $AADUsersWithoutRoles.Count, 'objects saved as $AADUsersWithoutRoles'

            $global:AADPrivAuthAdmins = $AADUserRoles | Where-Object { $_.RoleID -Contains '7be44c8a-adaf-4e2a-84d6-ab2649e08a13' }
            $global:AADAuthenticationAdmins = $AADUserRoles | Where-Object { $_.RoleID -Contains 'c4e39bd9-1100-46d3-8c65-fb160da0071f' }
            $global:AADHelpdeskAdmins = $AADUserRoles | Where-Object { $_.RoleID -Contains '729827e3-9c14-49f7-bb1b-9608f156bbb8' }
            $global:AADPasswordAdmins = $AADUserRoles | Where-Object { $_.RoleID -Contains '966707d0-3269-4727-9be2-8c3a10f19b9d' }
            $global:AADUserAccountAdmins = $AADUserRoles | Where-Object { $_.RoleID -Contains 'fe930be7-5e62-47db-91af-98c3a49a38b1' }
            $global:AADIntuneAdmins = $AADUserRoles | Where-Object { $_.RoleID -Contains '3a2c62db-5318-420d-8d74-23affee5d9d5' }
            $global:AADGroupsAdmins = $AADUserRoles | Where-Object { $_.RoleID -Contains 'fdd7a751-b60b-444a-984c-02652fe8fa1c' }
            $global:AADGlobalAdmins = $AADUserRoles | Where-Object { $_.RoleID -Contains '62e90394-69f5-4237-9190-012177145e10' }
            $global:AADPrivilegedRoleAdmins = $AADUserRoles | Where-Object { $_.RoleID -Contains 'e8611ab8-c189-46e8-94e1-60213ab1f814' }
            $global:AADAppAdmins = $AADUserRoles | Where-Object { $_.RoleID -Contains '9b895d92-2cd3-44c7-9d02-a6ac2d5ea5c3' }
            $global:AADCloudAppAdmins = $AADUserRoles | Where-Object { $_.RoleID -Contains '158c047a-c907-4556-b7ef-446551a6b5f7' }
            $global:AADAppsWithAppAdminRole = ForEach ($SP in $SPsWithAzureAppAdminRole) {
                $AppWithRole = $AADServicePrincipals | ? { $_.ServicePrincipalID -Match $SP.UserID }
                $AppWithRole
            }
            $global:AADSPsWithAzureAppAdminRole = $AADUserRoles | Where-Object { ($_.RoleID -match '158c047a-c907-4556-b7ef-446551a6b5f7' -or $_.RoleID -match '9b895d92-2cd3-44c7-9d02-a6ac2d5ea5c3' ) -and ($_.UserType -match 'serviceprincipal') }
            $global:AADCloudGroups = $AADGroups | Where-Object { $_.OnPremisesSecurityIdentifier -eq $null } | Select-Object DisplayName, ObjectID
    
            if ($AADPrivAuthAdmins) { Get-PrivAuthAdministrator }
            if ($AADAuthenticationAdmins) { Get-AuthenticationAdminRole }
            if ($AADHelpdeskAdmins) { Get-HelpDeskAdminRole }
            if ($AADPasswordAdmins) { Get-PasswordAdminRole }
            if ($AADUserAccountAdmins) { Get-UserAccountAdminRole }
            if ($AADIntuneAdmins) { Get-IntuneAdminRole }
            if ($AADGroupsAdmins) { Get-GroupsAdminRole }
            if ($AADGlobalAdmins) { Get-GlobalAdminRole }    
            if ($AADAppAdmins) { Get-AppAdminRole }    
            if ($AADCloudAppAdmins) { Get-CloudAppAdminRole }    
            if ($AADPrivilegedRoleAdmins) { Get-PrivilegedAdminRole }
            if ($AADAppsWithAppAdminRole) { Get-ApplicationAdmins }
        }
        else {
            if (!$prop) {
                Get-Role
            }
            elseif (Test-IsGuid $prop) {
                Get-MSGraphApi -api "users/$prop"
            }
            elseif ($prop -match '@') {
                try {
                    $query = 'users?$filter=userPrincipalName eq ' + "'" + $prop + "'"
                    Write-Debug $query
                    Get-MSGraphApi -api $query
                }
                catch {}  
            }
            else {
                Write-Fail 'Couldnt make it out'
            }
        }
    }


    function Get-App {
        [cmdletbinding()]param(
            [string]$prop,

            [switch]$o
        )

        if (!$prop) {
            if ($AADapps) {
                $result = $AADapps
            }
            else {
                $global:AADApps = Get-MSGraphApi -api "applications"
                $result = $AADApps
            }
        }
        elseif (Test-IsGuid $prop) {
            $result = Get-MSGraphApi -api "applications/$prop"
        }
        else {
            try {
                $query = 'applications?$filter=displayName eq ' + "'" + $prop + "'"
                $result = Get-MSGraphApi -api $query
                Write-Debug $query
            }
            catch {}  
        }
        if ($result) {
            if ($o) {
                return $result 
            }
            else {
                return $result | Select-Object displayName, publisherDomain, signinaudience
            }
        }
    }
    function Get-AppOwner {
        [cmdletbinding()]param(
            [string]$prop,

            [switch]$hound
        )
  
        if ($hound) {
            if (!$AADApps) {
                $global:AADApps = Get-App -o 
            }
            Clear-Vars AADAppOwners
            $global:AADAppOwners = [System.Collections.ArrayList]::new()
            $Progress = 0
            $TotalCount = $apps.Count 

            $apps | ForEach-Object {
                $AppId = $_.appId 
                $AppObjectId = $_.id 
                $AppName = $_.displayName 

                $global:Owners = Get-MSGraphApi -api "applications/$AppObjectId/owners?`$select=userPrincipalName,displayName,id"

                ForEach ($Owner in $Owners) {
                    if ($Owner.userPrincipalName) {
                        $AADAppOwner = [PSCustomObject]@{
                            appId       = $AppId
                            appObjectId = $AppObjectId
                            appName     = $AppName
                            ownerID     = $Owner.id
                            ownerName   = $Owner.displayName 
                            ownerType   = $Owner.'@odata.type'
                            ownerUPN    = $Owner.userPrincipalName
                        }
                        [void]$AADAppOwners.Add($AADAppOwner)
                    }
                }
            }
            Write-Success $AADAppOwners.Count, 'objects saved as $AADAppOwners'
        }
    }
    function Get-ServicePrincipal {
	    [cmdletbinding()]param(
		[string]$prop,

		[switch]$o,

		[switch]$hound
	    )

	    if ($hound) {
		$SPOS = Get-MSGraphApi -y -api "servicePrincipals?`$select=id,appId,appDisplayName,servicePrincipalType"

		$global:AADServicePrincipals = [System.Collections.ArrayList]::new()

		$SPOS | Foreach-Object { 
		    $ServicePrincipal = [PSCustomObject]@{
			AppId                = $_.appId 
			AppName              = $_.appDisplayName
			ServicePrincipalId   = $_.id
			ServicePrincipalType = $_.servicePrincipalType
		    }
		    [void]$AADServicePrincipals.Add($ServicePrincipal)
		}
		Write-Success $AADServicePrincipals.Count, 'objects saved as $AADServicePrincipals'
	    }
	    elseif ($prop) {
		if (Test-IsGuid $prop){
		    $query = 'servicePrincipals?$filter=roleTemplateId eq ' + "'" + $prop + "'"
		    Get-MSGraphApi -api $query
		}
		else {
		    try {
			$query = 'servicePrincipals?$filter=displayName eq ' + "'" + $prop + "'"
			Get-MSGraphApi -api $query
			Write-Debug $query
		    }
		    catch {
		    }  
		}  
	    }
	    else {
		$global:AADServicePrincipals = Get-MSGraphApi -api servicePrincipals -y
	    }
	    if ($o) {
		return $AADServicePrincipals 
	    }
	    else {
		Write-Success $AADServicePrincipals.Count, 'objects saved as $AADServicePrincipals'
	    }   
    }

    function Get-SPsWithoutRoles {
        $global:AADPrincipalRoles = [System.Collections.ArrayList]::new()
  
        ForEach ($User in $AADRoleMembers) {
            $SPRoles = New-Object PSObject
            If ($User.MemberType -match 'servicePrincipal') {
                $SPRoles = [PSCustomObject]@{
                    RoleID = $User.RoleID
                    SPId   = $User.MemberID
                }
                $AADPrincipalRoles.Add($SPRoles)
            }
        }
        $AADServicePrincipals | Where-Object { $_.ServicePrincipalID -notin $AADPrincipalRoles.SPId } | Foreach-Object {
            [void]$AADSPsWithoutRoles.Add($_)
        }
    }
    function Get-Group {
        [cmdletbinding()]param(
            [string]$prop,

            [switch]$hound
        )

        if ($hound) {
            if (Test-IsJWTValid $msgraph_token){
                            
                Clear-Vars AADGroups

                $global:AADGroups = Get-MSGraphApi -y -api "groups?`$select=displayName,onPremisesSecurityIdentifier,Id" -var "AADGroups"
                $AADGroups | ForEach-Object {
                    $_ | Add-Member -NotePropertyName tenantId -NotePropertyValue $tenant_id 
                }
                Write-Success $AADGroups.Count, 'objects saved as $AADGroups'
            }
            else {
                Write-Fail 'Graph token is not valid'
                Break
            }
        }
        elseif (!$prop) {
            $global:AADGroups = Get-MSGraphApi -api "groups"
        }
        elseif (Test-IsGuid $prop) {
            Get-MSGraphApi -api "groups/$prop"
        }
        else {
            $query = 'groups?$filter=displayName eq ' + "'" + $prop + "'"
            $res = Get-MSGraphApi -api $query
            if ($res) {
                return $res
            }
            else {
                $obj = Get-AADOObject -Name $prop 
                if (!$o) {
                    $groups = ($obj.memberof | Select-String 'CN=([^,]+)').Matches.Captures | Foreach-Object { $_.Groups[1].Value }
                    $members = $obj.member | Foreach-Object {
          ($_ -split '=')[1].trim(' ,') -replace ',OU', "" -replace '\\', ''  
                    }
                    $main = $obj | Select-Object samaccountname, description, objectsid, whenchanged
                    $main | Add-Member -NotePropertyName memberOf -NotePropertyValue $($groups -join "`n") -Force 
                    $main | Add-Member -NotePropertyName members -NotePropertyValue $($members -join "`n") -Force 
                    $main
                }
                else {
                    $obj
                }
            }  
        }
    }
    function Get-GroupOwner {
        [cmdletbinding()]param(
            [string]$prop,

            [switch]$hound
        )
  
        if ($hound) {
            if (!$AADGroups) {
                $global:AADGroups = Get-Group -hound -o 
            }  
            $global:AADTargetGroups = $AADGroups | ? { $_.OnPremisesSecurityIdentifier -eq $null }
            $global:AADGroupOwners = [System.Collections.ArrayList]::new() 
            $Progress = 0
            $TotalCount = $AADTargetGroups.Count 

            $AADTargetGroups | ForEach-Object {
                $Progress += 1
                $CurrentSpot = $Progress / 100
                if ($CurrentSpot -notmatch "\.") {
                    $ProgressPercentage = ($Progress / $TotalCount) * 100 -As [Int]
                    Write-Host -Fore DarkCyan " ..:::`t" -NoNewLine; Write-Host "Processing group owners:`t[${Progress}/${TotalCount}]`t[" -NoNewLine
                    Write-Host -Fore DarkCyan "${ProgressPercentage}%" -NoNewLine
                    Write-Host "]"
                }
                $GroupId = $_.id 
                $GroupName = $_.displayName 

                $Owner = Get-MSGraphApi -api "groups/$GroupId/owners?`$select=displayName,id,onPremisesSecurityIdentifier"

                $CurrentGroupOwner = @{
                    groupName     = $GroupName
                    groupId       = $GroupId
                    ownerName     = $Owner.displayName
                    ownerId       = $Owner.id
                    ownerType     = $Owner.'@odata.type'
                    ownerOnPremId = $Owner.onPremisesSecurityIdentifier
                }
                [void]$AADGroupOwners.Add($CurrentGroupOwner)
            }
            Write-Success $AADGroupOwners, 'objects saved as $AADGroupOwners'
        }
        elseif (!$prop) {
            Write-Fail 'Need to provide a device id'
        }

        elseif (Test-IsGuid $prop) {
            $result = Get-MSGraphApi -api "groups/$prop/owners"
            return $result
        }
        else {
            try {
                $devid = (Get-Group $prop).id
                $result = Get-MSGraphApi -api "groups/$gid/owners"
                return $result
            }
            catch {}  
        }
    }
    function Get-GroupMembers {
        [cmdletbinding()]param(
            [string]$prop,

            [switch]$hound
        )
  
        if ($hound) {
            if (!$AADGroups) {
                $global:AADGroups = Get-Group -hound -o 
            }  
            $Progress = 0  
            $TotalCount = $AADGroups.Count 

            Clear-Vars AADGroupMembers
            $global:AADGroupMembers = [System.Collections.ArrayList]::new()

            $AADGroups | ForEach-Object {
                $Progress += 1
                $CurrentSpot = $Progress / 100
                if ($CurrentSpot -notmatch "\.") {
                    $ProgressPercentage = ($Progress / $TotalCount) * 100 -As [Int]
                    Write-Host -Fore DarkCyan " ..:::`t" -NoNewLine; Write-Host "Processing group members:`t[${Progress}/${TotalCount}]`t[" -NoNewLine
                    Write-Host -Fore DarkCyan "${ProgressPercentage}%" -NoNewLine
                    Write-Host "]"
                }
                $GroupId = $_.id 
                $GroupName = $_.displayName 
                $GroupOnPremId = $_.onPremisesSecurityIdentifier

                $Members = Get-MSGraphApi -y -q -api "groups/$GroupId/members?`$select=displayName,id,onPremisesSecurityIdentifier"
                foreach ($Member in $Members) {
                    $CurrentGroupMember = [PSCustomObject]@{
                        groupName      = $GroupName
                        groupID        = $GroupId
                        groupOnPremID  = $GroupOnPremId 
                        memberName     = $Member.displayName
                        memberID       = $Member.id
                        memberType     = $Member.'#odata.type'
                        memberOnPremID = $Member.onPremisesSecurityIdentifier
                    }
                    [void]$AADGroupMembers.Add($CurrentGroupMember)
                }
            }
        }
        elseif (!$prop) {
            Write-Fail 'Need to provide a group id'
        }

        elseif (Test-IsGuid $prop) {
            $result = Get-MSGraphApi -api "groups/$prop/members"
            return $result
        }
        else {
            Get-NNetGGroupMMember $prop -FullData 
        }
    }
    function Get-AzureApi {
        param(
            [string]$match
        )
        if ($match){
            $apis | Where-Object { ($_.ApiName -match $match) -or ($_.ApiHost -match $match)} | Select-Object ApiName,VarName,ApiHost | Format-Table -Auto
        }
        else {
            $apis | Select-Object ApiName,VarName,ApiHost | Format-Table -Auto
        }
    }
    function Get-Dev {
        [cmdletbinding()]param(
            [Parameter(ValueFromPipeline = $true )]
            [string]$prop,

            [switch]$hound
        )
  
        if ($hound) {
            $global:AADDevices = Get-MSGraphApi -api "devices?`$select=displayName,deviceId,id,operatingSystem" -y 
            Write-Success $AADDevices.Count, 'objects saved as $AADDevices'
        }
        elseif (!$prop) {
            Get-MSGraphApi -api "devices"
        }
        elseif (Test-IsGuid $prop) {
            Get-MSGraphApi -api "devices(deviceId='$prop')"
        }
        else {
            $query = 'devices?$filter=displayName eq ' + "'" + $prop + "'"
            $res = Get-MSGraphApi -api $query
            if ($res) {
                $res
            }
            else {
                $obj = Get-NNetCComputer $prop
                if (!$o) {
                    $spns = $obj.serviceprincipalname | Where-Object { $_ -notmatch '\.' }
                    $main = $obj | Select-Object samaccountname, dnshostname, description, objectsid, pwdlastset, whenchanged, lastlogontimestamp, operatingsystem
                    $main | Add-Member -NotePropertyName memberOf -NotePropertyValue $($spns -join "`n") -Force 
                    $main
                }
                else {
                    $obj
                }
            }  
        }
    }
    function Get-DevOwner {
        [cmdletbinding()]param(
            [string]$prop,

            [switch]$hound,
	    
	    [switch]$o
        )
  
        if ($hound) {
            if (!$AADDevices) {
                Write-Fail '$AADDevices not found'
                Break
            }  

            Clear-Vars AADDeviceOwners 
            $global:AADDeviceOwners = [System.Collections.ArrayList]::new()
    
            $progress = 0
            $TotalCount = $AADDevices.Count 


            $AADDevices | ForEach-Object {
      
                $progress += 1 
                Write-Progress -interval 100 -TotalCount $TotalCount -progress $progress -item 'devices'

                if (!(Test-IsJWTValid $msgraph_token)) {
                    try {
                        Get-NewAccessToken msgraph
                    }
                    catch {
                        $notoken = 1
                        Write-Fail 'Unable to retrieve new token'
                    }
                }
                $d = $_.id 
                $Owner = Get-MSGraphApi -api "devices/$d/registeredOwners?`$select=displayName,id,onPremisesSecurityIdentifier"
      
                $AzureDeviceOwner = [PSCustomObject]@{
                    DeviceDisplayname = $_.displayName 
                    DeviceID          = $_.deviceId
                    DeviceOS          = $_.operatingSystem
                    OwnerDisplayName  = $Owner.displayName
                    OwnerID           = $Owner.id
                    OwnerType         = $Owner.'@odata.type'
                    OwnerOnPremID     = $Owner.onPremisesSecurityIdentifier
                }
                [void]$AADDeviceOwners.Add($AzureDeviceOwner)
            }
            Write-Success $AADDeviceOwners.Count, 'objects saved as $AADDeviceOwners'
        }

        elseif (!$prop) {
            Write-Fail 'Need to provide a device id'
        }

        elseif (Test-IsGuid $prop) {
            $result = Get-MSGraphApi -api "devices/$prop/registeredOwners"
            return $result
        }
        else {
            try {
                $devid = (Get-Dev $prop).id
                $result = Get-MSGraphApi -api "devices/$devid/registeredOwners"
                
		if ($o) {
		    return $result
		}
		else {
		    $result | Select-String userPrincipalName,displayName,jobTitle,officeLocation
		}
            }
            catch {}  
        }
    }
    function Get-Scope {
        [cmdletbinding()]param(
            [string]$prop,
            [switch]$new
        )
        if ($prop -and $sar){
            if ($o){
                $sar | Where-Object value -match $prop
            }
            else {
                $sar | Where-Object value -match $prop | Select-Object value,name | Sort Value 
            }
        }
        if ($new) {
            Clear-Vars sar
            $global:sar = [System.Collections.ArrayList]::new()

            if (!$prop) {
                if (!$AADServicePrincipals) {
                    Get-ServicePrincipal
                }
                $targetSps = $AADServicePrincipals | Where-Object { ($_.appRoles) -or ($_.publicPermissionScopes) }
                foreach ($sp in $targetSps) {
                    $scopes = $sp.publishedPermissionScopes 
                    $appRoles = $sp.appRoles 
                    $servicePrincipal = $sp.displayName

                    if ($scopes) {
                        foreach ($scope in $scopes) {
                            $obj = [pscustomobject]@{
                                sp          = $servicePrincipal 
                                name        = $scope.adminConsentDisplayName
                                description = $scope.adminConsentDescription  
                                id          = $scope.id
                                value       = $scope.value
                                enabled     = $scope.isEnabled 
                                type        = ("scope:" + $scope.type)
                            }
                            [void]$sar.Add($obj) 
                        }
                    }
                    if ($appRoles) {
                        foreach ($appRole in $appRoles) {
                            $obj = [pscustomobject]@{
                                sp          = $servicePrincipal 
                                name        = $appRole.displayName
                                description = $appRole.description  
                                id          = $appRole.id
                                value       = $appRole.value
                                enabled     = $appRole.isEnabled
                                type        = "appRole" 
                            }
                            [void]$sar.Add($obj)
                        }
                    }
                }
            }
            else {
                if (Test-IsGuid $prop) {
                    $targetSps = Get-MSGraphApi -api "servicePrincipals/$prop"
                }
            }
        }
    }
    
    ############################
  
    ##########  API  ###########
  
    ############################

    function Get-ApiEndpoints {
        param($aud)

        if (!$aud) {
            Write-Fail 'Choose an endpoint:'
            $apisList.ApiHost
            Write-Host `n
            Break
        }
        $items = ( $apisList | Where-Object ApiHost -eq $aud ).Endpoints
  
        Write-Host "`n   API operations for: " -NoNewline; Write-Host -Fore DarkCyan "$aud`n"    
        $items | Foreach-Object {
            Write-Host "`t$_"
        }
        Write-Host `n
    }
    function Use-AzureRole {
        param(
            [string]$roleToAssume
        )
        $id = (Get-Role $roleToAssume).roleTemplateId 

        $postdata = @{roleTemplateId = $id } | ConvertTo-Json
        Get-MSGraphApi -tok $aadgraph_token -api directoryRoles -postdata $postdata
    }
    function Get-Api {
        [cmdletbinding()]param(
            [parameter(Position = 0)]
            [string]$api,

            [string]$tok = $msgraph_token,
            $postdata,
            [switch]$raw,
            [switch]$pat,
            [switch]$nopaging
        )

        Write-Debug ( 'API: ' + $api )

        if ( $PSBoundParameters.containskey("debug") ) {
            if ( $debug = [bool]$PSBoundParameters.item("debug") ) { 
                $DebugPreference = "Continue"
            }
        }

        if ($pat) {
            Write-Debug 'Token is a PAT'
            $B64 = [Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($cur_upn + ":" + $pat))
            $headers = @{Authorization = "Bearer $B64" }
            Write-Debug ('Headers: ' + $headers.ToString())
            Break
        }

        try {
            $token_details = Read-Token $tok -e
            $aud = ($token_details.aud -split '/')[2]
            $aud = $aud.trim('/')
            # $varName    = ( $apisList | Where-Object ApiHost -eq $aud ).VarName 
            if (!(Test-IsJWTValid $tok)) {
                Write-Fail 'JWT not found'
                Break
            }

            Write-Debug ('Headers: ' + $headers.Keys)
        }
        catch {
            break
        }

        if ($api.split('?')[1]) {
            Write-Debug "Filter operation"
            $filter = $true
        }
  
        Write-Debug ('Audience: ' + $aud ) 
        $apiRes = ''
        switch ($aud) {
            "graph.windows.net" { Get-AADGraphApi }
            "app.vssps.visualstudio.com" { Get-AzureDevOpsApi }
            "management.core.windows.net" { Get-AzureCoreManagementApi }
            "management.azure.com" { Get-AzureServiceManagerApi }
            "endpoint.microsoft.com" { Get-IntuneApi }
            "5e41ee74-0d2d-4a72-8975-998ce83205eb-my.sharepoint.com" { Get-OneDriveApi }
            "5e41ee74-0d2d-4a72-8975-998ce83205eb.sharepoint.com" { Get-SharepointApi }
            "mysignins.microsoft.com" { Get-MySignInsApi }
            "officeapps.live.com" { Get-OfficeAppsApi }
            "outlook.office365.com" { Get-MSExchangeApi }
            "api.spaces.skype.com" { Get-MSTeamsApi }
            "api.diagnostics.office.com" { Get-SARAApi }
            "vault.azure.net" { Get-AzureKeyVaultApi }
            default { Get-MSGraphApi $api }

        }

        if ($postdata) {
    
            $dataType = $postdata.getType().Name
            Write-Debug ('postdata type is: ' + $dataType )
            if ($dataType -ne 'String') {
                try {
                    $global:postdata = $postdata | ConvertTo-Json
                }
                catch {
                    Write-Fail 'Could not convert', '`$postdata', 'to a string'
                    Break
                }
            }
            Write-Debug ('Post data: ' + $postdata )      
            Write-Debug $url
            Write-Debug $headers 
            Write-Debug 'requesting'
            Invoke-RestMethod -Uri $url -Headers $headers -Method POST -Body $postdata -ContentType 'application/json'
            Write-Debug 'done'
            Break
        } 
  
        if ($raw) {
            return $apiRes
        }
        else {
            try {
                Write-Debug $url 
         

            }
            catch {}
        }
        return $results
    }
    function Get-MSGraphApi {
    
        [cmdletbinding()]param(
            [parameter(Position = 0)]
            [string]$api,

            [string]$tok = $msgraph_token,

            [string]$ver = "beta",    

            [string]$var = "output",

            [switch]$raw,

            [switch]$y,

            [switch]$q       
        )
  
        if ( $PSBoundParameters.containskey("debug") ) {
            if ( $debug = [bool]$PSBoundParameters.item("debug") ) { 
                $DebugPreference = "Continue"
            }
        }
  
        if (!(Test-IsJWTValid $tok)) {
            Get-AccessToken msgraph
            $tok = $msgraph_token
        }
  

        $headers = @{Authorization = "Bearer $tok" }
        $c_headers = @{Authorization = "Bearer $tok"; ConsistencyLevel = "eventual" }
        $url = "https://graph.microsoft.com/$ver/$api" 
  
        Write-Debug ('Url:   ' + $url )
        Clear-Vars apiOut

        try {
            $global:apiOut = Invoke-RestMethod -Uri $url -Headers $headers
        }
        catch {
            return 0
        }

        if ($raw) {
            return $apiOut
        }
        elseif ($apiOut.'@odata.nextLink') {
            $apiPath = ($url -split '\?')[0]
            $apiNode = ($apiPath -split '/')[-1]
            $count_url = ($apiPath + "/`$count")

            $TotalCount = Invoke-RestMethod -Uri $count_url -headers $c_headers
    
            if (!$y) {
                $default = "N"
                if (!($value = Read-Host "Get all $TotalCount $apiNode? [y/$default]")) {
                    $value = $default 
                }
                if (($value -eq 'N') -or ($value -eq 'n')) {
                    Break
                }
            }
  
            $results = @()     
            $progress = 0
    
            while ($apiOut.'@odata.nextLink') {
                $progress += 100
                $results += $apiOut.Value
                Write-Progress -interval 1000 -TotalCount $TotalCount -progress $progress -item $apiNode 
                $apiOut = Invoke-RestMethod -Uri $apiOut.'@odata.nextLink' -Headers $headers 
            }
            $results += $apiOut.Value
            return $results
        }
        else {
            if (($apiOut | gm).Name -contains 'value') { 
                Write-Debug 'has value field'
                return $apiOut.value 
            }
            else { 
                Write-Debug 'no value field'
                $apiOut 
            }
        }
    }
    function Get-AADGraphApi ($api) {
        switch ($filter) {
            $True { $ver = "&api-version=1.6" }
            default { $ver = "?api-version=1.6" }
        }
        if ($api -eq 'roles') { $api = 'directoryRoles' }
        if ($api -ne "me") {
            $api = "myorganization/" + $api
        }

        $url = "https://" + $aud + "/" + $api + $ver 
        Invoke-RestMethod -Uri $url -Headers $headers
    }
    function Get-AzureDevOpsApi {} 
    function Get-AzureCoreManagementApi {
        param(
            [string]$api,

            [string]$ver = "2020-01-01",

            [switch]$y
        )
  
        if (!(Test-IsJWTValid $azcoremgmt_token)) {
            Get-AccessToken azcoremgmt
        }
  
        $base = "https://management.azure.com/"
        if ($api -match '\?') {
            $query = "$api" + '&api-version=' + "$ver"
        }
        else {
            $query = "$api" + '?api-version=' + "$ver"
        }
        $url = $base + $query
        $headers = @{Authorization = "Bearer $azcoremgmt_token" }

        $apiOut = Invoke-RestMethod -Uri $url -headers $headers 
        if ($apiOut.value) { 
            Write-Debug 'has value field'
            return $apiOut.value 
        }
        else { 
            Write-Debug 'no value field'
            $apiOut 
        }
    }
    function Get-AzureServiceManagerApi {}
    function Get-IntuneApi {}
    function Get-OneDriveApi {}
    function Get-SharepointApi {}
    function Get-MySignInsApi {}
    function Get-OfficeAppsApi {
        $base = "https://ocws.officeapps.live.com/ocs/v2/"

        $apiOut = Invoke-RestMethod -Uri $url -headers $headers 
        if ($apiOut.value) { 
            Write-Debug 'has value field'
            return $apiOut.value 
        }
        else { 
            Write-Debug 'no value field'
            $apiOut 
        }
    }
    function Get-MSExchangeApi {}
    function Get-MSTeamsApi {}
    function Get-SARAApi {}
    function Get-AzureKeyVaultApi {}

    ###################################

    #########  AZURE HOUND  ###########

    ###################################



    function Get-HoundSummary {

        Write-Host " "
        if ($AADUsers){
            Write-Success "`t",$AADUsers.count, "`tUsers","`t`t`t`$AADUsers"
            Write-Success "`t",$AADUsersAndRoles.count, "`t`tUsers and roles","`t`$AADUsersAndRoles"
            Write-Success "`t",$AADUserRoles.count, "`t`tUser roles","`t`t`$AADUserRoles"
            Write-Success "`t",$AADUsersWithRoles.count, "`t`tUsers with roles","`t`$AADUsersWithRoles"
            # Write-Success "`t",$AADUsersWithoutRoles.count, "`t`tUsers w/o roles","`t`$AADUsersWithoutRoles"
        }
        Write-Host " "
        if ($AADGroups.count -gt 0) {

            Write-Success "`t",$AADGroups.count, "`tGroups","`t`t`$AADGroupMembers"
            if ($AADGroupOwners.count -gt 0) {

                Write-Success "`t",$AADGroupOwners.count, "`tGroup Owners","`t`t`$AADGroupOwners"
            }
            if ($AADGroupMembers.count -gt 0) {

                Write-Success "`t",$AADGroupMembers.count, "`tGroup Members","`t`t`$AADGroupMembers"
            }
        }
        Write-Host " "
        if ($AADDevices.count -gt 0) {

            Write-Success "`t",$AADDevices.count, "`tDevices","`t`t`$AADDevices"
            if ($AADDeviceOwners.count -gt 0) {

                Write-Success "`t",$AADDeviceOwners.count, "`tDevice Owners","`t`t`$AADDeviceOwners"
            }
        }

        Write-Host " "
        if ($AADAdmins.count -gt 0) {
            $roleAbuse       = ($RoleList | ? roleVar -eq '$AADAdmins').roleAbuse
            Write-Info "`t",$AADAdmins.count, "`t`tAdmins","`t`t`$AADAdmins",("`t" + $roleAbuse)
        }
        Write-Host " "
        if ($AADGroupAdmins.count -gt 0) {
            $roleAbuse       = ($RoleList | ? roleVar -eq '$AADGroupAdmins').roleAbuse
            Write-Info "`t",$AADGroupAdmins.count, "`t`tGroup Admins","`t`t`$AADGroupAdmins",("`t" + $roleAbuse)
        }
        if ($AADHelpDeskAdmins.count -gt 0) {
            $roleAbuse       = ($RoleList | ? roleVar -eq '$AADHelpdeskAdmins').roleAbuse
            Write-Info "`t",$AADHelpdeskAdmins.count, "`t`tHelpdesk Admins","`t`$AADHelpdeskAdmins",("`t" + $roleAbuse)
        }
        if ($AADPasswordAdmins.count -gt 0) {
            $roleAbuse       = ($RoleList | ? roleVar -eq '$AADPasswordAdmins').roleAbuse
            Write-Info "`t",$AADPasswordAdmins.count, "`t`tPassword Admins","`t`t`$AADPasswordAdmins",("`t" + $roleAbuse)
        }
        if ($AADPrivAuthAdmins.count -gt 0) {
            $roleAbuse       = ($RoleList | ? roleVar -eq '$AADPrivAuthAdmins').roleAbuse
            Write-Info "`t",$AADPrivAuthAdmins.count, "`t`tPrivAuth Admins","`t`t`$AADPrivAuthAdmins",("`t" + $roleAbuse)
        }
        if ($AADPrivRoleAdmins.count -gt 0) {
            $roleAbuse       = ($RoleList | ? roleVar -eq '$AADPrivRoleAdmins').roleAbuse
            Write-Info "`t",$AADPrivRoleAdmins.count, "`t`tPrivileged Role Admins","`$AADPrivRoleAdmins",("`t" + $roleAbuse)
        }
        if ($AADAuthAdmins.count -gt 0) {
            $roleAbuse       = ($RoleList | ? roleVar -eq '$AADAuthAdmins').roleAbuse
            Write-Info "`t",$AADAuthAdmins.count, "`t`tAuthentication Admins","`t`t`$AADAuthAdmins",("`t" + $roleAbuse)
        }
        if ($AADUserAccountAdmins.count -gt 0) {
            $roleAbuse       = ($RoleList | ? roleVar -eq '$AADUserAccountAdmins').roleAbuse
            Write-Info "`t",$AADUserAccountAdmins.count, "`t`tUserAccount Admins","`t`$AADUserAccountAdmins",("`t" + $roleAbuse)
        }
        if ($AADIntuneAdmins.count -gt 0) {
            $roleAbuse       = ($RoleList | ? roleVar -eq '$AADIntuneAdmins').roleAbuse
            Write-Info "`t",$AADIntuneAdmins.count, "`t`tIntune Admins","`t`t`$AADIntuneAdmins",("`t" + $roleAbuse)
        }
        if ($AADGlobalAdmins.count -gt 0) {
            $roleAbuse       = ($RoleList | ? roleVar -eq '$AADGlobalAdmins').roleAbuse
            Write-Info "`t",$AADGlobalAdmins.count, "`t`tGlobal Admins","`t`t`$AADGlobalAdmins",("`t" + $roleAbuse)
        }
        if ($AADAppAdmins.count -gt 0) {
            $roleAbuse       = ($RoleList | ? roleVar -eq '$AADAppAdmins').roleAbuse
            Write-Info "`t",$AADAppAdmins.count, "`t`tApp Admins","`t`t`$AADAppAdmins",("`t`t" + $roleAbuse)
        }
        if ($AADCloudAppAdmins.count -gt 0) {
            $roleAbuse       = ($RoleList | ? roleVar -eq '$AADCloudAppAdmins').roleAbuse
            Write-Info "`t",$AADCloudAppAdmins.count, "`t`tCloudApp Admins","`t`$AADCloudAppAdmins",("`t" + $roleAbuse)
        }
        Write-Host ""
    }

    function New-Output ($Coll, $Type, $Directory) {
        $Count = $Coll.Count
        Write-Host "Writing output for $($Type)"
        if ($null -eq $Coll) {
            $Coll = New-Object System.Collections.ArrayList
        }
        $date = get-date -f yyyyMMddhhmmss
        $Output = New-Object PSObject
        $Meta = New-Object PSObject
        $Meta | Add-Member Noteproperty 'count' $Coll.Count
        $Meta | Add-Member Noteproperty 'type' "az$($Type)"
        $Meta | Add-Member Noteproperty 'version' 4
        $Output | Add-Member Noteproperty 'meta' $Meta
        $Output | Add-Member Noteproperty 'data' $Coll
        $FileName = $Directory + "\" + $date + "-" + "az" + $($Type) + ".json"
        $Output | ConvertTo-Json | Out-File -Encoding "utf8" -FilePath $FileName  
    }
    function Get-PrivAuthAdministrator {
        $TotalCount = $AADPrivAuthAdmins.Count
        $global:AADPrivAuthAdminRights = [System.Collections.ArrayList]::new()
  
        ForEach ($User in $AADPrivAuthAdmins) {
            $TargetUsers = $AADUserRoles | Where-Object { $_.UserUPN -NotMatch "#EXT#" } | Where-Object { $_.ObjectType -Match "User" }
            ForEach ($TargetUser in $TargetUsers) {
                $PWResetRight = [PSCustomObject]@{
                    UserName           = $User.UserName
                    ObjectType         = $User.ObjectType
                    UserID             = $User.UserID
                    UserOnPremID       = $User.UserOnPremID
                    TargetUserName     = $TargetUser.UserName
                    TargetUserID       = $TargetUser.UserID
                    TargetUserOnPremID = $TargetUser.UserOnPremID
                }
                [void]$AADPrivAuthAdminRights.Add($PWResetRight)
            }   
            ForEach ($TargetUser in $AADUsersWithoutRoles) {
                $PWResetRight = [PSCustomObject]@{
                    UserName           = $User.UserName
                    ObjectType         = $User.ObjectType
                    UserID             = $User.UserID
                    UserOnPremID       = $User.UserOnPremID
                    TargetUserName     = $TargetUser.DisplayName
                    TargetUserID       = $TargetUser.ObjectId
                    TargetUserOnPremID = $TargetUser.OnPremisesSecurityIdentifier
                }     
                [void]$AADPrivilegedAuthentidationAdminRights.Add($PWResetRight)
            }
        }
        Write-Success $AADPrivAuthAdminRights.Count, 'objects saved as $AADPrivAuthAdminRights'
    }
    function Get-AuthenticationAdminRole {
        $TotalCount = $AADAuthenticationAdmins.Count
        $global:AADAuthAdminsRights = [System.Collections.ArrayList]::new()
  
        ForEach ($User in $AADAuthenticationAdmins) {
            $TargetUsers = $AADUserRoles | Where-Object { $AADAuthAdminsList -Contains $_.RoleID } | Where-Object { $_.UserUPN -NotMatch "#EXT#" } | Where-Object { $_.ObjectType -Match "User" }
            ForEach ($TargetUser in $TargetUsers) {
                $PWResetRight = [PSCustomObject]@{
                    UserName           = $User.UserName
                    ObjectType         = $User.ObjectType
                    UserID             = $User.UserID
                    UserOnPremID       = $User.UserOnPremID
                    TargetUserName     = $TargetUser.UserName
                    TargetUserID       = $TargetUser.UserID
                    TargetUserOnPremID = $TargetUser.UserOnPremID
                }
                [void]$AADAuthAdminsRights.Add($PWResetRight)
            }
            ForEach ($TargetUser in $AADUsersWithoutRoles) {
                $PWResetRight = [PSCustomObject]@{
                    UserName           = $User.UserName
                    ObjectType         = $User.ObjectType
                    UserID             = $User.UserID
                    UserOnPremID       = $User.UserOnPremID
                    TargetUserName     = $TargetUser.DisplayName
                    TargetUserID       = $TargetUser.ObjectId
                    TargetUserOnPremID = $TargetUser.OnPremisesSecurityIdentifier
                } 
                [void]$AADAuthAdminsRights.Add($PWResetRight)
            }
        }
        Write-Success $AADAuthAdminsRights.Count, 'objects saved as $AADAuthAdminsRights'
    }
    function Get-HelpDeskAdminRole {
        $TotalCount = $AADHelpdeskAdmins.Count
        $global:AADHelpdeskAdminsRights = [System.Collections.ArrayList]::new()
  
        ForEach ($User in $AADHelpdeskAdmins) {
            $TargetUsers = $AADUserRoles | Where-Object { $AADHelpdeskAdminsList -Contains $_.RoleID } | Where-Object { $_.UserUPN -NotMatch "#EXT#" } | Where-Object { $_.ObjectType -Match "User" }
            ForEach ($TargetUser in $TargetUsers) {
                $PWResetRight = [PSCustomObject]@{
                    UserName           = $User.UserName
                    ObjectType         = $User.ObjectType
                    UserID             = $User.UserID
                    UserOnPremID       = $User.UserOnPremID
                    TargetUserName     = $TargetUser.UserName
                    TargetUserID       = $TargetUser.UserID
                    TargetUserOnPremID = $TargetUser.UserOnPremID
                }
                [void]$AADHelpdeskAdminsRights.Add($PWResetRight)
            }
            ForEach ($TargetUser in $AADUsersWithoutRoles) {
                $PWResetRight = [PSCustomObject]@{
                    UserName           = $User.UserName
                    ObjectType         = $User.ObjectType
                    UserID             = $User.UserID
                    UserOnPremID       = $User.UserOnPremID
                    TargetUserName     = $TargetUser.DisplayName
                    TargetUserID       = $TargetUser.ObjectId
                    TargetUserOnPremID = $TargetUser.OnPremisesSecurityIdentifier
                }
                [void]$AADHelpdeskAdminsRights.Add($PWResetRight)
            }
        }
        Write-Success $AADHelpdeskAdminsRights.Count, 'objects saved as $AADHelpdeskAdminsRights'
    }
    function Get-PasswordAdminRole {

        $TotalCount = $PasswordAdmins.Count
        $global:AADPasswordAdminsRights = [System.Collections.ArrayList]::new()
  
        ForEach ($User in $PasswordAdmins) {
            $TargetUsers = $AADUserRoles | Where-Object { $AADPasswordAdminList -Contains $_.RoleID } | Where-Object { $_.UserUPN -NotMatch "#EXT#" } | Where-Object { $_.ObjectType -Match "User" }
            ForEach ($TargetUser in $TargetUsers) {
                $PWResetRight = [PSCustomObject]@{
                    UserName           = $User.UserName
                    ObjectType         = $User.ObjectType
                    UserID             = $User.UserID
                    UserOnPremID       = $User.UserOnPremID
                    TargetUserName     = $TargetUser.UserName
                    TargetUserID       = $TargetUser.UserID
                    TargetUserOnPremID = $TargetUser.UserOnPremID
                }
                [void]$AADPasswordAdminsRights.Add($PWResetRight)
            }
            ForEach ($TargetUser in $AADUsersWithoutRoles) {
                $PWResetRight = [PSCustomObject]@{
                    UserName           = $User.UserName
                    ObjectType         = $User.ObjectType
                    UserID             = $User.UserID
                    UserOnPremID       = $User.UserOnPremID
                    TargetUserName     = $TargetUser.DisplayName
                    TargetUserID       = $TargetUser.ObjectId
                    TargetUserOnPremID = $TargetUser.OnPremisesSecurityIdentifier
                }
                [void]$AADPasswordAdminsRights.Add($PWResetRight)
            }
            Write-Success $AADPasswordAdminsRights.Count, 'objects saved as $AADPasswordAdminsRights'

        }
    }
    function Get-UserAccountAdminRole {
        $TotalCount = $AADUserAccountAdmins.Count
        $Progress = 0

        $global:AADUserAccountAdminsRights = [System.Collections.ArrayList]::new()
  
        ForEach ($User in $AADUserAccountAdmins) {
            $DisplayName = $User.UserName
            $AADTargetUsers = $AADUserRoles | Where-Object { $AADUserAdminList -Contains $_.RoleID } | Where-Object { $_.UserUPN -NotMatch "#EXT#" } | Where-Object { $_.ObjectType -Match "User" }
            ForEach ($TargetUser in $AADTargetUsers) {
                $PWResetRight = [PSCustomObject]@{
                    UserName           = $User.UserName
                    ObjectType         = $User.ObjectType
                    UserID             = $User.UserID
                    UserOnPremID       = $User.UserOnPremID
                    TargetUserName     = $TargetUser.UserName
                    TargetUserID       = $TargetUser.UserID
                    TargetUserOnPremID = $TargetUser.UserOnPremID
                }	
                [void]$AADUserAccountAdminsRights.Add($PWResetRight)
            }
            $AADTargetUsers = $AADUsersWithoutRoles | ? { $_.onPremisesSecurityIdentifier -eq $null } | Where-Object { $_.userPrincipalName -NotMatch "#EXT#" }
            ForEach ($TargetUser in $AADTargetUsers) {
                $PWResetRight = [PSCustomObject]@{
                    UserName           = $User.UserName
                    ObjectType         = $User.ObjectType
                    UserID             = $User.UserID
                    UserOnPremID       = $User.UserOnPremID
                    TargetUserName     = $TargetUser.displayName
                    TargetUserID       = $TargetUser.id
                    TargetUserOnPremID = $TargetUser.onPremisesSecurityIdentifier
                }
                [void]$AADUserAccountAdminsRights.Add($PWResetRight)
            }
            $Progress += 1
            Write-Progress -interval 1 -TotalCount $TotalCount -progress $progress -item 'UserAccountAdmins'   
        }
        Write-Success $AADUserAccountAdminsRights.Count, 'objects saved as $AADUserAccountAdminsRights'
    }
    function Get-IntuneAdminRole {
        $global:AADIntuneAdminsRights = [System.Collections.ArrayList]::new() 
  
        ForEach ($User in $AADIntuneAdmins) {
            ForEach ($TargetGroup in $AADCloudGroups) {
                $GroupRight = [PSCustomObject]@{
                    UserName        = $User.UserName
                    ObjectType      = $User.ObjectType
                    UserID          = $User.UserID
                    UserOnPremID    = $User.UserOnPremID
                    TargetGroupName = $TargetGroup.DisplayName
                    TargetGroupID   = $TargetGroup.ObjectID
                }
                [void]$AADIntuneAdminsRights.Add($GroupRight)
            }
        }
        Write-Success $AADIntuneAdminsRights.Count, 'objects saved as $AADIntuneAdminsRights'





    }
    function Get-GroupsAdminRole {
        $global:AADGroupsAdminsRights = [System.Collections.ArrayList]::new()
  
        ForEach ($User in $AADGroupsAdmins) {
            ForEach ($TargetGroup in $AADCloudGroups) {
                $GroupRight = [PSCustomObject]@{
                    UserName        = $User.UserName
                    ObjectType      = $User.ObjectType
                    UserID          = $User.UserID
                    UserOnPremID    = $User.UserOnPremID
                    TargetGroupName = $TargetGroup.DisplayName
                    TargetGroupID   = $TargetGroup.ObjectID
                }
                [void]$AADGroupsAdminsRights.Add($GroupRight)
            }
        }
        Write-Success $AADGroupsAdminsRights.Count, 'objects saved as $AADGroupsAdminsRights'
    }
    function Get-GlobalAdminRole {
        $TenantDetails = Get-Tenant -hound
        $global:AADGlobalAdminsRights = [System.Collections.ArrayList]::new()
  
        ForEach ($User in $AADGlobalAdmins) {    

            $GlobalAdminRight = [PSCustomObject]@{
                UserName          = $User.UserName
                ObjectType        = $User.ObjectType
                UserID            = $User.UserID
                UserOnPremID      = $User.UserOnPremID
                TenantDisplayName = $TenantDetails.displayName
                TenantID          = $TenantDetails.id
            }
            [void]$AADGlobalAdminsRights.Add($GlobalAdminRight)
        }
        Write-Success $AADGlobalAdminsRights.Count, 'objects saved as $AADGlobalAdminsRights'
    }
    function Get-PrivilegedAdminRole {
        $global:AADPrivilegedRoleAdminRights = [System.Collections.ArrayList]::new()
        $TenantDetails = Get-Tenant -hound

        ForEach ($User in $AADPrivilegedRoleAdmins) { 
            $PrivilegedRoleAdminRight = [PSCustomObject]@{
                UserName          = $User.UserName
                ObjectType        = $User.ObjectType
                UserID            = $User.UserID
                UserOnPremID      = $User.UserOnPremID
                TenantDisplayName = $TenantDetails.displayName
                TenantID          = $TenantDetails.id
            }
            [void]$AADPrivilegedRoleAdminRights.Add($PrivilegedRoleAdminRight)
        }
        Write-Success $AADPrivilegedRoleAdminRights.Count, 'objects saved as $AADPrivilegedRoleAdminRights'
    }
    function Get-ApplicationAdmins {
        $global:AADAppAdminsRights = [System.Collections.ArrayList]::new()
  
        ForEach ($Principal in $AADAppAdmins) {
            $TargetApps = $AppsWithAppAdminRole
            ForEach ($TargetApp in $TargetApps) {
                $AppRight = [PSCustomObject]@{
                    AppAdminID       = $Principal.UserID
                    AppAdminType     = $Principal.UserType
                    AppAdminOnPremID = $Principal.UserOnPremID
                    TargetAppID      = $TargetApp.AppID
                }
                [void]$AADAppAdminsRights.Add($AppRight)
            }
            ForEach ($TargetApp in $AADSPsWithoutRoles) {
                $AppRight = [PSCustomObject]@{
                    AppAdminID       = $Principal.UserID
                    AppAdminType     = $Principal.UserType
                    AppAdminOnPremID = $Principal.UserOnPremID
                    TargetAppID      = $TargetApp.AppID
                }
                [void]$AADAppAdminsRights.Add($AppRight)
            }
        }
        Write-Success $AADAppAdminsRights.Count, 'objects saved as $AADAppAdminsRights'
    }
    function Get-CloudApplicationAdmins {
        $global:AADCloudAppAdminRights = [System.Collections.ArrayList]::new()
  
        ForEach ($Principal in $AADAppAdmins) {   
            $TargetApps = $AADAppsWithAppAdminRole  
            ForEach ($TargetApp in $TargetApps) {
                $AppRight = [PSCustomObject]@{
                    AppAdminID       = $Principal.UserID
                    AppAdminType     = $Principal.UserType
                    AppAdminOnPremID = $Principal.UserOnPremID
                    TargetAppID      = $TargetApp.AppID
                }
                [void]$AADCloudAppAdminRights.Add($AppRight)
            }
            ForEach ($TargetApp in $AADSPsWithoutRoles) {
                $AppRight = [PSCustomObject]@{
                    AppAdminID       = $Principal.UserID
                    AppAdminType     = $Principal.UserType
                    AppAdminOnPremID = $Principal.UserOnPremID
                    TargetAppID      = $TargetApp.AppID
                }
                [void]$AADCloudAppAdminRights.Add($AppRight)
            }
        }
        Write-Success $AADPasswordAdminsRights.Count, 'objects saved as $AADPasswordAdminsRights'

    }
    function Get-PrivAuthAdminRights {
        $global:Coll = [System.Collections.ArrayList]::new()
  
        $AADPrivAuthAdminRights | ForEach-Object { $null = $Coll.Add($_) }
        $AADAADAuthAdminsRights     | ForEach-Object { $null = $Coll.Add($_) }
        $AADHelpdeskAdminsRights    | ForEach-Object { $null = $Coll.Add($_) }
        $AADPasswordAdminsRights    | ForEach-Object { $null = $Coll.Add($_) }
        $AADUserAccountAdminsRights | ForEach-Object { $null = $Coll.Add($_) }

        # New-Output -Coll $Coll -Type "pwresetrights" -Directory $OutputDirectory  

        # $Coll = New-Object System.Collections.ArrayList
        $AADIntuneAdminsRights      | ForEach-Object { $null = $Coll.Add($_) }
        $AADGroupsAdminsRights      | ForEach-Object { $null = $Coll.Add($_) }
    }
    function Get-AADPrincipalMap {
        $global:AADPrincipalMap = @{}
        $AADUsers | % {
            $AADPrincipalMap.add($_.objectid, $_.OnPremisesSecurityIdentifier)
        }
        $AADGroups | % {
            $AADPrincipalMap.add($_.objectid, $_.OnPremisesSecurityIdentifier)
        }
        $AADPrincipalMap
    }
    function Get-AppToSpRelations {
        $global:AADServicePrincipals = Get-App | Get-AzADServicePrincipal | Foreach-Object {
            $global:AADServicePrincipal = [PSCustomObject]@{
                AppId                = $_.ApplicationId
                AppName              = $_.DisplayName
                ServicePrincipalId   = $_.Id
                ServicePrincipalType = $_.ObjectType
            }
            $null = $Coll.Add($AADServicePrincipal)
        }
    }
    function Get-VMs {
        $global:AADCurrentVms = [System.Collections.ArrayList]::new()

        $AADSubscriptions | ForEach-Object {
            $SubDisplayName = $_.Name
            Select-Subscription -SubscriptionID $_.Id | Out-Null
            $AADVirtualMachines = Get-AzVM
            $TotalCount = $AADVirtualMachines.Count
            $Progress = 0
            $AADVirtualMachines | ForEach-Object {
                $VM = $_
                $DisplayName = $VM.Name
                $RGName = $VM.ResourceGroupName
                $RGID = (Get-ResourceGroup "$RGName").ResourceID
                $id = $VM.id
                $resourceSub = "$id".split("/", 4)[2]
                $AzVM = [PSCustomObject]@{
                    AzVMName          = $VM.Name
                    AZID              = $VM.VmId
                    ResourceGroupName = $RGName
                    ResoucreGroupSub  = $resourceSub
                    ResourceGroupID   = $RGID
                }
                [void]$AADCurrentVms.Add($AzVM)
            }
        }
    }
    function Get-KeyVaults {
        $AADCurrentAzKeyVaults = [System.Collections.ArrayList]::new()

        $AADSubscriptions | ForEach-Object {
            $SubDisplayName = $_.Name
            Select-Subscription -SubscriptionID $_.Id | Out-Null
            $AADKeyVaults = Get-AzKeyVault
            $TotalCount = $AADKeyVaults.Count
            $Progress = 0
            $AADKeyVaults | ForEach-Object {
                $KeyVault = $_
                $DisplayName = $KeyVault.Name

                $RGName = $KeyVault.ResourceGroupName
                $RGID = (Get-ResourceGroup "$RGName").ResourceID
                $id = $KeyVault.ResourceId
                $resourceSub = "$id".split("/", 4)[2]
                $AzKeyVault = [PSCustomObject]@{
                    AzKeyVaultName    = $KeyVault.VaultName
                    AzKeyVaultID      = $KeyVault.ResourceId
                    ResourceGroupName = $RGName
                    ResoucreGroupSub  = $resourceSub
                    ResourceGroupID   = $RGID
                }
                [void]$AADCurrentAzKeyVaults.Add($AzKeyVault)
            }
        }
        New-Output -Coll $CurrentAzKeyVaults -Type "keyvaults" -Directory $OutputDirectory
        Write-Host -Fore DarkCyan "[+] " -NoNewLine; Write-Host "Built `$CurrentAzKeyVaults"
        Write-Host -Fore DarkCyan "[+] " -NoNewLine; Write-Host "Wrote azkeyvaults.json"
    }
    function Get-InbPermsVMs {
        $AADCurrentVMPermissions = [System.Collections.ArrayList]::new()
  
        $AADSubscriptions | ForEach-Object {
            $SubDisplayName = $_.Name
            Select-Subscription -SubscriptionID $_.Id | Out-Null
            $AADVMs = Get-AzVm
            $TotalCount = $AADVMs.Count

            $Progress = 0
            $AADVMs | ForEach-Object {
                $VM = $_
                $DisplayName = $VM.Name

                $VMID = $VM.id
                $VMGuid = $VM.VmId
                $AADRoles = Get-AzRoleAssignment -scope $VMID
                ForEach ($Role in $AADRoles) {
                    $ControllerType = $Role.ObjectType
                    If ($ControllerType -eq "User") {
                        $Controller = Get-AzureADUser -ObjectID $Role.ObjectID
                        $OnPremID = $Controller.OnPremisesSecurityIdentifier
                    }
                    If ($ControllerType -eq "Group") {
                        $Controller = Get-AzureADGroup -ObjectID $Role.ObjectID
                        $OnPremID = $Controller.OnPremisesSecurityIdentifier
                    }
                    If ($ControllerType -eq "ServicePrincipal") {
                        $Controller = Get-AzureADServicePrincipal -ObjectID $Role.ObjectID
                        $OnPremID = $null
                    }
                    $VMPrivilege = New-Object PSObject
                    $VMPrivilege = [PSCustomObject]@{
                        VMID               = $VMGuid
                        ControllerName     = $Role.DisplayName
                        ControllerID       = $Role.ObjectID
                        ControllerType     = $Role.ObjectType
                        ControllerOnPremID = $OnPremID
                        RoleName           = $Role.RoleDefinitionName
                        RoleDefinitionId   = $Role.RoleDefinitionId
                    }
                    [void]$AADCurrentVMPermissions.Add($VMPrivilege)
                }
            }
        }
        New-Output -Coll $Coll -Type "vmpermissions" -Directory $OutputDirectory
        Write-Host -Fore DarkCyan "[+] " -NoNewLine; Write-Host "Built `$CurrentVMPermissions"
        Write-Host -Fore DarkCyan "[+] " -NoNewLine; Write-Host "Wrote azvmpermissions.json"
    }
    function Get-InbPermsResourceGroups {
  
        $progress = 0
        $TotalCount = $res

        $AADSubscriptions | ForEach-Object {
            $SubDisplayName = $_.Name
            Select-Subscription -SubscriptionID $_.Id | Out-Null
            Write-Host "Building resource groups object for subscription ${SubDisplayName}"
            $AADResourceGroups = Get-ResourceGroup
            $TotalCount = $AADResourceGroups.Count
    
            Write-Progress -interval 1000 -TotalCount $TotalCount -progress $progress -item 'Subscription'

            $AADResourceGroups | ForEach-Object {

                $RG = $_
                $DisplayName = $RG.DisplayName

                $RGID = $RG.ResourceId
                $AADRoles = Get-AzRoleAssignment -scope $RGID
      
                ForEach ($Role in $AADRoles) {
      
                    $ControllerType = $Role.ObjectType
        
                    If ($ControllerType -eq "User") {
                        $Controller = Get-AzureADUser -ObjectID $Role.ObjectID
                        $OnPremID = $Controller.OnPremisesSecurityIdentifier
                    }
        
                    If ($ControllerType -eq "Group") {
                        $Controller = Get-AzureADGroup -ObjectID $Role.ObjectID
                        $OnPremID = $Controller.OnPremisesSecurityIdentifier
                    }

                    $RGPrivilege = [PSCustomObject]@{
                        RGID               = $RGID
                        ControllerName     = $Role.DisplayName
                        ControllerID       = $Role.ObjectID
                        ControllerType     = $Role.ObjectType
                        ControllerOnPremID = $OnPremID
                        RoleName           = $Role.RoleDefinitionName
                        RoleDefinitionId   = $Role.RoleDefinitionId
                    } 
                    $null = $Coll.Add($RGPrivilege)
                }
            }
        }
        New-Output -Coll $Coll -Type "rgpermissions" -Directory $OutputDirectory
        Write-Host -Fore DarkCyan "[+] " -NoNewLine; Write-Host "Built `$CurrentVMPermissions"
        Write-Host -Fore DarkCyan "[+] " -NoNewLine; Write-Host "Wrote azvmpermissions.json"
    }
    function Get-InbPermsKeyVaults {
        $Coll = New-Object System.Collections.ArrayList
        $AADSubscriptions | ForEach-Object {
            $SubDisplayName = $_.Name
            Select-Subscription -SubscriptionID $_.Id | Out-Null
            Write-Host "Building key vaults object for subscription ${SubDisplayName}"

            $progress = 0

            $AADKeyVaults = Get-AzKeyVault
            $TotalCount = $AADKeyVaults.Count

            $AADKeyVaults | ForEach-Object {
                $KeyVault = $_
                $DisplayName = $KeyVault.DisplayName

                $progress += 1
                Write-Progress -interval 1000 -TotalCount $TotalCount -progress $progress -item 'KeyVault'

                $KVID = $KeyVault.ResourceId
                $AADRoles = Get-AzRoleAssignment -scope $KVID
                ForEach ($Role in $AADRoles) {
                    $ControllerType = $Role.ObjectType
                    If ($ControllerType -eq "User") {
                        $Controller = Get-AzureADUser -ObjectID $Role.ObjectID
                        $OnPremID = $Controller.OnPremisesSecurityIdentifier
                    }
        
                    If ($ControllerType -eq "Group") {
                        $Controller = Get-AzureADGroup -ObjectID $Role.ObjectID
                        $OnPremID = $Controller.OnPremisesSecurityIdentifier
                    }
                    $KVPrivilege = [PSCustomObject]@{
                        KVID               = $KVID
                        ControllerName     = $Role.DisplayName
                        ControllerID       = $Role.ObjectID
                        ControllerType     = $Role.ObjectType
                        ControllerOnPremID = $OnPremID
                        RoleName           = $Role.RoleDefinitionName
                        RoleDefinitionId   = $Role.RoleDefinitionId
                    }
                    $null = $Coll.Add($KVPrivilege)
                }
            }
        }
        New-Output -Coll $Coll -Type "kvpermissions" -Directory $OutputDirectory
        Write-Host -Fore DarkCyan "[+] " -NoNewLine; Write-Host "Built `$CurrentVMPermissions"
        Write-Host -Fore DarkCyan "[+] " -NoNewLine; Write-Host "Wrote azvmpermissions.json"
    }


    ###################################

    #########  AD POWERVIEW ###########

    ###################################


    function Get-NNetUUser {

        param(
            [Parameter(Position = 0, ValueFromPipeline = $True)]
            [String]
            $UserName,

            [String]
            $Domain,

            [String]
            $DomainController,

            [String]
            $ADSpath,

            [String]
            $Filter,

            [Switch]
            $SPN,

            [Switch]
            $AdminCount,

            [Switch]
            $Unconstrained,

            [Switch]
            $AllowDelegation,

            [ValidateRange(1, 10000)] 
            [Int]
            $PageSize = 200,

            [Management.Automation.PSCredential]
            $Credential
        )

        begin {
            # so this isn't repeated if users are passed on the pipeline
            $UserSearcher = Get-DDomainSSearcher -Domain $Domain -ADSpath $ADSpath -DomainController $DomainController -PageSize $PageSize -Credential $Credential
        }

        process {
            if ($UserSearcher) {

                # if we're checking for unconstrained delegation
                if ($Unconstrained) {
                    Write-Verbose "Checking for unconstrained delegation"
                    $Filter += "(userAccountControl:1.2.840.113556.1.4.803:=524288)"
                }
                if ($AllowDelegation) {
                    Write-Verbose "Checking for users who can be delegated"
                    # negation of "Accounts that are sensitive and not trusted for delegation"
                    $Filter += "(!(userAccountControl:1.2.840.113556.1.4.803:=1048574))"
                }
                if ($AdminCount) {
                    Write-Verbose "Checking for adminCount=1"
                    $Filter += "(admincount=1)"
                }

                # check if we're using a username filter or not
                if ($UserName) {
                    # samAccountType=805306368 indicates user objects
                    $UserSearcher.filter = "(&(samAccountType=805306368)(samAccountName=$UserName)$Filter)"
                }
                elseif ($SPN) {
                    $UserSearcher.filter = "(&(samAccountType=805306368)(servicePrincipalName=*)$Filter)"
                }
                else {
                    # filter is something like "(samAccountName=*blah*)" if specified
                    $UserSearcher.filter = "(&(samAccountType=805306368)$Filter)"
                }

                $Results = $UserSearcher.FindAll()
                $Results | Where-Object { $_ } | ForEach-Object {
                    # convert/process the LDAP fields for each result
                    $User = Convert-LLDAPPProperty -Properties $_.Properties
                    $User.PSObject.TypeNames.Add('PowerVewe.User')
                    $User
                }
                $Results.dispose()
                $UserSearcher.dispose()
            }
        }
    }
    function Get-NNetCComputer {
        [CmdletBinding()]
        Param (
            [Parameter(ValueFromPipeline = $True)]
            [Alias('HostName')]
            [String]
            $ComputerName = '*',

            [String]
            $SPN,

            [String]
            $OperatingSystem,

            [String]
            $ServicePack,

            [String]
            $Filter,

            [Switch]
            $Printers,

            [Switch]
            $Ping,

            [Switch]
            $FullData = $true,

            [String]
            $Domain,

            [String]
            $DomainController,

            [String]
            $ADSpath,

            [String]
            $SiteName,

            [Switch]
            $Unconstrained,

            [ValidateRange(1, 10000)] 
            [Int]
            $PageSize = 200,

            [Management.Automation.PSCredential]
            $Credential
        )

        begin {
            # so this isn't repeated if multiple computer names are passed on the pipeline
            $CompSearcher = Get-DDomainSSearcher -Domain $Domain -DomainController $DomainController -ADSpath $ADSpath -PageSize $PageSize -Credential $Credential
        }

        process {

            if ($CompSearcher) {

                # if we're checking for unconstrained delegation
                if ($Unconstrained) {
                    Write-Verbose "Searching for computers with for unconstrained delegation"
                    $Filter += "(userAccountControl:1.2.840.113556.1.4.803:=524288)"
                }
                # set the filters for the seracher if it exists
                if ($Printers) {
                    Write-Verbose "Searching for printers"
                    # $CompSearcher.filter="(&(objectCategory=printQueue)$Filter)"
                    $Filter += "(objectCategory=printQueue)"
                }
                if ($SPN) {
                    Write-Verbose "Searching for computers with SPN: $SPN"
                    $Filter += "(servicePrincipalName=$SPN)"
                }
                if ($OperatingSystem) {
                    $Filter += "(operatingsystem=$OperatingSystem)"
                }
                if ($ServicePack) {
                    $Filter += "(operatingsystemservicepack=$ServicePack)"
                }
                if ($SiteName) {
                    $Filter += "(serverreferencebl=$SiteName)"
                }

                $CompFilter = "(&(sAMAccountType=805306369)(name=$ComputerName)$Filter)"
                Write-Verbose "Get-NetComputer filter : '$CompFilter'"
                $CompSearcher.filter = $CompFilter

                try {
                    $Results = $CompSearcher.FindAll()
                    $Results | Where-Object { $_ } | ForEach-Object {
                        $Up = $True
                        if ($Ping) {
                            # TODO: how can these results be piped to ping for a speedup?
                            $Up = Test-Connection -Count 1 -Quiet -ComputerName $_.properties.dnshostname
                        }
                        if ($Up) {
                            # return full data objects
                            if ($FullData) {
                                # convert/process the LDAP fields for each result
                                $Computer = Convert-LLDAPPProperty -Properties $_.Properties
                                $Computer.PSObject.TypeNames.Add('PowerVewe.Computer')
                                $Computer
                            }
                            else {
                                # otherwise we're just returning the DNS host name
                                $_.properties.dnshostname
                            }
                        }
                    }
                    $Results.dispose()
                    $CompSearcher.dispose()
                }
                catch {
                    Write-Warning "Error: $_"
                }
            }
        }
    }
    function Get-NNetGGroup {
        [CmdletBinding()]
        param(
            [Parameter(ValueFromPipeline = $True)]
            [String]
            $GroupName = '*',

            [String]
            $SID,

            [String]
            $UserName,

            [String]
            $Filter,

            [String]
            $Domain,

            [String]
            $DomainController,

            [String]
            $ADSpath,

            [Switch]
            $AdminCount,

            [Switch]
            $FullData,

            [Switch]
            $RawSids,

            [Switch]
            $AllTypes,

            [ValidateRange(1, 10000)]
            [Int]
            $PageSize = 200,

            [Management.Automation.PSCredential]
            $Credential
        )

        begin {
            $GroupSearcher = Get-DDomainSSearcher -Domain $Domain -DomainController $DomainController -Credential $Credential -ADSpath $ADSpath -PageSize $PageSize
            if (!$AllTypes) {
                $Filter += "(groupType:1.2.840.113556.1.4.803:=2147483648)"
            }
        }

        process {
            if ($GroupSearcher) {

                if ($AdminCount) {
                    Write-Verbose "Checking for adminCount=1"
                    $Filter += "(admincount=1)"
                }

                if ($UserName) {
                    $User = Get-AADOObject -SamAccountName $UserName -Domain $Domain -DomainController $DomainController -Credential $Credential -ReturnRaw -PageSize $PageSize | Select-Object -First 1

                    if ($User) {
                        $UserDirectoryEntry = $User.GetDirectoryEntry()

                        $UserDirectoryEntry.RefreshCache("tokenGroups")

                        $UserDirectoryEntry.TokenGroups | ForEach-Object {
                            $GroupSid = (New-Object System.Security.Principal.SecurityIdentifier($_, 0)).Value

                            if ($GroupSid -notmatch '^S-1-5-32-.*') {
                                if ($FullData) {
                                    $Group = Get-AADOObject -SID $GroupSid -PageSize $PageSize -Domain $Domain -DomainController $DomainController -Credential $Credential
                                    $Group.PSObject.TypeNames.Add('PowerVewe.Group')
                                    $Group
                                }
                                else {
                                    if ($RawSids) {
                                        $GroupSid
                                    }
                                    else {
                                        Convert-SSidTToNName -SID $GroupSid
                                    }
                                }
                            }
                        }
                    }
                    else {
                        Write-Warning "UserName '$UserName' failed to resolve."
                    }
                }
                else {
                    if ($SID) {
                        $GroupSearcher.filter = "(&(objectCategory=group)(objectSID=$SID)$Filter)"
                    }
                    else {
                        $GroupSearcher.filter = "(&(objectCategory=group)(samaccountname=$GroupName)$Filter)"
                    }

                    $Results = $GroupSearcher.FindAll()
                    $Results | Where-Object { $_ } | ForEach-Object {       
                        $Group = Convert-LLDAPPProperty -Properties $_.Properties
                        $Group.PSObject.TypeNames.Add('PowerVewe.Group')
                        $Group

                    }
                    $Results.dispose()
                    $GroupSearcher.dispose()
                }
            }
        }
    }
    function Get-NNetGGroupMMember {

        [CmdletBinding()]
        param(
            [Parameter(ValueFromPipeline = $True)]
            [String]
            $GroupName,

            [String]
            $SID,

            [String]
            $Domain,

            [String]
            $DomainController,

            [String]
            $ADSpath,

            [Switch]
            $FullData,

            [Switch]
            $Recurse,

            [Switch]
            $UseMatchingRule,

            [ValidateRange(1, 10000)] 
            [Int]
            $PageSize = 200,

            [Management.Automation.PSCredential]
            $Credential
        )

        begin {
            if ($DomainController) {
                $TargetDomainController = $DomainController
            }
            else {
                $TargetDomainController = ((Get-NNetDDomain -Credential $Credential).PdcRoleOwner).Name
            }

            if ($Domain) {
                $TargetDomain = $Domain
            }
            else {
                $TargetDomain = Get-NNetDDomain -Credential $Credential | Select-Object -ExpandProperty name
            }

            # so this isn't repeated if users are passed on the pipeline
            $GroupSearcher = Get-DDomainSSearcher -Domain $TargetDomain -DomainController $TargetDomainController -Credential $Credential -ADSpath $ADSpath -PageSize $PageSize
        }

        process {
            if ($GroupSearcher) {
                if ($Recurse -and $UseMatchingRule) {
                    # resolve the group to a distinguishedname
                    if ($GroupName) {
                        $Group = Get-NNetGGroup -AllTypes -GroupName $GroupName -Domain $TargetDomain -DomainController $TargetDomainController -Credential $Credential -FullData -PageSize $PageSize
                    }
                    elseif ($SID) {
                        $Group = Get-NNetGGroup -AllTypes -SID $SID -Domain $TargetDomain -DomainController $TargetDomainController -Credential $Credential -FullData -PageSize $PageSize
                    }
                    else {
                        # default to domain admins
                        $SID = (Get-DDomainSSID -Domain $TargetDomain -DomainController $TargetDomainController) + "-512"
                        $Group = Get-NNetGGroup -AllTypes -SID $SID -Domain $TargetDomain -DomainController $TargetDomainController -Credential $Credential -FullData -PageSize $PageSize
                    }
                    $GroupDN = $Group.distinguishedname
                    $GroupFoundName = $Group.samaccountname

                    if ($GroupDN) {
                        $GroupSearcher.filter = "(&(samAccountType=805306368)(memberof:1.2.840.113556.1.4.1941:=$GroupDN)$Filter)"
                        $GroupSearcher.PropertiesToLoad.AddRange(('distinguishedName', 'samaccounttype', 'lastlogon', 'lastlogontimestamp', 'dscorepropagationdata', 'objectsid', 'whencreated', 'badpasswordtime', 'accountexpires', 'iscriticalsystemobject', 'name', 'usnchanged', 'objectcategory', 'description', 'codepage', 'instancetype', 'countrycode', 'distinguishedname', 'cn', 'admincount', 'logonhours', 'objectclass', 'logoncount', 'usncreated', 'useraccountcontrol', 'objectguid', 'primarygroupid', 'lastlogoff', 'samaccountname', 'badpwdcount', 'whenchanged', 'memberof', 'pwdlastset', 'adspath'))

                        $Members = $GroupSearcher.FindAll()
                        $GroupFoundName = $GroupName
                    }
                    else {
                        Write-Error "Unable to find Group"
                    }
                }
                else {
                    if ($GroupName) {
                        $GroupSearcher.filter = "(&(objectCategory=group)(samaccountname=$GroupName)$Filter)"
                    }
                    elseif ($SID) {
                        $GroupSearcher.filter = "(&(objectCategory=group)(objectSID=$SID)$Filter)"
                    }
                    else {
                        # default to domain admins
                        $SID = (Get-DDomainSSID -Domain $TargetDomain -DomainController $TargetDomainController) + "-512"
                        $GroupSearcher.filter = "(&(objectCategory=group)(objectSID=$SID)$Filter)"
                    }

                    try {
                        $Result = $GroupSearcher.FindOne()
                    }
                    catch {
                        $Members = @()
                    }

                    $GroupFoundName = ''

                    if ($Result) {
                        $Members = $Result.properties.item("member")

                        if ($Members.count -eq 0) {

                            $Finished = $False
                            $Bottom = 0
                            $Top = 0

                            while (!$Finished) {
                                $Top = $Bottom + 1499
                                $MemberRange = "member;range=$Bottom-$Top"
                                $Bottom += 1500
                            
                                $GroupSearcher.PropertiesToLoad.Clear()
                                [void]$GroupSearcher.PropertiesToLoad.Add("$MemberRange")
                                [void]$GroupSearcher.PropertiesToLoad.Add("samaccountname")
                                try {
                                    $Result = $GroupSearcher.FindOne()
                                    $RangedProperty = $Result.Properties.PropertyNames -like "member;range=*"
                                    $Members += $Result.Properties.item($RangedProperty)
                                    $GroupFoundName = $Result.properties.item("samaccountname")[0]

                                    if ($Members.count -eq 0) { 
                                        $Finished = $True
                                    }
                                }
                                catch [System.Management.Automation.MethodInvocationException] {
                                    $Finished = $True
                                }
                            }
                        }
                        else {
                            $GroupFoundName = $Result.properties.item("samaccountname")[0]
                            $Members += $Result.Properties.item($RangedProperty)
                        }
                    }
                    $GroupSearcher.dispose()
                }

                $Members | Where-Object { $_ } | ForEach-Object {
                    # if we're doing the LDAP_MATCHING_RULE_IN_CHAIN recursion
                    if ($Recurse -and $UseMatchingRule) {
                        $Properties = $_.Properties
                    } 
                    else {
                        if ($TargetDomainController) {
                            $Result = [adsi]"LDAP://$TargetDomainController/$_"
                        }
                        else {
                            $Result = [adsi]"LDAP://$_"
                        }
                        if ($Result) {
                            $Properties = $Result.Properties
                        }
                    }

                    if ($Properties) {

                        $IsGroup = @('268435456', '268435457', '536870912', '536870913') -contains $Properties.samaccounttype

                        if ($FullData) {
                            $GroupMember = Convert-LLDAPPProperty -Properties $Properties
                        }
                        else {
                            $GroupMember = New-Object PSObject
                        }

                        $GroupMember | Add-Member Noteproperty 'GroupDomain' $TargetDomain
                        $GroupMember | Add-Member Noteproperty 'GroupName' $GroupFoundName

                        if ($Properties.objectSid) {
                            $MemberSID = ((New-Object System.Security.Principal.SecurityIdentifier $Properties.objectSid[0], 0).Value)
                        }
                        else {
                            $MemberSID = $Null
                        }

                        try {
                            $MemberDN = $Properties.distinguishedname[0]

                            if (($MemberDN -match 'ForeignSecurityPrincipals') -and ($MemberDN -match 'S-1-5-21')) {
                                try {
                                    if (-not $MemberSID) {
                                        $MemberSID = $Properties.cn[0]
                                    }
                                    $MemberSimpleName = Convert-SSidTToNName -SID $MemberSID | Convert-AADNName -InputType 'NT4' -OutputType 'Simple'
                                    if ($MemberSimpleName) {
                                        $MemberDomain = $MemberSimpleName.Split('@')[1]
                                    }
                                    else {
                                        Write-Warning "Error converting $MemberDN"
                                        $MemberDomain = $Null
                                    }
                                }
                                catch {
                                    Write-Warning "Error converting $MemberDN"
                                    $MemberDomain = $Null
                                }
                            }
                            else {
                                # extract the FQDN from the Distinguished Name
                                $MemberDomain = $MemberDN.subString($MemberDN.IndexOf("DC=")) -replace 'DC=', '' -replace ',', '.'
                            }
                        }
                        catch {
                            $MemberDN = $Null
                            $MemberDomain = $Null
                        }

                        if ($Properties.samaccountname) {
                            # forest users have the samAccountName set
                            $MemberName = $Properties.samaccountname[0]
                        } 
                        else {
                            # external trust users have a SID, so convert it
                            try {
                                $MemberName = Convert-SidToName $Properties.cn[0]
                            }
                            catch {
                                # if there's a problem contacting the domain to resolve the SID
                                $MemberName = $Properties.cn
                            }
                        }

                        $GroupMember | Add-Member Noteproperty 'MemberDomain' $MemberDomain
                        $GroupMember | Add-Member Noteproperty 'MemberName' $MemberName
                        $GroupMember | Add-Member Noteproperty 'MemberSID' $MemberSID
                        $GroupMember | Add-Member Noteproperty 'IsGroup' $IsGroup
                        $GroupMember | Add-Member Noteproperty 'MemberDN' $MemberDN
                        $GroupMember.PSObject.TypeNames.Add('PowerVewe.GroupMember')
                        $GroupMember

                        # if we're doing manual recursion
                        if ($Recurse -and !$UseMatchingRule -and $IsGroup -and $MemberName) {
                            if ($FullData) {
                                Get-NNetGGroupMMember -FullData -Domain $MemberDomain -DomainController $TargetDomainController -Credential $Credential -GroupName $MemberName -Recurse -PageSize $PageSize
                            }
                            else {
                                Get-NNetGGroupMMember -Domain $MemberDomain -DomainController $TargetDomainController -Credential $Credential -GroupName $MemberName -Recurse -PageSize $PageSize
                            }
                        }
                    }
                }
            }
        }
    }
    function Get-NnetDDomain {

        param(
            [Parameter(ValueFromPipeline = $True)]
            [String]
            $Domain,

            [Management.Automation.PSCredential]
            $Credential
        )

        if ($Credential) {
        
            Write-Verbose "Using alternate credentials for Get-NNetDDomain"

            if (!$Domain) {
                # if no domain is supplied, extract the logon domain from the PSCredential passed
                $Domain = $Credential.GetNetworkCredential().Domain
                Write-Verbose "Extracted domain '$Domain' from -Credential"
            }
   
            $DomainContext = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext('Domain', $Domain, $Credential.UserName, $Credential.GetNetworkCredential().Password)
        
            try {
                [System.DirectoryServices.ActiveDirectory.Domain]::GetDomain($DomainContext)
            }
            catch {
                Write-Verbose "The specified domain does '$Domain' not exist, could not be contacted, there isn't an existing trust, or the specified credentials are invalid."
                $Null
            }
        }
        elseif ($Domain) {
            $DomainContext = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext('Domain', $Domain)
            try {
                [System.DirectoryServices.ActiveDirectory.Domain]::GetDomain($DomainContext)
            }
            catch {
                Write-Verbose "The specified domain '$Domain' does not exist, could not be contacted, or there isn't an existing trust."
                $Null
            }
        }
        else {
            [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()
        }
    }
    filter Get-NNetFForest {

        param(
            [Parameter(ValueFromPipeline = $True)]
            [String]
            $Forest,

            [Management.Automation.PSCredential]
            $Credential
        )

        if ($Credential) {
        
            Write-Verbose "Using alternate credentials for Get-NNetFForest"

            if (!$Forest) {
                # if no domain is supplied, extract the logon domain from the PSCredential passed
                $Forest = $Credential.GetNetworkCredential().Domain
                Write-Verbose "Extracted domain '$Forest' from -Credential"
            }
   
            $ForestContext = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext('Forest', $Forest, $Credential.UserName, $Credential.GetNetworkCredential().Password)
        
            try {
                $ForestObject = [System.DirectoryServices.ActiveDirectory.Forest]::GetForest($ForestContext)
            }
            catch {
                Write-Verbose "The specified forest '$Forest' does not exist, could not be contacted, there isn't an existing trust, or the specified credentials are invalid."
                $Null
            }
        }
        elseif ($Forest) {
            $ForestContext = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext('Forest', $Forest)
            try {
                $ForestObject = [System.DirectoryServices.ActiveDirectory.Forest]::GetForest($ForestContext)
            }
            catch {
                Write-Verbose "The specified forest '$Forest' does not exist, could not be contacted, or there isn't an existing trust."
                return $Null
            }
        }
        else {
            # otherwise use the current forest
            $ForestObject = [System.DirectoryServices.ActiveDirectory.Forest]::GetCurrentForest()
        }

        if ($ForestObject) {
            # get the SID of the forest root
            $ForestSid = (New-Object System.Security.Principal.NTAccount($ForestObject.RootDomain, "krbtgt")).Translate([System.Security.Principal.SecurityIdentifier]).Value
            $Parts = $ForestSid -Split "-"
            $ForestSid = $Parts[0..$($Parts.length - 2)] -join "-"
            $ForestObject | Add-Member NoteProperty 'RootDomainSid' $ForestSid
            $ForestObject
        }
    }
    function Get-NNetGGpo {

        [CmdletBinding()]
        Param (
            [Parameter(ValueFromPipeline = $True)]
            [String]
            $GPOname = '*',

            [String]
            $DisplayName,

            [String]
            $ComputerName,

            [String]
            $Domain,

            [String]
            $DomainController,
        
            [String]
            $ADSpath,

            [ValidateRange(1, 10000)] 
            [Int]
            $PageSize = 200,

            [Management.Automation.PSCredential]
            $Credential
        )

        begin {
            $GPOSearcher = Get-DDomainSSearcher -Domain $Domain -DomainController $DomainController -Credential $Credential -ADSpath $ADSpath -PageSize $PageSize
        }

        process {
            if ($GPOSearcher) {

                if ($ComputerName) {
                    $GPONames = @()
                    $Computers = Get-NNetCComputer -ComputerName $ComputerName -Domain $Domain -DomainController $DomainController -FullData -PageSize $PageSize

                    if (!$Computers) {
                        throw "Computer $ComputerName in domain '$Domain' not found! Try a fully qualified host name"
                    }
                
                    $ComputerOUs = @()
                    ForEach ($Computer in $Computers) {

                        $DN = $Computer.distinguishedname

                        $ComputerOUs += $DN.split(",") | ForEach-Object {
                            if ($_.startswith("OU=")) {
                                $DN.substring($DN.indexof($_))
                            }
                        }
                    }
                
                    Write-Verbose "ComputerOUs: $ComputerOUs"

                    # find all the GPOs linked to the computer's OU
                    ForEach ($ComputerOU in $ComputerOUs) {
                        $GPONames += Get-NNetOOU -Domain $Domain -DomainController $DomainController -ADSpath $ComputerOU -FullData -PageSize $PageSize | ForEach-Object { 
                            # get any GPO links
                            write-verbose "blah: $($_.name)"
                            $_.gplink.split("][") | ForEach-Object {
                                if ($_.startswith("LDAP")) {
                                    $_.split(";")[0]
                                }
                            }
                        }
                    }
                
                    Write-Verbose "GPONames: $GPONames"

                    # find any GPOs linked to the site for the given computer
                    $ComputerSite = (Get-SSiteNName -ComputerName $ComputerName).SiteName
                    if ($ComputerSite -and ($ComputerSite -notlike 'Error*')) {
                        $GPONames += Get-NNetSSite -SiteName $ComputerSite -FullData | ForEach-Object {
                            if ($_.gplink) {
                                $_.gplink.split("][") | ForEach-Object {
                                    if ($_.startswith("LDAP")) {
                                        $_.split(";")[0]
                                    }
                                }
                            }
                        }
                    }

                    $GPONames | Where-Object { $_ -and ($_ -ne '') } | ForEach-Object {

                        # use the gplink as an ADS path to enumerate all GPOs for the computer
                        $GPOSearcher = Get-DDomainSSearcher -Domain $Domain -DomainController $DomainController -Credential $Credential -ADSpath $_ -PageSize $PageSize
                        $GPOSearcher.filter = "(&(objectCategory=groupPolicyContainer)(name=$GPOname))"

                        try {
                            $Results = $GPOSearcher.FindAll()
                            $Results | Where-Object { $_ } | ForEach-Object {
                                $Out = Convert-LLDAPPProperty -Properties $_.Properties
                                $Out | Add-Member Noteproperty 'ComputerName' $ComputerName
                                $Out
                            }
                            $Results.dispose()
                            $GPOSearcher.dispose()
                        }
                        catch {
                            Write-Warning $_
                        }
                    }
                }

                else {
                    if ($DisplayName) {
                        $GPOSearcher.filter = "(&(objectCategory=groupPolicyContainer)(displayname=$DisplayName))"
                    }
                    else {
                        $GPOSearcher.filter = "(&(objectCategory=groupPolicyContainer)(name=$GPOname))"
                    }

                    try {
                        $Results = $GPOSearcher.FindAll()
                        $Results | Where-Object { $_ } | ForEach-Object {
                            if ($ADSPath -and ($ADSpath -Match '^GC://')) {
                                $Properties = Convert-LLDAPPProperty -Properties $_.Properties
                                try {
                                    $GPODN = $Properties.distinguishedname
                                    $GPODomain = $GPODN.subString($GPODN.IndexOf("DC=")) -replace 'DC=', '' -replace ',', '.'
                                    $gpcfilesyspath = "\\$GPODomain\SysVol\$GPODomain\Policies\$($Properties.cn)"
                                    $Properties | Add-Member Noteproperty 'gpcfilesyspath' $gpcfilesyspath
                                    $Properties
                                }
                                catch {
                                    $Properties
                                }
                            }
                            else {
                                # convert/process the LDAP fields for each result
                                Convert-LLDAPPProperty -Properties $_.Properties
                            }
                        }
                        $Results.dispose()
                        $GPOSearcher.dispose()
                    }
                    catch {
                        Write-Warning $_
                    }
                }
            }
        }
    }
    function Get-NNetGGpoGGroup {

        [CmdletBinding()]
        Param (
            [String]
            $GPOname = '*',

            [String]
            $DisplayName,

            [String]
            $Domain,

            [String]
            $DomainController,

            [String]
            $ADSpath,

            [Switch]
            $ResolveMemberSIDs,

            [Switch]
            $ResolveMemberNames,

            [Switch]
            $UsePSDrive,

            [ValidateRange(1, 10000)] 
            [Int]
            $PageSize = 200
        )

        $Option = [System.StringSplitOptions]::RemoveEmptyEntries
        Get-NNetGGpo | ForEach-Object {

            $GPOdisplayName = $_.displayname
            $GPOname = $_.name
            $GPOPath = $_.gpcfilesyspath

            $ParseArgs = @{
                'GptTmplPath' = "$GPOPath\MACHINE\Microsoft\Windows NT\SecEdit\GptTmpl.inf"
                'UsePSDrive'  = $UsePSDrive
            }

            # parse the GptTmpl.inf 'Restricted Groups' file if it exists
            $Inf = Get-GGptTTmpl @ParseArgs

            if ($Inf -and ($Inf.psbase.Keys -contains 'Group Membership')) {

                $Memberships = @{}

                # group the members/memberof fields for each entry
                ForEach ($Membership in $Inf.'Group Membership'.GetEnumerator()) {
                    $Group, $Relation = $Membership.Key.Split('__', $Option) | ForEach-Object { $_.Trim() }

                    # extract out ALL members
                    $MembershipValue = $Membership.Value | Where-Object { $_ } | ForEach-Object { $_.Trim('*') } | Where-Object { $_ }

                    if ($ResolveMemberSIDs) {
                        # if the resulting member is username and not a SID, attempt to resolve it
                        $GroupMembers = @()
                        ForEach ($Member in $MembershipValue) {
                            if ($Member -and ($Member.Trim() -ne '')) {
                                if ($Member -notmatch '^S-1-.*') {
                                    $MemberSID = Convert-NNameTToSSid -Domain $Domain -ObjectName $Member | Select-Object -ExpandProperty SID
                                    if ($MemberSID) {
                                        $GroupMembers += $MemberSID
                                    }
                                    else {
                                        $GroupMembers += $Member
                                    }
                                }
                                else {
                                    $GroupMembers += $Member
                                }
                            }
                        }
                        $MembershipValue = $GroupMembers
                    }

                    if (-not $Memberships[$Group]) {
                        $Memberships[$Group] = @{}
                    }
                    if ($MembershipValue -isnot [System.Array]) { $MembershipValue = @($MembershipValue) }
                    $Memberships[$Group].Add($Relation, $MembershipValue)
                }

                ForEach ($Membership in $Memberships.GetEnumerator()) {
                    if ($Membership -and $Membership.Key -and ($Membership.Key -match '^\*')) {
                        # if the SID is already resolved (i.e. begins with *) try to resolve SID to a name
                        $GroupSID = $Membership.Key.Trim('*')
                        if ($GroupSID -and ($GroupSID.Trim() -ne '')) {
                            $GroupName = Convert-SSidTToNName -SID $GroupSID
                        }
                        else {
                            $GroupName = $False
                        }
                    }
                    else {
                        $GroupName = $Membership.Key

                        if ($GroupName -and ($GroupName.Trim() -ne '')) {
                            if ($Groupname -match 'Administrators') {
                                $GroupSID = 'S-1-5-32-544'
                            }
                            elseif ($Groupname -match 'Remote Desktop') {
                                $GroupSID = 'S-1-5-32-555'
                            }
                            elseif ($Groupname -match 'Guests') {
                                $GroupSID = 'S-1-5-32-546'
                            }
                            elseif ($GroupName.Trim() -ne '') {
                                $GroupSID = Convert-NNameTToSSid -Domain $Domain -ObjectName $Groupname | Select-Object -ExpandProperty SID
                            }
                            else {
                                $GroupSID = $Null
                            }
                        }
                    }

                    $GPOGroup = New-Object PSObject
                    $GPOGroup | Add-Member Noteproperty 'GPODisplayName' $GPODisplayName
                    $GPOGroup | Add-Member Noteproperty 'GPOName' $GPOName
                    $GPOGroup | Add-Member Noteproperty 'GPOPath' $GPOPath
                    $GPOGroup | Add-Member Noteproperty 'GPOType' 'RestrictedGroups'
                    $GPOGroup | Add-Member Noteproperty 'Filters' $Null
                    $GPOGroup | Add-Member Noteproperty 'GroupName' $GroupName
                    $GPOGroup | Add-Member Noteproperty 'GroupSID' $GroupSID
                    $GPOGroup | Add-Member Noteproperty 'GroupMemberOf' $Membership.Value.Memberof
                    $GPOGroup | Add-Member Noteproperty 'GroupMembers' $Membership.Value.Members
                    $GPOGroup
                }
            }

            $ParseArgs = @{
                'GroupsXMLpath' = "$GPOPath\MACHINE\Preferences\Groups\Groups.xml"
                'UsePSDrive'    = $UsePSDrive
            }

            Get-GGroupsXXML @ParseArgs | ForEach-Object {
                if ($ResolveMemberSIDs) {
                    $GroupMembers = @()
                    ForEach ($Member in $_.GroupMembers) {
                        if ($Member -and ($Member.Trim() -ne '')) {
                            if ($Member -notmatch '^S-1-.*') {
                                # if the resulting member is username and not a SID, attempt to resolve it
                                $MemberSID = Convert-NNameTToSSid -Domain $Domain -ObjectName $Member | Select-Object -ExpandProperty SID
                                if ($MemberSID) {
                                    $GroupMembers += $MemberSID
                                }
                                else {
                                    $GroupMembers += $Member
                                }
                            }
                            else {
                                $GroupMembers += $Member
                            }
                        }
                    }
                    $_.GroupMembers = $GroupMembers
                }

                if ($ResolveMemberNames) {
                    $GroupMembers = @()
                    ForEach ($Member in $_.GroupMembers) {
                        if ($Member -and ($Member.Trim() -ne '')) {
                            if ($Member -match '^S-1-.*') {
                                # if the resulting member is a SID, attempt to resolve it
                                $MemberSID = Convert-SSidTToNName $Member 
                                if ($MemberSID) {
                                    $GroupMembers += $MemberSID
                                }
                                else {
                                    $GroupMembers += $Member
                                }
                            }
                            else {
                                $GroupMembers += $Member
                            }
                        }
                    }
                    $_.GroupMembers = $GroupMembers
                }

                $_ | Add-Member Noteproperty 'GPODisplayName' $GPODisplayName
                $_ | Add-Member Noteproperty 'GPOName' $GPOName
                $_ | Add-Member Noteproperty 'GPOType' 'GroupPolicyPreferences'
                $_
            }
        }
    }
    function Get-OObjectAAcl {
        [CmdletBinding()]
        Param (
            [Parameter(ValueFromPipelineByPropertyName = $True)]
            [String]
            $SamAccountName,

            [Parameter(ValueFromPipelineByPropertyName = $True)]
            [String]
            $Name = "*",

            [Parameter(ValueFromPipelineByPropertyName = $True)]
            [String]
            $DistinguishedName = "*",

            [Switch]
            $ResolveGUIDs,

            [String]
            $Filter,

            [String]
            $ADSpath,

            [String]
            $ADSprefix,

            [String]
            [ValidateSet("All", "ResetPassword", "WriteMembers", "DS-Replication-Get-Changes-All")]
            $RightsFilter,

            [String]
            $Domain,

            [String]
            $DomainController,

            [ValidateRange(1, 10000)] 
            [Int]
            $PageSize = 200
        )

        begin {
            $Searcher = Get-DDomainSSearcher -Domain $Domain -DomainController $DomainController -ADSpath $ADSpath -ADSprefix $ADSprefix -PageSize $PageSize 

            # get a GUID -> name mapping
            if ($ResolveGUIDs) {
                $GUIDs = Get-GGUIDMMap -Domain $Domain -DomainController $DomainController -PageSize $PageSize
            }
        }

        process {

            if ($Searcher) {

                if ($SamAccountName) {
                    $Searcher.filter = "(&(samaccountname=$SamAccountName)(name=$Name)(distinguishedname=$DistinguishedName)$Filter)"  
                }
                else {
                    $Searcher.filter = "(&(name=$Name)(distinguishedname=$DistinguishedName)$Filter)"  
                }
  
                try {
                    $Results = $Searcher.FindAll()
                    $Results | Where-Object { $_ } | ForEach-Object {
                        $Object = [adsi]($_.path)

                        if ($Object.distinguishedname) {
                            $Access = $Object.PsBase.ObjectSecurity.access
                            $Access | ForEach-Object {
                                $_ | Add-Member NoteProperty 'ObjectDN' $Object.distinguishedname[0]

                                if ($Object.objectsid[0]) {
                                    $S = (New-Object System.Security.Principal.SecurityIdentifier($Object.objectsid[0], 0)).Value
                                }
                                else {
                                    $S = $Null
                                }
                            
                                $_ | Add-Member NoteProperty 'ObjectSID' $S
                                $_
                            }
                        }
                    } | ForEach-Object {
                        if ($RightsFilter) {
                            $GuidFilter = Switch ($RightsFilter) {
                                "ResetPassword" { "00299570-246d-11d0-a768-00aa006e0529" }
                                "WriteMembers" { "bf9679c0-0de6-11d0-a285-00aa003049e2" }
                                "DS-Replication-Get-Changes-All" { "1131f6ad-9c07-11d1-f79f-00c04fc2dcd2" }
                                Default { "00000000-0000-0000-0000-000000000000" }
                            }
                            if ($_.ObjectType -eq $GuidFilter) { $_ }
                        }
                        else {
                            $_
                        }
                    } | ForEach-Object {
                        if ($GUIDs) {
                            # if we're resolving GUIDs, map them them to the resolved hash table
                            $AclProperties = @{}
                            $_.psobject.properties | ForEach-Object {
                                if ( ($_.Name -eq 'ObjectType') -or ($_.Name -eq 'InheritedObjectType') ) {
                                    try {
                                        $AclProperties[$_.Name] = $GUIDS[$_.Value.toString()]
                                    }
                                    catch {
                                        $AclProperties[$_.Name] = $_.Value
                                    }
                                }
                                else {
                                    $AclProperties[$_.Name] = $_.Value
                                }
                            }
                            New-Object -TypeName PSObject -Property $AclProperties
                        }
                        else { $_ }
                    }
                    $Results.dispose()
                    $Searcher.dispose()
                }
                catch {
                    Write-Warning $_
                }
            }
        }
    }
    function Convert-LLDAPPProperty {

        param(
            [Parameter(Mandatory = $True, ValueFromPipeline = $True)]
            [ValidateNotNullOrEmpty()]
            $Properties
        )

        $ObjectProperties = @{}

        $Properties.PropertyNames | ForEach-Object {
            if (($_ -eq "objectsid") -or ($_ -eq "sidhistory")) {
                # convert the SID to a string
                $ObjectProperties[$_] = (New-Object System.Security.Principal.SecurityIdentifier($Properties[$_][0], 0)).Value
            }
            elseif ($_ -eq "objectguid") {
                # convert the GUID to a string
                $ObjectProperties[$_] = (New-Object Guid (, $Properties[$_][0])).Guid
            }
            elseif ( ($_ -eq "lastlogon") -or ($_ -eq "lastlogontimestamp") -or ($_ -eq "pwdlastset") -or ($_ -eq "lastlogoff") -or ($_ -eq "badPasswordTime") ) {
                # convert timestamps
                if ($Properties[$_][0] -is [System.MarshalByRefObject]) {
                    # if we have a System.__ComObject
                    $Temp = $Properties[$_][0]
                    [Int32]$High = $Temp.GetType().InvokeMember("HighPart", [System.Reflection.BindingFlags]::GetProperty, $null, $Temp, $null)
                    [Int32]$Low = $Temp.GetType().InvokeMember("LowPart", [System.Reflection.BindingFlags]::GetProperty, $null, $Temp, $null)
                    $ObjectProperties[$_] = ([datetime]::FromFileTime([Int64]("0x{0:x8}{1:x8}" -f $High, $Low)))
                }
                else {
                    $ObjectProperties[$_] = ([datetime]::FromFileTime(($Properties[$_][0])))
                }
            }
            elseif ($Properties[$_][0] -is [System.MarshalByRefObject]) {
                # try to convert misc com objects
                $Prop = $Properties[$_]
                try {
                    $Temp = $Prop[$_][0]
                    Write-Verbose $_
                    [Int32]$High = $Temp.GetType().InvokeMember("HighPart", [System.Reflection.BindingFlags]::GetProperty, $null, $Temp, $null)
                    [Int32]$Low = $Temp.GetType().InvokeMember("LowPart", [System.Reflection.BindingFlags]::GetProperty, $null, $Temp, $null)
                    $ObjectProperties[$_] = [Int64]("0x{0:x8}{1:x8}" -f $High, $Low)
                }
                catch {
                    $ObjectProperties[$_] = $Prop[$_]
                }
            }
            elseif ($Properties[$_].count -eq 1) {
                $ObjectProperties[$_] = $Properties[$_][0]
            }
            else {
                $ObjectProperties[$_] = $Properties[$_]
            }
        }

        New-Object -TypeName PSObject -Property $ObjectProperties
    }
    filter Convert-NNameTToSSid {

        [CmdletBinding()]
        param(
            [Parameter(Mandatory = $True, ValueFromPipeline = $True)]
            [String]
            [Alias('Name')]
            $ObjectName,

            [String]
            $Domain
        )

        $ObjectName = $ObjectName -Replace "/", "\"
    
        if ($ObjectName.Contains("\")) {
            # if we get a DOMAIN\user format, auto convert it
            $Domain = $ObjectName.Split("\")[0]
            $ObjectName = $ObjectName.Split("\")[1]
        }
        elseif (-not $Domain) {
            $Domain = (Get-NNetDDomain).Name
        }

        try {
            $Obj = (New-Object System.Security.Principal.NTAccount($Domain, $ObjectName))
            $SID = $Obj.Translate([System.Security.Principal.SecurityIdentifier]).Value
        
            $Out = New-Object PSObject
            $Out | Add-Member Noteproperty 'ObjectName' $ObjectName
            $Out | Add-Member Noteproperty 'SID' $SID
            $Out
        }
        catch {
            Write-Verbose "Invalid object/name: $Domain\$ObjectName"
            $Null
        }
    }

    function Convert-AADNName {
        [CmdletBinding()]
        param(
            [Parameter(Mandatory = $True, ValueFromPipeline = $True)]
            [String]
            $ObjectName,

            [String]
            [ValidateSet("NT4", "Simple", "Canonical")]
            $InputType,

            [String]
            [ValidateSet("NT4", "Simple", "Canonical")]
            $OutputType
        )

        $NameTypes = @{
            'Canonical' = 2
            'NT4'       = 3
            'Simple'    = 5
        }

        if (-not $PSBoundParameters['InputType']) {
            if ( ($ObjectName.split('/')).Count -eq 2 ) {
                $ObjectName = $ObjectName.replace('/', '\')
            }

            if ($ObjectName -match "^[A-Za-z]+\\[A-Za-z ]+") {
                $InputType = 'NT4'
            }
            elseif ($ObjectName -match "^[A-Za-z ]+@[A-Za-z\.]+") {
                $InputType = 'Simple'
            }
            elseif ($ObjectName -match "^[A-Za-z\.]+/[A-Za-z]+/[A-Za-z/ ]+") {
                $InputType = 'Canonical'
            }
            else {
                Write-Warning "Can not identify InType for $ObjectName"
                return $ObjectName
            }
        }
        elseif ($InputType -eq 'NT4') {
            $ObjectName = $ObjectName.replace('/', '\')
        }

        if (-not $PSBoundParameters['OutputType']) {
            $OutputType = Switch ($InputType) {
                'NT4' { 'Canonical' }
                'Simple' { 'NT4' }
                'Canonical' { 'NT4' }
            }
        }

        $Domain = Switch ($InputType) {
            'NT4' { $ObjectName.split("\")[0] }
            'Simple' { $ObjectName.split("@")[1] }
            'Canonical' { $ObjectName.split("/")[0] }
        }

        function Invoke-Method([__ComObject] $Object, [String] $Method, $Parameters) {
            $Output = $Object.GetType().InvokeMember($Method, "InvokeMethod", $Null, $Object, $Parameters)
            if ( $Output ) { $Output }
        }
        function Set-Property([__ComObject] $Object, [String] $Property, $Parameters) {
            [Void] $Object.GetType().InvokeMember($Property, "SetProperty", $Null, $Object, $Parameters)
        }

        $Translate = New-Object -ComObject NameTranslate

        try {
            Invoke-Method $Translate "Init" (1, $Domain)
        }
        catch [System.Management.Automation.MethodInvocationException] { 
            Write-Verbose "Error with translate init: $_"
        }

        Set-Property $Translate "ChaseReferral" (0x60)

        try {
            Invoke-Method $Translate "Set" ($NameTypes[$InputType], $ObjectName)
        (Invoke-Method $Translate "Get" ($NameTypes[$OutputType]))
        }
        catch [System.Management.Automation.MethodInvocationException] {
            Write-Verbose "Error with translate: $_"
        }
    }
    function Get-AADOObject {
        [CmdletBinding()]
        Param (
            [Parameter(ValueFromPipeline = $True)]
            [String]
            $SID,

            [String]
            $Name,

            [String]
            $SamAccountName,

            [String]
            $Domain,

            [String]
            $DomainController,

            [String]
            $ADSpath,

            [String]
            $Filter,

            [Switch]
            $ReturnRaw,

            [ValidateRange(1, 10000)] 
            [Int]
            $PageSize = 200,

            [Management.Automation.PSCredential]
            $Credential
        )
        process {
            if ($SID) {
                try {
                    $Name = Convert-SSidTToNName $SID
                    if ($Name) {
                        $Canonical = Convert-AADNName -ObjectName $Name -InputType NT4 -OutputType Canonical
                        if ($Canonical) {
                            $Domain = $Canonical.split("/")[0]
                        }
                        else {
                            Write-Warning "Error resolving SID '$SID'"
                            return $Null
                        }
                    }
                }
                catch {
                    Write-Warning "Error resolving SID '$SID' : $_"
                    return $Null
                }
            }

            $ObjectSearcher = Get-DDomainSSearcher -Domain $Domain -DomainController $DomainController -Credential $Credential -ADSpath $ADSpath -PageSize $PageSize

            if ($ObjectSearcher) {
                if ($SID) {
                    $ObjectSearcher.filter = "(&(objectsid=$SID)$Filter)"
                }
                elseif ($Name) {
                    $ObjectSearcher.filter = "(&(name=$Name)$Filter)"
                }
                elseif ($SamAccountName) {
                    $ObjectSearcher.filter = "(&(samAccountName=$SamAccountName)$Filter)"
                }

                $Results = $ObjectSearcher.FindAll()
                $Results | Where-Object { $_ } | ForEach-Object {
                    if ($ReturnRaw) {
                        $_
                    }
                    else {
                        Convert-LLDAPPProperty -Properties $_.Properties
                    }
                }
                $Results.dispose()
                $ObjectSearcher.dispose()
            }
        }
    }
    function Get-DDomainSSID {
        param(
            [String]
            $Domain,

            [String]
            $DomainController
        )

        $DCSID = Get-NNetCComputer -Domain $Domain -DomainController $DomainController -FullData -Filter '(userAccountControl:1.2.840.113556.1.4.803:=8192)' | Select-Object -First 1 -ExpandProperty objectsid
        if ($DCSID) {
            $DCSID.Substring(0, $DCSID.LastIndexOf('-'))
        }
        else {
            Write-Verbose "Error getting domain SID for $Domain"
        }
    }
    filter Get-GGptTTmpl {

        [CmdletBinding()]
        Param (
            [Parameter(Mandatory = $True, ValueFromPipeline = $True)]
            [String]
            $GptTmplPath,

            [Switch]
            $UsePSDrive
        )

        if ($UsePSDrive) {
            # if we're PSDrives, create a temporary mount point
            $Parts = $GptTmplPath.split('\')
            $FolderPath = $Parts[0..($Parts.length - 2)] -join '\'
            $FilePath = $Parts[-1]
            $RandDrive = ("abcdefghijklmnopqrstuvwxyz".ToCharArray() | Get-Random -Count 7) -join ''

            Write-Verbose "Mounting path $GptTmplPath using a temp PSDrive at $RandDrive"

            try {
                $Null = New-PSDrive -Name $RandDrive -PSProvider FileSystem -Root $FolderPath  -ErrorAction Stop
            }
            catch {
                Write-Verbose "Error mounting path $GptTmplPath : $_"
                return $Null
            }

            # so we can cd/dir the new drive
            $TargetGptTmplPath = $RandDrive + ":\" + $FilePath
        }
        else {
            $TargetGptTmplPath = $GptTmplPath
        }

        Write-Verbose "GptTmplPath: $GptTmplPath"

        try {
            Write-Verbose "Parsing $TargetGptTmplPath"
            $TargetGptTmplPath | Get-IniContent -ErrorAction SilentlyContinue
        }
        catch {
            Write-Verbose "Error parsing $TargetGptTmplPath : $_"
        }

        if ($UsePSDrive -and $RandDrive) {
            Write-Verbose "Removing temp PSDrive $RandDrive"
            Get-PSDrive -Name $RandDrive -ErrorAction SilentlyContinue | Remove-PSDrive -Force
        }
    }
    filter Get-GGUIDMMap {

        Param (
            [Parameter(ValueFromPipeline = $True)]
            [String]
            $Domain,

            [String]
            $DomainController,

            [ValidateRange(1, 10000)] 
            [Int]
            $PageSize = 200
        )

        $GUIDs = @{'00000000-0000-0000-0000-000000000000' = 'All' }
        $SearchString = ("LDAP://" + $cur_for.Schema.Name )
        $Searcher = New-Object System.DirectoryServices.DirectorySearcher([ADSI]$SearchString)
        $Searcher.PageSize = $PageSize
        $Searcher.CacheResults = $False
        $Searcher.filter = "(schemaIDGUID=*)"

        <# $SchemaPath = (Get-NNetFForest).schema.name
    $SchemaSearcher = Get-DDomainSSearcher -ADSpath $SchemaPath -DomainController $DomainController -PageSize 200
    #>
        $SchemaSearcher = $Searcher    
        if ($SchemaSearcher) { 
            $SchemaSearcher.filter = "(schemaIDGUID=*)"
            try {
                $Results = $SchemaSearcher.FindAll()
                $Results | Where-Object { $_ } | ForEach-Object {
                    # convert the GUID
                    $GUIDs[(New-Object Guid (, $_.properties.schemaidguid[0])).Guid] = $_.properties.name[0]
                }
                $Results.dispose()
                $SchemaSearcher.dispose()
            }
            catch {
                Write-Verbose "Error in building GUID map: $_"
            }
        }

        <# 
    $RightsSearcher = Get-DDomainSSearcher -ADSpath $SchemaPath.replace("Schema","Extended-Rights") -DomainController $DomainController -PageSize 200 -Credential $Credential
    #>
        $SearchString = ("LDAP://" + $cur_for.Schema.Name -replace 'Schema', 'Extended-Rights')
        $Searcher = New-Object System.DirectoryServices.DirectorySearcher([ADSI]$SearchString)
        $Searcher.PageSize = $PageSize
        $Searcher.CacheResults = $False
        $RightsSearcher = $Searcher


        if ($RightsSearcher) {
            $RightsSearcher.filter = "(objectClass=controlAccessRight)"
            try {
                $Results = $RightsSearcher.FindAll()
                $Results | Where-Object { $_ } | ForEach-Object {
                    # convert the GUID
                    $GUIDs[$_.properties.rightsguid[0].toString()] = $_.properties.name[0]
                }
                $Results.dispose()
                $RightsSearcher.dispose()
            }
            catch {
                Write-Verbose "Error in building GUID map: $_"
            }
        }

        $GUIDs
    }
    filter Get-GGroupsXXML {

        [CmdletBinding()]
        Param (
            [Parameter(Mandatory = $True, ValueFromPipeline = $True)]
            [String]
            $GroupsXMLPath,

            [Switch]
            $UsePSDrive
        )

        if ($UsePSDrive) {
            # if we're PSDrives, create a temporary mount point
            $Parts = $GroupsXMLPath.split('\')
            $FolderPath = $Parts[0..($Parts.length - 2)] -join '\'
            $FilePath = $Parts[-1]
            $RandDrive = ("abcdefghijklmnopqrstuvwxyz".ToCharArray() | Get-Random -Count 7) -join ''

            Write-Verbose "Mounting path $GroupsXMLPath using a temp PSDrive at $RandDrive"

            try {
                $Null = New-PSDrive -Name $RandDrive -PSProvider FileSystem -Root $FolderPath  -ErrorAction Stop
            }
            catch {
                Write-Verbose "Error mounting path $GroupsXMLPath : $_"
                return $Null
            }

            # so we can cd/dir the new drive
            $TargetGroupsXMLPath = $RandDrive + ":\" + $FilePath
        }
        else {
            $TargetGroupsXMLPath = $GroupsXMLPath
        }

        try {
            [XML]$GroupsXMLcontent = Get-Content $TargetGroupsXMLPath -ErrorAction Stop

            # process all group properties in the XML
            $GroupsXMLcontent | Select-Xml "/Groups/Group" | Select-Object -ExpandProperty node | ForEach-Object {

                $Groupname = $_.Properties.groupName

                # extract the localgroup sid for memberof
                $GroupSID = $_.Properties.groupSid
                if (-not $GroupSID) {
                    if ($Groupname -match 'Administrators') {
                        $GroupSID = 'S-1-5-32-544'
                    }
                    elseif ($Groupname -match 'Remote Desktop') {
                        $GroupSID = 'S-1-5-32-555'
                    }
                    elseif ($Groupname -match 'Guests') {
                        $GroupSID = 'S-1-5-32-546'
                    }
                    else {
                        $GroupSID = Convert-NNameTToSSid -ObjectName $Groupname | Select-Object -ExpandProperty SID
                    }
                }

                # extract out members added to this group
                $Members = $_.Properties.members | Select-Object -ExpandProperty Member | Where-Object { $_.action -match 'ADD' } | ForEach-Object {
                    if ($_.sid) { $_.sid }
                    else { $_.name }
                }

                if ($Members) {

                    # extract out any/all filters...I hate you GPP
                    if ($_.filters) {
                        $Filters = $_.filters.GetEnumerator() | ForEach-Object {
                            New-Object -TypeName PSObject -Property @{'Type' = $_.LocalName; 'Value' = $_.name }
                        }
                    }
                    else {
                        $Filters = $Null
                    }

                    if ($Members -isnot [System.Array]) { $Members = @($Members) }

                    $GPOGroup = New-Object PSObject
                    $GPOGroup | Add-Member Noteproperty 'GPOPath' $TargetGroupsXMLPath
                    $GPOGroup | Add-Member Noteproperty 'Filters' $Filters
                    $GPOGroup | Add-Member Noteproperty 'GroupName' $GroupName
                    $GPOGroup | Add-Member Noteproperty 'GroupSID' $GroupSID
                    $GPOGroup | Add-Member Noteproperty 'GroupMemberOf' $Null
                    $GPOGroup | Add-Member Noteproperty 'GroupMembers' $Members
                    $GPOGroup
                }
            }
        }
        catch {
            Write-Verbose "Error parsing $TargetGroupsXMLPath : $_"
        }

        if ($UsePSDrive -and $RandDrive) {
            Write-Verbose "Removing temp PSDrive $RandDrive"
            Get-PSDrive -Name $RandDrive -ErrorAction SilentlyContinue | Remove-PSDrive -Force
        }
    }
    filter Get-DDomainSSearcher {

        param(
            [Parameter(ValueFromPipeline = $True)]
            [String]
            $Domain,

            [String]
            $DomainController,

            [String]
            $ADSpath,

            [String]
            $ADSprefix,

            [ValidateRange(1, 10000)] 
            [Int]
            $PageSize = 200,

            [Management.Automation.PSCredential]
            $Credential
        )

        $Domain = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain().name
        $DomainController = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain().DomainControllers[0].name

        $SearchString = "LDAP://"
        if ($DomainController) {
            $SearchString += $DomainController
            $SearchString += '/'
        }

        if ($Domain -and ($Domain.Trim() -ne "")) {
            $DN = "DC=$($Domain.Replace('.', ',DC='))"
        }
    
        $SearchString += $DN
        Write-Verbose "Get-DDomainSSearcher search string: $SearchString"

        $Searcher = New-Object System.DirectoryServices.DirectorySearcher([ADSI]$SearchString)
        $Searcher.PageSize = $PageSize
        $Searcher.CacheResults = $False
        $Searcher
    }
    function Invoke-AACLSScanner {

        [CmdletBinding()]
        Param (
            [Parameter(ValueFromPipeline = $True)]
            [String]
            $SamAccountName,

            [String]
            $Name = "*",

            [Alias('DN')]
            [String]
            $DistinguishedName = "*",

            [String]
            $Filter,

            [String]
            $ADSpath,

            [String]
            $ADSprefix,

            [String]
            $Domain,

            [String]
            $DomainController,

            [Switch]
            $ResolveGUIDs,

            [ValidateRange(1, 10000)] 
            [Int]
            $PageSize = 200
        )

        # Get all domain ACLs with the appropriate parameters
        Get-OObjectAACL @PSBoundParameters | ForEach-Object {
            # add in the translated SID for the object identity
            $_ | Add-Member Noteproperty 'IdentitySID' ($_.IdentityReference.Translate([System.Security.Principal.SecurityIdentifier]).Value)
            $_
        } | Where-Object {
            # check for any ACLs with SIDs > -1000
            try {
                # TODO: change this to a regex for speedup?
                [int]($_.IdentitySid.split("-")[-1]) -ge 1000
            }
            catch {}
        } | Where-Object {
            # filter for modifiable rights
        ($_.ActiveDirectoryRights -eq "GenericAll") -or ($_.ActiveDirectoryRights -match "Write") -or ($_.ActiveDirectoryRights -match "Create") -or ($_.ActiveDirectoryRights -match "Delete") -or (($_.ActiveDirectoryRights -match "ExtendedRight") -and ($_.AccessControlType -eq "Allow"))
        }
    }
    function Get-AADOObjectIInfo {

        param ($object)

        $sid = (Convert-NNameTToSSid $object).SID
        $obj = Get-AADOObject -SID $sid

        if ($obj.objectcategory -match 'CN=Person') {
            $user = $true
        }

        if ($user) {
            $groups = Get-NNetGGroup -UserName $object
        }
        Write-Host -Fore DarkCyan `n$object
        Write-Host `nCREATED:`t $obj.whenCreated
        Write-Host CHANGED:`t $obj.whenChanged
        Write-Host SID:`t`t $sid`n

        ########### AD Groups  ####################

        $elev_groups = @(
            '$cur_dom\Account Operators'
            '$cur_dom\Administrators'
            '$cur_dom\Allowed RODC Password Replication Group'
            '$cur_dom\Backup Operators'
            '$cur_dom\Certificate Service DCOM Access'
            '$cur_dom\Cert Publishers'
            '$cur_dom\Distributed COM Users'
            '$cur_dom\DnsAdmins'
            '$cur_dom\Domain Admins'
            '$cur_dom\Enterprise Admins'
            '$cur_dom\Event Log Readers'
            '$cur_dom\Group Policy Creators Owners'
            '$cur_dom\Hyper-V Administrators'
            '$cur_dom\Pre–Windows 2000 Compatible Access'
            '$cur_dom\Print Operators'
            '$cur_dom\Protected Users'
            '$cur_dom\Remote Desktop Users'
            '$cur_dom\Schema Admins'
            '$cur_dom\Server Operators'
            '$cur_dom\WinRMRemoteWMIUsers_'
        )
        Write-Host -Fore Yellow "Membership in AD groups: "`n

        if ($groups) {
            $groups | Foreach-Object {
                if ($elev_groups -contains $_) {
                    Write-Host -Fore Red $_
                }
                else {
                    Write-Host $_
                }
            }
        }
        else {
            Write-Host "None"`n
        }
        ######### Local Groups  ##############

        $gpohits = @()
        if (!($GPOResults)) {
            $global:GPOResults = Get-NNetGGpoGGroup
        }

        $GPOResults | Foreach-Object {
            if ($_.GroupMembers -contains $sid) {
                $gpohits += New-Object -TypeName psobject -Property @{
                    Group = $_.GroupName;
                    GPO   = $_.GPODisplayName;
                    Name  = $_.GPOName.trim('{}')
                }
            }
        }

        Write-Host -Fore Yellow `n"Membership in local groups via GPO: "`n
	
        if ($gpohits) {
            $gpohits = $gpohits | Sort-Object -unique
            Write-Host "Member of"$gpohits.Group"via policy"$gpohits.Name `n
        }
        else {
            Write-Host "None"`n
        }

        $o = Get-LinkedGpos $object
        Write-Host -Fore Yellow "Linked GPOs: "`n

        if ($o) {
            $o | Foreach-Object { Get-GPO -guid $_ | Select-Object Id, DisplayName, Owner }
        }
        else {
            Write-Host "None"`n
        }
    }
    function Get-UserRightsAssignments {

        param ($guid)

        [xml]$xml = Get-GPOReport -Guid $guid -ReportType xml
        $Rights = $xml.GPO.Computer.ExtensionData.Extension.UserRightsAssignment
        $a = @()
        Foreach ($right in $Rights) {
            $a += New-Object -TypeName psobject -Property @{
                UserRight = $right.Name;
                Users     = $right.Member.Name.'#text';
            }
        }
        $a | Select-Object UserRight, Users | Format-Table -auto
    }
    function Get-LinkedGpos {

        param ($object)

        $dn = (Get-AADOObject -Name $object).distinguishedName

        $ok = $dn.split(',')
        $new = $ok | Where-Object { $_ -notmatch 'CN=' }
        $path = $new -join (',')

        $gpos = @()
        while ($path -match 'OU=') {
            $gpos += (Get-ADOrganizationalUnit $path).LinkedGroupPolicyObjects
            $arr = $path.split(',')
            $first, $rest = $arr
            $path = $rest -join (',')
        }
        $gpos | Foreach-Object { $_.split('{')[1].split('}')[0] }
    }
    function Get-DDomainOObject {
 
        [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseDeclaredVarsMoreThanAssignments', '')]
        [OutputType('PowerView.ADObject')]
        [OutputType('PowerView.ADObject.Raw')]
        [CmdletBinding()]
        Param(
            [Parameter(Position = 0, ValueFromPipeline = $True, ValueFromPipelineByPropertyName = $True)]
            [Alias('DistinguishedName', 'SamAccountName', 'Name', 'MemberDistinguishedName', 'MemberName')]
            [String[]]
            $Identity,
  
            [ValidateNotNullOrEmpty()]
            [String]
            $Domain,
  
            [ValidateNotNullOrEmpty()]
            [Alias('Filter')]
            [String]
            $LDAPFilter,
  
            [ValidateNotNullOrEmpty()]
            [String[]]
            $Properties,
  
            [ValidateNotNullOrEmpty()]
            [Alias('ADSPath')]
            [String]
            $SearchBase,
  
            [ValidateNotNullOrEmpty()]
            [Alias('DomainController')]
            [String]
            $Server,
  
            [ValidateSet('Base', 'OneLevel', 'Subtree')]
            [String]
            $SearchScope = 'Subtree',
  
            [ValidateRange(1, 10000)]
            [Int]
            $ResultPageSize = 200,
  
            [ValidateRange(1, 10000)]
            [Int]
            $ServerTimeLimit,
  
            [ValidateSet('Dacl', 'Group', 'None', 'Owner', 'Sacl')]
            [String]
            $SecurityMasks,
  
            [Switch]
            $Tombstone,
  
            [Alias('ReturnOne')]
            [Switch]
            $FindOne,
  
            [Management.Automation.PSCredential]
            [Management.Automation.CredentialAttribute()]
            $Credential = [Management.Automation.PSCredential]::Empty,
  
            [Switch]
            $Raw
        )
  
        DynamicParam {
            $UACValueNames = [Enum]::GetNames($UACEnum)
            # add in the negations
            $UACValueNames = $UACValueNames | ForEach-Object { $_; "NOT_$_" }
            # create new dynamic parameter
            New-DynamicParameter -Name UACFilter -ValidateSet $UACValueNames -Type ([array])
        }
  
        BEGIN {
            $SearcherArguments = @{}
            if ($PSBoundParameters['Domain']) { $SearcherArguments['Domain'] = $Domain }
            if ($PSBoundParameters['Properties']) { $SearcherArguments['Properties'] = $Properties }
            if ($PSBoundParameters['SearchBase']) { $SearcherArguments['SearchBase'] = $SearchBase }
            if ($PSBoundParameters['Server']) { $SearcherArguments['Server'] = $Server }
            if ($PSBoundParameters['SearchScope']) { $SearcherArguments['SearchScope'] = $SearchScope }
            if ($PSBoundParameters['ResultPageSize']) { $SearcherArguments['ResultPageSize'] = $ResultPageSize }
            if ($PSBoundParameters['ServerTimeLimit']) { $SearcherArguments['ServerTimeLimit'] = $ServerTimeLimit }
            if ($PSBoundParameters['SecurityMasks']) { $SearcherArguments['SecurityMasks'] = $SecurityMasks }
            if ($PSBoundParameters['Tombstone']) { $SearcherArguments['Tombstone'] = $Tombstone }
            if ($PSBoundParameters['Credential']) { $SearcherArguments['Credential'] = $Credential }
            $ObjectSearcher = Get-DomainSearcher @SearcherArguments
        }
  
        PROCESS {
            #bind dynamic parameter to a friendly variable
            if ($PSBoundParameters -and ($PSBoundParameters.Count -ne 0)) {
                New-DynamicParameter -CreateVariables -BoundParameters $PSBoundParameters
            }
            if ($ObjectSearcher) {
                $IdentityFilter = ''
                $Filter = ''
                $Identity | Where-Object { $_ } | ForEach-Object {
                    $IdentityInstance = $_.Replace('(', '\28').Replace(')', '\29')
                    if ($IdentityInstance -match '^S-1-') {
                        $IdentityFilter += "(objectsid=$IdentityInstance)"
                    }
                    elseif ($IdentityInstance -match '^(CN|OU|DC)=') {
                        $IdentityFilter += "(distinguishedname=$IdentityInstance)"
                        if ((-not $PSBoundParameters['Domain']) -and (-not $PSBoundParameters['SearchBase'])) {
                            # if a -Domain isn't explicitly set, extract the object domain out of the distinguishedname
                            #   and rebuild the domain searcher
                            $IdentityDomain = $IdentityInstance.SubString($IdentityInstance.IndexOf('DC=')) -replace 'DC=', '' -replace ',', '.'
                            Write-Verbose "[Get-DomainObject] Extracted domain '$IdentityDomain' from '$IdentityInstance'"
                            $SearcherArguments['Domain'] = $IdentityDomain
                            $ObjectSearcher = Get-DomainSearcher @SearcherArguments
                            if (-not $ObjectSearcher) {
                                Write-Warning "[Get-DomainObject] Unable to retrieve domain searcher for '$IdentityDomain'"
                            }
                        }
                    }
                    elseif ($IdentityInstance -imatch '^[0-9A-F]{8}-([0-9A-F]{4}-){3}[0-9A-F]{12}$') {
                        $GuidByteString = (([Guid]$IdentityInstance).ToByteArray() | ForEach-Object { '\' + $_.ToString('X2') }) -join ''
                        $IdentityFilter += "(objectguid=$GuidByteString)"
                    }
                    elseif ($IdentityInstance.Contains('\')) {
                        $ConvertedIdentityInstance = $IdentityInstance.Replace('\28', '(').Replace('\29', ')') | Convert-ADName -OutputType Canonical
                        if ($ConvertedIdentityInstance) {
                            $ObjectDomain = $ConvertedIdentityInstance.SubString(0, $ConvertedIdentityInstance.IndexOf('/'))
                            $ObjectName = $IdentityInstance.Split('\')[1]
                            $IdentityFilter += "(samAccountName=$ObjectName)"
                            $SearcherArguments['Domain'] = $ObjectDomain
                            Write-Verbose "[Get-DomainObject] Extracted domain '$ObjectDomain' from '$IdentityInstance'"
                            $ObjectSearcher = Get-DomainSearcher @SearcherArguments
                        }
                    }
                    elseif ($IdentityInstance.Contains('.')) {
                        $IdentityFilter += "(|(samAccountName=$IdentityInstance)(name=$IdentityInstance)(dnshostname=$IdentityInstance))"
                    }
                    else {
                        $IdentityFilter += "(|(samAccountName=$IdentityInstance)(name=$IdentityInstance)(displayname=$IdentityInstance))"
                    }
                }
                if ($IdentityFilter -and ($IdentityFilter.Trim() -ne '') ) {
                    $Filter += "(|$IdentityFilter)"
                }
  
                if ($PSBoundParameters['LDAPFilter']) {
                    Write-Verbose "[Get-DomainObject] Using additional LDAP filter: $LDAPFilter"
                    $Filter += "$LDAPFilter"
                }
  
                # build the LDAP filter for the dynamic UAC filter value
                $UACFilter | Where-Object { $_ } | ForEach-Object {
                    if ($_ -match 'NOT_.*') {
                        $UACField = $_.Substring(4)
                        $UACValue = [Int]($UACEnum::$UACField)
                        $Filter += "(!(userAccountControl:1.2.840.113556.1.4.803:=$UACValue))"
                    }
                    else {
                        $UACValue = [Int]($UACEnum::$_)
                        $Filter += "(userAccountControl:1.2.840.113556.1.4.803:=$UACValue)"
                    }
                }
  
                if ($Filter -and $Filter -ne '') {
                    $ObjectSearcher.filter = "(&$Filter)"
                }
                Write-Verbose "[Get-DomainObject] Get-DomainObject filter string: $($ObjectSearcher.filter)"
  
                if ($PSBoundParameters['FindOne']) { $Results = $ObjectSearcher.FindOne() }
                else { $Results = $ObjectSearcher.FindAll() }
                $Results | Where-Object { $_ } | ForEach-Object {
                    if ($PSBoundParameters['Raw']) {
                        # return raw result objects
                        $Object = $_
                        $Object.PSObject.TypeNames.Insert(0, 'PowerView.ADObject.Raw')
                    }
                    else {
                        $Object = Convert-LDAPProperty -Properties $_.Properties
                        $Object.PSObject.TypeNames.Insert(0, 'PowerView.ADObject')
                    }
                    $Object
                }
                if ($Results) {
                    try { $Results.dispose() }
                    catch {
                        Write-Verbose "[Get-DomainObject] Error disposing of the Results object: $_"
                    }
                }
                $ObjectSearcher.dispose()
            }
        }
    }
    function Get-OnPremSearcher {

        param(
            [Parameter(ValueFromPipeline = $True)]
            [String]
            $Domain,
  
            [String]
            $DomainController,
  
            [String]
            $ADSpath,
  
            [String]
            $ADSprefix,
  
            [ValidateRange(1, 10000)] 
            [Int]
            $PageSize = 200,
  
            [Management.Automation.PSCredential]
            $Credential
        )
  
        $Domain = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain().name
        $DomainController = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain().DomainControllers[0].name
  
        $SearchString = "LDAP://"
        if ($DomainController) {
            $SearchString += $DomainController
            $SearchString += '/'
        }
  
        if ($Domain -and ($Domain.Trim() -ne "")) {
            $DN = "DC=$($Domain.Replace('.', ',DC='))"
        }
    
        $SearchString += $DN
        $Searcher = New-Object System.DirectoryServices.DirectorySearcher([ADSI]$SearchString)
        $Searcher.PageSize = $PageSize
        $Searcher.CacheResults = $False
        $Searcher
    }
    function Get-OnPremUser {
  
        param(
            [Parameter(Position = 0, ValueFromPipeline = $True)]
            [String]
            $UserName,
  
            [String]
            $Domain,
  
            [String]
            $DomainController,
  
            [String]
            $ADSpath,
  
            [String]
            $Filter,
  
            [Switch]
            $SPN,
  
            [Switch]
            $AdminCount,
  
            [Switch]
            $Unconstrained,
  
            [Switch]
            $AllowDelegation,
  
            [ValidateRange(1, 10000)] 
            [Int]
            $PageSize = 200,
  
            [Management.Automation.PSCredential]
            $Credential
        )
  
        begin {
            # so this isn't repeated if users are passed on the pipeline
            $UserSearcher = Get-OnPremSearcher -Domain $Domain -ADSpath $ADSpath -DomainController $DomainController -PageSize $PageSize -Credential $Credential
        }
  
        process {
            if ($UserSearcher) {
  
                # if we're checking for unconstrained delegation
                if ($Unconstrained) {
                    Write-Verbose "Checking for unconstrained delegation"
                    $Filter += "(userAccountControl:1.2.840.113556.1.4.803:=524288)"
                }
                if ($AllowDelegation) {
                    Write-Verbose "Checking for users who can be delegated"
                    # negation of "Accounts that are sensitive and not trusted for delegation"
                    $Filter += "(!(userAccountControl:1.2.840.113556.1.4.803:=1048574))"
                }
                if ($AdminCount) {
                    Write-Verbose "Checking for adminCount=1"
                    $Filter += "(admincount=1)"
                }
  
                # check if we're using a username filter or not
                if ($UserName) {
                    # samAccountType=805306368 indicates user objects
                    $UserSearcher.filter = "(&(samAccountType=805306368)(samAccountName=$UserName)$Filter)"
                }
                elseif ($SPN) {
                    $UserSearcher.filter = "(&(samAccountType=805306368)(servicePrincipalName=*)$Filter)"
                }
                else {
                    # filter is something like "(samAccountName=*blah*)" if specified
                    $UserSearcher.filter = "(&(samAccountType=805306368)$Filter)"
                }
  
                $Results = $UserSearcher.FindAll()
                $Results | Where-Object { $_ } | ForEach-Object {
                    # convert/process the LDAP fields for each result
                    $User = Convert-LLDAPPProperty -Properties $_.Properties
                    $User.PSObject.TypeNames.Add('PowerVewe.User')
                    $User
                }
                $Results.dispose()
                $UserSearcher.dispose()
            }
        }
    }
    function Get-Adsi {
        param(
            [string]$arg
        )
  
        Clear-Vars os 
  
        if ($arg -notmatch 'LDAP') {
            $global:col = $arg.split('.') | Foreach-Object { "DC=" + $_ }
            $global:col2 = $col -join ','
            $global:ldap = "LDAP://" + $col2 
        }
        else {
            $global:ldap = $arg 
        }
        $global:os = [adsisearcher]""
        $os.Filter = "(objectClass=trustedDomain)"
        $os.SearchRoot = [ADSI]$ldap
        try { 
            $os.FindAll().Properties
        }
        catch {
  
        }
    }
    function Get-ADTrusts {
        Clear-Vars adsiRes 
  
        $global:curDom = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()
        $global:curFor = [System.DirectoryServices.ActiveDirectory.Forest]::GetCurrentForest()
        $global:ft = $curFor.GetAllTrustRelationships()
        $global:dt = $curDom.GetAllTrustRelationships()
  
        # $dt + $ft | Select-Object SourceName,TargetName,TrustType,TrustDirection
  
        $global:names = $ft.TrustedDomainInformation.DnsName + $ft.TopLevelNames.Name + $curDom.Name + $curFor.Name 
  
        $global:obj = [System.Collections.ArrayList]::new()
  
        $names | Foreach-Object {
            $name = $_ 
            $adsiRes = Get-Adsi $name
      
            $adsiRes | Foreach-Object {
                $o = New-Object -Typename psobject -Property @{
                    name            = $name
                    trustpartner    = $_.trustpartner[0]
                    trustdirection  = $_.trustdirection[0]
                    trusttype       = $_.trusttype[0]
                    trustattributes = $_.trustattributes[0]
                }
                [void]$obj.Add($o)
            }
        }
        $obj = $obj | Sort-Object -unique name, trustpartner, trustdirection
        $obj | Foreach-Object {
            $_.trustdirection = $trustdirections.Item([string]$_.trustdirection)
            $_.trusttype = $trusttypes.Item([string]$_.trusttype)
            $_.trustattributes = $trustattributes.Item([string]$_.trustattributes)
        }
     
        $obj | Select-Object name, trustpartner, trustdirection, trusttype, trustattributes | Sort-Object name, trustdirection | Format-Table -auto
    }

    function Get-RiskDetections {
        $riskDetections | Select-String Name, Type, Desc
    }
    function Get-ConditionalAccessPolicies {
        $conditionalAccessPolicies | Select-String Name, Type, Desc
    }
	function Write-Decode {
	  [cmdletbinding()]param(
	    [parameter(ValueFromPipeline)]
	    [string]$string
	  )
	  Clear-Vars out
	  $out      = [System.Web.HttpUtility]::HtmlDecode($string)
	  $out      = [System.Web.HttpUtility]::UrlDecode($out)
	  return $out 
	}
    function Get-AccountStatus {
          param(
               [array]$accounts
           )
	 if (!(Test-IsConnected)) {
             Write-Fail 'You must be connected to the VPN'
	     Break
	 }
	   
         $out = ForEach ($account in $accounts) {
           $obj = '';
           $obj = get-user $account -o
           $date = date
           $days = ($date.ToUniversalTime() - $obj.lastlogontimestamp).Days
           if ($days -ge 23) {
              $status = 'needs attention'
           }
           elseif ($days -gt 18) {
              $status = 'caution'
           }
           else {
              $status = 'good'
           }
           New-Object -TypeName psobject -Property @{
              samaccountname = $obj.samaccountname
              lastlogontimestamp = $obj.lastlogontimestamp
              status = $status
              days  = 30 - $days
           }
         }
         $out | Sort-Object lastlogontimestamp | Write-HostColored -PatternColorMap @{'needs'="red";'good'="green";'caution'="yellow"} -WholeLine
    }


    ############################

    ########## MAIN ############

    ############################


    $global:DirectoryToolsFunctions = $DirectoryToolsCmdlets.Name

    $DirectoryToolsCmdlets | Foreach-Object { New-Alias -Name $_.Alias -Value $_.Name -Scope Global -Force }

    Export-ModuleMember -Function $DirectoryToolsFunctions 

}

