New-Module -Name MemoryTools -Scriptblock {

[array]$global:MemoryToolsCmdlets = @(
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-MemoryToolsCmdlets"
            Alias       = "gmtc"
            Description = "Get MemoryTool cmdlets"
            Category    = "util"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-MemoryTools"
            Alias       = "mt"
            Description = "Get usage on cmdlets in this module "
            Category    = "util"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "New-IInMMemoryMModule"
            Alias       = "nmm"
            Description = "Create new memory module"
            Category    = "util"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-ffunc"
            Alias       = "gfnc"
            Description = "Create function"
            Category    = "util"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-AAddWWin332TType"
            Alias       = "gaw3"
            Description = "Create a .NET type for a Win32 function"
            Category    = "util"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-ppsenum"
            Alias       = "gpse"
            Description = "Create an in-memory enumeration"
            Category    = "util"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-ffield"
            Alias       = "gfld"
            Description = "Define a struct field"
            Category    = "util"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-sstruct"
            Alias       = "gstr"
            Description = "Create an in-memory struct"
            Category    = "util"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "OpenProcess"
            Alias       = "op"
            Description = "Open a process"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "RReadPProcessMMemory"
            Alias       = "rpm"
            Description = "Read process memory"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "WWritePProcessMMemory"
            Alias       = "wpm"
            Description = "Write process memory"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "VirtualAllocEx"
            Alias       = "vqe"
            Description = "Allocate virtual memory"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "VirtualQueryEx"
            Alias       = "vqe"
            Description = "Query virtual memory"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "CCloseHHandle"
            Alias       = "ch"
            Description = "Close handle to a process"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "CCreateRRemoteTThread"
            Alias       = "crt"
            Description = "Start a thread in a remote process"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "KK332GGetMMappedFFileNName"
            Alias       = "gmfn"
            Description = "Get a mapped file name"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Open-Thread"
            Alias       = "ot"
            Description = "Open an existing thread object"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Open-ThreadToken"
            Alias       = "ott"
            Description = "Open access token associated with a thread"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Open-ProcessToken"
            Alias       = "opt"
            Description = "Open access token associated with a process"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-QueryFullProcessImageName"
            Alias       = "qfpin"
            Description = "Get full image name of a process"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-PProcessSStrings"
            Alias       = "gps"
            Description = "Get strings from a running process"
            Category    = "info"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-PProcessMMemoryIInfo"
            Alias       = "gpmi"
            Description = "Get info about running process memory"
            Category    = "info"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-SystemInfo"
            Alias       = "gsi"
            Description = "Get info about system"
            Category    = "info"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-HoundVars"
            Alias       = "ghv"
            Description = "Show Azure Hound variables"
            Category    = "vars"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-HoundVarsRestored"
            Alias       = "ghvr"
            Description = "Get all hound variables restored to current session"
            Category    = "vars"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-HoundVarsSaved"
            Alias       = "ghvs"
            Description = "Get all hound variables currently saved"
            Category    = "vars"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-TokenInformation"
            Alias       = "gti"
            Description = "Get token information"
            Category    = "info"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "NtQueryInformationThread"
            Alias       = "nqit"
            Description = "Get information about a thread"
            Category    = "info"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "ConvertSidToStringSid"
            Alias       = "csts"
            Description = "Convert SID to String SID"
            Category    = "util"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Open-Thread"
            Alias       = "ot"
            Description = "Open an existing thread object"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-WinConstants"
            Alias       = "gwcc"
            Description = "Get Win32 constants"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-WinFunctions"
            Alias       = "gwff"
            Description = "Get Win32 functions"
            Category    = "api"
            Mod         = "MeTools"
        }),
    $(New-Object -TypeName psobject -Property @{
            Name        = "Get-DelegateType"
            Alias       = "gctt"
            Description = "Get delegate type"
            Category    = "api"
            Mod         = "MeTools"
        })
)
function Get-SystemInfo {

    $global:SInfo = [System.AppDomain]::CurrentDomain.GetAssemblies() | Where-Object FullName -match 'SysInfo'
    if (!$SInfo) {
        $global:SInfo = New-IInMMemoryMModule -ModuleName SysInfo
    }

    $ProcessorType = Get-ppsenum -Module $SInfo -FullName SYSINFO.PROCESSOR_ARCH UInt16 -EnumElements @{
        PROCESSOR_ARCHITECTURE_INTEL   = 0
        PROCESSOR_ARCHITECTURE_MIPS    = 1
        PROCESSOR_ARCHITECTURE_ALPHA   = 2
        PROCESSOR_ARCHITECTURE_PPC     = 3
        PROCESSOR_ARCHITECTURE_SHX     = 4
        PROCESSOR_ARCHITECTURE_ARM     = 5
        PROCESSOR_ARCHITECTURE_IA64    = 6
        PROCESSOR_ARCHITECTURE_ALPHA64 = 7
        PROCESSOR_ARCHITECTURE_AMD64   = 9
        PROCESSOR_ARCHITECTURE_UNKNOWN = 0xFFFF
    }

    $SYSTEM_INFO = Get-sstruct $SInfo SYSINFO.SYSTEM_INFO @{
        ProcessorArchitecture     = Get-ffield 0 $ProcessorType
        Reserved                  = Get-ffield 1 Int16
        PageSize                  = Get-ffield 2 Int32
        MinimumApplicationAddress = Get-ffield 3 IntPtr
        MaximumApplicationAddress = Get-ffield 4 IntPtr
        ActiveProcessorMask       = Get-ffield 5 IntPtr
        NumberOfProcessors        = Get-ffield 6 Int32
        ProcessorType             = Get-ffield 7 Int32
        AllocationGranularity     = Get-ffield 8 Int32
        ProcessorLevel            = Get-ffield 9 Int16
        ProcessorRevision         = Get-ffield 10 Int16
    }

    $FunctionDefinitions = @(
        (Get-Ffunc kernel32 GetSystemInfo ([Void]) @($SYSTEM_INFO.MakeByRefType()))
    )

    $Types = $FunctionDefinitions | Get-AAddWWin332TType -Module $SInfo -Namespace 'Win32SysInfo'
    $K32Sys = $Types['kernel32']

    $SysInfo = [Activator]::CreateInstance($SYSTEM_INFO)
    $K32Sys::GetSystemInfo([Ref] $SysInfo)

    $SysInfo
}
function Get-DelegateType {
    Param
    (
        [OutputType([Type])]

        [Parameter( Position = 0)]
        [Type[]]
        $Parameters = (New-Object Type[](0)),

        [Parameter( Position = 1 )]
        [Type]
        $ReturnType = [Void]
    )

    $Domain             = [AppDomain]::CurrentDomain
    $DynAssembly        = New-Object System.Reflection.AssemblyName('ReflectedDelegate')
    $AssemblyBuilder    = [System.Reflection.Emit.AssemblyBuilder]::DefineDynamicAssembly($DynAssembly, [System.Reflection.Emit.AssemblyBuilderAccess]::Run)
    $ModuleBuilder = $AssemblyBuilder.DefineDynamicModule('InMemoryModule', $false)
    $TypeBuilder = $ModuleBuilder.DefineType('MyDelegateType', 'Class, Public, Sealed, AnsiClass, AutoClass', [System.MulticastDelegate])
    $ConstructorBuilder = $TypeBuilder.DefineConstructor('RTSpecialName, HideBySig, Public', [System.Reflection.CallingConventions]::Standard, $Parameters)
    $ConstructorBuilder.SetImplementationFlags('Runtime, Managed')
    $MethodBuilder = $TypeBuilder.DefineMethod('Invoke', 'Public, HideBySig, NewSlot, Virtual', $ReturnType, $Parameters)
    $MethodBuilder.SetImplementationFlags('Runtime, Managed')

    Write-Output $TypeBuilder.CreateType()
}

$funcDefs = @(
    (func kernel32 CloseHandle ([bool]) @([IntPtr]) -SetLastError),
    (func advapi32 ConvertSidToStringSid ([bool]) @([IntPtr],[IntPtr].MakeByRefType()) -SetLastError),
    (func kernel32 CreateToolhelp32Snapshot ([IntPtr]) @([UInt32],[UInt32]) -SetLastError),
    (func advapi32 GetTokenInformation ([bool]) @([IntPtr], [Int32],[IntPtr],[UInt32],[UInt32].MakeByRefType()) -SetLastError),
    # (func ntdll    NtQueryInformationThread ([UInt32]) @([IntPtr]), [Int32], [IntPtr], [Int32], [IntPtr]) ),
    (func kernel32 OpenProcess ([IntPtr]) @([UInt32], [bool],[UInt32]) -SetLastError),
    (func advapi32 OpenProcessToken ([bool]) @([IntPtr],[UInt32],[IntPtr].MakeByRefType()) -SetLastError),
    (func kernel32 OpenThread ([IntPtr]) @([UInt32],[bool],[UInt32]) -SetLastError),
    (func advapi32 OpenThreadToken ([bool]) @([IntPtr], [UInt32], [bool], [IntPtr].MakeByRefType()) -SetLastError),
    (func kernel32 QueryFullProcessImageName ([bool]) @([IntPtr], [UInt32], [System.Text.StringBuilder],[UInt32].MakeByRefType()) -SetLastError),
    (func kernel32 ReadProcessMemory ([Bool]) @([IntPtr],[IntPtr],[Byte[]],[Int],[Int].MakeByRefType()) -SetLastError),
    (func kernel32 TerminateThread ([bool]) @([IntPtr],[UInt32]) -SetLastError)
    # (func kernel32 Thread32First ([bool]) @([IntPtr],$THREADENTRY32.MakeByRefType()) -SetLastError),
    # (func kernel32 Thread32Next ([bool]) @([IntPtr],$THREADENTRY32.MakeByRefType()) -SetLastError),
    # (func kernel32 VirtualQueryEx ([Int32]) @([IntPtr],[IntPtr],$MEMORYBASICINFORMATION.MakeByRefType(),[UInt32] ) -SetLastError)
)
############################

##########  VARS  ##########

############################

function Get-HoundVarsSaved {
    param(
        [string]$path
    )
    $names = ( Get-HoundVars ).Name
    $names | Foreach-Object {
        (Get-Variable $_).Value | Export-CliXml -Path $path\$_.xml 
    }
}
function Get-HoundVars {

    param(
        [switch]$l
    )
    $arr = [System.Collections.ArrayList]::new()

    Get-Variable -Include 'AAD*' -Exclude "aadgraph_*" | Foreach-Object {
        if ($_.Name -match 'Admin|List') {
            $objType = "list"
        }    
        elseif ($_.Name -match 'AADUser') {
            $objType = "user"
        }    
        elseif ($_.Name -match 'ServicePrincipal|SP') {
            $objType = "servicePrincipal"
        }
        elseif ($_.Name -match 'Role') {
            $objType = "role"
        }
        elseif ($_.Name -match 'Group') {
            $objType = "group"
        }
        elseif ($_.Name -match 'Device') {
            $objType = "device"
        }   
        else {
            $objType = "other"
        }
    
        $arr += New-Object -TypeName psobject -Property @{
            Name    = $_.Name 
            Count   = $_.value.count
            objType = $objType
        }
    }
    if (!$l) {
        $arr | Where-Object objType -notmatch "other|list" | Sort-Object -Desc objType, count  | Format-Table -GroupBy objType -HideTableHeaders -Property Name, Count 
    }
    else {
        $arr | Where-Object objType -match "other|list" | Sort-Object -Desc objType, count  | Format-Table -GroupBy objType -HideTableHeaders -Property Name, Count
    }
}
function Get-HoundVarsRestored {
    $files =Get-ChildItem $env:USERPROFILE\savedObjects\*.xml 
    $files | Foreach-Object {
        New-Variable -Scope Global -Name $_.BaseName -Value $(Import-CliXml -Path $_.fullname) -Force 
    }
}

############################

########## UTIL ############

############################

function Get-MemoryTools {
    param(
        [string]$type = 'default'
    )

    Switch ($type) {
        'a' { Get-MemoryToolsCmdlets -type 'api' }
        'i' { Get-MemoryToolsCmdlets -type 'info' }
        'v' { Get-MemoryToolsCmdlets -type 'vars' }
        'u' { Get-MemoryToolsCmdlets -type 'util' }
        default {
        
            Write-Host "`n"
            Write-Host -Fore DarkRed " ...:::" -NoNewLine; Write-Host "`tUsage:   " -NoNewLine;
            Write-Host -Fore DarkRed "mt " -NoNewLine; Write-Host -Fore DarkGray "[category]"
            Write-Host "`n"

            [pscustomobject]$options = [ordered]@{
                'a          api '   = "Win32 APIs"       
                'i          info'   = "Gather information about processes"
                'v          vars'   = "Environmental variables"
                'u          util'   = "Utilities/Helper functions" 
            }

            $options.Keys | Foreach-Object {
                $firstLetter = $_[0]
                Write-Host -Fore DarkRed "`tmt " -NoNewLine;Write-Host "$firstLetter" -Fore LightGreen -NoNewLine
                Write-Host -Fore DarkGray "$($_.trim($firstLetter))" -NoNewLine ; Write-Host "`t$($options.Item("$_"))"
            }
            Write-Host "`n"
        }
    }
}
function Get-MemoryToolsCmdlets {
    param(
        [string]$type
    )

    if (!$type) {
        $cmdlets = $MemoryToolsCmdlets | Sort-Object Name
    }
    else {
        $cmdlets = $MemoryToolsCmdlets | Where-Object Category -eq $type | Sort-Object Name
    }
    Write-Host "`n"
    Write-Host -Fore DarkGray ("`tCmdlet`t`t`t`t") -NoNewLine; Write-Host -Fore DarkGray " Alias`t`t" -Nonewline;
    Write-Host -Fore DarkGray " Description" 
    Write-Host "`t$('-' * 24)`t $('-' * 5)`t`t $('-' * 30)"
    $cmdlets | ForEach-Object {
        if ($_.Name.Length -gt 23) {
            Write-Host -Fore Gray ("`t" + $_.Name + "`t ") -NoNewLine; Write-Host -Fore DarkRed $_.Alias -Nonewline
            Write-Host -Fore Gray (" `t`t " + $_.Description ) 
        }    
        elseif ($_.Name.Length -gt 15) {
            Write-Host -Fore Gray ("`t" + $_.Name + "`t`t ") -NoNewLine; Write-Host -Fore DarkRed $_.Alias -Nonewline
            Write-Host -Fore Gray (" `t`t " + $_.Description ) 
        }
        elseif ($_.Name.Length -gt 7) {
            Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t ") -NoNewLine; Write-Host -Fore DarkRed $_.Alias -Nonewline
            Write-Host -Fore Gray (" `t`t " + $_.Description ) 
        }
        else {
            Write-Host -Fore Gray ("`t" + $_.Name + "`t`t`t`t ") -NoNewLine; Write-Host -Fore DarkRed $_.Alias -Nonewline
            Write-Host -Fore Gray (" `t`t " + $_.Description ) 
        }
    }
    Write-Host "`n"
}
function New-IInMMemoryMModule {
    Param
    (
        [Parameter(Position = 0)]
        [ValidateNotNullOrEmpty()]
        [String]
        $ModuleName = [Guid]::NewGuid().ToString()
    )

    $AppDomain = [AppDomain]::CurrentDomain
    $LoadedAssemblies = $AppDomain.GetAssemblies()

    foreach ($Assembly in $LoadedAssemblies) {
        if ($Assembly.FullName -and ($Assembly.FullName.Split(',')[0] -eq $ModuleName)) {
            return $Assembly
        }
    }

    $DynAssembly = New-Object Reflection.AssemblyName($ModuleName)
    $Domain = $AppDomain
    if ($IsCoreCLR) {
        $AssemblyBuilder = [Reflection.Emit.AssemblyBuilder]::DefineDynamicAssembly($DynAssembly, 'Run')
    }
    else {
        $AssemblyBuilder = $Domain.DefineDynamicAssembly($DynAssembly, 'Run')
    }

    $ModuleBuilder = $null # $AssemblyBuilder.DefineDynamicModule($ModuleName, $False)

    return $ModuleBuilder
}
function Get-Ffunc {
    Param
    (
        [Parameter(Position = 0, Mandatory = $True)]
        [String]
        $DllName,

        [Parameter(Position = 1, Mandatory = $True)]
        [string]
        $FunctionName,

        [Parameter(Position = 2, Mandatory = $True)]
        [Type]
        $ReturnType,

        [Parameter(Position = 3)]
        [Type[]]
        $ParameterTypes,

        [Parameter(Position = 4)]
        [Runtime.InteropServices.CallingConvention]
        $NativeCallingConvention,

        [Parameter(Position = 5)]
        [Runtime.InteropServices.CharSet]
        $Charset,

        [String]
        $EntryPoint,

        [Switch]
        $SetLastError
    )

    $Properties = @{
        DllName      = $DllName
        FunctionName = $FunctionName
        ReturnType   = $ReturnType
    }

    if ($ParameterTypes) { $Properties['ParameterTypes'] = $ParameterTypes }
    if ($NativeCallingConvention) { $Properties['NativeCallingConvention'] = $NativeCallingConvention }
    if ($Charset) { $Properties['Charset'] = $Charset }
    if ($SetLastError) { $Properties['SetLastError'] = $SetLastError }
    if ($EntryPoint) { $Properties['EntryPoint'] = $EntryPoint }

    New-Object PSObject -Property $Properties
}
function Get-AAddWWin332TType {
    [OutputType([Hashtable])]
    Param(
        [Parameter(Mandatory = $True, ValueFromPipelineByPropertyName = $True)]
        [String]
        [ValidateNotNullOrEmpty()]
        $DllName,

        [Parameter(Mandatory = $True, ValueFromPipelineByPropertyName = $True)]
        [String]
        [ValidateNotNullOrEmpty()]
        $FunctionName,

        [Parameter(ValueFromPipelineByPropertyName = $True)]
        [String]
        [ValidateNotNullOrEmpty()]
        $EntryPoint,

        [Parameter(Mandatory = $True, ValueFromPipelineByPropertyName = $True)]
        [Type]
        $ReturnType,

        [Parameter(ValueFromPipelineByPropertyName = $True)]
        [Type[]]
        $ParameterTypes,

        [Parameter(ValueFromPipelineByPropertyName = $True)]
        [Runtime.InteropServices.CallingConvention]
        $NativeCallingConvention = [Runtime.InteropServices.CallingConvention]::StdCall,

        [Parameter(ValueFromPipelineByPropertyName = $True)]
        [Runtime.InteropServices.CharSet]
        $Charset = [Runtime.InteropServices.CharSet]::Auto,

        [Parameter(ValueFromPipelineByPropertyName = $True)]
        [Switch]
        $SetLastError,

        [Parameter(Mandatory = $True)]
        [ValidateScript( { ($_ -is [Reflection.Emit.ModuleBuilder]) -or ($_ -is [Reflection.Assembly]) })]
        $Module,

        [ValidateNotNull()]
        [String]
        $Namespace = ''
    )

    BEGIN {
        $TypeHash = @{}
    }

    PROCESS {
        if ($Module -is [Reflection.Assembly]) {
            if ($Namespace) {
                $TypeHash[$DllName] = $Module.GetType("$Namespace.$DllName")
            }
            else {
                $TypeHash[$DllName] = $Module.GetType($DllName)
            }
        }
        else {
            
            if (!$TypeHash.ContainsKey($DllName)) {
                if ($Namespace) {
                    $TypeHash[$DllName] = $Module.DefineType("$Namespace.$DllName", 'Public,BeforeFieldInit')
                }
                else {
                    $TypeHash[$DllName] = $Module.DefineType($DllName, 'Public,BeforeFieldInit')
                }
            }

            $Method = $TypeHash[$DllName].DefineMethod(
                $FunctionName,
                'Public,Static,PinvokeImpl',
                $ReturnType,
                $ParameterTypes)

            
            $i = 1
            foreach ($Parameter in $ParameterTypes) {
                if ($Parameter.IsByRef) {
                    [void] $Method.DefineParameter($i, 'Out', $null)
                }

                $i++
            }

            $DllImport = [Runtime.InteropServices.DllImportAttribute]
            $SetLastErrorField = $DllImport.GetField('SetLastError')
            $CallingConventionField = $DllImport.GetField('CallingConvention')
            $CharsetField = $DllImport.GetField('CharSet')
            $EntryPointField = $DllImport.GetField('EntryPoint')
            if ($SetLastError) { $SLEValue = $True } else { $SLEValue = $False }

            if ($EntryPoint) { $ExportedFuncName = $EntryPoint } else { $ExportedFuncName = $FunctionName }

            
            $Constructor = [Runtime.InteropServices.DllImportAttribute].GetConstructor([String])
            $DllImportAttribute = New-Object Reflection.Emit.CustomAttributeBuilder($Constructor,
                $DllName,
                [Reflection.PropertyInfo[]] @(),
                [Object[]] @(),
                [Reflection.FieldInfo[]] @($SetLastErrorField,
                    $CallingConventionField,
                    $CharsetField,
                    $EntryPointField),
                [Object[]] @($SLEValue,
                    ([Runtime.InteropServices.CallingConvention] $NativeCallingConvention),
                    ([Runtime.InteropServices.CharSet] $Charset),
                    $ExportedFuncName))

            $Method.SetCustomAttribute($DllImportAttribute)
        }
    }

    END {
        if ($Module -is [Reflection.Assembly]) {
            return $TypeHash
        }

        $ReturnTypes = @{}

        foreach ($Key in $TypeHash.Keys) {
            $Type = $TypeHash[$Key].CreateType()

            $ReturnTypes[$Key] = $Type
        }
        return $ReturnTypes
    }
}
function Get-ppsenum {

    [OutputType([Type])]
    Param
    (
        [Parameter(Position = 0, Mandatory = $True)]
        [ValidateScript( { ($_ -is [Reflection.Emit.ModuleBuilder]) -or ($_ -is [Reflection.Assembly]) })]
        $Module,

        [Parameter(Position = 1, Mandatory = $True)]
        [ValidateNotNullOrEmpty()]
        [String]
        $FullName,

        [Parameter(Position = 2, Mandatory = $True)]
        [Type]
        $Type,

        [Parameter(Position = 3, Mandatory = $True)]
        [ValidateNotNullOrEmpty()]
        [Hashtable]
        $EnumElements,

        [Switch]
        $Bitfield
    )

    if ($Module -is [Reflection.Assembly]) {
        return ($Module.GetType($FullName))
    }

    $EnumType = $Type -as [Type]

    $EnumBuilder = $Module.DefineEnum($FullName, 'Public', $EnumType)

    if ($Bitfield) {
        $FlagsConstructor = [FlagsAttribute].GetConstructor(@())
        $FlagsCustomAttribute = New-Object Reflection.Emit.CustomAttributeBuilder($FlagsConstructor, @())
        $EnumBuilder.SetCustomAttribute($FlagsCustomAttribute)
    }

    foreach ($Key in $EnumElements.Keys) {
        
        $null = $EnumBuilder.DefineLiteral($Key, $EnumElements[$Key] -as $EnumType)
    }

    $EnumBuilder.CreateType()
}
function Get-ffield {
    Param
    (
        [Parameter(Position = 0, Mandatory = $True)]
        [UInt16]
        $Position,

        [Parameter(Position = 1, Mandatory = $True)]
        [Type]
        $Type,

        [Parameter(Position = 2)]
        [UInt16]
        $Offset,

        [Object[]]
        $MarshalAs
    )

    @{
        Position  = $Position
        Type      = $Type -as [Type]
        Offset    = $Offset
        MarshalAs = $MarshalAs
    }
}
function Get-sstruct {

    [OutputType([Type])]
    Param
    (
        [Parameter(Position = 1, Mandatory = $True)]
        [ValidateScript( { ($_ -is [Reflection.Emit.ModuleBuilder]) -or ($_ -is [Reflection.Assembly]) })]
        $Module,

        [Parameter(Position = 2, Mandatory = $True)]
        [ValidateNotNullOrEmpty()]
        [String]
        $FullName,

        [Parameter(Position = 3, Mandatory = $True)]
        [ValidateNotNullOrEmpty()]
        [Hashtable]
        $StructFields,

        [Reflection.Emit.PackingSize]
        $PackingSize = [Reflection.Emit.PackingSize]::Unspecified,

        [Switch]
        $ExplicitLayout,

        [System.Runtime.InteropServices.CharSet]
        $CharSet = [System.Runtime.InteropServices.CharSet]::Ansi
    )

    if ($Module -is [Reflection.Assembly]) {
        return ($Module.GetType($FullName))
    }

    [Reflection.TypeAttributes] $StructAttributes = 'Class,
        Public,
        Sealed,
        BeforeFieldInit'

    if ($ExplicitLayout) {
        $StructAttributes = $StructAttributes -bor [Reflection.TypeAttributes]::ExplicitLayout
    }
    else {
        $StructAttributes = $StructAttributes -bor [Reflection.TypeAttributes]::SequentialLayout
    }

    switch ($CharSet) {
        Ansi {
            $StructAttributes = $StructAttributes -bor [Reflection.TypeAttributes]::AnsiClass
        }
        Auto {
            $StructAttributes = $StructAttributes -bor [Reflection.TypeAttributes]::AutoClass
        }
        Unicode {
            $StructAttributes = $StructAttributes -bor [Reflection.TypeAttributes]::UnicodeClass
            s
        }
    }

    $StructBuilder = $Module.DefineType($FullName, $StructAttributes, [ValueType], $PackingSize)
    $ConstructorInfo = [Runtime.InteropServices.MarshalAsAttribute].GetConstructors()[0]
    $SizeConst = @([Runtime.InteropServices.MarshalAsAttribute].GetField('SizeConst'))

    $Fields = New-Object Hashtable[]($StructFields.Count)

    
    
    
    foreach ($Field in $StructFields.Keys) {
        $Index = $StructFields[$Field]['Position']
        $Fields[$Index] = @{FieldName = $Field; Properties = $StructFields[$Field] }
    }

    foreach ($Field in $Fields) {
        $FieldName = $Field['FieldName']
        $FieldProp = $Field['Properties']

        $Offset = $FieldProp['Offset']
        $Type = $FieldProp['Type']
        $MarshalAs = $FieldProp['MarshalAs']

        $NewField = $StructBuilder.DefineField($FieldName, $Type, 'Public')

        if ($MarshalAs) {
            $UnmanagedType = $MarshalAs[0] -as ([Runtime.InteropServices.UnmanagedType])
            if ($MarshalAs[1]) {
                $Size = $MarshalAs[1]
                $AttribBuilder = New-Object Reflection.Emit.CustomAttributeBuilder($ConstructorInfo,
                    $UnmanagedType, $SizeConst, @($Size))
            }
            else {
                $AttribBuilder = New-Object Reflection.Emit.CustomAttributeBuilder($ConstructorInfo, [Object[]] @($UnmanagedType))
            }

            $NewField.SetCustomAttribute($AttribBuilder)
        }

        if ($ExplicitLayout) { $NewField.SetOffset($Offset) }
    }

    
    
    $SizeMethod = $StructBuilder.DefineMethod('GetSize',
        'Public, Static',
        [Int],
        [Type[]] @())
    $ILGenerator = $SizeMethod.GetILGenerator()
    
    $ILGenerator.Emit([Reflection.Emit.OpCodes]::Ldtoken, $StructBuilder)
    $ILGenerator.Emit([Reflection.Emit.OpCodes]::Call,
        [Type].GetMethod('GetTypeFromHandle'))
    $ILGenerator.Emit([Reflection.Emit.OpCodes]::Call,
        [Runtime.InteropServices.Marshal].GetMethod('SizeOf', [Type[]] @([Type])))
    $ILGenerator.Emit([Reflection.Emit.OpCodes]::Ret)

    
    
    $ImplicitConverter = $StructBuilder.DefineMethod('op_Implicit',
        'PrivateScope, Public, Static, HideBySig, SpecialName',
        $StructBuilder,
        [Type[]] @([IntPtr]))
    $ILGenerator2 = $ImplicitConverter.GetILGenerator()
    $ILGenerator2.Emit([Reflection.Emit.OpCodes]::Nop)
    $ILGenerator2.Emit([Reflection.Emit.OpCodes]::Ldarg_0)
    $ILGenerator2.Emit([Reflection.Emit.OpCodes]::Ldtoken, $StructBuilder)
    $ILGenerator2.Emit([Reflection.Emit.OpCodes]::Call,
        [Type].GetMethod('GetTypeFromHandle'))
    $ILGenerator2.Emit([Reflection.Emit.OpCodes]::Call,
        [Runtime.InteropServices.Marshal].GetMethod('PtrToStructure', [Type[]] @([IntPtr], [Type])))
    $ILGenerator2.Emit([Reflection.Emit.OpCodes]::Unbox_Any, $StructBuilder)
    $ILGenerator2.Emit([Reflection.Emit.OpCodes]::Ret)

    $StructBuilder.CreateType()
}

$global:METools = [System.AppDomain]::CurrentDomain.GetAssemblies() | Where-Object FullName -match 'METools'
if (!$METools) {
    $global:METools = New-IInMMemoryMModule -ModuleName METools
}

$Types = $funcDefs | Get-AAddWWin332TType -Module $METools -Namespace 'Win32'
$K32 = $null # $Types['kernel32']


###########################

########## API ############

###########################


function CCreateRRemoteTThread {
    param
    (
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $PProcessHHandle,

        [Parameter(Mandatory = $true)]
        [IntPtr]
        $StartAddress,
        
        [Parameter()]
        [UInt32]
        $StackSize = 0,
        
        [Parameter()]
        [ValidateSet('None', 'CREATE_SUSPENDED', 'STACK_SIZE_PARAM_IS_A_RESERVATION')]
        [string[]]
        $CreationFlags = 'None'
    )

    $ThreadId = 0
    
    $SUCCESS = $K32::CCreateRRemoteTThread($PProcessHHandle, 0, $StackSize, $StartAddress, [IntPtr]::Zero, 4, [ref]$ThreadId); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if (-not $SUCCESS) {
        Write-Error "[CCreateRRemoteTThread] Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }

    $props = @{
        Handle = $SUCCESS
        Id     = $ThreadId
    }

    New-Object -TypeName psobject -Property $props
}
function KK332GGetMMappedFFileNName {
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $PProcessHHandle,

        [Parameter(Mandatory = $true)]
        [IntPtr]
        $BaseAddress
    )

    $buf = New-Object byte[](1024)

    $SUCCESS = $K32::KK332GGetMMappedFFileNName($PProcessHHandle, $BaseAddress, $buf, $buf.Length); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if (-not $SUCCESS) {
        throw "[KK332GGetMMappedFFileNName] Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }

    Write-Output ([System.Text.Encoding]::Unicode.GetString($buf))
} 
function OpenProcess {
    param
    (
        [Parameter(Mandatory = $true)]
        [UInt32]
        $ProcessId,
        
        [Parameter(Mandatory = $true)]
        [UInt32]
        $DesiredAccess,
        
        [Parameter()]
        [bool]
        $InheritHandle = $false
    )
    
    $hProcess = $K32::OpenProcess($DesiredAccess, $InheritHandle, $ProcessId); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if ($hProcess -eq 0) {
        Write-Debug "OpenProcess Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }
    
    Write-Output $hProcess
}
function RReadPProcessMMemory {
    param
    (
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $PProcessHHandle,
        
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $BaseAddress,
        
        [Parameter(Mandatory = $true)]
        [Int]
        $Size    
    )
    
    $buf = New-Object byte[]($Size)
    [Int32]$NumberOfBytesRead = 0
    
    $Success = $K32::RReadPProcessMMemory($PProcessHHandle, $BaseAddress, $buf, $buf.Length, [ref]$NumberOfBytesRead); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if (-not $Success) {
        Write-Debug "RReadPProcessMMemory Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }
    
    Write-Output $buf
}    
function VirtualAllocEx {
    param
    (
        [Parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
        [Alias('Handle')]
        [IntPtr]
        $PProcessHHandle,

        [Parameter(Mandatory = $true)]
        [UInt32]
        $Size,

        [Parameter(Mandatory = $true)]
        [UInt32]
        $AllocationType = 0x3000,

        [Parameter(Mandatory = $true)]
        [UInt32]
        $Protect = 0x04
    )

    $RemoteMemAddr = $K32::VirtualAllocEx($PProcessHHandle, [IntPtr]::Zero, $Size, $AllocationType, $Protect)

    if (-not($RemoteMemAddr)) {
        Throw "Unable to allocated memory in desired process!"
    }
    else {
        Write-Output $RemoteMemAddr
    }
}
function VirtualQueryEx {
    param
    (
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $PProcessHHandle,
        
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $BaseAddress
    )
    
    $memory_basic_info = [Activator]::CreateInstance($MEMORYBASICINFORMATION)
    $Success = $K32::VirtualQueryEx($PProcessHHandle, $BaseAddress, [Ref]$memory_basic_info, $MEMORYBASICINFORMATION::GetSize()); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if (-not $Success) {
        Write-Debug "VirtualQueryEx Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }
    
    Write-Output $memory_basic_info
}
function WWritePProcessMMemory {
    param
    (
        [Parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
        [Alias('Handle')]
        [IntPtr]
        $PProcessHHandle,

        [Parameter(Mandatory = $true)]
        [IntPtr]
        $BaseAddress,

        [Parameter(Mandatory = $true)]
        [byte[]]
        $Buffer
    )

    [Int32]$lpNumberOfBytesWritten = 0

    $SUCCESS = $K32::WWritePProcessMMemory($PProcessHHandle, $BaseAddress, $Buffer, $Buffer.Length, [ref]$lpNumberOfBytesWritten); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if ($SUCCESS -eq 0) {
        throw "WWritePProcessMMemory Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }
}
function CCloseHHandle {
    param
    (
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $Handle    
    )
    
    $Success = $K32::CCloseHHandle($Handle); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if (-not $Success) {
        Write-Debug "Close Handle Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }
}
function OpenProcessToken { 

    [OutputType([IntPtr])]
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $ProcessHandle,
        
        [Parameter(Mandatory = $true)]
        [ValidateSet('TOKEN_ASSIGN_PRIMARY', 'TOKEN_DUPLICATE', 'TOKEN_IMPERSONATE', 'TOKEN_QUERY', 'TOKEN_QUERY_SOURCE', 'TOKEN_ADJUST_PRIVILEGES', 'TOKEN_ADJUST_GROUPS', 'TOKEN_ADJUST_DEFAULT', 'TOKEN_ADJUST_SESSIONID', 'DELETE', 'READ_CONTROL', 'WRITE_DAC', 'WRITE_OWNER', 'SYNCHRONIZE', 'STANDARD_RIGHTS_REQUIRED', 'TOKEN_ALL_ACCESS')]
        [string[]]
        $DesiredAccess  
    )
    
    # Calculate Desired Access Value
    $dwDesiredAccess = 0

    foreach ($val in $DesiredAccess) {
        $dwDesiredAccess = $dwDesiredAccess -bor $TOKEN_ACCESS::$val
    }

    $hToken = [IntPtr]::Zero
    $Success = $Advapi32::OpenProcessToken($ProcessHandle, $dwDesiredAccess, [ref]$hToken); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if (-not $Success) {
        throw "OpenProcessToken Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }
    
    Write-Output $hToken
}
function OpenThread {

    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)]
        [UInt32]
        $ThreadId,
        
        [Parameter(Mandatory = $true)]
        [ValidateSet('THREAD_TERMINATE', 'THREAD_SUSPEND_RESUME', 'THREAD_GET_CONTEXT', 'THREAD_SET_CONTEXT', 'THREAD_SET_INFORMATION', 'THREAD_QUERY_INFORMATION', 'THREAD_SET_THREAD_TOKEN', 'THREAD_IMPERSONATE', 'THREAD_DIRECT_IMPERSONATION', 'THREAD_SET_LIMITED_INFORMATION', 'THREAD_QUERY_LIMITED_INFORMATION', 'DELETE', 'READ_CONTROL', 'WRITE_DAC', 'WRITE_OWNER', 'SYNCHRONIZE', 'THREAD_ALL_ACCESS')]
        [string[]]
        $DesiredAccess,
        
        [Parameter()]
        [bool]
        $InheritHandle = $false
    )
    
    # Calculate Desired Access Value
    $dwDesiredAccess = 0
    
    foreach ($val in $DesiredAccess) {
        $dwDesiredAccess = $dwDesiredAccess -bor $THREAD_ACCESS::$val
    }

    $hThread = $Kernel32::OpenThread($dwDesiredAccess, $InheritHandle, $ThreadId); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if ($hThread -eq 0) {
        #throw "OpenThread Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }
    
    Write-Output $hThread
}
function OpenThreadToken {

    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $ThreadHandle,
        
        [Parameter(Mandatory = $true)]
        [ValidateSet('TOKEN_ASSIGN_PRIMARY', 'TOKEN_DUPLICATE', 'TOKEN_IMPERSONATE', 'TOKEN_QUERY', 'TOKEN_QUERY_SOURCE', 'TOKEN_ADJUST_PRIVILEGES', 'TOKEN_ADJUST_GROUPS', 'TOKEN_ADJUST_DEFAULT', 'TOKEN_ADJUST_SESSIONID', 'DELETE', 'READ_CONTROL', 'WRITE_DAC', 'WRITE_OWNER', 'SYNCHRONIZE', 'STANDARD_RIGHTS_REQUIRED', 'TOKEN_ALL_ACCESS')]
        [string[]]
        $DesiredAccess,
        
        [Parameter()]
        [bool]
        $OpenAsSelf = $false   
    )
    
    # Calculate Desired Access Value
    $dwDesiredAccess = 0

    foreach ($val in $DesiredAccess) {
        $dwDesiredAccess = $dwDesiredAccess -bor $TOKEN_ACCESS::$val
    }

    $hToken = [IntPtr]::Zero
    $Success = $Advapi32::OpenThreadToken($ThreadHandle, $dwDesiredAccess, $OpenAsSelf, [ref]$hToken); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if (-not $Success) {
        throw "OpenThreadToken Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }
    
    Write-Output $hToken
}
function QueryFullProcessImageName {

    param
    (
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $ProcessHandle,
        
        [Parameter()]
        [UInt32]
        $Flags = 0
    )
    
    $capacity = 2048
    $sb = New-Object -TypeName System.Text.StringBuilder($capacity)

    $Success = $Kernel32::QueryFullProcessImageName($ProcessHandle, $Flags, $sb, [ref]$capacity); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if (-not $Success) {
        Write-Debug "QueryFullProcessImageName Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }
    
    Write-Output $sb.ToString()
}
function Get-RRawBBytes {
    
    param (
        [Parameter(Mandatory = $true)]
        [UInt32]
        $ProcessID,

        [Parameter(Mandatory = $true)]
        [UInt64]
        $BaseAddress
    )

    $global:RBytes = [System.AppDomain]::CurrentDomain.GetAssemblies() | Where-Object FullName -match 'RawBytes'
    if (!$RBytes) {
        $global:RBytes = New-IInMMemoryMModule -ModuleName RawBytes
    }
    
    $FunctionDefinitions = @(
        (Get-Ffunc kernel32 OpenProcess ([IntPtr]) @([UInt32], [Bool], [UInt32]) -SetLastError),
        (Get-Ffunc kernel32 ReadProcessMemory ([Bool]) @([IntPtr], [IntPtr], [Byte[]], [Int], [Int].MakeByRefType()) -SetLastError),
        (Get-Ffunc kernel32 CloseHandle ([Bool]) @([IntPtr]) -SetLastError)
    )

    $Types = $FunctionDefinitions | Get-AAddWWin332TType -Module $RBytes -Namespace 'Win32Raw'
    $K32Raw = $Types['kernel32']

    $hProcess = $K32Raw::OpenProcess(0x10, $False, $ProcessID) # PROCESS_VM_READ (0x00000010)

    $Allocation = Get-PProcessMMemoryIInfo -ProcessID $ProcessID | Where-Object BaseAddress -eq $BaseAddress 
    $ReadAllocation = $True

    if (($Allocation.Type -eq 'MEM_IMAGE') -and (-not $IncludeImages)) { $ReadAllocation = $False }
    
    if ($Allocation.Protect.ToString().Contains('PAGE_GUARD')) { $ReadAllocation = $False }

    if ($ReadAllocation) {
        $Bytes = New-Object Byte[]($Allocation.RegionSize)
        $BytesRead = 0
        $Result = $K32Raw::ReadProcessMemory($hProcess, $Allocation.BaseAddress, $Bytes, $Allocation.RegionSize, [Ref] $BytesRead)

        if ((-not $Result) -or ($BytesRead -ne $Allocation.RegionSize)) {
            Write-Warning "Unable to read 0x$($Allocation.BaseAddress.ToString('X16')) from PID $ProcessID. Size: 0x$($Allocation.RegionSize.ToString('X8'))"
        } 
        else {
            $Bytes
        }
        $Bytes = $null
    }
    
    $null = $K32Raw::CloseHandle($hProcess)
}
function Get-VVirtualMMemoryIInfo {

    Param (
        [Parameter(Position = 0, Mandatory = $True)]
        [ValidateScript( { Get-Process -Id $_ })]
        [Int]
        $ProcessID,

        [Parameter(Position = 1, Mandatory = $True)]
        [IntPtr]
        $ModuleBaseAddress,

        [Int]
        $PageSize = 0x1000
    )

    $global:MUtils = [System.AppDomain]::CurrentDomain.GetAssemblies() | Where-Object FullName -match 'MUtils'
    if (!$MUtils) {
        $global:MUtils = New-IInMMemoryMModule -ModuleName MUtils
    }

    $MemProtection = Get-ppsenum $MUtils MEMUTIL.MEM_PROTECT Int32 @{
        PAGE_EXECUTE           = 0x00000010
        PAGE_EXECUTE_READ      = 0x00000020
        PAGE_EXECUTE_READWRITE = 0x00000040
        PAGE_EXECUTE_WRITECOPY = 0x00000080
        PAGE_NOACCESS          = 0x00000001
        PAGE_READONLY          = 0x00000002
        PAGE_READWRITE         = 0x00000004
        PAGE_WRITECOPY         = 0x00000008
        PAGE_GUARD             = 0x00000100
        PAGE_NOCACHE           = 0x00000200
        PAGE_WRITECOMBINE      = 0x00000400
    } -Bitfield

    $MemState = Get-ppsenum $MUtils MEMUTIL.MEM_STATE Int32 @{
        MEM_COMMIT  = 0x00001000
        MEM_FREE    = 0x00010000
        MEM_RESERVE = 0x00002000
    } -Bitfield

    $MemType = Get-ppsenum $MUtils MEMUTIL.MEM_TYPE Int32 @{
        MEM_IMAGE   = 0x01000000
        MEM_MAPPED  = 0x00040000
        MEM_PRIVATE = 0x00020000
    } -Bitfield

    if ([IntPtr]::Size -eq 4) {
        $MEMORY_BASIC_INFORMATION = Get-sstruct $MUtils MEMUTIL.MEMORY_BASIC_INFORMATION @{
            BaseAddress       = Get-ffield 0 Int32
            AllocationBase    = Get-ffield 1 Int32
            AllocationProtect = Get-ffield 2 $MemProtection
            RegionSize        = Get-ffield 3 Int32
            State             = Get-ffield 4 $MemState
            Protect           = Get-ffield 5 $MemProtection
            Type              = Get-ffield 6 $MemType
        }
    }
    else {
        $MEMORY_BASIC_INFORMATION = Get-sstruct $MUtils MEMUTIL.MEMORY_BASIC_INFORMATION @{
            BaseAddress       = Get-ffield 0 Int64
            AllocationBase    = Get-ffield 1 Int64
            AllocationProtect = Get-ffield 2 $MemProtection
            Alignment1        = Get-ffield 3 Int32
            RegionSize        = Get-ffield 4 Int64
            State             = Get-ffield 5 $MemState
            Protect           = Get-ffield 6 $MemProtection
            Type              = Get-ffield 7 $MemType
            Alignment2        = Get-ffield 8 Int32
        }
    }

    $FunctionDefinitions = @(
        (Get-Ffunc kernel32 VirtualQueryEx ([Int32]) @([IntPtr], [IntPtr], $MEMORY_BASIC_INFORMATION.MakeByRefType(), [Int]) -SetLastError),
        (Get-Ffunc kernel32 OpenProcess ([IntPtr]) @([UInt32], [Bool], [UInt32]) -SetLastError),
        (Get-Ffunc kernel32 CloseHandle ([Bool]) @([IntPtr]) -SetLastError)
    )

    $Types = $FunctionDefinitions | Get-AAddWWin332TType -Module $MUtils -Namespace 'Win32MemUtils'
    $K32vm = $Types['kernel32']

    # Get handle to the process
    $hProcess = $K32vm::OpenProcess(0x400, $False, $ProcessID) # PROCESS_QUERY_INFORMATION (0x00000400)

    if (-not $hProcess) {
        throw "Unable to get a process handle for process ID: $ProcessID"
    }

    $MemoryInfo = New-Object $MEMORY_BASIC_INFORMATION
    $BytesRead = $K32vm::VirtualQueryEx($hProcess, $ModuleBaseAddress, [Ref] $MemoryInfo, $PageSize)

    $null = $K32vm::CloseHandle($hProcess)

    $Fields = @{
        BaseAddress       = $MemoryInfo.BaseAddress
        AllocationBase    = $MemoryInfo.AllocationBase
        AllocationProtect = $MemoryInfo.AllocationProtect
        RegionSize        = $MemoryInfo.RegionSize
        State             = $MemoryInfo.State
        Protect           = $MemoryInfo.Protect
        Type              = $MemoryInfo.Type
    }

    $Result = New-Object PSObject -Property $Fields
    $Result.PSObject.TypeNames.Insert(0, 'MEM.INFO')

    $Result
}
function Get-SStructFFromMMemory {
    [CmdletBinding()] Param (
        [Parameter(Position = 0, Mandatory = $True)]
        [Alias('ProcessId')]
        [Alias('PID')]
        [UInt16]
        $Id,

        [Parameter(Position = 1, Mandatory = $True)]
        [IntPtr]
        $MemoryAddress,

        [Parameter(Position = 2, Mandatory = $True)]
        [Alias('Type')]
        [Type]
        $StructType
    )

    Set-StrictMode -Version 2

    $PROCESS_VM_READ = 0x0010 # The process permissions we'l ask for when getting a handle to the process

    # Get a reference to the private GetPProcessHHandle method is System.Diagnostics.Process
    $GetPProcessHHandle = [Diagnostics.Process].GetMethod('GetPProcessHHandle', [Reflection.BindingFlags] 'NonPublic, Instance', $null, @([Int]), $null)

    try {
        # Make sure user didn't pass in a non-existent PID
        $Process = Get-Process -Id $Id -ErrorVariable GetProcessError
        # Get the default process handle
        $Handle = $Process.Handle
    }
    catch [Exception] {
        throw $GetProcessError
    }

    if ($null -eq $Handle) {
        throw "Unable to obtain a handle for PID $Id. You will likely need to run this script elevated."
    }


    $mscorlib = [AppDomain]::CurrentDomain.GetAssemblies() | Where-Object { $_.FullName.Split(',')[0].ToLower() -eq 'mscorlib' }
    $Win32Native = $mscorlib.GetTypes() | Where-Object { $_.FullName -eq 'Microsoft.Win32.Win32Native' }
    $MEMORY_BASIC_INFORMATION = $Win32Native.GetNestedType('MEMORY_BASIC_INFORMATION', [Reflection.BindingFlags] 'NonPublic')

    if ($null -eq $MEMORY_BASIC_INFORMATION) {
        throw 'Unable to get a reference to the MEMORY_BASIC_INFORMATION structure.'
    }

    $ProtectField = $MEMORY_BASIC_INFORMATION.GetField('Protect', [Reflection.BindingFlags] 'NonPublic, Instance')
    $AllocationBaseField = $MEMORY_BASIC_INFORMATION.GetField('BaseAddress', [Reflection.BindingFlags] 'NonPublic, Instance')
    $RegionSizeField = $MEMORY_BASIC_INFORMATION.GetField('RegionSize', [Reflection.BindingFlags] 'NonPublic, Instance')

    try { $NativeUtils = [NativeUtils] } catch [Management.Automation.RuntimeException] {
        # Only build the assembly if it hasn't already been defined
        
        $DynAssembly = New-Object Reflection.AssemblyName('MemHacker')
        $AssemblyBuilder = [AppDomain]::CurrentDomain.DefineDynamicAssembly($DynAssembly, [Reflection.Emit.AssemblyBuilderAccess]::Run)
        $ModuleBuilder = $null # $AssemblyBuilder.DefineDynamicModule('MemHacker', $False)
        $Attributes = 'AutoLayout, AnsiClass, Class, Public, SequentialLayout, Sealed, BeforeFieldInit'
        $TypeBuilder = $ModuleBuilder.DefineType('NativeUtils', $Attributes, [ValueType])
        $TypeBuilder.DefinePInvokeMethod('ReadProcessMemory', 'kernel32.dll', [Reflection.MethodAttributes] 'Public, Static', [Reflection.CallingConventions]::Standard, [Bool], @([IntPtr], [IntPtr], [IntPtr], [UInt32], [UInt32].MakeByRefType()), [Runtime.InteropServices.CallingConvention]::Winapi, 'Auto') | Out-Null
        $TypeBuilder.DefinePInvokeMethod('VirtualQueryEx', 'kernel32.dll', [Reflection.MethodAttributes] 'Public, Static', [Reflection.CallingConventions]::Standard, [UInt32], @([IntPtr], [IntPtr], $MEMORY_BASIC_INFORMATION.MakeByRefType(), [UInt32]), [Runtime.InteropServices.CallingConvention]::Winapi, 'Auto') | Out-Null

        $NativeUtils = $TypeBuilder.CreateType()
    }

    # Request a handle to the process in interest
    try {
        $SafeHandle = $GetPProcessHHandle.Invoke($Process, @($PROCESS_VM_READ))
        $Handle = $SafeHandle.DangerousGetHandle()
    }
    catch {
        throw $Error[0]
    }

    $MemoryBasicInformation = [Activator]::CreateInstance($MEMORY_BASIC_INFORMATION)

    $NativeUtils::VirtualQueryEx($Handle, $MemoryAddress, [Ref] $MemoryBasicInformation, [Runtime.InteropServices.Marshal]::SizeOf([Type] $MEMORY_BASIC_INFORMATION)) | Out-Null

    $PAGE_EXECUTE_READ = 0x20
    $PAGE_EXECUTE_READWRITE = 0x40
    $PAGE_READONLY = 2
    $PAGE_READWRITE = 4

    $Protection = $ProtectField.GetValue($MemoryBasicInformation)
    $AllocationBaseOriginal = $AllocationBaseField.GetValue($MemoryBasicInformation)
    $GetPointerValue = $AllocationBaseOriginal.GetType().GetMethod('GetPointerValue', [Reflection.BindingFlags] 'NonPublic, Instance')
    $AllocationBase = $GetPointerValue.Invoke($AllocationBaseOriginal, $null).ToInt64()
    $RegionSize = $RegionSizeField.GetValue($MemoryBasicInformation).ToUInt64()

    Write-Verbose "Protection: $Protection"
    Write-Verbose "AllocationBase: $AllocationBase"
    Write-Verbose "RegionSize: $RegionSize"

    if (($Protection -ne $PAGE_READONLY) -and ($Protection -ne $PAGE_READWRITE) -and ($Protection -ne $PAGE_EXECUTE_READ) -and ($Protection -ne $PAGE_EXECUTE_READWRITE)) {
        $SafeHandle.Close()
        throw 'The address specified does not have read access.'
    }

    $StructSize = [Runtime.InteropServices.Marshal]::SizeOf([Type] $StructType)
    $EndOfAllocation = $AllocationBase + $RegionSize
    $EndOfStruct = $MemoryAddress.ToInt64() + $StructSize

    if ($EndOfStruct -gt $EndOfAllocation) {
        $SafeHandle.Close()
        throw 'You are attempting to read beyond what was allocated.'
    }

    try {
        # Allocate unmanaged memory. This will be used to store the memory read from ReadProcessMemory
        $LocalStructPtr = [Runtime.InteropServices.Marshal]::AllocHGlobal($StructSize)
    }
    catch [OutOfMemoryException] {
        throw Error[0]
    }

    Write-Verbose "Memory allocated at 0x$($LocalStructPtr.ToString("X$([IntPtr]::Size * 2)"))"

    $ZeroBytes = New-Object Byte[]($StructSize)
    [Runtime.InteropServices.Marshal]::Copy($ZeroBytes, 0, $LocalStructPtr, $StructSize)

    $BytesRead = [UInt32] 0

    if ($NativeUtils::ReadProcessMemory($Handle, $MemoryAddress, $LocalStructPtr, $StructSize, [Ref] $BytesRead)) {
        $SafeHandle.Close()
        [Runtime.InteropServices.Marshal]::FreeHGlobal($LocalStructPtr)
        throw ([ComponentModel.Win32Exception][Runtime.InteropServices.Marshal]::GetLastWin32Error())
    }

    Write-Verbose "Struct Size: $StructSize"
    Write-Verbose "Bytes read: $BytesRead"

    $ParsedStruct = [Runtime.InteropServices.Marshal]::PtrToStructure($LocalStructPtr, [Type] $StructType)

    [Runtime.InteropServices.Marshal]::FreeHGlobal($LocalStructPtr)
    $SafeHandle.Close()

    Write-Output $ParsedStruct
}

############################

########## INFO ############

############################

function Get-PProcessSStrings {

    [CmdletBinding()] Param (
        [Parameter(Position = 0, Mandatory = $True, ValueFromPipelineByPropertyName = $True)]
        [Alias('Id')]
        [ValidateScript( { Get-Process -Id $_ })]
        [Int32]
        $ProcessID,

        [UInt16]
        $MinimumLength = 3,

        [ValidateSet('Default', 'Ascii', 'Unicode')]
        [String]
        $Encoding = 'Default',

        [Switch]
        $IncludeImages,

        [Switch]
        $Ip4Address,

        [Switch]
        $Uri
    )

    $global:PStrings = [System.AppDomain]::CurrentDomain.GetAssemblies() | Where-Object FullName -match 'PStrings'
    if (!$PStrings) {
        $global:PStrings = New-IInMMemoryMModule -ModuleName PStrings
    }

    $FunctionDefinitions = @(
        (Get-Ffunc kernel32 OpenProcess ([IntPtr]) @([UInt32], [Bool], [UInt32]) -SetLastError),
        (Get-Ffunc kernel32 ReadProcessMemory ([Bool]) @([IntPtr], [IntPtr], [Byte[]], [Int], [Int].MakeByRefType()) -SetLastError),
        (Get-Ffunc kernel32 CloseHandle ([Bool]) @([IntPtr]) -SetLastError)
    )

    $global:Types = $FunctionDefinitions | Get-AAddWWin332TType -Module $PStrings -Namespace 'Win32ProcessStrings'
    $global:K32Str = $Types['kernel32']
    
    $hProcess = $K32Str::OpenProcess(0x10, $False, $ProcessID) # PROCESS_VM_READ (0x00000010)

    Get-PProcessMMemoryIInfo -ProcessID $ProcessID | Where-Object { $_.State -eq 'MEM_COMMIT' } | ForEach-Object {
        $Allocation = $_
        $ReadAllocation = $True
        if (($Allocation.Type -eq 'MEM_IMAGE') -and (-not $IncludeImages)) { $ReadAllocation = $False }
        # Do not attempt to read guard pages
        if ($Allocation.Protect.ToString().Contains('PAGE_GUARD')) { $ReadAllocation = $False }

        if ($ReadAllocation) {
            $Bytes = New-Object Byte[]($Allocation.RegionSize)

            $BytesRead = 0
            $Result = $K32Str::ReadProcessMemory($hProcess, $Allocation.BaseAddress, $Bytes, $Allocation.RegionSize, [Ref] $BytesRead)
            
            if (($Encoding -eq 'Ascii') -or ($Encoding -eq 'Default')) {
                # This hack will get the raw ascii chars. The System.Text.UnicodeEncoding object will replace some unprintable chars with question marks.
                $ArrayPtr = [Runtime.InteropServices.Marshal]::UnsafeAddrOfPinnedArrayElement($Bytes, 0)
                $RawString = [Runtime.InteropServices.Marshal]::PtrToStringAnsi($ArrayPtr, $Bytes.Length)
                $Regex = [Regex] "[\x20-\x7E]{$MinimumLength,}"
                $RMatches = $Regex.Matches($RawString) 
                
                if ($Uri) {
                    [regex] $UriRegex = "(http[s]?|[s]?ftp[s]?)(:\/\/)([^\s,]+)"
                    $UriMatches = $RMatches | Where-Object $_.Value -Match $UriRegex
                } 
                if ($ipv4address) {
                    $byte = "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
                    [regex] $IPv4Regex = "$byte$dot$byte$dot$byte$dot$byte"
                    $IPv4Matches = $RMatches | Where-Object $_.Value -Match $IPv4Regex
                }
                
                $RMatches | ForEach-Object {
                    $Properties = @{
                        Address  = [IntPtr] ($Allocation.BaseAddress + $_.Index)
                        Encoding = 'Ascii'
                        String   = $_.Value
                    }

                    $String = New-Object PSObject -Property $Properties
                    $String.PSObject.TypeNames.Insert(0, 'MEM.STRING')

                    Write-Output $String
                }
            }
            if (($Encoding -eq 'Unicode') -or ($Encoding -eq 'Default')) {
                $Encoder = New-Object System.Text.UnicodeEncoding
                $RawString = $Encoder.GetString($Bytes, 0, $Bytes.Length)
                $Regex = [Regex] "[\u0020-\u007E]{$MinimumLength,}"
                
                $RMatches = $Regex.Matches($RawString) 
                
                if ($Uri) {
                    [regex] $UriRegex = "(http[s]?|[s]?ftp[s]?)(:\/\/)([^\s,]+)"
                    $RMatches = $RMatches | Where-Object $_.Value -Match $UriRegex
                } 
                elseif ($ipv4address) {
                    $byte = "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
                    [regex] $IPv4Regex = "$byte$dot$byte$dot$byte$dot$byte"
                    $RMatches = $RMatches | Where-Object $_.Value -Match $IPv4Regex
                }
                $RMatches | ForEach-Object {
                    $Properties = @{
                        Address  = [IntPtr] ($Allocation.BaseAddress + ($_.Index * 2))
                        Encoding = 'Unicode'
                        String   = $_.Value
                    }

                    $String = New-Object PSObject -Property $Properties
                    $String.PSObject.TypeNames.Insert(0, 'MEM.STRING')

                    Write-Output $String 
                }
            }
            $Bytes = $null
        }
    }
    $null = $K32Str::CloseHandle($hProcess)
}    
function Get-PProcessMMemoryIInfo {

    Param (
        [Parameter(ParameterSetName = 'InMemory', Position = 0, Mandatory = $True, ValueFromPipelineByPropertyName = $True)]
        [Alias('Id')]
        [ValidateScript( { Get-Process -Id $_ })]
        [Int]
        $ProcessID
    )

    $SysInfo = Get-SystemInfo

    $MemoryInfo = Get-VVirtualMMemoryIInfo -ProcessID $ProcessID -ModuleBaseAddress ([IntPtr]::Zero) -PageSize $SysInfo.PageSize

    $MemoryInfo

    while (($MemoryInfo.BaseAddress + $MemoryInfo.RegionSize) -lt $SysInfo.MaximumApplicationAddress) {
        $BaseAllocation = [IntPtr] ($MemoryInfo.BaseAddress + $MemoryInfo.RegionSize)
        $MemoryInfo = Get-VVirtualMMemoryIInfo -ProcessID $ProcessID -ModuleBaseAddress $BaseAllocation -PageSize $SysInfo.PageSize
        
        if ($MemoryInfo.State -eq 0) { break }
        $MemoryInfo
    }
}
function ConvertSidToStringSid {
    param (
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $SidPointer    
    )
    
    $StringPtr = [IntPtr]::Zero
    $Success = $Advapi32::ConvertSidToStringSid($SidPointer, [ref]$StringPtr); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if (-not $Success) {
        Write-Debug "ConvertSidToStringSid Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }
    
    Write-Output ([System.Runtime.InteropServices.Marshal]::PtrToStringAuto($StringPtr))
}
function GetTokenInformation {

    param
    (
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $TokenHandle,
        
        [Parameter(Mandatory = $true)]
        $TokenInformationClass 
    )

    # initial query to determine the necessary buffer size
    $TokenPtrSize = 0
    $Success = $Advapi32::GetTokenInformation($TokenHandle, $TokenInformationClass, 0, $TokenPtrSize, [ref]$TokenPtrSize)
    [IntPtr]$TokenPtr = [System.Runtime.InteropServices.Marshal]::AllocHGlobal($TokenPtrSize)
    
    # retrieve the proper buffer value
    $Success = $Advapi32::GetTokenInformation($TokenHandle, $TokenInformationClass, $TokenPtr, $TokenPtrSize, [ref]$TokenPtrSize); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()
    
    if ($Success) {
        switch ($TokenInformationClass) {
            1 {
                # TokenUser    
                $TokenUser = $TokenPtr -as $TOKEN_USER
                ConvertSidToStringSid -SidPointer $TokenUser.User.Sid
            }
            3 {
                # TokenPrivilege
                # query the process token with the TOKEN_INFORMATION_CLASS = 3 enum to retrieve a TOKEN_PRIVILEGES structure
                $TokenPrivileges = $TokenPtr -as $TOKEN_PRIVILEGES
                
                $sb = New-Object System.Text.StringBuilder
                
                for ($i = 0; $i -lt $TokenPrivileges.PrivilegeCount; $i++) {
                    if ((($TokenPrivileges.Privileges[$i].Attributes -as $LuidAttributes) -band $LuidAttributes::SE_PRIVILEGE_ENABLED) -eq $LuidAttributes::SE_PRIVILEGE_ENABLED) {
                        $sb.Append(", $($TokenPrivileges.Privileges[$i].Luid.LowPart.ToString())") | Out-Null  
                    }
                }
                Write-Output $sb.ToString().TrimStart(', ')
            }
            17 {
                # TokenOrigin
                $TokenOrigin = $TokenPtr -as $LUID
                Write-Output (Get-LogonSession -LogonId $TokenOrigin.LowPart)
            }
            22 {
                # TokenAccessInformation
            
            }
            25 {
                # TokenIntegrityLevel
                $TokenIntegrity = $TokenPtr -as $TOKEN_MANDATORY_LABEL
                switch (ConvertSidToStringSid -SidPointer $TokenIntegrity.Label.Sid) {
                    $UNTRUSTED_MANDATORY_LEVEL {
                        Write-Output "UNTRUSTED_MANDATORY_LEVEL"
                    }
                    $LOW_MANDATORY_LEVEL {
                        Write-Output "LOW_MANDATORY_LEVEL"
                    }
                    $MEDIUM_MANDATORY_LEVEL {
                        Write-Output "MEDIUM_MANDATORY_LEVEL"
                    }
                    $MEDIUM_PLUS_MANDATORY_LEVEL {
                        Write-Output "MEDIUM_PLUS_MANDATORY_LEVEL"
                    }
                    $HIGH_MANDATORY_LEVEL {
                        Write-Output "HIGH_MANDATORY_LEVEL"
                    }
                    $SYSTEM_MANDATORY_LEVEL {
                        Write-Output "SYSTEM_MANDATORY_LEVEL"
                    }
                    $PROTECTED_PROCESS_MANDATORY_LEVEL {
                        Write-Output "PROTECTED_PROCESS_MANDATORY_LEVEL"
                    }
                    $SECURE_PROCESS_MANDATORY_LEVEL {
                        Write-Output "SECURE_PROCESS_MANDATORY_LEVEL"
                    }
                }
            }
        }
    }
    else {
        Write-Debug "GetTokenInformation Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }        
    try {
        [System.Runtime.InteropServices.Marshal]::FreeHGlobal($TokenPtr)
    }
    catch {
    
    }
}
function NtQueryInformationThread {

    param (
        [Parameter(Mandatory = $true)]
        [IntPtr]
        $ThreadHandle  
    )
    
    $buf = [System.Runtime.InteropServices.Marshal]::AllocHGlobal([IntPtr]::Size)

    $Success = $Ntdll::NtQueryInformationThread($ThreadHandle, 9, $buf, [IntPtr]::Size, [IntPtr]::Zero); $LastError = [Runtime.InteropServices.Marshal]::GetLastWin32Error()

    if (-not $Success) {
        Write-Debug "NtQueryInformationThread Error: $(([ComponentModel.Win32Exception] $LastError).Message)"
    }
    
    Write-Output ([System.Runtime.InteropServices.Marshal]::ReadIntPtr($buf))
}
function Get-WinConstants {
    $Win32Constants = New-Object System.Object

    $Win32Constants | Add-Member -MemberType NoteProperty -Name MEM_COMMIT -Value 0x00001000
    $Win32Constants | Add-Member -MemberType NoteProperty -Name MEM_RESERVE -Value 0x00002000
    $Win32Constants | Add-Member -MemberType NoteProperty -Name PAGE_NOACCESS -Value 0x01
    $Win32Constants | Add-Member -MemberType NoteProperty -Name PAGE_READONLY -Value 0x02
    $Win32Constants | Add-Member -MemberType NoteProperty -Name PAGE_READWRITE -Value 0x04
    $Win32Constants | Add-Member -MemberType NoteProperty -Name PAGE_WRITECOPY -Value 0x08
    $Win32Constants | Add-Member -MemberType NoteProperty -Name PAGE_EXECUTE -Value 0x10
    $Win32Constants | Add-Member -MemberType NoteProperty -Name PAGE_EXECUTE_READ -Value 0x20
    $Win32Constants | Add-Member -MemberType NoteProperty -Name PAGE_EXECUTE_READWRITE -Value 0x40
    $Win32Constants | Add-Member -MemberType NoteProperty -Name PAGE_EXECUTE_WRITECOPY -Value 0x80
    $Win32Constants | Add-Member -MemberType NoteProperty -Name PAGE_NOCACHE -Value 0x200
    $Win32Constants | Add-Member -MemberType NoteProperty -Name IMAGE_REL_BASED_ABSOLUTE -Value 0
    $Win32Constants | Add-Member -MemberType NoteProperty -Name IMAGE_REL_BASED_HIGHLOW -Value 3
    $Win32Constants | Add-Member -MemberType NoteProperty -Name IMAGE_REL_BASED_DIR64 -Value 10
    $Win32Constants | Add-Member -MemberType NoteProperty -Name IMAGE_SCN_MEM_DISCARDABLE -Value 0x02000000
    $Win32Constants | Add-Member -MemberType NoteProperty -Name IMAGE_SCN_MEM_EXECUTE -Value 0x20000000
    $Win32Constants | Add-Member -MemberType NoteProperty -Name IMAGE_SCN_MEM_READ -Value 0x40000000
    $Win32Constants | Add-Member -MemberType NoteProperty -Name IMAGE_SCN_MEM_WRITE -Value 0x80000000
    $Win32Constants | Add-Member -MemberType NoteProperty -Name IMAGE_SCN_MEM_NOT_CACHED -Value 0x04000000
    $Win32Constants | Add-Member -MemberType NoteProperty -Name MEM_DECOMMIT -Value 0x4000
    $Win32Constants | Add-Member -MemberType NoteProperty -Name IMAGE_FILE_EXECUTABLE_IMAGE -Value 0x0002
    $Win32Constants | Add-Member -MemberType NoteProperty -Name IMAGE_FILE_DLL -Value 0x2000
    $Win32Constants | Add-Member -MemberType NoteProperty -Name IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE -Value 0x40
    $Win32Constants | Add-Member -MemberType NoteProperty -Name IMAGE_DLLCHARACTERISTICS_NX_COMPAT -Value 0x100
    $Win32Constants | Add-Member -MemberType NoteProperty -Name MEM_RELEASE -Value 0x8000
    $Win32Constants | Add-Member -MemberType NoteProperty -Name TOKEN_QUERY -Value 0x0008
    $Win32Constants | Add-Member -MemberType NoteProperty -Name TOKEN_ADJUST_PRIVILEGES -Value 0x0020
    $Win32Constants | Add-Member -MemberType NoteProperty -Name SE_PRIVILEGE_ENABLED -Value 0x2
    $Win32Constants | Add-Member -MemberType NoteProperty -Name ERROR_NO_TOKEN -Value 0x3f0

    return $Win32Constants
}

function Add-CreateTA {
    $CCreateTThreadAAddr = Get-ProcAddress Kernel32.dll CreateThread
    $CCreateTThreadDelegate = Get-DelegateType @([IntPtr], [IntPtr], [IntPtr], [IntPtr], [UInt32], [UInt32].MakeByRefType()) ([IntPtr])
    $CCreateTThread = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($CCreateTThreadAAddr, $CCreateTThreadDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name CCreateTThread -Value $CCreateTThread
}
function Add-WritePM {
    $WWritePProcessMMemoryAAddr = Get-ProcAddress kernel32.dll WriteProcessMemory
    $WWritePProcessMMemoryDelegate = Get-DelegateType @([IntPtr], [IntPtr], [IntPtr], [UIntPtr], [UIntPtr].MakeByRefType()) ([Bool])
    $WWritePProcessMMemory = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($WWritePProcessMMemoryAAddr, $WWritePProcessMMemoryDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name WWritePProcessMMemory -Value $WWritePProcessMMemory
}
function Add-CreateRT {
    $CCreateRRemoteTThreadAAddrr = Get-ProcAddress kernel32.dll CreateRemoteThread
    $CCreateRRemoteTThreadDDelegate = Get-DelegateType @([IntPtr], [IntPtr], [UIntPtr], [IntPtr], [IntPtr], [UInt32], [IntPtr]) ([IntPtr])
    $CCreateRRemoteTThread = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($CCreateRRemoteTThreadAAddrr, $CCreateRRemoteTThreadDDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name CCreateRRemoteTThread -Value $CCreateRRemoteTThread
}
function Get-WinFunctions {

    $global:Win32Functions = New-Object System.Object

    $VirtualAllocAddr = Get-ProcAddress kernel32.dll VirtualAlloc
    $VirtualAllocDelegate = Get-DelegateType @([IntPtr], [UIntPtr], [UInt32], [UInt32]) ([IntPtr])
    $VirtualAlloc = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($VirtualAllocAddr, $VirtualAllocDelegate)
    $Win32Functions | Add-Member NoteProperty -Name VirtualAlloc -Value $VirtualAlloc

    $VirtualAllocExAddr = Get-ProcAddress kernel32.dll VirtualAllocEx
    $VirtualAllocExDelegate = Get-DelegateType @([IntPtr], [IntPtr], [UIntPtr], [UInt32], [UInt32]) ([IntPtr])
    $VirtualAllocEx = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($VirtualAllocExAddr, $VirtualAllocExDelegate)
    $Win32Functions | Add-Member NoteProperty -Name VirtualAllocEx -Value $VirtualAllocEx

    $memcpyAddr = Get-ProcAddress msvcrt.dll memcpy
    $memcpyDelegate = Get-DelegateType @([IntPtr], [IntPtr], [UIntPtr]) ([IntPtr])
    $memcpy = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($memcpyAddr, $memcpyDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name memcpy -Value $memcpy

    $memsetAddr = Get-ProcAddress msvcrt.dll memset
    $memsetDelegate = Get-DelegateType @([IntPtr], [Int32], [IntPtr]) ([IntPtr])
    $memset = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($memsetAddr, $memsetDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name memset -Value $memset

    $LoadLibraryAddr = Get-ProcAddress kernel32.dll LoadLibraryA
    $LoadLibraryDelegate = Get-DelegateType @([String]) ([IntPtr])
    $LoadLibrary = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($LoadLibraryAddr, $LoadLibraryDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name LoadLibrary -Value $LoadLibrary

    $GetProcAddressAddr = Get-ProcAddress kernel32.dll GetProcAddress
    $GetProcAddressDelegate = Get-DelegateType @([IntPtr], [String]) ([IntPtr])
    $GetProcAddress = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($GetProcAddressAddr, $GetProcAddressDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name GetProcAddress -Value $GetProcAddress

    $GetProcAddressIntPtrAddr = Get-ProcAddress kernel32.dll GetProcAddress 
    $GetProcAddressIntPtrDelegate = Get-DelegateType @([IntPtr], [IntPtr]) ([IntPtr])
    $GetProcAddressIntPtr = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($GetProcAddressIntPtrAddr, $GetProcAddressIntPtrDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name GetProcAddressIntPtr -Value $GetProcAddressIntPtr

    $VirtualFreeAddr = Get-ProcAddress kernel32.dll VirtualFree
    $VirtualFreeDelegate = Get-DelegateType @([IntPtr], [UIntPtr], [UInt32]) ([Bool])
    $VirtualFree = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($VirtualFreeAddr, $VirtualFreeDelegate)
    $Win32Functions | Add-Member NoteProperty -Name VirtualFree -Value $VirtualFree

    $VirtualFreeExAddr = Get-ProcAddress kernel32.dll VirtualFreeEx
    $VirtualFreeExDelegate = Get-DelegateType @([IntPtr], [IntPtr], [UIntPtr], [UInt32]) ([Bool])
    $VirtualFreeEx = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($VirtualFreeExAddr, $VirtualFreeExDelegate)
    $Win32Functions | Add-Member NoteProperty -Name VirtualFreeEx -Value $VirtualFreeEx

    $VirtualProtectAddr = Get-ProcAddress kernel32.dll VirtualProtect
    $VirtualProtectDelegate = Get-DelegateType @([IntPtr], [UIntPtr], [UInt32], [UInt32].MakeByRefType()) ([Bool])
    $VirtualProtect = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($VirtualProtectAddr, $VirtualProtectDelegate)
    $Win32Functions | Add-Member NoteProperty -Name VirtualProtect -Value $VirtualProtect

    $GetModuleHandleAddr = Get-ProcAddress kernel32.dll GetModuleHandleA
    $GetModuleHandleDelegate = Get-DelegateType @([String]) ([IntPtr])
    $GetModuleHandle = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($GetModuleHandleAddr, $GetModuleHandleDelegate)
    $Win32Functions | Add-Member NoteProperty -Name GetModuleHandle -Value $GetModuleHandle

    $FreeLibraryAddr = Get-ProcAddress kernel32.dll FreeLibrary
    $FreeLibraryDelegate = Get-DelegateType @([IntPtr]) ([Bool])
    $FreeLibrary = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($FreeLibraryAddr, $FreeLibraryDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name FreeLibrary -Value $FreeLibrary

    $OpenProcessAddr = Get-ProcAddress kernel32.dll OpenProcess
    $OpenProcessDelegate = Get-DelegateType @([UInt32], [Bool], [UInt32]) ([IntPtr])
    $OpenProcess = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($OpenProcessAddr, $OpenProcessDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name OpenProcess -Value $OpenProcess

    $WaitForSingleObjectAddr = Get-ProcAddress kernel32.dll WaitForSingleObject
    $WaitForSingleObjectDelegate = Get-DelegateType @([IntPtr], [UInt32]) ([UInt32])
    $WaitForSingleObject = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($WaitForSingleObjectAddr, $WaitForSingleObjectDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name WaitForSingleObject -Value $WaitForSingleObject
    
    $RReadPProcessMMemoryAAddr = Get-ProcAddress kernel32.dll ReadProcessMemory
    $RReadPProcessMMemoryDelegate = Get-DelegateType @([IntPtr], [IntPtr], [IntPtr], [UIntPtr], [UIntPtr].MakeByRefType()) ([Bool])
    $RReadPProcessMMemory = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($RReadPProcessMMemoryAAddr, $RReadPProcessMMemoryDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name RReadPProcessMMemory -Value $RReadPProcessMMemory

    $LLookupPPrivilegeVValueAAddr = Get-ProcAddress Advapi32.dll LookupPrivilegeValueA
    $LookupPrivilegeValueDelegate = Get-DelegateType @([String], [String], [IntPtr]) ([Bool])
    $LookupPrivilegeValue = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($LLookupPPrivilegeVValueAAddr, $LookupPrivilegeValueDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name LookupPrivilegeValue -Value $LookupPrivilegeValue

    $IImpersonateSSelfAAddr = Get-ProcAddress Advapi32.dll ImpersonateSelf
    $IImpersonateSSelfDelegate = Get-DelegateType @([Int32]) ([Bool])
    $IImpersonateSSelf = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($IImpersonateSSelfAAddr, $IImpersonateSSelfDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name IImpersonateSSelf -Value $IImpersonateSSelf

    $IsWow64ProcessAddr = Get-ProcAddress Kernel32.dll IsWow64Process
    $IsWow64ProcessDelegate = Get-DelegateType @([IntPtr], [Bool].MakeByRefType()) ([Bool])
    $IsWow64Process = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($IsWow64ProcessAddr, $IsWow64ProcessDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name IsWow64Process -Value $IsWow64Process

    $GetExitCodeThreadAddr = Get-ProcAddress kernel32.dll GetExitCodeThread
    $GetExitCodeThreadDelegate = Get-DelegateType @([IntPtr], [Int32].MakeByRefType()) ([Bool])
    $GetExitCodeThread = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($GetExitCodeThreadAddr, $GetExitCodeThreadDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name GetExitCodeThread -Value $GetExitCodeThread

    $OpenThreadTokenAddr = Get-ProcAddress Advapi32.dll OpenThreadToken
    $OpenThreadTokenDelegate = Get-DelegateType @([IntPtr], [UInt32], [Bool], [IntPtr].MakeByRefType()) ([Bool])
    $OpenThreadToken = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($OpenThreadTokenAddr, $OpenThreadTokenDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name OpenThreadToken -Value $OpenThreadToken
    
    $AAdjustTTokenPPrivilegesAAddr = Get-ProcAddress Advapi32.dll AdjustTokenPrivileges
    $AAdjustTTokenPPrivilegesDelegate = Get-DelegateType @([IntPtr], [Bool], [IntPtr], [UInt32], [IntPtr], [IntPtr]) ([Bool])
    $AAdjustTTokenPPrivileges = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($AAdjustTTokenPPrivilegesAAddr, $AAdjustTTokenPPrivilegesDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name AAdjustTTokenPPrivileges -Value $AAdjustTTokenPPrivileges

    $GGetCCurrentTThreadAAddr = Get-ProcAddress kernel32.dll GetCurrentThread
    $GGetCCurrentTThreadDelegate = Get-DelegateType @() ([IntPtr])
    $GGetCCurrentTThread = [System.Runtime.InteropServices.Marshal]::GetDelegateForFunctionPointer($GGetCCurrentTThreadAAddr, $GGetCCurrentTThreadDelegate)
    $Win32Functions | Add-Member -MemberType NoteProperty -Name GGetCCurrentTThread -Value $GGetCCurrentTThread

    Add-CreateTA
    Add-WritePM
    Add-CreateRT

    return $Win32Functions
}

############################

########## MAIN ############

############################


$global:MemoryToolsFunctions = $MemoryToolsCmdlets.Name

$MemoryToolsCmdlets | Foreach-Object { Set-Alias -Name $_.Alias -Value $_.Name -Scope Global -Force }


Export-ModuleMember -Function $MemoryToolsFunctions 
}
