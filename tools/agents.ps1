New-Module -Name Agents -Scriptblock {

    [array]$global:AgentsCmdlets = @(
    
        $(New-Object -TypeName psobject -Property @{
            name          = "Get-Agent"
            Alias         = "gag"
            Description   = "Get media, tech, ngos, etc"
            Category      = "i"
            Mod           = "agent"
            Status        = "good"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Get-Woke"
            Alias         = "gwo"
            Description   = "Get woke agents"
            Category      = "i"
            Mod           = "agent"
            Status        = "good"
        }),        
        $(New-Object -TypeName psobject -Property @{
            name          = "Get-Error"
            Alias         = "ger"
            Description   = "Get biases, fallacies"
            Category      = "i"
            Mod           = "agent"
            Status        = "good"
        })             
        $(New-Object -TypeName psobject -Property @{
            name          = "Get-AgentCmdlets"
            Alias         = "gacmd"
            Description   = "Get cmdlets"
            Category      = "h"
            Mod           = "agent"
            Status        = "good"
        }),               
        $(New-Object -TypeName psobject -Property @{
            name          = "Get-AgentTools"
            Alias         = "at"
            Description   = "Get tools"
            Category      = "h"
            Mod           = "agent"
            Status        = "good"
        })
    )        

    [array]$global:agentList = @(
        $(New-Object -TypeName psobject -Property @{
            name          = "The Athletic"
            owner         = "New York Times"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Aldridge, David")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Athletic"
            owner         = "New York Times"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Amick, Sam")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "WarnerMedia"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Anthony, Greg")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "NBA.com"
            owner         = "NBA"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Aschburner, Steve")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "LEquipe"
            owner         = "LEquipe"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Mass Media"
            employees     = @("Aubin, Maxime")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "BAND"
            owner         = "Grupo Bandeirantes de Comunicacao"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Barao, Eduardo")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "PBWA/GQ"
            owner         = "non-profit organization"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Beck, Howard")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "New York Daily News"
            owner         = "Alden Global Capital"
            holder        = @("")
            links         = @("")
            type          = "tabloid"
            ownerType     = "Holding Company"
            employees     = @("Bondy, Stefan")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Bontemps, Tim")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Charlotte Observer"
            owner         = "Chatham Asset Management"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Holding Company"
            employees     = @("Boone, Rod")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Fox Sports"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Broussard, Chris")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "FS1"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Bucher, Ric")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Athletic"
            owner         = "New York Times"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Buha, Jovan")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Burke, Doris")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Dallas Morning News"
            owner         = "Dallas News Corp"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Holding Company"
            employees     = @("Caplan, Callie")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Athletic"
            owner         = "New York Times"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Charania, Shams")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Miami Herald"
            owner         = "Chatham Asset Management"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Holding Company"
            employees     = @("Chiang, Anthony")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Memphis Commercial Appeal"
            owner         = "SoftBank Group"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Holding Company"
            employees     = @("Cole, Damichael")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Collier, Jamal")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Movistar+"
            owner         = "Telefonica"
            holder        = @("")
            links         = @("")
            type          = "tabloid"
            ownerType     = "Mass Media"
            employees     = @("Daimiel, Antoni")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Yahoo! Sports"
            owner         = "Apollo Global Management Inc"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Holding Company"
            employees     = @("Devine, Dan")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Cleveland.com"
            owner         = "Warner Bros Discovery"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Fedor, Chris")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Oregonian"
            owner         = "Warner Bros Discovery"
            holder        = @("")
            links         = @("")
            type          = "tabloid"
            ownerType     = "Mass Media"
            employees     = @("Fentress, Aaron")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Toronto Sun"
            owner         = "Chatham Asset Management"
            holder        = @("")
            links         = @("")
            type          = "tabloid"
            ownerType     = "Holding Company"
            employees     = @("Ganter, Mike")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Sirius Radio"
            owner         = "Liberty Media"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Geltzeiler, Brian")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Yahoo! Sports"
            owner         = "Apollo Global Management Inc"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Holding Company"
            employees     = @("Goodwill, Vince")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Rogers Sportsnet"
            owner         = "Rogers Communications"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Grange, Michael")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "WarnerMedia"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Greenberg, Jared")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Greenberg, Mike")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Athletic"
            owner         = "New York Times"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Guillory, Will")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Athletic"
            owner         = "New York Times"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Harper, Zach")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "DPA"
            owner         = "Deutsche Presse-Agentur GmbH (dpa)"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Haupt, Max")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "NBCSports.com"
            owner         = "Comcast"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Helin, Kurt")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Sports Illustrated"
            owner         = "Authentic Brands Group"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Herring, Chris")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Boston Globe"
            owner         = "Boston Globe Media Partners LLC"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Mass Media"
            employees     = @("Himmelsbach, Adam")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Hubbarth, Cassidy")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Athletic"
            owner         = "New York Times"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Iko, Kelly")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Sirius Radio"
            owner         = "Liberty Media"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Isola, Frank")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Jackson, Mark")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "WarnerMedia"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Johnson, Ernie")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Athletic"
            owner         = "New York Times"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Jones, Jason")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Bay Area News Group"
            owner         = "Alden Global Capital"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Holding Company"
            employees     = @("Kenney, Madeline")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Kestecher, Marc")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Salt Lake Tribune"
            owner         = "Salt Lake Tribune Inc"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Mass Media"
            employees     = @("Larsen, Andy")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "WarnerMedia"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Ledlow, Kristen")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "WarnerMedia"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Lefkoe, Adam")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "New York Post"
            owner         = "News Corp"
            holder        = @("")
            links         = @("")
            type          = "tabloid"
            ownerType     = "Mass Media"
            employees     = @("Lewis, Brian")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Lowe, Zach")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Associated Press"
            owner         = "Not-for-profit Cooperative"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Mahoney, Brian")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Ringer"
            owner         = "Spotify"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Mahoney, Rob")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Sports Illustrated"
            owner         = "CVC Capital Partners"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Holding Company"
            employees     = @("Mannix, Chris")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "San Antonio Express-News"
            owner         = "Hearst Communications"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Mass Media"
            employees     = @("McDonald, Jeff")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("McMenamin, Dave")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Meng, Chao")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Sports Graphic Number"
            owner         = "Japan"
            holder        = @("")
            links         = @("")
            type          = "tabloid"
            ownerType     = "Mass Media"
            employees     = @("Miyaji, Yoko")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Sport 5"
            owner         = "RGE Group"
            holder        = @("")
            links         = @("")
            type          = "tabloid"
            ownerType     = "Mass Media"
            employees     = @("Modai, Yoav")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Oklahoman"
            owner         = "SoftBank Group"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Holding Company"
            employees     = @("Mussatto, Joe")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Showtime"
            owner         = "Meta Platform"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Holding Company"
            employees     = @("Nichols, Rachel")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Ringer"
            owner         = "Spotify"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("OConnor, Kevin")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Milwaukee Journal Sentinel"
            owner         = "SoftBank Group"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Holding Company"
            employees     = @("Owczarski, James")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Pasch, Dave")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Pelton, Kevin")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Perkins, Kendrick")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Ringer"
            owner         = "Spotify"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Pina, Michael")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Philadelphia Inquirer"
            owner         = "Lenfest Institute"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Mass Media"
            employees     = @("Pompey, Keith")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "NBA.com"
            owner         = "NBA"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Powell, Shaun")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Orlando Sentinel"
            owner         = "Alden Global Capital"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Holding Company"
            employees     = @("Price, Khobi")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Arizona Republic"
            owner         = "SoftBank Group"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Holding Company"
            employees     = @("Rankin, Duane")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "WarnerMedia"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Ready, Stephanie")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Redick, JJ")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "CBSSports.com"
            owner         = "Meta Platform"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Holding Company"
            employees     = @("Reiter, Bill")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "beIN Sports"
            owner         = "State of Qatar"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Reverchon, Remi")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Associated Press"
            owner         = "Not-for-profit Cooperative"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Reynolds, Tim")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Athletic"
            owner         = "New York Times"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Robbins, Josh")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Fox Sports"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Rohlin, Melissa")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Bleacher Report"
            owner         = "Warner Bros Discovery (WBD)"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Rooks, Taylor")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Rose, Jalen")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Ringer"
            owner         = "Spotify"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Russillo, Ryen")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Detroit Free Press"
            owner         = "SoftBank Group"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Holding Company"
            employees     = @("Sankofa, Omari")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "NBA.com"
            owner         = "NBA"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Schuhmann, John")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "WarnerMedia"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Scott, Dennis")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Shelburne, Ramona")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Ringer"
            owner         = "Spotify"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Simmons, Bill")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Denver Post"
            owner         = "Alden Global Capital"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Holding Company"
            employees     = @("Singer, Mike")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Athletic"
            owner         = "New York Times"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Slater, Anthony")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Toronto Star"
            owner         = "Torstar"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Mass Media"
            employees     = @("Smith, Doug The")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Smith, Stephen A.")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "WarnerMedia"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Smith, Steve")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Ringer"
            owner         = "Spotify"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Sohi, Seerat")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Sirius Radio"
            owner         = "Liberty Media"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Termine, Justin")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "WarnerMedia"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Van Gundy, Stan")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Athletic"
            owner         = "New York Times"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Vardon, Joe")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "EFE News"
            owner         = "Government of Spain"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Villafranca, David")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Boston Globe"
            owner         = "Boston Globe Media Partners LLC"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Mass Media"
            employees     = @("Washburn, Gary")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Wilbon, Michael")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Windhorst, Brian")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "WarnerMedia"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "media network"
            ownerType     = "Mass Media"
            employees     = @("Winer, Matt")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "NBA.com"
            owner         = "NBA"
            holder        = @("")
            links         = @("")
            type          = "sports website"
            ownerType     = "Mass Media"
            employees     = @("Wright, Michael C.")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owner         = "Disney"
            holder        = @("")
            links         = @("")
            type          = "sports network"
            ownerType     = "Mass Media"
            employees     = @("Youngmisuk, Ohm")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "USA Today"
            owner         = "SoftBank Group"
            holder        = @("")
            links         = @("")
            type          = "daily newspaper"
            ownerType     = "Holding Company"
            employees     = @("Zillgitt, Jeff")
        }),    
        $(New-Object -TypeName psobject -Property @{
            name           = "TNT"
            owners         = "Warner Bros. Discovery"
            holders        = @("Advance Publications","Vanguard","BlackRock")
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name           = "CNN"
            owners         = "Warner Bros. Discovery"
            holders        = @("Advance Publications","Vanguard","BlackRock")
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "ABC"
            owners        = "Walt Disney Company"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "FOX"
            owners        = "FOX Corporation"
            holders  = @("Murdoch Family")
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "CBS"
            owners        = "Paramount Global"
            holders  = @("National Amusements")
            type           = "media"
        }),  
        $(New-Object -TypeName psobject -Property @{
            name          = "ESPN"
            owners        = "Walt Disney Company"
            holders  = @("Murdoch Family")
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "NBC"
            owners        = "Comcast"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Warner Media"
            owners        = "Walt Disney Company"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Athletic"
            owners        = "New York Times"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "New York Daily News"
            owners        = "Alden Global Capital"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Charlotte Observer"
            owners        = "Chatham Asset Management"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "New York Post"
            owners        = "News Corp"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Miami Herald"
            owners        = "Chatham Asset Management"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Commercial Appeal"
            owners        = "SoftBank Group"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Yahoo! Sports"
            owners        = "Apollo Global Management Inc"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Sirius Radio"
            owners        = "Liberty Media"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Sports Illustrated"
            owners        = "Authentic Brands Group"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Ringer"
            owners        = "Spotify"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "San Antonio Express-News"
            owners        = "Hearst Communications"
            holders       = @("")
            funders       = ""
            type           = "media"
        }), 
        $(New-Object -TypeName psobject -Property @{
            name          = "Facebook"
            owners        = "Meta Platform"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Instagram"
            owners        = "Meta Platform"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "WhatsApp"
            owners        = "Meta Platform"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Google"
            owners        = "Alphabet Inc"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Amazon"
            owners        = "Amazon Inc"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Apple"
            owners        = "Apple Inc"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Microsoft"
            owners        = "Microsoft Corporation"
            holders       = @("")
            funders       = ""

            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "CNBC"
            owners        = "Comcast"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "MSNBC"
            owners        = "Comcast"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "FOX News"
            owners        = "News Corp"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "FS1/FS2"
            owners        = "News Corp"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Wall Street Journal"
            owners        = "News Corp"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "TBS"
            owners        = "Warner Bros. Discovery"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Cinemax"
            owners        = "Warner Bros. Discovery"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Pandora"
            owners        = "Liberty Media"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Showtime"
            owners        = "Paramount"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Hulu"
            owners        = "Walt Disney Company"
            holders       = @("")
            funders       = ""
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Time"
            owners        = "Salesforce"
            holders  = @("FMR LLC","Vanguard","BlackRock")
            type           = "media"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Craig Newmark Philanthropies"
            owners        = ""
            holders       = @("")
            type          = "foundation"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Omidyar Group"
            owners        = ""
            holders       = @("")
            type          = "foundation"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Knight Foundation"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "foundation"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Gates Foundation"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "foundation"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Google News Initiative"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "foundation"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Facebook Journalism Project"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "foundation"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Ford Foundation"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "foundation"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "William and Flora Hewlett Foundation"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "foundation"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Open Society Foundations"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "foundation"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Rockefeller Foundation"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "foundation"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Charles Koch Foundation"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "foundation"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "National Science Foundation"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "U.S. Department of State"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "DARPA"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "U.S. Department of Defense"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "U.S. Department of Homeland Security"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "NATO"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "CISA"
            story         = "Congress created CISA in November 2018 to defend the U.S. from cybersecurity threats from hostile foreign actors. Alex Stamos was the senior leader at EIP and VP, which served as the deputized domestic disinformation flagger for DHS via Chris Krebs at CISA. Stamos in 2020 proposed that DHS centralize government censorship."
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "U.K. 77th Brigade"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Foreign Malign Influence Center"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "FDA"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "NIH"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "CDC"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "USDA"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "NIDIA"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = ""
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "agency"
        }),            
        $(New-Object -TypeName psobject -Property @{      
            name          = "Instagram"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tech"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Pintrest"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tech"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Twitter"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tech"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "TikTok"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tech"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "YouTube"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tech"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "LinkedIn"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tech"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "EU Disinfo Lab"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "ngo"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Advance Democracy"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "ngo"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Wikipedia"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "ngo"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Meedan"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "ngo"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Ad Council"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "ngo"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Center for Countering Digital Hate"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "ngo"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "First Draft"
            story         = "Claire Wardle cofounded and directed, First Draft News. a nonprofit coalition, in June 2015, to build the censorship complex. 2016, our original coalition expanded to become an international Partner Network of newsrooms, universities, platforms and civil society organizations. In 2017, while at the Shorenstein Center for Media, Politics and Public Policy at Harvards Kennedy School, Wardle helped develop the Information Disorder Lab, a framing that Aspen Institute would embrace. In June 2022, First Draft closed, but its work lives on at the Information Futures Lab at Brown University School of Public Health."
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "ngo"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Public Good Projects"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "ngo"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Credibility Coalition"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "ngo"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Algoritmic Transparency Institute"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "ngo"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "National Conference on Citizenship"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "ngo"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Bellingcat"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "ngo"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "DisInfoWatch"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Reveal EU"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "International Fact Checking Network"
            owners        = "Poynter Institute"
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Tampa Bay Times"
            owners        = "Poynter Institute"
            holders       = @("")
            funders       = ""
            type          = "media"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Lead Stories"
            owners        = "Lead Stories, LLC"
            story         = "Supplies fact-checking services to Facebook, publish defamatory statements to justify Facebook bans"  
            holders       = @("")
            links         = @(
                "https://digitalcommons.law.scu.edu/cgi/viewcontent.cgi?article=3369&context=historical"
            )
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Poynter Institute"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Myth Detector"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Politifact"
            owners        = "Poynter Institute"
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "MediaWise"
            owners        = "Poynter Institute"
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Verified"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Full Fact"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "FactCheck.me"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "BotCheck.me"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Trusted News Initiative"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "FactCheck.org"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Claim Buster"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Global Disinformation Index"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "factCheck"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Institute for Strategic Dialogue"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tank"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Alliance for Securing Democracy"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tank"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "German Marshall Fund"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tank"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Integrity Initiative"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tank"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Aspen Institute"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tank"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Atlantic Council"
            story         = "created the foreign-facing DisinfoPortal in June 2018, working directly with the National Endowment for Democracy (NED) and 23 organizations to censor election narratives leading up to the 2019 elections in Europe. Graham Brookie, leader of the Atlantic Council’s DFR Lab. Brookie served in the Obama White House on the National Security Council."
            owners        = ""
            holders       = @("")
            funders       = "US taxpayer funding to the Atlantic Council comes from the Defense Department, the US Marines, the US Air Force, the US Navy, the State Department, USAID, the National Endowment for Democracy, as well as energy companies and weapons manufacturers"
            type          = "tank"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Digital Forensics Research Lab"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tank"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Center for European Policy Analysis"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "tank"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Automated Controversy Detection"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "academic"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Information Futures Lab at Brown University"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "academic"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Annenberg Public Policy Center"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "academic"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Technology and Social Change Project at TJE"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "academic"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Shorenstein Center on Media, Politics, and Public Policy"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "academic"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Duke Reporters Lab"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "academic"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "University of Washington Center for an Informed Public"
            story         = "Kate Starbird, runs the University of Washington disinformation lab, has for years been funded primarily by U.S. government agencies to do social media narrative analytics of political groups, or insurgency movements, of interest or concern to U.S. military intelligence or diplomatic equities. Starbird acknowledged that the censorship focus of CISA and EIP moved from foreign, inauthentic social media users to domestic, authentic social media users between 2016 to 2020. Starbird is now the head of CISA’s censorship advisory subcommittee"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "academic"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Clemson University Media Forensics Hub"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "academic"
        }), 
        $(New-Object -TypeName psobject -Property @{      
            name          = "Stanford Internet Observatory"
            story         = "DiResta was the research director for the organization caught creating bot accounts and spreading disinformation about Alabama Republican Senate Candidate Roy Moore."
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "academic"
        })
        $(New-Object -TypeName psobject -Property @{      
            name          = "DisInfoCloud"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "forProfit"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "New Knowledge / Yonder"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "forProfit"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Park Advisors"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "forProfit"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Moonshot CVE"
            story         = "private firm to redirect right-wing people online away from radicalism27 but was found to have pushed right-wing people toward an anarchist leader. Includes Elizabeth Neumann, former DHS Asst. Sec. for Counter Terrorism."
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "forProfit"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Graphika"
            story         = "a private network analysis firm, published a report for the Senate Intelligence Committee in December 2018, which claimed Russia's interference in 2016 U.S. presidential election. DoD Minerva Initiative which focuses on psychological warfare, and DARPA, both gave grants to Graphika. In 2021, the Pentagon awarded nearly 5 million in grants and nearly 2 million in contracts to the organization"
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "forProfit"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Miburo / Digital Threat Analysis Center"
            story         = ""
            owners        = ""
            holders       = @("")
            funders       = ""
            type          = "academic"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Ben Nimmo"
            story         = "Head of Global Threat Intelligence for Facebook, and thus one of America’s most important censors. Nimmo was the technical lead for censorship at the Atlantic Council's Digital Forensics Research Lab, was employed by Graphika in the fall of 2020 46, and worked in NATO information operations.47 In 2018, Nimmo publicly reported an anonymous Twitter account, Ian56, as a Russian disinformation bot account because it expressed left-of-center populist anti-war views when in reality Ian56 was a real person.48 After Nimmo's report, Ian56 was reported to the UK government"
            owners        = ""
            holders       = ""
            funders       = ""
            type          = "person"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "LinkedIn"
            story         = "Head of Global Threat Intelligence for Facebook, and thus one of America’s most important censors. Nimmo was the technical lead for censorship at the Atlantic Council's Digital Forensics Research Lab, was employed by Graphika in the fall of 2020 46, and worked in NATO information operations.47 In 2018, Nimmo publicly reported an anonymous Twitter account, Ian56, as a Russian disinformation bot account because it expressed left-of-center populist anti-war views when in reality Ian56 was a real person.48 After Nimmo's report, Ian56 was reported to the UK government"
            owners        = "Microsoft"
            holders       = ""
            funders       = ""
            type          = "food"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Chris Krebs"
            story         = "CISA Director (2018 to 2020). Chair of Aspen Institute Commission on Information Disorder, helped organize DHSs wholeof-society approach to censorship.42 Krebs administered the federal side of the 2020 election after DHS effectively nationalized election security on January 6, 2017, via the declaration of elections as critical infrastructure. Krebs then declared that misinformation was an attack on election security. Krebs said in April 2022 that the Hunter Biden laptop still looked like Russian disinformation and that what mattered was that news media did not cover the laptop during the 2020 election cycle.43 Krebs advocated for censoring critics of government COVID-19 protocols44 and said misinformation is the largest threat to election security."
            owners        = ""
            holders       = ""
            funders       = ""
            type          = "person"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Stamos"
            story         = "Stamos was the Chief Security Officer of Facebook and led Facebook's response to alleged Russian disinformation after the 2016 election. Stamos left Facebook, now Meta, in 2018, after reportedly conflicting with other Facebook executives over how much to censor.Stamos says he favors moving away from a free and open Internet toward a more controlled cable news network model. A huge part of the problem is large influencers, said Stamos"
            owners        = ""
            holders       = ""
            funders       = ""
            type          = "person"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Jen Easterly"
            story         = "CISA Director. A former military intelligence officer and the National Security Agency (NSA) deputy director for counterterrorism. One could argue we’re in the business of critical infrastructure, said Easterly in November 2021, and the most critical infrastructure is our cognitive infrastructure, so building that resilience to misinformation and disinformation, I think, is incredibly important. The month before, Easterly said during a CISA summit that Chris Krebs's construction of a counter-misinformation complex with the private sector was a high priority for DHS.40 A U.S. District Court ruled in October of last year that Easterly could be deposed because of her first-hand knowledge of the CISA nerve center around disinformation"
            owners        = ""
            holders       = ""
            funders       = ""
            type          = "person"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name          = "Yevgheny Prigozhin"
            story         = "Longtime Putin-ally and founder of the Wagner Group"
            owners        = ""
            holders       = ""
            funders       = ""
            type          = "person"
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Disney"
            owner         = ""
            holder        = @("")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("ESPN","Marvel","Lucasfilm","ABC","21st Century Studios","History","A&E","Lifetime","Pixar","Hulu","Hollywood Records","National Geographic","VICE Media")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Amazon"
            owner         = ""
            holder        = @("")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("Whole Foods","Zappos","PillPack","Twitch","MGM Holdings","Audible","Goodreads","iMDB","Ring","Lab126","")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Google"
            owner         = ""
            holder        = @("")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("Adsense","Android","DoubleClick","YouTube","Nest","Waze","Fitbit")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Anschutz Corporation"
            owner         = ""
            holder        = @("")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("AEG","Clarity Media Group","Xanterra Travel Collection","Washington Examiner")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Apple"
            owner         = ""
            holder        = @("")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("Beats","Beddit","Claris","InVisage")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "AT&T"
            owner         = ""
            holder        = @("")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("DirecTV")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Anheuser-Busch/Budweiser"
            owner         = ""
            holder        = @("")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("Budweiser")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Axel Springer SE"
            owner         = ""
            holder        = @("")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("Auto Bild","Audio Video Foto Bild","Computer Bild","Sport Bild","Auto.cz","Fakt","B.Z.","Watchmi","Musikexpress","Transfermarkt","Business Insider","Insider","upday","Politico","Politico Europe","Protocol")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Bank of America"
            owner         = ""
            holder        = @("Berkshire Hathaway")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Rolling Stone"
            owner         = ""
            holder        = @("")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "American City Business Journals"
            owner         = "Advance Publications"
            holder        = @("")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("")
        }),
       $(New-Object -TypeName psobject -Property @{
            name          = "Conde Nast"
            owner         = "Advance Publications"
            holder        = @("")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("Vanity Fair")
        }),
        $(New-Object -TypeName psobject -Property @{
             name          = "InBev"
             owner         = "Advance Publications"
             holder        = @("")
             links         = @("")
             type          = ""
             ownerType     = ""
             employees     = @("Becks","Brahma","Corona","Leffe","Stella Artois","")
         }),
         $(New-Object -TypeName psobject -Property @{
            name          = "Daily Beast"
            owner         = "IAC"
            holder        = @("BlackRock","Vanguard","JP Morgan")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("Daily Beast","Entertainment Weekly","Health","People","Shape")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Economist"
            owner         = "Economist Group"
            holder        = @("Exor N.V.","Rothschild","Cadbury","Schroder")
            links         = @("")
            type          = ""
            ownerType     = ""
            employees     = @("Economist")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Atlantic"
            owner         = "Emerson Collective"
            holder        = @("Laurene Powell Jobs")
            links         = @("")
            type          = "magazine"
            ownerType     = "philanthrocapitalism"
            employees     = @("Economist")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Guardian"
            owner         = "Guardian Media Group"
            holder        = @("Scott Trust Limited")
            links         = @("")
            type          = "newspaper"
            ownerType     = ""
            employees     = @("The Guardian", "The Observer")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Observer"
            owner         = "Guardian Media Group"
            holder        = @("Scott Trust Limited")
            links         = @("")
            type          = "newspaper"
            ownerType     = ""
            employees     = @("The Guardian", "The Observer")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "The Telegraph"
            owner         = "Press Holdings"
            holder        = @("Frederick Barclay")
            links         = @("")
            type          = "newspaper"
            ownerType     = "holding company"
            employees     = @("The Telegraph", "The Daily Telegraph", "The Sunday Telegraph")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Sky Group"
            owner         = "Comcast"
            holder        = @("")
            links         = @("")
            type          = "media"
            ownerType     = "media"
            employees     = @("Sky Uk", "Sky Ireland", "Sky Italia","Sky Studios","Now","Sky News")
        }),
        $(New-Object -TypeName psobject -Property @{
            name          = "Adidas"
            owner         = "Comcast"
            holder        = @("")
            links         = @("")
            type          = "media"
            ownerType     = "media"
            employees     = @("Sky Uk", "Sky Ireland", "Sky Italia","Sky Studios","Now","Sky News")
        })
)



   $wokeSingles = @(
        '1-800 Flowers',
        '23andMe',
        '3M',
        'AARP',
        'Abercrombie & Fitch',
        'Activision (maker of Call of Duty and many other video games)',
        'Absolut Vodka',
        'Accenture',
        'Adidas',
        'Adobe ',
        'Advanced Auto Parts',
        'AIG',
        'Airbnb',
        'Alaska Airlines',
        'Albertsons/Safeway',
        'All Elite Wrestling',
        'Ally Bank/Ally Financial',
        'Amalgamated Bank',
    
        'American Airlines',
        'American Express',
        'American Honda Motor Company',
        'Amerisource Bergen',
        'Anastasia Beverly Hills',
        'Anthropologie',
        'Aritzia',
        'Asana',
        'Audi Volkswagen',
        'Axe',
        'Bain & Company',
        'Balenciaga',
        'Banana Republic',
        'Bank of America',
        'Bank of New York Mellon',
        'Bass Pro Shops',
        'Bath & Body Works',
        'Bayer',
        'Bed, Bath & Beyond',
        'Ben & Jerry',
        'Berkshire Partners',
        'Best Buy',
        'Billie',
        'Biogen',
        'Biossance',
        'Blackrock',
        'Bliss',
        'Bloomin Brands (Outback Steakhouse, Carrabba Italian Grill, Bonefish Grill, etc.) ',
        'Blue Cross Blue Shield',
        'BMC Software',
        'Boeing',
        'Boston Consulting Group',
        'Box Inc.',
        'Boy Smells',
        'Brahmin',
        'Bravado',
        'BrewDog',
        'Bristol Myers Squibb',
        'Broadridge Financial Solutions',
        'Bumble',
        'Burger King',
        'Burton Snowboards',
        'Buzzfeed',
        'Cadbury',
        'Cambridge Associates',
        'Capital One',
        'Cardinal Health',
        'Caribou Coffee',
        'CBRE',
        'CeraVe (Dylan Mulvaney sponsor)',
        'Champion/Hanes',
        'Chevron',
        'Chick-fil-A',
        'Chipotle',
        'Chobani',
        'Cigna',
        'Cisco',
        'Citigroup',
        'Civic Entertainment Group',
        'Climate First Bank',
        'Climb Credit',
        'Coca-Cola Company',
        'CODAworx',
        'Collina Strada',
        'Colourpop',
        'Comcast NBC Universal',
        'Condé Nast',
        'Constant Contact',
        'Converse',
        'Cowboy Ventures',
        'Cracker Barrel',
        'Creative Artists Agency',
        'Credit Karma',
        'Crest (Dylan Mulvaney sponsor)',
        'Cushman & Wakefield',
        'CVS',
        'David Jones',
        'Deckers Brands (UGG, HOKA, Teva, Sanuk, and Koolaburra by UGG)',
        'Dell Technologies',
        'Deloitte',
        'Delta Airlines',
        'Deutsche Bank',
        'Discord',
        'Discover Card',
        'Docusign',
        'Dollar Tree',
        'Door Dash',
        'Dr. Martens',
        'Dropbox',
        'Duke Energy',
        'Duolingo',
        'Eaton',
        'Ebay',
        'Emerson Collective',
        'Equinor',
        'Erie Insurance',
        'Estee Lauder',
        'Etsy',
        'Eventbrite',
        'Everlane',
        'Evite (woke direct marketing emails, including one from a “LGBTQ + activist.”)',
        'Expedia',
        'EY',
        'Fabletics',
        'Family Dollar',
        'Fashion Nova',
        'Ferrara',
        'FirstMark Capital',
        'Fitbit',
        'For Love and Lemons',
        'Ford Motor Company',
        'Fossil',
        'Four Seasons Hotels & Resorts',
        'Fox Corporation/Fox News',
        'Frontier Airlines',
        'Gannett Company',
        'Ganni',
        'Gap',
        'Gatorade',
        'GE',
        'Geico',
        'General Catalyst',
        'General Mills (Cheerios, Bisquick, Haagen Dazs, etc)',
        'General Motors',
        'Giant Eagle',
        'Gilead Sciences',
        'Glossier',
        'Go Daddy',
        'Goldman Sachs',
        'Goodreads (please read about how these dirty rotten scoundrels cancelled me and my books here!)',
        'Grosvenor Capital Management',
        'Grubhub',
        'H & M',
        'H & R Block',
        'Hachette',
        'Hairhouse',
        'Hanes',
        'Harper Collins',
        'Harris Teeter',
        'Harry Inc.',
        'Hasbro',
        'Haus Labs  (Dylan Mulvaney sponsor)',
        'Hershey (Chocolates)',
        'Hess',
        'Hilton',
        'Honest Beauty',
        'Hooker Furniture Company',
        'Hotel Tonight (woke emails)',
        'HP',
        'Hummel',
        'Hyatt Group Hotels & Resorts',
        'IBM',
        'IHG Hotels & Resorts',
        'Impact Wrestling',
        'Impossible Foods',
        'Indeed',
        'Ingersoll Rand',
        'Insight Partners Leadership',
        'Instacart (Dylan Mulvaney sponsor)',
        'Intel',
        'Intelligensia Coffee',
        'Intuit',
        'J & J',
        'J Crew',
        'Jazz Lincoln Center',
        'Jeep',
        'Jeni Ice Cream (Nancy Pelosi Ice Cream!) ',
        'JetBlue',
        'JLL',
        'Justeattakeaway.com',
        'Kate Spade',
        'Kellog Company',
        'Kenneth Cole',
        'Khosla Ventures',
        'Kind',
        'Kitchen Aid (Dylan Mulvaney sponsorship)',
        'Kohl ',
        'Kraft Heinz',
        'Kroger',
        'La Colombe',
        'Lego',
        'Legoland, home of the world largest Lego Pride parade',
        'Leidos',
        'Levi Stauss & Co.',
        'Lipslut',
        'Live Nation Entertainment',
        'Lockheed Martin',
        'Loop Capital Markets',
        'Los Angeles Dodgers (AKA Sisters of Perpetual Indulgence)',
        'Love Beauty & Planet (Dylan Mulvaney sponsor)',
        'Lowe',
        'Lucozade',
        'Lululemon',
        'Lush Cosmetics',
        'Lyft',
        'M & T Bank',
        'Mac Cosmetics (Dylan Mulvaney sponsorship)',
        'Macmillan',
        'Macy',
        'Madewell',
        'Mailchimp',
        'Marc Jacobs',
        'Marriott',
        'Mars North America',
        'Mastercard',
        'Match Group (Match.com, Tinder, OK Cupid etc.)',
        'Mattel',
        'Mattress Firm',
        'McDonald',
        'McKesson Corporation',
        'McKinsey & Company',
        'Merck',
        'Merrill Lynch',
        'Metlife',
        'Michael',
        'Milk Makeup',
        'Miller Brewing Co.',
        'Misfits Market',
        'MLB',
        'MLS',
        'Molson Coors',
        'Molson Coors Beverage Company',
        'Momentive/Survey Monkey',
        'Mondelez International',
        'Mondelez International (Oreos)',
        'Morphe',
        'Mutual of Omaha',
        'Nabisco',
        'NAE Vegan Shoes',
        'Nascar',
        'Native (Dylan Mulvaney sponsor)',
        'Natura & Co. ',
        'NCAA',
        'Netflix',
        'Neutrogena (Dylan Mulvaney sponsor)',
        'New Japan Pro Wrestling',
        'Newell Brands',
        'Newmark',
        'Next Door',
        'NFL',
        'NHL',
        'Nike',
        'Nordstrom',
        'North Face',
        'Office Depot',
        'Ohio Health',
        'OK Cupid',
        'Olay (Oil of Olay)',
        'Old Navy',
        'Oracle',
        'Oreos (Nabisco limited edition Pride Oreos)',
        'Orvis',
        'Otherwise Incorporated',
        'Pacifica Beauty',
        'Pacsun',
        'Paper Source',
        'Paramount',
        'Patagonia',
        'Paypal',
        'Peloton',
        'Penguin Random House',
        'Pepsi/Frito-Lay/Cracker Jacks/Jills',
        'Petsmart',
        'Pfizer',
        'Pinterest',
        'Plaid',
        'Planet Fitness',
        'PNC Bank',
        'Pokemon',
        'Pop Tarts (Kelloggs- Queer Pop Tarts)',
        'Popflex Active',
        'Prabal Gurung',
        'Predxion',
        'Proctor & Gamble/Pantene',
        'Progressive Insurance',
        'Prudential Financial',
        'Puma',
        'PwC',
        'Quaker Oats Co.',
        'QVC',
        'Ralph Lauren',
        'Raymond James',
        'Reddit',
        'Redfin',
        'Reebok',
        'REI Co-op',
        'Respondology',
        'Richer Poorer',
        'Rivian',
        'Safeway/Albertsons',
        'Saks 5th Avenue/Off 5th',
        'Salesforce',
        'Sam Club',
        'San Jose Sharks',
        'SAP',
        'Savage x Fenty, founded by Rihanna',
        'ScotiaBank',
        'Sephora',
        'ServiceNow',
        'Seventh Generation',
        'SHEIN',
        'SHIPT',
        'Shopify',
        'Simon & Schuster',
        'Skillshare',
        'Slack',
        'Slow Ventures',
        'Smith & Company',
        'Snap',
        'Sodastream (Dylan Mulvaney sponsor)',
        'Sodexo Magic',
        'Sodexo USA',
        'Softbank',
        'Sonos',
        'Sony Interactive Entertainment',
        'Sound Ventures',
        'Southwest Airlines',
        'Spanx',
        'Spark Capital',
        'Square',
        'Stanley Black & Decker',
        'Starbucks',
        'State Farm',
        'State Street Bank',
        'Steelcase',
        'Stitch Fix',
        'Strava',
        'Stripe',
        'Sundance',
        'Survey Monkey',
        'SVB Bank',
        'Sveda Vodka (Dylan Mulvaney sponsor)',
        'Sweetgreen',
        'Swiss Air',
        'Synchrony',
        'T. Rowe Price',
        'Tampa Bay Rays',
        'Tampax (Dylan Mulvaney sponsor)',
        'Target',
        'Tesla',
        'Teva (sandals)',
        'TGI Fridays',
        'That Game Company',
        'The Body Shop',
        'The New York Times (and almost every other news outlet in Western countries)',
        'Thrive Market',
        'Tiffany & Co.',
        'Tito Vodka',
        'TMobile',
        'Toms',
        'Tory Burch LLC',
        'Trillium Asset Management',
        'Tripadvisor',
        'Trout Unlimited',
        'Truist Financial',
        'Twilio',
        'Twitter (obviously less woke now under Elon Musk!)',
        'U.S. Armed Forces',
        'U.S. Men National Soccer Team',
        'Uber',
        'Ubisoft',
        'Ugg',
        'ULTA',
        'Ulta Beauty (Dylan Mulvaney sponsorship)',
        'Under Armour',
        'Unilever',
        'United Airlines',
        'United Health Group',
        'Urban Decay (Dylan Mulvaney sponsor)',
        'Urlazh (Dylan Mulvaney sponsor)',
        'USAA',
        'Vanguard',
        'Vans',
        'Verizon',
        'Viacom CBS',
        'Vice Media Group',
        'Victoria Secret',
        'Virgin Atlantic',
        'Vistaprint',
        'VMWare',
        'Vox Media',
        'Walmart',
        'Warburg Pincus',
        'Warby Parker',
        'Warner Media',
        'We Work',
        'Wells Fargo',
        'William Blair',
        'Williams Sonoma',
        'Wrigley (limited edition Pride skittles)',
        'Yahoo Inc',
        'Yelp',
        'Zendesk',
        'Zillow',
        'Zola',
        'Zoom Video Communications',
        'Zynga Games'
    )


    [array]$global:errorList = @(
        $(New-Object -TypeName psobject -Property @{      
            name  = "anchoring"
            story = "first thing you judge influences your judgement of all that follows"
            link  = "https://yourbias.is/anchoring"
            icon  = ""
            type = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "sunk cost"
            story = "irrationally clinging to things that have already cost you something"
            link  = "https://yourbias.is/the-sunk-cost-fallacy"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "availability heuristic"
            story = "judgements are influenced by what springs most easily to mind"
            link  = "https://yourbias.is/the-availability-heuristic"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "curse of knowledge"
            story = "once you understand something you presume it to be obvious to everyone"
            link  = "https://yourbias.is/the-curse-of-knowledge"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "confirmation"
            story = "favoring things that confirm your existing beliefs"
            link  = "https://yourbias.is/confirmation-bias"
            icon  = "https://upload.wikimedia.org/wikipedia/commons/0/0c/Fake_Debate_Fallacy_Icon_Black.svg"
            type  = "bias"
        }),    
        $(New-Object -TypeName psobject -Property @{      
            name  = "dunning-kruger effect"
            story = "the more you know, the less confident you're likely to be"
            link  = "https://yourbias.is/the-dunning-kruger-effect"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "belief"
            story = "rationalizing anything supporting conclusions that support your existing beliefs"
            link  = "https://yourbias.is/belief-bias"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "self-serving"
            story = "taking responsibility for your success and blaming external factors for your failures"
            link  = "https://yourbias.is/self-serving-bias"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "backfire effect"
            story = "when challenge to aspect of your core beliefs makes you believe something more strongly"
            link  = "https://yourbias.is/the-backfire-effect"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "barnum effect"
            story = "seeing personal specifics in vague statements by filling in the gaps"
            link  = "https://yourbias.is/the-barnum-effect"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "groupthink"
            story = "letting social dynamics of a group situation override the best outcomes"
            link  = "https://yourbias.is/groupthink"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "negativity"
            story = "allowing negative things to disproportionately influence your thinking"
            link  = "https://yourbias.is/negativity-bias"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "declinism"
            story = "remembering the past better than it was, expecting future to be worse that it will likely be"
            link  = "https://yourbias.is/declinism"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "framing effect"
            story = "unduly influenced by context and delivery"
            link  = "https://yourbias.is/the-framing-effect"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "fundamental attribution error"
            story = "judging others on their character, but yourself on the situation"
            link  = "https://yourbias.is/fundamental-attribution-error"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "halo effect"
            story = "how much you like someone influences your other judgements of them"
            link  = "https://yourbias.is/the-halo-effect"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "optimism"
            story = "overestimating the likelihood of positive outcomes"
            link  = "https://yourbias.is/optimism-bias"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "pessimism"
            story = "overestimating the likelihood of negative outcomes"
            link  = "https://yourbias.is/pessimism-bias"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "just-world hypothesis"
            story = "preference for justice makes you presume it exists"
            link  = "https://yourbias.is/just-world-hypothesis"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "in-group"
            story = "unfairly favoring those who belong to your group"
            link  = "https://yourbias.is/in-group-bias"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "placebo effect"
            story = "false attribution of something as a solution when it has no effect"
            link  = "https://yourbias.is/the-placebo-effect"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "bystander effect"
            story = "presuming someone else is going to do something in an emergency situation"
            link  = "https://yourbias.is/the-bystander-effect"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "reactance"
            story = "urge to do the opposite of what someone is trying to make you do"
            link  = "https://yourbias.is/reactance"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "spotlight effect"
            story = "overestimating how much people notice how you look and act"
            link  = "https://yourbias.is/the-spotlight-effect"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = ""
            story = "unfairly favoring those who belong to your group"
            link  = "https://yourbias.is/in-group-bias"
            icon  = ""
            type  = "bias"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "strawman"
            story = "misrepresenting an argument to make it easier to attack"
            link  = "https://yourlogicalfallacyis.com/strawman"
            icon  = "https://upload.wikimedia.org/wikipedia/commons/4/43/Straw_Man_Fallacy_Icon_Black.svg"
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "false cause"
            story = "presuming a real or perceived relationship between things proves causation"
            link  = "https://yourlogicalfallacyis.com/false-cause"
            icon  = "https://upload.wikimedia.org/wikipedia/commons/6/66/Single_Cause_Fallacy_Icon_Black.svg"
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "appeal to emotion"
            story = "manipulating emotional response in place of valid or compelling argument"
            link  = "https://yourlogicalfallacyis.com/appeal-to-emotion"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "fallacy fallacy"
            story = "presuming since a claim is poorly argued than the claim itself is wrong"
            link  = "https://yourlogicalfallacyis.com/the-fallacy-fallacy"
            icon  = "https://upload.wikimedia.org/wikipedia/commons/d/df/Logical_Fallacies_Fallacy_Icon_Black.svg"
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "slippery slope"
            story = "if A happens, Z will eventually happen, therefore A should not happen"
            link  = "https://yourlogicalfallacyis.com/slippery-slope"
            icon  = "https://upload.wikimedia.org/wikipedia/commons/3/39/Slippery_Slope_Fallacy_Icon_Black.svg"
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "ad hominem"
            story = "attacking opponent's character or personal traits in order to undermine argument"
            link  = "https://yourlogicalfallacyis.com/ad-hominem"
            icon  = "https://upload.wikimedia.org/wikipedia/commons/8/87/Ad_Hominem_Fallacy_Icon_Black.svg"
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "tu quoque"
            story = "avoiding criticism by turning it back on accuser"
            link  = "https://yourlogicalfallacyis.com/tu-quoque"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "personal incredulity"
            story = "because you're not aware or don't understand something, it's probably not true"
            link  = "https://yourlogicalfallacyis.com/personal-incredulity"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "special pleading"
            story = "moving goalposts or making exceptions when your claim is shown to be false"
            link  = "https://yourlogicalfallacyis.com/special-pleading"
            icon  = "https://upload.wikimedia.org/wikipedia/commons/3/30/Moving_Goalposts_Fallacy_Icon_Black.svg"
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "loaded-question"
            story = "asking a question with presumption built into it to make opponent appear guilty"
            link  = "https://yourlogicalfallacyis.com/loaded-question"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "burden of proof"
            story = "saying burde of proof isn't on person making claim, but someone else to disprove"
            link  = "https://yourlogicalfallacyis.com/burden-of-proof"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "ambiguity"
            story = "usig double meaning or ambiguity of language to mislead or misrepresent truth"
            link  = "https://yourlogicalfallacyis.com/ambiguity"
            icon  = "https://upload.wikimedia.org/wikipedia/commons/6/69/Ambiguity_Fallacy_Icon_Black.svg"
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "gamblers fallacy"
            story = "saying runs occur to statistically independent phenomena such as roulette wheel spins"
            link  = "https://yourlogicalfallacyis.com/the-gamblers-fallacy"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "bandwagon"
            story = "appeal to popularity or prevalance of something as attempted form of validation"
            link  = "https://yourlogicalfallacyis.com/bandwagon"
            icon  = "https://upload.wikimedia.org/wikipedia/commons/9/9a/Bulk_Fake_Experts_Fallacy_Icon_Black.svg"
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "appeal to authority"
            story = "since an authority thinks something, it must therefore be true"
            link  = "https://yourlogicalfallacyis.com/appeal-to-authority"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "composition division"
            story = "assuming part of something must be applied to all or whole must be applied to its parts"
            link  = "https://yourlogicalfallacyis.com/composition-division"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "no true scotsman"
            story = "appeal to purity to dismiss relevant criticisms or flaws of an argument"
            link  = "https://yourlogicalfallacyis.com/no-true-scotsman"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "genetic"
            story = "judging good or bad on basis of where it comes from or from whom it came"
            link  = "https://yourlogicalfallacyis.com/genetic"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "black or white"
            story = "presenting two alternative states as only possibilities when others exist"
            link  = "https://yourlogicalfallacyis.com/black-or-white"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "begging the question"
            story = "presenting circular argument in which conclusion is included in premise"
            link  = "https://yourlogicalfallacyis.com/begging-the-question"
            icon  = "https://upload.wikimedia.org/wikipedia/commons/5/55/Persecuted_Victim_Fallacy_Icon_Black.svg"
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "appeal to nature"
            story = "because something is natural it is therefore valid, justified, innevitable, good, or ideal"
            link  = "https://yourlogicalfallacyis.com/appeal-to-nature"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "anecdotal"
            story = "using personal experience or isolated example instead of sound argument or compelling evidence"
            link  = "https://yourlogicalfallacyis.com/anecdotal"
            icon  = "https://upload.wikimedia.org/wikipedia/commons/b/b1/Anecdote_Fallacy_Icon_Black.svg"
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "texas sharpshooter"
            story = "cherry-picking data clusters to suit argument, or finding pattern to fit a presumption"
            link  = "https://yourlogicalfallacyis.com/the-texas-sharpshooter"
            icon  = ""
            type  = "fallacy"
        }),
        $(New-Object -TypeName psobject -Property @{      
            name  = "middle ground"
            story = "claiming a compromise or middle point between two extremes must be the truth"
            link  = "https://yourlogicalfallacyis.com/middle-ground"
            icon  = ""
            type  = "fallacy"
        })
    )
    function Get-AgentTools {
        param(
        [string]$type = 'default'
        )
    
        Switch ($type) {
    
            'i'          {  Get-AgentCmdlets 'i'       }

            default        {
                
                Write-Host "`n"
                Write-Host -Fore DarkGray " ...:::" -NoNewLine;Write-Host "`tUsage:   " -NoNewLine;
                    Write-Host -Fore DarkGray "at " -NoNewLine;Write-Host -Fore Gray "[category]"
                Write-Host "`n"
        
                [pscustomobject]$options = [ordered]@{
                    'i'       = "Info `t >   Query for information"
                    'a'       = "Assets`t >   Info on assets"
                    'c'       = "Calc  `t >   Order calculations"
                    'o'       = "Orders`t >   Trading tools"
                    'p'       = "Prices`t >   Get prices"
                    's'       = "Set `t >   Set conditions"
                    'u'       = "Utils  `t >   Support functions"
                }
        
                $options.Keys | Foreach-Object {
                Write-Host -Fore DarkGray "`tat " -NoNewLine;
                Write-Host -Fore Gray "$_" -NoNewLine ; Write-Host "`t`t$($options.Item("$_"))"
                }
                Write-Host "`n"
            }
        }
    }
    function Get-AgentCmdlets {
        param(
            [string]$type
        )

        if (!$type) {
            $cmdlets = $agentCmdlets | Sort-Object Name
        }
        else {
            $cmdlets = $agentCmdlets | Where-Object Category -eq $type | Sort-Object Name
        }
        Write-Host "`n"
        $cmdlets | Select-Object Name,Alias,Description | Format-Table -Auto  |  Write-HostColored -PatternColorMap @{
            'Name'          ='DarkGray'
            'Alias'         ='DarkGray'
            'Description'   ='DarkGray'
            '-'             ='DarkCyan'} 

        Write-Host "`n"
    }
    function Get-Woke {
        param(
            [string]$term,
            [switch]$all
        )
        if ($term){
            if ($wokeSingles | Select-String $term){
                $wokeSingles | Select-String $term | Foreach-Object {$_.Line}
            }      
            if ($wokeCompanies.owner | Select-String $term){
                $wokeCompanies | Where-Object owner -match $term
            }
            if ($wokeCompanies.owned | Select-String $term){
                $wokeCompanies | Where-Object owned -match $term
            }        
        }
        else {
            $wokeCompanies
        }
    }
    function Get-Agent {
        param(
        [string]$name,
        [switch]$agency,
        [switch]$media,
        [switch]$ngo,
        [switch]$fact,
        [switch]$tank,
        [switch]$academic,
        [switch]$foundation,
        [switch]$tech
        )
        if ($name){
            $agentList | Select-Object name,type,owners,holders | Where-Object name -match $name | Sort-Object -Desc owners | Format-Table -auto
        }
        elseif ($agency){
            $agentList | Where-Object type -eq "agency" | Select-Object name,owners,holders | Sort-Object -Desc owners | Format-Table -auto
        }    
        elseif ($media){
            $agentList | Where-Object type -eq "media" | Select-Object name,owners,holders |  Sort-Object -Desc owners | Format-Table -auto
        }    
        elseif ($tech){
            $agentList | Where-Object type -eq "tech" | Select-Object name,owners,holders | Sort-Object -Desc owners | Format-Table -auto
        }    
        elseif ($ngo){
            $agentList | Where-Object type -eq "ngo" | Select-Object name,owners,holders |  Sort-Object -Desc owners | Format-Table -auto
        }    
        elseif ($fact){
            $agentList | Where-Object type -eq "factCheck" | Select-Object name,owners,holders | Sort-Object -Desc owners | Format-Table -auto
        }            
        elseif ($tank){
            $agentList | Where-Object type -eq "tank" | Select-Object name,owners,holders  | Sort-Object -Desc owners | Format-Table -auto
        }        
        elseif ($academic){
            $agentList | Where-Object type -eq "academic" | Select-Object name,owners,holders |  Sort-Object -Desc owners | Format-Table -auto
        }    
        elseif ($forProfit){
            $agentList | Where-Object type -eq "forProfit" | Select-Object name,owners,holders |  Sort-Object -Desc owners | Format-Table -auto
        }    
        else {
            $agentList | Select-Object type,name,owners,holders | Format-Table -auto
        }    
    }
    function Get-Error {
        param(
            [string]$name,
            [switch]$bias,
            [switch]$fallacy
        )
        if ($name){
            $errorList | Where-Object name -match $name | Select-Object name,type,story | Format-Table -auto
        }
        elseif ($bias){
            $errorList | Where-Object type -eq "bias" | Select-Object name, story | Sort-Object name | Format-Table -auto
        }    
        elseif ($fallacy){
            $errorList | Where-Object type -eq "fallacy" | Select-Object name, story |  Sort-Object name | Format-Table -auto
        }    
        else {
            $errorList | Select-Object name, type, desc | Format-Table -auto
        }    
    }

    $global:AgentsFunctions =  $AgentsCmdlets.Name 

    $AgentsCmdlets | Foreach-Object { Set-Alias -Name $_.Alias -Value $_.Name -Scope Global -Force }

    Export-ModuleMember -Function $AgentsFunctions 
} 




