|Title|Sample Files|
|-|-|
|[Active Defense Solutions for the Private Sector](./docs/capstone.md)| Private organizations need legal, inexpensive options for detecting intruders and actively defending their most valuable assets.  Active Defense tools offer a solution that leverages automation, denial and deception, counterdeception, intelligence, counterintelligence, and offensive countermeasures to improve IR capabilities, improve protection of critical data and assets, and deter intruders from carrying out further attacks|
[Advanced C2 Techniques](./docs/Advanced%20C2%20Techniques.md)|Advanced C2 tradecraft is focused around avoiding traditional detection techniques and mimicking common, approved services through use of legitimate certificates/processes/domains, indirect/asynchronous connections, irregular intervals, encryption and tunneling, redirectors, domain fronting, and third party services|
[Analysis of a Phishing Email](./docs/Analysis%20of%20a%20Phishing%20Email.md)|Emotet is a downloader that drops implants using malicious attachments or links in spam emails. Observing how malware interacts with the operating system is essential for understanding its full capabilities and what actions it performed on a victim system|
[Analyzing Domain Fronting Malware](./docs/Analyzing%20Domain%20Fronting%20Malware.md)|Domain fronting is the use of high-reputation domains such as Amazon CloudFront, Google, Microsoft Azure, CloudFlare, Fastly, and Akamai, to route traffic to another host on their infrastructure without revealing the identity of the destination host|
[Analyzing Excel Documents with Macros](./docs/Analyzing%20Excel%20Doc%20Macros.md)|Using olevba and oledump to analyze malicious Office documents with VBA macros, Embedded OLE objects, and embedded shellcode|
[Analyzing Malicious PowerShell Commands](./docs/Analyzing%20Malicious%20PowerShell%20Commands.md)|Not-really-a-pdf.pdf.lnk universal-patch.hta Hardening_Script.sct|
[Analyzing Malicious VBScript](./docs/Analyzing%20Malicious%20VBScript.md)|fo_exercise.pcapng vbe from INC0744592|
[Analyzing Malicious Websites with Thug](./docs/Analyzing%20Malicious%20Websites%20with%20Thug.md)|-|[Analyzing Packed Executables](./docs/Analyzing%20Packed%20Executables.md)|mnemonic1.exe|
[Application Whitelisting Bypasses](./docs/Application%20Whitelisting%20Bypasses.md)|bypass.bat/cmd/dll/exe/hta/ps1/sct/vbs/xml dotnet-bypass.exe mimikatz.exe assembly.xml|
[Behavioral vs Atomic Indicators](./docs/Behavioral%20vs%20Atomic%20Indicators.md)|-|
[Bits Bytes and Encoding](./docs/Bits%20Bytes%20and%20Encoding.md)|-|
[Building Analysis VMs with Packer Vagrant Malboxes](./docs/Building%20Analysis%20VMs.md)|win7/win10 Vagrant boxes jdeeres-pc|
[Code Signing Certificates](./docs/Code%20Signing%20Certificates.md)|-|
[Creating Analysis and Sandbox VMs](./docs/Creating%20Analysis%20and%20Sandbox%20VMs.md)|Prepare-PackageVM.ps1|
[Debugging a Windows Program](./docs/Debugging%20a%20Windows%20Program.md)|GoTeam.exe GoTeam.c|
[Deploying and Using Cuckoo Sandbox](./docs/Deploying%20and%20Using%20Cuckoo%20Sandbox.md)|272861.exe|
[Detecting Unmanaged PowerShell](./docs/Detecting%20Unmanaged%20Powershell.md)|umps.exe umps.dll|
[Developing TTP-based Responses](./docs/Developing%20TTP-based%20Responses.md)|9b57.exe|
[DNS Prefetching](./docs/DNS%20Prefetching.md)|-|
[Dynamic Analysis Walkthrough](./docs/Dynamic%20Analysis%20Walkthrough.md)|topmovies2017.xls|
[Endpoint Interrogation with PowerShell](./docs/Endpoint%20Interrogation%20PowerShell%201.md)|Get-ActiveProcesses.ps1 Get-Netstat.ps1 Get-LogonDetails.ps1 Get-FilesystemChanges.ps1 Get-PSAutoRun.ps1|
[Endpoint Interrogation with PowerShell Part 2](./docs/Endpoint%20Interrogation%20PowerShell%202.md)|Get-ShadowCopies.ps1 Get-AlternateDataStreams.ps1|
[Evasion Techniques on Disk](./docs/Evasion%20Techniques%20on%20Disk.md)|-|
[Evasion Techniques on the Network](./docs/Evasion%20Techniques%20on%20the%20Network.md)|-|
[Gathering Indicators and TTPs](./docs/Gathering%20Indicators%20and%20TTPs.md)|VM-USER-PC-2017-05-02-202227.raw|
[Gathering Information on a Rogue System](./docs/Gathering%20Information%20on%20Rogue%20System.md)|-|
[Hunting on the Network with Tshark](./docs/Hunting%20on%20the%20Network%20with%20Tshark.md)|CSIRT-pcap-7.pcapng|
[Identifying Trojan Executables](./docs/Identifying%20Trojan%20Executables.md)|WinSCP.exe|
[Identifying Persistence Techniques](./docs/Identifying%20Persistence%20Techniques.md)|Persistence-VM.ova|
[Identifying Persistence Techniques Solutions](./docs/Identifying%20Persistence%20Techniques%20Solutions.md)|-|
[Improved Detections Using YARA Rules](./docs/Improved%20Detections%20YARA%20rules.md)|Canada Post notice Card.doc|
[Indicator Context and Pivoting](./docs/Indicator%20Context%20and%20Pivoting.md)|-|
[Indicator Storage Sharing and Visualization](./docs/Indicator%20Storage%20Sharing%20Visualization.md)|REMnux-CSIRT-v2.ova|
[Introduction to Kali Linux](./docs/Introduction%20to%20Kali%20Linux.md)|-|
[Intrusion Analysis Using the Kill Chain](./docs/Intrusion%20Analysis%20Using%20Kill%20Chain.md)|CSIRT-pcap-4.pcap|
[Intrusion Analysis Using the Kill Chain Solution](./docs/Intrusion%20Analysis%20Using%20Kill%20Chain%20Solution.md)|-|
[JavaScript Analysis with NodeJs and DevTools](./docs/JavaScript%20Analysis%20with%20NodeJs%20and%20DevTools.md)|c.min[1].js, check.js|
[JavaScript and Browser-Based Malware](./docs/JavaScript%20and%20Browser-Based%20Malware.md)|html/|
[JavaScript Evasion Techniques](./docs/JavaScript%20Evasion%20Techniques.md)|html2/|
[Memory-based Attack Techniques](./docs/Memory-based%20Attack%20Techniques.md)|
[Obfuscation Encryption and Anti-Analysis Techniques](./docs/Obfuscation%20Encryption%20Anti-Analysis%20Techniques.md)|memupdaterpro.exe|
[Overview of Common Adversary Tactics and Techniques](./docs/Overview%20of%20Common%20Adversary%20Tactics%20and%20Techniques.md)|-|
[Overview of Incident Response and Intelligence Cycle](./docs/Overview%20of%20Incident%20Response%20and%20Intelligence%20Cycle.md)|-|[Phishing To Injection Techniques](./docs/Phishing%20To%20Injection%20Techniques.md)|evil-vba.hta evil-ps.hta evil-torch.hta|
[Pivoting and Link Analysis](./docs/Pivoting%20and%20Link%20Analysis.md)|-|
[Privilege Escalation in macOS](./docs/Privilege%20Escalation%20in%20macOS.md)|macOS Vagrant box|
[Privilege Escalation in Windows](./docs/Privilege%20Escalation%20in%20Windows.md)|-|
[Review of Windows Scripting Technologies](./docs/Review%20of%20Windows%20Scripting%20Technologies.md)|scripts/|
[Searching Chopping Joining and Replacing](./docs/Searching%20Chopping%20Joining%20and%20Replacing.md)|obfuscated1.js|
[Static Analysis of a Windows PE](./docs/Static%20Analysis%20of%20a%20Windows%20PE.md)|guidance.dll, artifact.exe|
[Using a Threat-based Approach For Detection](./docs/Using%20a%20Threat-based%20Approach%20For%20Detection.md)|-|
[Using Indicators for Detection](./docs/Using%20Indicators%20For%20Detection.md)|launcher.bat|
[Verifying Digital Signatures of PE Files](./docs/Verifying%20Digital%20Signatures%20of%20PE%20Files.md)|Check-FileSignature.ps1|
[Windows Processes and Memory Analysis](./docs/Windows%20Processes%20and%20Memory%20Analysis.md)|
[Working with PowerShell Script Modules](./docs/Working%20with%20PowerShell%20Script%20Modules.md)|Interrogate-Endpoint/, Invoke-Greeting/|











...
...



|Title|Sample Files|
|-|-|
[Postmortem Forensics in the Cloud](./docs/Postmortem%20Forensics%20in%20the%20Cloud.md)|Get-AWSLambdaCode.ps1, Get-AWSCloudTrailLogs.ps1|
[Postmortem Forensics with SIFT Workstation](./docs/Postmortem%20Forensics%20with%20SIFT%20Workstation.md)|-|
[AWS Access and Logging](./docs/AWS%20Access%20and%20Logging.md)|-|
[Review of Amazon Web Services](./docs/Review%20of%20Amazon%20Web%20Services.md)|-|
[Using the AWS Module](./docs/Using%20the%20AWS%20Module.md)|AWSmodule.psm1|
[Hardening AWS Environments Using External Accounts](./docs/Hardening%20AWS%20Environments%20Using%20External%20Accounts.md)|Get-ExtTempAWSCreds.ps1|
[Investigating AWS Internet Gateways](./docs/Investigating%20AWS%20Internet%20Gateways.md)|Get-AWSFlowLogs.ps1|
[AWS IAM Best Practices](./docs/AWS%20IAM%20Best%20Practices.md)|Get-TempAWSCreds.ps1|
[Authentication Using Smart Cards and Public Key Kerberos](./docs/Authentication%20Using%20Smart%20Cards%20and%20Public%20Key%20Kerberos.md)|logon.pcapng|
[Common Functions and Data Sources in Splunk](./docs/Common%20Functions%20and%20Data%20Sources%20in%20Splunk.md)|-|
[Analysis of Fareit Malware](./docs/Analysis%20of%20Fareit%20Malware.md)|RFQ.exe|
[Windows Mitigations and Defenses](./docs/Windows%20Mitigations%20and%20Defenses.md)|Backup-Files.ps1, Check-MitigationsDefenses.ps1|
[Using Docker on Windows](./docs/Using%20Docker%20on%20Windows.md)|friendlyhello.tar.xz|
[Web Authentication and Session Management](./docs/Web%20Authentication%20and%20Session%20Management.md)|Get-TempAWSCreds.ps1|
[Memory Analysis with Volatility](./docs/Memory%20Analysis%20with%20Volatility.md)|-|
[Tracking Lateral Movement By Authenticated RCE](./docs/Tracking%20Lateral%20Movement%20By%20Authenticated%20RCE.md)|-|
[Moving From Detection To Containment](./docs/Moving%20From%20Detection%20To%20Containment.md)|Get-InjectionFunctions.ps1|
[Hunting on the Endpoint with Volatility](./docs/Hunting%20on%20the%20Endpoint%20with%20Volatility.md)|win_7.mem|
[Filtering Expected Web Traffic in Splunk](./docs/Filtering%20Expected%20Web%20Traffic%20in%20Splunk.md)|webfilter.csv webfilter macro|
[Windows Firewall Rules Profiles and Logging](./docs/Windows%20Firewall%20Rules%20Profiles%20Logging.md)|Get-FirewallLog.ps1|
[Persistence Using WMI Event Subscriptions](./docs/Persistence%20Using%20WMI%20Event%20Subscriptions.md)|Test-WMIPersistence.ps1|
[REMnux Refresher](./docs/REMnux%20Refresher.md)|-|
[Using PowerShell for Efficiency](./docs/Using%20PowerShell%20for%20Efficiency.md)|-|
[Updates to Windows VMs](./docs/Updates%20to%20Windows%20VMs.md)|win7.box win10.box|
[Identifying Web Application Attacks](./docs/Identifying%20Web%20Application%20Attacks.md)|-|
[Traffic Analysis with Wireshark](./docs/Traffic%20Analysis%20with%20Wireshark.md)|wireshark_exercise.pcapng|
[Extracting Shellcode from a PDF](./docs/Extracting%20Shellcode%20from%20PDF.md)|Free-Money-Seminar.pdf|
[Identifying Tor Use on the Network](./docs/Identifying%20Tor%20Use%20on%20the%20Network.md)|CSIRT-pcap-5.pcapng|
[Detecting Lateral Movement with Splunk](./docs/Detecting%20Lateral%20Movement%20with%20Splunk.md)|-|
[Windows Authentication and Lateral Movement](./docs/Windows%20Authentication%20and%20Lateral%20Movement.md)|-|
[Analyzing PDFs with REMnux](./docs/Analyzing%20PDFs%20with%20REMnux.md)|InfosecCheatSheet.pdf|
[Identifying C2 on the Network](./docs/Identifying%20C2%20on%20Network.md)|C2_Examples dashboard|
[Investigating Malware on Macs](./docs/Investigating%20Malware%20on%20Macs.md)|CSIRT-pcap-3.pcap|
[Researching Threat Indicators](./docs/Researching%20Threat%20Indicators.md)|-|
[Analyzing Office Docs](./docs/Analyzing%20Office%20Docs.md)|CSIRT-pcap-2.pcapng CurrentSalariesReview2017.docx|
[Analyzing Browser Exploits](./docs/Analyzing%20Browser%20Exploits.md)|CSIRT-pcap-1.pcapng|
[Analyzing Executables](./docs/Analyzing%20Executables.md)|putty.exe |
[CSIRT Mac OSX VM](./docs/CSIRT%20MAC%20OSX%20VM.md)|-|
[SIFT REMnux and Windows VMs](./docs/SIFT%20REMnux%20and%20Windows%20VMs.md)|-|
