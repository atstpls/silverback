
1,2,7
11,12 known attacks, side channel
15 asymmetric
49 - 51 generating keys and storage
65 aes modes   
92 rc4
105 hash
112 MD 116 SHA
181 RSA
207 agreement protocol
209 DH types
217 ECC









encryption
gpg
openssl
files
luks


## GPG

encrypt

gpg --output encrypted.data --symmetric --cipher-algo AES256 unencrypted.data


decrypt

gpg --output unencrypted.data --decrypt encrypted.data


## OpenSSL


https://bettercrypto.org/static/applied-crypto-hardening.pdf


https://bettercrypto.org/static/applied-crypto-hardening.pdf


Encryption
― The process of converting ordinary information (plain text) into something
unreadable (cipher text) through the use of a mathematical formula called an
algorithm.


Symmetric (Secret Key Cryptography)
― Uses one key for both encryption and decryption.
― The one key must be shared by both the sender and the recipient, which
makes key management critical to the security of the crypto system.
― From a mathematical perspective, the calculations required for symmetric
cryptography are nearly 1,000 times faster than public key cryptography.
• Asymmetric (Public Key Cryptography)
― Uses two keys, one public (available to all) and one private (remains secret).
― Public verification key/private signing key.
― Public encryption key/private decryption key.
― Private keys sign/decrypt data.
― Third party (Certificate Authority) issues and maintains keys.


Though you have specifically asked about OpenSSL you might want to consider using GPG instead for the purpose of encryption based on this article OpenSSL vs GPG for encrypting off-site backups?
To use GPG to do the same you would use the following commands:
To Encrypt:
gpg --output encrypted.data --symmetric --cipher-algo AES256 un_encrypted.data
To Decrypt:
gpg --output un_encrypted.data --decrypt encrypted.data
Note: You will be prompted for a password when encrypting or decrypt.
