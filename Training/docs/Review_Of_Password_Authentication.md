# Passwords

horsebattery method 
sherobbedmyboots -> Username
unioncentralorchids -> domains



Show using Get-Cred with a script

```
irm -Headers @{"token": $(Get-Cred github)}
```

## Passwords
- Length
- Complexity
- Rotation and Reuse2FA

## Password managers
- standalone
- cloud


## Cracking Countermeasures
- Passphrases
- Hashing
- Salts
- Slow hashing
- 2FA



## Cryptographic Implementations


- Asymmetric crypto systems are used to generate/exchange symmetric keys
- Symmetric crypto is stream ciphers and block ciphers (these get targeted)


attack the implementation: web session cookies, protocols, apps, data encryption


### Symmetric

Stream
Encrypt one byte at a time
Can't use secret twice, so use IVs... longer IV means more unique keys before rotation is needed

Block
Encrypt data a block at a time
Modes:
	- ECB encrypts each block with the same key
	- CBC encrypts each block with ciphertext from previous block   (repeating IVs are vuln)
	- CTR

Identifying which algorithm
	- Not evenly divisible by 8: Stream, probably RC4
	- Always divisible by 16: probably AES (128)
	- Inconsistently divisible by 16, always divisible by 8: DES/3DES (64 bit)

Hashing - length and format can reveal type
	- Length: MD5: 16 byte,  SHA1: 20 byte
	- Format:

	Visual historgrams and entropy analysis tools can be used to identify encryption



CBC Bit Flipping ---
	Test WEB Server
Oracle Padding Attack ----
	POODLE   JS
Stream Cipher IV Reuse Attack - xor known plaintext (dhcp request) with encrypted text to get keystream
   	KRACK ATTACK  WPA
Hash Length Extension Attacks
	Server does md5(filename + secret key)... exploit with tool like hash extender








Get-Pwned PowerSHell Module

Import-Module HaveIBeenPwned

Take https://www.powershellgallery.com/packages/HaveIBeenPwned/1.0
and put in own user agent string


https://medium.com/@securitystreak/password-policy-template-2017-best-practices-cbdcbb6beb49


![](images/AWS%20EC2%20Best%20Practices/image001.png)<br><br>




Hashcat also supports several different attack modes:

Dictionary – Works through a list of potential passwords, trying them until one works.

Combinator – Concatenates words from multiple wordlists in attempts to guess right.

Brute force – Trying all combinations of characters in a given charset.

Hybrid – Combining wordlists with masks, using the added specificity to increase efficiency. No disadvantage compared to brute-forcing.

-m specifies the hash type.

– a defines the attack mode.

use the command "locate wordlist" to select wordlist


sudo apt update
sudo apt install cmake build-essential
sudo apt install checkinstall git
git clone https://github.com/hashcat/hashcat.git
cd hashcat
git submodule update –init
sudo make
sudo checkinstall
hashcat –version


Excellent! Now we’re going to need some OpenCL software which hashcat requires to utilize the CPUs for cracking. To this end, follow the commands below:

wget “http://registrationcenter-download.intel.com/akdlm/irc_nas/12556/opencl_runtime_16.1.2_x64_rh_6.4.0.37.tgz”
tar zxvf opencl_runtime_16.1.2_x64_rh_6.4.0.37.tgz
cd opencl_runtime_16.1.2_x64_rh_6.4.0.37
sudo ./install.sh
sudo hashcat -b to verify



benchmark

hashcat -b

For my first benchmark, I utilized a 4 GB memory, 2 CPU server with 80 GB of storage.

94431 kH/s means we’re attempting to guess the hash correctly 94,431,000 times per second. Of course, this is pulling the MD5 hash, which isn’t as secure as the standard SHA-512, which we’re guessing 9,846,000 times per second. Even more difficult, WPA/WPA2, the latter of which is an industry standard level of secure, we’re only guessing about 3,536,000 times a second


msfvenom - generates payloads in many formats executables, scripts, shellcode, (`msfvenom --list payloads`) and encodes it in many ways xor, base64, add, etc (`msfvenom --list encoders`)
 in a number of different formats exe, msi, vba, ps1, vbscript, etc. (`msfvenom --help-formats`)




 ```
 function Get-Cred ($account){  

  $file = "." + $account + ".txt"
  $g = cat $env:USERPROFILE\$file | ConvertTo-SecureString
  [System.Runtime.InteropServices.marshal]::PtrToStringAuto([System.Runtime.InteropServices.marshal]::SecureStringToBSTR($g))
}

function Add-Cred ($account) {

  $file = $env:USERPROFILE + "\." + $account + ".txt"
  Read-Host "Enter cred: " | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString | Out-File $file
}

function Test-Cred {  
  param ($cred)
  switch ($cred) {
    "slack" {Test-SlackToken}
    "git"   {Test-GitToken}
  }
}




function Test-SlackToken {
  $tok = Read-Host "Enter Slack Token: "
  Invoke-RestMethod https://uscis.slack.com/api/auth.test -Headers @{'Authorization'="Bearer $tok"}
}

function Test-GitToken {
  $tok = Read-Host "Enter Git Token: "
  Invoke-RestMethod https://git.uscis.dhs.gov/api/v3/user -Headers @{'Authorization'="Bearer $tok"}
}
```
