# Linux Mitigations and Defenses

Setup



```bash
echo 'PS1="\[\033[32m\](\[\033[37m\]me: \[\033[93m\]\w\[\033[32m\])\$\[\033[37m\] "' >> .bashrc

# use username
echo 'PS1="\[\033[32m\](\[\033[37m\]\u: \[\033[93m\]\w\[\033[32m\])\$\[\033[37m\] "' >> .bashrc
```

Defense in Depth
Compartmentalization






https://www.blackhat.com/us-18/training/aikido-on-the-command-line-linux-lockdown-and-proactive-security.html

https://www.sans.org/score/checklists/linux

https://bettercrypto.org/static/applied-crypto-hardening.pdf



Make sure to hit:

MFA - SMS, push notifications, or TOTP, but focus on U2F
Tor/VPN
BIOS password
SSH - disable root login, use keys (password protected) and disable password auth
configure sudo with visudo, use fullpaths and scripts to follow least privilege
AppArmor
SSH with Google Authenticator

















linux
sudo
ssh (local and remote) (p. 94)
groups
full paths
wrapper scripts
app armor
deny by default fw rules



![](images/AWS%20EC2%20Best%20Practices/image001.png)<br><br>

## Setup


- [Countering Malware](#countering-malware)
	- [Application Whitelisting](#application-whitelisting)
		- [AppLocker](#applocker)
		- [Device Guard](#device-guard)
	- [Patch Management](#patch-management)
	- [Disable Untrusted Office Features](#disable-untrusted-office-features)
		- [Disable Macros](#disable-macros)
		- [Disable OLE Object Execution](#disable-ole-execution)
		- [Disable ActiveX](#disable-activex)
		- [Disable DDE](#disable-dde)
	- [Disable AutoRun-AutoPlay](#disable-autorun-autoplay)
- [Limiting Extent of Damage](#limiting-extent-of-damage)               
	- [Least Privilege](#least-privilege)
		- [User Account Control](#user-account-control)
		- [Writable Windows Shares](#writable-windows-shares)
		- [Restrict Inbound Connections](#restrict-inbound-connections)
		- [Disable or Restrict Unused Features](#disable-or-restrict-unused-features)
	- [Multi-factor Authentication](#multi-factor-authentication)
		- [MFA on Local Accounts](#mfa-on-local-accounts)
		- [MFA on Domain Accounts](#mfa-on-domain-accounts)
	- [Daily Backups](#daily-backups)
		- [Logical or Physical Separation](#logical-or-physical-separation)
		- [Required Validity Check](#required-validity-check)
		- [Continuous Operation](#continuous-operation)
	- [Credential Hygiene](#credential-hygiene)
		- [Use Unique and Complex Passwords](#use-unique-and-complex-passwords)
		- [Monitor for Credential Exposure](#monitor-for-credential-exposure)
		- [Enable Credential Guard](#enable-credential-guard)

##

## Mitigations and Defenses

**Tactics** can be described as the art or skill of employing available means to accomplish an end.  

**Techniques** are the unique ways or methods used to perform functions such as spearphishing, credential theft, and drive-by downloads.

**Mitigations and Defenses** are the controls we have in place to prevent or counter the use of adversary tactics and techniques in our environment.  

Using the [ATT&CK Technique Matrix](https://attack.mitre.org/matrices/enterprise/linux/), we can get a list of tactics and techniques relevant to a Windows client machine and their respective mitigations/defenses:

|Tactics|Techniques|Mitigations and Defenses|
|-|-|-|
|[Initial Access](https://attack.mitre.org/wiki/Initial_Access)				|[Drive-by](https://attack.mitre.org/wiki/Technique/T1189), [Spearphishing](https://attack.mitre.org/wiki/Technique/T1193), [Removable Media](https://attack.mitre.org/wiki/Technique/T1091)				|[App Whitelisting](#application-whitelisting), [Disable Untrusted Office Features](#disable-untrusted-office-features), [Patch Management](#patch-management), [Sandbox-VM Isolation](#sandbox-vm-isolation), [EMET](), [Disable AutoRun-AutoPlay](#disable-autorun-autoplay)|
|[Execution](https://attack.mitre.org/wiki/Execution)						|[CLI](https://attack.mitre.org/wiki/Technique/T1059), [GUI](https://attack.mitre.org/wiki/Technique/T1061), [PowerShell](https://attack.mitre.org/wiki/Technique/T1086), [WMI](https://attack.mitre.org/wiki/Technique/T1047), [WinRM](https://attack.mitre.org/wiki/Technique/T1028), [Tasks](https://attack.mitre.org/wiki/Technique/T1053) 				|[App Whitelisting](#application-whitelisting), [MFA](#multi-factor-authentication), [Disable NTLM/Only Use Kerberos](), [Disable AutoRun-AutoPlay](#disable-autorun-autoplay)|
|[Persistence](https://attack.mitre.org/wiki/Persistence) 					|[Logon Scripts](https://attack.mitre.org/wiki/Technique/T1037), [Run Keys](https://attack.mitre.org/wiki/Technique/T1060), [Services](https://attack.mitre.org/wiki/Technique/T1058), [Tasks](https://attack.mitre.org/wiki/Technique/T1053), [WMI](https://attack.mitre.org/wiki/Technique/T1084)|[App Whitelisting](#application-whitelisting), [Credential Hygiene](), [Least Privilege]()|
|[Privilege Escalation](https://attack.mitre.org/wiki/Privilege_Escalation)	|[Permissions Weakness](), [Exploit](), [Valid Account]() 					|[App Whitelisting](#application-whitelisting), [MFA](#multi-factor-authentication), [Disable NTLM/Only Use Kerberos](), [Patch Management](#patch-management)|
|[Defense Evasion](https://attack.mitre.org/wiki/Defense_Evasion) 			|[Binary Padding](https://attack.mitre.org/wiki/Technique/T1009), [Token Manipulation](https://attack.mitre.org/wiki/Technique/T1134), [DLL Search Order Hijacking](https://attack.mitre.org/wiki/Technique/T1038), [Indirect Command Execution](https://attack.mitre.org/wiki/Technique/T1202)|[App Whitelisting](), [Least Privilege](), [Patch Management](#patch-management)
|[Credential Access](https://attack.mitre.org/wiki/Credential_Access)		|[Brute Force](https://attack.mitre.org/wiki/Technique/T1110), [Dumping](https://attack.mitre.org/wiki/Technique/T1003), [Discovery](https://attack.mitre.org/wiki/Technique/T1081), [Capture](https://attack.mitre.org/wiki/Technique/T1056)			|[App Whitelisting](#application-whitelisting), [MFA](#multi-factor-authentication), [Disable NTLM/Only Use Kerberos](), [Strong Password Policy](),  [Type3 Logons/Restricted Admin mode RDP]()|
|[Discovery](https://attack.mitre.org/wiki/Discovery)  						|[Account](https://attack.mitre.org/wiki/Technique/T1087), [File](https://attack.mitre.org/wiki/Technique/T1083), [Share](https://attack.mitre.org/wiki/Technique/T1135), [System](https://attack.mitre.org/wiki/Technique/T1018), [System Info](https://attack.mitre.org/wiki/Technique/T1082)						|[App Whitelisting](#application-whitelisting), [MFA](#multi-factor-authentication), [Disable NTLM/Only Use Kerberos]()|
|[Lateral Movement](https://attack.mitre.org/wiki/Lateral_Movement) 		|[DCOM](https://attack.mitre.org/wiki/Technique/T1175), [WinRM](https://attack.mitre.org/wiki/Technique/T1028), [File Copy](https://attack.mitre.org/wiki/Technique/T1105), [Pass Ticket/Hash](https://attack.mitre.org/wiki/Technique/T1097), [Removable Media](https://attack.mitre.org/wiki/Technique/T1091)  |[App Whitelisting](#application-whitelisting), [MFA](#multi-factor-authentication), [Disable NTLM/Only Use Kerberos]()|
|[Collection](https://attack.mitre.org/wiki/Collection)|[Automated Collection](https://attack.mitre.org/wiki/Technique/T1119), [Input Capture](https://attack.mitre.org/wiki/Technique/T1056), [Screen Capture](https://attack.mitre.org/wiki/Technique/T1113)|[App Whitelisting](#application-whitelisting), [MFA](#multi-factor-authentication), [Encryption]()|
|[Exfiltration](https://attack.mitre.org/wiki/Exfiltration)  				|[Automated Exfiltration](https://attack.mitre.org/wiki/Technique/T1020), [Physical Medium]()|[App Whitelisting](#application-whitelisting), [Disable AutoRun-AutoPlay](#disable-autorun-autoplay), [IDPS](), [Proxy Firewall]()|
|[Command and Control](https://attack.mitre.org/wiki/Command_and_Control) 	|[Connection Proxy](https://attack.mitre.org/wiki/Technique/T1090), [Multiband](https://attack.mitre.org/wiki/Technique/T1026), [Removable Media](https://attack.mitre.org/wiki/Technique/T1092), [RAT](https://attack.mitre.org/wiki/Technique/T1219)|[App Whitelisting](#application-whitelisting), [Disable AutoRun-AutoPlay](#disable-autorun-autoplay), [IDPS](), [Proxy Firewall]()|


<br>

Using this table, let's look at some of the most useful mitigation techniques organized into two broad categories:

- [Countering Malware](#countering-malware)
- [Limiting Extent of Damage](#limiting-extent-of-damage)











## First

If not already, connect to the Internet.

Run updates:
```
sudo apt-get update && sudo apt-get upgrade -y
```

Mount a filesystem:

```
lsblk
mkdir HD
sudo mount /dev/sda1 HD
```

Curl, Sublime, Docker, KeePass, net-tools, openssh-server

```
# install scripts
HD/Scripts/install_software.sh
```
```
sudo apt install docker.io -y
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker <username>
# log out and back in
```





## Password Hardening/PW Managers


```
mkdir .keepass
chmod -R 700 .keepass
chmod 600 .keepass/database.kdbx
```


## Browser Hardening

DuckDuckGo as default page and default search engine

Go to Menu --> Add-Ons --> Plugins


- HTTPS Everywhere
- Privacy Badger
- uBlock Origin
-



## Disk Encryption
