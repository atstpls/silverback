# Deception and Defending Forward

State and non-state actors, undeterred by current laws and technologies, continue to
conduct devastating cyberattacks with minimal cost and risk.  Combined with  
the low risk of retaliation or legal repercussions, this encourages repeated
attacks against a chosen organization until it is penetrated.  

Traditional cyber security strategies which are largely defensive and reactive
in nature have proven ineffective while also costing defenders significantly
more to implement security controls than what it costs the attackers to
circumvent the controls.  

AV/Endpoints look for known bad indicators/behavioral indicators...
deception shows you intel happening right now on your network

This training document explores a collection of legal, inexpensive options to
prevent and deter attacks and improve detection and response times and
effectiveness using low risk, low cost active defense tools and techniques.  

- [Setup and General Security](#)
- [ADHD](#adhd)
  - [Honeyports](#honeyports)
  - [PortSpoof](#portspoof)
  - [Kippo](#kippo)
  - [DenyHosts](#denyhosts)
  - [Artillery](#artillery)
  - [Cryptolocked](#cryptolocked)
  - [WebLabyrinth](#weblabyrinth)
  - [Spidertrap](#spidertrap)
  - [HoneyDocs](#honeydocs)
  - [Decloak](#decloak)
- [Canary Tokens](#canary-tokens)
  - [URL Token](#url-token)
  - [DNS Token](#dns-token)
  - [Email Token](#email-token)
  - [Image Token](#imagetoken)
  - [Word Document](#word-document)
  - [PDF Document](#pdf-document)
  - [Windows Folder](#windows-folder)


## Setup

The [ADHD](https://adhdproject.github.io/#!index.md)

To install:

```
git clone https://github.com/adhdproject/buildkit-ng.git
sudo ./buildkit-ng/scripts/adhd-install.sh -i
```

![](images/Deception%20and%20Defending%20Forward/image001.png)<br><br>


If using Debian you may need to make the following change to fix the script:

```
sed -i 's/`lsb_release -is`/Ubuntu/g' adhd-install.sh
```








https://github.com/thinkst/canarytokens-docker

https://github.com/thinkst/opencanary



https://cymmetria.com/videos/cyber-deception-program/?utm_campaign=Black%20Hat%202018&utm_source=hs_automation&utm_medium=email&utm_content=64728377&_hsenc=p2ANqtz--KCUz5wBfgFuwDZH_UFC54FGYpKm9HGs4MCE15x-6ezMg9Lp1kh_qQFRGPb5LPmnW00uhTAm0C4fQvrl7Wz2xEuTzB4Q&_hsmi=64728377

https://l.cymmetria.com/thank-you-for-downloading-wp-introduction-to-cyber-deception?submissionGuid=9acafffe-7b0c-4fb3-b522-7af83e390f6d



https://cymmetria.com/videos/hunting-maturity/


git clone this for demo

https://canarytokens.org/generate#


## ADHD

Active defense tools aim to deter intruders with

- annoyance tools that increase the amount of time and work needed for the intruder
to be successful
- attribution tools used to reveal the identity and location of the intruder
- attack tools used to counter and mitigate immediate threats to the network

The majority of these tools are simple, effective, easy to implement, free, and
cause little to no impact to an organization’s systems.  In addition, these tools
also produce an extremely low amount of false positive alerts as they are only
triggered by malicious activity, and have the ability to catch slower, stealthier,
and more targeted attacks that traditional defenses tend to miss.


The more moves an intruder is forced to make, the greater the exposure and duration
of the attack, and the greater chance the defender has of detection.  Annoyance
tools such as honeypots and decoys deceive and delay an attacker while also
offering improved intelligence acquisition.  

Annoyance tools:

- alert when interacted with
- encourage intruder to sacrifice anonymity for functionality
- Cause intruders to spend time and effort on targets having no value
- provide real-time tactical intelligence including an intruder’s
geographic location, capabilities, condition, motives, and objectives
- delay intruders from reaching their objective which buys time for incident response
teams to plan and implement the appropriate countermeasures.  

- [Honeyports](#honeyports)
- [PortSpoof](#portspoof)
- [Kippo](#kippo)
- [DenyHosts](#denyhosts)
- [Artillery](#artillery)
- [Cryptolocked](#cryptolocked)
- [WebLabyritnth](#weblabyrinth)
- [Spidertrap](#spidertrap)
- [HoneyDocs](#honeydocs)
- [Decloak](#decloak)


### HoneyPorts

Honeyports is a python script that listens for incoming connections on a list of desired ports.  When a system connects, it blocks the connection and adds firewall rules to block further connections from that system.  It only filters full TCP connections making it very difficult for an intruder to spoof

```
alias honeyports=python /opt...py
honeyports -p 23
```

This tool is great at detecting and foiling probes and scans by an intruder as well as insider attacks.  It is also acts as an excellent failsafe—when an intruder attempts to access a target machine on a honeyport, the target machine immediately goes dark for that intruder, preventing further interaction and alerting the security team of the presence of an intruder or insider threat

### PortSpoof

Portspoof slows down the intruder's reconnaissance phase by making it look like all TCP ports are always open and emulating a service on every TCP port.  As a result, it is very difficult for an intruder to determine if valid software is listening on any particular port.

```
iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 1:65535 -j REDIRECT --to-ports 4444
portspoof
```

This tool is great at fooling intruders using fake port responses and running services.  This causes intruders to either waste time and effort investigating each service trying to determine which are real and which are fake, or ignore the scan results and move on leaving the system's legitimate services running untouched.


### Kippo


```
git clone https://github.com/desaster/kippo.git




```

[Kippo]() is an SSH honeypot tool that logs brute force attacks and all shell
interaction after the intruder gains access.  It presents the intruder with a
full fake Debian file system and logs and responds to all commands given.  

It allows the intruder to download files to the local box which typically yields
valuable threat intelligence such as IP addresses and URLs where the intruder is
hosting malware, attack scripts used, and usernames and passwords for other systems
compromised by the intruder.  Session logs and files downloaded are stored for
later analysis.  

When an intruder attempts to exit the SSH session, the prompt will change to
`root@localhost` while Kippo continues to capture commands and passwords.  This
can catch intruders who think they’ve logged out attempt to log into additional
compromised servers with associated usernames and passwords.

-	Open Kippo's configuration file `kippo.cfg` and change default port number to some arbitrary number such as 4633.
-	Create iptables redirect from port 22 to Kippo's on port 4633:
`sudo iptables -t nat -A PREROUTING -p tcp --dport 22 -j REDIRECT --to-port 4633`
Sophisticated attackers will look for signs that they are in a honeypot.  
Make the following changes to personalize and disguise the tool (Linuxcareer, 2015):
-	Replace the banner in `kippo/core/honeypot.py` with something personalized.
-	Clone your file system without revealing any information with Kippo's utility
`utils/createfs.py`.
With root privileges execute the following command:

```
cd /home/kippo/kippo-0.5/
utils/createfs.py > fs.pickle
```

-	Replace the operating system name located in `/etc/issue` file to another OS:
$ `echo "Linux Mint 14 Julaya \n \l" > honeyfs/etc/issue`
-	Replace information in `honeyfs/etc/passwd` to make it more believable.
-	Add interesting files to the file system to encourage the intruder to interact with it.
-	Add files to the txtcmds directory with files that simulate the real command is being run.  For example, run the sftp command on a regular system, capture the output, and place it in `/txtcmds/usr/bin/sftp`.  Now when the intruder runs the command it will return a normal output and encourage more interaction (SANS ISC, 2015).
-	Add multiple passwords that will permit the intruder access by editing `data/userdb.txt`.  
-	Edit the configuration file kippo.cfg and change your hostname to something more attractive with hostname = mgmt01.
Monitoring and Use.  Security teams can use the utils/playlog.py utility to replay an attacker's shell interactions which are stored in `log/tty/` directory.  Several additional options are available including Kippo-Graph (McRee, 2015), which provides visual representations of all Kippo activity including Top 10s for passwords, usernames, combos used, and SSH clients, as well as success ratios, successes per day/week, connections per IP, successful logins from same IP, and probes per day/week.  Kippo-Input shows all commands passed by attackers caught in the honeypot.  Kippo-Playlog plays back the attacker’s efforts as video and offers DIG and location details on the related attacker IP.

### DenyHosts

DenyHosts is a Python script that analyzes logs on an SSH server in order to report what hosts are attempting to log in, what user accounts are being targeted, and the frequency of these attempts.  Hosts which are observed making repeated attacks are blocked by updating the system’s `/etc/hosts.deny` file.  


-	Configure settings in the following file:
`/denyhosts/denyhosts.cfg`
-	Set number of max logon attempts by an invalid user account with DENY_THRESHOLD_INVALID.
-	Set number of max logon attempts by the root user account with DENY_THRESHOLD_ROOT.
-	Set number of max logon attempts by a valid user account with DENY_THRESHOLD_INVALID.
-	Set the email address to receive reports using the ADMIN_EMAIL variable.  To disable the email reports, set the ADMIN_EMAIL value to nothing.
-	Set SMTP parameters for email reporting using the SMTP_USERNAME and SMTP_PASSWORD variables.
Monitoring and Use.  DenyHosts can be run from the command line, in daemon mode, or from cron.  For optimal performance, DenyHosts must have read permissions on the `/var/log` sshd server logs and write permissions for the `/etc/hosts.deny` file.  Monitor email account for email reports on newly banned hosts, user accounts under attack, and suspicious logins.  Use history of users, hosts, and user/host combinations for research and threat profiling.

### Artillery

Artillery is an enterprise-grade honeyport, file integrity, active defense, and
system baselining tool (McRee, 2014).  Artillery monitors port connections and
when a honeyport is connected to, the tool enables iptable rules to block
further connection attempts.  It also has the ability to detect brute force
attacks, detect file system changes, and generates other indicators of
compromise.  It can integrate with threat intelligence feeds and can be deployed
across multiple systems with centralized event collection.  Artillery can also
be used for correlation and notification when an attacker IP address has previously
been identified and can be configured to alert via email and syslog.  

Configuration.  Edit the config file `/var/artillery/config`:
-	Specify folders to monitor by adding to MONITOR_FOLDERS variable.  Remove folders which are configured to change frequently to reduce false positives.
-	Specify ports to monitor adding to PORTS variable.
-	Set password with PASSWORD variable.
-	Enable HONEYPOT_BAN=ON to begin blocking traffic.  Whitelisting can be accomplished using WHITELIST_IP.
-	Configure email reporting settings, default is email alerts turned off.  EMAIL_TIMER and EMAIL_FREQUENCY are used to configure email frequency.  SMTP settings used for sending email alerts.  For receiving alerts set ALERT_USER_EMAIL to the desired email address.
-	Enable brute force monitoring with SSH_BRUTE_MONITOR and FTP_BRUTE_MONITOR.
Configure threat intelligence feeds via banlist.txt with THREAT_INTELLIGENCE_FEED and THREAT_FEED.
-	Log locally SYSLOG_TYPE=LOCAL or write to a remote syslog server with SYSLOG_TYPE=REMOTE and SYSLOG_REMOTE_HOST=”10.10.10.10”
-	Restart service with sudo service artillery restart.
Monitoring and Use.  Artillery is an excellent tool for detection of early warning indicators and attacks.  Integrate the tool into threat intelligence feeds for optimal performance.  To remove a blacklisted IP, you must manually remove the IP from `/var/artillery/banlist.txt`.


### Cryptolocked

Cryptolocked is a file system integrity tool which monitors file systems for unauthorized modification.  When an unauthorized modification or deletion of a tripfile is detected, the tool triggers failsafe countermeasures denying intruders access to the host.
Configuration.  By default the tool only deploys one tripfile (test.txt).  To change this, edit the cryptolocked.py file to show the following:
tentacles = True

Configure email alerts using the `cryptolocked.py` file:

```
fromaddr = “user@gmail.com”
username = 'username'
password = 'password'
toaddr = “user@domain.com”
```

<br>

To initiate the tool in basic, unarmed mode for testing, as root run:
`/opt/cryptolocked$ python2 ./cryptolocked.py`

To arm the tool, first run in debug mode, as root run:
`/opt/cryptolocked# python2 ./cryptolocked.py` debug

Then, arm the tool with:
`/opt/cryptolocked# python2 ./cryptolocked.py` armed

When armed, if a tripfile is modified or destroyed, the countermeasures specified
in the file `cryptolocked.py` will be executed (by default this is set to shutdown).  

Monitoring and Use.  Place sensitive-looking files that should never be accessed on the system and wait for an intruder to modify or delete.  Modification or deletion of the file kicks the intruder out immediately to prevent further tampering.  The countermeasures can be changed to something other than shutdown such as disabling all interfaces, dumping system memory, running incident response tools and commands, or simply logging the event to syslog.

### WebLabyrinth

Weblabyrinth is designed to serve an endless supply of fake pages and junk data to the intruder’s web crawler in order to cause the software or hard drive supporting the software to crash.  The web crawler is tricked into crawling infinitely which prevents intruders from automatically discovering pages and input fields.  At a minimum, intruders will be forced to manually crawl the website instead of utilizing automated tools.  Weblabyrinth returns random HTTP status codes to confuse the intruder and can also be configured to alert using email or text generation for detection by signature-based IDS.
Configuration. Configure options in the config.inc.php file:
-	Configure email alerts:
‘alert_email’ => array(
‘enabled’ => ‘true’
‘address’ => ‘root@localhost’
-	Configure text generation for detection by IDS:
‘alert_ids’ => array(
‘enabled’ => ‘true’
‘text’ => ‘entertexthere’
-	Configure alerts to trigger when crawler reaches a certain level in the config.inc.php file:
‘alert_levels_deep’ => 3,
-	Set up aliases of common web crawler targets:
alias /admin /var/www/labyrinth/
alias /dbadmin /labyrinth
alias /myadmin /labyrinth
alias /webadmin /labyrinth
Monitoring and Use.  If alert by text generation is being utilized, IDS will need a rule to work with Weblabyrinth such as the following Snort alert that indicates a webcrawler is crawling Weblabyrinth:
alert tcp any 80 -> any any (content:"entertexthere"; msg: "Weblabyrinth alert keyword detected";)
This rule can be modified to block the IP of the webcrawler.
To view Weblabyrinth logs, go to http://127.0.0.1/adminer/index.php and log in to Adminer with the following:
System:	MySQL
Server:		127.0.0.1
Username:	weblabyrinthuser
Password:	adhd
Database:	weblabyrinth
-	Once logged in, click on the table called “crawlers” and click “Select data” to view logs.

### Spidertrap

Spidertrap is a simple python script that feeds the intruder a list of randomly-generated or specifically chosen directories causing the intruder to waste time searching through a fake file system.  The tool specifically targets webcrawlers by creating hidden form elements on pages which point to the spidertrap server.
Configuration.  Default port is 8000, to change ports modify the file:
/opt/spidertrap/spidertrap.py
To run Spidertrap, use:
/opt/spidertrap$ python2 spidertrap.py
To provide a list of links to use, run the following:
/opt/spidertrap$ python2 spidertrap.py listoflinks.txt
Monitoring and Use.  Use this tool to trick webcrawlers into looping through a collection of links while achieving nothing.  To prolong the time the intruder spends on this, edit the list of links to include sensitive-looking directories, files, and other information.

### Other Annoyance Tools

Honey hostnames, simple DNS records that have no legitimate production purpose but would attract an intruder’s attention, can be planted so that when an intruder tries to resolve them, the security team is alerted (Galbraith, 2015).  The same concept can be applied using honey accounts, honey hashes (fake hashes loaded into memory to invite pass-the-hash attacks), and honey password managers


### HoneyDocs

Word Web Bugs can be embedded inside word processing documents using hidden techniques such as linked style sheets and one-pixel images.  When an intruder steals and opens these documents, a connection is made from the intruder's system back to the web bug server revealing the intruder's actual IP address.  Even if the intruder is using Tor for command and control, they may not have their document viewer configured to access the Internet through Tor.  
Configuration.  To configure the web bug server IP address:

```
cd /opt/webbugserver/
/opt/webbugserver$ vi ./web_bug.doc
Ensure current IP address of ADHD machine is listed in the CSS and image src tags of this file
Save and exit vi with:
:wq!
Copy the file over to the desired web server:
/opt/webbugserver$ sudo cp ./web_bug.doc /var/www
```

When an intruder finds, downloads, and opens the honeydoc, the document calls back to the web bug server.
Monitoring and Use.  Web bugs are an excellent way to track intellectual property.  Also, they can be planted on a system that is known to be compromised in the hopes that the intruder will open the document searching for credentials or other sensitive information.  When a web bug gets triggered, it connects back to the ADHD server which logs the connection, IP address, user agent, and timestamp.
	- To view Webbugserver logs, go to http://127.0.0.1/adminer/index.php and log in to Adminer with the following:
System:		MySQL
Server:			127.0.0.1
Username:		webbuguser
Password:		adhd
Database:		webbug
	- Once logged in click on the table called “requests” and click “Select data” to view logs.

### Decloak

Description.  Decloak uses a browser’s built in HTML rendering, Java plugins, and Flash plugins in an attempt to identify the real IP address of an intruder attempting to hide behind various anonymizing techniques.  
Configuration.  To start Decloak’s DNS server, first deactivate ADHD’s default DNS server by editing the network manager’s configuration file:

```
$ sudo sed –I ‘s/^dns/#dns/’ /etc/NetworkManager/NetworkManager.conf
Then restart the network manager:
$ sudo service network-manager restart
Add a public DNS server to resolve Internet hosts:
$ sudo sh –c “echo ‘nameserver 8.8.8.8’ >> /etc/resolv.conf”
Stop Avahi on port 5353 in order to free the port for use by Decloak:
$ sudo service avahi-daemon stop
Start Decloak DNS server:
/opt/decloak$ sudo ./dnsreflect.pl
```

Monitoring and Use.  When an intruder browses to a Decloak activated website, Decloak attempts to bypass any anonymizing techniques being used and stores the information it has gathered in a database.
-	To view Decloak logs, go to http://127.0.0.1/adminer/index.php and log in to Adminer with the following:
System:		PostgreSQL
Server:			127.0.0.1
Username:		decloakuser
Password:		adhd
Database:		decloak
-	Once logged in click on the table called “requests” and click “Select data” to view logs.


## Canary Tokens

- [URL Token](#url-token)
- [DNS Token](#dns-token)
- [Email Token](#email-token)
- [Image Token](#imagetoken)
- [Word Document](#word-document)
- [PDF Document](#pdf-document)
- [Windows Folder](#windows-folder)


### Canary Tokens


### URL Token


### DNS Token


### Email Token



### Image Token


### Word Document


### PDF document

### Windows Folder



## Summary

Following all industry guidance does not prevent an organization from falling
victim to data breaches, corporate espionage, and cyber fraud.

The use of these tools increases the attacker’s workload and uncertainty while
also increasing the sophistication required for a successful attack.  They allow
defenders to exhaust an intruder’s resources and improve tracking and response
capabilities.  This results in improved preparation for and response during an
attack while also providing valuable information on the attacker for subsequent
legal and law enforcement investigations.  Together, these tools offer a low cost
solution for private organizations to reduce long timeframes for detection and
response, improve attribution capabilities, and better protect critical data and
systems from intruders.

This provides better
protection of an organization’s critical systems and data and further deters
intruders on the network from continuing with their mission.

include a discussion of why active defense tools and techniques can be advantageous
and how they can be used to impede, decelerate, and counter an attacker’s
post-exploitation activities.
