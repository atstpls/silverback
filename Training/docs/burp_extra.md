# Implants and Attack Vectors

Use post-exploitation tools to understand attack vectors

- [Cobalt Strike Beacon](#cobalt-strike-beacon)
- [Empire](#empire)
- [PoshC2](#poshc2)
- [SILENTTRINITY](#silenttrinity)
- [Covenant](#covenant)
- [Koadic C2](#koadic-c2)


## Cobalt Strike Beacon 

user driven
- hta
- java applet
- MS Office macro
- dropper





[Kodiac]() calls its agents "zombies":

![](images/Emulating%20A%20Threat%20Actor%20-%20APT%2039/image021.png)<br><br>

Each zombie can perform various post-exploitation functions such as scanning
other hosts on the network:

![](images/Emulating%20A%20Threat%20Actor%20-%20APT%2039/image022.png)<br><br>


![](images/Emulating%20A%20Threat%20Actor%20-%20APT%2039/image023.png)<br><br>



# Burp Extra


Let's use a HTB web challenges to demonstrate called **Lernaern**:

![](images/Web%20Application%20Attack%20Tactics/image001.png)<br><br>

When you make a web request, the Proxy catches it and you have the option
to either **Forward** or **Drop** the request:

![](images/Web%20Application%20Attack%20Tactics/image003.png)<br><br>

[BurpSuite]() keeps a record of all requests and responses. Typically
an authentication request is chosen and sent to **Intruder** for use
in a dictionary attack:

![](images/Web%20Application%20Attack%20Tactics/image008.png)<br><br>

The **Position** tab allows you to select the fields that will be substituted for
words in a wordlist:

![](images/Web%20Application%20Attack%20Tactics/image010.png)<br><br>

The **Payloads** tab allows you to select a wordlist to use:

![](images/Web%20Application%20Attack%20Tactics/image011.png)<br><br>

Once you start the attack, each request is logged with its response under the
**Results** tab. Note how the correct password returned a response with a
different length (809):

![](images/Web%20Application%20Attack%20Tactics/image012.png)<br><br>

You can send this response over to the [Comparer](#comparer) along with one
of the other responses to quickly see what's different about the two, and get
the HTB flag:

![](images/Web%20Application%20Attack%20Tactics/image014.png)<br><br>
