# Log Analysis Using PowerShell


- [Technology Review](#technology-review)
  - [Modem](#modem)
  - [Router](#router)
  - [Workstation](#workstation)
  - [VPN](#vpn)
- [Firewall Log Analysis](#firewall-log-analysis)
  - [Firewall Allowed](#firewall-allowed)
  - [Firewall Touched Ports](#firewall-touched-ports)
  - [Firewall Responding Ports](#firewall-responding-ports)
  - [Firewall Top Talkers](#firewall-top-talkers)
  - [Firewall Port Traffic](#firewall-port-traffic)
- [Flow Log Analysis](#flow-log-analysis)
  - [Flow Allowed](#flow-allowed)
  - [Flow Touched Ports](#flow-touched-ports)
  - [Flow Responding Ports](#flow-responding-ports)
  - [Flow Top Talkers](#flow-top-talkers)
  - [Flow Port Traffic](#flow-port-traffic)


## Technology Review

- [Modem](#modem)
- [Router](#router)
- [Workstation](#workstation)
- [VPN](#vpn)

### Modem

A modem connects the user's ISP network to their home and translates the analog
broadband signal to digital data. Any device that is connected to the modem will
be able to acquire a public IP address.  This is most commonly a router, but can
also be a user's workstation.

### Router

A router controls the flow of digital data around the home network and allows devices
to access the Internet.  Some devices use a wired connections while others connect
wirelessly. The router has a WAN (Wide Area Network) interface and a LAN (Local Area Network)
interface.  

The WAN interface is issued a public IP address by the ISP allowing it to interact
with any systems on the Internet.  Because of all the scanners that continuously
probe public IP addresses, modern routers are configured to drop traffic from any
addresses that the user has not initiated communication.  

The LAN interface is a gateway address for the home network. Any device on the home
network that needs to connect to the Internet sends the traffic to this interface
which is usually 192.168.1.1 or similar. The router passes traffic back and forth
between the Internet and home devices.   

### Workstation

A workstation is designed to be a home device and operate behind the protections
of a router.  But in some cases a workstation is connected directly to the modem
and acquires the public IP address from the ISP.

In this scenario, the network connection profile of the workstation's interface
with the public IP becomes very important.  Windows systems have three types of
network connection profiles:

|Type|Description|
|-|-|
|Public|For operating on an untrusted network such as the Internet|
|Private|For operating on a trusted network such as a home network|
|Domain|For when computer is joined to a trusted domain|

<br>

See [Windows Firewall Rules, Profiles, and Logging](./Windows%20Firewall%20Rules%20Profiles%20Logging.md) for more details.

### VPN

A VPN (Virtual Private Network) creates a network interface on the local machine
that is bridged to an internal interface on an external server.  This allows the
local machine to send and receive all traffic through the VPN tunnel and on through
the external server's interface which is in our case is connected to the internal
network:

![](images/Evasion%20Techniques%20Used%20on%20the%20Network/image017.png)<br><br>

This serves two primary purposes.  1) All traffic to/from the network is encrypted and
cannot be monitored by those outside the network.  And 2) any connections from
the workstations will be monitored and protected by proxies.

When a workstation is using the VPN, you will see the two interfaces: the interface
with an address on the local network and the interface with an address on the
CIS1 network:

![](images/Log%20Analysis%20Using%20PowerShell/image001.png)<br><br>

With the VPN on, we can see all traffic to/from the workstation in the proxy logs.
With the VPN off, we cannot see any of the traffic to/from the workstation.  In this
case we need to look at firewall logs to see what network connections the workstation
made.

## Firewall Log Analysis

- [Firewall Allowed](#firewall-allowed)
- [Firewall Touched Ports](#firewall-touched-ports)
- [Firewall Responding Ports](#firewall-responding-ports)
- [Firewall Top Talkers](#firewall-top-talkers)
- [Firewall Port Traffic](#firewall-port-traffic)

### Firewall Allowed

```powershell
$logs = Get-FirewallLogs .\Downloads\firewall_logs\PrivateFW.log
$logs.count
```
97,292

Let's cut it down to those that were allowed
:

```powershell
$allow = $logs | ? Action -eq "ALLOW"
$allow.count
```


48,315


### Firewall Touched Ports


```powershell
$dstips = $allow | ? DstIp -notmatch "^10\." | Select -Unique DstIp
$dstips.count
```

247

Same with Ports

```powershell
$dstports = $allow | ? DstIp -notmatch "^10\." | Select -Unique DstPort
$dstports.count
```

47


```powershell
$allow | ? DstIp -notmatch "^10\." | Group-Object DstPort -NoElement | Sort -Desc Count | ft -auto


```


### Firewall Responding Ports



```
$allow | ? SrcIp -notmatch "^10\." | ? DstIp -eq '68.0.0.0' | Group DstPort -NoElement | ft -auto

Count Name
----- ----
  509 3389
   36 68
   31 80
   51 8081
```


### Firewall Top Talkers




### Firewall Top Talkers


```
$allow_old | ? DstIp -notmatch "^10\." | ? SrcIp -eq '68.0.0.0' | ? SrcPort -eq '5353' | Select DateTime,SrcIp,DstIp,DstPort | ft -auto
```


## Flow Log Analysis

Traffic to/from systems in the AWS cloud can be observed by analysis of flow logs
which can be used to provide the same data:

- [Flow Allowed](#flow-allowed)
- [Flow Touched Ports](#flow-touched-ports)
- [Flow Responding Ports](#flow-responding-ports)
- [Flow Top Talkers](#flow-top-talkers)
- [Flow Port Traffic](#flow-port-traffic)



### Flow Allowed


### Flow Touched Ports



### Flow Responding Ports



### Flow Top Talkers



### Flow Port Traffic


```
$a17 | ? DstIp -Match '10.1.3.199' |? Action -Match 'ACCEPT'| Group DstPort -NoElement |? Count -gt 3 | Sort -Desc Count
```
Count Name
----- ----
   95 8041           
   13 50010
    5 8088
```
$2ca | ? DstIp -Match '10.0.0.0' |? Action -Match 'ACCEPT'| Group DstPort -NoElement |? Count -gt 3 | Sort -Desc Count
```
Count Name
----- ----
  193 8041
   69 50010
   16 8088
   13 445
```
$7b6 | ? DstIp -Match '10.0.0.0' | ? Action -Match 'ACCEPT' |Group DstPort -NoElement |? Count -gt 3 | Sort -Desc Count
```
Count Name
----- ----
 2572 8020
  116 8088
   81 8443
   42 8025
   42 8444
   26 22
   13 23
   10 9997
    8 123
    6 2181
    6 0
    4 1433
    4 80
    4 81
    4 4560


### TOP TALKERS
```
