# Tactical Triage



Triage: Evaluate alerts/events based on result, impact, and severity 

Response: Initiate or recommend the best course of actions

Tactics: Methods or plans used to achieve a specific objective


Three primary challenges 

1. Analysts responding to a normal incident are presented complex tactical problems
Factor in a `more experienced adversary`, an `uncommon technique`, an `unfamiliar configuration`:
they can quickly become overwhelmed by events unfolding around them
   - decision-making ability suffers (response effectiveness)
   - opportunities to mitigate damage are missed (response speed)
   - accuracy of reporting on severity and impact affected (incident SA)


2. Analysts already have obstacles to overcome
 
Biases: diverse backgrounds, skill sets, and experience levels
-->> different methods, leads to different findings, conclusions, terminology

Aksi confirmation, anchoring, availability, mirror-image     bandwagon effect
   - Many competing hypotheses will be developed, compared to evidence to support or disprove
Experience
   - Analysts asked to provide tactical guidance without having ever actually practiced the task



3. No industry-standard blueprint for response actions
Guidance:
   - Need to effectively communicate conclusion, supporting artifacts, prioritization, impact, as well as the analytic process used to get there
   - Know environment, anticipate and predict adversary attempts
   - Ensure lessons learned from each incident are analyzed and acted upon


Solution ? 
DCCSCCIS353745

DCCSCCIS353725

DCCSCCIS353746

DCCSCCIS353869

<br>

How do we make this happen?

<br>

- [A Game Plan](#a-game-plan)
	- [Technical Proficiency](#technical-proficiency)
	- [Tactical Proficiency](#tactical-proficiency)
	- [The Right Combination](#the-right-combination)
- [Building a Tactical Analyst](#building-a-tactical-analyst)
	- [Evaluation Model](#evaluation-model)
	- [Response Model](#response-model)
	- [Tactics Development](#tactics-development)
- [Triage and Response Tactics](#triage-and-response-tactics)
	- [T3 Intro](#t3-intro)
	- [T3 Analyst Practice](#t3-analyst-practice)
	- [T3 Incident Simulation](#t3-incident-simulation)
	- [T3 Testing and Tuning](#t3-testing-and-tuning) 


<br>

## A Game Plan

Realistically, what do we want from this analyst?

We have tools that:
- alert on suspicious and anomalous behavior
- map to ATT&CK techniques
- interpret and baseline normal user/asset behavior and activities
- Analytics correlate events, provide visibility into the timeline of attack
- Artifacts collected for incident investigation
	* Analyst may not have subject matter expertise or substantial knowledge
- Contextual information on attacker’s tactics and techniques and history


Even with all this presented to the analyst, he/she still need answers:

1. Evaluate

2. Respond

But he/she needs to be outstanding in this regard.  Bruce Lee?

- [Technical Proficiency](#technical-proficiency)
- [Tactical Proficiency](#tactical-proficiency)
- [The Right Combination](#the-right-combination)

### Technical Proficiency 

Bruce Lee, 


"Absorb what is useful, reject what is useless, add what is essentially your own."  -Bruce Lee

Bruce Lee and INFOSEC?  

- forward thinking bout sharing information and martial arts techniques
- I'm a fan of simplicity, efficiency, using what works
- Bruce Lee was obsessed with utility of martial arts and understanding attacks/counters
- Famously took things from many different styles and created his own style based on what worked in an actual fight
- He was driven to continuously add to and improve his knowledge and skill sets
- Work ethic, creativity, never-ending drive, rose to every challenge

Of course we would love a SOC full of Bruce Lee's, but is this realistic ?   Not really

- Bruce is considered a legend, spent entire life studying martial arts, had many years of experience against advanced opponents
- Average analyst has a few years experience, few incidents, probably not much experience with top tier actors



How do we learn new skills.

Stages of Competency.

Fifth stage.


### Tactical Proficiency 

Techniques take years to learn, tactics can be changed almost immediately.... We teach analysts how to most effectively use a small set of proven techniques.

Explain Tactics:

*Client Malware Install* >   Drive by download      >   application/plugin exploit
                             Phishing               >   VBA macro, OLE object, USB

*External service ATO*   >   find/guess/phish/bypass >  U-P/API/OAUTH/2FA
                             steal/forge             >  cookie/JWT  

*Server Malware Install* >   External Service        >  Exploit, Misconfiguration    




### The Right Combination 

More important to create and grow analyst mindset, confidence, and experience.

This is not a stand-alone, comprehensive triage program.  Best used in conjunction with a triage educational program and organizational policies and protocols.  This


Average analyst

How many ransomware incidents have they worked? Zero days? APT?

Too much focus on content instead of practice.  Feedback and guidance provided?

How can we expect them to perform well on their first time working a type of incident ?
How can we expect them to succeed against top tier actors ?

Speed time to proficiency?  Proficiency of which skills? they are hundreds, thousands....



- Campaigns are tracked, tools/tactics/infrastructure is reported, but not much
in the way of response, because that depends on your specific environment

1. understand attack/counter
2. understand what should be done after a successful or unsuccessful intrusion/attack >>> Environment Specific
3. I know quickest resolution, but how to get it done ??  Knowing the path, walking the path



A better example is Nicky Rodriguez, AKA Nicky Rod

- One day decided this 22-year old wanted to learn Brazilian Jiu-Jitsu and joined a local BJJ gym
- After training less than a year, he qualifies for the 2019 ADCC World Grappling Championships
- Competes as a huge underdog, beats three former world champion blackbelts on his way to a silver medal

Now keep in mind, not long before this competition this guy was a white belt... To give you a little perspective, this is the equivalent of someone

- Entering the world of competitive swimming, and a year later is hanging with Michael Phelps in the Olympics
- Someone deciding to learn golf and after a year nearly beating Tiger Woods at the U.S. Open


He shocked the grappling world, was dubbed the blackbelt slayer, and was the subject of many of my favorite memes (image)

As soon as everything calmed down, everyone wanted to know:

- How did this happen?  
- How was this guy with a year of training able to beat athletes that have been, not just doing this for years, but winning world championships


Turns out his coach was John Danaher, widely regarded as one of the best Jiu-Jitsu minds in the game (image) and leads one of the most successful teams in the sport.  Danaher's well-known for:

1. Focus on HPT - Out of the hundreds of BJJ techniques out, specialize in few proven to consistently work against the highest levels of competition (HPT)

2. Tactics and Gameplan Strategy -


Nicky Rod's training and tactics were based on both his most likely Path To Victory and most likely Path to Failure
	- Gameplan for each match took into consideration the many ways he would lose and the few ways he might win


When this happened, I couldn't help but think about the similarities with SOC/CSIRT and our predicament

- At any time a team of analysts with low to medium experience is expected to defend against adversaries of any skill level
- Like BJJ, Everyone knows attacks/defenses  ( ATT&CK ) but there are too many to know well enough to use to optimal effect

Good news is APT is not ADVANCED, they use what works time and time again, in other words, they use HPT
And so there are also techniques that have been proven time and time again to successfully counter advanced actors...

Would it be possible to teach analysts these HPTs and replicate this accelerated learning/performance in the SOC ?

How could we give our analysts the necessary knowledge and skills to catch and stop adversaries of all skill levels ?

Could they be proficient enough to use them effectively in an incident ?

Basically, what would Nicky Rod the analyst look like? (image)


## Building the Analyst

"" -Nicky Rod



Why Another model ???

IR cycle focuses on defender's actions
Kill Chain focuses on attackers actions
Diamond model helps with identifying patterns and relationships and organizing IR data
D5 model: Deny, Disrupt, Degrade, Decieve, Destroy   
Frustrate, disrupt, slow down, cause expended energy, tie to Active Defense
F3EAD - Find, Fix, Finish, Exploit, Analyze, Disseminate  



Complexity is the enemy:

- complexity of environments and assets
- increasing number of actors, exploits, technologies, third parties
- intel collection, analysis, dissemination, incomplete/ambiguous information
- adversary intentions, capabilities, deception
- changing perimiter


We're told to study, analyze, understand the enemy... this works but there are too many  

We have incoming atomic indicators, plus behavioral indicators that are asset-agnostic, actor-agnostic, (mostly)

Don't forget past incidents, criticality, different team members focusing on different aspects of the same threat

Narrow from looking for everything, to looking for MITRE, to looking for available MITRE, to actual MITRE currently there for attacker to exploit

Model is the answer...

models help show the complex interplay between defender and adversary , provide framework for understanding each's processes

common language, general understanding, pattern recognition

- Mental models are critical to allowing individuals to process what otherwise would be an incomprehensible volume of information

https://www.cia.gov/library/center-for-the-study-of-intelligence/csi-publications/books-and-monographs/Tradecraft%20Primer-apr09.pdf



- [Evaluation Model](#evaluation-model)
- [Response Model](#response-model)
- [Tactics Development](#tactics-development)


### Evaluation Model

Used to understand what adversary has and what he/she can do with it 

rwx

c&p


### Response Model 

Used to control and mitigate impact based on evaluation 






`Successful Models`

WHAT DO ?   firewall rule, block hash, disable account, capture evidence

from a tactical perspective  
Focus on perfecting the mechanical details of HPTs

level of the mechanical form with which a student executes a certain technique more important than repetition. He considers a drill to be sufficient when the student accomplishes satisfactory mechanical form.

TCCC - US has highest casualty survival rate
Identify and treat casualties with preventable causes of death

Hemorrhage, airway obstruction, tension pneumothorax

Under-categorization (undertriage) puts systems at risk, Over-categorization (over-triage) uses
scarce time and resources

MARCH algorithm

M – Massive Hemorrhage   Control life-threatening bleeding
                         Apply limb tourniquet, hemostatic dressing, or junctional tourniquet

A – Airway Management    Chin lift or jaw thrust, NPA, recovery position, Surgical cricothyroidotomy

R – Respiration / Breathing        Tension Pneumothorax
                                   TPTX if Progressive respiratory distress or hypoxia, torso trauma, Hypotension
Treatment: needle decompression
Open / Sucking chest wound, Apply vented chest seal, burp the wound, if indicated, for breathing difficulty
Initiate pulse oximetry monitoring
If mod./severe TBI – apply supplemental O2 to maintain SpO2 >90% SpO2

C – Circulation        Bleeding, Reassess frequently, if recurrence of shock, then verify all hemorrhage is under control and repeat resuscitation above

H – Hypothermia prevention    Minimize environmental exposure, promote heat retention
Use hypothermia kit with active re-warming
If not available: dry blankets, poncho liners, sleeping bags
Warm IV fluids are preferred



1. was malicious process started   level 1  freeze host  &  creds/procs assess and freeze


2. was sensitive data exposed   level 2   creds/procs assess and freeze


Immediate containment is difference between levels 1 and 2  


for level 2, are vital signs are outside accepted parameters? is it high-risk, high-severity, cannot wait... (exposure, history, impact, etc.) condition could easily deteriorate , suggestive of condition requiring time-sensitve treatment

`Our Models`



FOCUS ON PROCS and CREDS


Blue dot tracked flights to cities, we can track processes/creds to other hosts

utilized flight data for public health,  tracking population movements to predict/anticipate how diseases spread

anonymized location data from millions of cellphones shows phones moving out of wuhan to which cities

HealthMap’s first big success came during the 2009 H1N1 (swine flu) pandemic, when it used sources that included Spanish-language online news reports to aid in early detection of an unidentified respiratory illness in Veracruz, Mexico. Five years later, it tapped the WHO Twitter feed and other sources to track the spread of the Ebola virus, which ultimately killed more than 11,000 people in West Africa

The World Health Organization now routinely uses HealthMap, ProMED and similar systems to monitor infectious disease outbreaks and inform clinicians, officials and the public. Yet big-data disease detection is still in its infancy compared with traditional methods, and the social media ingredients, in particular, have yet to make any major contribution in predicting where and how infectious diseases may strike.

So far, at least, HealthMap still doesn’t rely heavily on social media; instead it mostly tracks reports from online news sources and governments, while including some social media posts from public health professionals. Additionally, HealthMap calls on volunteers to submit weekly data to its crowdsourced disease-tracking platform, Flu Near You.



### High Percentage Responses

there is no industry-standard blueprint for response actions

Which MITRE ATT&CK techniques are easier to learn?

- https://github.com/TravisFSmith/mitre_attack

- https://www.youtube.com/watch?v=4s3pZirFCPk

Choose a tactic, choose a technique

1. can you exploit it
2. with mitigation, can you bypass
3. what artifacts are left behind

- https://raw.githubusercontent.com/TravisFSmith/mitre_attack/master/teaching/All.json


Still too much....

A response playbook is a set of steps IR will take when presented with a given threat

- Isolate the endpoint on the network
- Kill any process associated with the unauthorized software
- Ban the binary

A high percentage response is one that works consistently across all levels of environment and adversary


Narrow down MITRE TTPs to a few cards or moves

- Exfil Data
- Account creation/takeover               Access token, U/P, API key, web/svc account
- Process creation/takeover               Execute code in a process    signed/unsigned

How do we respond?

1. If they control an account, blocking IPs won't work...

- Goal is to find/create creds, inject/create processes
- revoke/change creds and kill all sessions, find/kill vector used
- identify everything of value that was available during window
- remove everything created by account following compromise


2. If they control a process,  blocking IPs may work, but...

- Goal is to dump/create creds, inject/create processes
- kill process, kill token, find and kill the vector used
- identify everything of value that was available during window
- remove everything created by process following compromise


Examples

- Authentication - change user password, but did we revoke session?  account recovery procedures?
- Network - blocked domain, blocked IP, blocked subnet, is it cloud provider, proxy, tor node, vpn ?
- KEYS - revoked key... what temp keys were created, and so on...
- Killed process - but what creds were available, AWS, SSH, etc., any sensitive information taken?
- Account Takeover - what systems is it logged on to, how many since breech


### Tactics Development

"Rehearse problem solving mechanics for success against knowledgeable and skilled adversaries"   -John Danaher

"Develop depth of knowledge and skill in small set of high percentage techniques... transform knowledge into skill through practical training" -John Danaher

 how does analyst "know" and "apply" it to an IR investigation


- More simple, is more effective
- Organize knowledge into simplified chunks you can remember under the stress of competition
- It's not how much you know but how much you can recall/retrieve/implement under stress

Analysts learn iterative decision making... Assess what is happening to the best of your ability with the information you have, and then make smaller decisions with minimum commitment to move in the direction you most highly suspect is the right o


Web Vulns, Misconfigurations, Exposed Creds,

Simulated Vulns, Simulated Owned, Simulated

Examples

- Simulated Owned System   ()
- Simulated Vulnerable App  (https://hub.docker.com/_/splunk-enterprise)


- Have a proactive goal but be ready to employ reactive counters
- integrate systems of high percentage techniques, move across systems until breakthrough is achieved
- cut down opponent movement options into a few predictable alternatives,
- learn and map out the main reactions, funnel an opponent into progressively fewer options as you work towards finishing positions
- indirect thinking to remove obstacles, disguise true intentions


Reporting
- Strategy intel is high level for business decisions (major compromises, gaps/improvements identified)
- Operational intel use Diamond Model and tell story (good guys, bad guys, tools, actions, campaign )
- Tactical intel (IOCs, TTPs,)

Be brief, make use of time

tactical - low-level supporting IR    IOCs, vulns, artifacts,

More knowledge leads to more effective reporting 

## Triage Tactics Trainer

But in order to be successful against top-tier actors, responders must understand tactics
T3 is one of our custom tools that helps us practice tactics, simulate incidents, and test/tune automated triage actions

approach problems in a way that supports the overarching goals behind a strategic effort rather than
treating each individual situation as its own entity.

intelligence-driven response
- identify when an intrusion is particularly significant to their organization

Models
- area of focus to solve the problem
- critical to understanding situations, decision making, learning how to respond correctly and consistently,


- [T3 Intro](#t3-intro)
- [T3 Analyst Practice](#t3-analyst-practice)
- [T3 Incident Simulation](#t3-incident-simulation)
- [T3 Testing and Tuning](#t3-testing-and-tuning) 


### T3 Intro

t3 role plays an adversary who may or may not know environment and SOPs so the analyst can practice actively defending

What?
- The project uses Docker to stand up an isolated network on the localhost:
- Cloned site
- Splunk instance
- Soc-Faker to simulate external traffic
- Basic console to start from/ take notes  --> Swimlane

Why?
- Helps analysts practice triage and response with familiar targets, tools, scenarios
- Helps simulate incidents to support analysis 
- Helps develop and test automated triage and response actions


How?
- Deploy portable, isolated working environment to focus more on the capabilities of the application rather than setup/troubleshooting
- Use mental models which are critical to allowing individuals to process what otherwise would be an incomprehensible volume of information
- Use a system of HPT for evaluation and response... at anytime, analyst knows what he's trying to accomplish
- Encourage precise terminology/language which allows more efficient communication and operation




TODO
- Adding in Caldera app, adversary emulation tool built on the MITRE ATT&CK framework

<br>

### T3 Analyst Practice

*PROBLEM:*



*SOLUTION:*

Analysts practice interpreting, evaluating, responding, and reporting
- learn to approach, get a clear picture of what is as situations develop
- gain ability to discriminate between what is important and what is not so they don't waste time on meaningless events
- their ability to identify attacks, make adjustments, and overcome obstacles improves

Analysts improve through various scenarios
- Focus less on tool and technique use, more towards pattern recognition and escalation
- gain familiarity of working under mental stress and time sensitive constraints
- practice improvisation with end goal in mind, adapting to variations, finding multiple different ways to achieve goal

Ninja
- confident, experienced, understand common tactics and how they are used to achieve objectives
- can succeed regardless of what techniques are used
- can prioritize response actions and provide simple, clear, and concise guidance
- provides insight and support to the incident-response

Let's walk through an example:

- [Dir Traversal Alert](#dir-traversal-alert)
- [Creds and Processes](#creds-and-processes)
- [RWX](#rwx)
- [Solve](#solve)

<br>

#### Path Traversal 

Path traversal is when a client is able to use a web application to successfully request files on the server that aren't meant to be shared.  This can 
be used to read, write, or execute files.

In this example, the analyst is presented with a Path Traversal alert from Splunk.  


#### Creds and Processes

As we build automation and response actions..

 *Credentials and Processes*


R        W/E
v         v  
C  <-->  C/P
v         v
A/S      A/S/H



Model is a representation of a real world cause and effect relationship, expressed in language, logic, or mathematics

- HPTs that require the least amount of training time to master and apply in a realworld scenario.
- develop a profound knowledge of these families and use them as a coherent system.


Like MARCH for TCCC, we need reliable way to assess endpoints and perform/confirm success of process/cred kill

Like Blue Dot tracked cities, we need reliable way to track creds/processes on other hosts/services 

Scenarios provided to give analyst the opportunity to practice categorizing
events using the models.  

For example, Set A can be used for initial assessment and Set B can be used for
remedial or follow-up assessment.


### RWX

READ  -   read files/folders    - find creds     --> access web app, pivot to another app or cloud service 
WRITE -   create/modify files   - create/takeover accounts, create/change configs, schedule commands/processes 
EXECUTE - execute code          - create/takeover processes

### Solve 





## T3 Testing and Tuning


*SOLUTION:*

learn and map out the main reactions, funnel an opponent into progressively fewer options as you work towards finishing positions

Utilize a small set of attacks specific to our threats and environment

Master a Small set of counters that work in most of these situations

Have a proactive goal but be ready to employ reactive counters

Automated Triage will help SOC support functions:
- alerting and signature development
- triage based on severity and impact to help analyst understand significance of alert, provide secondary indicators, examples of true/false positives, responses and procedures
- situational awareness, help analyst understand emerging trends, significant threats, gives a strategic understanding of threats and capabilities  

Automated triage streamlines these processes and become as efficient as possible, rehearse some of the details and be even more prepared.

Triage playboooks take the form of a simple guide that converts the knowledge from the training into a single page recipe

Compresses and accelerates IR process


- [Universal Tactics](#universal-tactics)
- [Rapid and Repeatable](#rapid-and-repeatable)
- [Base Conditions](#base-conditions)
- [Context Needed](#context-needed)
- [Implementation](#implementation)


<br>

### Universal Tactics

MUST BE FLEXIBLE WORKFLOWS  

1. If they control an account, blocking IPs won't work...

ACTOR GOALS:
- Find/create creds
- Inject/create processes

OUR GOALS:
- Kill all sessions
- Revoke/change creds
- Find and fix the vector used
- Identify everything of value that was available during window
- Remove/reverse everything created by account following compromise


2. If they control a process,  blocking IPs may work, but...

ACTOR GOALS:
- Find/create creds
- Inject/create processes

OUR GOALS:
- Kill process and tokens
- Revoke/change creds
- Find and fix the vector used
- Identify everything of value that was available during window
- Remove/reverse everything created by process following compromise





- Sample available at https://app.any.run/tasks/318bf961-2413-4e10-ada8-0e31ab29f4f7




Examples

- Authentication - change user password, but did we revoke session?  account recovery procedures?
- Network - blocked domain, blocked IP, blocked subnet, is it cloud provider, proxy, tor node, vpn ?
- KEYS - revoked key... what temp keys were created, and so on...
- Killed process - but what creds were available, AWS, SSH, etc., any sensitive information taken?
- Account Takeover - what systems is it logged on to, how many since breech


CRITICAL COMPETENCIES FOR ANALYSTS:

- Understanding attacks and counters (what's going on, what do I do next)
- Understanding our systems and environment (what/where is that)(Context such as infrastructure: AWS, GCP, Azure, DHS?)
- Understanding severity and impact (crown jewels, follow-on attacks, why that )


Similar to detections, we need to be universal, capable against all skill levels
- Too many techniques out there to know/be good enough at


### Rapid and Repeatable

MITRE recently started a program to evaluate vendor claims
- Number and complexity of techniques makes testing all of them impractical
- Began evaluating via adversary emulation, testing small sets of techniques used by a known threat  group (APT 3 , APT 29 )
- Chain techniques together as steps in a logical flow to simulate an intrusion
- Evaluate if/how vendor detects each step
- timing differences, noiseless environment, overall solid test  
- Document environment used, steps taken can be simulated using https://github.com/mitre-attack/attack-arsenal


Simplify and accelerate Triage

Automating triage will give us a rapid and reproducible standardized approach

### Base Conditions

- Client tries to visit tor site
- Client visits a service on one
- Client visits a Tor site
- Our app visited by Tor node
- Our app scanned by Tor node

Almost all the alerts we could get are reporting the possibility of one of these two broad conditions:


1. An account is being controlled (credential compromise, malware)

    This can be an account for a web service (everify), an API key for a cloud service (AWS), a local account (SYSTEM), or a domain account (atstaple).  Whatever type of account it is, we always need to know:

    - Permissions - what can it do and where (group memberships, privileges, etc.)

    - Account Owner/POC - who uses account, who owns account, who can change account

    - Impact - what services will be affected if we reset/revoke it


2. A process is being controlled (malware)

    This can be a process running on any of our assets---a workstation, a cloud instance, a public-facing server, etc.  When this happens, we need to know:

    - Associated Account - what account is the process running under (and all of its properties listed above)

    - System Owner/POC - who uses system, who owns system, who can change system

    - Impact - what services will be affected if we contain it

### Context Needed

VICTIM, INFRASTRUCTURE and CAPABILITY.

VICTIM


INFRASTRUCTURE

- Domain
    - What IP does it currently resolve to
    - When was it registered, what email address was used
    - What subdomains exist

- IP Address
    - Who owns it, uses it (WHOIS)
    - What domains are currently hosted on it


CAPABILITY

- Files
    - Contents of malicious file and/or website, screenshot
    - Reputation score, metadata, other details
    - Analysis via blog post or technical report

- Tools
    - Tool/framework being used
    - Third party/online services being utilized
    - Vulnerabilities/exploit code/POCs being used

- Techniques
    - TTPs being used
    - Applicable Kill Chain phases if available (Delivery, Persistence, C2)
    - redirector?

### Implementation

Additional information

2. Have we seen it before ?        passive dns/whois/ssl, splunk history, previous tickets  
                                  --> recon? attempts? successes?

3. How many affected ?             network traffic in splunk, PS scripts, fireeye enterprise search, EINSTIEN  
                                  --> systems, users, apps, data

4. What's the damage ?             data loss, creds, actions on objectives
                                  --> analysis, reporting, tracking,

5. How do we fix/detect ?          



