# Shortcuts and Other Useful Hacks



- [Keyboard Shortcuts](#keyboard-shortcuts)



## Keyboard Shortcuts

|||
|-|-|
|`Alt + Tab`|Switch between open applications |
|`Alt + Esc`|Minimize top application |
|`Ctrl + Tab`|Switch between browser tabs|
|`Alt + SPACE`|Open menu for current application|
|`Alt + F4`|Close the application|
|`Alt + LEFT/RIGHT`|Go to Previous/Next Page |
|`WIN + UP/DOWN`|Maximize/Minimize window|

Delivery
Spearphishing emails, masquerading domains, web shells
OWA bruteforcing and credential stealing

Recon
WMI Queries, AD enumeration, privilege checks

Actions On Objectives
Credential theft, Exfil, Timestomping

C2
Reverse SSH shells, DefaultNetworkCredentials, PowerShell AES Encryption

Persistence
WMI Event Subscription, Scheduled Task, Run key, startup tasks



Mshta.exe -> cmd.exe -> cmd.exe -> powershell.exe
Winword.exe -> winword.exe <encoded powershell>
Winword.exe -> cmd.exe -> Powershell.exe
Winword.exe -> cmd.exe -> schtasks.exe
Winword.exe -> Powershell.exe
TURNEDUP.EXE -> rundll32.exe -> cmd.exe -> reg.exe
StikyNote.exe -> iexplore.exe
CACHEMONEY.EXE -> cmd.exe -> ipconfig
CACHEMONEY.EXE -> cmd.exe -> schtasks.exe
CACHEMONEY.EXE -> cmd.exe -> bitsadmin.exe
CACHEMONEY.EXE -> cmd.exe -> taskkill.exe
