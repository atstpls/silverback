# Reliability and Resilience


- [Cloud Backups](#cloud-backups)
- [Password Manager](#password-manager)
- [U2F Authentication](#u2f-authentication)
- [Account Recovery](#account-recovery)
- [DNS Privacy](#dns-privacy)
- [VPN and SSH Tunnels](#vpn-and-ssh-tunnels)  tor
- [MFA using UTF or TOTP](#)  protect against guessing, dumping
- [Disk Encryption](#)
- [Least Privilege](#)   sudo, app armor
- [SSH Key Authentication with 2FA](#)



or


credential hygiene - Password Manager
account control - U2F + recovery procedures
cloud storage - AWS, Drive
network - dns/vpn





## Cloud Backups


## Password Manager

## U2F Authentication


## Account Recovery


## DNS Privacy


## VPN and SSH Tunnels



## MFA using UTF or TOTP


## Disk Encryption


## Least Privilege


## SSH Key Authentication with 2FA
