# Compromised Single Factor Detection and Response


## Adversary Objectives

Each of the following can be used by a web service to authenticate a user's identity:

- [First Factor](#first-factor)
- [Second Factor](#second-factor)
- [Session Credential](#session-credential)
- [OAuth Token](#oauth-token)





- [HTTPS](#https)
- [Two Factor Authentication](#two-factor-authentication)
    - [One-Time Passwords](#one-time-passwords)
    - [Hardware Tokens](#hardware-tokens)

### HTTPS

|||
|-|-|
|HTTP|Capture HTTP traffic and steal cookie|
|HTTPS|MITM adds JS to send password to attacker, serve it over HTTP|
|HSTS|Get certificate for domain name, serve over HTTPS|
|Public Key Pins|Phish users for passwords|
|2FA|MITM phishes second factor|
|   |WebAuthn is an API that allows websites to communicate with a security device to let a user log into their service.



|Type|Example|Description|
|-|-|-|
|saml|AWS|Token (assertion) contains user authentication and authorization info|
|openid||Token proves that user authenticated|
|oauth2|Azure|Token allows application to take actions on the behalf of a user|


OAuth2 does this by allowing tokens to be issued by an identity provider to these third-party applications, with the approval of the user. The client then uses the token to access the resource server on behalf

Example, [Azure](https://portal.azure.com/) will first prompt you for your account:

![](images/Exploring%20Identity%20Requirements%20For%20Cloud%20Resources/image006.png)<br><br>


local storage - javascript object to store simple strings
session storage - javascript object to store information, deleted when tab closed


While standard cookies are intended for communication with the server, web storage is intended only for storing information client-side but can be used to implement persistent tracking.

Web storage data is not automatically transmitted to the server in every HTTP request, and a web server can't directly write to Web storage. However, either of these effects can be achieved with explicit client-side scripts, allowing for fine-tuning of the desired interaction with the server.

Browsers that support web storage have the global variables sessionStorage and localStorage declared at the window level. The following JavaScript code can be used on these browsers to trigger web storage behaviour:

sessionStorage
// Store value on browser for duration of the session
sessionStorage.setItem('key', 'value');

// Retrieve value (gets deleted when browser is closed and re-opened) ...
alert(sessionStorage.getItem('key'));
localStorage
// Store value on the browser beyond the duration of the session
localStorage.setItem('key', 'value');

// Retrieve value (persists even after closing and re-opening the browser)
alert(localStorage.getItem('key'));


These start out empty:

![](images/Browser/image020.png)<br><br>

But are used when visiting a site:

![](images/Browser/image018.png)<br><br>

![](images/Browser/image019.png)<br><br>




JWTs let you do things like:

Cryptographically sign a token so you know that a token wasn't tampered with by a user.
Encrypt tokens so the contents cannot be read in plain text.
Embed JSON data INSIDE of a token string in a standard way.
Now, for the most part: pretty much everyone in the development community has agreed that if you're using any sort of OAuth, then the tokens you're using should be JSON Web Tokens.

A user sends their username/password to your server at some URL like /login.
Your server generates a JWT token for the user.
Your server returns that token to the user.
The user stores this token in their cookies, mobile device, or possible API server, where they use it to make requests.

Using JWT in the browser you have to store it in either LocalStorage or SessionStorage, which can lead to XSS attacks.

When signing, even if the transport layer gets compromised, an attacker can only read your traffic, won't be able to act as a user, as the attacker will not be able to sign requests - as the private key is not in his/her possession

When a consumer of an API makes a request it has to sign it, meaning it has to create a hash from the entire request using a private key. For that hash calculation you may use:

HTTP method
Path of the request
HTTP headers
Checksum of the HTTP payload
and a private key to create the hash
To make it work, both the consumer of the API and the provider have to have the same private key. Once you have the signature, you have to add it to the request, either in query strings or HTTP headers. Also, a date should be added as well, so you can define an expiration date.

a token-based approach with CORS enabled makes it trivial to expose APIs to different services and domains.



## Summary

Modern authentication and authorization utilizes a centralized identity management model using SAML, WS-Fed, Open ID Connect, and OAuth.

Instead of users maintaining many different credential sets, a trusted credential service provider (CSP) acts as a primary authentication point which allows users to prove their identities to different applications.

Things that could indicate a compromised token:

- Requests from a different location
- Requests dramatically increase from normal levels

Response:

- Revoke compromised tokens immediately
- Force your client to change their password immediately. In the context of a web or mobile app, force your user to reset their password immediately, preferably through some sort of multi-factor authentication flow
- Inspect client and server

Best Practices:
- Store tokens in server-side cookies that are not accessible to JavaScript
- Use WebAuthn when possible

  A user enrolls a device and no longer needs to enter their password unless they try to access the same account from a different device.  WebAuthn eliminates the need for users to type their passwords, which negates the threat from common password-stealing methods like phishing and man-in-the-middle attacks.


Blacklists, patching to include browser plugins and non-browser applications

with JWTs, it's good practice to set short expiry dates. If you're using WebSockets you can even issue JWTs with 10 minute expiry (for example) and re-issue a new one automatically every 8 minutes if the user is still connected and logged in; then when the user logs out or becomes disconnected; their last issued JWT will become invalid in only 10 minutes (at which point it becomes completely useless to an attacker).

[Tamper resistant keys](https://krebsonsecurity.com/2018/07/google-security-keys-neutralized-employee-phishing/) protect against phishing




### First Factor




### Second Factor

Second factor credentials harden the authentication process but can also be phished with social engineering or intercepted using man-in-the-middle attacks.

### Session Credential

This can be a session cookie or [web token](https://cheatsheetseries.owasp.org/cheatsheets/JSON_Web_Token_Cheat_Sheet_for_Java.html) granted to a user who has successfully authenticated that provides temporary access to the web service.

### OAuth Token

This is a token that is provided to an application or user to interact with the web service sometimes in a limited context.  The token can be discovered with scanning and reconnaissance, stolen or captured with various client-side attacks, or obtained through social engineering techniques.




### Forgot Passwords




####

Additionally, applications can check each requests’ Origin header to ensure that they are coming from a trusted source.  When the CSRF payload attempting to initiate the WebSocket handshake is sent to the server, the Origin header is set to the attacker’s malicious website.  Proper validation checks on the Origin header will see that the request is coming from an untrusted source and discard it.  

#### Anti-CSRF Tokens

A CSRF token is an additional piece of authentication data that is unique to each request made by a user.  This token can be added to each request—either in an HTTP header or the body of the request—then validated by the server before evaluating the rest of the request.  Since the token is unique to each request made by the user, an attacker would not be able to insert the correct token into their forged payload, nullifying the request.  

#### Session ID in Headers

Another way to defend against this type of attack is to use a session management system that does not send user session identifiers through cookies.  Session identifiers can be sent through HTTP headers which will not be accessible to the attacker or the browser at the time of the attack preventing an attacker from being able to authenticate to the target application as the victim.

Example, if an attacker can access a browser's cookies run JS on a browser,



- [HTTPS](#https)
- [Two Factor Authentication](#two-factor-authentication)
    - [One-Time Passwords](#one-time-passwords)
    - [Hardware Tokens](#hardware-tokens)

### HTTPS

|||
|-|-|
|HTTP|Capture HTTP traffic and steal cookie|
|HTTPS|MITM adds JS to send password to attacker, serve it over HTTP|
|HSTS|Get certificate for domain name, serve over HTTPS|
|Public Key Pins|Phish users for passwords|
|2FA|MITM phishes second factor|
|   |WebAuthn is an API that allows websites to communicate with a security device to let a user log into their service.


}

ore flexible, can send it to different domains,

local storage is available to entire origin
tabs and windows from another origin will not be able to access local storage

session storage available to window and children
windows and children can access session storage for duration of session





With OAuth you first exchange your user credentials for a 'token', and then authenticate users based on this 'token'.

user/principal, service provider/relying party, identity provider



|Type|Example|Description|
|-|-|-|
|saml|AWS|Token (assertion) contains user authentication and authorization info|
|openid||Token proves that user authenticated|
|oauth2|Azure|Token allows application to take actions on the behalf of a user|


OAuth2 does this by allowing tokens to be issued by an identity provider to these third-party applications, with the approval of the user. The client then uses the token to access the resource server on behalf

Example, [Azure](https://portal.azure.com/) will first prompt you for your account:

![](images/Cookies%20and%20Tokens/image006.png)<br><br>


JWTs aren’t magic–they’re just blobs of JSON that have been cryptographically signed. Regardless of whether they’re symmetrically or asymmetrically signed, they provide the same guarantees: you can trust that a JWT is valid and created by someone you have faith in

JWTs don’t try to encrypt your data so nobody else can see it, they simply help you verify that it was created by someone you trust.

The idea is that when someone authenticates to a website/API, the server will generate a JWT that contains the user’s ID, as well as some other critical information, and then send it to the browser/API/etc. to store as a session token.

local storage - javascript object to store simple strings
session storage - javascript object to store information, deleted when tab closed


While standard cookies are intended for communication with the server, web storage is intended only for storing information client-side but can be used to implement persistent tracking.

Web storage data is not automatically transmitted to the server in every HTTP request, and a web server can't directly write to Web storage. However, either of these effects can be achieved with explicit client-side scripts, allowing for fine-tuning of the desired interaction with the server.

Browsers that support web storage have the global variables sessionStorage and localStorage declared at the window level. The following JavaScript code can be used on these browsers to trigger web storage behaviour:

sessionStorage
// Store value on browser for duration of the session
sessionStorage.setItem('key', 'value');

// Retrieve value (gets deleted when browser is closed and re-opened) ...
alert(sessionStorage.getItem('key'));
localStorage
// Store value on the browser beyond the duration of the session
localStorage.setItem('key', 'value');

// Retrieve value (persists even after closing and re-opening the browser)
alert(localStorage.getItem('key'));


These start out empty:

![](images/Browser/image020.png)<br><br>

But are used when visiting a site:

![](images/Browser/image018.png)<br><br>

![](images/Browser/image019.png)<br><br>




In the most 'general' sense, a token is just a string that uniquely identifies a user. That's it.

People realized this, and developed a new standard for creating tokens, called the JSON Web Token standard. This standard basically provides a set of rules for creating tokens in a very specific way, which makes tokens more useful for you in general.

JWTs let you do things like:

Cryptographically sign a token so you know that a token wasn't tampered with by a user.
Encrypt tokens so the contents cannot be read in plain text.
Embed JSON data INSIDE of a token string in a standard way.
Now, for the most part: pretty much everyone in the development community has agreed that if you're using any sort of OAuth, then the tokens you're using should be JSON Web Tokens.

A user sends their username/password to your server at some URL like /login.
Your server generates a JWT token for the user.
Your server returns that token to the user.
The user stores this token in their cookies, mobile device, or possible API server, where they use it to make requests.



Nowadays JWT (JSON Web Token) is everywhere - still it is worth taking a look on potential security issues.

First let's see what JWT is!

JWT consists of three parts:

Header, containing the type of the token and the hashing algorithm
Payload, containing the claims
Signature, which can be calculated as follows if you chose HMAC SHA256: HMACSHA256( base64UrlEncode(header) + "." + base64UrlEncode(payload), secret)

```
curl --header "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ" my-website.com
```

As the previous ones, the tokens can be observed in Chrome as well:

Google Chrome JSON Web Token as a web authentication method

If you are writing APIs for native mobile applications or SPAs, JWT can be a good fit. One thing to keep in mind: to use JWT in the browser you have to store it in either LocalStorage or SessionStorage, which can lead to XSS attacks.

The cons:

Need to make extra effort to mitigate XSS attacks

When signing, even if the transport layer gets compromised, an attacker can only read your traffic, won't be able to act as a user, as the attacker will not be able to sign requests - as the private key is not in his/her possession

When a consumer of an API makes a request it has to sign it, meaning it has to create a hash from the entire request using a private key. For that hash calculation you may use:

HTTP method
Path of the request
HTTP headers
Checksum of the HTTP payload
and a private key to create the hash
To make it work, both the consumer of the API and the provider have to have the same private key. Once you have the signature, you have to add it to the request, either in query strings or HTTP headers. Also, a date should be added as well, so you can define an expiration date.

a token-based approach with CORS enabled makes it trivial to expose APIs to different services and domains.

can store more data, easier to use with non-browser applications, less overhead



- [Cross Site Scripting](#cross-site-scripting)
- [Cross Site Request Forgery](#cross-site-request-forgery)


### Cross Site Request Forgery

Cross Site Request Forgery (CSRF)
CSRF

CORS policy governs what is sent to cross domains

Here is a CSRF example from [JavaScript and Browser-Based Malware](JavaScript%20and%20Browser-Based%20Malware.md#cross-site-request-forgery) where a password change operation is performed on a remote server by an attacker using JavaScript:

![](images/Cookies%20and%20Tokens/image014.png)<br><br>
|Mitigation|Description|
|-|-|
|`HttpOnly` |ensures client scripts cannot access cookies to mitigate XSS attacks|
|`secure=true` |ensures cookies can only be set over an encrypted connection|
|`SameSite=strict`|ensures the cookie can only be sent with requests originating from the same site to prevent CSRF attacks|
|Signed Cookies|prevents clients from modifying cookies|

<br>


Example, if an attacker can access a browser's cookiesrun JS on a browser,

The cons:

Need to make extra effort to mitigate CSRF attacks (Anti-CSRF tokens)
Incompatibility with REST - as it introduces a state into a stateless protocol

a major problem with cookies is that they authenticate the entire browser. Site A can


### Cross Site Scripting

Stored Cross site scripting - the browser visits the site and is made to run untrusted content
```html
<script>document.location="http://evilsite.com"</script>
```

User visits a page that has XXS code

Reflective XSS - User receives a link to a page that echos requests.  Link contains code that's echoed back to browser and executed.
```html
http://www.example.com/index.html#title=<script>document.location="http://evilsite.com"</script>
```


DOM XSS - client side JavaScript dynamically modifies a rendered page based on content in the URL
```html
http://www.example.com/index.html#title=<script>document.location="http://evilsite.com"</script>
```

Here is a session hijacking example from [JavaScript Analysis Tools and Methodology](Javascript%20Analysis%20Tools%20and%20Methodology.md) where JavaScript is used to send a user's cookie to a remote system:

![](images/Cookies%20and%20Tokens/image052.png)<br><br>






### Digest Authentication

HTTP Digest Authentication uses hashes while sending the username and password to the server. If not included, the server sends a response with a `WWW-authenticate` attribute in the header and a nonce.


When a request for the web page is sent, the server sends back a response with a 'WWW-authenticate' attribute in the header and a 'nonce'. A 'nonce' is a string, which differs for each request. The client uses a series of hashes that involve the username and password, the requested URL, the authentication realm name and nonce, and sends the request again. The server picks the password from its data source and again goes through the same process of hashing and compares the results. Authentication is a success if the values match.




modern authentication and authorization

Thankfully the industry has made a dramatic shift towards providing robust web-based APIs and support for modern authentication and authorization such as SAML, WS-Fed, Open ID Connect, and OAuth.  This presents a unique opportunity for organizations to shift towards a centralized identity management model where one authoritative store drives the lifecycle of an identity across all applications.  With the introduction of the modern protocols, users aren’t required to maintain thousands of credentials and can instead rely upon a singular trusted credential service provider (CSP) to act as the primary authentication point allowing users to then assert their identities to applications.  This frees the application from having to be saddled with storing and managing user credentials as well as improving the user experience, not to mention using these modern protocols is far simpler for your average developer.


requests from a different location
requests dramatically increase from normal levels


then,
1. Revoke compromised tokens immediately.

2. Force your client to change their password immediately. In the context of a web or mobile app, force your user to reset their password immediately, preferably through some sort of multi-factor authentication flow

3. Inspect client and server

or browser-based applications, this means never storing your tokens in HTML5 Local Storage and instead storing tokens in server-side cookies that are not accessible to JavaScript.
